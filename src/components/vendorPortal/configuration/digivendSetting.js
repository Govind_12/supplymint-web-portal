import React from 'react';
import ShipmentTrackContain from "./shipmentTrackContain";
import Configuration from './configuration';
import DocumentFormats from './documentFormats';
import TableHeader from './tableHeader';
import UserTypeActivityChangeConfirmModal from "../../loaders/userTypeActivityChangeConfirmModal";
import FilterLoader from '../../loaders/filterLoader';
import RequestError from  '../../loaders/requestError';   
import RequestSuccess from '../../loaders/requestSuccess';
import ToastLoader from '../../loaders/toastLoader';
import VendorTransporter from '../modals/vendorTransporter';
import SettingList from './settingListModal';

export default class DigiVendSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
            tab: "documentFormat",
            allowInvoiceApproval: true,
            disAllowInvoiceApproval: false,
            allowInspectionRequired: true,
            disAllowInspectionRequired: false,
            allowInvoiceUpload: true,
            disAllowInvoiceUpload: false,
            allowMultipleASN: true,
            disAllowMultipleASN: false,
            allowCancelASN: true,
            disAllowCancelASN: false,

            allowPOConfirmation: true,
            disAllowPOConfirmation: false,
            poConfirmTAT: 0,
            uploadInvoiceTAT: 0,
            lrCreationTAT: 0,
            allowAsnMask: true,
            disAllowAsnMask: false,
            noOfPercentFulfil: 0,
            allowCreateAsnFromTo: true,
            disAllowCreateAsnFromTo: false,
            createAsnFromToTransitDay: 0,
            createAsnFromToBufferDay: 0,

            documentFormatData: "",
            settingConfigData: "",

            asnTabSaveEnable: false,
            lrTabSaveEnable: false,
            gateEntryTabSaveEnable: false,
            grcTabSaveEnable: false,

            settingTabSaveEnable: false,

            noOfReInspection: 0,
            asnBufferDays: 0,
            inspBufferDays: 0,

            headerMsg: "",
            paraMsg: "",
            radioChange: true,
            confirmModal: false,
            isSet: false,
            headerTabVal:1,
            pageName:"Open PO",
            mainHeaderObjectData:{},
            setHeaderObjectData:{},
            itemHeaderObjectData:{},

            allowConfirmGrc: true,
            disAllowConfirmGrc: false,
            allowCancelLinePendingQty: true,
            disAllowCancelLinePendingQty: false,

            allowPackingList: true,
            disAllowPackingList: false,

            toastMsg: "",
            toastLoader: false,
            allowLedgerReport: true,
            disAllowLedgerReport: false,
            allowSalesReport: true,
            disAllowSalesReport: false,
            allowStockReport: true,
            disAllowStockReport: false,

            allowOrderReport: true,
            disAllowOrderReport: false,
            allowShipmentReport: true,
            disAllowShipmentReport: false,
            allowLogisticReport: true,
            disAllowLogisticReport: false,
            allowInspectionReport: true,
            disAllowInspectionReport: false,
            
            // Transporter Variable:::

            valueConfigurationTab: false,
            transporterModal: false,
            transporter: "",
            transporterCode: "",
            transportarData: {},
            otherTransporterValue: "",
            otherTransportFlag: false,
            enableSaveValueConfigButton: false,
            otherTransporterErr: false,
            transportererr: false,

            allowUploadInvoiceTAT : true,
            disAllowUploadInvoiceTAT: false,
            allowLrCreationTAT: true,
            disAllowLrCreationTAT: false,
            allowNoOfPercentFulfil: true,
            disAllowNoOfPercentFulfil: false,
            allowNoOfReInspection: true,
            disAllowNoOfReInspection: false,
            allowAsnBufferDays: true,
            disAllowAsnBufferDays: false,
            allowInspBufferDays: true,
            disAllowInspBufferDays: false,
            settingList: false,

            //Check Uncheck variables::
            invoiceApprovalCheck: false,
            inspectionRequiredCheck: false,
            invoiceUploadRequiredCheck: false,
            multipleASNCreationCheck: false,
            asnCancellationACheck: false,
            maskASNNoCheck: false,
            confirmGRCCheck: false,
            cancelLineItemsCheck: false,
            packingListCheck: false,
            poConfirmationCheck: false,
            ledgerReportCheck: false,
            salesReportCheck: false,
            stockReportCheck: false,
            orderReportCheck: false,
            shipmentReportCheck: false,
            logisticReportCheck: false,
            inspectionReportCheck: false,
            tatForInvoiceCheck: false,
            tatForLRCreationCheck: false,
            noOfTimesReInspectionCheck: false, 
            asnBufferCheck: false,
            inspectionBufferCheck: false, 
            percentOfFulfilmentCheck: false,
            createASNCheck: false,
            totalCheckedSetting: 0,

            processSettingAllCheck: false,
            reportSettingAllCheck: false,
            processWiseTatSettingAllCheck: false,
            gateEntryCheck: false,
            //------------------------------------//

            allowGateEntry: true,
            disAllowGateEntry: false,

            getMainFixedHeader: {},
            getSetFixedHeader: {},
            getItemFixedHeader: {},
 
        }
    }

    openSettingList(e) {
        e.preventDefault();
        this.setState({
            settingList: !this.state.settingList
        });
    }
    CloseSettingList = () => {
        this.setState({ settingList: false
        });
    }
    
    componentDidMount() {
        let mainHeaderPayload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: "ENT_PENDING_ALL",
            basedOn: "ALL"
        }
        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        this.props.getStatusButtonActiveRequest();
        this.props.getAsnFormatRequest();

        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
        sessionStorage.setItem('currentPage', "RDVCONFMAIN")
        sessionStorage.setItem('currentPageName', "General Settings")
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }
    
    componentWillReceiveProps(nextProps) {
        if (nextProps.changeSetting.getAsnFormat.isSuccess) {
           this.setState({ documentFormatData: nextProps.changeSetting.getAsnFormat.data.resource },()=>{
               if(this.documentFormatChild !== undefined)
                  this.documentFormatChild.resetAll();
           })
           this.props.getAsnFormatClear();
        }

        if (nextProps.changeSetting.createAsnFormat.isSuccess) {
            this.props.getAsnFormatRequest()
            this.props.createAsnFormatClear()
        } 
        if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) {
            this.setState({ settingConfigData: nextProps.logistic.getButtonActiveConfig.data.resource})
            if( nextProps.logistic.getButtonActiveConfig.data.resource.asnApprovalPage != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.asnApprovalPage != null){
                this.setState({ allowInvoiceApproval: nextProps.logistic.getButtonActiveConfig.data.resource.asnApprovalPage === 'TRUE' ? true : false},() =>{
                    if( this.state.allowInvoiceApproval){
                       this.setState({disAllowInvoiceApproval: false })
                    }else{
                       this.setState({disAllowInvoiceApproval: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired != null){
                this.setState({ allowInspectionRequired: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowInspectionRequired){
                       this.setState({disAllowInspectionRequired: false })
                    }else{
                       this.setState({disAllowInspectionRequired: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.invoiceUploadRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.invoiceUploadRequired != null){
                this.setState({ allowInvoiceUpload: nextProps.logistic.getButtonActiveConfig.data.resource.invoiceUploadRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowInvoiceUpload){
                       this.setState({disAllowInvoiceUpload: false })
                    }else{
                       this.setState({disAllowInvoiceUpload: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.isPartialPORequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.isPartialPORequired != null){
                this.setState({ allowMultipleASN: nextProps.logistic.getButtonActiveConfig.data.resource.isPartialPORequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowMultipleASN){
                       this.setState({disAllowMultipleASN: false })
                    }else{
                       this.setState({disAllowMultipleASN: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.isLRProcessingCancelASN != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.isLRProcessingCancelASN != null){
                this.setState({ allowCancelASN: nextProps.logistic.getButtonActiveConfig.data.resource.isLRProcessingCancelASN === 'TRUE' ? true : false},() =>{
                    if( this.state.allowCancelASN){
                       this.setState({disAllowCancelASN: false })
                    }else{
                       this.setState({disAllowCancelASN: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.vendorPOConfirmation != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.vendorPOConfirmation != null){
                this.setState({ allowPOConfirmation: nextProps.logistic.getButtonActiveConfig.data.resource.vendorPOConfirmation === 'TRUE' ? true : false},() =>{
                    if( this.state.allowPOConfirmation){
                       this.setState({disAllowPOConfirmation: false })
                    }else{
                       this.setState({disAllowPOConfirmation: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.asnMask != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.asnMask != null){
                this.setState({ allowAsnMask: nextProps.logistic.getButtonActiveConfig.data.resource.asnMask === 'TRUE' ? true : false},() =>{
                    if( this.state.allowAsnMask){
                       this.setState({disAllowAsnMask: false })
                    }else{
                       this.setState({disAllowAsnMask: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablility != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablility != null){
                this.setState({ allowCreateAsnFromTo: nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablility === 'TRUE' ? true : false},() =>{
                    if( this.state.allowCreateAsnFromTo){
                       this.setState({disAllowCreateAsnFromTo: false })
                    }else{
                       this.setState({disAllowCreateAsnFromTo: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.grcVisible != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.grcVisible != null){
                this.setState({ allowConfirmGrc: nextProps.logistic.getButtonActiveConfig.data.resource.grcVisible === 'TRUE' ? true : false},() =>{
                    if( this.state.allowConfirmGrc){
                       this.setState({disAllowConfirmGrc: false })
                    }else{
                       this.setState({disAllowConfirmGrc: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.cancelLinePendingQty != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.cancelLinePendingQty != null){
                this.setState({ allowCancelLinePendingQty: nextProps.logistic.getButtonActiveConfig.data.resource.cancelLinePendingQty === 'TRUE' ? true : false},() =>{
                    if( this.state.allowCancelLinePendingQty){
                       this.setState({disAllowCancelLinePendingQty: false })
                    }else{
                       this.setState({disAllowCancelLinePendingQty: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.isPackingRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.isPackingRequired != null){
                this.setState({ allowPackingList: nextProps.logistic.getButtonActiveConfig.data.resource.isPackingRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowPackingList){
                       this.setState({disAllowPackingList: false })
                    }else{
                       this.setState({disAllowPackingList: true })
                    }
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.vendorLedgerReport != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.vendorLedgerReport != null){
                this.setState({ allowLedgerReport: nextProps.logistic.getButtonActiveConfig.data.resource.vendorLedgerReport === 'TRUE' ? true : false},() =>{
                    if( this.state.allowLedgerReport){
                       this.setState({disAllowLedgerReport: false })
                    }else{
                       this.setState({disAllowLedgerReport: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.vendorSalesReport != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.vendorSalesReport != null){
                this.setState({ allowSalesReport: nextProps.logistic.getButtonActiveConfig.data.resource.vendorSalesReport === 'TRUE' ? true : false},() =>{
                    if( this.state.allowSalesReport){
                       this.setState({disAllowSalesReport: false })
                    }else{
                       this.setState({disAllowSalesReport: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.vendorStockReport != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.vendorStockReport != null){
                this.setState({ allowStockReport: nextProps.logistic.getButtonActiveConfig.data.resource.vendorStockReport === 'TRUE' ? true : false},() =>{
                    if( this.state.allowStockReport){
                       this.setState({disAllowStockReport: false })
                    }else{
                       this.setState({disAllowStockReport: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.vendorOrderReport != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.vendorOrderReport != null){
                this.setState({ allowOrderReport: nextProps.logistic.getButtonActiveConfig.data.resource.vendorOrderReport === 'TRUE' ? true : false},() =>{
                    if( this.state.allowOrderReport){
                       this.setState({disAllowOrderReport: false })
                    }else{
                       this.setState({disAllowOrderReport: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.vendorShipmentReport != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.vendorShipmentReport != null){
                this.setState({ allowShipmentReport: nextProps.logistic.getButtonActiveConfig.data.resource.vendorShipmentReport === 'TRUE' ? true : false},() =>{
                    if( this.state.allowShipmentReport){
                       this.setState({disAllowShipmentReport: false })
                    }else{
                       this.setState({disAllowShipmentReport: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.vendorLogisticsReport != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.vendorLogisticsReport != null){
                this.setState({ allowLogisticReport: nextProps.logistic.getButtonActiveConfig.data.resource.vendorLogisticsReport === 'TRUE' ? true : false},() =>{
                    if( this.state.allowLogisticReport){
                       this.setState({disAllowLogisticReport: false })
                    }else{
                       this.setState({disAllowLogisticReport: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.vendorInspectionReport != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.vendorInspectionReport != null){
                this.setState({ allowInspectionReport: nextProps.logistic.getButtonActiveConfig.data.resource.vendorInspectionReport === 'TRUE' ? true : false},() =>{
                    if( this.state.allowInspectionReport){
                       this.setState({disAllowInspectionReport: false })
                    }else{
                       this.setState({disAllowInspectionReport: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.uploadInvoiceRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.uploadInvoiceRequired != null){
                this.setState({ allowUploadInvoiceTAT: nextProps.logistic.getButtonActiveConfig.data.resource.uploadInvoiceRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowUploadInvoiceTAT){
                       this.setState({disAllowUploadInvoiceTAT: false })
                    }else{
                       this.setState({disAllowUploadInvoiceTAT: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.lrCreationRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.lrCreationRequired != null){
                this.setState({ allowLrCreationTAT: nextProps.logistic.getButtonActiveConfig.data.resource.lrCreationRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowLrCreationTAT){
                       this.setState({disAllowLrCreationTAT: false })
                    }else{
                       this.setState({disAllowLrCreationTAT: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilmentRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilmentRequired != null){
                this.setState({ allowNoOfPercentFulfil: nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilmentRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowNoOfPercentFulfil){
                       this.setState({disAllowNoOfPercentFulfil: false })
                    }else{
                       this.setState({disAllowNoOfPercentFulfil: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.reqcRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.reqcRequired != null){
                this.setState({ allowNoOfReInspection: nextProps.logistic.getButtonActiveConfig.data.resource.reqcRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowNoOfReInspection){
                       this.setState({disAllowNoOfReInspection: false })
                    }else{
                       this.setState({disAllowNoOfReInspection: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDaysRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDaysRequired != null){
                this.setState({ allowAsnBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDaysRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowAsnBufferDays){
                       this.setState({disAllowAsnBufferDays: false })
                    }else{
                       this.setState({disAllowAsnBufferDays: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferDaysRequired != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferDaysRequired != null){
                this.setState({ allowInspBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferDaysRequired === 'TRUE' ? true : false},() =>{
                    if( this.state.allowInspBufferDays){
                       this.setState({disAllowInspBufferDays: false })
                    }else{
                       this.setState({disAllowInspBufferDays: true })
                    }  
                })
            }
            if( nextProps.logistic.getButtonActiveConfig.data.resource.gateEntryVisible != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.gateEntryVisible != null){
                this.setState({ allowGateEntry: nextProps.logistic.getButtonActiveConfig.data.resource.gateEntryVisible === 'TRUE' ? true : false},() =>{
                    if( this.state.allowGateEntry){
                       this.setState({disAllowGateEntry: false })
                    }else{
                       this.setState({disAllowGateEntry: true })
                    }  
                })
            }
            if(nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys !== null){
                this.setState({
                    noOfReInspection: nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount) : 0, 
                    asnBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays != undefined ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays) : 0,
                    inspBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays != undefined ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays) : 0,
                    poConfirmTAT: nextProps.logistic.getButtonActiveConfig.data.resource.vendorPOConfirmationTAT != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.vendorPOConfirmationTAT != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.vendorPOConfirmationTAT) : 0, 
                    uploadInvoiceTAT: nextProps.logistic.getButtonActiveConfig.data.resource.uploadInvoiceTAT != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.uploadInvoiceTAT != undefined ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.uploadInvoiceTAT) : 0,
                    lrCreationTAT: nextProps.logistic.getButtonActiveConfig.data.resource.lrCreationTAT != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.lrCreationTAT != undefined ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.lrCreationTAT) : 0,   
                    noOfPercentFulfil: nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != undefined ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment) : 0, 
                    createAsnFromToTransitDay: nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablilityTransitDays != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablilityTransitDays != undefined ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablilityTransitDays) : 0,   
                    createAsnFromToBufferDay: nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablilityBufferDays != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablilityBufferDays != undefined ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablilityBufferDays) : 0,   
                    transporterCode: nextProps.logistic.getButtonActiveConfig.data.resource.defaultTransporter != undefined ? (nextProps.logistic.getButtonActiveConfig.data.resource.defaultTransporter).split("|")[0] : "",
                    transporter: nextProps.logistic.getButtonActiveConfig.data.resource.defaultTransporter != undefined ? (nextProps.logistic.getButtonActiveConfig.data.resource.defaultTransporter).split("|")[1] : "",
                    
                    //check uncheck variables::
                    invoiceApprovalCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnApprovalPage == "TRUE" ? true : false,
                    inspectionRequiredCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.inspectionRequired == "TRUE" ? true : false,
                    invoiceUploadRequiredCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.invoiceUploadRequired == "TRUE" ? true : false,
                    multipleASNCreationCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.isPartialPORequired == "TRUE" ? true : false,
    
                    asnCancellationACheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.isLRProcessingCancelASN == "TRUE" ? true : false,
                    maskASNNoCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnMask == "TRUE" ? true : false,
                    confirmGRCCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.grcVisible == "TRUE" ? true : false,
                    cancelLineItemsCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.cancelLinePendingQty == "TRUE" ? true : false,
                    packingListCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.isPackingRequired == "TRUE" ? true : false,
                    poConfirmationCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorPOConfirmation == "TRUE" ? true : false,
                    ledgerReportCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorLedgerReport == "TRUE" ? true : false,
                    salesReportCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorSalesReport == "TRUE" ? true : false,
                    stockReportCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorStockReport == "TRUE" ? true : false,
                    orderReportCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorOrderReport == "TRUE" ? true : false,
                    shipmentReportCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorShipmentReport == "TRUE" ? true : false,
                    logisticReportCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorLogisticsReport == "TRUE" ? true : false,
                    inspectionReportCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorInspectionReport == "TRUE" ? true : false,
                    tatForInvoiceCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.uploadInvoiceRequired == "TRUE" ? true : false,
    
                    tatForLRCreationCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.lrCreationRequired == "TRUE" ? true : false,
                    noOfTimesReInspectionCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.reqcRequired == "TRUE" ? true : false, 
                    asnBufferCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnBufferDaysRequired == "TRUE" ? true : false,
                    inspectionBufferCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.inspectionBufferDaysRequired == "TRUE" ? true : false, 
                    percentOfFulfilmentCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnCreationFulfilmentRequired == "TRUE" ? true : false,
                    createASNCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnFromToApplicablility == "TRUE" ? true : false,
                    gateEntryCheck: nextProps.logistic.getButtonActiveConfig.data.resource.visibleKeys.gateEntryVisible == "TRUE" ? true : false,
    
                    //---------------------------------------------------------------------//
                },()=>this.checkedSettingCount())
            }
            
            this.props.getStatusButtonActiveClear()
            this.setState({ loader: false, settingTabSaveEnable: false, enableSaveValueConfigButton: false, 
                            otherTransportFlag: false, settingList: false})
        }

        if( nextProps.changeSetting.invoiceApprovalBasedLR.isSuccess){
            this.props.invoiceApprovalBasedLRClear();
            this.props.getStatusButtonActiveRequest();
        }
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
                nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                // let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
                let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? {...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]} : []
                let getMainCustomHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length ? {...nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]} : {...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]} : []
                this.setState({
                    getMainHeaderConfig,
                    getMainCustomHeader,
                    mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
                    getMainFixedHeader: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
                });
            }
        }
        if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getSetHeaderConfig.data.resource != null &&
                nextProps.replenishment.getSetHeaderConfig.data.basedOn == "SET") {
                //let getSetHeaderConfig = nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]) : []
                let getSetHeaderConfig = nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != {} ? {...nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]} : []
                let getSetCustomHeader = nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != {} ? Object.keys(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]).length ? {...nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]} : {...nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]} : []
                this.setState({
                    getSetHeaderConfig,
                    getSetCustomHeader,
                    setHeaderConfigDataState: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] } : {},
                    getSetFixedHeader: nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] : {},
                });
            }
        }
        if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getItemHeaderConfig.data.resource != null &&
                nextProps.replenishment.getItemHeaderConfig.data.basedOn == "ITEM") {
                //let getItemHeaderConfig = nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]) : []
                let getItemHeaderConfig = nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] != {} ? {...nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]} : []
                let getItemCustomHeader = nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] != {} ? Object.keys(nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]).length ? {...nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]} : {...nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]} : []
                this.setState({
                    getItemHeaderConfig,
                    getItemCustomHeader,
                    itemHeaderConfigDataState: nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] } : {},
                    getItemFixedHeader: nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"] : {},
                });
            }
        }
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
                nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ASN") {
                //let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
                let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? {...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]} : []
                let getMainCustomHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length ? {...nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]} : {...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]} : []
                this.setState({
                    getMainHeaderConfig,
                    getMainCustomHeader,
                    mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
                    getMainFixedHeader: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
                });
            }
        }
        if (nextProps.logistic.getAllVendorTransporter.isSuccess) {
            this.setState({
                transportarData: nextProps.logistic.getAllVendorTransporter.data !== null ? nextProps.logistic.getAllVendorTransporter.data : {},
                transporterModal : true,
            },()=> document.addEventListener('click', this.onClickCloseTransporter))
            this.props.getAllVendorTransporterClear()
            if (nextProps.logistic.getAllVendorTransporter.data.resource != null) {
                return{
                    transportarData: nextProps.logistic.getAllVendorTransporter.data
                }
            }else{
                return{
                    transportarData : {}
                } 
            }
        }
    }

    changeTabCS = (e) => {
        if( e.target.hash == "#numberformats")
          this.setState({ tab: "documentFormat" })
        else if( e.target.hash == "#lrcreationapproval")
          this.setState({ tab: "settings"})  
        else if( e.target.hash == "#tableHeaders")
          this.setState({ tab : "tableHeaders" })
        else if( e.target.hash == "#valueConfiguration")
          this.setState({ tab: "valueConfiguration"})  
    }

    invoiceApprovalBasedLR = (e) =>{
        if( e.target.id == "allow"){
            this.setState({ allowInvoiceApproval: !this.state.allowInvoiceApproval, disAllowInvoiceApproval: !this.state.disAllowInvoiceApproval },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });

        }else if( e.target.id == "disallow"){
            this.setState({ disAllowInvoiceApproval: !this.state.disAllowInvoiceApproval, allowInvoiceApproval: !this.state.allowInvoiceApproval },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }      
    }

    inspectionRequired =(e)=>{
        if( e.target.id == "allowInspRequired"){
            this.setState({ allowInspectionRequired: !this.state.allowInspectionRequired, disAllowInspectionRequired: !this.state.disAllowInspectionRequired },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });

        }else if( e.target.id == "disAllowInspRequired"){
            this.setState({ disAllowInspectionRequired: !this.state.disAllowInspectionRequired, allowInspectionRequired: !this.state.allowInspectionRequired },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    invoiceUpload =(e)=>{
        if( e.target.id == "allowInvoiceUpload"){
            this.setState({ allowInvoiceUpload: !this.state.allowInvoiceUpload, disAllowInvoiceUpload: !this.state.disAllowInvoiceUpload },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowInvoiceUpload"){
            this.setState({ disAllowInvoiceUpload: !this.state.disAllowInvoiceUpload, allowInvoiceUpload: !this.state.allowInvoiceUpload },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    multipleASN =(e)=>{
        if( e.target.id == "allowMultipleASN"){
            this.setState({ allowMultipleASN: !this.state.allowMultipleASN, disAllowMultipleASN: !this.state.disAllowMultipleASN },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowMultipleASN"){
            this.setState({ disAllowMultipleASN: !this.state.disAllowMultipleASN, allowMultipleASN: !this.state.allowMultipleASN },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    cancelASN =(e)=>{
        if( e.target.id == "allowCancelASN"){
            this.setState({ allowCancelASN: !this.state.allowCancelASN, disAllowCancelASN: !this.state.disAllowCancelASN },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowCancelASN"){
            this.setState({ disAllowCancelASN: !this.state.disAllowCancelASN, allowCancelASN: !this.state.allowCancelASN },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    poConfirmation =(e)=>{
        if( e.target.id == "allowPOConfirmation"){
            this.setState({ allowPOConfirmation: !this.state.allowPOConfirmation, disAllowPOConfirmation: !this.state.disAllowPOConfirmation },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowPOConfirmation"){
            this.setState({ disAllowPOConfirmation: !this.state.disAllowPOConfirmation, allowPOConfirmation: !this.state.allowPOConfirmation },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    asnMask =(e)=>{
        if( e.target.id == "allowAsnMask"){
            this.setState({ allowAsnMask: !this.state.allowAsnMask, disAllowAsnMask: !this.state.disAllowAsnMask },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowAsnMask"){
            this.setState({ disAllowAsnMask: !this.state.disAllowAsnMask, allowAsnMask: !this.state.allowAsnMask },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    createAsnFromTo =(e)=>{
        if( e.target.id == "allowCreateAsnFromTo"){
            this.setState({ allowCreateAsnFromTo: !this.state.allowCreateAsnFromTo, disAllowCreateAsnFromTo: !this.state.disAllowCreateAsnFromTo },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowCreateAsnFromTo"){
            this.setState({ disAllowCreateAsnFromTo: !this.state.disAllowCreateAsnFromTo, allowCreateAsnFromTo: !this.state.allowCreateAsnFromTo },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    confirmGrc =(e)=>{
        if( e.target.id == "allowConfirmGrc"){
            this.setState({ allowConfirmGrc: !this.state.allowConfirmGrc, disAllowConfirmGrc: !this.state.disAllowConfirmGrc },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowConfirmGrc"){
            this.setState({ disAllowConfirmGrc: !this.state.disAllowConfirmGrc, allowConfirmGrc: !this.state.allowConfirmGrc },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    cancelLinePendingQty =(e)=>{
        if( e.target.id == "allowCancelLinePendingQty"){
            this.setState({ allowCancelLinePendingQty: !this.state.allowCancelLinePendingQty, disAllowCancelLinePendingQty: !this.state.disAllowCancelLinePendingQty },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowCancelLinePendingQty"){
            this.setState({ disAllowCancelLinePendingQty: !this.state.disAllowCancelLinePendingQty, allowCancelLinePendingQty: !this.state.allowCancelLinePendingQty },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    packingListRequired =(e)=>{
        if( e.target.id == "allowPackingList"){
            this.setState({ allowPackingList: !this.state.allowPackingList, disAllowPackingList: !this.state.disAllowPackingList },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowPackingList"){
            this.setState({ disAllowPackingList: !this.state.disAllowPackingList, allowPackingList: !this.state.allowPackingList },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }
    ledgerReport =(e)=>{
        if( e.target.id == "allowLedgerReport"){
            this.setState({ allowLedgerReport: !this.state.allowLedgerReport, disAllowLedgerReport: !this.state.disAllowLedgerReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowLedgerReport"){
            this.setState({ disAllowLedgerReport: !this.state.disAllowLedgerReport, allowLedgerReport: !this.state.allowLedgerReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    salesReport =(e)=>{
        if( e.target.id == "allowSalesReport"){
            this.setState({ allowSalesReport: !this.state.allowSalesReport, disAllowSalesReport: !this.state.disAllowSalesReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowSalesReport"){
            this.setState({ disAllowSalesReport: !this.state.disAllowSalesReport, allowSalesReport: !this.state.allowSalesReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    stockReport =(e)=>{
        if( e.target.id == "allowStockReport"){
            this.setState({ allowStockReport: !this.state.allowStockReport, disAllowStockReport: !this.state.disAllowStockReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowStockReport"){
            this.setState({ disAllowStockReport: !this.state.disAllowStockReport, allowStockReport: !this.state.allowStockReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    orderReport =(e)=>{
        if( e.target.id == "allowOrderReport"){
            this.setState({ allowOrderReport: !this.state.allowOrderReport, disAllowOrderReport: !this.state.disAllowOrderReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowOrderReport"){
            this.setState({ disAllowOrderReport: !this.state.disAllowOrderReport, allowOrderReport: !this.state.allowOrderReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    shipmentReport =(e)=>{
        if( e.target.id == "allowShipmentReport"){
            this.setState({ allowShipmentReport: !this.state.allowShipmentReport, disAllowShipmentReport: !this.state.disAllowShipmentReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowShipmentReport"){
            this.setState({ disAllowShipmentReport: !this.state.disAllowShipmentReport, allowShipmentReport: !this.state.allowShipmentReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    logisticReport =(e)=>{
        if( e.target.id == "allowLogisticReport"){
            this.setState({ allowLogisticReport: !this.state.allowLogisticReport, disAllowLogisticReport: !this.state.disAllowLogisticReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowLogisticReport"){
            this.setState({ disAllowLogisticReport: !this.state.disAllowLogisticReport, allowLogisticReport: !this.state.allowLogisticReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    inspectionReport =(e)=>{
        if( e.target.id == "allowInspectionReport"){
            this.setState({ allowInspectionReport: !this.state.allowInspectionReport, disAllowInspectionReport: !this.state.disAllowInspectionReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowInspectionReport"){
            this.setState({ disAllowInspectionReport: !this.state.disAllowInspectionReport, allowInspectionReport: !this.state.allowInspectionReport },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    isUploadInvoiceTAT =(e)=>{
        if( e.target.id == "allowUploadInvoiceTAT"){
            this.setState({ allowUploadInvoiceTAT: !this.state.allowUploadInvoiceTAT, disAllowUploadInvoiceTAT: !this.state.disAllowUploadInvoiceTAT },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowUploadInvoiceTAT"){
            this.setState({ disAllowUploadInvoiceTAT: !this.state.disAllowUploadInvoiceTAT, allowUploadInvoiceTAT: !this.state.allowUploadInvoiceTAT },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    isLrCreationTAT =(e)=>{
        if( e.target.id == "allowLrCreationTAT"){
            this.setState({ allowLrCreationTAT: !this.state.allowLrCreationTAT, disAllowLrCreationTAT: !this.state.disAllowLrCreationTAT },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowLrCreationTAT"){
            this.setState({ disAllowLrCreationTAT: !this.state.disAllowLrCreationTAT, allowLrCreationTAT: !this.state.allowLrCreationTAT },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }
    
    isNoOfPercentFulfil =(e)=>{
        if( e.target.id == "allowNoOfPercentFulfil"){
            this.setState({ allowNoOfPercentFulfil: !this.state.allowNoOfPercentFulfil, disAllowNoOfPercentFulfil: !this.state.disAllowNoOfPercentFulfil },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowNoOfPercentFulfil"){
            this.setState({ disAllowNoOfPercentFulfil: !this.state.disAllowNoOfPercentFulfil, allowNoOfPercentFulfil: !this.state.allowNoOfPercentFulfil },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    isNoOfReInspection =(e)=>{
        if( e.target.id == "allowNoOfReInspection"){
            this.setState({ allowNoOfReInspection: !this.state.allowNoOfReInspection, disAllowNoOfReInspection: !this.state.disAllowNoOfReInspection },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowNoOfReInspection"){
            this.setState({ disAllowNoOfReInspection: !this.state.disAllowNoOfReInspection, allowNoOfReInspection: !this.state.allowNoOfReInspection },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    isAsnBufferDays =(e)=>{
        if( e.target.id == "allowAsnBufferDays"){
            this.setState({ allowAsnBufferDays: !this.state.allowAsnBufferDays, disAllowAsnBufferDays: !this.state.disAllowAsnBufferDays },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowAsnBufferDays"){
            this.setState({ disAllowAsnBufferDays: !this.state.disAllowAsnBufferDays, allowAsnBufferDays: !this.state.allowAsnBufferDays },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }
    isInspBufferDays =(e)=>{
        if( e.target.id == "allowInspBufferDays"){
            this.setState({ allowInspBufferDays: !this.state.allowInspBufferDays, disAllowInspBufferDays: !this.state.disAllowInspBufferDays },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowInspBufferDays"){
            this.setState({ disAllowInspBufferDays: !this.state.disAllowInspBufferDays, allowInspBufferDays: !this.state.allowInspBufferDays },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }
    isGateEntry =(e)=>{
        if( e.target.id == "allowGateEntry"){
            this.setState({ allowGateEntry: !this.state.allowGateEntry, disAllowGateEntry: !this.state.disAllowGateEntry },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
 
        }else if( e.target.id == "disAllowGateEntry"){
            this.setState({ disAllowGateEntry: !this.state.disAllowGateEntry, allowGateEntry: !this.state.allowGateEntry },()=>{
                this.enableDisableSaveButtonOnSettingTab()
            });
        }
    }

    saveSettingTab =(e)=>{
        let payload = [];
        let settingConfigStack = [];
            settingConfigStack.push(this.state.settingConfigData);
        this.state.settingConfigData !== "" && settingConfigStack.map( data =>{
             if( data.asnApprovalPage !== (this.state.allowInvoiceApproval ? "TRUE" : "FALSE")){
                payload.push({ key: "ASN_INVOICE_APPROVAL_PAGE",
                               value: this.state.allowInvoiceApproval ? "TRUE" : "FALSE"})
             }
             if( data.inspectionRequired !== (this.state.allowInspectionRequired ? "TRUE" : "FALSE")){
                payload.push({ key: "IS_QC_REQUIRED",
                               value: this.state.allowInspectionRequired ? "TRUE" : "FALSE"})
            }
            if( data.invoiceUploadRequired !== (this.state.allowInvoiceUpload ? "TRUE" : "FALSE")){
                payload.push({ key: "IS_INVOICE_REQUIRED",
                               value: this.state.allowInvoiceUpload ? "TRUE" : "FALSE"})
            }
            if( data.isPartialPORequired !== (this.state.allowMultipleASN ? "TRUE" : "FALSE")){
                payload.push({ key: "IS_PARTIAL_PO_REQUIRED",
                               value: this.state.allowMultipleASN ? "TRUE" : "FALSE"})
            }
            if( data.isLRProcessingCancelASN !== (this.state.allowCancelASN ? "TRUE" : "FALSE")){
                payload.push({ key: "LR_PROCESSING_CANCEL_ASN",
                               value: this.state.allowCancelASN ? "TRUE" : "FALSE"})
            }

            if( data.vendorPOConfirmation !== (this.state.allowPOConfirmation ? "TRUE" : "FALSE")){
                payload.push({ key: "VENDOR_PO_CONFIRMATION",
                               value: this.state.allowPOConfirmation ? "TRUE" : "FALSE"})
            }
            if( data.asnMask !== (this.state.allowAsnMask ? "TRUE" : "FALSE")){
                payload.push({ key: "ASN_MASK",
                               value: this.state.allowAsnMask ? "TRUE" : "FALSE"})
            }
            if( data.asnFromToApplicablility !== (this.state.allowCreateAsnFromTo ? "TRUE" : "FALSE")){
                payload.push({ key: "ASN_FROM_TO_APPLICABLE",
                               value: this.state.allowCreateAsnFromTo ? "TRUE" : "FALSE"})
            }
            if( data.grcVisible !== (this.state.allowConfirmGrc ? "TRUE" : "FALSE")){
                payload.push({ key: "GRC_VISIBLE",
                               value: this.state.allowConfirmGrc ? "TRUE" : "FALSE"})
            }
            if( data.cancelLinePendingQty !== (this.state.allowCancelLinePendingQty ? "TRUE" : "FALSE")){
                payload.push({ key: "CANCEL_LINE_PENDING_QTY",
                               value: this.state.allowCancelLinePendingQty ? "TRUE" : "FALSE"})
            }
            if( data.isPackingRequired !== (this.state.allowPackingList ? "TRUE" : "FALSE")){
                payload.push({ key: "IS_PACKING_REQUIRED",
                               value: this.state.allowPackingList ? "TRUE" : "FALSE"})
            }
            if( data.vendorLedgerReport !== (this.state.allowLedgerReport ? "TRUE" : "FALSE")){
                payload.push({ key: "VEND_LEDGER_REPORT",
                               value: this.state.allowLedgerReport ? "TRUE" : "FALSE"})
            }
            if( data.vendorSalesReport !== (this.state.allowSalesReport ? "TRUE" : "FALSE")){
                payload.push({ key: "VEND_SALES_REPORT",
                               value: this.state.allowSalesReport ? "TRUE" : "FALSE"})
            }
            if( data.vendorStockReport !== (this.state.allowStockReport ? "TRUE" : "FALSE")){
                payload.push({ key: "VEND_STOCK_REPORT",
                               value: this.state.allowStockReport ? "TRUE" : "FALSE"})
            }
            if( data.vendorOrderReport !== (this.state.allowOrderReport ? "TRUE" : "FALSE")){
                payload.push({ key: "VEND_ORDER_REPORT",
                               value: this.state.allowOrderReport ? "TRUE" : "FALSE"})
            }
            if( data.vendorShipmentReport !== (this.state.allowShipmentReport ? "TRUE" : "FALSE")){
                payload.push({ key: "VEND_SHIPMENT_REPORT",
                               value: this.state.allowShipmentReport ? "TRUE" : "FALSE"})
            }
            if( data.vendorLogisticsReport !== (this.state.allowLogisticReport ? "TRUE" : "FALSE")){
                payload.push({ key: "VEND_LOGISTICS_REPORT",
                               value: this.state.allowLogisticReport ? "TRUE" : "FALSE"})
            }
            if( data.vendorInspectionReport !== (this.state.allowInspectionReport ? "TRUE" : "FALSE")){
                payload.push({ key: "VEND_INSPECTION_REPORT",
                               value: this.state.allowInspectionReport ? "TRUE" : "FALSE"})
            }
            if( data.uploadInvoiceRequired !== (this.state.allowUploadInvoiceTAT ? "TRUE" : "FALSE")){
                payload.push({ key: "UPLOAD_INVOICE_REQUIRED",
                               value: this.state.allowUploadInvoiceTAT ? "TRUE" : "FALSE"})
            }
            if( data.lrCreationRequired !== (this.state.allowLrCreationTAT ? "TRUE" : "FALSE")){
                payload.push({ key: "LR_CREATION_REQUIRED",
                               value: this.state.allowLrCreationTAT ? "TRUE" : "FALSE"})
            }
            if( data.asnCreationFulfilmentRequired !== (this.state.allowNoOfPercentFulfil ? "TRUE" : "FALSE")){
                payload.push({ key: "ASN_CREATION_FULFILMENT_REQUIRED",
                               value: this.state.allowNoOfPercentFulfil ? "TRUE" : "FALSE"})
            }
            if( data.reqcRequired !== (this.state.allowNoOfReInspection ? "TRUE" : "FALSE")){
                payload.push({ key: "REQC_REQUIRED",
                               value: this.state.allowNoOfReInspection ? "TRUE" : "FALSE"})
            }
            if( data.asnBufferDaysRequired !== (this.state.allowAsnBufferDays ? "TRUE" : "FALSE")){
                payload.push({ key: "ASN_BUFFERDAYS_REQUIRED",
                               value: this.state.allowAsnBufferDays ? "TRUE" : "FALSE"})
            }
            if( data.inspectionBufferDaysRequired !== (this.state.allowInspBufferDays ? "TRUE" : "FALSE")){
                payload.push({ key: "INSPECTION_BUFFERDAYS_REQUIRED",
                               value: this.state.allowInspBufferDays ? "TRUE" : "FALSE"})
            }
            if( data.gateEntryVisible !== (this.state.allowGateEntry ? "TRUE" : "FALSE")){
                payload.push({ key: "GATE_ENTRY_VISIBLE",
                               value: this.state.allowGateEntry ? "TRUE" : "FALSE"})
            }

            if( Number(data.asnFromToApplicablilityTransitDays) !== Number(this.state.createAsnFromToTransitDay == "" ? 0 : this.state.createAsnFromToTransitDay)){
                payload.push({  key: "ASN_FROM_TO_APPLICABLE_TRANSITDAYS",
                                value: this.state.createAsnFromToTransitDay == "" ? 0 : this.state.createAsnFromToTransitDay })
            }
            if( Number(data.asnFromToApplicablilityBufferDays) !== Number(this.state.createAsnFromToBufferDay == "" ? 0 : this.state.createAsnFromToBufferDay)){
                payload.push({  key: "ASN_FROM_TO_APPLICABLE_BUFFERDAYS",
                                value: this.state.createAsnFromToBufferDay == "" ? 0 : this.state.createAsnFromToBufferDay })
            }

            if( Number(data.vendorPOConfirmationTAT) !== Number(this.state.poConfirmTAT == "" ? 0 : this.state.poConfirmTAT)){
                payload.push({  key: "VENDOR_PO_CONFIRMATION_TAT",
                                value: this.state.poConfirmTAT == "" ? 0 : this.state.poConfirmTAT})
            }
            if( Number(data.uploadInvoiceTAT) !== this.state.uploadInvoiceTAT){
                payload.push({ key: "UPLOAD_INVOICE_TAT",
                               value: this.state.uploadInvoiceTAT })
            }
            if( Number(data.lrCreationTAT) !== this.state.lrCreationTAT){
                payload.push({  key: "LR_CREATION_TAT",
                                value: this.state.lrCreationTAT })
            }
            if( Number(data.asnCreationFulfilment) !== Number(this.state.noOfPercentFulfil == "" ? 0 : this.state.noOfPercentFulfil)){
                payload.push({  key: "ASN_CREATION_FULFILMENT",
                                value: this.state.noOfPercentFulfil == "" ? 0 : this.state.noOfPercentFulfil })
            }

            if( Number(data.reqcCount) !== this.state.noOfReInspection){
                payload.push({  key: "REQC_COUNT",
                                value: this.state.noOfReInspection })
            }
            if( Number(data.asnBufferDays) !== this.state.asnBufferDays){
                payload.push({ key: "ASN_BUFFERDAYS",
                               value: this.state.asnBufferDays })
            }
            if( Number(data.inspectionBufferdays) !== this.state.inspBufferDays){
                payload.push({  key: "INSPECTION_BUFFERDAYS",
                                value: this.state.inspBufferDays })
            }
        })

        //FOR CHECK AND UNCHECK PAYLOAD::::
        payload.push({
            key: "VISIBLE_KEYS",
            value: {
                invoiceUploadRequired: this.state.invoiceUploadRequiredCheck ? "TRUE" : "FALSE",
                inspectionRequired: this.state.inspectionRequiredCheck ? "TRUE" : "FALSE",
                asnApprovalPage: this.state.invoiceApprovalCheck ? "TRUE" : "FALSE",
                isPartialPORequired: this.state.multipleASNCreationCheck ? "TRUE" : "FALSE",
                isLRProcessingCancelASN: this.state.asnCancellationACheck ? "TRUE" : "FALSE",
                vendorPOConfirmation: this.state.poConfirmationCheck ? "TRUE" : "FALSE",
                asnMask: this.state.maskASNNoCheck ? "TRUE" : "FALSE",
                asnFromToApplicablility: this.state.createASNCheck ? "TRUE" : "FALSE",
                grcVisible: this.state.confirmGRCCheck ? "TRUE" : "FALSE",
                cancelLinePendingQty: this.state.cancelLineItemsCheck ? "TRUE" : "FALSE",
                vendorLedgerReport: this.state.ledgerReportCheck ? "TRUE" : "FALSE",
                vendorSalesReport: this.state.salesReportCheck ? "TRUE" : "FALSE",
                vendorStockReport: this.state.stockReportCheck ? "TRUE" : "FALSE",
                vendorOrderReport: this.state.orderReportCheck ? "TRUE" : "FALSE",
                vendorShipmentReport: this.state.shipmentReportCheck ? "TRUE" : "FALSE",
                vendorLogisticsReport: this.state.logisticReportCheck ? "TRUE" : "FALSE",
                vendorInspectionReport: this.state.inspectionReportCheck ? "TRUE" : "FALSE",
                isPackingRequired: this.state.packingListCheck ? "TRUE" : "FALSE",
                asnCreationFulfilmentRequired: this.state.percentOfFulfilmentCheck ? "TRUE" : "FALSE",
                uploadInvoiceRequired: this.state.tatForInvoiceCheck ? "TRUE" : "FALSE",
                lrCreationRequired: this.state.tatForLRCreationCheck ? "TRUE" : "FALSE",
                reqcRequired: this.state.noOfTimesReInspectionCheck ? "TRUE" : "FALSE",
                asnBufferDaysRequired: this.state.asnBufferCheck ? "TRUE" : "FALSE",
                inspectionBufferDaysRequired: this.state.inspectionBufferCheck ? "TRUE" : "FALSE",
                gateEntryVisible: this.state.gateEntryCheck ? "TRUE" : "FALSE"
            }
        })

        if( payload.length > 0 ){
            this.props.invoiceApprovalBasedLRRequest(payload);
        }
    }
   
    saveDocumentFormats =(e)=>{
        if( this.documentFormatChild !== undefined ){
            this.documentFormatChild.asnPrefixErr();
            this.documentFormatChild.lrPrefixErr();
            this.documentFormatChild.gateEntryPrefixErr();
            this.documentFormatChild.grcPrefixErr();

            this.documentFormatChild.asnYearErr();
            this.documentFormatChild.lrYearErr();
            this.documentFormatChild.gateEntryYearErr();
            this.documentFormatChild.grcYearErr();

            this.documentFormatChild.asnDocErr();
            this.documentFormatChild.lrDocErr();
            this.documentFormatChild.gateEntryDocErr();
            this.documentFormatChild.grcDocErr();

        }

        let payload = {};
        if( this.documentFormatChild !== undefined && this.documentFormatChild.state != undefined ){
            
            if( this.documentFormatChild.state.tab === "asn" && !this.documentFormatChild.state.asnPrefixErr && !this.documentFormatChild.state.asnYearErr && !this.documentFormatChild.state.asnDocErr){
                payload = {asn : {
                    prefix: this.documentFormatChild.state.asnPrefixValue,
                    randomNo: this.documentFormatChild.state.asnDocValue,
                    yearFormat: this.documentFormatChild.state.asnYearValue
                }}
            }
            else if( this.documentFormatChild.state.tab === "lr" && !this.documentFormatChild.state.lrPrefixErr && !this.documentFormatChild.state.lrYearErr && !this.documentFormatChild.state.lrDocErr){
                payload = {lr : {
                    prefix: this.documentFormatChild.state.lrPrefixValue,
                    randomNo: this.documentFormatChild.state.lrDocValue,
                    yearFormat: this.documentFormatChild.state.lrYearValue
                }}
            }    
            else if( this.documentFormatChild.state.tab === "gateEntry" && !this.documentFormatChild.state.gateEntryPrefixErr && !this.documentFormatChild.state.gateEntryYearErr && !this.documentFormatChild.state.gateEntryDocErr){
                payload = {gateEntry : {
                    prefix: this.documentFormatChild.state.gateEntryPrefixValue,
                    randomNo: this.documentFormatChild.state.gateEntryDocValue,
                    yearFormat: this.documentFormatChild.state.gateEntryYearValue
                }}
            } 
            else if( this.documentFormatChild.state.tab === "grc" && !this.documentFormatChild.state.grcPrefixErr && !this.documentFormatChild.state.grcYearErr && !this.documentFormatChild.state.grcDocErr){
                payload = {grc : {
                    prefix: this.documentFormatChild.state.grcPrefixValue,
                    randomNo: this.documentFormatChild.state.grcDocValue,
                    yearFormat: this.documentFormatChild.state.grcYearValue
                }}
            }
            this.props.createAsnFormatRequest(payload);
        }
    }

    saveAllConfiguration =(e)=>{  
      if( this.state.tab === "settings"){
         this.saveSettingTab();
      }
      if( this.state.tab === "documentFormat"){
        this.saveDocumentFormats();
      }
      if( this.state.tab === "tableHeaders" && e.target.className !== "pst-save btnDisabled"){
        this.saveTableHeader()
      }
      if( this.state.tab === "valueConfiguration"){
        this.saveValueConfiguration()
      }
    }

    clearAllConfiguration =(e)=>{
        if( this.state.tab == "settings"){
            this.props.getStatusButtonActiveRequest();  
        }
        if( this.state.tab === "documentFormat"){
            this.props.getAsnFormatRequest();
        }
        if( this.state.tab === "tableHeaders"){
            this.setState({ loader: true},()=>
            this.clearTableHeader())
        }
        if( this.state.tab === "valueConfiguration"){
            this.props.getStatusButtonActiveRequest();
        }
    }

    escFun = (e) =>{  
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && (e.target.className.includes("backdrop") || e.target.className.includes("alertPopUp")))){
            this.setState({ loader: false, success: false, alert: false, transporterModal: false, settingList: false })
        }
    }

    onError =(e)=> {
        e.preventDefault();
        this.setState({ alert: false });
    }
    onRequest =(e)=> {
        e.preventDefault();
        this.setState({ success: false });
    }

    changeSet = () => {
        if (this.state.isSet) {
            this.setState({
                headerMsg: "Are you sure you want to raise purchase order on the basis of NON SET ",
                paraMsg: "Click confirm to continue.",
                radioChange: false,
                confirmModal: true,

            })
        } else {
            this.setState({

                headerMsg: "Are you sure you want to raise purchase order on the basis of  SET ",
                paraMsg: "Click confirm to continue.",
                radioChange: true,
                confirmModal: true,
            })
        }
    }

    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }

    tableHeaderTabOpen =( headerTabVal ) =>{
        if(headerTabVal == 1){
            this.setState({
                headerTabVal:headerTabVal
            })
            if(this.state.isSet == false){
                if(this.state.pageName == "Open PO"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PENDING_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Processed Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PROCESSED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Pending Inspection"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PENDING_QC",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Cancelled Inspection"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)    
                }else if(this.state.pageName == "Pending ASN Approval"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "LR Processing"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_PROCESSING_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_INTRANSIT_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Gate Entry"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_DELIVERED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_GRC_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
            }else{
                if(this.state.pageName == "Pending Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PENDING_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Processed Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PROCESSED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "ASN Under Approval"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVAL_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "LR Processing"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_PROCESSING_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Gate Entry"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_DELIVERED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_GRC_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
            }
            let displayName = this.state.pageName == "Ledger Report" ? !this.state.isSet ? "ENT_VENDOR_LEDGER" : "VEN_VENDOR_LEDGER"
                            :  this.state.pageName == "Outstanding Report" ? "ENT_OUTSTANDING"
                            :  this.state.pageName == "Sales Report" ? !this.state.isSet ? "ENT_SALES_ITEM_REPORT" : "VEN_SALES_ITEM_REPORT"
                            :  this.state.pageName == "Stock Report" ? !this.state.isSet ? "ENT_STOCK_ITEM_REPORT" : "VEN_STOCK_ITEM_REPORT"
                            :  this.state.pageName == "Orders Report" ? !this.state.isSet ? "ENT_ORDER_REPORT" : "VENDOR_ORDER_REPORT"
                            :  this.state.pageName == "Inspections Report" ? !this.state.isSet ? "ENT_INSPECTION_REPORT" : "VENDOR_INSPECTION_REPORT"
                            :  this.state.pageName == "Shipments Report" ? !this.state.isSet ? "ENT_SHIPMENT_REPORT" : "VENDOR_SHIPMENT_REPORT"
                            :  this.state.pageName == "Logistics Report" ? !this.state.isSet ? "ENT_LOGISTICS_REPORT" : "VENDOR_LOGISTICS_REPORT"
                            : ""
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: displayName,
                basedOn: "ALL"
            }
            if( displayName !== null && displayName !== undefined && displayName !== "")
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        } 
        if(headerTabVal == 2){
            this.setState({
                headerTabVal:headerTabVal
            })
            if(this.state.isSet == false){
                if(this.state.pageName == "Open PO"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PENDING_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "Processed Orders"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PROCESSED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_CANCELLED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "Pending Inspection"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "Cancelled Inspection"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)    
                }else if(this.state.pageName == "Pending ASN Approval"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "LR Processing"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_PROCESSING_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_ITEM",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "Gate Entry"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_DELIVERED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_GRC_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
            }else{
                if(this.state.pageName == "Pending Orders"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PENDING_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "Processed Orders"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PROCESSED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_CANCELLED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "ASN Under Approval"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVAL_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_CANCELLED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "LR Processing"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_PROCESSING_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }else if(this.state.pageName == "Gate Entry"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_DELIVERED_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let setHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_GRC_SET",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(setHeaderPayload)
                }
            }
        } 
        if(headerTabVal == 3){
            this.setState({
                headerTabVal:headerTabVal
            })
            if(this.state.isSet == false){
                if(this.state.pageName == "Open PO"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PENDING_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "Processed Orders"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PROCESSED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_CANCELLED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "Pending Inspection"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "Cancelled Inspection"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_ITEM",
                        basedOn: "SET"
                    }
                    this.props.getSetHeaderConfigRequest(itemHeaderPayload)     
                }else if(this.state.pageName == "Pending ASN Approval"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "LR Processing"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_PROCESSING_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_INTRANSIT_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "Gate Entry"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_DELIVERED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_GRC_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
            }else{
                if(this.state.pageName == "Pending Orders"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PENDING_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "Processed Orders"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PROCESSED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_CANCELLED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "ASN Under Approval"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVAL_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_CANCELLED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "LR Processing"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_PROCESSING_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }else if(this.state.pageName == "Gate Entry"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_DELIVERED_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let itemHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_GRC_ITEM",
                        basedOn: "ITEM"
                    }
                    this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                }
            }
        }
        if(headerTabVal == 4){
            this.setState({
                headerTabVal:headerTabVal
            })
            if(this.state.isSet == false){
                if(this.state.pageName == "Processed Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PROCESSED_ASN",
                        basedOn: "ASN"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Goods In-Transit"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_INTRANSIT_ASN",
                        basedOn: "ASN"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
            }else{
                if(this.state.pageName == "Processed Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PROCESSED_ASN",
                        basedOn: "ASN"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Goods In-Transit"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_ASN",
                        basedOn: "ASN"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }

            }
        }
    }
    openConfigurationHeadersPageName = (data) => {
        for (var i=0; i< document.getElementsByClassName("modal_tableBox").length ;i++){
            document.getElementsByClassName("modal_tableBox")[i].value = ""
        }
        this.setState({
            pageName:data,
            // isSet:false,
            headerTabVal:1,
            mainHeaderObjectData:{},
            setHeaderObjectData:{},
            itemHeaderObjectData:{},
        })
        if(data == "Open PO"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_PENDING_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }else if(this.state.pageName == "Pending Orders"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "VENDOR_PENDING_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }else if(data == "Processed Orders"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: !this.state.isSet ? "ENT_PROCESSED_ALL" : "VENDOR_PROCESSED_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }
        else if(data == "Cancelled Orders"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: !this.state.isSet ? "ENT_CANCELLED_ALL" : "VENDOR_CANCELLED_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }else if(data == "Pending Inspection"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_PENDING_QC",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }else if(data == "Cancelled Inspection"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_ASN_CANCELLED_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)    
        }else if(data == "Pending ASN Approval"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_ASN_APPROVAL_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }else if(data == "ASN Under Approval"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "VENDOR_ASN_APPROVAL_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }
        else if(data == "Approved ASN"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_ASN_APPROVED_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }else if(data == "Rejected ASN"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: !this.state.isSet ? "ENT_ASN_CANCELLED_ALL" : "VENDOR_ASN_CANCELLED_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }else if(data == "LR Processing"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: !this.state.isSet ? "ENT_LR_PROCESSING_ALL" : "VENDOR_LR_PROCESSING_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }
        else if(data == "Goods In-Transit"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: !this.state.isSet ? "ENT_LR_INTRANSIT_ALL" : "VENDOR_LR_INTRANSIT_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }else if(data == "Gate Entry"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: !this.state.isSet ? "ENT_LR_DELIVERED_ALL" : "VENDOR_LR_DELIVERED_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }
        else if(data == "Grc Status"){
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: !this.state.isSet ? "ENT_LR_GRC_ALL" : "VENDOR_LR_GRC_ALL",
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }
        else{
            let displayName = data == "Ledger Report" ? !this.state.isSet ? "ENT_VENDOR_LEDGER" : "VEN_VENDOR_LEDGER"
                              :  data == "Outstanding Report" ? "ENT_OUTSTANDING"
                              :  data == "Sales Report" ? !this.state.isSet ? "ENT_SALES_ITEM_REPORT" : "VEN_SALES_ITEM_REPORT"
                              :  data == "Stock Report" ? !this.state.isSet ? "ENT_STOCK_ITEM_REPORT" : "VEN_STOCK_ITEM_REPORT"
                              :  data == "Orders Report" ? !this.state.isSet ? "ENT_ORDER_REPORT" : "VENDOR_ORDER_REPORT"
                              :  data == "Inspections Report" ? !this.state.isSet ? "ENT_INSPECTION_REPORT" : "VENDOR_INSPECTION_REPORT"
                              :  data == "Shipments Report" ? !this.state.isSet ? "ENT_SHIPMENT_REPORT" : "VENDOR_SHIPMENT_REPORT"
                              :  data == "Logistics Report" ? !this.state.isSet ? "ENT_LOGISTICS_REPORT" : "VENDOR_LOGISTICS_REPORT"
                              : ""
            let mainHeaderPayload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: displayName,
                basedOn: "ALL"
            }
            this.props.getMainHeaderConfigRequest(mainHeaderPayload)
        }
    }
    
    changesIsSet = () =>{
        //Retailer Headers::
        if(this.state.isSet == true){
            this.setState({
                isSet:!this.state.isSet,       
                headerTabVal:1,
                pageName: this.state.pageName == "Pending Orders" ? "Open PO" : this.state.pageName == "ASN Under Approval" ? "Pending ASN Approval" 
                          : this.state.pageName 
            }, ()=>{
                if(this.state.pageName == "Open PO"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PENDING_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Processed Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PROCESSED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Pending Inspection"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PENDING_QC",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Pending ASN Approval"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "LR Processing"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_PROCESSING_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_INTRANSIT_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Gate Entry"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_DELIVERED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == ">Grc Status"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_GRC_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else{
                    let displayName = this.state.pageName == "Ledger Report" ? !this.state.isSet ? "ENT_VENDOR_LEDGER" : "VEN_VENDOR_LEDGER"
                            :  this.state.pageName == "Outstanding Report" ? "ENT_OUTSTANDING"
                            :  this.state.pageName == "Sales Report" ? !this.state.isSet ? "ENT_SALES_ITEM_REPORT" : "VEN_SALES_ITEM_REPORT"
                            :  this.state.pageName == "Stock Report" ? !this.state.isSet ? "ENT_STOCK_ITEM_REPORT" : "VEN_STOCK_ITEM_REPORT"
                            :  this.state.pageName == "Orders Report" ? !this.state.isSet ? "ENT_ORDER_REPORT" : "VENDOR_ORDER_REPORT"
                            :  this.state.pageName == "Inspections Report" ? !this.state.isSet ? "ENT_INSPECTION_REPORT" : "VENDOR_INSPECTION_REPORT"
                            :  this.state.pageName == "Shipments Report" ? !this.state.isSet ? "ENT_SHIPMENT_REPORT" : "VENDOR_SHIPMENT_REPORT"
                            :  this.state.pageName == "Logistics Report" ? !this.state.isSet ? "ENT_LOGISTICS_REPORT" : "VENDOR_LOGISTICS_REPORT"
                            : ""
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: displayName,
                        basedOn: "ALL"
                    }
                    if( displayName !== null && displayName !== undefined && displayName !== "")
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
            })  

        }else{//Vendor Header:: 
            this.setState({
                isSet:!this.state.isSet,
                headerTabVal:1,
                pageName: this.state.pageName == "Open PO" ? "Pending Orders" : this.state.pageName == "Pending ASN Approval" ? "ASN Under Approval" : 
                          this.state.pageName == "Outstanding Report" ? "Pending Orders" : this.state.pageName == "Pending Inspection" ? "Pending Orders"
                          : this.state.pageName == "Cancelled Inspection" ? "Pending Orders" : this.state.pageName
            },()=>{
                if(this.state.pageName == "Pending Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PENDING_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Processed Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PROCESSED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "ASN Under Approval"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVAL_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_CANCELLED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "LR Processing"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_PROCESSING_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }else if(this.state.pageName == "Gate Entry"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_DELIVERED_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_GRC_ALL",
                        basedOn: "ALL"
                    }
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
                else{
                    let displayName = this.state.pageName == "Ledger Report" ? !this.state.isSet ? "ENT_VENDOR_LEDGER" : "VEN_VENDOR_LEDGER"
                            :  this.state.pageName == "Outstanding Report" ? "ENT_OUTSTANDING"
                            :  this.state.pageName == "Sales Report" ? !this.state.isSet ? "ENT_SALES_ITEM_REPORT" : "VEN_SALES_ITEM_REPORT"
                            :  this.state.pageName == "Stock Report" ? !this.state.isSet ? "ENT_STOCK_ITEM_REPORT" : "VEN_STOCK_ITEM_REPORT"
                            :  this.state.pageName == "Orders Report" ? !this.state.isSet ? "ENT_ORDER_REPORT" : "VENDOR_ORDER_REPORT"
                            :  this.state.pageName == "Inspections Report" ? !this.state.isSet ? "ENT_INSPECTION_REPORT" : "VENDOR_INSPECTION_REPORT"
                            :  this.state.pageName == "Shipments Report" ? !this.state.isSet ? "ENT_SHIPMENT_REPORT" : "VENDOR_SHIPMENT_REPORT"
                            :  this.state.pageName == "Logistics Report" ? !this.state.isSet ? "ENT_LOGISTICS_REPORT" : "VENDOR_LOGISTICS_REPORT"
                            : ""
                    let mainHeaderPayload = {
                        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                        attributeType: "TABLE HEADER",
                        displayName: displayName,
                        basedOn: "ALL"
                    }
                    if( displayName !== null && displayName !== undefined && displayName !== "")
                    this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                }
            })
        }
    }

    mainHeaderInputChange = (e,data) => {
        // const map =this.state.mainHeaderConfigDataState
        // var dataaa=getKeyByValue(map,data)
        this.state.mainHeaderObjectData[data]=e.target.value;
        // clean(this.state.mainHeaderObjectData)
        this.state.getMainCustomHeader[data] = e.target.value;
        this.setState({
            mainHeaderObjectData:this.state.mainHeaderObjectData,
            getMainCustomHeader: this.state.getMainCustomHeader
        })
    }
    setHeaderInputChange = (e,data) => {
        // const map =this.state.setHeaderConfigDataState
        // var dataaa=getKeyByValue(map,data)
        this.state.setHeaderObjectData[data]=e.target.value;
        // clean(this.state.setHeaderObjectData)
        this.state.getSetCustomHeader[data] = e.target.value;
        this.setState({
            setHeaderObjectData:this.state.setHeaderObjectData,
            getSetCustomHeader: this.state.getSetCustomHeader
        })
    }
    itemHeaderInputChange = (e,data) => {
        // const map =this.state.itemHeaderConfigDataState
        // var dataaa=getKeyByValue(map,data)
        this.state.itemHeaderObjectData[data]=e.target.value
        // clean(this.state.itemHeaderObjectData)
        this.state.getItemCustomHeader[data] = e.target.value;
        this.setState({
            itemHeaderObjectData:this.state.itemHeaderObjectData,
            getItemCustomHeader: this.state.getItemCustomHeader
        })
    }
    saveTableHeader = () => {
        if( this.state.headerTabVal == 1 ){
            clean(this.state.mainHeaderObjectData)
            this.setState({
                mainHeaderObjectData: this.state.mainHeaderObjectData
            },()=>{
                if(this.state.isSet == false){
                    if(this.state.pageName == "Open PO"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_PENDING_ALL",
                        // }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "ORDERS",
                            section: "PENDING-ORDERS",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PENDING_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "Processed Orders"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_PROCESSED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "ORDERS",
                            section: "PROCESSED-ORDERS",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PROCESSED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                    else if(this.state.pageName == "Cancelled Orders"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_CANCELLED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "ORDERS",
                            section: "CANCELLED-ORDERS",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_CANCELLED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "Pending Inspection"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_PENDING_QC",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "SHIPMENT",
                            section: "ASN-APPROVAL",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PENDING_QC",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "Cancelled Inspection"){
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "SHIPMENT",
                            section: "ASN-CANCELLED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_CANCELLED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
            
                        this.props.createMainHeaderConfigRequest(payload)  
                    }else if(this.state.pageName == "Pending ASN Approval"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_ASN_APPROVAL_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "SHIPMENT",
                            section: "ASN-APPROVAL",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVAL_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                    else if(this.state.pageName == "Approved ASN"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_ASN_APPROVED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "SHIPMENT",
                            section: "ASN-APPROVED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "Rejected ASN"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_ASN_CANCELLED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "SHIPMENT",
                            section: "ASN-CANCELLED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_CANCELLED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "LR Processing"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_LR_PROCESSING_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "LOGISTIC",
                            section: "LR-PROCESSING",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_PROCESSING_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                    else if(this.state.pageName == "Goods In-Transit"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_LR_INTRANSIT_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "LOGISTIC",
                            section: "LR-INTRANSIT",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_INTRANSIT_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "Gate Entry"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_LR_DELIVERED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "LOGISTIC",
                            section: "LR-DELIVERED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_DELIVERED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                    else if(this.state.pageName == "Grc Status"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "ENT_LR_GRC_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "LOGISTIC",
                            section: "LR-DELIVERED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_GRC_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                }else{
                    if(this.state.pageName == "Pending Orders"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_PENDING_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "ORDERS",
                            section: "PENDING-ORDERS",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PENDING_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "Processed Orders"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_PROCESSED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "ORDERS",
                            section: "PROCESSED-ORDERS",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PROCESSED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                    else if(this.state.pageName == "Cancelled Orders"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_CANCELLED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "ORDERS",
                            section: "CANCELLED-ORDERS",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_CANCELLED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "ASN Under Approval"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_ASN_APPROVAL_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "SHIPMENT",
                            section: "ASN-APPROVAL",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVAL_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                    else if(this.state.pageName == "Approved ASN"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_ASN_APPROVED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "SHIPMENT",
                            section: "ASN-APPROVED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "Rejected ASN"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_ASN_CANCELLED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "SHIPMENT",
                            section: "ASN-CANCELLED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_CANCELLED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "LR Processing"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_LR_PROCESSING_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "LOGISTIC",
                            section: "LR-PROCESSING",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_PROCESSING_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                    else if(this.state.pageName == "Goods In-Transit"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_LR_INTRANSIT_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "LOGISTIC",
                            section: "LR-INTRANSIT",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_INTRANSIT_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }else if(this.state.pageName == "Gate Entry"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_LR_DELIVERED_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "LOGISTIC",
                            section: "LR-DELIVERED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_DELIVERED_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                    else if(this.state.pageName == "Grc Status"){
                        // let mainHeaderPayload = {
                        //     attributeType:"TABLE HEADER",
                        //     updatedFields:this.state.mainHeaderObjectData,
                        //     displayName: "VENDOR_LR_GRC_ALL",
                        //     }
                        // this.props.updateHeaderRequest(mainHeaderPayload)
                        let payload = {
                            basedOn: "ALL",
                            module: "SHIPMENT TRACKING",
                            subModule: "LOGISTIC",
                            section: "LR-DELIVERED",
                            source: "WEB-APP",
                            typeConfig: "PORTAL",
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_GRC_ALL",
                            fixedHeaders: this.state.getMainFixedHeader,
                            defaultHeaders: this.state.getMainHeaderConfig,
                            customHeaders: this.state.getMainCustomHeader,
                        }
                        this.props.createMainHeaderConfigRequest(payload)
                    }
                }
                let displayName = this.state.pageName == "Ledger Report" ? !this.state.isSet ? "ENT_VENDOR_LEDGER" : "VEN_VENDOR_LEDGER"
                                :  this.state.pageName == "Outstanding Report" ? "ENT_OUTSTANDING"
                                :  this.state.pageName == "Sales Report" ? !this.state.isSet ? "ENT_SALES_ITEM_REPORT" : "VEN_SALES_ITEM_REPORT"
                                :  this.state.pageName == "Stock Report" ? !this.state.isSet ? "ENT_STOCK_ITEM_REPORT" : "VEN_STOCK_ITEM_REPORT"
                                :  this.state.pageName == "Orders Report" ? !this.state.isSet ? "ENT_ORDER_REPORT" : "VENDOR_ORDER_REPORT"
                                :  this.state.pageName == "Inspections Report" ? !this.state.isSet ? "ENT_INSPECTION_REPORT" : "VENDOR_INSPECTION_REPORT"
                                :  this.state.pageName == "Shipments Report" ? !this.state.isSet ? "ENT_SHIPMENT_REPORT" : "VENDOR_SHIPMENT_REPORT"
                                :  this.state.pageName == "Logistics Report" ? !this.state.isSet ? "ENT_LOGISTICS_REPORT" : "VENDOR_LOGISTICS_REPORT"
                                : ""
                if( displayName !== null && displayName !== undefined && displayName !== ""){
                    let payload = {
                        basedOn: "ALL",
                        module: "Analytics",
                        subModule: "Analytics",
                        section: "Analytics",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: displayName,
                        fixedHeaders: this.state.getMainFixedHeader,
                        defaultHeaders: this.state.getMainHeaderConfig,
                        customHeaders: this.state.getMainCustomHeader,
                    }
                    this.props.createMainHeaderConfigRequest(payload)
                }
            })
        }
        if( this.state.headerTabVal == 2 ){
            clean(this.state.setHeaderObjectData)
            this.setState({
                setHeaderObjectData: this.state.setHeaderObjectData
            },()=>{
            if(this.state.isSet == false){
                if(this.state.pageName == "Open PO"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PENDING-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PENDING_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Processed Orders"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PROCESSED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PROCESSED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "CANCELLED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_CANCELLED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Pending Inspection"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVAL",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Cancelled Inspection"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-CANCELLED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)    
                }else if(this.state.pageName == "Pending ASN Approval"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVAL",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-CANCELLED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "LR Processing"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-PROCESSING",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_PROCESSING_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-INTRANSIT",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_INTRANSIT_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Gate Entry"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-DELIVERED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_DELIVERED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-DELIVERED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_GRC_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
            }else{
                if(this.state.pageName == "Pending Orders"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PENDING-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PENDING_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Processed Orders"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PROCESSED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PROCESSED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "CANCELLED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_CANCELLED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "ASN Under Approval"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVAL",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVAL_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-CANCELLED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_CANCELLED_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "LR Processing"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-PROCESSING",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_PROCESSING_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-INTRANSIT",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Gate Entry"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-DELIVERED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_PROCESSING_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let payload = {
                        basedOn: "SET",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-DELIVERED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_GRC_SET",
                        fixedHeaders: this.state.getSetFixedHeader,
                        defaultHeaders: this.state.getSetHeaderConfig,
                        customHeaders: this.state.getSetCustomHeader,
                    }
        
                    this.props.createSetHeaderConfigRequest(payload)
                }
            }
            })
        }
        if( this.state.headerTabVal == 3 ){
            clean(this.state.itemHeaderObjectData)
            this.setState({
                itemHeaderObjectData: this.state.itemHeaderObjectData
            },()=>{
            if(this.state.isSet == false){
                if(this.state.pageName == "Open PO"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PENDING-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PENDING_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Processed Orders"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PROCESSED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PROCESSED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "CANCELLED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_CANCELLED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Pending Inspection"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVAL",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Cancelled Inspection"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-CANCELLED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)     
                }else if(this.state.pageName == "Pending ASN Approval"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVAL",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVAL_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_APPROVED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-CANCELLED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_ASN_CANCELLED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "LR Processing"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-PROCESSING",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_PROCESSING_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-INTRANSIT",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Gate Entry"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-DELIVERED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_DELIVERED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-DELIVERED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_GRC_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
            }else{
                if(this.state.pageName == "Pending Orders"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PENDING-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PENDING_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Processed Orders"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PROCESSED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PROCESSED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Cancelled Orders"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "CANCELLED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_CANCELLED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "ASN Under Approval"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVAL",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVAL_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Approved ASN"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-APPROVED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_APPROVED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Rejected ASN"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "SHIPMENT",
                        section: "ASN-CANCELLED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_ASN_CANCELLED_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "LR Processing"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-PROCESSING",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_PROCESSING_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Goods In-Transit"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-INTRANSIT",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Gate Entry"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-DELIVERED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_PROCESSING_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
                else if(this.state.pageName == "Grc Status"){
                    let payload = {
                        basedOn: "ITEM",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-DELIVERED",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_GRC_ITEM",
                        fixedHeaders: this.state.getItemFixedHeader,
                        defaultHeaders: this.state.getItemHeaderConfig,
                        customHeaders: this.state.getItemCustomHeader,
                    }
                    this.props.createItemHeaderConfigRequest(payload)
                }
            }
            })
        }
        if( this.state.headerTabVal == 4 ){
            clean(this.state.mainHeaderObjectData)
            this.setState({
                mainHeaderObjectData: this.state.mainHeaderObjectData
            },()=>{
            if(this.state.isSet == false){
                if(this.state.pageName == "Processed Orders"){
                    // let mainHeaderPayload = {
                    //     attributeType:"TABLE HEADER",
                    //     updatedFields:this.state.mainHeaderObjectData,
                    //     displayName: "ENT_PROCESSED_ASN",
                    // }
                    // this.props.updateHeaderRequest(mainHeaderPayload)
                    let payload = {
                        basedOn: "ASN",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PROCESSED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_PROCESSED_ASN",
                        fixedHeaders: this.state.getMainFixedHeader,
                        defaultHeaders: this.state.getMainHeaderConfig,
                        customHeaders: this.state.getMainCustomHeader,
                    }
                    this.props.createMainHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Goods In-Transit"){
                    // let mainHeaderPayload = {
                    //     attributeType:"TABLE HEADER",
                    //     updatedFields:this.state.mainHeaderObjectData,
                    //     displayName: "ENT_LR_INTRANSIT_ASN",
                    // }
                    // this.props.updateHeaderRequest(mainHeaderPayload)
                    let payload = {
                        basedOn: "ASN",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-INTRANSIT",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "ENT_LR_INTRANSIT_ASN",
                        fixedHeaders: this.state.getMainFixedHeader,
                        defaultHeaders: this.state.getMainHeaderConfig,
                        customHeaders: this.state.getMainCustomHeader,
                    }
                    this.props.createMainHeaderConfigRequest(payload)
                }
            }else{
                if(this.state.pageName == "Processed Orders"){
                    // let mainHeaderPayload = {
                    //     attributeType:"TABLE HEADER",
                    //     updatedFields:this.state.mainHeaderObjectData,
                    //     displayName: "VENDOR_PROCESSED_ASN",
                    // }
                    // this.props.updateHeaderRequest(mainHeaderPayload)
                    let payload = {
                        basedOn: "ASN",
                        module: "SHIPMENT TRACKING",
                        subModule: "ORDERS",
                        section: "PROCESSED-ORDERS",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_PROCESSED_ASN",
                        fixedHeaders: this.state.getMainFixedHeader,
                        defaultHeaders: this.state.getMainHeaderConfig,
                        customHeaders: this.state.getMainCustomHeader,
                    }
                    this.props.createMainHeaderConfigRequest(payload)
                }else if(this.state.pageName == "Goods In-Transit"){
                    // let mainHeaderPayload = {
                    //     attributeType:"TABLE HEADER",
                    //     updatedFields:this.state.mainHeaderObjectData,
                    //     displayName: "VENDOR_LR_INTRANSIT_ASN",
                    // }
                    // this.props.updateHeaderRequest(mainHeaderPayload)
                    let payload = {
                        basedOn: "ASN",
                        module: "SHIPMENT TRACKING",
                        subModule: "LOGISTIC",
                        section: "LR-INTRANSIT",
                        source: "WEB-APP",
                        typeConfig: "PORTAL",
                        attributeType: "TABLE HEADER",
                        displayName: "VENDOR_LR_INTRANSIT_ASN",
                        fixedHeaders: this.state.getMainFixedHeader,
                        defaultHeaders: this.state.getMainHeaderConfig,
                        customHeaders: this.state.getMainCustomHeader,
                    }
                    this.props.createMainHeaderConfigRequest(payload)
                }
            }
            })
        }
    }

    componentDidUpdate(previousProps, previousState) {
        if(this.props.updateHeader.updateHeaderData.isSuccess){
            if(this.state.headerTabVal == 1){
                if(this.state.isSet == false){
                    if(this.state.pageName == "Open PO"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PENDING_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Processed Orders"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PROCESSED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                    else if(this.state.pageName == "Cancelled Orders"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_CANCELLED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Pending Inspection"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PENDING_QC",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Pending ASN Approval"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVAL_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                    else if(this.state.pageName == "Approved ASN"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Rejected ASN"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_CANCELLED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "LR Processing"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_PROCESSING_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                    else if(this.state.pageName == "Goods In-Transit"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_INTRANSIT_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Gate Entry"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_DELIVERED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                    else if(this.state.pageName == "Grc Status"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_GRC_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                }else{
                    if(this.state.pageName == "Open PO"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PENDING_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Processed Orders"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PROCESSED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                    else if(this.state.pageName == "Cancelled Orders"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_CANCELLED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Pending Inspection"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PENDING_QC",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Pending ASN Approval"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVAL_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                    else if(this.state.pageName == "Approved ASN"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Rejected ASN"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_CANCELLED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "LR Processing"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_PROCESSING_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                    else if(this.state.pageName == "Goods In-Transit"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_INTRANSIT_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Gate Entry"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_DELIVERED_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                    else if(this.state.pageName == "Grc Status"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_GRC_ALL",
                            basedOn: "ALL"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                }
                let displayName = this.state.pageName == "Ledger Report" ? !this.state.isSet ? "ENT_VENDOR_LEDGER" : "VEN_VENDOR_LEDGER"
                            :  this.state.pageName == "Outstanding Report" ? "ENT_OUTSTANDING"
                            :  this.state.pageName == "Sales Report" ? !this.state.isSet ? "ENT_SALES_ITEM_REPORT" : "VEN_SALES_ITEM_REPORT"
                            :  this.state.pageName == "Stock Report" ? !this.state.isSet ? "ENT_STOCK_ITEM_REPORT" : "VEN_STOCK_ITEM_REPORT"
                            :  this.state.pageName == "Orders Report" ? !this.state.isSet ? "ENT_ORDER_REPORT" : "VENDOR_ORDER_REPORT"
                            :  this.state.pageName == "Inspections Report" ? !this.state.isSet ? "ENT_INSPECTION_REPORT" : "VENDOR_INSPECTION_REPORT"
                            :  this.state.pageName == "Shipments Report" ? !this.state.isSet ? "ENT_SHIPMENT_REPORT" : "VENDOR_SHIPMENT_REPORT"
                            :  this.state.pageName == "Logistics Report" ? !this.state.isSet ? "ENT_LOGISTICS_REPORT" : "VENDOR_LOGISTICS_REPORT"
                            : ""
                let mainHeaderPayload = {
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                    attributeType: "TABLE HEADER",
                    displayName: displayName,
                    basedOn: "ALL"
                }
                if( displayName !== null && displayName !== undefined && displayName !== "")
                this.props.getMainHeaderConfigRequest(mainHeaderPayload)

                for (var i=0; i< document.getElementsByClassName("modal_tableBox").length ;i++){
                    document.getElementsByClassName("modal_tableBox")[i].value = ""
                }
                this.setState({
                    mainHeaderObjectData:{},
                    loader: false,
                })
            } 
            if(this.state.headerTabVal == 2){
                if(this.state.isSet == false){
                    if(this.state.pageName == "Open PO"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PENDING_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Processed Orders"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PROCESSED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                    else if(this.state.pageName == "Cancelled Orders"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_CANCELLED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Pending Inspection"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVAL_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Pending ASN Approval"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVAL_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                    else if(this.state.pageName == "Approved ASN"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Rejected ASN"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_CANCELLED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "LR Processing"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_PROCESSING_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                    else if(this.state.pageName == "Goods In-Transit"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_INTRANSIT_ITEM",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Gate Entry"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_DELIVERED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                    else if(this.state.pageName == "Grc Status"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_GRC_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                }else{
                    if(this.state.pageName == "Open PO"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PENDING_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Processed Orders"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PROCESSED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                    else if(this.state.pageName == "Cancelled Orders"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_CANCELLED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Pending Inspection"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVAL_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Pending ASN Approval"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVAL_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                    else if(this.state.pageName == "Approved ASN"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Rejected ASN"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_CANCELLED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "LR Processing"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_PROCESSING_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                    else if(this.state.pageName == "Goods In-Transit"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_INTRANSIT_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }else if(this.state.pageName == "Gate Entry"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_DELIVERED_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                    else if(this.state.pageName == "Grc Status"){
                        let setHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_GRC_SET",
                            basedOn: "SET"
                        }
                        this.props.getSetHeaderConfigRequest(setHeaderPayload)
                    }
                }
                for (var i=0; i< document.getElementsByClassName("modal_tableBox").length ;i++){
                    document.getElementsByClassName("modal_tableBox")[i].value = ""
                }
                this.setState({
                    setHeaderObjectData:{},
                    loader: false,
                })
            } 
            if(this.state.headerTabVal == 3){
                if(this.state.isSet == false){
                    if(this.state.pageName == "Open PO"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PENDING_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Processed Orders"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PROCESSED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                    else if(this.state.pageName == "Cancelled Orders"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_CANCELLED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Pending Inspection"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVAL_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Pending ASN Approval"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVAL_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                    else if(this.state.pageName == "Approved ASN"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_APPROVED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Rejected ASN"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_ASN_CANCELLED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "LR Processing"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_PROCESSING_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                    else if(this.state.pageName == "Goods In-Transit"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_INTRANSIT_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Gate Entry"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_DELIVERED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                    else if(this.state.pageName == "Grc Status"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_GRC_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                }else{
                    if(this.state.pageName == "Open PO"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PENDING_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Processed Orders"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PROCESSED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                    else if(this.state.pageName == "Cancelled Orders"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_CANCELLED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Pending Inspection"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVAL_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Pending ASN Approval"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVAL_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                    else if(this.state.pageName == "Approved ASN"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_APPROVED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Rejected ASN"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_ASN_CANCELLED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "LR Processing"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_PROCESSING_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                    else if(this.state.pageName == "Goods In-Transit"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_INTRANSIT_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }else if(this.state.pageName == "Gate Entry"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_DELIVERED_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                    else if(this.state.pageName == "Grc Status"){
                        let itemHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_GRC_ITEM",
                            basedOn: "ITEM"
                        }
                        this.props.getItemHeaderConfigRequest(itemHeaderPayload)
                    }
                }
                for (var i=0; i< document.getElementsByClassName("modal_tableBox").length ;i++){
                    document.getElementsByClassName("modal_tableBox")[i].value = ""
                }
                this.setState({
                    itemHeaderObjectData:{},
                    loader: false,
                })
            }
            if(this.state.headerTabVal == 4){
                if(this.state.isSet == false){
                    if(this.state.pageName == "Processed Orders"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_PROCESSED_ASN",
                            basedOn: "ASN"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Goods In-Transit"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "ENT_LR_INTRANSIT_ASN",
                            basedOn: "ASN"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
                }else{
                    if(this.state.pageName == "Processed Orders"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_PROCESSED_ASN",
                            basedOn: "ASN"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }else if(this.state.pageName == "Goods In-Transit"){
                        let mainHeaderPayload = {
                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                            attributeType: "TABLE HEADER",
                            displayName: "VENDOR_LR_INTRANSIT_ASN",
                            basedOn: "ASN"
                        }
                        this.props.getMainHeaderConfigRequest(mainHeaderPayload)
                    }
    
                }
                for (var i=0; i< document.getElementsByClassName("modal_tableBox").length ;i++){
                    document.getElementsByClassName("modal_tableBox")[i].value = ""
                }
                this.setState({
                    mainHeaderObjectData:{},
                    loader: false,
                })
            }
        }
        
    }
    clearTableHeader = () =>{
        for (var i=0; i< document.getElementsByClassName("modal_tableBox").length ;i++){
            document.getElementsByClassName("modal_tableBox")[i].value = ""
        }
        this.setState({
            loader: false,
            mainHeaderObjectData:{},
            setHeaderObjectData:{},
            itemHeaderObjectData:{},
        })
    }

    enableDisableSaveButtonOnDocTab =()=>{
        if(this.documentFormatChild !== undefined && this.documentFormatChild.state != undefined && this.documentFormatChild.state.tab === "asn" 
           && !this.documentFormatChild.state.asnPrefixErr && !this.documentFormatChild.state.asnYearErr && !this.documentFormatChild.state.asnDocErr && this.documentFormatChild.state.asnTabSaveEnable){
               this.setState({ asnTabSaveEnable: true})
        }
        else if(this.documentFormatChild !== undefined && this.documentFormatChild.state != undefined && this.documentFormatChild.state.tab === "lr" 
           && !this.documentFormatChild.state.lrPrefixErr && !this.documentFormatChild.state.lrYearErr && !this.documentFormatChild.state.lrDocErr && this.documentFormatChild.state.lrTabSaveEnable){
               this.setState({ lrTabSaveEnable: true})
        }
        else if(this.documentFormatChild !== undefined && this.documentFormatChild.state != undefined && this.documentFormatChild.state.tab === "gateEntry" 
           && !this.documentFormatChild.state.gateEntryPrefixErr && !this.documentFormatChild.state.gateEntryYearErr && !this.documentFormatChild.state.gateEntryDocErr && this.documentFormatChild.state.gateEntryTabSaveEnable){
               this.setState({ gateEntryTabSaveEnable: true})
        }
        else if(this.documentFormatChild !== undefined && this.documentFormatChild.state != undefined && this.documentFormatChild.state.tab === "grc" 
           && !this.documentFormatChild.state.grcPrefixErr && !this.documentFormatChild.state.grcYearErr && !this.documentFormatChild.state.grcDocErr && this.documentFormatChild.state.grcTabSaveEnable){
               this.setState({ grcTabSaveEnable: true})
        }
        else{
            this.setState({
                asnTabSaveEnable: false,
                lrTabSaveEnable: false,
                gateEntryTabSaveEnable: false,
                grcTabSaveEnable: false
            })
        }   
    }

    enableDisableSaveButtonOnSettingTab =()=>{
        let settingConfigStack = [];
            settingConfigStack.push(this.state.settingConfigData);
        this.state.settingConfigData !== "" && settingConfigStack.map( data =>{
            if( data.asnApprovalPage !== (this.state.allowInvoiceApproval ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.inspectionRequired !== (this.state.allowInspectionRequired ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.invoiceUploadRequired !== (this.state.allowInvoiceUpload ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.isPartialPORequired !== (this.state.allowMultipleASN ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.isLRProcessingCancelASN !== (this.state.allowCancelASN ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }

            else if( data.vendorPOConfirmation !== (this.state.allowPOConfirmation ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.asnMask !== (this.state.allowAsnMask ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.asnFromToApplicablility !== (this.state.allowCreateAsnFromTo ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.grcVisible !== (this.state.allowConfirmGrc ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.cancelLinePendingQty !== (this.state.allowCancelLinePendingQty ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.isPackingRequired !== (this.state.allowPackingList ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }

            else if( data.vendorLedgerReport !== (this.state.allowLedgerReport ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.vendorSalesReport !== (this.state.allowSalesReport ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.vendorStockReport !== (this.state.allowStockReport ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.vendorOrderReport !== (this.state.allowOrderReport ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.vendorShipmentReport !== (this.state.allowShipmentReport ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.vendorLogisticsReport !== (this.state.allowLogisticReport ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.vendorInspectionReport !== (this.state.allowInspectionReport ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            } 
            else if( data.uploadInvoiceRequired !== (this.state.allowUploadInvoiceTAT ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.lrCreationRequired !== (this.state.allowLrCreationTAT ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.asnCreationFulfilmentRequired !== (this.state.allowNoOfPercentFulfil ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.reqcRequired !== (this.state.allowNoOfReInspection ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.asnBufferDaysRequired !== (this.state.allowAsnBufferDays ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.inspectionBufferDaysRequired !== (this.state.allowInspBufferDays ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( data.gateEntryVisible !== (this.state.allowGateEntry ? "TRUE" : "FALSE")){
                this.setState({ settingTabSaveEnable: true})
            }
            //Checked and unchecked setting::
            else if( this.state.invoiceApprovalCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnApprovalPage == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.inspectionRequiredCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.inspectionRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.invoiceUploadRequiredCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.invoiceUploadRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.multipleASNCreationCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.isPartialPORequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.asnCancellationACheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.isLRProcessingCancelASN == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.maskASNNoCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnMask == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.confirmGRCCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.grcVisible == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.cancelLineItemsCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.cancelLinePendingQty == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }

            else if( this.state.packingListCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.isPackingRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.poConfirmationCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorPOConfirmation == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.ledgerReportCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorLedgerReport == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.salesReportCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorSalesReport == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.stockReportCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorStockReport == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.orderReportCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorOrderReport == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }

            else if( this.state.shipmentReportCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorShipmentReport == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.logisticReportCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorLogisticsReport == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.inspectionReportCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.vendorInspectionReport == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.tatForInvoiceCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.uploadInvoiceRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.tatForLRCreationCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.lrCreationRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.noOfTimesReInspectionCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.reqcRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }

            else if( this.state.asnBufferCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnBufferDaysRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.inspectionBufferCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.inspectionBufferDaysRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.percentOfFulfilmentCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnCreationFulfilmentRequired == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.createASNCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.asnFromToApplicablility == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( this.state.gateEntryCheck !== (this.props.logistic.getButtonActiveConfig.data.resource.visibleKeys.gateEntryVisible == "TRUE" ? true : false)){
                this.setState({ settingTabSaveEnable: true})
            }
            //-----------------------------------------------------------------------------//

            else if( Number(data.asnFromToApplicablilityTransitDays) !== Number(this.state.createAsnFromToTransitDay == "" ? 0 : this.state.createAsnFromToTransitDay)){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( Number(data.asnFromToApplicablilityBufferDays) !== Number(this.state.createAsnFromToBufferDay == "" ? 0 : this.state.createAsnFromToBufferDay)){
                this.setState({ settingTabSaveEnable: true})
            }

            else if( Number(data.vendorPOConfirmationTAT) !== Number(this.state.poConfirmTAT == "" ? 0 : this.state.poConfirmTAT) ){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( Number(data.uploadInvoiceTAT) !== this.state.uploadInvoiceTAT ){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( Number(data.lrCreationTAT) !== this.state.lrCreationTAT ){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( Number(data.asnCreationFulfilment) !== Number(this.state.noOfPercentFulfil == "" ? 0 : this.state.noOfPercentFulfil)){
                this.setState({ settingTabSaveEnable: true})
            }

            else if( Number(data.reqcCount) !== this.state.noOfReInspection ){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( Number(data.asnBufferDays) !== this.state.asnBufferDays ){
                this.setState({ settingTabSaveEnable: true})
            }
            else if( Number(data.inspectionBufferdays) !== this.state.inspBufferDays ){
                this.setState({ settingTabSaveEnable: true})
            }
            else
               this.setState({ settingTabSaveEnable: false})
        })
    }

    onInspBufferDaysChange =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ inspBufferDays: numberPattern.test(e.target.value) ? e.target.value : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }

    onASNBufferDaysChange =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ asnBufferDays: numberPattern.test(e.target.value) ? e.target.value : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }

    onInspectionDaysChange =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ noOfReInspection: numberPattern.test(e.target.value) ? e.target.value : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }

    onChangePOConfirmTAT =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ poConfirmTAT: numberPattern.test(e.target.value) ? e.target.value : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }

    onUploadInvoiceDaysChange =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ uploadInvoiceTAT: numberPattern.test(e.target.value) ? e.target.value : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }
    
    onLrCreationDaysChange =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ lrCreationTAT: numberPattern.test(e.target.value) ? e.target.value : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }

    onPercentFulfilChange =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ noOfPercentFulfil: numberPattern.test(e.target.value) ? 
                                          ((e.target.value > 100) ? this.state.noOfPercentFulfil : e.target.value) : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }

    onChangeAsnFromToTransitDay =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ createAsnFromToTransitDay: numberPattern.test(e.target.value) ? e.target.value : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }

    onChangeAsnFromToBufferDay =(e)=>{
        let numberPattern = /^[0-9]+$/;
        this.setState({ createAsnFromToBufferDay: numberPattern.test(e.target.value) ? e.target.value : "" },()=>{
            this.enableDisableSaveButtonOnSettingTab()
        })
    }

    showMultipleAsnAlert =(e)=>{
        this.setState({
            toastMsg: "Access Denied. Please contact SupplyMint for more details.",
            toastLoader: true,
        })
        setTimeout(() => {
            this.setState({
                toastLoader: false
            })
        }, 7000)
    }

    //---------------------------------------------- Configuration Tab ------------------------//

    saveValueConfiguration =(e)=>{
        let payload = [];
        let tranporterName = this.state.otherTransportFlag ? this.state.otherTransporterValue : this.state.transporter
        let transporterCode = this.state.transporterCode

        let settingConfigStack = [];
            settingConfigStack.push(this.state.settingConfigData);
        this.state.settingConfigData !== "" && settingConfigStack.map( data =>{
            if( data.defaultTransporter !== (transporterCode+"|"+tranporterName)){
                payload.push({ key: "DEFAULT_TRANSPORTER",
                               value: (transporterCode+"|"+tranporterName)})
            }
        })
        if( payload.length > 0 ){
            this.props.invoiceApprovalBasedLRRequest(payload);
        }
    }

    openTransporterModal = (e, keyCode, pageNo) => {
        if( e.which == '13' || keyCode == 'Search' || keyCode == 'page'){
            this.setState({
                 transporterModal: false,
            }, () =>{
                let payload = {
                    no: pageNo == undefined || pageNo == null ? 1 : pageNo,
                    type: this.state.transporter == "" ? 1 : 3,
                    search: this.state.transporter,
                    slCode: "",
                    userType: "vendorlogi"
                }
                this.props.getAllVendorTransporterRequest(payload)
            })
        }     
    }

    onCloseTransporter =()=> {
        this.setState({
            transporterModal: false,
        }, ()=>document.removeEventListener("click", this.onClickCloseTransporter))
    }

    handleTransportarSearchText = (e) =>{
    //   if( e.currentTarget.id == 'other')
    //      this.setState({ otherTransporterValue: e.target.value, },()=>this.otherTransporter())
    //   else  
         this.setState({ transporter: e.target.value, transporterCode : "", otherTransportFlag: false},()=>this.transporter())
    }

    onSelectTransportar = (e, data) =>{
        // if( data.transporterName == 'Other')
        //    this.setState({ otherTransportFlag: true, transporter: data.transporterName, transporterCode: data.transporterCode,transporterModal : false, },()=>this.transporter())
        // else
           this.setState({ otherTransportFlag: false, transporterCode: data.transporterCode,
                            transporter: data.transporterName, transporterModal : false,
                            otherTransporterValue: ""},()=>this.transporter())  
                                                   
    }

    transporter =()=> {
        let flag = this.state.transporterCode == "" ? true : false;
        if (this.state.transporter == "" || flag) {
            this.setState({ transportererr: true },()=>this.enableSaveValueConfigButton())
        } else {
            this.setState({transportererr: false },()=>this.enableSaveValueConfigButton())
        }
    }
    // otherTransporter = () => {
    //     if( this.state.otherTransportFlag ){
    //         if (this.state.otherTransporterValue == "") {
    //             this.setState({ otherTransporterErr: true},()=>this.enableSaveValueConfigButton())
    //         } else {
    //             this.setState({otherTransporterErr: false},()=>this.enableSaveValueConfigButton())
    //         }
    //     }else {
    //             this.setState({otherTransporterErr: false},()=>this.enableSaveValueConfigButton())
    //     }
    // }

    enableSaveValueConfigButton =()=>{
         this.setState({ 
            enableSaveValueConfigButton: !this.state.transportererr && !this.state.otherTransporterErr ? true : false,
         })
         document.removeEventListener("click", this.onClickCloseTransporter)
    }

    onClickCloseTransporter =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop")){
            this.setState({
                transporterModal: false
            })
        }
    }

    handleCheck =(e)=>{
        if(e.target.id == "invoiceApprovalCheck")
           this.setState({ invoiceApprovalCheck: !this.state.invoiceApprovalCheck },()=>this.checkedSettingCount())
        if(e.target.id == "inspectionRequiredCheck")
           this.setState({ inspectionRequiredCheck: !this.state.inspectionRequiredCheck },()=>this.checkedSettingCount())
        if(e.target.id == "invoiceUploadRequiredCheck")
           this.setState({ invoiceUploadRequiredCheck: !this.state.invoiceUploadRequiredCheck },()=>this.checkedSettingCount())
        if(e.target.id == "multipleASNCreationCheck")
           this.setState({ multipleASNCreationCheck: !this.state.multipleASNCreationCheck },()=>this.checkedSettingCount())
        if(e.target.id == "asnCancellationACheck")
           this.setState({ asnCancellationACheck: !this.state.asnCancellationACheck },()=>this.checkedSettingCount())
        if(e.target.id == "maskASNNoCheck")
           this.setState({ maskASNNoCheck: !this.state.maskASNNoCheck },()=>this.checkedSettingCount())
        if(e.target.id == "confirmGRCCheck")
           this.setState({ confirmGRCCheck: !this.state.confirmGRCCheck },()=>this.checkedSettingCount())
        if(e.target.id == "cancelLineItemsCheck")
           this.setState({ cancelLineItemsCheck: !this.state.cancelLineItemsCheck },()=>this.checkedSettingCount())
        if(e.target.id == "packingListCheck")
           this.setState({ packingListCheck: !this.state.packingListCheck },()=>this.checkedSettingCount())
        if(e.target.id == "poConfirmationCheck")
           this.setState({ poConfirmationCheck: !this.state.poConfirmationCheck },()=>this.checkedSettingCount())

        if(e.target.id == "ledgerReportCheck")
           this.setState({ ledgerReportCheck: !this.state.ledgerReportCheck },()=>this.checkedSettingCount())
        if(e.target.id == "salesReportCheck")
           this.setState({ salesReportCheck: !this.state.salesReportCheck },()=>this.checkedSettingCount())
        if(e.target.id == "stockReportCheck")
           this.setState({ stockReportCheck: !this.state.stockReportCheck },()=>this.checkedSettingCount())
        if(e.target.id == "orderReportCheck")
           this.setState({ orderReportCheck: !this.state.orderReportCheck },()=>this.checkedSettingCount())
        if(e.target.id == "shipmentReportCheck")
           this.setState({ shipmentReportCheck: !this.state.shipmentReportCheck },()=>this.checkedSettingCount())
        if(e.target.id == "logisticReportCheck")
           this.setState({ logisticReportCheck: !this.state.logisticReportCheck },()=>this.checkedSettingCount())

        if(e.target.id == "inspectionReportCheck")
           this.setState({ inspectionReportCheck: !this.state.inspectionReportCheck },()=>this.checkedSettingCount())
        if(e.target.id == "tatForInvoiceCheck")
           this.setState({ tatForInvoiceCheck: !this.state.tatForInvoiceCheck },()=>this.checkedSettingCount())
        if(e.target.id == "tatForLRCreationCheck")
           this.setState({ tatForLRCreationCheck: !this.state.tatForLRCreationCheck },()=>this.checkedSettingCount())
        if(e.target.id == "noOfTimesReInspectionCheck")
           this.setState({ noOfTimesReInspectionCheck: !this.state.noOfTimesReInspectionCheck },()=>this.checkedSettingCount())
        if(e.target.id == "asnBufferCheck")
           this.setState({ asnBufferCheck: !this.state.asnBufferCheck },()=>this.checkedSettingCount())
        if(e.target.id == "inspectionBufferCheck")
           this.setState({ inspectionBufferCheck: !this.state.inspectionBufferCheck },()=>this.checkedSettingCount())
        if(e.target.id == "percentOfFulfilmentCheck")
           this.setState({ percentOfFulfilmentCheck: !this.state.percentOfFulfilmentCheck },()=>this.checkedSettingCount())
        if(e.target.id == "createASNCheck")
           this.setState({ createASNCheck: !this.state.createASNCheck },()=>this.checkedSettingCount())
        if(e.target.id == "gateEntryCheck")
           this.setState({ gateEntryCheck: !this.state.gateEntryCheck },()=>this.checkedSettingCount())

        if(e.target.id == "processSettingAllCheck"){
            this.setState({ processSettingAllCheck: !this.state.processSettingAllCheck},()=>{
                if(this.state.processSettingAllCheck){
                    this.setState({
                        invoiceApprovalCheck: true,
                        inspectionRequiredCheck: true,
                        invoiceUploadRequiredCheck: true, 
                        multipleASNCreationCheck: true,
                        asnCancellationACheck: true,
                        maskASNNoCheck: true, 
                        confirmGRCCheck: true, 
                        cancelLineItemsCheck: true, 
                        packingListCheck: true, 
                        poConfirmationCheck: true,
                        gateEntryCheck: true,
                    },()=>this.checkedSettingCount())
                }else{
                    this.setState({
                        invoiceApprovalCheck: false,
                        inspectionRequiredCheck: false,
                        invoiceUploadRequiredCheck: false, 
                        multipleASNCreationCheck: false,
                        asnCancellationACheck: false,
                        maskASNNoCheck: false, 
                        confirmGRCCheck: false, 
                        cancelLineItemsCheck: false, 
                        packingListCheck: false, 
                        poConfirmationCheck: false,
                        gateEntryCheck: false,
                    },()=>this.checkedSettingCount())
                }
            }) 
        }   

        if(e.target.id == "reportSettingAllCheck"){
            this.setState({reportSettingAllCheck: !this.state.reportSettingAllCheck},()=>{
                if(this.state.reportSettingAllCheck){
                   this.setState({
                        ledgerReportCheck: true,
                        salesReportCheck: true,
                        stockReportCheck: true,
                        orderReportCheck: true,
                        shipmentReportCheck: true,
                        logisticReportCheck: true,
                        inspectionReportCheck: true,
                   },()=>this.checkedSettingCount()) 
                }else{
                    this.setState({
                        ledgerReportCheck: false,
                        salesReportCheck: false,
                        stockReportCheck: false,
                        orderReportCheck: false,
                        shipmentReportCheck: false,
                        logisticReportCheck: false,
                        inspectionReportCheck: false,
                   },()=>this.checkedSettingCount()) 
                }
            })
        }
        if(e.target.id == "processWiseTatSettingAllCheck"){
            this.setState({ processWiseTatSettingAllCheck: !this.state.processWiseTatSettingAllCheck},()=>{
                if(this.state.processWiseTatSettingAllCheck){
                    this.setState({
                        tatForInvoiceCheck: true,
                        tatForLRCreationCheck: true,
                        noOfTimesReInspectionCheck: true, 
                        asnBufferCheck: true, 
                        inspectionBufferCheck: true, 
                        percentOfFulfilmentCheck: true, 
                        createASNCheck: true,
                    })
                }else{
                    this.setState({
                        tatForInvoiceCheck: false,
                        tatForLRCreationCheck: false,
                        noOfTimesReInspectionCheck: false, 
                        asnBufferCheck: false, 
                        inspectionBufferCheck: false, 
                        percentOfFulfilmentCheck: false, 
                        createASNCheck: false,
                    }) 
                }
            })
        }
    }

    checkedSettingCount =(e)=>{
        let totalCheckedSettingCount = 0;
        if(this.state.invoiceApprovalCheck)
            totalCheckedSettingCount += 1;
        if(this.state.inspectionRequiredCheck)
            totalCheckedSettingCount += 1;
        if(this.state.invoiceUploadRequiredCheck)
            totalCheckedSettingCount += 1;
        if(this.state.multipleASNCreationCheck)
            totalCheckedSettingCount += 1;   
        if(this.state.asnCancellationACheck)
            totalCheckedSettingCount += 1;
        if(this.state.maskASNNoCheck)
            totalCheckedSettingCount += 1;
        if(this.state.confirmGRCCheck)
            totalCheckedSettingCount += 1;
        if(this.state.cancelLineItemsCheck)
            totalCheckedSettingCount += 1;

        if(this.state.packingListCheck)
            totalCheckedSettingCount += 1;
        if(this.state.poConfirmationCheck)
            totalCheckedSettingCount += 1;
        if(this.state.ledgerReportCheck)
            totalCheckedSettingCount += 1;
        if(this.state.salesReportCheck)
            totalCheckedSettingCount += 1;   
        if(this.state.stockReportCheck)
            totalCheckedSettingCount += 1;
        if(this.state.orderReportCheck)
            totalCheckedSettingCount += 1;
        if(this.state.shipmentReportCheck)
            totalCheckedSettingCount += 1;

        if(this.state.logisticReportCheck)
            totalCheckedSettingCount += 1;
        if(this.state.inspectionReportCheck)
            totalCheckedSettingCount += 1;
        if(this.state.tatForInvoiceCheck)
            totalCheckedSettingCount += 1;
        if(this.state.tatForLRCreationCheck)
            totalCheckedSettingCount += 1;   
        if(this.state.noOfTimesReInspectionCheck)
            totalCheckedSettingCount += 1;
        if(this.state.asnBufferCheck)
            totalCheckedSettingCount += 1;
        if(this.state.inspectionBufferCheck)
            totalCheckedSettingCount += 1;
        if(this.state.percentOfFulfilmentCheck)
            totalCheckedSettingCount += 1;
        if(this.state.createASNCheck)
            totalCheckedSettingCount += 1;

        if(this.state.gateEntryCheck)
            totalCheckedSettingCount += 1;    
        
        this.setState({ totalCheckedSetting: totalCheckedSettingCount},()=>this.enableDisableSaveButtonOnSettingTab()) 

        this.processSettingAllCheckCount();
        this.reportSettingAllCheckCount();
        this.processWiseTatSettingAllCheckCount();
    }

    processSettingAllCheckCount =(e)=>{
        if(this.state.invoiceApprovalCheck && this.state.inspectionRequiredCheck && this.state.invoiceUploadRequiredCheck
            && this.state.multipleASNCreationCheck && this.state.asnCancellationACheck && this.state.maskASNNoCheck
            && this.state.confirmGRCCheck && this.state.cancelLineItemsCheck && this.state.packingListCheck && this.state.poConfirmationCheck 
            && this.state.gateEntryCheck)
        {
            this.setState({ processSettingAllCheck: true})
        }else{
            this.setState({ processSettingAllCheck: false})
        }    
    }

    reportSettingAllCheckCount =(e)=>{
        if(this.state.ledgerReportCheck && this.state.salesReportCheck && this.state.stockReportCheck
            && this.state.orderReportCheck && this.state.shipmentReportCheck && this.state.logisticReportCheck
            && this.state.inspectionReportCheck)
        {
            this.setState({ reportSettingAllCheck: true})
        }else{
            this.setState({ reportSettingAllCheck: false})
        }
    }

    processWiseTatSettingAllCheckCount =(e)=>{
        if(this.state.tatForInvoiceCheck && this.state.tatForLRCreationCheck && this.state.noOfTimesReInspectionCheck
            && this.state.asnBufferCheck && this.state.inspectionBufferCheck && this.state.percentOfFulfilmentCheck
            && this.state.createASNCheck)
        {
            this.setState({ processWiseTatSettingAllCheck: true})
        }else{
            this.setState({ processWiseTatSettingAllCheck: false})
        }
    }

    render() {
        const{invoiceApprovalCheck,inspectionRequiredCheck,invoiceUploadRequiredCheck,multipleASNCreationCheck,
            asnCancellationACheck,maskASNNoCheck,confirmGRCCheck,cancelLineItemsCheck,packingListCheck,poConfirmationCheck,
            ledgerReportCheck,salesReportCheck,stockReportCheck,orderReportCheck,shipmentReportCheck,logisticReportCheck,
            inspectionReportCheck,tatForInvoiceCheck,tatForLRCreationCheck,noOfTimesReInspectionCheck, asnBufferCheck,
            inspectionBufferCheck, percentOfFulfilmentCheck,createASNCheck,gateEntryCheck} = this.state;
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="subscription-tab procurement-setting-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                            <li className="nav-item active">
                                <a className="nav-link st-btn p-l-0" href="#numberformats" role="tab" data-toggle="tab" onClick={this.changeTabCS}>Document Formats</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#lrcreationapproval" role="tab" data-toggle="tab" onClick={this.changeTabCS}>Settings</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#tableHeaders" role="tab" data-toggle="tab" onClick={this.changeTabCS}>Table Headers</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#valueConfiguration" role="tab" data-toggle="tab" onClick={this.changeTabCS}>Configuration</a>
                            </li>
                            <li className="pst-button">
                                <div className="pstb-inner">
                                  {/* <button type="button" className={this.state.asnSave ? "pst-save" : "pst-save btnDisabled"} onClick={(e) => this.state.asnSave ? this.saveAsn(e) : null} >Save</button> */}
                                 {this.state.tab === "tableHeaders" && <button type="button" className=
                                {this.state.tab == "tableHeaders" ?
                                this.state.headerTabVal == 1 &&  Object.keys(this.state.mainHeaderObjectData).length > 0 ? "pst-save":
                                this.state.headerTabVal == 2 &&  Object.keys(this.state.setHeaderObjectData).length > 0 ? "pst-save" :
                                this.state.headerTabVal == 3 &&  Object.keys(this.state.itemHeaderObjectData).length > 0 ? "pst-save":
                                this.state.headerTabVal == 4 &&  Object.keys(this.state.mainHeaderObjectData).length > 0 ? "pst-save" :"pst-save btnDisabled":"pst-save btnDisabled"} onClick={this.saveAllConfiguration} >Save</button>}

                                { this.state.tab === "documentFormat" && (this.state.asnTabSaveEnable ? <button type="button" className="pst-save" onClick={this.saveAllConfiguration} >Save</button>
                                  : this.state.lrTabSaveEnable ? <button type="button" className="pst-save" onClick={this.saveAllConfiguration} >Save</button>
                                  : this.state.gateEntryTabSaveEnable ? <button type="button" className="pst-save" onClick={this.saveAllConfiguration} >Save</button>
                                  : this.state.grcTabSaveEnable ? <button type="button" className="pst-save" onClick={this.saveAllConfiguration} >Save</button>
                                  : <button type="button" className="pst-save btnDisabled">Save</button> )} 
                                {this.state.tab === "settings" && (this.state.settingTabSaveEnable ? <button type="button" className="pst-save" onClick={this.saveAllConfiguration} >Save</button>
                                  : <button type="button" className="pst-save btnDisabled">Save</button> )}
                                 {this.state.tab === "valueConfiguration" && (this.state.enableSaveValueConfigButton ? <button type="button" className="pst-save" onClick={this.saveValueConfiguration} >Save</button>
                                  : <button type="button" className="pst-save btnDisabled">Save</button> )}  
                                  <button type="button" onClick={this.clearAllConfiguration}>Cancel</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="numberformats" role="tabpanel">
                            {/* <Configuration handleAsnChange={this.handleAsnChange} handleShipmentTrackingMenu={this.handleShipmentTrackingMenu} {...this.props} {...this.state} /> */}
                            {this.state.documentFormatData !== "" && <DocumentFormats {...this.props} {...this.state} enableDisableSaveButtonOnDocTab={this.enableDisableSaveButtonOnDocTab} onRef={ref => (this.documentFormatChild = ref)}/>}
                        </div>
                        <div className="tab-pane fade" id="lrcreationapproval" role="tabpanel">
                            <div className="new-gen-toggle cal-set-height p-lr-47">
                                <div className="choose-setting-list m-top-30">
                                    <h3>Choose Setting List</h3>
                                    <div className="inputTextKeyFucMain">
                                        <button type="button" className="csl-btn" onClick={(e) => this.openSettingList(e)}>{this.state.totalCheckedSetting} Setting selected</button>
                                        {this.state.settingList && <SettingList {...this.state} {...this.props} CloseSettingList={this.CloseSettingList} handleCheck={this.handleCheck}/>}
                                    </div>
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30 mng-child">
                                    <h3 className="digivend-setting-head">Process Setting</h3>
                                    {invoiceApprovalCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3> Invoice Approval Before LR Creation</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allow" type="radio" checked={this.state.allowInvoiceApproval} name="approval" onClick={this.invoiceApprovalBasedLR}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disallow" type="radio" checked={this.state.disAllowInvoiceApproval} name="disapproval" onClick={this.invoiceApprovalBasedLR}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want an option to approve an invoice before LR creation by vendor or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>An option for Invoice approval will appear on your panel under ‘LR Processing’, along with two tabs for ‘Pending Approval’ and ‘Approved’. Once the invoice is approved by you, only then the vendor will be able to create an LR for the same.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>The vendor will be able to create an LR after uploading the invoice of an order, without having to wait for your approval on the invoice.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {inspectionRequiredCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Conduct Inspection</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowInspRequired" type="radio"  name="inspectionRequired" checked={this.state.allowInspectionRequired} onClick={this.inspectionRequired}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowInspRequired" type="radio" name="inspectionNotRequired" checked={this.state.disAllowInspectionRequired} onClick={this.inspectionRequired}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to conduct inspections for your orders or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>The vendor will be bound to create an inspection for all your orders. Once you update the status of an inspection under ‘Pending Inspections’, only then the order will move forward for ASN approval.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>The vendor will directly create an ASN which will appear straight under ‘ASN under approval’. No inspection will take place in this case.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {invoiceUploadRequiredCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Invoice Attachment Mandatory</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowInvoiceUpload" type="radio"  name="invoiceUpload" checked={this.state.allowInvoiceUpload} onClick={this.invoiceUpload}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowInvoiceUpload" type="radio" name="invoiceNotUpload" checked={this.state.disAllowInvoiceUpload} onClick={this.invoiceUpload}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want a vendor to mandatorily attach a file for invoice at the time of invoice upload or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will not be able to upload an invoice unless they attach a file for the same.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will have the option to upload an invoice, with or without attaching a file for the same.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {multipleASNCreationCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Multiple Inspection/ASN Creation</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowMultipleASN" type="radio"  name="multiplasn" checked={this.state.allowMultipleASN} onClick={this.multipleASN}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowMultipleASN" type="radio" name="multipleasn" checked={this.state.disAllowMultipleASN} onClick={this.multipleASN}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to allow a vendor to create multiple inspections/ASNs for a single order or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the option to create multiple inspections/ASNs for a single order. This means they will be permitted to deliver orders partially.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will be bound to create a single inspection/ASN for a single order.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30">
                                    {asnCancellationACheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>ASN Cancellation after Approval for Vendor</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowCancelASN" type="radio"  name="cancelasn" checked={this.state.allowCancelASN} onClick={this.cancelASN}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowCancelASN" type="radio" name="cancelasn" checked={this.state.disAllowCancelASN} onClick={this.cancelASN}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to allow a vendor to cancel ASNs after your approval for the same or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the option to cancel an ASN after your approval on the same.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will not be able to cancel an ASN after your approval for the same.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {maskASNNoCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Mask ASN No. for Vendor until ASN Approval</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowAsnMask" type="radio"  name="allowAsnMask" checked={this.state.allowAsnMask} onClick={this.asnMask}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowAsnMask" type="radio" name="disAllowAsnMask" checked={this.state.disAllowAsnMask} onClick={this.asnMask}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to hide ASN numbers from vendors unless the ASN is approved by you or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>The ASN number will stay hidden from a vendor unless the ASN is approved by you.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>The ASN number will be visible to the vendor even before you approve an ASN.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {confirmGRCCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>GRC Confirmation</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowConfirmGrc" type="radio"  name="allowConfirmGrc" checked={this.state.allowConfirmGrc} onClick={this.confirmGrc}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowConfirmGrc" type="radio" name="disAllowConfirmGrc" checked={this.state.disAllowConfirmGrc} onClick={this.confirmGrc}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want an option to send GRC confirmation to vendors or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>You will have the option to confirm GRC to vendors under ‘Gate Entry’.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>You will not have any option to confirm GRC to vendors.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {cancelLineItemsCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Cancel Line Items</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowCancelLinePendingQty" type="radio"  name="allowCancelLinePendingQty" checked={this.state.allowCancelLinePendingQty} onClick={this.cancelLinePendingQty}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowCancelLinePendingQty" type="radio" name="disAllowCancelLinePendingQty" checked={this.state.disAllowCancelLinePendingQty} onClick={this.cancelLinePendingQty}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to allow vendors to cancel some line items in a multiple set order or not. The impact on single and multiple set orders will also depend on your setting for ‘multiple ASN creation’.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will be able to create inspections/ASNs with zero request quantity of one or more single sets in a multiple set order.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will have to fulfil the quantity criteria for all single sets in a multiple set order.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30">
                                    {packingListCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Upload Packing List during Inspection/ASN Creation</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowPackingList" type="radio"  name="allowPackingList" checked={this.state.allowPackingList} onClick={this.packingListRequired}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowPackingList" type="radio" name="disAllowPackingList" checked={this.state.disAllowPackingList} onClick={this.packingListRequired}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to give the vendor an option to upload the packing list at the time of ASN creation or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>An option for uploading packing list will appear for vendors at the time of Inspection/ASN creation.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>No option for uploading packing list will appear for vendors at the time of Inspection/ASN creation.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {poConfirmationCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>PO Confirmation by Vendor</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowPOConfirmation" type="radio"  name="allowPOConfirmation" checked={this.state.allowPOConfirmation} onClick={this.poConfirmation}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowPOConfirmation" type="radio" name="disAllowPOConfirmation" checked={this.state.disAllowPOConfirmation} onClick={this.poConfirmation}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                                {this.state.allowPOConfirmation && <div className="gpfd-create">
                                                    <label className="gpfd-lbl">TAT</label>
                                                    <input type="text" value={this.state.poConfirmTAT} onChange={this.onChangePOConfirmTAT} placeholder={this.state.poConfirmTAT == "" ? 0 : this.state.poConfirmTAT}/>
                                                    <label>Days</label>
                                                </div>}
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want vendors to have the option to approve POs to indicate the confirmation of orders.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span> Vendors will have the option to 'Approve PO' under ‘Pending Orders’, along with two tabs for ‘Awaiting PO’ and ‘Confirmed PO’. You’ll also have to set a turn-around time for the vendor to approve a PO after which the order will be withdrawn.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will be bound to create inspections for all orders. They will not have the option to approve PO to indicate confirmation of order.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {gateEntryCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Gate Entry Delivery Confirmation</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowGateEntry" type="radio"  name="allowGateEntry" checked={this.state.allowGateEntry} onClick={this.isGateEntry}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowGateEntry" type="radio" name="disAllowGateEntry" checked={this.state.disAllowGateEntry} onClick={this.isGateEntry}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want an option to send gate entry delivery confirmation to vendors or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span> You will have the option to confirm gate entry delivery of logistics under ‘Good In-Transit’.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>You will not have any option to confirm gate entry delivery of logistics to vendors.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30 mng-child">
                                    <h3 className="digivend-setting-head">Reports Setting for Vendors</h3>
                                    {ledgerReportCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Ledger Report Access to Vendor</h3>
                                            <div className="gpf-radio-list m-top-15">
                                                <label className="gen-radio-btn">
                                                    <input id="allowLedgerReport" type="radio"  name="allowLedgerReport" checked={this.state.allowLedgerReport} onClick={this.ledgerReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowLedgerReport" type="radio" name="disAllowLedgerReport" checked={this.state.disAllowLedgerReport} onClick={this.ledgerReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to give access to vendors to generate Ledger Reports or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the access to generate ledger reports.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will not be able to access any data under 'Ledger Report'.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {salesReportCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Sales Report Access to Vendor</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowSalesReport" type="radio"  name="allowSalesReport" checked={this.state.allowSalesReport} onClick={this.salesReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowSalesReport" type="radio" name="disAllowSalesReport" checked={this.state.disAllowSalesReport} onClick={this.salesReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to give access to vendors to generate Sales Reports or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the access to generate sales reports.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will not be able to access any data under 'Sales Report'.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {stockReportCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Stock Report Access to Vendor</h3>
                                            <ul className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowStockReport" type="radio"  name="allowStockReport" checked={this.state.allowStockReport} onClick={this.stockReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowStockReport" type="radio" name="disAllowStockReport" checked={this.state.disAllowStockReport} onClick={this.stockReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </ul>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to give access to vendors to generate Stock Reports or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the access to generate stocks reports.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will not be able to access any data under 'Stocks Report'.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {orderReportCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Order Report Access to Vendor</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowOrderReport" type="radio"  name="allowOrderReport" checked={this.state.allowOrderReport} onClick={this.orderReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowOrderReport" type="radio" name="disAllowOrderReport" checked={this.state.disAllowOrderReport} onClick={this.orderReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to give access to vendors to generate Order Reports or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the access to generate orders reports.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will not be able to access any data under 'Orders Report'.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30">
                                    {shipmentReportCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Shipment Report Access to Vendor</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowShipmentReport" type="radio"  name="allowShipmentReport" checked={this.state.allowShipmentReport} onClick={this.shipmentReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowShipmentReport" type="radio" name="disAllowShipmentReport" checked={this.state.disAllowShipmentReport} onClick={this.shipmentReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to give access to vendors to generate Shipment Reports or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the access to generate shipments reports.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will not be able to access any data under 'Shipments Report'.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {logisticReportCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Logistic Report Access to Vendor</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowLogisticReport" type="radio"  name="allowLogisticReport" checked={this.state.allowLogisticReport} onClick={this.logisticReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowLogisticReport" type="radio" name="disAllowLogisticReport" checked={this.state.disAllowLogisticReport} onClick={this.logisticReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to give access to vendors to generate Logistic Reports or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the access to generate logistics reports.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will not be able to access any data under 'Logistics Report'.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {inspectionReportCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Inspection Report Access to Vendor</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowInspectionReport" type="radio"  name="allowInspectionReport" checked={this.state.allowInspectionReport} onClick={this.inspectionReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowInspectionReport" type="radio" name="disAllowInspectionReport" checked={this.state.disAllowInspectionReport} onClick={this.inspectionReport}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to give access to vendors to generate Inspection Reports or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have the access to generate inspections reports.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will not be able to access any data under 'Inspections Report'.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30 mng-child">
                                    <h3 className="digivend-setting-head">Process wise TAT setting</h3>
                                    {tatForInvoiceCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>TAT for Invoice upload after ASN Approval</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowUploadInvoiceTAT" type="radio"  name="allowUploadInvoiceTAT" checked={this.state.allowUploadInvoiceTAT} onClick={this.isUploadInvoiceTAT}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowUploadInvoiceTAT" type="radio" name="disAllowUploadInvoiceTAT" checked={this.state.disAllowUploadInvoiceTAT} onClick={this.isUploadInvoiceTAT}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                                {this.state.allowUploadInvoiceTAT && <div className="gpfd-create">
                                                    <input type="text" value={this.state.uploadInvoiceTAT} onChange={this.onUploadInvoiceDaysChange} placeholder={this.state.uploadInvoiceTAT == "" ? 0 : this.state.uploadInvoiceTAT}/>
                                                    <label>Days</label>
                                                </div>}
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to set a turn-around time for vendors to upload invoices after your approval of ASN or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have to upload invoices of orders within a specified turn-around time after your approval of ASN.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will have the option to upload invoices any time after your approval of ASN.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {tatForLRCreationCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>TAT for LR Creation after Invoice Approval</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowLrCreationTAT" type="radio"  name="allowLrCreationTAT" checked={this.state.allowLrCreationTAT} onClick={this.isLrCreationTAT}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowLrCreationTAT" type="radio" name="disAllowLrCreationTAT" checked={this.state.disAllowLrCreationTAT} onClick={this.isLrCreationTAT}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                                {this.state.allowLrCreationTAT && <div className="gpfd-create">
                                                    <input type="text" value={this.state.lrCreationTAT} onChange={this.onLrCreationDaysChange} placeholder={this.state.lrCreationTAT == "" ? 0 : this.state.lrCreationTAT}/>
                                                    <label>Days</label>
                                                </div>}
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to set a turn-around time for vendors to create LRs after your approval of invoice or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will have to create LRs within a specified turn-around time after your approval of invoice.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will have the option to create LRs any time after your approval of invoice.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {noOfTimesReInspectionCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Restrict No. of Re-Inspection</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowNoOfReInspection" type="radio"  name="allowNoOfReInspection" checked={this.state.allowNoOfReInspection} onClick={this.isNoOfReInspection}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowNoOfReInspection" type="radio" name="disAllowNoOfReInspection" checked={this.state.disAllowNoOfReInspection} onClick={this.isNoOfReInspection}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                                {this.state.allowNoOfReInspection && <div className="gpfd-create">
                                                    <input type="text" value={this.state.noOfReInspection} onChange={this.onInspectionDaysChange} placeholder={this.state.noOfReInspection == "" ? 0 : this.state.noOfReInspection}/>
                                                    <label>Days</label>
                                                </div>}
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to restrict the number of times you can re-inspect an ASN or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>The number of times you can re-inspect an ASN will be restricted. After the limit is reached, the ASN will not be permitted for any further re-inspection. You can only complete the inspection with a pass, fail, abortive, put on hold, to be done later, or to be done at warehouse status.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>An ASN will be allowed to have infinite number of re-inspections unless you complete the inspection with a pass, fail, abortive, put on hold, to be done later, or to be done at warehouse status.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {asnBufferCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Buffer Days for Invoice Upload & LR Creation</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowAsnBufferDays" type="radio"  name="allowAsnBufferDays" checked={this.state.allowAsnBufferDays} onClick={this.isAsnBufferDays}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowAsnBufferDays" type="radio" name="disAllowAsnBufferDays" checked={this.state.disAllowAsnBufferDays} onClick={this.isAsnBufferDays}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                                {this.state.allowAsnBufferDays && <div className="gpfd-create">
                                                    <input type="text" value={this.state.asnBufferDays} onChange={this.onASNBufferDaysChange} placeholder={this.state.asnBufferDays == "" ? 0 : this.state.asnBufferDays}/>
                                                    <label>Days</label>
                                                </div>}
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to allow vendors to delay processes like invoice upload and LR creation till after the ASN expires or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>Vendors will be permitted to delay the processes of invoice upload and  LR creation till the number of days you have selected as buffer days.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will have to meet ASN deadlines. If the ASN expires, vendors will not be permitted to upload an invoice or create an LR.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30">
                                    {inspectionBufferCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Inspection Buffer Days</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowInspBufferDays" type="radio"  name="allowInspBufferDays" checked={this.state.allowInspBufferDays} onClick={this.isInspBufferDays}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowInspBufferDays" type="radio" name="disAllowInspBufferDays" checked={this.state.disAllowInspBufferDays} onClick={this.isInspBufferDays}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                                {this.state.allowInspBufferDays && <div className="gpfd-create">
                                                    <input type="text" value={this.state.inspBufferDays} onChange={this.onInspBufferDaysChange} placeholder={this.state.inspBufferDays == "" ? 0 : this.state.inspBufferDays}/>
                                                    <label>Days</label>
                                                </div>}
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to add buffer days for conducting inspections or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>You will have more days to choose from while selecting the dates for conducting inspections. You will have to select the number of buffer days you want to add to the ‘inspection to’ date set by vendors.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>You will have to conduct inspections between the dates set for inspections by vendors.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {percentOfFulfilmentCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Percentage Criteria for Inspection/ASN Creation</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowNoOfPercentFulfil" type="radio"  name="allowNoOfPercentFulfil" checked={this.state.allowNoOfPercentFulfil} onClick={this.isNoOfPercentFulfil}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowNoOfPercentFulfil" type="radio" name="disAllowNoOfPercentFulfil" checked={this.state.disAllowNoOfPercentFulfil} onClick={this.isNoOfPercentFulfil}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                                {this.state.allowNoOfPercentFulfil && <div className="gpfd-create">
                                                    <input type="text" value={this.state.noOfPercentFulfil} onChange={this.onPercentFulfilChange} placeholder={this.state.noOfPercentFulfil == "" ? 0 : this.state.noOfPercentFulfil}/>
                                                    <label>%</label>
                                                </div>}
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to set a percentage criteria for the quantity with which a vendor can create an inspection/ASN or not.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>The quantity with which vendors creates an inspection/ASN will have to meet the percentage criteria fixed by you.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>Vendors will be able to create inspections/ASNs with any amount of quantity.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {createASNCheck && <div className="col-lg-3 pad-lft-0">
                                        <div className="gpf-dbs">
                                            <h3>Create ASN Validity</h3>
                                            <div className="gpf-radio-list">
                                                <label className="gen-radio-btn">
                                                    <input id="allowCreateAsnFromTo" type="radio"  name="allowCreateAsnFromTo" checked={this.state.allowCreateAsnFromTo} onClick={this.createAsnFromTo}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Allow</span>
                                                </label>
                                                <label className="gen-radio-btn">
                                                    <input id="disAllowCreateAsnFromTo" type="radio" name="disAllowCreateAsnFromTo" checked={this.state.disAllowCreateAsnFromTo} onClick={this.createAsnFromTo}/>
                                                    <span className="drfb-check"></span>
                                                    <span className="drfb-text">Disallow</span>
                                                </label>
                                                {this.state.allowCreateAsnFromTo && <div className="gpfd-create">
                                                    <label className="gpfd-lbl">Transit</label>
                                                    <input type="text" className="width160" value={this.state.createAsnFromToTransitDay} onChange={this.onChangeAsnFromToTransitDay} placeholder={this.state.createAsnFromToTransitDay == "" ? 0 : this.state.createAsnFromToTransitDay}/>
                                                    <label>Days</label>
                                                    <div className="m-top-10">
                                                        <label className="gpfd-lbl">Buffer</label>
                                                        <input type="text" className="width160" value={this.state.createAsnFromToBufferDay} onChange={this.onChangeAsnFromToBufferDay} placeholder={this.state.createAsnFromToBufferDay == "" ? 0 : this.state.createAsnFromToBufferDay}/>
                                                        <label>Days</label>
                                                    </div>
                                                </div>}
                                            </div>
                                            <div className="gpf-info">
                                                <img src={require('../../../assets/info-icon.svg')} />
                                                <div className="gpf-tooltip">
                                                    <p>Choose whether you want to add buffer and transit days to ASN’s validity or not. The timeline for the expected delivery date, at the time of inspection/ASN creation for a vendor, will vary according to ASN’s validity.</p>
                                                    <span className="gpft-text"><span className="gpf-bold">Allow : </span>The number of Buffer and Transit days selected by you will be added to ASN validity, if feasible.</span>
                                                    <span className="gpft-text"><span className="gpf-bold">Disallow : </span>PO validity will be treated as ASN validity.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                </div>        
                            </div>
                        </div>
                        <div className="tab-pane cal-set-height fade" id="tableHeaders" role="tabpanel">
                            <TableHeader 
                            mainHeaderInputChange={this.mainHeaderInputChange}
                            setHeaderInputChange={this.setHeaderInputChange}
                            itemHeaderInputChange={this.itemHeaderInputChange}
                            {...this.props}
                            changesIsSet={this.changesIsSet}
                            {...this.state}
                            openConfigurationHeadersPageName={this.openConfigurationHeadersPageName} 
                            tableHeaderTabOpen={this.tableHeaderTabOpen}/>
                        </div>
                        <div className="tab-pane fade" id="valueConfiguration" role="tabpanel">
                            <div className="gen-pi-formate min-heightcalc p-lr-47">
                                <div className="buff-days-inc-dec m-top-30">
                                    <div className="bdid-left width240">
                                        <h3>Select Default Transporter Name</h3>
                                    </div>
                                    <div className="bdid-right">
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus search-input" id="transporter" value={this.state.transporter} onChange={this.handleTransportarSearchText} onKeyPress={this.openTransporterModal} autoComplete="off"/>
                                            <span className="modal-search-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick={(e) => this.openTransporterModal(e, 'Search')}>
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.transporterModal && <div className="backdrop-transparent"></div>}
                                            {this.state.transporterModal ? <VendorTransporter {...this.props} {...this.state} onCloseTransporter={this.onCloseTransporter} openTransporterModal={this.openTransporterModal} transportarData={this.state.transportarData} onRef={ref => (this.child = ref)} onSelectTransportar={this.onSelectTransportar}/> : null}
                                        </div>
                                    </div>
                                    {/* {this.state.otherTransportFlag && <div className="bdid-right">
                                        <div className="inputTextKeyFucMain">
                                            <label></label>
                                            <input type="text" className="other-trans-input onFocus" id="other" value={this.state.otherTransporterValue} onChange={this.handleTransportarSearchText} placeholder={this.state.otherTransportFlag ? "Enter Other Transporter" : ""} autoComplete="off"/>
                                        </div>
                                    </div>} */}
                                </div>
                            </div>
                        </div>
                        {this.state.loader ? <FilterLoader /> : null}
                        {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                        {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                    </div>
                </div>
                {this.state.confirmModal ? <UserTypeActivityChangeConfirmModal {...this.props} {...this.state} headerMsg={this.state.headerMsg} changesIsSet={this.changesIsSet} paraMsg={this.state.paraMsg} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}
            </div>
        )
    }
}
function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}
function clean(obj) {
    for (var propName in obj) { 
      if (obj[propName] == "") {
        delete obj[propName];
      }
    }
}