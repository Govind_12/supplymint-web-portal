import React, { Component } from 'react'
import AsnSetting from './asnSetting';

export default class ShipmentTrackContain extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-md-12 pad-0">
                    <div className="subMenu p-lr-47">
                        <ul className="list-inline">
                            <li className={this.props.shipTrackMenu == "asnFormat" ? "fontBold activeTab" : ""} name="asnFormat" onClick={this.props.handleShipmentTrackingMenu}>ASN Format</li>
                        </ul>
                    </div>
                </div>
                {this.props.shipTrackMenu == "asnFormat" && <AsnSetting {...this.props} {...this.state} />}
            </div>
        )
    }
}
