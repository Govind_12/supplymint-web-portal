import React from 'react';

class SettingList extends React.Component {
    constructor(props){
        super(props);
        this.state={

        }
    }

    render() {
        return (
            <React.Fragment>
                <div className="backdrop-transparent zIndex2"></div>
                <div className="dropdown-menu-city1 general-setting-list-drop">
                    <ul className="dropdown-menu-city-item">
                        <li className="dmci-heading">
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="processSettingAllCheck" name="processSettingAllCheck" checked={this.props.processSettingAllCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Process Setting
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="invoiceApprovalCheck" name="invoiceApprovalCheck" checked={this.props.invoiceApprovalCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Invoice approval based LR Creation
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="inspectionRequiredCheck" name="inspectionRequiredCheck" checked={this.props.inspectionRequiredCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Inspection Required
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="invoiceUploadRequiredCheck" name="invoiceUploadRequiredCheck" checked={this.props.invoiceUploadRequiredCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Invoice Upload Required
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="multipleASNCreationCheck" name="multipleASNCreationCheck" checked={this.props.multipleASNCreationCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Multiple ASN Creation
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="asnCancellationACheck" name="asnCancellationACheck" checked={this.props.asnCancellationACheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                ASN Cancellation after Approval for Vendor
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="maskASNNoCheck" name="maskASNNoCheck" checked={this.props.maskASNNoCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Mask ASN No. Until Approved by Retailer for Vendor
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="confirmGRCCheck" name="confirmGRCCheck" checked={this.props.confirmGRCCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Confirm GRC
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="cancelLineItemsCheck" name="cancelLineItemsCheck" checked={this.props.cancelLineItemsCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Cancel Line Items
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="packingListCheck" name="packingListCheck" checked={this.props.packingListCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Packing List upload at the time of ASN Creation
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="poConfirmationCheck" name="poConfirmationCheck" checked={this.props.poConfirmationCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                PO Confirmation by Vendor
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="gateEntryCheck" name="gateEntryCheck" checked={this.props.gateEntryCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Gate Entry Delivery Confirmation
                            </label>
                        </li>
                        <li className="dmci-heading">
                            <label className="checkBoxLabel0">
                                <input type="checkBox"  id="reportSettingAllCheck" name="reportSettingAllCheck" checked={this.props.reportSettingAllCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Reports Setting for Vendors
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="ledgerReportCheck" name="ledgerReportCheck" checked={this.props.ledgerReportCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Ledger Report
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="salesReportCheck" name="salesReportCheck" checked={this.props.salesReportCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Sales Report
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="stockReportCheck" name="stockReportCheck" checked={this.props.stockReportCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Stock Report
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="orderReportCheck" name="orderReportCheck" checked={this.props.orderReportCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Order Report
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="shipmentReportCheck" name="shipmentReportCheck" checked={this.props.shipmentReportCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Shipment Report
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="logisticReportCheck" name="logisticReportCheck" checked={this.props.logisticReportCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Logistic Report
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="inspectionReportCheck" name="inspectionReportCheck" checked={this.props.inspectionReportCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Inspection Report
                            </label>
                        </li>
                        <li className="dmci-heading">
                            <label className="checkBoxLabel0">
                                <input type="checkBox"  id="processWiseTatSettingAllCheck" name="processWiseTatSettingAllCheck" checked={this.props.processWiseTatSettingAllCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Process wise TAT setting
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="tatForInvoiceCheck" name="tatForInvoiceCheck" checked={this.props.tatForInvoiceCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                TAT for Invoice upload after ASN Approval
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="tatForLRCreationCheck" name="tatForLRCreationCheck" checked={this.props.tatForLRCreationCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                TAT for LR Creation after Invoice Approval
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="noOfTimesReInspectionCheck" name="noOfTimesReInspectionCheck" checked={this.props.noOfTimesReInspectionCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                No. Of times Re-Inspection Allowed
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="asnBufferCheck" name="asnBufferCheck" checked={this.props.asnBufferCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                ASN Buffer Days Allowed
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="inspectionBufferCheck" name="inspectionBufferCheck" checked={this.props.inspectionBufferCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Inspection Buffer Days Allowed
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="percentOfFulfilmentCheck" name="percentOfFulfilmentCheck" checked={this.props.percentOfFulfilmentCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                % of Fulfilment allow for ASN Creation for Vendor
                            </label>
                        </li>
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" id="createASNCheck" name="createASNCheck" checked={this.props.createASNCheck} onChange={this.props.handleCheck}/>
                                <span className="checkmark1"></span>
                                Create ASN from  & ASN to Date Range Validity
                            </label>
                        </li>
                    </ul>
                </div>
            </React.Fragment>
        )
    }
}

export default SettingList;