import React from 'react';
import axios from 'axios';
import { CONFIG } from '../../../config';
import SmallLoader from '../../loaders/smallLoader';
import CheckGreen from "../../../assets/greencheck.png";

class CustomDataUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customSelect: false,
            customOption: "Select Template",
            customData: "",
            templateData: "",
            header: "",

            downloadLink: null,
            status: "",
            fileData: {},
            fname: "",
            filePath: "",
        }
    }

    componentDidMount() {
        let payload ={
            no: 1,
            type: 1,
            module: "DIGIVEND"
        }
        this.props.dataSyncRequest(payload)

        let payloadForTemplate = {
            module: "DIGIVEND"
        }
        this.props.getTemplateRequest(payloadForTemplate)
        sessionStorage.setItem('currentPage', "RDVMAIN")
        sessionStorage.setItem('currentPageName', "Custom Data")
    }
    
    static getDerivedStateFromProps(nextProps, preState){
        if (nextProps.administration.dataSync.isSuccess) {
            return {
                customData: nextProps.administration.dataSync.data.resource == null ? [] : nextProps.administration.dataSync.data.resource
            }
        }
        if (nextProps.administration.getTemplate.isSuccess) {
            return {
                templateData: nextProps.administration.getTemplate.data.resource == null ? [] : nextProps.administration.getTemplate.data.resource
            }
        }
        return null;
    }

    componentDidUpdate(){
        if(this.props.administration.dataSync.isSuccess){
            this.setState({
                customData: this.props.administration.dataSync.data.resource == null ? [] : this.props.administration.dataSync.data.resource
            })
            this.props.dataSyncClear();
        }
        if (this.props.administration.getTemplate.isSuccess){
            this.setState({
                templateData: this.props.administration.getTemplate.data.resource == null ? [] : this.props.administration.getTemplate.data.resource
            })
            this.props.getTemplateClear();
        } 
        if (this.props.administration.download.isSuccess){
            this.setState({
                downloadLink: this.props.administration.download.data.resource
            })
            this.props.downloadClear();
        }
    }

    openCustomSelect =(e)=> {
        e.preventDefault();
        this.setState({
            customSelect: !this.state.customSelect
        }, () => document.addEventListener('click', this.closeCustomSelect));
    }
    closeCustomSelect =()=> {
        this.setState({ customSelect: false }, () => {
            document.removeEventListener('click', this.closeCustomSelect);
        });
    }
    customOption =(e, data)=> {
        e.preventDefault();
        this.setState({
            customOption: data !== undefined ? data.fullFileName : "Select Template",
            header: data !== undefined ? data.header : "",
            fname: "",
        },()=>{
            if(data !== undefined)
              this.props.downloadRequest(data.fullFileName)
        })
    }

    fileChange =(e)=> {
        let files = e.target.files[0];
        document.getElementById('fileUpload').value = ''
        this.setState({
            fileData: files,
            fname: files.name,
            downloadLink: null,
            status: "",
            filePath: "",
        })
    }

    downloadTemplate(headers, fileName) {
        var csv = headers;
        console.log(csv);
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = fileName+'.CSV';
        hiddenElement.click();
    }

    onUploadFile =(e)=> {
        e.preventDefault();
        const formData = new FormData();
        this.setState({
            smallLoader: true,
            status: "File Uploading"
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        formData.append('file', this.state.fileData);
        formData.append('template', this.state.customOption);
        axios.post(`${CONFIG.BASE_URL}/aws/s3bucket/upload`, formData, { headers: headers })
            .then(res => {
                this.setState({
                    smallLoader: false,
                    filePath: res.data.data.resource.filePath,
                    status: "File Uploaded Successfully"
                })
            }).catch((error) => {
                this.setState({
                    smallLoader: false,
                    status: "Failed To Upload File"
                })
            })
    }

    deleteUploads =(e)=> {
        this.setState({ fname : "", fileData: {}})
    }

    render() {
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="custop-data-upload p-lr-47">
                        <div className="col-lg-8 col-md-12 col-sm-12 pad-0 m-top-30">
                            <div className="cdu-head">
                                <h3>Custom Data Upload</h3>
                                <p>Choose template below to upload or download data</p>
                            </div>
                            <div className="cdu-body">
                                <div className="cdub-select-upload">
                                    <label>Choose Data Template</label>
                                    <div className="cdubsu-select">
                                        <button type="button" onClick={(e) => this.openCustomSelect(e)}><span>{this.state.customOption}</span></button>
                                        {this.state.customSelect &&
                                        <div className="cdubsus-dropdown">
                                            <ul className="cdubsusd-inner">
                                                {/* <li key="1" onClick={(e) => this.customOption(e)}>Select Template</li> */}
                                                {this.state.templateData.map(( data,key)=>
                                                 <li key={key} onClick={(e) => this.customOption(e, data)}>{data.fullFileName}</li>
                                                )}
                                            </ul>
                                        </div>}
                                        {this.state.customOption === "Select Template" ? null : 
                                        <label className="cdubsu-upload">
                                            <input type="file" id="fileUpload" onChange={(e) => this.fileChange(e)}/>
                                                <span className="cdubsuu-img">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 18">
                                                        <path fill="#8b77fa" fillRule="evenodd" d="M3.867.069c-.943 0-1.89.379-2.63 1.135C-.286 2.76-.375 5.017 1.03 6.453l9.677 9.886c1.02 1.044 2.37 1.616 3.81 1.592 1.422-.018 2.771-.604 3.796-1.651 1.028-1.05 1.614-2.399 1.651-3.797a5.241 5.241 0 0 0-1.522-3.827L10.505.618l-.812.837L17.63 9.49c1.624 1.66 1.568 4.221-.132 5.957-.81.829-1.874 1.292-2.994 1.306a4.06 4.06 0 0 1-2.981-1.247L1.846 5.62a2.27 2.27 0 0 1-.649-1.739c.038-.665.342-1.32.855-1.845C3.062 1.005 4.628.99 5.62 2l8.059 8.236c.174.177.265.39.264.615a.902.902 0 0 1-.272.622.946.946 0 0 1-.642.297.795.795 0 0 1-.606-.25L6.706 5.68l-.815.834 5.716 5.839c.391.4.9.616 1.457.595a2.083 2.083 0 0 0 1.422-.642c.39-.399.607-.913.61-1.449a2.056 2.056 0 0 0-.602-1.454L6.435 1.167A3.58 3.58 0 0 0 3.867.07z"/>
                                                    </svg>
                                                </span>
                                            <span className="cdubsu-up">Choose File</span>
                                        </label>} 
                                        {this.state.fname !== "" && <div className="asn-uploaded-file"><div className="asn-up-file-name"><p>{this.state.fname}</p></div><span onClick={(e) => this.deleteUploads(e)}><img src={require('../../../assets/clearSearch.svg')} /></span></div>} 
                                        {this.state.fname != "" || this.state.smallLoader ?
                                            this.state.smallLoader ?
                                            <li>
                                                <SmallLoader />
                                                {this.state.status == "" ? null : <span className="displayBlock uploadStatusMs loaderTextSke">{this.state.status}</span>}
                                            </li>
                                            :
                                            <li>
                                                {this.state.status == "File Uploaded Successfully" ? null : <button className="cs-upload" onClick={(e) => this.onUploadFile(e)} type="button">Upload</button>}
                                                {this.state.status == "" ? null : this.state.status == "Failed To Upload File" ? <span className="uploadStatusMs uploadStatusSkc">{this.state.status}
                                                </span> : <div className="uploadSuccessIcon"> <img src={CheckGreen} /> <span className="uploadStatusMs uploadStatusSkc">{this.state.status}
                                                </span> </div>}
                                            </li>
                                        : null} 
                                    </div>
                                    {this.state.customOption === "Select Template" ? null :<div className="cdub-expected-header m-top-10">
                                        <h5>Expected Column Header</h5> <button className="cdub-download" onClick={(e) => this.downloadTemplate(this.state.header,this.state.customOption)} type="button">Download Template</button>
                                        <p>{this.state.header}
                                        {/* <button type="button">+ 15 Headers</button> */}
                                        </p>
                                    </div>}
                                </div>
                            </div>
                        </div>
                        {this.state.downloadLink == null ? null : this.state.customOption === "Select Template" ? null : <div className="col-lg-4 col-md-12 col-sm-12 pad-0">
                            <div className="cdu-right">
                                <div className="cdur-uploaded-file">
                                    <span>Previously Uploaded File Found</span>
                                    <img src={require('../../../assets/eicon.svg')} />
                                </div>
                                <div className="cdur-selected-file">
                                    <p> {this.state.downloadLink.fileName}</p>
                                    <span className="cdursf-last-mod">Last Modified: <span className="cdursf-time">{this.state.downloadLink.modifiedOn}</span></span>
                                </div>
                                <a href={this.state.downloadLink.url} download>
                                <button type="button" className="cdur-download-now">Download Now</button></a>
                            </div>
                        </div>}
                    </div>
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-10">
                    <div className="vendor-gen-table">
                        <div className="manage-table">
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th><label>Name</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Data Count</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Event Start</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Event End</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Key</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Status</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.customData.length ? this.state.customData.map( (data,key) =>
                                    <tr key={key}>
                                        <td><label>{data.name}</label></td>
                                        <td><label>{data.dataCount}</label></td>
                                        <td><label>{data.eventStart}</label></td>
                                        <td><label>{data.eventEnd}</label></td>
                                        <td><label>{data.key}</label></td>
                                        <td><label>{data.status}</label></td>
                                    </tr> ) 
                                    : <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default CustomDataUpload;