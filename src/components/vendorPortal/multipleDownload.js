import React from 'react';
import { OrderedMap, List, Map } from "immutable";

const DOWNLOAD_FILES = new OrderedMap({
    vendorPendingOrder: [{
        key: "POPDF",
        label: " POPDF"
    }],
    vendorProcessedOrder: [{
        key: "POPDF",
        label: " POPDF"
    }],
    vendorAsnUnderApprove: [{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }, {
        key: "BARCODE",
        label: "BARCODE"
    }, {
        key: "QC",
        label: "QC"
    }, {
        key: "PACKAGING",
        label: "PACKAGING"
    }],
    vendorApprovedAsn: [{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }, {
        key: "BARCODE",
        label: "BARCODE"
    }, {
        key: "QC",
        label: "QC"
    }],
    vendorLrProcess: [{
        key: "INVOICE",
        label: "INVOICE"
    }, {
        key: "PACKAGING",
        label: "PACKAGING"
    },{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }],
    entPendingOrder: [{
        key: "POPDF",
        label: " POPDF"
    }],
    entProcessedOrder: [{
        key: "POPDF",
        label: " POPDF"
    }],
    entAsnUnderApprove: [{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }, {
        key: "BARCODE",
        label: "BARCODE"
    }, {
        key: "QC",
        label: "QC"
    }, {
        key: "PACKAGING",
        label: "PACKAGING"
    }],
    entApprovedAsn: [{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }, {
        key: "BARCODE",
        label: "BARCODE"
    }, {
        key: "QC",
        label: "QC"
    }],
    entLrProcess: [{
        key: "INVOICE",
        label: "INVOICE"
    }, {
        key: "PACKAGING",
        label: "PACKAGING"
    },{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }],
    gateEntry: [{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }, {
        key: "INVOICE",
        label: "INVOICE"
    }, {
        key: "PACKAGING",
        label: "PACKAGING"
    }],
    grcStatus: [{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }, {
        key: "INVOICE",
        label: "INVOICE"
    }],
    goodsInTrans: [{
        key: "SHIPMENTPDF",
        label: "ASN PDF"
    }, {
        key: "INVOICE",
        label: "INVOICE"
    }, {
        key: "PACKAGING",
        label: "PACKAGING"
    }, {
        key: "QC",
        label: "QC"
    }],
    insRequired: [{
        key: "POPDF",
        label: "POPDF"
    }, {
        key: "BARCODE",
        label: "BARCODE"
    }, {
        key: "PPS",
        label: "PPS REPORT"
    }, {
        key: "REPORT",
        label: "TEST REPORT"
    }, {
        key: "PACKAGING",
        label: "PACKING LIST"
    }],
    insRequested: [{
        key: "POPDF",
        label: "POPDF"
    }, {
        key: "PPS",
        label: "PPS REPORT"
    }, {
        key: "REPORT",
        label: "TEST REPORT"
    }, {
        key: "PACKAGING",
        label: "PACKING LIST"
    }],
    cancelledInspection: [{
        key: "PPS",
        label: "PPS REPORT"
    }, {
        key: "REPORT",
        label: "TEST REPORT"
    }, {
        key: "PACKAGING",
        label: "PACKING LIST"  
    }, {
        key: "QC",
        label: "QC"
    }],
        
    orderReport:  [{
        key: "POPDF",
        label: "POPDF"
    }],
    shipmentReport: [
        {
            key: "INVOICE",
            label: "INVOICE"
        }, 
        {
        key: "BARCODE",
        label: "BARCODE"
    }, {
        key: "PACKAGING",
        label: "PACKING LIST"
    },
    ],
    lrReport: [{
        key: "INVOICE",
        label: "INVOICE"
    }, {
        key: "PACKAGING",
        label: "PACKING LIST"
    }],
    inspectionReport: [{
        key: "BARCODE",
        label: "BARCODE"
    }, {
        key: "QC",
        label: "QC"
    }]
})
export default class MultipleDownload extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.orders.multipleDocumentDownload.isSuccess) {
            if (nextProps.orders.multipleDocumentDownload.data.resource != undefined) {
                window.open(nextProps.orders.multipleDocumentDownload.data.resource)
                if (document.getElementById('multipleDownload') != undefined) {
                    document.getElementById('multipleDownload').value = ""
                    return []
                }
            }
        }
        return {}
    }
  
    bulkDownload = (key) => {
        if (key != "") {
            let payload = this.props.checkItems.map((data) => ({
                // fileType: key, orderId: "", shipmentId: key == "POPDF" ? "" : data.shipmentId || "", documentNumber: key == "POPDF" ? data.documentNumber : ""
                fileType: key, orderId: data.orderId == undefined ? "" : data.orderId, shipmentId: data.shipmentId == undefined || data.shipmentId == 0 ? (data.saId == 0 || data.saId == undefined ? data.id : data.saId) : data.shipmentId, documentNumber: data.documentNumber == undefined ? "": data.documentNumber,
                isLRPage: this.props.drop != "goodsInTrans" ? "FALSE" : "TRUE",
                lrId: data.id
            }))
            this.props.multipleDocumentDownloadRequest(payload)
        }
    }
    render() {
        //console.log(this.props.checkItems)
        return (
            <div>
                {/* <div className="displayInline posRelative npo-btn">
                    {this.props.checkItems.length != 0 ? DOWNLOAD_FILES.get(this.props.drop).map((_, k) => <span onClick={() => this.bulkDownload(_.key)} dataV={_.k} key={k}>{_.label}</span>)
                        : <select className={"multipleDownloadSelect btnDisabled pointerNone"} disabled>
                            <option value="">Download</option>
                        </select>}
                </div> */}
                <ul className="pi-history-download">
                    <li>
                        {this.props.checkItems.length != 0 ? DOWNLOAD_FILES.get(this.props.drop).map((_, k) => 
                        <button className="pi-pdf-download" type="button" onClick={() => this.bulkDownload(_.key)} datav={_.k} key={k}>
                            { (_.key == "SHIPMENTPDF" || _.key == "PPS" || _.key == "REPORT" || _.key == "POPDF") &&
                            <span className="pi-pdf-svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 23.764 22.348">
                                    <g id="prefix__files">
                                        <g id="prefix__Group_2456" data-name="Group 2456">
                                            <g id="prefix__Group_2455" data-name="Group 2455">
                                                <path fill="#12203c" id="prefix__Path_606" d="M1.4 20.252V2.095a.7.7 0 0 1 .7-.7h10.471V4.19a1.4 1.4 0 0 0 1.4 1.4h2.793v2.1h1.4V4.888a.7.7 0 0 0-.2-.5L13.765.2a.7.7 0 0 0-.5-.2H2.1A2.1 2.1 0 0 0 0 2.095v18.157a2.1 2.1 0 0 0 2.1 2.1h4.884v-1.4H2.1a.7.7 0 0 1-.7-.7z" data-name="Path 606" />
                                                <text fontSize="8px" fontFamily="ProximaNova-Bold,Proxima Nova" fontWeight="700;fill:#12203c" id="prefix__PDF" transform="translate(7.764 15.414)" >
                                                    <tspan x="0" y="0">PDF</tspan>
                                                </text>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </span>}
                            {/* This icon for barcode Upload */}
                            {_.key == "BARCODE" &&
                            <span className="pipdf-barcode">
                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="12" viewBox="0 0 19.45 12.967">
                                    <path d="M3.242 9.725V3.242h1.621v6.483zm9.725 0V3.242h1.621v6.483zm-7.294 0V3.242h.81v6.483zm1.621 0V3.242h1.621v6.483zm2.431 0V3.242h.81v6.483zm1.621 0V3.242h.81v6.483zm4.052 0V3.242h.81v6.483zm-.81-8.1h3.242v3.238h1.62V0h-4.862zM1.621 4.863V1.621h3.242V0H0v4.863zm3.242 6.483H1.621V8.1H0v4.863h4.863zM17.829 8.1v3.242h-3.241v1.621h4.862V8.1z" data-name="iconmonstr-barcode-3 (1)"/>
                                </svg>
                            </span>}
                            {/* This icon for INVOICE  */}
                            {(_.key == "INVOICE" || _.key == "QC") &&
                            <span className="pipdf-invoice">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 17.663 21.492">
                                <g data-name="Group 2541">
                                    <g data-name="Group 2540">
                                        <path d="M4.377 8.359h3.184a.5.5 0 0 0 0-1H4.377a.5.5 0 0 0 0 1z" data-name="Path 620" transform="translate(-40.218) translate(40.218)"/>
                                        <path d="M10.546 10.597H4.377a.5.5 0 1 0 0 .995h6.169a.5.5 0 0 0 0-.995z" data-name="Path 621" transform="translate(-40.218) translate(40.218)"/>
                                        <path d="M10.546 13.834H4.377a.5.5 0 1 0 0 .995h6.169a.5.5 0 0 0 0-.995z" data-name="Path 622" transform="translate(-40.218) translate(40.218)"/>
                                        <path d="M17.661 17.562V1.891A1.816 1.816 0 0 0 15.922 0H5.095a2.024 2.024 0 0 0-2.113 1.891v.846h-.871A2.164 2.164 0 0 0 0 4.8v15.7a.871.871 0 0 0 .5.7.771.771 0 0 0 .846-.149l1.517-1.368L4.679 21.3a.746.746 0 0 0 .5.2.8.8 0 0 0 .5-.2l1.791-1.617L9.261 21.3a.746.746 0 0 0 .995 0l1.816-1.617 1.492 1.368a.6.6 0 0 0 .746.149.746.746 0 0 0 .373-.7v-3.535l.3-.224 1.517 1.368a.721.721 0 0 0 .522.2.447.447 0 0 0 .249-.05.721.721 0 0 0 .39-.697zm-3.979 2.388l-1.17-1.144a.7.7 0 0 0-.97-.025L9.726 20.4 7.96 18.783a.8.8 0 0 0-.995 0L5.174 20.4l-1.816-1.617a.821.821 0 0 0-1.07.025L.995 19.95V4.8a1.169 1.169 0 0 1 1.119-1.07h10.7a.967.967 0 0 1 .868 1.07v15.15zm2.984-18.059v15.124l-1.244-1.144c-.2-.2-.249-.249-.746-.174V4.8a1.959 1.959 0 0 0-1.866-2.065H3.982v-.844A1.033 1.033 0 0 1 5.099 1h10.7a.821.821 0 0 1 .872.766.812.812 0 0 1-.005.125z" data-name="Path 623" transform="translate(-40.218) translate(40.218)"/>
                                    </g>
                                </g>
                            </svg>
                            </span>}
                            {_.key == "PACKAGING" &&
                            <span className="pipdf-packing">
                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 18.33 18.329">
                                    <g data-name="box (2)">
                                        <path d="M9.159 18.329a.379.379 0 0 1-.176-.044L.205 13.703A.382.382 0 0 1 0 13.365V4.773a.382.382 0 0 1 .552-.341l8.777 4.391a.382.382 0 0 1 .211.341v8.783a.382.382 0 0 1-.184.327.376.376 0 0 1-.197.055zm-8.395-5.2l8.013 4.184V9.4L.764 5.391z" data-name="Path 844" transform="translate(.001 -.001)"/>
                                        <path d="M9.171 18.33a.381.381 0 0 1-.382-.382V9.165A.382.382 0 0 1 9 8.824l8.781-4.392a.382.382 0 0 1 .553.341v8.592a.382.382 0 0 1-.205.338l-8.777 4.582a.379.379 0 0 1-.176.044zm.382-8.929v7.917l8.013-4.184V5.392zm8.395 3.964h.008z" data-name="Path 845" transform="translate(.001 -.001)"/>
                                        <path d="M.382 5.155a.382.382 0 0 1-.171-.723L9 .041a.383.383 0 0 1 .341 0l8.777 4.391a.382.382 0 1 1-.341.684L9.171.809.553 5.115a.38.38 0 0 1-.171.04z" data-name="Path 846" transform="translate(.001 -.001)"/>
                                        <path d="M13.747 11.456a.382.382 0 0 1-.382-.382V7.109L4.799 2.823a.382.382 0 0 1 .342-.684l8.777 4.391a.384.384 0 0 1 .211.342v4.2a.382.382 0 0 1-.382.382z" data-name="Path 847" transform="translate(.001 -.001)"/>
                                    </g>
                                </svg>
                            </span>}
                            {_.label}
                        </button>): null}
                    </li>
                </ul>
            </div>
        )
    }
}