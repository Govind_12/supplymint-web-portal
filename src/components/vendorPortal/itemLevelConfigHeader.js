import React from 'react';
import { Link } from 'react-router-dom';
import successIcon from '../../assets/greencheck.png'
import closeSearch from "../../assets/close-recently.svg"
import _ from 'lodash';
import cross from '../../assets/group-4.svg'
class ItemLevelConfigHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ItemLvlColoumSetting: this.props.ItemLvlColoumSetting,
            search: ""
        }
    }

    ItemLvlOpenColoumSetting(data) {
        this.props.ItemLvlOpenColoumSetting(data)
    }

    ItemLvlCloseColumn(data) {
        this.props.ItemLvlCloseColumn(data)
    }
    handleChange(e) {
        this.setState({
            search: e.target.value
        })
    }
    clearSearch() {
        this.setState({
            search: ""
        })
    }
    render() {
        // console.log(this.props.ItemLvlColoumSetting)
        const { search } = this.state
        var result = _.filter(this.props.ItemLvlFixedHeader, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        });
        // var result = this.props.ItemLvlFixedHeader
        return (
            <div>
                {this.props.ItemLvlColoumSetting ? <div className="columnFilterGeneric" >

                    <span className="glyphicon glyphicon-menu-right" onClick={(e) => this.ItemLvlOpenColoumSetting("false")}></span>
                    <div className="columnSetting" onClick={(e) => this.ItemLvlOpenColoumSetting("false")}>Columns Setting</div>
                    <div className="columnFilterDropDown">
                        <div className="col-md-12 pad-0 filterHeader">
                            <div className="col-md-7 pad-0">
                                <input type="search" className="width100 inputSearch" value={this.state.search} onChange={(e) => this.handleChange(e)} placeholder="Search Columns…" />
                                {this.state.search != "" ? <div className="crossIcon"><img src={cross} onClick={(e) => this.clearSearch(e)} /></div> : null}
                            </div>
                            <div className="col-md-5 pad-right-0 columns">
                                <div className="col-md-7 pad-0">
                                    <h3>Visible Columns <span>{this.props.ItemLvlHeaderCondition ? this.props.ItemLvlGetHeaderConfig.length : this.props.ItemLvlCustomHeadersState.length}</span></h3>
                                </div>
                                <div className="col-md-5 pad-0 textCenter">
                                    <button className="resetBtn" onClick={(e) => this.props.ItemLvlResetColumnConfirmation(e)}>Reset to default</button>
                                    {!this.props.ItemLvlHeaderCondition ? <button className={this.props.ItemLvlCustomHeadersState.length == 0 || this.props.ItemLvlSaveState.length == 0 ? "opacity saveBtn" : "saveBtn"} onClick={(e) => this.props.ItemLvlCustomHeadersState.length == 0 || this.props.ItemLvlSaveState.length == 0 ? null : this.props.ItemLvlsaveColumnSetting(e)}>Save</button> :
                                        <button className={this.props.ItemLvlGetHeaderConfig.length == 0 || this.props.ItemLvlSaveState.length == 0 ? "opacity saveBtn" : "saveBtn"} onClick={(e) => this.props.ItemLvlGetHeaderConfig.length == 0 || this.props.ItemLvlSaveState.length == 0 ? null : this.props.ItemLvlsaveColumnSetting(e)}>Save</button>}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-7 pad-0">
                            <div className="filterLeftHead">
                                <div className="col-md-12 identifier">
                                    <h2>Fixed</h2>
                                    <ul className="list-inline">
                                        {result.length != 0 ? result.map((data, key) => (<div key={key} className="inlineBlock col-md-4 pad-lft-0">
                                            {this.props.ItemLvlHeaderCondition ? <li className={!this.props.ItemLvlGetHeaderConfig.includes(data) ? "active" : "opacity active"} onClick={(e) => !this.props.ItemLvlGetHeaderConfig.includes(data) ? this.props.ItemLvlpushColumnData(data) : null}>{data}</li> :
                                                <li className={!this.props.ItemLvlCustomHeadersState.includes(data) ? "active" : "opacity active"} onClick={(e) => !this.props.ItemLvlCustomHeadersState.includes(data) ? this.props.ItemLvlpushColumnData(data) : null}>{data}</li>}
                                        </div>
                                        )) : "No Data Found"}
                                        {/* <li>Assortment</li>
                                <li>Updated MBQ</li>
                                <li>Closing Stock</li>
                                <li className="active">Opening Stock</li>
                                <li className="active">Page Views</li>
                                <li>Requirements</li> */}
                                    </ul>
                                </div>
                                {/* <div className="col-md-12 identifier">
                                    <h2>Custom</h2>
                                    <ul className="list-inline">
                                        {this.props.ItemLvlCustomHeadersState.map((data, key) => (
                                            <li key={key} className="active">{data}</li>
                                        ))}
                                    </ul>
                                </div> */}
                            </div>
                        </div>
                        <div className="col-md-5 pad-0">
                            <div className="filterRightHead">
                                {/* <div className="col-md-12 savedColumn">
                            <h3>Saved Column presets</h3>
                            <label>My setting</label>
                            <label>Setting 2</label>
                        </div> */}
                                <div className="col-md-12 visibleColumn">
                                    <h3>All Visible Columns</h3>
                                    <div className="row">
                                        {this.props.ItemLvlHeaderCondition ? this.props.ItemLvlGetHeaderConfig.map((data, key) => (
                                            <div key={key} className="col-md-6 pad-0 m-top-3 displayFlex justifyCenter"><label className="alignMiddle displayFlex"><span className="eachVisibleColumn">{data}</span>
                                                {this.props.mandateHeaderItem.length != 0 ? !this.props.mandateHeaderItem.includes(data) && <span onClick={(e) => this.ItemLvlCloseColumn(data)}> <img src={closeSearch} /></span> :<span onClick={(e) => this.ItemLvlCloseColumn(data)}> <img src={closeSearch} /></span> }</label></div>
                                        )) : this.props.ItemLvlCustomHeadersState.map((dataa, keyy) => (
                                            <div key={keyy} className="col-md-6 pad-0 m-top-3 displayFlex justifyCenter">
                                                <label className="alignMiddle displayFlex">
                                                    <span className="eachVisibleColumn">{dataa}</span>{this.props.mandateHeaderItem.length != 0 ? !this.props.mandateHeaderItem.includes(dataa) && <span onClick={(e) => this.ItemLvlCloseColumn(dataa)}> <img className="float_Right displayPointer" src={closeSearch} /></span> : <span onClick={(e) => this.ItemLvlCloseColumn(dataa)}> <img className="float_Right displayPointer" src={closeSearch} /></span>}
                                                </label>
                                            </div>
                                        ))}
                                    </div>
                                    {/* <div className="col-md-6 pad-0 m-top-3"><label>MBQ</label></div>
                            <div className="col-md-6 pad-0"> <label>Updated MBQ</label></div>
                            <div className="col-md-6 pad-0"><label>Closing Stock</label></div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : <div className="columnFilterGeneric" onClick={(e) => this.ItemLvlOpenColoumSetting("true")}>
                        <span className="glyphicon glyphicon-menu-left"></span></div>}
            </div>
        )
    }
}

export default ItemLevelConfigHeader;