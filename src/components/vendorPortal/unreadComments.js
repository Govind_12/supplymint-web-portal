import React from 'react';

export default class UnreadComment extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openOrder: 0,
            processedOrder: 0,
            cancelledOrder: 0,
            inspectionRequired: 0,
            inspectionRequested: 0,
            pendingAsnApproval: 0,
            approvedAsn: 0,
            rejectedAsn: 0,
            lrProcessing: 0,
            shipmentInvoiceCount: 0,
            goodsInTransit: 0,
            gateEntry: 0,
            grcStatus: 0,
            isEnterprise: sessionStorage.getItem("uType") == "ENT" ? true : false,

        };
    }
    componentDidMount() {
        this.props.dashboardUnreadCommentRequest("Asd")
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.dashboardUnreadComment.isSuccess) {
            this.setState({
                openOrder: nextProps.orders.dashboardUnreadComment.data.resource.pendingCount && nextProps.orders.dashboardUnreadComment.data.resource.pendingCount,
                processedOrder: nextProps.orders.dashboardUnreadComment.data.resource.processedCount && nextProps.orders.dashboardUnreadComment.data.resource.processedCount,
                cancelledOrder: nextProps.orders.dashboardUnreadComment.data.resource.cancelCount && nextProps.orders.dashboardUnreadComment.data.resource.cancelCount,
                inspectionRequired: nextProps.orders.dashboardUnreadComment.data.resource.pendingQCConfirmCount && nextProps.orders.dashboardUnreadComment.data.resource.pendingQCConfirmCount,
                inspectionRequested: nextProps.orders.dashboardUnreadComment.data.resource.pendingQCCount && nextProps.orders.dashboardUnreadComment.data.resource.pendingQCCount,
                pendingAsnApproval: this.state.isEnterprise ? nextProps.orders.dashboardUnreadComment.data.resource.shipmentRequestedCount && nextProps.orders.dashboardUnreadComment.data.resource.shipmentRequestedCount 
                                                : nextProps.orders.dashboardUnreadComment.data.resource.vendorShipmentRequestedCount && nextProps.orders.dashboardUnreadComment.data.resource.vendorShipmentRequestedCount,
                approvedAsn: nextProps.orders.dashboardUnreadComment.data.resource.shipmentConfirmedCount && nextProps.orders.dashboardUnreadComment.data.resource.shipmentConfirmedCount,
                rejectedAsn: nextProps.orders.dashboardUnreadComment.data.resource.shipmentRejectedCount && nextProps.orders.dashboardUnreadComment.data.resource.shipmentRejectedCount,
                lrProcessing: nextProps.orders.dashboardUnreadComment.data.resource.shipmentShippedCount && nextProps.orders.dashboardUnreadComment.data.resource.shipmentShippedCount,
                shipmentInvoiceCount: nextProps.orders.dashboardUnreadComment.data.resource.shipmentInvoiceCount && nextProps.orders.dashboardUnreadComment.data.resource.shipmentInvoiceCount,
                goodsInTransit: nextProps.orders.dashboardUnreadComment.data.resource.shipmentIntransitCount && nextProps.orders.dashboardUnreadComment.data.resource.shipmentIntransitCount,
                gateEntry: nextProps.orders.dashboardUnreadComment.data.resource.shipmentDeliveredCount && nextProps.orders.dashboardUnreadComment.data.resource.shipmentDeliveredCount,
                grcStatus: nextProps.orders.dashboardUnreadComment.data.resource.grcCount && nextProps.orders.dashboardUnreadComment.data.resource.grcCount,
                
            })
            this.props.dashboardUnreadCommentClear();
        }
        if (nextProps.orders.dashboardUnreadComment.isError) {
            this.props.dashboardUnreadCommentClear();
        }
        
    }

    isDashboardComment = () =>{
       sessionStorage.setItem("isDashboardComment", 1);
    }

    isInspectionReaquired = () =>{
        sessionStorage.setItem("inspectionStatus", "PENDING_QC_CONFIRM");
        sessionStorage.setItem("isDashboardComment", 1);
    }
    isInspectionRequested = () =>{
        sessionStorage.setItem("inspectionStatus", "PENDING_QC");
        sessionStorage.setItem("isDashboardComment", 1); 
    }
    render(){
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content unread-comment-modal">
                    <div className="ucm-head">
                        <h3>Unread Comments</h3>
                        <button type="button" className="bum-close-btn" onClick={this.props.cancelUnreadComment}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="ucm-body">
                        <div className="ucmb-row">
                            <h3>Orders</h3>
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.openOrder}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/purchaseOrder/pendingOrders'), this.isDashboardComment()}}>Open Orders</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/purchaseOrder/pendingOrders'), this.isDashboardComment()}}>Open Orders</button>
                                }
                                </span>
                            </div>
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.processedOrder}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/purchaseOrder/processedOrders'), this.isDashboardComment()}}>Processed Orders</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/purchaseOrder/processedOrders'), this.isDashboardComment()}}>Processed Orders</button>
                                }
                                </span>
                            </div>
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.cancelledOrder}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/purchaseOrder/cancelledOrders'), this.isDashboardComment()}}>Cancelled PO</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/purchaseOrder/cancelledOrders'), this.isDashboardComment()}}>Cancelled Order</button>
                                }
                                </span>
                            </div>
                        </div>
                        { this.state.isEnterprise ? <div className="ucmb-row">
                            <h3>Inspection</h3>
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.inspectionRequired}</p>
                                <span className="ucmbrb-pgname">
                                <button type="button" onClick={() => {this.props.history.push('/enterprise/qualityCheck/pendingQc'), this.isInspectionReaquired()}}>Inspection Required</button>
                                </span>
                            </div>
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.inspectionRequested}</p>
                                <span className="ucmbrb-pgname">
                                <button type="button" onClick={() => {this.props.history.push('/enterprise/qualityCheck/pendingQc'), this.isInspectionRequested()}}>Inspection Requested</button>
                                </span>
                            </div>  
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>0</p>
                                <span className="ucmbrb-pgname">
                                <button type="button" onClick={() => {this.props.history.push('/enterprise/qualityCheck/cancelledInspection'), this.isDashboardComment()}}>Cancelled Inspection</button>
                                </span>
                            </div>                                                                                                                                                                                                                                                                                                
                        </div>: null}
                        <div className="ucmb-row">
                            <h3>Shipments</h3> 
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.pendingAsnApproval}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/shipment/asnUnderApproval'), this.isDashboardComment()}}>Pending ASN Approval</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/shipment/asnUnderApproval'), this.isDashboardComment()}}>Pending ASN Approval</button>
                                }
                                </span>
                            </div>
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.approvedAsn}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/shipment/approvedAsn'), this.isDashboardComment()}}>Approved ASN</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/shipment/approvedAsn'), this.isDashboardComment()}}>Approved ASN</button>
                                }
                                </span>
                            </div>
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.rejectedAsn}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/shipment/cancelledAsn'), this.isDashboardComment()}}>Rejected ASN</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/shipment/cancelledAsn'), this.isDashboardComment()}}>Rejected ASN</button>
                                }
                                </span>
                            </div>
                        </div>
                        <div className="ucmb-row">
                            <h3>Logistics</h3>
                            { this.props.activeStatusButton && <div className="ucmbr-box ucmbrb-lr">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.shipmentInvoiceCount}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ? 
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/lrProcessing'), this.isDashboardComment(),sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_INVOICE_REQUESTED")}}>LR Processing</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/lrProcessing'), this.isDashboardComment(),sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_INVOICE_REQUESTED")}}>LR Processing</button>
                                }
                                </span>
                                <span className="ucmbrb-pending">Pending for Approval</span>
                            </div> }
                            { this.props.activeStatusButton && <div className="ucmbr-box ucmbrb-lr">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.lrProcessing}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/lrProcessing'), this.isDashboardComment(),sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_SHIPPED") }}>LR Processing</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/lrProcessing'), this.isDashboardComment(), sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_SHIPPED")}}>LR Processing</button>
                                }
                                </span>
                                <span className="ucmbrb-approved">Approved</span>
                            </div>}
                            { !this.props.activeStatusButton && <div className="ucmbr-box ucmbrb-lr">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.lrProcessing}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/lrProcessing'), this.isDashboardComment()}}>LR Processing</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/lrProcessing'), this.isDashboardComment()}}>LR Processing</button>
                                }
                                </span>
                            </div>}
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.goodsInTransit}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/goodsIntransit'), this.isDashboardComment()}}>Goods In Transit</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/goodsIntransit'), this.isDashboardComment()}}>Goods In Transit</button>
                                }
                                </span>
                            </div>
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.gateEntry}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/goodsDelivered'), this.isDashboardComment()}}>Gate Entry</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/goodsDelivered'), this.isDashboardComment()}}>Gate Entry</button>
                                }
                                </span>
                            </div>
                        </div>
                        <div className="ucmb-row">
                            <div className="ucmbr-box">
                                <span className="ucmbrb-head">Total Unread Comments</span>
                                <p>{this.state.grcStatus}</p>
                                <span className="ucmbrb-pgname">
                                { this.state.isEnterprise ?
                                  <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/grcStatus'), this.isDashboardComment()}}>GRC Status</button>
                                  : <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/grcStatus'), this.isDashboardComment()}}>GRC Status</button>
                                }
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    } 
}