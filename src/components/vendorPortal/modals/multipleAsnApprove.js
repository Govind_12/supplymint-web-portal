import React, { Component } from 'react'
import Close from '../../../assets/close-red.svg';
import Upload from '../../../assets/upload-icon.svg';
import ToastLoader from "../../loaders/toastLoader";
import FilterLoader from "../../loaders/filterLoader";
import { CONFIG } from "../../../config/index";
import fileIcon from '../../../assets/fileIcon.svg';
import Eye from '../../../assets/eye-icon.svg';
import axios from 'axios';
import moment from 'moment';
import CreateAsnUpload from '../vendor/orders/createAsnUpload';

const todayDate = moment(new Date()).format("YYYY-MM-DD")

export default class MultipleAsnApprove extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rangeVal: 1,
            poDetailsShipment: [],
            sliderDisable: false,
            valueDate: "",
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            qcModalConfirm: false,
            search: "",
            type: 1,
            qualityModal: false,
            qcUpload: false,
            isQc: "",
            qcDate: "",
            qcDateErr: false,
            qcDateFormatted: "",
            genericDateFormat: "",
            isQcErr: false,
            qcFromDate: "",
            qcToDate: "",
            multipleAsn: [],
            toastMsg: "",
            toastLoader: false,
            barcodeDrop: false,
            barcode: [],
            fileNames: [],
            allQC: [],
            expandedID: "",

            uploadFileType:"",
            ppsUploadModal: false,
            currentAsnId: 0,
        }
    }
    componentDidMount() {
        let multipleAsn = this.props.active
        multipleAsn.forEach((item) => {
            // item.isQc = "",
            item.qcDate = "",
                item.barcode = "",
                item.qcDateErr = false,
                item.qcErr = false,
                item.remarks = "",
                item.alreadyUploadedBarcode = item.isBarcodeUploaded,
                item.barcodeArray = []
        });
        this.setState({
            multipleAsn
        })
    }

    confirmShipment = (e) => {
        // this.qcError()
        // this.qcDateerror()
        this.props.CancelShipment(e)
        let flag = false
        setTimeout(() => {
            let multipleAsn = this.state.multipleAsn
            multipleAsn.forEach((data) => {
                if (data.qcDateErr == true || data.qcErr == true) {
                    flag = true
                    return;
                }
            })
            if (!flag) {
                // if (this.state.poDetailsShipment.isQCDone == null) {
                //     this.setState({
                //         qcModalConfirm: true
                //     })
                // } else {
                var active = this.state.multipleAsn
                let pushD = []
                active.forEach((data) => {
                    let payload = {
                        shipmentId: data.id,
                        orderId: data.orderId,
                        status: "SHIPMENT_CONFIRMED",
                        isQC: data.isQC == null ? "FALSE" : data.isQC,
                        qcDate: data.qcDate == "" ? "" : data.qcDate + "T00:00+05:30",
                        remarks: data.remarks,
                        shipmentAdviceCode: data.shipmentAdviceCode,
                        requestedQty: data.totalRequestedQty,
                        pendingQty: data.totalPendingQty,
                        orderQty: data.poQty,
                        expectedDeliveryDate: data.shipmentRequestSelectionDate + "T00:00+05:30",
                        vendorCode: data.vendorCode,
                        vendorName: data.vendorName,
                        documentNumber: data.documentNumber,
                        shipmentPage: "ASNUNDERAPPROVAL",
                        enterpriseSite: data.siteDetails,
                        orderNumber: data.orderNumber,
                    }
                    pushD.push(payload)
                })
                let payload = {
                    shipmentStatusList: pushD,
                    shipmentCriteria: {}
                }
                this.props.shipmentConfirmCancelRequest(payload)
                // this.props.CancelShipment()

                //Calling File upload after submission::
                this.props.onFileUploadProps(active)
            }
        }, 10)
        // }
    }

    closeQc() {
        this.setState({
            qcModalConfirm: false
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getCompleteDetailShipment.isSuccess) {
            if (nextProps.shipment.getCompleteDetailShipment.data.resource != null) {
                var format = genericDateFormat(nextProps.shipment.getCompleteDetailShipment.data.resource.dateFormat)
                this.setState({
                    poDetailsShipment: nextProps.shipment.getCompleteDetailShipment.data.resource,
                    prev: nextProps.shipment.getCompleteDetailShipment.data.prePage,
                    current: nextProps.shipment.getCompleteDetailShipment.data.currPage,
                    next: nextProps.shipment.getCompleteDetailShipment.data.currPage + 1,
                    maxPage: nextProps.shipment.getCompleteDetailShipment.data.maxPage,
                    genericDateFormat: format,
                    qcFromDate: moment(nextProps.shipment.getCompleteDetailShipment.data.resource.qcFromDate, format).format("YYYY-MM-DD"),
                    qcToDate: moment(nextProps.shipment.getCompleteDetailShipment.data.resource.qcToDate, format).format("YYYY-MM-DD")
                })
            } else {
                this.setState({
                    poDetailsShipment: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.shipment.getAllComment.isSuccess) {
            if (nextProps.shipment.getAllComment.data.resource != null) {
                this.setState({
                    barcode: nextProps.shipment.getAllComment.data.resource.BARCODE,
                })
            }
        }
        if (nextProps.shipment.deleteUploads.isSuccess) {
            var multipleAsn = [...this.state.multipleAsn]
            multipleAsn.map((data) => {
                if (data.id == this.state.deletedId) {
                    data.isBarcodeUploaded = nextProps.shipment.deleteUploads.data.isBarcodeAvailable
                    if (nextProps.shipment.deleteUploads.data.isBarcodeAvailable == "FALSE")
                        this.setState({ barcodeDrop: false })
                }
            })
            this.setState({ multipleAsn })
        }
    }
    changeDate = (e) => {
        this.setState({
            valueDate: e.target.value
        });
    }

    callHandleChangeModal = (e) => {
        var scrollValue = handleChangeModal(e)
        this.setState({ rangeVal: scrollValue.rangeVal, sliderDisable: scrollValue.total == 0 ? false : true })
    }
    handleModal(e) {
        this.setState({
            qualityModal: !this.state.qualityModal
        })
    }
    handleChange = (e) => {
        if (e.target.id == "isQc") {
            this.setState({ isQc: e.target.value }, () => { this.isQcError() })
        } else if (e.target.id == "qcDate") {
            var formattedDate = moment(e.target.value, "YYYY-MM-DD").format(this.state.genericDateFormat)
            this.setState({ qcDate: e.target.value, qcDateFormatted: formattedDate }, () => { this.qcDateError() })
        }
    }
    isQcError = (id) => {
        let multipleAsn = this.state.multipleAsn
        if (id != undefined) {
            multipleAsn.map((data) => id == data.id && data.isQc == "" ? (data.qcErr = true) : (data.qcErr = false))
        } else {
            multipleAsn.map((data) => data.isQc == "" ? (data.qcErr = true) : (data.qcErr = false))
        }
        this.setState({ multipleAsn })
    }
    qcDateError = () => {
        if (this.state.isQc == "TRUE" && this.state.qcDate == "") {
            this.setState({
                qcDateErr: true
            })
        } else {
            this.setState({
                qcDateErr: false
            })
        }
    }
    changeQc(id, e) {
        let multipleAsn = this.state.multipleAsn
        multipleAsn.forEach((asn) => {
            if (asn.id == id) {
                asn.isQc = e.target.value
            }
        })
        this.setState({ multipleAsn }, () => { this.isQcError(id) })
    }
    changeDatee(id, e) {

        let multipleAsn = this.state.multipleAsn
        multipleAsn.forEach((asn) => {
            if (asn.id == id) {
                asn.qcDate = e.target.value
            }
        })
        this.setState({ multipleAsn }, () => {
            this.qcDateerror()
        })
    }
    changeRemarks(id, e) {
        let multipleAsn = this.state.multipleAsn
        multipleAsn.forEach((asn) => {
            if (asn.id == id) {
                asn.remarks = e.target.value
            }
        })
        this.setState({ multipleAsn }, () => {
            this.qcDateerror()
        })
    }
    qcDateerror() {
        let multipleAsn = this.state.multipleAsn
        multipleAsn.forEach((asn) => {
            if (asn.isQc == "TRUE" && asn.qcDate == "") {
                asn.qcDateErr = true
            } else {
                asn.qcDateErr = false
            }
        })
        this.setState({ multipleAsn })
    }
    qcError() {
        let multipleAsn = this.state.multipleAsn
        multipleAsn.forEach((asn) => {
            if (asn.isQc == "") {
                asn.qcErr = true
            } else {
                asn.qcErr = false
            }
        })
        this.setState({ multipleAsn })
    }
    onClose(event, id) {
        let multipleAsn = this.state.multipleAsn
        // multipleAsn.forEach((asn) => {
        //     if (asn.id == id) {
        //         multipleAsn.splice(asn, 1)
        //     }
        // })
        if (multipleAsn.length > 1) {
            multipleAsn = multipleAsn.filter((data) => data.id != id)
            this.setState({ multipleAsn })
        } else {
            this.setState({
                toastMsg: "Single ASN Cannot be Cancelled",
                toastLoader: true,
                loader: false
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000)
        }

    }
    fileUpload =(id, e, data)=> {
        this.setState({ loader: true })
        let values = e.target.files
        let multipleAsn = this.state.multipleAsn
        let files = Object.values(e.target.files)
        var fileNames = files.map((item, index) => Object.values(e.target.files)[index].name)
        let orderCode = ""
        let shipmentId = ""
        let orderNumber = ""
        let modulee = ""
        let subModule = ""
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        const fd = new FormData();
        multipleAsn.forEach((asn) => {
            if (asn.id == id) {
                orderCode = asn.orderCode,
                    shipmentId = asn.id,
                    orderNumber = asn.orderNumber,
                    modulee = "SHIPMENT",
                    subModule = "BARCODE"
            }
        })
        multipleAsn.forEach((asn, index) => {
            if (asn.id == id) {
                if (values[0].size < 5242880) {
                    // if (values[0].type == "text/plain" || values[0].type == "") {
                    let payload = {
                        "shipmentId": shipmentId,
                        "orderNumber": orderNumber,
                        "module": modulee,
                        "subModule": subModule,
                        "isQCAvailable": data.isQCUploaded,
                        "isInvoiceAvailable": data.isInvoiceUploaded,
                        "isBarcodeAvailable": "TRUE",
                        "orderId": data.orderId,
                        "documentNumber": data.documentNumber,
                        "commentId": asn.id,
                        "commentCode": asn.shipmentAdviceCode,
                        "vendorCode": asn.vendorCode,
                    }
                    for (let i = 0; i < files.length; i++) {
                        fd.append("files", files[i])
                    }
                    fd.append("uploadNode", JSON.stringify(payload))
                    axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
                        .then(res => {
                            this.setState({
                                loader: false
                            })
                            if (res.data.status == "2000") {
                                asn.barcode = res.data.data.resource
                                multipleAsn.map((data) => {
                                    if (data.id == id) {
                                        data.isBarcodeUploaded = asn.barcode[0].isBarcodeAvailable
                                    }
                                })
                            } else {

                            }
                            this.setState({ multipleAsn })
                        }).catch((error) => {
                            this.props.openToastError()
                            this.setState({
                                loader: false,
                                multipleAsn,
                            })
                        });
                }
                else {
                    this.setState({
                        toastMsg: "Image size must be of >5mb",
                        toastLoader: true,
                        loader: false
                    })
                    e.target.value = ""
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 10000)
                }
            }
        })
        e.target.value = ''
        this.setState({ fileNames, expandedID: data.id, multipleAsn })
    }

    deleteUploads(data, multipleAsn) {
        let barcode = this.state.barcode.filter((check) => check.fileURL != data.fileURL)
        let files = []
        let a = {
            fileName: data.fileName,
            isActive: "FALSE"
        }
        files.push(a)
        let payload = {
            shipmentId: this.state.expandedID,
            module: "SHIPMENT",
            subModule: "BARCODE",
            files: files,
            commentId: multipleAsn.id
        }
        this.setState({
            selectedFiles: files,
            barcode,
            fileNames: [],
            deletedId: multipleAsn.id
        })
        this.props.deleteUploadsRequest(payload)
    }
    dowloadBarcode = (data) => {
        if (!this.state.barcodeDrop || this.state.prevId !== data.id) {
            let payload = {
                // shipmentId: data.id,
                // module: "SHIPMENT",
                // no: 1,
                // // orderCode: data.orderCode,
                // orderNumber: data.orderNumber,
                // isAllAttachment: "TRUE",
                // subModule: "BARCODE",
                // refresh: "whole",
                // orderId: data.orderId,
                // documentNumber: data.documentNumber

                module: "SHIPMENT",
                pageNo: 1,
                subModule: "BARCODE",
                isAllAttachment: "TRUE",
                type: 1,
                search: "",
                commentId: data.id,
                commentCode: data.shipmentAdviceCode
            }
            this.props.getAllCommentRequest(payload)
            this.setState({ barcodeDrop: true, prevId: data.id, fileNames: [] })
        } else {
            this.setState({ barcodeDrop: false })
        }
        this.setState({ barcodeDrop: !this.state.barcodeDrop, expandedID: data.id })
    }

    //---------------------------------------- New File Upload ---------------------------// 
    openPpsUploadModal =(e, data, fileType)=> {
        e.preventDefault();
        this.setState({
            uploadFileType: fileType,
            ppsUploadModal: !this.state.ppsUploadModal,
            currentAsnId: data.id,
        },()=> document.addEventListener('click', this.clickClosePpsModal));
    }

    onFileUploadSubmit = (fileList, moduleName) =>{
        if(moduleName == "BARCODE"){
            let multipleAsn = this.state.multipleAsn
            multipleAsn.map( data => {
                if(data.id == this.state.currentAsnId){
                    data.barcodeArray = fileList
                    data.fileType = "BARCODE"
                }  
            })
            this.setState({
                multipleAsn,
                ppsUploadModal: false,
            },()=> document.removeEventListener('click', this.clickClosePpsModal))
        }
    }

    CancelPpsUploadModal = (e) => {
        this.setState({
            ppsUploadModal: false,
        },()=> document.removeEventListener('click', this.clickClosePpsModal))
    }

    clickClosePpsModal =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop")){
            this.setState({ ppsUploadModal: false})
        }
    }

    render() {
        const { qualityModal, sliderDisable } = this.state
        return (
            <div> <div className="modal display_block multipleAsnApprove" id="pocolorModel">
                <div className="backdrop display_block modal-backdrop-new"></div>
                <div className="display_block">
                    <div className="modal-content nmc-asn-style setAsnUnderApproveWidth">
                        <div className="nmc-head">
                            <div className="nmch-left">
                                <h3>Approve ASN Request</h3>
                            </div>
                            <div className="nmch-right">
                                {/* <button type="button" className="addCommentBtn" onClick={(e) => this.handleModal(e)}>Add Comment</button> */}
                                <button type="button" id="close" onClick={this.props.CancelShipment}>Cancel</button>
                                <button type="button" id="close" className="confirmBtnBlue" onClick={this.confirmShipment}>Confirm</button>
                            </div>
                        </div>
                        <div className="nmc-body">
                            <div className="responsive-table">
                                <div className="nmcb-inner">
                                    <table className="table modal-table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div className="nmcb-asn-detail">
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">PO NUMBER</label>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div className="nmcb-asn-detail">
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">ASN No.</label>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div className="nmcb-asn-detail">
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">ASN Date </label>
                                                        </div>
                                                    </div>
                                                </th>
                                                {/* <th>
                                                    <div className="nmcb-asn-detail">
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">QC </label>
                                                        </div>
                                                    </div>
                                                </th> */}
                                                
                                                {this.state.multipleAsn.some((_) => _.isBarcodeUploaded !== "TRUE") && < th >
                                                    <div className="nmcb-asn-detail">
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">Barcode </label>
                                                        </div>
                                                    </div>
                                                </th>}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.multipleAsn.map((data, key) => (
                                                <tr key={key}>
                                                    <td>
                                                        <div className="nmcbad-left">
                                                            <label>{data.orderNumber}</label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="nmcbad-left">
                                                            <label>{data.shipmentAdviceCode}</label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="nmcbad-left">
                                                            <label>{data.shipmentRequestDate}</label>
                                                        </div>
                                                    </td>
                                                    {/* <td>
                                                        <div className="nmcb-qc displayFlex">
                                                            <div className="posRelative displayInline">
                                                                <select id="isQc" className="genericSelect nmcbd-select" onChange={(e) => this.changeQc(data.id, e)}>
                                                                    <option value="">Select</option>
                                                                    <option value="TRUE">Required</option>
                                                                    <option value="FALSE">Not Required</option>
                                                                </select>
                                                                {data.qcErr && <span className="error m0">Select QC</span>}
                                                            </div>
                                                        </div>
                                                        <div className="nmcb-qc">
                                                            <div className="posRelative">
                                                                {data.isQc == "TRUE" && data.isQC != "" ? <input type="date" placeholder={data.qcDate != "" ? data.qcDate : "Select QC Date"} min={todayDate > data.qcFromDateSelection ? todayDate : data.qcFromDateSelection} max={data.qcToDateSelection} onChange={(e) => this.changeDatee(data.id, e)} /> : null}
                                                                {data.isQc == "FALSE" && data.isQC != "" ? <input type="text" placeholder={data.remarks != "" ? data.remarks : "Remark"} onChange={(e) => this.changeRemarks(data.id, e)} /> : null}
                                                                {data.isQc == "TRUE" && data.qcDateErr && <span className="error m0">Select QC Date</span>}
                                                            </div>
                                                        </div>
                                                    </td> */}
                                                    {(data.alreadyUploadedBarcode === "FALSE" || data.isBarcodeUploaded == null) ?
                                                    <td> 
                                                        <div className="caffc-fields">
                                                            <React.Fragment>
                                                                <div className="caffb-upload-file">
                                                                    <label onClick={(e) => this.openPpsUploadModal(e, data, "BARCODE")}>
                                                                        {data.barcodeArray !== undefined && (data.barcodeArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{data.barcodeArray.length} Files</span>)}
                                                                        <img src={require('../../../assets/attach.svg')} />
                                                                    </label>
                                                                    <span id="ppsfileLabel"></span>
                                                                </div>
                                                            </React.Fragment>
                                                            {this.state.ppsUploadModal && this.state.multipleAsn.map( item => item.id == this.state.currentAsnId && <CreateAsnUpload CancelPpsUploadModal={this.CancelPpsUploadModal} onFileUploadSubmit={this.onFileUploadSubmit} newBarcodeArray ={item.barcodeArray} {...this.state} {...this.props}/>)}
                                                        </div>
                                                        {/* <div className="nmcd-upload-icon uploadInvoiceLeft posRelative">
                                                            <div className="bottomToolTip displayInline">
                                                                <label className="file width115px customInput m0 hoverMe displayPointer">
                                                                    <input type="file" id="fileUpload" onChange={(e) => this.fileUpload(data.id, e, data)} multiple="multiple" />
                                                                    <span >Choose file</span>
                                                                    <img src={fileIcon} />
                                                                </label>
                                                                <div className="fileUploads">
                                                                    {this.state.fileNames.length != 0 && data.id == this.state.expandedID && <label>{this.state.fileNames.length}-Files Uploaded</label>}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="uploadInvoiceRight">
                                                            {data.isBarcodeUploaded == "TRUE" ? <button type="button" className="removeAfter eye-icon-new" onClick={(e) => this.dowloadBarcode(data)}><span className="tooltip-shipment-asn">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                    <g id="prefix__eye" transform="translate(-4.736 -19.47)">
                                                                        <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                        <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#000" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                    </g>
                                                                </svg>
                                                                <span className="tooltip-content-shipment">View Attachment</span></span></button>
                                                                : <button type="button" className="removeAfter eye-icon-new pointerNone"><span className="tooltip-shipment-asn">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                        <g id="prefix__eye" fill="#ccd8ed" transform="translate(-4.736 -19.47)">
                                                                            <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                            <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#ccd8eds" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                        </g>
                                                                    </svg>
                                                                    <span className="tooltip-content-shipment">View Attachment</span></span></button>}
                                                            {this.state.barcodeDrop && data.id == this.state.expandedID && <div className="filesDownloadDrop ">
                                                                <div className="fileUploads">
                                                                    {this.state.barcode.length != 0 ? this.state.barcode.map((hdata, key) => (<React.Fragment><div className="asn-uploaded-file"><div className="asn-up-file-name"><p key={key} className="displayLine" onClick={() => window.open(hdata.fileURL)}>{hdata.fileName}</p></div><span className="displayPointer" onClick={(e) => this.deleteUploads(hdata, data)}>&#10005;</span></div></React.Fragment>))
                                                                        : <label>No Data Found</label>}
                                                                </div>
                                                            </div>}
                                                        </div> */}
                                                    </td> : <td><div className="nmcbad-left"><label>Barcode Uploaded</label></div></td>} 
                                                    <td>
                                                        <span className="nmcb-close-btn"><img src={Close} className="height30px displayPointer" onClick={(e) => this.onClose(e, data.id)}></img></span>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}

                {/* {qualityModal ? <QualityCheckModalEnterprise {...this.state} {...this.props} displayDetails={true} qcUpload={this.state.qcUpload} {...this.state} {...this.props} poId={this.props.poId} handleModal={(e) => this.handleModal(e)} shipmentId={this.props.shipmentId} poDetailsShipment={this.state.poDetailsShipment} /> : null} */}
                {this.state.qcModalConfirm ? <QcDateConfirmModal {...this.state} {...this.props} closeQc={(e) => this.closeQc(e)} CancelShipment={this.props.CancelShipment} /> : null}
            </div>
            </div >

        )
    }
}
