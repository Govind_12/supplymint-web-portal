import React, { Component } from 'react'
import axios from 'axios';
import { CONFIG } from "../../../config/index";
export default class ConfirmInspDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            ppsDownloadModal: false,
            fileType:"",
            files:[],
            allowPackingList: this.props.allowPackingList,
        }
    }
    openPpsDownloadModal(e,type,fileArray) {
        e.preventDefault();
        this.setState({
            fileType:type,
            files:fileArray,
            ppsDownloadModal: !this.state.ppsDownloadModal
        });
    }
    closePpsDownloadModal = (e) => {
        this.setState({
            fileType:"",
            files:[],
            ppsDownloadModal: false,
        })
    }
    
    
    /* componentDidUpdate(preProps,preState){
        const { asnDetails } = this.props
        console.log(asnDetails,"asnDetails")
         if(asnDetails.isPPSAvailable){
            let payload = {
                shipmentId: asnDetails.shipmentId,
                module: "SHIPMENT",
                pageNo: 1,
                // orderCode: asnDetails.orderCode,
                orderNumber: asnDetails.orderNumber,
                isAllAttachment: "TRUE",
                subModule: "PPS",
                refresh: "whole",
                orderId: asnDetails.orderId,
                documentNumber: asnDetails.documentNumber
            }
            let headers = {
                'X-Auth-Token': sessionStorage.getItem('token'),
                'Content-Type': 'application/json'
            }
            axios.post(`${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/find/all/comqcinvoice`, payload, { headers: headers })
            .then(res => {
                this.setState({
                    oldPpsFiles: res.data.data.resource.PPS
                })
                
            }).catch((error) => {
                this.setState({
                    toastError: true
                })
            })
        }
        if(asnDetails.isTestReportAvailable){
            let payload = {
                shipmentId: asnDetails.shipmentId,
                module: "SHIPMENT",
                pageNo: 1,
                // orderCode: asnDetails.orderCode,
                orderNumber: asnDetails.orderNumber,
                isAllAttachment: "TRUE",
                subModule: "REPORT",
                refresh: "whole",
                orderId: asnDetails.orderId,
                documentNumber: asnDetails.documentNumber
            }
            let headers = {
                'X-Auth-Token': sessionStorage.getItem('token'),
                'Content-Type': 'application/json'
            }
            axios.post(`${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/find/all/comqcinvoice`, payload, { headers: headers })
            .then(res => {
                this.setState({
                    oldReportFiles: res.data.data.resource.REPORT
                })
                
            }).catch((error) => {
                this.setState({
                    toastError: true
                })
            })
        }
    } */
    render() {
        //console.log(this.props.asnDetails)
        const { asnDetails,oldReportFiles,oldPpsFiles,oldPackingFiles } = this.props
        return(
            <div className="confirm-insp-details">
                <div className="cid-head-details">
                    <img onClick={this.props.CloseMoreDetails} src={require('../../../assets/clearSearch.svg')} />
                    <h3>More Details</h3>
                </div>
                <div className="cid-body">
                    <div className="cidb-row">
                        <div className="cidbr-inner">
                            <h5>Expected Delivery Date</h5>
                        <span className="cidbri-date">{asnDetails.shipmentDate}</span>
                        </div>
                        <div className="cidbr-inner">
                            <h5>Inspection From Date</h5>
                            <span className="cidbri-date">{asnDetails.qcFromDate}</span>
                        </div>
                        <div className="cidbr-inner">
                            <h5>Inspection To Date</h5>
                            <span className="cidbri-date">{asnDetails.qcToDate}</span>
                        </div>
                    </div>
                    <div className="cidb-row">
                        <div className="cidbr-inner">
                            <h5>Contact Person</h5>
                            <span className="cidbri-date">{asnDetails.contactPerson ? asnDetails.contactPerson : "Not Available"}</span>
                        </div>
                        <div className="cidbr-inner">
                            <h5>Contact Number</h5>
                            <span className="cidbri-date">{asnDetails.contactNo ? asnDetails.contactNo : "Not Available"}</span>
                        </div>
                    </div>
                    <div className="cidb-row">
                        <div className="cidbr-inner">
                            <h5>Address</h5>
                            <span className="cidbri-date">{asnDetails.address ? asnDetails.address : "Not Available"}</span>
                            <span className="generic-tooltip">{asnDetails.address ? asnDetails.address : "Not Available"}</span>
                            {/* <span className="cidbri-date">Lorem ipsum dolor sit amet</span>
                            <span className="cidbri-addr">New Delhi, New Delhi</span> */}
                        </div>
                        <div className="cidbr-inner">
                            <h5>Location</h5>
                            <span className="cidbri-date">{asnDetails.location ? asnDetails.location : "Not Available"}</span>
                            <span className="generic-tooltip">{asnDetails.location ? asnDetails.location : "Not Available"}</span>
                            {/* <span className="cidbri-date">Lorem ipsum dolor sit amet</span>
                            <span className="cidbri-mail">ramesh001@gmail.com</span> */}
                        </div>
                    </div>
                </div>
                <div className="cid-footer">
                    <div className="cidf-inner">
                        <h4>Attachments</h4>
                        <span className="cidfi-button" onClick={(e) => this.openPpsDownloadModal(e,"PPS",oldPpsFiles)}>
                            <label>PPS Available</label>
                            <span className="cidf-download">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 21.5 19.926">
                                    <g data-name="download (3)">
                                        <g data-name="Group 2115">
                                            <path d="M20.884 9.714a.613.613 0 0 0-.616.616v5.6a2.764 2.764 0 0 1-2.761 2.761H3.993a2.764 2.764 0 0 1-2.761-2.761v-5.695a.616.616 0 1 0-1.232 0v5.695a4 4 0 0 0 3.993 3.993h13.515a4 4 0 0 0 3.993-3.993v-5.6a.616.616 0 0 0-.617-.616z" data-name="Path 424"/>
                                            <path d="M10.317 15.035a.62.62 0 0 0 .434.183.6.6 0 0 0 .433-.183l3.915-3.915a.617.617 0 0 0-.872-.872l-2.861 2.866V.614a.616.616 0 0 0-1.232 0v12.5L7.269 10.25a.617.617 0 0 0-.872.872z" data-name="Path 425"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                        </span>
                        <span className="cidfi-button" onClick={(e) => this.openPpsDownloadModal(e,"REPORT",oldReportFiles)}>
                            <label>Test Report</label>
                            <span className="cidf-download">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 21.5 19.926">
                                    <g data-name="download (3)">
                                        <g data-name="Group 2115">
                                            <path d="M20.884 9.714a.613.613 0 0 0-.616.616v5.6a2.764 2.764 0 0 1-2.761 2.761H3.993a2.764 2.764 0 0 1-2.761-2.761v-5.695a.616.616 0 1 0-1.232 0v5.695a4 4 0 0 0 3.993 3.993h13.515a4 4 0 0 0 3.993-3.993v-5.6a.616.616 0 0 0-.617-.616z" data-name="Path 424"/>
                                            <path d="M10.317 15.035a.62.62 0 0 0 .434.183.6.6 0 0 0 .433-.183l3.915-3.915a.617.617 0 0 0-.872-.872l-2.861 2.866V.614a.616.616 0 0 0-1.232 0v12.5L7.269 10.25a.617.617 0 0 0-.872.872z" data-name="Path 425"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                        </span>
                        {this.state.allowPackingList && <span className="cidfi-button" onClick={(e) => this.openPpsDownloadModal(e,"PACKAGING",oldPackingFiles)}>
                            <label>Packing List</label>
                            <span className="cidf-download">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 21.5 19.926">
                                    <g data-name="download (3)">
                                        <g data-name="Group 2115">
                                            <path d="M20.884 9.714a.613.613 0 0 0-.616.616v5.6a2.764 2.764 0 0 1-2.761 2.761H3.993a2.764 2.764 0 0 1-2.761-2.761v-5.695a.616.616 0 1 0-1.232 0v5.695a4 4 0 0 0 3.993 3.993h13.515a4 4 0 0 0 3.993-3.993v-5.6a.616.616 0 0 0-.617-.616z" data-name="Path 424"/>
                                            <path d="M10.317 15.035a.62.62 0 0 0 .434.183.6.6 0 0 0 .433-.183l3.915-3.915a.617.617 0 0 0-.872-.872l-2.861 2.866V.614a.616.616 0 0 0-1.232 0v12.5L7.269 10.25a.617.617 0 0 0-.872.872z" data-name="Path 425"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                        </span>}
                    </div>
                </div>
                {this.state.ppsDownloadModal && <div className="confirm-insp-details pps-download-drop">
                    <div className="cid-head-details">
                        <img onClick={(e) => this.closePpsDownloadModal(e)} src={require('../../../assets/clearSearch.svg')} />
                        <h3>Download {this.state.fileType == "PPS" ? "PPS": this.state.fileType == "REPORT" ? "Test Report" : "Packing List"} Document</h3>
                    </div>
                    <div className="cid-footer">
                        <div className="cidf-inner">
                        {
                            this.state.files.length != 0 ?
                            this.state.files.map((data,index)=>{
                                return (
                                    <span key={index} className="cidfi-button" >
                                        <label>{data.fileName}</label>
                                        <span className="cidf-download" onClick={() => window.open(data.fileURL)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 21.5 19.926">
                                                <g data-name="download (3)">
                                                    <g data-name="Group 2115">
                                                        <path d="M20.884 9.714a.613.613 0 0 0-.616.616v5.6a2.764 2.764 0 0 1-2.761 2.761H3.993a2.764 2.764 0 0 1-2.761-2.761v-5.695a.616.616 0 1 0-1.232 0v5.695a4 4 0 0 0 3.993 3.993h13.515a4 4 0 0 0 3.993-3.993v-5.6a.616.616 0 0 0-.617-.616z" data-name="Path 424"/>
                                                        <path d="M10.317 15.035a.62.62 0 0 0 .434.183.6.6 0 0 0 .433-.183l3.915-3.915a.617.617 0 0 0-.872-.872l-2.861 2.866V.614a.616.616 0 0 0-1.232 0v12.5L7.269 10.25a.617.617 0 0 0-.872.872z" data-name="Path 425"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                )
                            })
                        :null}
                        </div>
                    </div>
                </div>}
            </div>
        )
    }
}