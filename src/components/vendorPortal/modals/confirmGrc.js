import React from "react";
import SetTag from '../../../assets/set-tag.svg';
import NonSetTag from '../../../assets/non-set-tag.svg';

class ConfirmGrc extends React.Component {
    constructor(props) {
        super(props);
         
    }
    componentDidMount() {

    }

    render() {
        // For set level and item level asn we need to show different headers
        var itemDetails = Object.keys(this.props.checkedItem).length > 0 && this.props.checkedItem[0].poType === "poicode" ? true : false
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content grc-confirmation-modal">
                    <div className="grccm-head">
                        <h3>Confirm GRC</h3>
                        <div className="grccmh-btn">
                            {this.props.validateInwardFlag ? <button type="button" className="grc-confirm-btn" onClick={this.props.confirmGrc}>Confirm</button>:
                               <button type="button" className="grc-confirm-btn btnDisabled">Confirm</button>}
                            <button type="button" onClick={this.props.handleConfirmGrc}>Discard</button>
                        </div>
                    </div>
                    <div className="grccm-body">
                        <div className="grccm-asn-no">
                            {this.props.checkedItem[0] != undefined && <table>
                                <tr><td><h4>PO NUMBER</h4></td><td style={{paddingLeft: "10px", fontSize: "1px"}}><h4>ASN NO.</h4></td></tr>
                                <tr><td id="openMinimize"><span>{this.props.checkedItem[0].orderNumber}</span></td>
                                    <td id="openMinimize" style={{paddingLeft: "10px"}}><span>{this.props.checkedItem[0].shipmentAdviceNo}</span></td>
                                    <td id="openMinimize" style={{paddingLeft: "10px"}}>{this.props.checkedItem[0].poType === "adhoc" ? <img src={SetTag} />
                                    : <img src={NonSetTag} />}</td>
                                </tr>
                            </table>}  
                            {/* <h4>{this.props.checkedItem[0] != undefined && this.props.checkedItem[0].shipmentAdviceNo}</h4> */}
                            {/* {this.props.checkedItem[0] != undefined && (this.props.checkedItem[0].poType === "adhoc" ? <img src={SetTag} />
                                : <img src={NonSetTag} />)} */}
                            {/* <img src={SetTag} />  */}
                            {/* <img src={NonSetTag} /> */}
                        </div>
                        <div className="grccmb-table">
                            <table className="table">
                                <thead>
                                    {itemDetails ? <tr>
                                        <th><label className="bold">ICode</label></th>
                                        <th><label>ICode Qty</label></th>
                                        <th><label>Inward Qty</label></th>
                                        <th><label>Reject Qty</label></th>
                                        <th><label>Pending Qty</label></th>
                                        <th><label>Remarks</label></th>
                                        <th><label>Inwarded Qty</label></th>
                                        <th><label>Rejected Qty</label></th>
                                    </tr> : <tr>
                                            <th><label className="bold">Set Barcode</label></th>
                                            <th><label>Set Barcode Qty</label></th>
                                            <th><label>Inward Qty</label></th>
                                            <th><label>Reject Qty</label></th>
                                            <th><label>Pending Qty</label></th>
                                            <th><label>Remarks</label></th>
                                            <th><label>Inwarded Qty</label></th>
                                            <th><label>Rejected Qty</label></th>
                                        </tr>}
                                </thead>
                                <tbody>
                                    {this.props.grcDetails.length > 0 && this.props.grcDetails.map((_, key) => (
                                        <tr key={key}>
                                            <td><label>{itemDetails ? _.itemCode : _.setBarcode}</label></td>
                                            <td><label>{_.qty}</label></td>
                                            <td><label><input type="number" id={"inwardedQty"+key} value={_.inwardQty == 0 ? "" : _.inwardQty} onChange={(e) => this.props.handleQtyChange(e, _, key)} placeholder="0"/></label></td>
                                            <td><label><input type="number" id={"rejectedQty"+key} onChange={(e) => this.props.handleQtyChange(e, _, key)} placeholder="0"/></label></td>
                                            <td><label><input className="grc-dis-input" type="text" disabled value={Number(_.pendingQty)} /></label></td>
                                            <td><label><input className="grc-remark-input" type="text" id={"remarks"+key} onChange={(e) => this.props.handleQtyChange(e, _, key)} value={_.remarks} /></label></td>
                                            <td><label><input className="grc-dis-input" type="text" value={Number(_.inwardedQty)} id={"alreadyInwardedQty"+key} disabled/></label></td>
                                            <td><label><input className="grc-dis-input" type="text" value={Number(_.rejectedQty)} id={"alreadyRejectedQty"+key} disabled/></label></td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                        <div className="grcm-bottom">
                            <div className="grcmb-inner">
                                <span className="grcmb-inner-total-no">Total No <span className="bold">{this.props.grcDetails.length}</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ConfirmGrc;