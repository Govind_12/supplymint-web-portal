import React from "react";
import Tick from '../../../assets/tick.svg';
import InfoIcon from '../../../assets/info-icon.svg';
import { OrderedMap } from "immutable";


// const TRACK_DATA = new OrderedMap({
//     pendingPO: {
//         key: "pendingPO",
//         disableStatus: "Not Received",
//         label: "Order",
//         number: "1",
//         requestedDateLabel: "Received On",
//         uuid: "documentNumber"
//     },
//     asnReq: {
//         key: "asnReq",
//         disableStatus: "ASN Pending for Approval",
//         label: "ASN",
//         number: "2",
//         requestedDateLabel: "Requested On",
//         uuid: "shipmentAdviceCode",
//         revised: "revisedAsn",
//         revisedAvailableBtn: "REV",
//         revisedNotAvalible: "NEW",
//     },
//     qcSummary: {
//         key: "qcSummary",
//         disableStatus: "Quality Check Pending",
//         label: "Quality Check",
//         number: "3",
//         requestedDateLabel: "Requested QC Date",
//         uuid: "shipmentAdviceCode",
//         revised: "revisedQC",
//         revisedAvailableBtn: "RE-QC",
//         revisedNotAvalible: "RE",
//         actualDate: "actualQCDateTime"
//     },
//     shipmentProcessing: {
//         key: "shipmentProcessing",
//         disableStatus: "Shipment pending from Supplier",
//         label: "Shipment Processed",
//         number: "4",
//         requestedDateLabel: "Shipment Confirm on",
//         uuid: "shipmentAdviceCode"
//     },
//     lrProcessing: {
//         key: "lrProcessing",
//         disableStatus: "LR Creation Pending",
//         label: "LR Processed",
//         number: "5",
//         requestedDateLabel: "LR Date",
//         uuid: "shipmentAdviceCode"
//     },
//     goodIntransit: {
//         key: "goodIntransit",
//         disableStatus: "Shipment is Intransit",
//         label: "Intransit / Delivered",
//         number: "6",
//         requestedDateLabel: "Last Updated on",
//         uuid: "shipmentAdviceCode"
//     }

// })
// const ASN_DATA = new OrderedMap({
//     rev: {
//         label: "Rev",
//         dataKey: "key"
//     },
//     date: {
//         label: "Date",
//         dataKey: "time"
//     },
//     asnQty: {
//         label: "ASN Quantity",
//         dataKey: "asnQty"
//     },
//     revQty: {
//         label: "Revised Quantity",
//         dataKey: "asnRevisedQty"
//     }
// })

class ShipmentTracking extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            moreDetails: false,
            lRMoreDetails: false,
        }
    }
    openMoreDetails(e) {
        e.preventDefault();
        this.setState({
            moreDetails: !this.state.moreDetails
        });
    }
    openLRMoreDetails(e) {
        e.preventDefault();
        this.setState({
            lRMoreDetails: !this.state.lRMoreDetails
        });
    }
    render() {
        const { shipmentTrackingData } = this.props
        console.log(shipmentTrackingData)
        const qcStatus = shipmentTrackingData.qcSummary != undefined && shipmentTrackingData.qcSummary.revisedHistory != null && shipmentTrackingData.qcSummary.revisedHistory.filter((_) => (_.qcStatus == "Pass"));
        return (
            <div className="modal" id="shipmenttrackingmodal">
                <div className="backdrop modal-backdrop-new"></div>
                {Object.keys(this.props.shipmentTrackingData).length != 0 && <div className="modal-content shipment-tracking-modal">
                    <div className="col-lg-12 pad-0">
                        <div className="stm-head">
                            <h3>Track Order</h3>
                            <div className="stmh-inner">
                                <p>Last Updated</p>
                                <span className="stmhi-time" ><span className="stmhit-date" >{shipmentTrackingData.lastUpdatedTime}</span></span>
                                <h4>Order Number : {shipmentTrackingData.pendingPO.orderNumber}	</h4>
                            </div>
                            <button type="button" className="stm-close-btn" onClick={this.props.shipmentTrackModal}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                    <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="shipment-tracking">
                        {/* Shipment New Design With Functionality */}
                        {/* {TRACK_DATA.map((_, k) => <div className="col-lg-12 pad-0">
                            <div className="st-head">
                                <h5>{_.label}</h5>
                                {shipmentTrackingData[k].flag == "true" && k != "qcSummary" ? <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span> :
                                    k != "qcSummary" && <span className="sth-circle bg-fff sth-num"></span>}
                                {k == "qcSummary" && (shipmentTrackingData[k].qcStatus == "" || shipmentTrackingData[k].qcStatus == undefined ? <span className="sth-circle bg-fff sth-num">{_.number}</span> : shipmentTrackingData[k].qcStatus == "reQC" ? <span className="sth-circle bg-org pad99"><img src={InfoIcon} /></span> : 
                                    <span className="sth-circle bg-green">
                                         <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                            <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                                <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                                <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                            </g> 
                                        </svg>
                                    </span>)}
                                <div className="stm-body">
                                    <h3>{_.label}</h3>
                                    <span className="stmb-number">{shipmentTrackingData[k][_.uuid]}	| </span><span className="stmb-msg"> Order has been created successfully</span>
                                    <span className="stmb-item"><span>Created on</span> <span>{shipmentTrackingData[k].updatedTime == "" ? "NA" : shipmentTrackingData[k].updatedTime}</span></span>
                                    <span className="stmb-item">
                                        <span>Created by</span>
                                        <span className="stmbi-name">{shipmentTrackingData[k].upn == "" ? "NA" : shipmentTrackingData[k].createdBy}</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        ).toList()} */}
                        {/* New Shipment End */}
                        {/* <div className="sth-inner-no">
                            <span className="sth-vno">{shipmentTrackingData[k][_.uuid]}</span>
                            {_.revised && shipmentTrackingData[k].flag == "true" && <button type="button" className={_.revised + "track"}>{shipmentTrackingData[k].revisedHistory == null ? _.revisedNotAvalible : _.revisedAvailableBtn}</button>}
                            {_.revised == "revisedAsn" && shipmentTrackingData[k].revisedHistory != null && <div className="tooltip sth-lable-dropd"><h5>ASN Revision history</h5>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Rev</th>
                                            <th>Date</th>
                                            <th>ASN Quantity</th>
                                            <th>Revised Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {shipmentTrackingData[k].revisedHistory.map((__, key) =>
                                            <tr key={key}>
                                                <td>R{key + 1}</td>
                                                <td>{__.time}</td>
                                                <td>{__.asnQty}</td>
                                                <td>{__.asnRevisedQty}</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>}
                            {_.revised == "revisedQC" && shipmentTrackingData[k].revisedHistory != null && <div className="tooltip sth-lable-dropd"><h5>QC Revision history</h5>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Rev</th>
                                            <th>Actual Date</th>
                                            <th>QC Date </th>
                                            <th>Comments</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {shipmentTrackingData[k].revisedHistory.map((__, key) =>
                                            <tr key={key}>
                                                <td>R{key + 1}</td>
                                                <td>{__.time}</td>
                                                <td>{__.actualQCDate}</td>
                                                <td>{__.qcRemarks}</td>
                                                <td>{__.qcStatus}</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>}
                        </div>
                        
                        <div className="st-content">
                            <div className="stc-head">
                                <span>Status</span>
                                <h3>{k == "qcSummary" ? shipmentTrackingData[k].qcStatus : shipmentTrackingData[k].flag == "true" ? shipmentTrackingData[k].status : _.disableStatus}</h3>
                            </div>
                            <div className="stb-status">
                                <span className="stb-rstatus">{_.requestedDateLabel}</span>
                                <p className="stbs-time">{shipmentTrackingData[k].time == "" ? "NA" : shipmentTrackingData[k].time}</p>
                            </div>
                            {_.actualDate && <div className="stb-status">
                                <span className="stb-rstatus">Actual QC Date</span>
                                <p className="stbs-time">{shipmentTrackingData[k].actualQCDateTime == "" ? "NA" : shipmentTrackingData[k].actualQCDateTime}</p>
                            </div>}
                            <div className="stb-status">
                                <span className="stb-rstatus">Last Updated on</span>
                                <p className="stbs-time">{shipmentTrackingData[k].updatedTime == "" ? "NA" : shipmentTrackingData[k].updatedTime}</p>
                            </div>
                            <div className="stb-status">
                                <span className="stb-rstatus">Created by</span>
                                <p className="stbs-time">{shipmentTrackingData[k].upn == "" ? "NA" : shipmentTrackingData[k].createdBy}</p>
                            </div>
                        </div> */}
                        {/* New Design Of shipment Tracking */}
                        <div className="col-lg-12 pad-0 m-top-15">
                            { shipmentTrackingData.pendingPO.flag == 'true' ? <div className="st-head">
                                <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>  
                                <div className="stm-body">
                                    <h3>Order</h3>
                                    <span className="stmb-number">{shipmentTrackingData.pendingPO.orderNumber}</span>
                                    <span className="stmb-msg"> |  Order has been created successfully</span>
                                    <div className="stmb-details">
                                        <span className="stmb-item"> 
                                            <span>Created on</span>
                                            <span>{shipmentTrackingData.pendingPO.time}</span>                                                                                                                                                                                                                                                                                                                                                                                                                          
                                        </span>
                                        <span className="stmb-item">
                                            <span>Created by</span>
                                            <span className="stmbi-name">{shipmentTrackingData.pendingPO.createdBy}</span>
                                        </span>
                                    </div> 
                                </div> 
                            </div> :
                            <div className="st-head st-border-nono">
                                <span className="sth-circle bg-yellow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                  <h3>Order</h3>
                                  {/* <span className="stmb-number">{shipmentTrackingData.pendingPO.shipmentAdviceCode}</span> */}
                                  <span className="stmb-msg stmbu-msg">Order waiting to be done.</span> 
                                </div>
                            </div> }
                        </div> 
                        <div className="col-lg-12 pad-0 m-top-15">
                            { shipmentTrackingData.asnReq.flag == 'true' ? <div className="st-head">
                                <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>  
                                <div className="stm-body">
                                    <h3>ASN</h3>
                                    <span className="stmb-number">{shipmentTrackingData.asnReq.shipmentAdviceCode}</span>
                                    <span className="stmb-msg"> |  ASN has been created successfully</span>
                                    <div className="stmb-details">
                                        <span className="stmb-item"> 
                                            <span>Created on</span>
                                            <span>{shipmentTrackingData.asnReq.time}</span>                                                                                                                                                                                                                                                                                                                                                                                                                          
                                        </span>
                                        <span className="stmb-item">
                                            <span>Created by</span>
                                            <span className="stmbi-name">{shipmentTrackingData.asnReq.createdBy}</span>
                                        </span>
                                    </div> 
                                </div> 
                            </div> :
                            <div className="st-head st-border-nono">
                                <span className="sth-circle bg-yellow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                  <h3>ASN</h3>
                                  {/* <span className="stmb-number">{shipmentTrackingData.asnReq.shipmentAdviceCode}</span> */}
                                  <span className="stmb-msg stmbu-msg">ASN waiting to be done.</span> 
                                </div>
                            </div> }
                        </div>
                        <div className="col-lg-12 pad-0 m-top-15">
                            { shipmentTrackingData.qcSummary.flag == 'true' ? <div className="st-head">
                                <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                    <h3>Pending Inspection</h3>
                                    <span className="stmb-number">{shipmentTrackingData.qcSummary.shipmentAdviceCode}	</span><span className="stmb-msg">|  Order has been created successfully</span>
                                    <div className="stmb-more-details">
                                        <button type="button" className="stmbmd-details" onClick={(e) => this.openMoreDetails(e)}>
                                        {this.state.moreDetails === true ? <img src={require('../../../assets/rounded-min.svg')} /> : <img src={require('../../../assets/rounded-add.svg')} /> }
                                            <span>More Details</span></button>
                                        {this.state.moreDetails &&
                                        <div className="stmbmd-more-d">
                                            <div className="stmbmd-left">
                                                <img src={require('../../../assets/correct.svg')} />
                                                <h5>Inspection requested</h5>
                                                <span className="stmb-item"><span>Created on</span><span>{shipmentTrackingData.qcSummary.time}</span></span>
                                                <span className="stmb-item"><span>Created By</span><span className="stmbi-name">{shipmentTrackingData.qcSummary.createdBy}</span></span>
                                            </div>
                                            <div className="stmbmd-right">
                                                {(shipmentTrackingData.qcSummary.revisedHistory !== null && shipmentTrackingData.qcSummary.revisedHistory[0].flag == 'true') ? <img src={require('../../../assets/correct.svg')} /> : <img src={require('../../../assets/warningNew2.svg')} /> }
                                                <h5>Inspection required</h5>
                                                <span className="stmb-item"><span>Created on</span><span>{shipmentTrackingData.qcSummary.revisedHistory !== null && (shipmentTrackingData.qcSummary.revisedHistory[0].time != "" || shipmentTrackingData.qcSummary.revisedHistory[0].time != null) ? shipmentTrackingData.qcSummary.revisedHistory[0].time : "NA"}</span></span>
                                                <span className="stmb-item"><span>Created By</span><span className="stmbi-name">{shipmentTrackingData.qcSummary.revisedHistory !== null && (shipmentTrackingData.qcSummary.revisedHistory[0].createdBy != "" || shipmentTrackingData.qcSummary.revisedHistory[0].createdBy != null) ? shipmentTrackingData.qcSummary.revisedHistory[0].createdBy : "NA"}</span></span>
                                            </div>
                                        </div>}
                                    </div>
                                </div>
                            </div> :
                            <div className="st-head st-border-nono">
                                <span className="sth-circle bg-yellow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                  <h3>Pending Inspection</h3>
                                  {/* <span className="stmb-number">{shipmentTrackingData.qcSummary.shipmentAdviceCode}</span> */}
                                  <span className="stmb-msg stmbu-msg">Order waiting to be done.</span> 
                                </div>
                            </div> }
                        </div>
                        <div className="col-lg-12 pad-0 m-top-15">
                            { shipmentTrackingData.shipmentProcessing.flag == 'true' ? <div className="st-head">
                                <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>  
                                <div className="stm-body">
                                    <h3>Shipment</h3>
                                    <span className="stmb-number">{shipmentTrackingData.shipmentProcessing.shipmentAdviceCode}</span>
                                    <span className="stmb-msg"> |  Shipment has been created successfully</span>
                                    <div className="stmb-details">
                                        <span className="stmb-item"> 
                                            <span>Created on</span>
                                            <span>{shipmentTrackingData.shipmentProcessing.time}</span>                                                                                                                                                                                                                                                                                                                                                                                                                          
                                        </span>
                                        <span className="stmb-item">
                                            <span>Created by</span>
                                            <span className="stmbi-name">{shipmentTrackingData.shipmentProcessing.createdBy}</span>
                                        </span>
                                    </div> 
                                </div> 
                            </div> :
                            <div className="st-head st-border-nono">
                                <span className="sth-circle bg-yellow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                  <h3>Shipment</h3>
                                  {/* <span className="stmb-number">{shipmentTrackingData.shipmentProcessing.shipmentAdviceCode}</span> */}
                                  <span className="stmb-msg stmbu-msg">Shipment waiting to be done.</span> 
                                </div>
                            </div> }
                        </div>
                        <div className="col-lg-12 pad-0 m-top-15">
                            {shipmentTrackingData.lrProcessing.flag == 'true' ? <div className="st-head">
                                <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                    <h3>LR Processed</h3>
                                    <span className="stmb-number">{shipmentTrackingData.lrProcessing.shipmentAdviceCode}</span><span className="stmb-msg"> |  LR creation has been done successfully</span>
                                    <div className="stmb-more-details">
                                        <button type="button" className="stmbmd-details" onClick={(e) => this.openLRMoreDetails(e)}>
                                        {this.state.lRMoreDetails === true ? <img src={require('../../../assets/rounded-min.svg')} /> : <img src={require('../../../assets/rounded-add.svg')} /> }
                                            <span>More Details</span></button>
                                        {this.state.lRMoreDetails &&
                                        <div className="stmbmd-more-d">
                                            <div className="stmbmd-left">
                                                <img src={require('../../../assets/correct.svg')} />
                                                <h5>Pending Approval </h5>
                                                <span className="stmb-item"><span>Created on</span><span>{shipmentTrackingData.lrProcessing.time}</span></span>
                                                <span className="stmb-item"><span>Created By</span><span className="stmbi-name">{shipmentTrackingData.lrProcessing.createdBy}</span></span>
                                            </div>
                                            <div className="stmbmd-right">
                                            {(shipmentTrackingData.lrProcessing.revisedHistory !== null && shipmentTrackingData.lrProcessing.revisedHistory[0].flag == 'true') ? <img src={require('../../../assets/correct.svg')} /> : <img src={require('../../../assets/warningNew2.svg')} /> }
                                                <h5>Approved</h5>
                                                <span className="stmb-item"><span>Created on</span><span>{shipmentTrackingData.lrProcessing.revisedHistory !== null && (shipmentTrackingData.lrProcessing.revisedHistory[0].time != "" || shipmentTrackingData.lrProcessing.revisedHistory[0].time != null) ? shipmentTrackingData.lrProcessing.revisedHistory[0].time : "NA"}</span></span>
                                                <span className="stmb-item"><span>Created By</span><span className="stmbi-name">{shipmentTrackingData.lrProcessing.revisedHistory !== null && (shipmentTrackingData.lrProcessing.revisedHistory[0].createdBy != "" || shipmentTrackingData.lrProcessing.revisedHistory[0].createdBy != null) ? shipmentTrackingData.lrProcessing.revisedHistory[0].createdBy : "NA"}</span></span>
                                            </div>
                                        </div>}
                                    </div> 
                                </div>
                            </div> :
                             <div className="st-head st-border-nono">
                                <span className="sth-circle bg-yellow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                             <div className="stm-body">
                               <h3>LR Processed</h3>
                               {/* <span className="stmb-number">{shipmentTrackingData.lrProcessing.shipmentAdviceCode}</span> */}
                               <span className="stmb-msg stmbu-msg">LR creation waiting to be done.</span> 
                             </div>
                         </div> }
                        </div>
                        <div className="col-lg-12 pad-0 m-top-15">
                            { shipmentTrackingData.goodIntransit.flag == 'true' ? <div className="st-head">
                                <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>  
                                <div className="stm-body">
                                    <h3>Goods In-transit</h3>
                                    <span className="stmb-number">{shipmentTrackingData.goodIntransit.logisticNo}</span>
                                    <span className="stmb-msg"> | Shipment shipped and reached to the destination.</span>
                                    <div className="stmb-details">
                                        <span className="stmb-item"> 
                                            <span>Created on</span>
                                            <span>{shipmentTrackingData.goodIntransit.time}</span>                                                                                                                                                                                                                                                                                                                                                                                                                          
                                        </span>
                                        <span className="stmb-item">
                                            <span>Created by</span>
                                            <span className="stmbi-name">{shipmentTrackingData.goodIntransit.createdBy}</span>
                                        </span>
                                    </div> 
                                </div> 
                            </div> :
                            <div className="st-head st-border-nono">
                                <span className="sth-circle bg-yellow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                  <h3>Goods In-transit</h3>
                                  {/* <span className="stmb-number">{shipmentTrackingData.goodIntransit.logisticNo}</span> */}
                                  <span className="stmb-msg stmbu-msg">Goods In-transit waiting to be done.</span> 
                                </div>
                            </div> }
                        </div>
                        <div className="col-lg-12 pad-0 m-top-15">
                            { shipmentTrackingData.gateEntry.flag == 'true' ? <div className="st-head">
                                <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>  
                                <div className="stm-body">
                                    <h3>Gate Entry</h3>
                                    <span className="stmb-number">{shipmentTrackingData.gateEntry.gateEntryNo}</span>
                                    <span className="stmb-msg"> | Shipment reached to the gate entry.</span>
                                    <div className="stmb-details">
                                        <span className="stmb-item"> 
                                            <span>Created on</span>
                                            <span>{shipmentTrackingData.gateEntry.time}</span>                                                                                                                                                                                                                                                                                                                                                                                                                          
                                        </span>
                                        <span className="stmb-item">
                                            <span>Created by</span>
                                            <span className="stmbi-name">{shipmentTrackingData.gateEntry.createdBy}</span>
                                        </span>
                                    </div> 
                                </div> 
                            </div> :
                            <div className="st-head st-border-nono">
                                <span className="sth-circle bg-yellow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                  <h3>Gate Entry</h3>
                                  {/* <span className="stmb-number">{shipmentTrackingData.gateEntry.shipmentAdviceCode}</span> */}
                                  <span className="stmb-msg stmbu-msg">Shipment waiting to be done.</span> 
                                </div>
                            </div> }
                        </div>
                        <div className="col-lg-12 pad-0 m-top-15">
                            { shipmentTrackingData.grcEntry.flag == 'true' ? <div className="st-head">
                                <span className="sth-circle bg-green">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>  
                                <div className="stm-body">
                                    <h3>GRC</h3>
                                    <span className="stmb-number">{shipmentTrackingData.grcEntry.grcNo}</span>
                                    <span className="stmb-msg"> |  Shipment has been delivered successfully</span>
                                    <div className="stmb-details">
                                        <span className="stmb-item"> 
                                            <span>Created on</span>
                                            <span>{shipmentTrackingData.grcEntry.time}</span>                                                                                                                                                                                                                                                                                                                                                                                                                          
                                        </span>
                                        <span className="stmb-item">
                                            <span>Created by</span>
                                            <span className="stmbi-name">{shipmentTrackingData.grcEntry.createdBy}</span>
                                        </span>
                                    </div> 
                                </div> 
                            </div> :
                            <div className="st-head st-border-nono">
                                <span className="sth-circle bg-yellow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="0 0 13.509 15.9">
                                        <g id="prefix__success_1_" data-name="success (1)" transform="translate(-38.5)">
                                            <path fill="#fff" id="prefix__Path_826" d="M182.816 30H173.5v6.521h9.316l-2.329-3.261z" class="prefix__cls-1" data-name="Path 826" transform="translate(-130.808 -29.068)"/>
                                            <path fill="#fff" id="prefix__Path_827" d="M41.761 0H39.9v14.037h-1.4V15.9h4.658v-1.863h-1.4z" class="prefix__cls-1" data-name="Path 827"/>
                                        </g> 
                                    </svg>
                                </span>
                                <div className="stm-body">
                                  <h3>GRC</h3>
                                  {/* <span className="stmb-number">{shipmentTrackingData.grcEntry.grcNo}</span> */}
                                  <span className="stmb-msg stmbu-msg">Shipment waiting to be done.</span> 
                                </div>
                            </div> }
                        </div>
                    </div>
                </div>}
            </div>
        )
    }
}

export default ShipmentTracking