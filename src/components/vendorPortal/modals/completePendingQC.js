import React, { Component } from 'react'
import moment from 'moment';
import QcDateConfirmModal from '../modals/qcDateConfirmModal';
import fileIcon from '../../../assets/fileIcon.svg';
import Close from '../../../assets/close-red.svg';
import axios, { post } from "axios";
import { CONFIG } from "../../../config/index";
import ToastLoader from '../../loaders/toastLoader';
import FilterLoader from '../../loaders/filterLoader';
import RequestError from  '../../loaders/requestError'; 
import CompleteInspMoreDetail from './completeInspMoreDetail';
import CreateAsnUpload from '../vendor/orders/createAsnUpload';

const todayDate = moment(new Date()).format("YYYY-MM-DD");
export default class CompletePendingQC extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rangeVal: 1,
            sliderDisable: false,
            valueDate: "",
            poDetailsShipment: [],
            qcModalConfirm: false,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            qualityModal: false,
            qcUpload: true,
            isQCDone: "",
            isInvoiceUploaded: "",
            toastLoader: false,
            toastMsg: "",

            allQC: "",
            invoice: [],
            QC: [],
            EMAIL: [],
            multipleQc: [...this.props.active],
            showDrop: false,
            prevId: "",
            showInvoice: false,
            prevInvoiceID: "",
            fileNames: [],
            emailFileNames: [],
            showEmail: false,
            barcode: [],
            remarksErr: false,

            noOfReInspection: this.props.noOfReInspection,
            inspectionBufferDays: this.props.inspectionBufferdays,
            errorMessage: "",
            errorCode: "",
            code: "",
            alert: false,
            moreDetails: false,
            moreDetailsData: "",

            ppsUploadModal: false,
            currentAsnId: 0,
            allowNoOfReInspection: this.props.allowNoOfReInspection,
            allowInspBufferDays: this.props.allowInspBufferDays
        }
    }
    componentDidMount() {
        var data = this.state.multipleQc
        data.map((data) => (data.reQcDate = "", data.QCStatus = data.qcStatus == "Hold" ? "hold" : "", data.select = "", data.selectErr = "", data.reQcDateErr = "", data.remarksErr = "", data.remarks= "", data.preQCStatus= data.qcStatus,
                             data.emailUploadErr = false,
                             data.qcStatus == "Hold" ?  document.getElementById("status-selection"+data.id).options[4].selected = true :
                                                        document.getElementById("status-selection"+data.id).options[4].selected = false,
                             data.showEmailAndRemark = false,
                             data.selectPPSErr = false,data.ppsRemarkErr = false,
                             data.selectTestReportErr = false,data.testRemarkErr = false,
                             data.selectFitTagErr = false,data.fitRemarkErr = false,
                             data.selectInspTypeErr = false,data.reportNumberErr = false,
                             data.moreDetailsModalErr = false,
                             data.qcArray = [],
                             data.emailArray = []                            
                             ))
        this.setState({ multipleQc: data })
        //this.props.getStatusButtonActiveRequest();
       
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getAllComment.isSuccess) {
            if (nextProps.shipment.getAllComment.data.resource != null) {
                this.setState({
                    QC: nextProps.shipment.getAllComment.data.resource.QC,
                    allQC: nextProps.shipment.getAllComment.data.resource,
                    invoice: nextProps.shipment.getAllComment.data.resource.INVOICE,
                    barcode: nextProps.shipment.getAllComment.data.resource.BARCODE,
                    EMAIL: nextProps.shipment.getAllComment.data.resource.EMAIL,
                })
            }
        }
        if (nextProps.shipment.deleteUploads.isSuccess) {
            var multipleQc = [...this.state.multipleQc]
            multipleQc.map((data) => {
                if (data.id == this.state.deletedId) {
                    data.isQCUploaded = nextProps.shipment.deleteUploads.data.isQCAvailable,
                    data.isEmailUploaded = nextProps.shipment.deleteUploads.data.isEmailAvailable
                    if (nextProps.shipment.deleteUploads.data.isQCAvailable == "FALSE") {
                        this.setState({ showDrop: false, })

                    if( nextProps.shipment.deleteUploads.data.isEmailAvailable == "FALSE")
                        this.setState({ showEmail: false, })
                    }
                }
            })
            this.setState({ multipleQc })
        }
        // if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) {
        //     this.setState({
        //         noOfReInspection: nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount) : 0, 
        //         inspectionBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays) : 0,  
        //     })
        //     this.props.getStatusButtonActiveClear()
        //     this.setState({ loader: false})
        // }
        // else if (nextProps.logistic.getButtonActiveConfig.isError) {
        //     this.setState({
        //         errorMessage: nextProps.logistic.getButtonActiveConfig.message.error == undefined ? undefined : nextProps.logistic.getButtonActiveConfig.message.error.errorMessage,
        //         loader: false,
        //         alert: true,
        //         code: nextProps.logistic.getButtonActiveConfig.message.status,
        //         errorCode: nextProps.logistic.getButtonActiveConfig.message.error == undefined ? undefined : nextProps.logistic.getButtonActiveConfig.message.error.errorCode
        //     })
        //     this.props.getStatusButtonActiveClear()
        // }
    }

    viewAllAttachment = (data, subModule) => {
        if (!this.state.showDrop || !this.state.showEmail || this.state.prevId !== data.id) {
            let payload = {
                // shipmentId: data.id,
                // module: "SHIPMENT",
                // no: 1,
                // orderCode: data.orderCode,
                // orderNumber: data.orderNumber,
                // isAllAttachment: "TRUE",
                // subModule: subModule

                module: "SHIPMENT",
                pageNo: 1,
                subModule: subModule,
                isAllAttachment: "TRUE",
                type: 1,
                search: "",
                commentId: data.id,
                commentCode: data.shipmentAdviceCode
            }
            this.props.getAllCommentRequest(payload)
            if( subModule == "QC")
               this.setState({ showDrop: !this.state.showDrop })
            else if( subModule == "EMAIL")
               this.setState({ showEmail: !this.state.showEmail })   
            this.setState({ prevId: data.id, expandedID: data.id, fileNames: [], emailFileNames: [] })

        } else {
            if( subModule == "QC")
               this.setState({ showDrop: false })
            else if( subModule == "EMAIL")
               this.setState({ showEmail: false}) 
        }
    }

    onFileUpload(e, subModule, data) {
        // document.getElementById('fileName').innerHTML = Object.values(e.target.files)[0].name
        let files = Object.values(e.target.files)
        let multipleQc = [...this.state.multipleQc]
        let date = new Date()
        let id = e.target.id
        const fd = new FormData();
        var fileNames = files.map((item, index) => Object.values(e.target.files)[index].name)
        let uploadNode = {
            orderCode: data.orderCode,
            shipmentId: data.id,
            orderNumber: data.orderNumber,
            module: "SHIPMENT",
            subModule: subModule,
            orderId: data.orderId,
            documentNumber: data.documentNumber,
            commentId: data.id,
            commentCode: data.shipmentAdviceCode,
            vendorCode: data.vendorCode,
        }
        // this.setState({
        //     loader: true,
        //     fileNames,
        //     expandedID: data.id
        // })

        if( subModule == "QC")
           this.setState({ loader: true, fileNames,expandedID: data.id, showDrop: false,})
        else if (subModule == "EMAIL")   
           this.setState({ loader: true, emailFileNames: fileNames, expandedID: data.id, showEmail: false, })

        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        for (var i = 0; i < files.length; i++) {
            fd.append("files", files[i]);
        }
        fd.append("uploadNode", JSON.stringify(uploadNode))
        // this.loaderUpload("true")
        axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
            .then(res => {
                this.setState({
                    loader: false
                })
                let result = res.data.data.resource
                if (res.data.status == "2000") {
                    multipleQc.map((hdata) => {
                        if (hdata.id == data.id) {
                            hdata.isQCUploaded = res.data.data.resource[0].isQCAvailable,
                            hdata.isEmailUploaded = res.data.data.resource[0].isEmailAvailable,
                            hdata.emailUploadErr = hdata.preQCStatus === "Hold" ? (res.data.data.resource[0].isEmailAvailable === "TRUE" ? false : true) : false
                        }
                    })
                }
                if (id == "fileUpload") {
                    let QC = this.state.QC
                    let EMAIL = this.state.EMAIL

                    for (let i = 0; i < result.length; i++) {
                        let payload = {
                            commentedBy: sessionStorage.getItem("prn"),
                            comments: "",
                            commentDate: "",
                            commentType: "URL",
                            folderDirectory: result[i].folderDirectory,
                            folderName: result[i].folderName,
                            filePath: result[i].filePath,
                            fileName: result[i].fileName,
                            fileURL: result[i].fileURL,
                            urlExpirationTime: result[i].urlExpirationTime,
                            qcVersion: "NEW",
                            uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                            submodule: subModule
                        }
                        // QC.push(payload)
                        if( subModule == "QC")
                           QC.push(payload)
                        else if( subModule == "EMAIL")
                           EMAIL.push(payload)
                    }
                    this.setState({
                        QC,
                        loader: false,
                        EMAIL,
                        multipleQc
                    })
                } else {
                    let allComment = this.state.allComment

                    for (let i = 0; i < result.length; i++) {
                        let payload = {
                            commentedBy: sessionStorage.getItem("prn"),
                            comments: "",
                            commentDate: "",
                            commentType: "URL",
                            folderDirectory: result[i].folderDirectory,
                            folderName: result[i].folderName,
                            filePath: result[i].filePath,
                            fileName: result[i].fileName,
                            fileURL: result[i].fileURL,
                            urlExpirationTime: result[i].urlExpirationTime,
                            uploadedBy: sessionStorage.getItem('prn'),
                            isUploaded: "TRUE",
                            commentVersion: "NEW",
                            uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                            submodule: subModule
                        }
                        allComment.push(payload)
                    }
                    this.setState({
                        allComment,
                        multipleQc,
                        callAddComments: true,
                        loader: false
                    }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
                }
            }).catch((error) => {
                this.setState({
                    loader: false
                })
                this.props.openToastError()
            })
        e.target.value = ''
    }
    deleteUploads(data, multipleAsn, subModule) {
        let QC = this.state.QC.filter((check) => check.fileURL != data.fileURL)
        let EMAIL = this.state.EMAIL.filter((check) => check.fileURL != data.fileURL)
        let files = []
        let a = {
            fileName: data.fileName,
            isActive: "FALSE"
        }
        files.push(a)
        let payload = {
            poId: this.props.poId,
            shipmentId: this.state.expandedID,
            module: "SHIPMENT",
            subModule: subModule,
            isQCAvailable: multipleAsn.isQCUploaded,
            isInvoiceAvailable: multipleAsn.isInvoiceUploaded,
            isBarcodeAvailable: multipleAsn.isBarcodeUploaded,
            isEmailAvailable: multipleAsn.isEmailUploaded,
            files: files,
            commentId: multipleAsn.id
        }
        this.setState({
            selectedFiles: files,
            QC,
            deletedId: multipleAsn.id,
            EMAIL
        })
        this.props.deleteUploadsRequest(payload)
    }
    onDelete = (e, id) => {
        let multipleQc = [...this.state.multipleQc]
        if (multipleQc.length > 1) {
            multipleQc = multipleQc.filter((data) => data.id != id)
        }
        this.setState({ multipleQc })
    }
    handleFields = (e, id, type) => {
        let multipleQc = [...this.state.multipleQc]
        if (type == "remarks") {
            multipleQc.map((data) => (data.id == id && (data.remarks = e.target.value)))
            this.setState({ multipleQc },()=>this.remarksErr(id))
        } else if (type == "select") {
            multipleQc.map((data) =>{ if(data.id === id && e.target.value === "reQC" && (this.state.allowNoOfReInspection && data.reqcCount === this.state.noOfReInspection)){
                                            this.setState({
                                                toastMsg: "You have exceeded your Re-QC limit.",
                                                toastLoader: true,
                                                loader: false,
                                            })
                                            setTimeout(() => {
                                                this.setState({
                                                    toastLoader: false
                                                })
                                            }, 10000)
                                            data.QCStatus = "";
                                            document.getElementById("status-selection"+data.id).options[0].selected = true;
                                      }else{
                                        (data.id == id && (data.QCStatus = e.target.value)),
                                        (data.id == id && data.preQCStatus === "Hold"&& (data.showEmailAndRemark = true)),
                                        (data.id == id && (data.QCStatus == "hold" || data.QCStatus == "Fail" || data.QCStatus == "Abort") && (data.showEmailAndRemark = false)) 
                                      }
                                    })
            this.setState({ multipleQc }, () => {this.select(id); this.moreDetailsModalValid()})

        } else if (type == "reQcDate") {
            multipleQc.map((data) => (data.id == id && (data.reQcDate = e.target.value)))
            this.setState({ multipleQc }, () => (this.dateErr(id)))
        }else if ( type == "emailUpload") {
            multipleQc.map((data) => (data.id == id && (data.emailUploadErr = data.isEmailUploaded === "TRUE" ? false : true)))
            this.setState({ multipleQc })
        }
        // More Details Entries::
        else if ( type == "selectPPS") {
            multipleQc.map((data) => (data.id == id && (data.selectPPS = e.target.value)))
            this.setState({ multipleQc },()=>this.moreDetailsModalValid())
        }
        else if ( type == "ppsRemark") {
            multipleQc.map((data) => (data.id == id && (data.ppsRemark = e.target.value)))
            this.setState({ multipleQc },()=>this.moreDetailsModalValid());
        }
        else if ( type == "selectTestReport") {
            multipleQc.map((data) => (data.id == id && (data.selectTestReport = e.target.value)))
            this.setState({ multipleQc },()=>this.moreDetailsModalValid())
        }
        else if ( type == "testRemark") {
            multipleQc.map((data) => (data.id == id && (data.testRemark = e.target.value)))
            this.setState({ multipleQc },()=>this.moreDetailsModalValid())
        }
        else if ( type == "selectFitTag") {
            multipleQc.map((data) => (data.id == id && (data.selectFitTag = e.target.value)))
            this.setState({ multipleQc },()=>this.moreDetailsModalValid())
        }
        else if ( type == "fitRemark") {
            multipleQc.map((data) => (data.id == id && (data.fitRemark = e.target.value)))
            this.setState({ multipleQc },()=>this.moreDetailsModalValid())
        }
        else if ( type == "selectInspType") {
            multipleQc.map((data) => (data.id == id && (data.selectInspType = e.target.value)))
            this.setState({ multipleQc },()=>this.moreDetailsModalValid())
        }
        else if ( type == "reportNumber") {
            multipleQc.map((data) => (data.id == id && (data.reportNumber = e.target.value)))
            this.setState({ multipleQc },()=>this.moreDetailsModalValid())
        }
    }

    remarksErr=(id)=>{
        let multipleQc = [...this.state.multipleQc]
        if (id != undefined) {
            multipleQc.map((data) => id == data.id && (data.QCStatus == "Abort" || data.preQCStatus == "Hold" || data.QCStatus == "hold") && data.remarks == "" ? (data.remarksErr = true) : (data.remarksErr = false))
        } else {
            multipleQc.map((data) => (data.QCStatus == "Abort" || data.preQCStatus == "Hold" || data.QCStatus == "hold") && data.remarks == "" ? (data.remarksErr = true) : (data.remarksErr = false))
        }
        this.setState({ multipleQc })
    }

    select = (id) => {
        let multipleQc = [...this.state.multipleQc]
        if (id != undefined) {
            multipleQc.map((data) => id == data.id && data.QCStatus == "" ? (data.selectErr = true) : (data.selectErr = false))
        } else {
            multipleQc.map((data) => data.QCStatus == "" ? (data.selectErr = true) : (data.selectErr = false))
        }
        this.setState({ multipleQc })
    }
    dateErr = () => {
        let multipleQc = [...this.state.multipleQc]
        multipleQc.map((data) => (data.QCStatus == "reQC" || data.QCStatus == "doneLater" || data.QCStatus == "DONELATER_AT_WAREHOUSE") && data.reQcDate == "" ? (data.reQcDateErr = true) : (data.reQcDateErr = false))
        this.setState({ multipleQc })
    }

    invoiceUpload =(e)=>{
        let multipleQc = [...this.state.multipleQc]
        multipleQc.map((data) => {
                    if (data.preQCStatus == "Hold" && data.QCStatus !== "hold" && data.QCStatus !== "Fail" && data.QCStatus !== "Abort"){
                        data.emailUploadErr = data.isEmailUploaded === "TRUE" ? false : true
                    }else{
                        data.emailUploadErr = false
                    }
                })
        this.setState({ multipleQc })         
    }

    moreDetailsModalValid =()=>{
        let multipleQc = this.state.multipleQc
        multipleQc.forEach((data) => {
            if (data.QCStatus == "Pass" || data.QCStatus == "Fail" || data.QCStatus == "hold"){
                data.selectPPSErr = data.selectPPS == undefined || data.selectPPS == "" ? true : false
                data.ppsRemarkErr = data.ppsRemark == undefined || data.ppsRemark == "" ? true : false
                data.selectTestReportErr = data.selectTestReport == undefined || data.selectTestReport == "" ? true : false
                data.testRemarkErr = data.testRemark == undefined || data.testRemark == "" ? true : false
                data.selectFitTagErr = data.selectFitTag == undefined || data.selectFitTag == "" ? true : false
                data.fitRemarkErr = data.fitRemark == undefined || data.fitRemark == "" ? true : false
                data.selectInspTypeErr = data.selectInspType == undefined || data.selectInspType == "" ? true : false
                data.reportNumberErr = data.reportNumber == undefined || data.reportNumber == "" ? true : false
            }
            else{
                data.selectPPSErr = false
                data.ppsRemarkErr = false
                data.selectTestReportErr = false
                data.testRemarkErr = false
                data.selectFitTagErr = false
                data.fitRemarkErr = false
                data.selectInspTypeErr = false
                data.reportNumberErr = false
            }
            data.moreDetailsModalErr = data.selectPPSErr || data.ppsRemarkErr || data.selectTestReportErr || data.testRemarkErr ||
                                       data.selectFitTagErr || data.fitRemarkErr || data.selectInspTypeErr || data.reportNumberErr ? true : false;

        })
        this.setState({ multipleQc })
    }

    onConfirm = () => {
        this.closeMoreDetails();
        this.select()
        this.dateErr()
        this.remarksErr()
        this.invoiceUpload()
        this.moreDetailsModalValid()
        let flag = false
        setTimeout(() => {
            let multipleQc = this.state.multipleQc
            multipleQc.forEach((data) => {
                if (data.selectErr == true || data.reQcDateErr == true || data.remarksErr || data.emailUploadErr || 
                    data.selectPPSErr || data.ppsRemarkErr || data.selectTestReportErr || data.testRemarkErr || data.selectFitTagErr 
                    || data.fitRemarkErr || data.selectInspTypeErr || data.reportNumberErr || data.moreDetailsModalErr) {
                    flag = true
                    return;
                }
            })
            if (!flag) {
                let push = []
                multipleQc.map((hdata) => {
                    let payload = {
                        "shipmentId": hdata.id,
                        "module": "SHIPMENT",
                        "isQCDone": hdata.QCStatus == "Pass" ? "TRUE" : "FALSE",
                        "shipmentStatus": hdata.QCStatus === "Pass" ? "SHIPMENT_REQUESTED" : hdata.QCStatus === "Fail" || hdata.QCStatus === "Abort" ? "SHIPMENT_CANCELLED" : hdata.QCStatus === "reQC" || hdata.QCStatus === "hold" ? "PENDING_QC_CONFIRM" : "SHIPMENT_REQUESTED",
                        "QCStatus": hdata.QCStatus,
                        "isInvoiceUploaded": "",
                        "reason": hdata.remarks,
                        "orderCode": hdata.orderCode,
                        "orderNumber": hdata.orderNumber,
                        "reqcDate": hdata.QCStatus == "reQC" || hdata.QCStatus == "doneLater" || hdata.QCStatus == "DONELATER_AT_WAREHOUSE" ? moment(hdata.reQcDate).format("YYYY-MM-DD") + "T00:00+05:30" : "",
                        "shipmentAdviceCode": hdata.shipmentAdviceCode,
                        "orderId": hdata.orderId,
                        "documentNumber": hdata.documentNumber,
                        "qcSelectionDate": hdata.qcSelectionDate !== null ? moment(hdata.qcSelectionDate, "DD-MM-YYYY").format("YYYY-MM-DD") + "T00:00+05:30" : "",
                        "SHIPMENT": {
                            "QC": [
                            ],
                            "COMMENTS": [
                            ],
                            "INVOICE": [
                            ],
                            "EMAIL":[
                            ],
                        },
                        "pps": hdata.selectPPS == undefined ? "" : hdata.selectPPS,
                        "ppsRemark": hdata.ppsRemark = undefined ? "" : hdata.selectPPS,
                        "testReport": hdata.selectTestReport == undefined ? "" : hdata.selectTestReport,
                        "testReportRemark": hdata.testRemark == undefined ? "" : hdata.testRemark,
                        "fitTag": hdata.selectFitTag == undefined ? "" : hdata.selectFitTag,
                        "fitTagRemark": hdata.fitRemark == undefined ? "" : hdata.fitRemark,
                        "inspectionType": hdata.selectInspType == undefined ? "" : hdata.selectInspType,
                        "reportNumber": hdata.reportNumber == undefined ? "" : hdata.reportNumber,
                    }
                    push.push(payload)
                })
                let finalPayLoad = {
                    qcList: push
                }
                this.props.addMultipleCommentQcRequest(finalPayLoad)

                //Calling File upload after submission::
                this.props.onFileUploadProps(multipleQc)
            }
        }, 10)
    }
    dowloadInvoice(data) {
        if (!this.state.showInvoice || this.state.prevInvoiceID !== data.id) {
            let payload = {
                // shipmentId: data.id,
                // module: "SHIPMENT",
                // no: 1,
                // orderCode: data.orderCode,
                // orderNumber: data.orderNumber,
                // isAllAttachment: "TRUE",
                // subModule: "INVOICE"

                module: "SHIPMENT",
                pageNo: 1,
                subModule: "INVOICE",
                isAllAttachment: "TRUE",
                type: 1,
                search: "",
                commentId: data.id,
                commentCode: data.shipmentAdviceCode
            }
            this.props.getAllCommentRequest(payload)
            this.setState({ showDrop: false, showInvoice: true, prevInvoiceID: data.id, expandedID: data.id, fileNames: [] })
        } else {
            this.setState({ showInvoice: false })
        }
    }
    dowloadBarcode = (data) => {
        if (!this.state.barcodeDrop || this.state.prevId !== data.id) {
            let payload = {
                // shipmentId: data.id,
                // module: "SHIPMENT",
                // no: 1,
                // // orderCode: data.orderCode,
                // orderNumber: data.orderNumber,
                // isAllAttachment: "TRUE",
                // subModule: "BARCODE",
                // refresh: "whole",
                // orderId: data.orderId,
                // documentNumber: data.documentNumber

                module: "SHIPMENT",
                pageNo: 1,
                subModule: "BARCODE",
                isAllAttachment: "TRUE",
                type: 1,
                search: "",
                commentId: data.id,
                commentCode: data.shipmentAdviceCode
            }
            this.props.getAllCommentRequest(payload)
            this.setState({ barcodeDrop: true, prevId: data.id, fileNames: [] })
        } else {
            this.setState({ barcodeDrop: false })
        }
        this.setState({ barcodeDrop: !this.state.barcodeDrop, expandedID: data.id })
    }

    openMoreDetails =(e, data)=> {
        e.preventDefault();
        this.setState({ moreDetails: true, moreDetailsData: data})
    }
    closeMoreDetails = (e) => {
        this.setState({ moreDetails: false })
    }

    moreDetailModalRefresh =()=>{
        var data = this.state.multipleQc
        data.map((data) => (data.selectPPS = "",
                             data.ppsRemark = "",
                             data.selectTestReport = "",                                                        document.getElementById("status-selection"+data.id).options[4].selected = false,
                             data.testRemark = "",
                             data.selectFitTag = "",
                             data.fitRemark = "",
                             data.selectInspType = "",
                             data.reportNumber = ""                      
                             ))
        this.setState({ multipleQc: data })
    }
    
    //---------------------------------------- New File Upload ---------------------------// 
    openPpsUploadModal =(e, data, fileType)=> {
        e.preventDefault();
        let multipleQc = this.state.multipleQc
        multipleQc.map( item => {
            if(data.id == item.id){
                data.uploadFileType = fileType
            }  
        })
        this.setState({
            multipleQc,
            ppsUploadModal: !this.state.ppsUploadModal,
            currentAsnId: data.id,
        },()=> document.addEventListener('click', this.clickClosePpsModal));
    }

    onFileUploadSubmit = (fileList, moduleName) =>{
        let multipleQc = this.state.multipleQc
        if(moduleName == "QC"){ 
            multipleQc.map( data => {
                if(data.id == this.state.currentAsnId){
                    data.qcArray = fileList
                    data.fileType = "QC"
                }  
            })  
        }
        if(moduleName == "EMAIL"){ 
            multipleQc.map( data => {
                if(data.id == this.state.currentAsnId){
                    data.emailArray = fileList
                    data.fileType = "EMAIL"
                    data.isEmailUploaded = fileList.length ? "TRUE" : "FALSE"
                }  
            })  
        }
        this.setState({
            multipleQc,
            ppsUploadModal: false,
        },()=> document.removeEventListener('click', this.clickClosePpsModal))
    }

    CancelPpsUploadModal = (e) => {
        this.setState({
            ppsUploadModal: false,
        },()=> document.removeEventListener('click', this.clickClosePpsModal))
    }

    clickClosePpsModal =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop")){
            this.setState({ ppsUploadModal: false})
        }
    }
    //-----------------------------------------------------------------------------------------//

    
    render() {
        return (
            <div>
                <div className="modal" id="pocolorModel">
                    <div className="backdrop modal-backdrop-new"></div>
                    <div className="modal-content shipment-aprv-asn-mdl setCompleteQcWidth">
                        <div className="shipment-approvedasn-modal-head">
                            <div className="samh-left">
                                <h3>Complete Insp</h3>
                            </div>
                            <div className="samh-right">
                                <button type="button" onClick={() => {this.props.handleCompleteQC(); this.moreDetailModalRefresh()}}>Cancel</button>
                                <button type="button" className="confirmBtnBlue" onClick={this.onConfirm}>Confirm</button>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="shipment-approvedasn-modal-body">
                                <div className="responsive-table">
                                    <table className="table modal-table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div className="samb-asnno">
                                                        <label className="samb-bold">PO NUMBER </label>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div className="samb-asnno">
                                                        <label className="samb-bold">ASN No.</label>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div className="samb-asnno">
                                                        <label className="samb-bold">Insp Status</label>
                                                    </div>
                                                </th>
                                                <th>
                                                {this.state.multipleQc.some( data => (data.QCStatus == "Pass" || data.QCStatus == "reQC" || data.QCStatus == "" || data.QCStatus == "Fail")) &&
                                                    <div className="samb-asnno">
                                                        <label className="samb-bold">Upload Insp </label>
                                                    </div>}
                                                </th>
                                                <th>
                                                {this.state.multipleQc.some( data => data.showEmailAndRemark) && 
                                                    <div className="samb-asnno">
                                                        <label className="samb-bold">Upload Email <span className="mandatory">*</span></label>
                                                    </div>}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.multipleQc.map((data, key) => (
                                                <tr key={key}>
                                                    <td><label className="displayBlock">{data.orderNumber}</label></td>
                                                    <td>
                                                        <label>{data.shipmentAdviceCode}{data.moreDetailsModalErr && <span className="error">Fill More Details</span>}</label> 
                                                        <span className={this.state.moreDetails === false ? "samba-moredetail" : "samba-moredetail samba-moredetail-focus"} onClick={(e) => this.openMoreDetails(e, data)}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 19.292 19.376">
                                                                <g id="edit_2_" transform="translate(-.757)">
                                                                    <path fill="#a4b9dd" id="Path_954" d="M107.625.653L107.6.632a2.414 2.414 0 0 0-3.411.154l-8.628 9.446a.832.832 0 0 0-.175.3l-1.014 3.041a1.157 1.157 0 0 0 1.1 1.522 1.152 1.152 0 0 0 .464-.1l2.937-1.285a.832.832 0 0 0 .281-.2l8.628-9.445a2.417 2.417 0 0 0-.157-3.412zM96.317 13.009l.6-1.785.05-.055 1.124 1.031-.05.055zM106.549 2.94l-7.335 8.03-1.128-1.03 7.335-8.03a.748.748 0 0 1 1.057-.048l.023.021a.749.749 0 0 1 .048 1.057z" className="cls-1" transform="translate(-88.361)"/>
                                                                    <path fill="#a4b9dd" id="Path_955" d="M17.59 38.16a.833.833 0 0 0-.833.833v7.072a2.121 2.121 0 0 1-2.119 2.119H4.542a2.121 2.121 0 0 1-2.119-2.119V36.051a2.121 2.121 0 0 1 2.119-2.119h7.308a.833.833 0 1 0 0-1.666H4.542a3.789 3.789 0 0 0-3.785 3.785v10.014a3.789 3.789 0 0 0 3.785 3.785h10.1a3.789 3.789 0 0 0 3.785-3.785v-7.072a.833.833 0 0 0-.837-.833z" className="cls-1" transform="translate(0 -30.474)"/>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <div className="samb-asnno">
                                                            <div className="samb-dropdown">
                                                                <select id={"status-selection"+data.id} value={data.QCStatus} onChange={(e) => this.handleFields(e, data.id, "select")}>
                                                                    <option value="">Select</option>
                                                                    <option value="Pass">Pass</option>
                                                                    <option value="Fail">Fail</option>
                                                                    <option value="Abort">Abortive</option>
                                                                    <option value="hold">On Hold</option>
                                                                    <option value="reQC">Re-Inspection</option>
                                                                    <option value="doneLater">To Be Done Later</option>
                                                                    <option value="DONELATER_AT_WAREHOUSE">To Be Done At Warehouse</option>
                                                                </select>
                                                                {data.selectErr && <span className="error">Please Select</span>}
                                                                <div className="posRelative displayInline">
                                                                    {(data.QCStatus == "reQC" || data.QCStatus == "doneLater" || data.QCStatus == "DONELATER_AT_WAREHOUSE") && 
                                                                        <div className="sambd-erm"><input type="date" min={todayDate > data.qcFromDateSelection ? todayDate : data.qcFromDateSelection} max={this.state.allowInspBufferDays ? moment(data.qcToDateSelection).add(this.state.inspectionBufferDays,'days').format("YYYY-MM-DD") : moment(data.qcToDateSelection).format("YYYY-MM-DD")} value={data.reQcDate} placeholder={data.reQcDate == "" ? "Select Date" : data.reQcDate} onChange={(e) => this.handleFields(e, data.id, "reQcDate")} />
                                                                        {data.reQcDateErr && <span className="error">Please Select Date</span>}
                                                                    </div>}
                                                                    {(data.QCStatus == "Fail" || data.QCStatus == "Pass" || data.QCStatus == "Abort" || data.showEmailAndRemark || data.QCStatus == "hold" || data.reQcDate > data.qcToDateSelection) && 
                                                                        <div className="sambd-erm"><input type="text" value={data.remarks} onChange={(e) => this.handleFields(e, data.id, "remarks")} placeholder={((data.QCStatus == "Fail" || data.QCStatus == "Pass" ) && !data.showEmailAndRemark && data.QCStatus !== "hold") ? "Remark" : "Remark"}/>
                                                                            {data.remarksErr && <span className="error">Enter Your Remark</span>}
                                                                        </div>}
                                                                    {/* {data.QCStatus == "Pass" && <input type="text" value={data.remarks} onChange={(e) => this.handleFields(e, data.id, "remarks")} placeholder="Remark" />} */}      
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        { data.QCStatus !== "hold" && data.QCStatus !== "Abort" && data.QCStatus !== "doneLater" && data.QCStatus !== "DONELATER_AT_WAREHOUSE" && 
                                                        <div className="caffc-fields">
                                                            <React.Fragment>
                                                                <div className="caffb-upload-file">
                                                                    <label onClick={(e) => this.openPpsUploadModal(e, data, "QC")}>
                                                                        { data.qcArray !== undefined && (data.qcArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{data.qcArray.length} Files</span>)}
                                                                        <img src={require('../../../assets/attach.svg')} />
                                                                    </label>
                                                                    <span id="ppsfileLabel"></span>
                                                                </div>
                                                            </React.Fragment>
                                                        </div>}
                                                    {/* { data.QCStatus !== "hold" && data.QCStatus !== "Abort" && data.QCStatus !== "doneLater" && data.QCStatus !== "DONELATER_AT_WAREHOUSE" && 
                                                            <div className="samb-asnno">
                                                            <div className="uploadInvoiceLeft pad-0 heightAuto">
                                                                <div className="bottomToolTip displayInline">
                                                                    <label className="file width115px customInput m-top-5 hoverMe" >
                                                                        <input type="file" id="fileUpload" onChange={(e) => this.onFileUpload(e, "QC", data)} multiple="multiple" />
                                                                        <span >Choose file</span>
                                                                        <img src={fileIcon} />
                                                                    </label>
                                                                    <div className="fileName" id="fileName"></div>
                                                                    <div className="fileUploads">
                                                                        {this.state.fileNames.length != 0 && data.id == this.state.expandedID && <label>{this.state.fileNames.length}-Files Uploaded</label>}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="uploadInvoiceRight">
                                                                {data.isQCUploaded == "TRUE" ? <button type="button" className="removeAfter eye-icon-new" onClick={(e) => { this.viewAllAttachment(data, "QC") }}><span className="tooltip-shipment-asn">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                        <g id="prefix__eye" transform="translate(-4.736 -19.47)">
                                                                            <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                            <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#000" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                        </g>
                                                                    </svg>
                                                                    <span className="tooltip-content-shipment">View Attachment</span></span></button>
                                                                    : <button type="button" className="removeAfter eye-icon-new pointerNone" ><span className="tooltip-shipment-asn">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                            <g id="prefix__eye" fill="#ccd8ed" transform="translate(-4.736 -19.47)">
                                                                                <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                                <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#ccd8eds" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                            </g>
                                                                        </svg>
                                                                        <span className="tooltip-content-shipment">View Attachment</span></span></button>}
                                                                {this.state.showDrop && data.id == this.state.expandedID && <div className="filesDownloadDrop ">
                                                                    <div className="fileUploads">
                                                                        {this.state.QC.length != 0 ? this.state.QC.map((hdata, key) => <div className="asn-uploaded-file"><div className="asn-up-file-name"><p key={key} onClick={() => window.open(hdata.fileURL)}>{hdata.fileName}</p></div><span onClick={(e) => this.deleteUploads(hdata, data, "QC")}>&#10005;</span></div>)
                                                                            : <label>No Data Found</label>}
                                                                    </div>
                                                                </div>}
                                                            </div>
                                                        </div>} */}
                                                    </td>

                                                    <td>
                                                        {this.state.multipleQc.map( item => item.id == data.id && item.showEmailAndRemark && item.QCStatus !== "Fail" && item.QCStatus !== "Abort" &&
                                                        <div className="caffc-fields">
                                                            <React.Fragment>
                                                                <div className="caffb-upload-file">
                                                                    <label onClick={(e) => this.openPpsUploadModal(e, data, "EMAIL")}>
                                                                        {data.emailArray !== undefined && (data.emailArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{data.emailArray.length} Files</span>)}
                                                                        <img src={require('../../../assets/attach.svg')} />
                                                                    </label>
                                                                    <span id="ppsfileLabel"></span>
                                                                    {item.emailUploadErr && <span className="error">Upload email</span>}
                                                                </div>
                                                            </React.Fragment>
                                                        </div>)}
                                                        {/* {this.state.multipleQc.map( item => item.id == data.id && item.showEmailAndRemark && item.QCStatus !== "Fail" && item.QCStatus !== "Abort" && <div>
                                                            <div className="samb-asnno">
                                                                <div className="uploadInvoiceLeft pad-0 heightAuto">
                                                                    <div className="bottomToolTip displayInline">
                                                                        <label className="file width115px customInput m-top-5 hoverMe" >
                                                                            <input type="file" id="fileUpload" onChange={(e) => {this.onFileUpload(e, "EMAIL", item), this.handleFields(e, item.shipmentId, "emailUpload")}} multiple="multiple" />
                                                                            <span >Choose file</span>
                                                                            <img src={fileIcon} />
                                                                        </label>
                                                                        <div className="fileName" id="fileName"></div>
                                                                        <div className="fileUploads">
                                                                            {this.state.emailFileNames.length != 0 && item.id == data.id && !item.emailUploadErr && <label>{this.state.emailFileNames.length}-Files Uploaded</label>}
                                                                        </div>
                                                                        <p className="displayMe"> <span></span></p>
                                                                    </div>
                                                                </div>
                                                                {item.emailUploadErr && <span className="error">Upload email</span>}
                                                            </div>
                                                            <div className="uploadInvoiceRight">
                                                                {item.isEmailUploaded == "TRUE" ? <button type="button" className="removeAfter eye-icon-new" onClick={(e) => { this.viewAllAttachment(item, "EMAIL") }}><span className="tooltip-shipment-asn">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                        <g id="prefix__eye" transform="translate(-4.736 -19.47)">
                                                                            <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                            <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#000" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                        </g>
                                                                    </svg>
                                                                    <span className="tooltip-content-shipment">View Attachment</span></span></button>
                                                                    : <button type="button" className="removeAfter eye-icon-new pointerNone" ><span className="tooltip-shipment-asn">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                            <g id="prefix__eye" fill="#ccd8ed" transform="translate(-4.736 -19.47)">
                                                                                <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                                <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#ccd8eds" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                            </g>
                                                                        </svg>
                                                                        <span className="tooltip-content-shipment">View Attachment</span></span></button>}
                                                                {this.state.showEmail && item.id == data.id && <div className="filesDownloadDrop ">
                                                                    <div className="fileUploads">
                                                                        {this.state.EMAIL.length != 0 ? this.state.EMAIL.map((hdata, key) => <div className="asn-uploaded-file"><div className="asn-up-file-name"><p key={key} onClick={() => window.open(hdata.fileURL)}>{hdata.fileName}</p></div><span onClick={(e) => this.deleteUploads(hdata, item, "EMAIL")}>&#10005;</span></div>)
                                                                            : <label>No Data Found</label>}
                                                                    </div>
                                                                </div>}
                                                            </div>
                                                        </div>)} */}
                                                    </td>

                                                    <td>
                                                        <div className="close-flex">
                                                            <span className="close-btn" onClick={(e) => this.onDelete(e, data.id)}><img className="displayPointer" src={Close}></img></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {this.state.moreDetails && <div className="backdrop-transparent"></div>}
                        {this.state.moreDetails && <CompleteInspMoreDetail closeMoreDetails={this.closeMoreDetails} {...this.state} {...this.props} handleFields={this.handleFields}/>}
                        {this.state.ppsUploadModal && this.state.multipleQc.map( item => item.id == this.state.currentAsnId && <React.Fragment><div className="backdrop modal-backdrop-new"></div><CreateAsnUpload CancelPpsUploadModal={this.CancelPpsUploadModal} onFileUploadSubmit={this.onFileUploadSubmit} newQcArray={item.qcArray} newEmailArray={item.emailArray} {...this.state} {...this.props} uploadFileType={item.uploadFileType}/></React.Fragment> )}
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.qcModalConfirm ? <QcDateConfirmModal {...this.state} {...this.props} closeQc={(e) => this.closeQc(e)} CancelShipment={this.props.CancelShipment} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div >
        )
    }
}
