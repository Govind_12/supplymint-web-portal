import React from "react";
import CancelIcon from "../../../assets/cancel.svg";

export default class CancelModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reasonsArray: [],
            remark: "",
            reason: "",
            reasonErr: true,
            remarkErr: false,
        }
    }
   
    componentDidMount() {
        let payload ={
            type: this.props.type
        }
        this.props.getCancelAndRejectReasonRequest(payload)
        this.props.onRef(this)
    }
    componentWillUnmount() {
       this.props.onRef(undefined)
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.orders.cancelAndRejectReason.isSuccess && nextProps.orders.cancelAndRejectReason.data.resource !== null ) {
            this.setState({
                reasonsArray: nextProps.orders.cancelAndRejectReason.data.resource,
            })
            this.props.getCancelAndRejectReasonClear()
        }
    }

    handleChange =(e)=>{
        if( e.target.id == "reason"){
            this.setState({ 
                reasonErr: e.target.value == "" ? true : false,
                reason: e.target.value,
                remarkErr: (e.target.value == "Others" && this.state.remark == "") ? true : false,
            })
        }
        if( e.target.id == "remark"){
            this.setState({remark: e.target.value,},()=>{
                this.setState({
                    remarkErr: (this.state.reason == "Others" && this.state.remark == "") ? true : false,
                })                  
            })
        }
    }

    render() {
        return (
           <div className="modal" id="cancelrequestmodal">
              <div className="backdrop modal-backdrop-new"></div>
                 <div className="modal-content cancel-request-modal">
                    <div className="crm-alert-icon">
                        <img src={CancelIcon} />
                    </div>
                    <div className="crm-body">
                        <span className="crmb-alrt-msg">{this.props.rejectAsnMsg !== undefined ? this.props.rejectAsnMsg : (this.props.rejectInvoiceStatus !== undefined && this.props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED" ? "Are you sure you want to reject the invoice request"
                            : "Are you sure you want to cancel the request")}
                        </span>
                        <div className="crmb-reason">
                            <label>Reason <span className="mandatory">*</span></label>
                            <select className="selectionGenric imgFocus" id="reason" value={this.state.reason} onChange={(e) => this.handleChange(e)}>
                                <option key={"0"} value="">Select Reason</option>
                                {this.state.reasonsArray.map( (data, key) => <option key={key} value={data}>{data}</option>)}
                            </select>
                        </div>
                        <div className="crmb-input">
                            <textarea id="remark" value={this.state.remark} onChange={this.handleChange} placeholder={this.props.rejectAsnMsg !== undefined || (this.props.rejectInvoiceStatus !== undefined && this.props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED") ?  "Write your reason for rejection…" : "Write your reason for cancellation…"}></textarea>
                        </div>
                    </div>
                    <div className="crm-footer">
                        <button className="crmf-disbtn" type="button" value="check" onClick={this.props.cancelRequest}>Discard</button>
                        {(!this.state.reasonErr && !this.state.remarkErr) ? <button className="crmf-canbtn" type="button" value="confirm" onClick={this.props.cancelRequest}>{this.props.rejectAsnMsg !== undefined || (this.props.rejectInvoiceStatus !== undefined && this.props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED") ? "Reject" : "Cancel"}</button>
                         : <button className="crmf-canbtn btnDisabled" type="button" value="confirm" disabled>{this.props.rejectAsnMsg !== undefined || (this.props.rejectInvoiceStatus !== undefined && this.props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED") ? "Reject" : "Cancel"}</button>}
                    </div>
              </div>
          </div>
        )
    }
}

// export const CancelModal = React.forwardRef((props, ref) => (
//     <div className="modal" id="cancelrequestmodal">
//         <div className="backdrop modal-backdrop-new"></div>
//         <div className="modal-content cancel-request-modal">
//             <div className="crm-alert-icon">
//                 <img src={CancelIcon} />
//             </div>
//             <div className="crm-body">
//                 <span>{props.rejectAsnMsg !== undefined ? props.rejectAsnMsg : (props.rejectInvoiceStatus !== undefined && props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED" ? "Are you sure you want to reject the invoice request"
//                  : "Are you sure you want to cancel the request")}</span>
//                 <div className="crmb-input">
//                     <textarea placeholder={props.rejectAsnMsg !== undefined || (props.rejectInvoiceStatus !== undefined && props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED") ?  "Write your reason for rejection…" : "Write your reason for cancellation…"} ref={ref} ></textarea>
//                 </div>
//             </div>
//             <div className="crm-footer">
//                 <button className="crmf-disbtn" type="button" value="check" onClick={props.cancelRequest}>Discard</button>
//                 <button className="crmf-canbtn" type="button" value="confirm" onClick={props.cancelRequest}>{props.rejectAsnMsg !== undefined || (props.rejectInvoiceStatus !== undefined && props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED") ? "Reject" : "Cancel"}</button>
//             </div>

//         </div>
//     </div>
// )
// )