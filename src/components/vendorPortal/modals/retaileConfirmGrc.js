import React from "react";
import SetTag from '../../../assets/set-tag.svg';
import NonSetTag from '../../../assets/non-set-tag.svg';
import Pagination from "../../pagination";

class RetailerConfirmGrc extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content grc-confirmation-modal confirm-grc-retailer">
                    <div className="grccm-head">
                        <h3>Confirm GRC</h3>
                    </div>
                    <div className="grccm-body">
                        <div className="grccm-asn-no">
                            <h4>VM/00035/20-21</h4>
                            <img src={SetTag} />
                            {/* <img src={NonSetTag} /> */}
                            <button className="grccm-asn-clear" type="button">Clear</button>
                        </div>
                        <div className="grccmb-table">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th><label>ICode</label></th>
                                        <th><label>ICode Qty</label></th>
                                        <th><label>Inwarded Qty</label></th>
                                        <th><label>Inwarded Qty</label></th>
                                        <th><label>Reason</label></th>
                                        <th><label>Remarks</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label className="bold">DS829389</label></td>
                                        <td><label>120</label></td>
                                        <td><label><input type="text" value="120" /></label></td>
                                        <td><label><input className="grc-retailer-pend" type="text" value="0" /></label></td>
                                        <td><label>
                                            <select>
                                                <option>QC Rejected</option>
                                                <option>Short Qty</option>
                                                <option>Others</option>
                                            </select>
                                        </label></td>
                                        <td><label><input className="grc-remark-input" type="text" /></label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">DS829389</label></td>
                                        <td><label>120</label></td>
                                        <td><label><input type="text" value="120" /></label></td>
                                        <td><label><input className="grc-retailer-pend" type="text" value="0" /></label></td>
                                        <td><label>
                                            <select>
                                                <option>QC Rejected</option>
                                                <option>Short Qty</option>
                                                <option>Others</option>
                                            </select>
                                        </label></td>
                                        <td><label>Not Required</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">DS829389</label></td>
                                        <td><label>120</label></td>
                                        <td><label><input type="text" value="120" /></label></td>
                                        <td><label><input className="grc-retailer-pend" type="text" value="0" /></label></td>
                                        <td><label>Not Required</label></td>
                                        <td><label><input className="grc-remark-input" type="text" /></label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">DS829389</label></td>
                                        <td><label>120</label></td>
                                        <td><label><input type="text" value="120" /></label></td>
                                        <td><label><input className="grc-retailer-pend" type="text" value="0" /></label></td>
                                        <td><label>Not Required</label></td>
                                        <td><label>Not Required</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">DS829389</label></td>
                                        <td><label>120</label></td>
                                        <td><label><input type="text" value="120" /></label></td>
                                        <td><label><input className="grc-retailer-pend" type="text" value="0" /></label></td>
                                        <td><label>Not Required</label></td>
                                        <td><label>Not Required</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">DS829389</label></td>
                                        <td><label>120</label></td>
                                        <td><label><input type="text" value="120" /></label></td>
                                        <td><label><input className="grc-retailer-pend" type="text" value="0" /></label></td>
                                        <td><label>Not Required</label></td>
                                        <td><label>Not Required</label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="new-gen-pagination">
                            <div className="ngp-left">
                                <div className="table-page-no">
                                    <span>Page :</span><label className="paginationBorder">01</label>
                                </div>
                            </div>
                            <div className="ngp-right">
                                <div className="nt-btn">
                                    <Pagination />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="grccm-footer">
                        <div className="grccmh-btn">
                            <button type="button" className="grc-confirm-btn">Confirm</button>
                            <button type="button" onClick={this.props.handleConfirmGrc}>Discard</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default RetailerConfirmGrc;