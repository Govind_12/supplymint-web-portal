import React from 'react';

class QcDateConfirmModal extends React.Component {
    constructor(props) {
        super(props);
    }

    shipedData = (e) => {
        var active = this.props.active
        active.map((data) => data.status = "SHIPMENT_CONFIRMED")
        let payload = {
            shipmentStatusList: active
        }
        this.props.shipmentConfirmCancelRequest(payload)
        this.props.CancelShipment()
        this.props.closeQc()
    }

    render() {
        return (
            <div className="confirmModalSize modal  display_block " id="editVendorModal">
                <div className="backdrop display_block"></div>
                <div className=" display_block confirmModalShipment">
                    <div className="modal-content height100">
                        <div className="col-md-12 col-sm-12 col-xs-12">
                            <div className="headingQc">
                                <h2>Confirmation</h2>
                                <label>are you sure to confirm the shipment request?</label>
                            </div>
                            {/* <div className="paraQc">
                                <p>You haven't done quality check for this order. Do you want to proceed without QC or want to do it later?</p>
                            </div>
                            <div className="bodyQc">
                                <h3>
                                    Quality Investigation valid upto
                                </h3>
                                <input type="date" className="inputQc" />
                            </div> */}
                            <div className="footer_shipment">
                                <button type="button" className="shipmentShip" onClick={this.shipedData}>Yes</button>
                                <button type="button" className="shipmentCancle" onClick={(e)=>this.props.closeQc(e)}>No</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default QcDateConfirmModal