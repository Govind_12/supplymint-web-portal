import React from 'react'

class VendorTransporter extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            transportarData: [],
            pageNo: 1,
            type: 1,
        }

    }
    
    componentDidMount(){
        this.setState({
            transportarData: this.props.transportarData.resource,
            prev: this.props.transportarData.prePage,
            current: this.props.transportarData.currPage !== undefined ? this.props.transportarData.currPage : 0,
            next: this.props.transportarData.currPage + 1,
            maxPage: this.props.transportarData.maxPage !== undefined ? this.props.transportarData.maxPage : 0,
        })
        this.props.onRef(this);
    }

    componentWillUnmount() {
        this.props.onRef(undefined);
    }

    page = (e) =>{
        if (e.target.id == "prev") {
            if (this.props.transportarData.currPage != 0) {
                this.setState({
                    type: 1,
                    pageNo: this.props.transportarData.currPage - 1,
                },()=> this.props.openTransporterModal(e, 'page', this.props.transportarData.currPage - 1)) 
            }
        } else if (e.target.id == "next") {
            if (this.props.transportarData.currPage != this.props.transportarData.maxPage) {
                this.setState({
                    type: 1,
                    pageNo: this.props.transportarData.currPage + 1,
                },()=> this.props.openTransporterModal(e, 'page', this.props.transportarData.currPage + 1)) 
            }
        }
    }

    render() {
        return(
            <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                <div className="dropdown-modal-header">
                    <span className="vd-name div-col-2">Transporter/Courier Co. Code</span>
                    <span className="vd-name div-col-2">Transporter/Courier Co. Name</span>
                </div>
                <ul className="dropdown-menu-city-item">
                    {this.state.transportarData == undefined || this.state.transportarData.length == 0 ? <li><span>No Data Found</span></li> 
                        : this.state.transportarData.map((data, key) => (
                            <li key={key} id={data.transporterCode} onClick={(e)=>this.props.onSelectTransportar(e, data)}>
                                <span className="vendor-details">
                                    <span className="vd-name div-col-2">{data.transporterCode}</span>
                                    <span className="vd-loc div-col-2">{data.transporterName}</span>
                                </span>
                            </li>)
                        )
                    }
                    {/* <li key="other" id="other" onClick={this.props.onSelectTransportar}>Other</li> */}
                </ul>
                <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" id="btn-close" onClick={this.props.onCloseTransporter}>Close</button>
                    </div>
                    <div className="page-next-prew-btn">
                        {this.state.prev != 0 ? <button className="pnpb-prev" type="button" id="prev" onClick={this.page}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button> :
                        <button className="pnpb-prev" type="button" id="prev" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button> }
                        <span className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</span>
                        { this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" id="next" onClick={this.page}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button> :
                        <button className="pnpb-next" type="button" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button>}
                    </div>
                </div>
            </div>
        )
    }
}

export default VendorTransporter;