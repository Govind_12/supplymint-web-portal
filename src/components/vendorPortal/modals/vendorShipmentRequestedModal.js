import React, { Component } from 'react';
import exclaimIcon from '../../../assets/exclain.svg';


export default class VendorShipmentRequestedModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shipmentDetails: [],
            lineItemArray: [],
            shipmentRequstDate: ""
            // shipmentRequstDate: moment(new Date).format("YYYY-MM-DD"),
        }
    }

    render() {
        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="display_block">
                        <div className="col-md-12 col-sm-12 modal-content modalShow pad-0 createShipmentModal">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle">
                                    <div className="col-md-6 pad-0">
                                        <h2 className="displayInline m-rgt-20">Shipment Request</h2>
                                        {/* <span className="shipmentNo m-rgt-20">{ this.state.shipmentDetails.orderNumber}</span> */}
                                        <button type="button" className="printbarCode">Print Barcode</button>
                                    </div>
                                    <div className="col-md-6 pad-0">
                                        <div className="float_Right">
                                            <button type="button" className="discardBtns m-rgt-10" onClick={this.props.handleShipmentModal}>Cancel</button>
                                            <button type="button" className="saveBtnBlue" onClick={this.createFinalShipment}>Save</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="modalOpertions">
                                    <h3>PO Details</h3>
                                    <div className="col-md-12 pad-0">
                                        <div className="shipment-col displayFlex">
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>P.O Number</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{this.state.shipmentDetails.orderNumber}</h5> */}
                                            </div>

                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>P.O Date</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{this.state.shipmentDetails.poDate.split("T")[0]}</h5> */}
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>P.O Valid From</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{this.state.shipmentDetails.validFrom.split("T")[0]}</h5> */}
                                            </div>
                                        </div>
                                        <div className="shipment-col displayFlex">
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>P.O Valid To</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{this.state.shipmentDetails.validTo.split("T")[0]}</h5> */}
                                            </div>

                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>Vendor Name</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{this.state.shipmentDetails.vendorName}</h5> */}
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>Delivery Site</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{this.state.shipmentDetails.siteDetails} </h5> */}
                                            </div>
                                        </div>
                                        <div className="shipment-col displayFlex">
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>Transpoter</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{this.state.shipmentDetails.transporterName}</h5> */}
                                            </div>

                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>ASN Number</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{this.state.shipmentDetails.shipmentAdviceCode}</h5> */}
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>Requested on</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                {/* <h5>{moment(today).format("YYYY-MM-DD")}</h5> */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="m-top-5 verticalInlineFlex">
                                        <label className="m-bot-0 m-rgt-10">Shipment Request Date *</label>
                                        <input type="date" />
                                    </div>
                                </div>

                                <div className="modalContentMid">
                                    <div className="col-md-12 col-sm-12 pad-0 modalTableNew">
                                        <h3>Items</h3>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 tableGeneric bordere3e7f3 mainRoleTable vendorTable cancelPoTable shipmentTable m-top-15">
                                            <div className="zui-wrapper">
                                                <div className="scrollableOrgansation scrollableTableFixed maxheight215 heightAuto">
                                                    <table className="table zui-table roleTable border-bot-table">
                                                        <thead>
                                                            <tr>
                                                                <th><label>Order Qty</label></th>
                                                                <th><label> Request Qty</label></th>
                                                                

                                                                {/* <th><label> Total Requested Qty</label></th>
                                                                <th><label> Total Cancelled Qty</label></th>
                                                                <th><label> New Request Qty</label></th> */}

                                                                <th><label> Cancel Qty</label></th>
                                                                <th><label> Pending Qty </label></th>


                                                                <th><label>Item Code</label></th>
                                                                <th><label>Item Name</label></th>
                                                                <th><label>Division</label> </th>
                                                                <th> <label>Section</label></th>
                                                                <th> <label>Department</label></th>
                                                                <th> <label>Article</label></th>
                                                                <th><label>Category 1</label></th>
                                                                <th><label>Category 2</label></th>
                                                                <th><label>Category 3</label></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.lineItemArray.length !== 0 ?
                                                                this.state.lineItemArray.map((data, key) => (
                                                                    <tr key={key}>
                                                                        <td><label>{data.orderQty}</label></td>
                                                                        <td><input type="text" id="requestedQty" className={parseInt(this.state.lineItemArray[key].requestedQty) > parseInt(data.orderQty) ? "active errorBorder" : "active"} onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data.requestedQty} value={this.state.lineItemArray[key].id == data.id && this.state.lineItemArray[key].requestedQty} /></td>

                                                                        {/* <td><label>{data.totalRequestedQty}</label></td>
                                                                        <td><label>{data.totalCancelledQty}</label></td>
                                                                        <td><input type="text" id="newRequestedQty" className={parseInt(this.state.lineItemArray[key].orderQty) > (data.totalReqestedQty + data.totalCancelQty) ? "active errorBorder" : "active"} onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data.cancelQty} value={this.state.lineItemArray[key].id == data.id && this.state.lineItemArray[key].cancelQty} /></td> */}


                                                                        <td><input type="text" id="cancelQty" className={ parseInt(this.state.lineItemArray[key].cancelledQty) && ( parseInt(this.state.lineItemArray[key].orderQty) -  (parseInt(this.state.lineItemArray[key].requestedQty)) < parseInt(this.state.lineItemArray[key].cancelledQty)) ? "active errorBorder" : "active"} onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data.cancelledQty} value={this.state.lineItemArray[key].id == data.id && this.state.lineItemArray[key].cancelledQty} /></td>
                                                                        <td><label>{this.state.lineItemArray[key].pendingQty}</label></td>
                                                                        {/* <td><input type="text" id="pendingQty" className={(parseInt(this.state.lineItemArray[key].orderQty) - parseInt(this.state.lineItemArray[key].requestedQty))   < (parseInt(this.state.lineItemArray[key].pendingQty)) ? "active errorBorder" : "active"} onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data.pendingQty} value={this.state.lineItemArray[key].id == data.id && this.state.lineItemArray[key].pendingQty} /></td> */}

                                                                        <td><label >{data.itemCode} </label> </td>
                                                                        <td> <label className="textBreak">{data.itemName}</label></td>
                                                                        <td><label>{data.hl1Name}</label> </td>
                                                                        <td> <label> {data.hl2Name} </label></td>
                                                                        <td> <label> {data.hl3Name}</label></td>
                                                                        <td> <label> {data.hl4Name}</label></td>
                                                                        <td> <label> {data.cat1Name} </label></td>
                                                                        <td> <label> {data.cat2Name} </label></td>
                                                                        <td> <label> {data.cat3Name} </label></td>
                                                                    </tr>
                                                                ))
                                                                : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modalBottom">
                                    <p className="note displayFlex"><img src={exclaimIcon} />Note: Requested quantity can not be more than order quantity</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
