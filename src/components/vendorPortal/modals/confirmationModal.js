import React from "react";
import CancelIcon from "../../../assets/cancel.svg";

class ConfirmationModal extends React.Component {
    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content cancel-request-modal confirmation-modal">
                    <div className="crm-alert-icon">
                        <img src={CancelIcon} />
                    </div>
                    <div className="crm-body">
                        <span>Are you sure you want to Change the ASN Valid To Date</span>
                    </div>
                    <div className="crm-footer avde-bottom">
                        <button className="confirm-btn" type="button">Confirm</button>
                        <button  type="button">Discard</button>
                    </div>
                </div>
            </div>
        )
    }   
}
export default ConfirmationModal;