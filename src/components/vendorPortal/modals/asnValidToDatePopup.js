import React from "react";
import moment from "moment";

class AsnValidToDatePopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    handleDateRemark = (e, id) => {
        // this.setState({ extendDateRemark: event.target.value })
        let checkedShipmentData = [...this.props.checkedShipmentData]
        checkedShipmentData.map( data => data.id == id && (data.remarks = e.target.value))
        this.setState({ checkedShipmentData });
            
        //this.props.changeExtensionDate(event, _, "textarea")
    }

    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content asn-valid-date-extention">
                    <div className="avde-head">
                        <h3>Update ASN Valid to Date</h3>
                    </div>
                    <div className="avde-body">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th><label>PO Number</label></th>
                                    <th><label>ASN Number</label></th>
                                    <th className="avdeb-rap"><label>Current ASN Valid to Date</label></th>
                                    <th><label>New ASN Valid to Date</label></th>
                                    <th><label>Remark *</label></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.checkedShipmentData.length > 0 && this.props.checkedShipmentData.map((_, key) => (
                                    <tr key={key}>
                                        <td><label>{_.orderNumber}</label></td>
                                        <td><label>{_.shipmentAdviceNo}</label></td>
                                        <td><label>{_.validTo}</label></td>
                                        <td><input type="date" placeholder={_.newDate == "" ? "Select Date" : _.newDate} min={moment(_.validTo).add('1','days').format("YYYY-MM-DD")} value={_.newDate} onChange={(event) => this.props.changeExtensionDate(event, _, "input")} /></td>
                                        <td><input type="text" placeholder="" name={_.id+"dateRemarkdateRemark"} id={_.id+"dateRemarkdateRemark"} value={_.remarks} onChange={(event) => this.handleDateRemark(event, _.id)} /></td>
                                        <td>
                                            <div className="table-delete-icon">
                                                <svg onClick={(event) => this.props.changeExtensionDate(event, _, "remove")} xmlns="http://www.w3.org/2000/svg" width="14" height="15.391" viewBox="0 0 12.826 15.391">
                                                    <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M6.489 12.185a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm3.207-10.9v1.28H2V1.283h3.663c.577 0 1.046-.7 1.046-1.283h3.409c0 .578.468 1.283 1.046 1.283zM12.9 3.848v10.261H3.924V3.848H2.641v11.543h11.544V3.848z" transform="translate(-2)" />
                                                </svg>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <div className="avde-bottom">
                        {this.props.checkedShipmentData.every((_) => _.newDate !== "" && _.remarks != "") ? <button type="button" className="confirm-btn" onClick={() => this.props.changeExtensionDate("", "", "confirm")}>Confirm</button>
                            : <button type="button" className="confirm-btn btnDisabled">Confirm</button>}
                        <button type="button" onClick={this.props.handledateExtensionModal}>Discard</button>
                    </div>
                </div>
            </div >
        )
    }
}

export default AsnValidToDatePopup;