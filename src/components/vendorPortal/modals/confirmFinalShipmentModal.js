import React from 'react';

class ConfirmFinalShipmentModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="modal  display_block " id="editVendorModal">
                <div className="backdrop display_block"></div>
                <div className=" display_block confirmModalShipment">
                    <div className="modal-content ">
                        <div className="col-md-12 col-sm-12 col-xs-12">
                            <div className="headingShip">
                                <h2>Confirmation</h2>
                            </div>
                            <div className="paraShipment">
                                <p>Do you want to proceed the request for final shipment.</p>
                            </div>
                           
                            <div className="footer_shipment">
                                <button type="button" className="shipmentShip" >Ship</button>
                                <button type="button" className="shipmentCancle" >Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default ConfirmFinalShipmentModal