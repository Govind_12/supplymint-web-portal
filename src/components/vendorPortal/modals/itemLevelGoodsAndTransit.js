import React, { Component } from 'react';
import ItemLevelConfigHeader from '../itemLevelConfigHeader';
import ItemLevelConfirmationModal from '../itemLevelConfirmationModal';

export default class ItemLevelDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ItemLvlGetHeaderConfig: [],
            ItemLvlFixedHeader: [],
            ItemLvlCustomHeaders: {},
            ItemLvlHeaderConfigState: {},
            ItemLvlHeaderConfigDataState: {},
            ItemLvlFixedHeaderData: [],
            ItemLvlCustomHeadersState: [],
            ItemLvlHeaderState: {},
            ItemLvlHeaderSummary: [],
            ItemLvlDefaultHeaderMap: [],
            ItemLvlConfirmModal: false,
            ItemLvlHeaderMsg: "",
            ItemLvlParaMsg: "",
            ItemLvlHeaderCondition: false,
            ItemLvlSaveState: [],
            ItemLvlColoumSetting: false,
            actionExpand: false,
            expandedId: 0,
            dropOpen: true,
            setBaseExpandDetails: [],
            setRequestedQty: "",
            setBasedArray: [],
            mandateHeaderItem: []
        }
    }
    componentDidMount() {
        
        if (!this.props.replenishment.getItemHeaderConfig.isSuccess) {
            if(this.props.poType == "poicode"){
                let payload = {
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                    attributeType: "TABLE HEADER",
                    displayName: this.props.item_Display,
                    basedOn: "SET"
                }
                this.props.getItemHeaderConfigRequest(payload)
            }else{
                let payload = {
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                    attributeType: "TABLE HEADER",
                    displayName: this.props.set_Display,
                    basedOn: "SET"
                }
                this.props.getItemHeaderConfigRequest(payload)
            }
        }
    }

     static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "ITEM") {
                let ItemLvlGetHeaderConfig = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]).length != 0 ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let ItemLvlFixedHeader = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) != 0 ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let ItemLvlCustomHeadersState = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]).length != 0 ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                return {
                    ItemLvlCustomHeaders: prevState.ItemLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    ItemLvlGetHeaderConfig,
                    ItemLvlFixedHeader,
                    ItemLvlCustomHeadersState,
                    ItemLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    ItemLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    ItemLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    ItemLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    ItemLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderItem: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                }
            }
        }
        return {}
    } 
    ItemLvlOpenColoumSetting(data) {
        if (this.state.ItemLvlCustomHeadersState.length == 0) {
            this.setState({
                ItemLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                ItemLvlColoumSetting: true
            })
        } else {
            this.setState({
                ItemLvlColoumSetting: false
            })
        }
    }
    ItemLvlpushColumnData(data) {
        let ItemLvlGetHeaderConfig = this.state.ItemLvlGetHeaderConfig
        let ItemLvlCustomHeadersState = this.state.ItemLvlCustomHeadersState
        let ItemLvlHeaderConfigDataState = this.state.ItemLvlHeaderConfigDataState
        let ItemLvlCustomHeaders = this.state.ItemLvlCustomHeaders
        let ItemLvlSaveState = this.state.ItemLvlSaveState
        let ItemLvlDefaultHeaderMap = this.state.ItemLvlDefaultHeaderMap
        let ItemLvlHeaderSummary = this.state.ItemLvlHeaderSummary
        let ItemLvlFixedHeaderData = this.state.ItemLvlFixedHeaderData
        if (this.state.ItemLvlHeaderCondition) {

            if (!data.includes(ItemLvlGetHeaderConfig) || ItemLvlGetHeaderConfig.length == 0) {
                ItemLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(ItemLvlHeaderConfigDataState))) {

                    let invert = _.invert(ItemLvlFixedHeaderData)

                    let keyget = invert[data];

                    Object.assign(ItemLvlCustomHeaders, { [keyget]: data })
                    ItemLvlSaveState.push(keyget)
                }

                if (!Object.keys(ItemLvlCustomHeaders).includes(ItemLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(ItemLvlFixedHeaderData))[data];


                    ItemLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {

            if (!data.includes(ItemLvlCustomHeadersState) || ItemLvlCustomHeadersState.length == 0) {

                ItemLvlCustomHeadersState.push(data)

                if (!ItemLvlCustomHeadersState.includes(ItemLvlHeaderConfigDataState)) {

                    let keyget = (_.invert(ItemLvlFixedHeaderData))[data];


                    Object.assign(ItemLvlCustomHeaders, { [keyget]: data })
                    ItemLvlSaveState.push(keyget)

                }

                if (!Object.keys(ItemLvlCustomHeaders).includes(ItemLvlHeaderSummary)) {

                    let keygetvalue = (_.invert(ItemLvlFixedHeaderData))[data];

                    ItemLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            ItemLvlGetHeaderConfig,
            ItemLvlCustomHeadersState,
            ItemLvlCustomHeaders,
            ItemLvlSaveState,
            ItemLvlDefaultHeaderMap,
            ItemLvlHeaderSummary
        })
    }
    ItemLvlCloseColumn(data) {
        let ItemLvlGetHeaderConfig = this.state.ItemLvlGetHeaderConfig
        let ItemLvlHeaderConfigState = this.state.ItemLvlHeaderConfigState
        let ItemLvlCustomHeaders = []
        let ItemLvlCustomHeadersState = this.state.ItemLvlCustomHeadersState
        let ItemLvlFixedHeaderData = this.state.ItemLvlFixedHeaderData
        if (!this.state.ItemLvlHeaderCondition) {
            for (let j = 0; j < ItemLvlCustomHeadersState.length; j++) {
                if (data == ItemLvlCustomHeadersState[j]) {
                    ItemLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in ItemLvlFixedHeaderData) {
                if (!ItemLvlCustomHeadersState.includes(ItemLvlFixedHeaderData[key])) {
                    ItemLvlCustomHeaders.push(key)
                }
            }
            if (this.state.ItemLvlCustomHeadersState.length == 0) {
                this.setState({
                    ItemLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < ItemLvlGetHeaderConfig.length; i++) {
                if (data == ItemLvlGetHeaderConfig[i]) {
                    ItemLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in ItemLvlFixedHeaderData) {
                if (!ItemLvlGetHeaderConfig.includes(ItemLvlFixedHeaderData[key])) {
                    ItemLvlCustomHeaders.push(key)
                }
            }
        }
        ItemLvlCustomHeaders.forEach(e => delete ItemLvlHeaderConfigState[e]);
        this.setState({
            ItemLvlGetHeaderConfig,
            ItemLvlCustomHeaders: ItemLvlHeaderConfigState,
            ItemLvlCustomHeadersState,
        })
        setTimeout(() => {
            let keygetvalue = (_.invert(this.state.ItemLvlFixedHeaderData))[data];
            let ItemLvlSaveState = this.state.ItemLvlSaveState
            ItemLvlSaveState.push(keygetvalue)
            let ItemLvlHeaderSummary = this.state.ItemLvlHeaderSummary
            let ItemLvlDefaultHeaderMap = this.state.ItemLvlDefaultHeaderMap
            if (!this.state.ItemLvlHeaderCondition) {
                for (let j = 0; j < ItemLvlHeaderSummary.length; j++) {
                    if (keygetvalue == ItemLvlHeaderSummary[j]) {
                        ItemLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < ItemLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == ItemLvlDefaultHeaderMap[i]) {
                        ItemLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                ItemLvlHeaderSummary,
                ItemLvlDefaultHeaderMap,
                ItemLvlSaveState
            })
        }, 100);
    }
    ItemLvlsaveColumnSetting(e) {
        this.setState({
            ItemLvlColoumSetting: false,
            ItemLvlHeaderCondition: false,
            ItemLvlSaveState: []
        })
        let payload = {
            basedOn: "ITEM",
            module: "SHIPMENT TRACKING",
            subModule: this.props.subModule,
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.item_Display,

            fixedHeaders: this.state.ItemLvlFixedHeaderData,

            defaultHeaders: this.state.ItemLvlHeaderConfigDataState,

            customHeaders: this.state.ItemLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    ItemLvlResetColumnConfirmation() {
        this.setState({
            ItemLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // ItemLvlParaMsg: "Click confirm to continue.",
            ItemLvlConfirmModal: true,
        })
    }
    ItemLvlResetColumn() {
        let payload = {
            basedOn: "ITEM",
            module: "SHIPMENT TRACKING",
            subModule: this.props.subModule,
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.item_Display,

            fixedHeaders: this.state.ItemLvlFixedHeaderData,

            defaultHeaders: this.state.ItemLvlHeaderConfigDataState,

            customHeaders: this.state.ItemLvlHeaderConfigDataState,

        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            ItemLvlHeaderCondition: true,
            ItemLvlColoumSetting: false,
            ItemLvlSaveState: []
        })
    }
    ItemLvlCloseConfirmModal(e) {
        this.setState({
            ItemLvlConfirmModal: !this.state.ItemLvlConfirmModal,
        })
    }
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createItemHeaderConfig.isSuccess) {
            if(this.props.poType == "poicode"){
                this.props.getItemHeaderConfigRequest(this.props.itemHeaderPayload)
            }else{
                this.props.getItemHeaderConfigRequest(this.props.setHeaderPayload)
            }
        }
    }
    render() {
        const { search, status } = this.state
        return (
            <div>
                <div className="item-detail-table-expend">
                    <div className="idte-inner">
                        {/* <ItemLevelConfigHeader {...this.props} {...this.state} ItemLvlColoumSetting={this.state.ItemLvlColoumSetting} ItemLvlGetHeaderConfig={this.state.ItemLvlGetHeaderConfig} ItemLvlResetColumnConfirmation={(e) => this.ItemLvlResetColumnConfirmation(e)} ItemLvlOpenColoumSetting={(e) => this.ItemLvlOpenColoumSetting(e)} ItemLvlCloseColumn={(e) => this.ItemLvlCloseColumn(e)} ItemLvlpushColumnData={(e) => this.ItemLvlpushColumnData(e)} ItemLvlsaveColumnSetting={(e) => this.ItemLvlsaveColumnSetting(e)} />
                         */}
                        <div id="expandedTable" ref={node => this.expandTableref = node}>
                            <table className="table">
                                <thead>
                                    <tr>
                                        {/* {this.state.ItemLvlCustomHeadersState.length == 0 ? this.state.ItemLvlGetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.state.ItemLvlCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))} */}
                                        {this.props.itemCustomHeadersState.length == 0 ? this.props.getItemHeaderConfig.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            )) : this.props.itemCustomHeadersState.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            ))}
                                    </tr> 
                                </thead>
                                <tbody>
                                    {this.props.itemBaseArray.length != 0 ? this.props.itemBaseArray.map((data, key) =>
                                        <tr key={key}>
                                            {/* {this.state.ItemLvlHeaderSummary.length == 0 ? this.state.ItemLvlDefaultHeaderMap.map((hdata, key) => (
                                                <td key={key} >
                                                    <label>{data[hdata]}</label>
                                                </td>
                                            )) : this.state.ItemLvlHeaderSummary.map((sdata, keyy) => (
                                                <td key={keyy} >
                                                    <label>{data[sdata]}</label>
                                                </td>
                                            ))} */}
                                            {this.props.itemHeaderSummary.length == 0 ? this.props.itemDefaultHeaderMap.map((hdata, key) => (
                                                        <td key={key} >
                                                            <label>{data[hdata]}</label>
                                                        </td>
                                                    )) : this.props.itemHeaderSummary.map((sdata, keyy) => (
                                                        <td key={keyy} >
                                                            <label>{data[sdata]}</label>
                                                        </td>
                                                    ))}
                                        </tr>
                                    ) : <tr className="tableNoData"><td> NO DATA FOUND </td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {this.state.ItemLvlConfirmModal ? <ItemLevelConfirmationModal {...this.state} {...this.props} ItemLvlCloseConfirmModal={(e) => this.ItemLvlCloseConfirmModal(e)} ItemLvlResetColumn={(e) => this.ItemLvlResetColumn(e)} /> : null}
                </div >
            </div>
        )
    }
}
