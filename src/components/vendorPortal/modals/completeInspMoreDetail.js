import React from 'react';

class CompleteInspMoreDetail extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            poDetailsShipment: [],
        }
    }

    componentDidMount() {
        let payload = {
            orderId: this.props.moreDetailsData.orderId,
            setHeaderId: "",
            detailType: this.props.moreDetailsData.poType == "poicode" ? "item" : "set",
            poType: this.props.moreDetailsData.poType,
            shipmentStatus: "PENDING_QC",
            shipmentId: this.props.moreDetailsData.shipmentId
        }
        this.props.getCompleteDetailShipmentRequest(payload)
        Object.keys(document.getElementById("selectPPS").options).map( key => {
            if( this.props.moreDetailsData.selectPPS == document.getElementById("selectPPS").options[key].value)
               document.getElementById("selectPPS").options[key].selected = true;
        })
        Object.keys(document.getElementById("selectTestReport").options).map( key => {
            if( this.props.moreDetailsData.selectTestReport == document.getElementById("selectTestReport").options[key].value)
               document.getElementById("selectTestReport").options[key].selected = true;
        })
        Object.keys(document.getElementById("selectFitTag").options).map( key => {
            if( this.props.moreDetailsData.selectFitTag == document.getElementById("selectFitTag").options[key].value)
               document.getElementById("selectFitTag").options[key].selected = true;
        })
        Object.keys(document.getElementById("selectInspType").options).map( key => {
            if( this.props.moreDetailsData.selectInspType == document.getElementById("selectInspType").options[key].value)
               document.getElementById("selectInspType").options[key].selected = true;
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getCompleteDetailShipment.isSuccess && nextProps.shipment.getCompleteDetailShipment.data.resource != null) {
            this.setState({
                poDetailsShipment: nextProps.shipment.getCompleteDetailShipment.data.resource.poDetails
            })
        }
    }

    isMandate =(data)=>{
        if(data.QCStatus == "Pass" || data.QCStatus == "Fail" || data.QCStatus == "hold")
           return true
    }

    render() {
        const{poDetailsShipment} = this.state
        return(
            <div className="confirm-insp-details">
                <div className="cid-head-details">
                    <img onClick={this.props.closeMoreDetails} src={require('../../../assets/clearSearch.svg')} />
                    <h3>More Details</h3>
                </div>
                <div className="cid-body">
                    <div className="cidb-row">
                        {/* <div className="cidbr-inner">
                            <h5>PO Number</h5>
                            <span className="cidbri-date">PO000451-0620BSP</span>
                        </div> */}
                        <div className="cidbr-inner">
                            <h5>Department</h5>
                            {poDetailsShipment.map( (data, key) => <span className="cidbri-date">{key == 0 && data.hl3Name}</span>)}
                        </div>
                        <div className="cidbr-inner">
                            <h5>Item Description</h5>
                            {poDetailsShipment.map( (data, key) => <span className="cidbri-date">{key == 0 && data.hl4Name}</span>)}
                        </div>
                    </div>
                    <div className="cidb-row">
                        <div className="cidbr-inner">
                            <h5>PPS{(this.props.moreDetailsData.QCStatus == "Pass" || this.props.moreDetailsData.QCStatus == "Fail" || this.props.moreDetailsData.QCStatus == "hold") && <span className="mandatory">*</span>}</h5>
                            <select id="selectPPS" onChange={(e) => this.props.handleFields(e, this.props.moreDetailsData.id, "selectPPS")}>
                                <option key="0" value="">Select</option>
                                <option key="1" value="yes">Yes</option>
                                <option key="2" value="no">No</option>
                            </select>
                            {this.props.moreDetailsData.selectPPSErr && <span className="error">Select PPS</span>}
                        </div>
                        <div className="cidbr-remark">
                            <h5>Remark{(this.props.moreDetailsData.QCStatus == "Pass" || this.props.moreDetailsData.QCStatus == "Fail" || this.props.moreDetailsData.QCStatus == "hold") && <span className="mandatory">*</span>}</h5>
                            <input type="text" value={this.props.moreDetailsData.ppsRemark} onChange={(e) => this.props.handleFields(e, this.props.moreDetailsData.id, "ppsRemark")}/>
                            {this.props.moreDetailsData.ppsRemarkErr && <span className="error">Enter PPS Remark</span>}
                        </div>
                    </div>
                    <div className="cidb-row">
                        <div className="cidbr-inner">
                            <h5>Test Report{(this.props.moreDetailsData.QCStatus == "Pass" || this.props.moreDetailsData.QCStatus == "Fail" || this.props.moreDetailsData.QCStatus == "hold") && <span className="mandatory">*</span>}</h5>
                            <select id="selectTestReport" onChange={(e) => this.props.handleFields(e, this.props.moreDetailsData.id, "selectTestReport")}>
                                <option key="0" value="">Select</option>
                                <option key="1" value="yes">Yes</option>
                                <option key="2" value="no">No</option>
                            </select>
                            {this.props.moreDetailsData.selectTestReportErr && <span className="error">Select Test Report</span>}
                        </div>
                        <div className="cidbr-remark">
                            <h5>Remark{(this.props.moreDetailsData.QCStatus == "Pass" || this.props.moreDetailsData.QCStatus == "Fail" || this.props.moreDetailsData.QCStatus == "hold") && <span className="mandatory">*</span>}</h5>
                            <input type="text" value={this.props.moreDetailsData.testRemark} onChange={(e) => this.props.handleFields(e, this.props.moreDetailsData.id, "testRemark")}/>
                            {this.props.moreDetailsData.testRemarkErr && <span className="error">Enter Test Remark</span>}
                        </div>
                    </div>
                    <div className="cidb-row">
                        <div className="cidbr-inner">
                            <h5>Fit Tag{(this.props.moreDetailsData.QCStatus == "Pass" || this.props.moreDetailsData.QCStatus == "Fail" || this.props.moreDetailsData.QCStatus == "hold") && <span className="mandatory">*</span>}</h5>
                            <select id="selectFitTag" onChange={(e) => this.props.handleFields(e, this.props.moreDetailsData.id, "selectFitTag")}>
                                <option key="0" value="">Select</option>
                                <option key="1" value="yes">Yes</option>
                                <option key="2" value="no">No</option>
                            </select>
                            {this.props.moreDetailsData.selectFitTagErr && <span className="error">Select Fit Tag</span>}
                        </div>
                        <div className="cidbr-remark">
                            <h5>Remark{(this.props.moreDetailsData.QCStatus == "Pass" || this.props.moreDetailsData.QCStatus == "Fail" || this.props.moreDetailsData.QCStatus == "hold") && <span className="mandatory">*</span>}</h5>
                            <input type="text" value={this.props.moreDetailsData.fitRemark} onChange={(e) => this.props.handleFields(e, this.props.moreDetailsData.id, "fitRemark")}/>
                            {this.props.moreDetailsData.fitRemarkErr && <span className="error">Enter Fit Remark</span>}
                        </div>
                    </div>
                    <div className="cidb-row">
                        <div className="cidbr-remark">
                            <h5>Inspection Type{(this.props.moreDetailsData.QCStatus == "Pass" || this.props.moreDetailsData.QCStatus == "Fail" || this.props.moreDetailsData.QCStatus == "hold") && <span className="mandatory">*</span>}</h5>
                            <select id="selectInspType" onChange={(e) => this.props.handleFields(e, this.props.moreDetailsData.id, "selectInspType")}>
                                <option key="0" value="">Select</option>
                                <option key="1" value="sizeSet">Size Set</option>
                                <option key="2" value="inline">Inline</option>
                                <option key="3" value="midLine">Midline</option>
                                <option key="4" value="final">Final</option>
                                <option key="5" value="reInsp-1">Re-inspection 1</option>
                                <option key="6" value="reInsp-2">Re-inspection 2</option>
                            </select>
                            {this.props.moreDetailsData.selectInspTypeErr && <span className="error">Select Insp Type</span>}
                        </div>
                        <div className="cidbr-inner">
                            <h5>Report Number{(this.props.moreDetailsData.QCStatus == "Pass" || this.props.moreDetailsData.QCStatus == "Fail" || this.props.moreDetailsData.QCStatus == "hold") && <span className="mandatory">*</span>}</h5>
                            <input type="text" value={this.props.moreDetailsData.reportNumber} onChange={(e) => this.props.handleFields(e, this.props.moreDetailsData.id, "reportNumber")}/>
                            {this.props.moreDetailsData.reportNumberErr && <span className="error">Enter Report No.</span>}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default CompleteInspMoreDetail