import React from 'react';

class EmailTranscriptModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidUpdate(){
       // this.toEmailHandle();
    }

    toEmailHandle = () => {
        var input = document.querySelector('input[name="custom-input-toEmail"]'),
            // init Tagify script on the above inputs
            tagify = new Tagify(input, {
                pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                maxTags: 10,
                dropdown: {
                    maxItems: 20,           // <- mixumum allowed rendered suggestions
                    classname: "tags-look", // <- custom classname for this dropdown, so it could be targeted
                    enabled: 0,             // <- show suggestions on focus
                    closeOnSelect: false    // <- do not hide the suggestions dropdown once an item has been selected
                }
            })
        if(document.getElementsByName("custom-input-toEmail").length)    
          tagify.on('add', function (e) {
        })

        var input = document.querySelector('input[name="custom-input-ccEmail"]'),
            // init Tagify script on the above inputs
            tagify2 = new Tagify(input, {
                pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                maxTags: 10,
                dropdown: {
                    maxItems: 20,           // <- mixumum allowed rendered suggestions
                    classname: "tags-look2`", // <- custom classname for this dropdown, so it could be targeted
                    enabled: 0,             // <- show suggestions on focus
                    closeOnSelect: false    // <- do not hide the suggestions dropdown once an item has been selected
                }
            })
    }

    render () {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content comment-email-transcript-modal">
                    <div className="cetm-head">
                        <h3>Email Transcript</h3>
                        <button type="button" onClick={this.props.closeEmailTranscript}><img src={require('../../assets/clearSearch.svg')} /></button>
                    </div>
                    <div className="cetm-body">
                        <form>
                            <div className="eti-form">
                                <div id="toEmail" className="eti-form-inner">
                                    <label>To</label>
                                    <input type="text" onBlur={this.toEmailHandle} name="custom-input-toEmail" placeholder="Type email" />
                                </div>
                                <div id="ccEmail" className="eti-form-inner">
                                    <label>CC</label>
                                    <input type="text" onBlur={this.toEmailHandle} name="custom-input-ccEmail" />
                                </div>
                                <div className="eti-form-inner">
                                    <label>Subject</label>
                                    <input type="text" onChange={(e) => this.props.handleDesc(e, 'subject')} name="subject" value={this.props.subject} className="eti-form-inner-subject"></input>
                                </div>
                                <div className="eti-form-textarea m-top-30">
                                    <label>Remark</label>
                                    <textarea placeholder="Write your remark here" onChange={(e) => this.props.handleDesc(e, 'remark')} name="remark" value={this.props.remark} ></textarea>
                                </div>
                                <div className="eti-form-button m-top-60">
                                    <button type="button" className="eti-form-send-btn" onClick={this.props.submitTranscript}>Send Transcript</button>
                                    <button type="button" className="eti-form-close-btn" onClick={this.props.closeEmailTranscript}>Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default EmailTranscriptModal;