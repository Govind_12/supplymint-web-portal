import React from 'react';
import Json from '../../administration/jsonFile.json';

const allState = Object.keys(Json[0].states)
const allLocations = Json[0].states

class CreateCustomer extends React.Component {

    constructor(props){
        super(props);
        this.state ={
            businessTypeCheck: true,
            individualTypeCheck: false,
            custSalutation: "",
            custSalutationError: false,
            custFirstName: "",
            custFirstNameError: false,
            custLastName: "",
            custLastNameError: false,
            custCompany: "",
            custCompanyError: false,
            custDisplayName: "",
            custDisplayNameError: false,
            custEmail: "",
            custEmailError: false,
            custNumber: "",
            custNumberError: false,
            custWebsite: "",
            custWebsiteError: false,

            gstTreatment: "",
            gstTreatmentError: false,
            gstIn: "",
            gstInError: false,
            gstInPos: "",
            gstInPosError: false,
            currency: "",
            currencyError: false,
            paymentTerms: "",
            paymentTermsError: false,
            taxableCheck: true,
            taxExemptCheck: false,

            billingAndShippingAddSameCheck: false,
            billingCountry: "",
            billingCountryError: false,
            billingState: "",
            billingStateError: false,
            billingCity: "",
            billingCityError: false,
            billingPincode: "",
            billingPincodeError: false,
            billingAddress1: "",
            billingAddress1Error: false,
            billingAddress2: "",
            billingAddress2Error: false,
            billingCities: [],

            shippingCountry: "",
            shippingCountryError: false,
            shippingState: "",
            shippingStateError: false,
            shippingCity: "",
            shippingCityError: false,
            shippingPincode: "",
            shippingPincodeError: false,
            shippingAddress1: "",
            shippingAddress1Error: false,
            shippingAddress2: "",
            shippingAddress2Error: false,
            shippingCities: [],

            remark: "",
            updatedInvoiceManagementCustData: sessionStorage.getItem("updatedInvoiceManagementCustData") == "" ? [] : JSON.parse(sessionStorage.getItem("updatedInvoiceManagementCustData")),
            id: "",
        }
    }

    componentDidMount(){
        this.state.updatedInvoiceManagementCustData.length && this.state.updatedInvoiceManagementCustData.map( data =>{
            this.setState({
                id: data.id,
                businessTypeCheck: data.custType == "Business" ? true : false,
                individualTypeCheck: data.custType == "Individual" ? true : false,
                custSalutation: data.custSalutation == null ? "" : data.custSalutation,
                custFirstName: data.custFirstName == null ? "" : data.custFirstName,
                custLastName: data.custLastName == null ? "" : data.custLastName,
                custCompany: data.custCompany == null ? "" : data.custCompany,
                custDisplayName: data.custDisplayName == null ? "" : data.custDisplayName,
                custNumber: data.custNumber == null ? "" : data.custNumber,
                custEmail: data.custEmail == null ? "" : data.custEmail,
                currency: data.currency == null ? "" : data.currency,
                custWebsite: data.custWebsite == null ? "" : data.custWebsite,

                gstIn: data.gstIn == null ? "" : data.gstIn,
                gstInPos: data.gstInPos == null ? "" : data.gstInPos,
                gstTreatment: data.gstTreatment == null ? "" : data.gstTreatment,
                paymentTerms: data.paymentTerms == null ? "" : data.paymentTerms,
                taxableCheck: data.taxPreference == "Taxable" ? true : false,
                taxExemptCheck: data.taxPreference == "Tax Exempt" ? true : false,

                billingAddress1: data.billingAddress1 == null ? "" : data.billingAddress1,
                billingAddress2: data.billingAddress2 == null ? "" : data.billingAddress2,
                billingCity: data.billingCity == null ? "" : data.billingCity,
                billingCountry: data.billingCountry == null ? "" : data.billingCountry,
                billingPincode: data.billingPincode == null ? "" : data.billingPincode,
                billingState: data.billingState == null ? "" : data.billingState,
    
                shippingAddress1: data.shippingAddress1 == null ? "" : data.shippingAddress1,
                shippingAddress2: data.shippingAddress2 == null ? "" : data.shippingAddress2,
                shippingCity: data.shippingCity == null ? "" : data.shippingCity,
                shippingCountry: data.shippingCountry == null ? "" : data.shippingCountry,
                shippingPincode: data.shippingPincode == null ? "" : data.shippingPincode,
                shippingState: data.shippingState == null ? "" : data.shippingState,

                remarks: data.remarks == null ? "" : data.remarks,
                billingCities: allLocations[data.billingState == null ? "" : data.billingState],
                shippingCities: allLocations[data.shippingState == null ? "" : data.shippingState]
            })
        })
    }

    handleChange =(e, id)=>{
        let value = e.target.value;
        let numberPattern = /^[0-9]+$/;

        if( id == "custType")
           this.setState({ businessTypeCheck: !this.state.businessTypeCheck , individualTypeCheck: !this.state.individualTypeCheck})
        if( id == "custSalutation")
           this.setState({ custSalutation: value == "" ? "Select Solution" : value },()=>this.custSalutationError())
        if( id == "custFirstName")
            this.setState({ custFirstName: value},()=>this.custFirstNameError())
        if( id == "custLastName")
            this.setState({ custLastName: value},()=>this.custLastNameError())
        if( id == "custCompany")
            this.setState({ custCompany: value},()=>this.custCompanyError())
        if( id == "custDisplayName")
            this.setState({ custDisplayName: value},()=>this.custDisplayNameError())
        if( id == "custEmail")
            this.setState({ custEmail: value},()=>this.custEmailError())
        if( id == "custNumber")
            this.setState({ custNumber:  numberPattern.test(value) ? value.length <= 10 ? value : this.state.custNumber : !value.length ? "" : this.state.custNumber},()=>this.custNumberError())
        if( id == "custWebsite")
            this.setState({ custWebsite: value},()=>this.custWebsiteError())

        if( id == "gstTreatment")
            this.setState({ gstTreatment: value == "" ? "Select GST Treatment" : value },()=>this.gstTreatmentError())
        if( id == "gstIn")
            this.setState({ gstIn: value},()=>this.gstInError())    
        if( id == "gstInPos")
            this.setState({ gstInPos: value == "" ? "Select GST Position" : value },()=>this.gstInPosError())
        if( id == "currency")
            this.setState({ currency: value == "" ? "Select Currency" : value },()=>this.currencyError())
        if( id == "paymentTerms")
            this.setState({ paymentTerms: value == "" ? "Select Payment Terms" : value },()=>this.paymentTermsError())
            
        if( id == "taxPreference"){
            this.setState({ taxableCheck: !this.state.taxableCheck , taxExemptCheck: !this.state.taxExemptCheck})
        }

        if( id == "billingCountry")
            this.setState({ billingCountry: value == "" ? "Select Country" : value },()=>this.billingCountryError())
        if( id == "billingState")
            this.setState({ billingState: value == "" ? "Select State" : value, billingCities: allLocations[value] },()=>this.billingStateError())
        if( id == "billingCity")
            this.setState({ billingCity: value == "" ? "Select City" : value },()=>this.billingCityError())
        if( id == "billingPincode")
            this.setState({ billingPincode: value},()=>this.billingPincodeError())
        if( id == "billingAddress1")
            this.setState({ billingAddress1: value},()=>this.billingAddress1Error())
        if( id == "billingAddress2")
            this.setState({ billingAddress2: value},()=>this.billingAddress2Error())

        if( id == "billingAndShippingAddSameCheck")
            this.setState({ billingAndShippingAddSameCheck: !this.state.billingAndShippingAddSameCheck},()=>this.shippingAndBillingSame())

        if( id == "shippingCountry")
            this.setState({ shippingCountry: value == "" ? "Select Country" : value },()=>this.shippingCountryError())
        if( id == "shippingState")
            this.setState({ shippingState: value == "" ? "Select State" : value, shippingCities: allLocations[value] },()=>this.shippingStateError())
        if( id == "shippingCity")
            this.setState({ shippingCity: value == "" ? "Select City" : value },()=>this.shippingCityError())
        if( id == "shippingPincode")
            this.setState({ shippingPincode: value},()=>this.shippingPincodeError())
        if( id == "shippingAddress1")
            this.setState({ shippingAddress1: value},()=>this.shippingAddress1Error())
        if( id == "shippingAddress2")
            this.setState({ shippingAddress2: value},()=>this.shippingAddress2Error())    
        if( id == "remark")
            this.setState({ remark: value}) 

    }

    shippingAndBillingSame =()=>{
        if(this.state.billingAndShippingAddSameCheck){
            this.setState({
                shippingCountry: this.state.billingCountry,
                shippingState: this.state.billingState,
                shippingCity: this.state.billingCity,
                shippingPincode: this.state.billingPincode,
                shippingAddress1: this.state.billingAddress1,
                shippingAddress2: this.state.billingAddress2,
                shippingCities: allLocations[this.state.billingState]
            })
        }else{
            this.setState({
                shippingCountry: "",
                shippingState: "",
                shippingCity: "",
                shippingPincode: "",
                shippingAddress1: "",
                shippingAddress2: ""
            })
        }

        setTimeout(() => {
            this.shippingCountryError()
            this.shippingStateError()
            this.shippingCityError()
            this.shippingPincodeError()
            this.shippingAddress1Error()
            this.shippingAddress2Error()
        }, 100);  
    }

    custSalutationError =()=>{
        this.setState({
            custSalutationError: this.state.custSalutation.trim().length == 0 || this.state.itemUnit == "Select Solution" ? true : false,
        })
    }
    custFirstNameError =()=>{
        this.setState({
            custFirstNameError: this.state.custFirstName.trim().length > 0 ? false : true,
        })
    }
    custLastNameError =()=>{
        this.setState({
            custLastNameError: this.state.custLastName.trim().length > 0 ? false : true,
        })
    }
    custCompanyError =()=>{
        this.setState({
            custCompanyError: this.state.custCompany.trim().length > 0 ? false : true,
        })
    }
    custDisplayNameError =()=>{
        this.setState({
            custDisplayNameError: this.state.custDisplayName.trim().length > 0 ? false : true,
        })
    }
    custEmailError =()=>{
        var emailPattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.setState({
            custEmailError: emailPattern.test(this.state.custEmail) ? false : true
        })
    }
    custNumberError =()=>{
        this.setState({
            custNumberError: this.state.custNumber.length == 10 ? false : true
        })
    }
    custWebsiteError =()=>{
        this.setState({
            custWebsiteError: this.state.custWebsite.trim().length > 0 ? false : true,
        })
    }
    gstTreatmentError =()=>{
        this.setState({
            gstTreatmentError: this.state.gstTreatment.trim().length == 0 || this.state.gstTreatment == "Select GST Treatment" ? true : false,
        })
    }
    gstInError =()=>{
        let gstPattern = /^[0-9]{2}[A-Z]{4}[1-9A-Z]{1}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        this.setState({
            gstInError: gstPattern.test(this.state.gstIn) ? false : true,
        })
    }
    gstInPosError =()=>{
        this.setState({
            gstInPosError: this.state.gstInPos.trim().length == 0 || this.state.gstInPos == "Select GST Position" ? true : false,
        })
    }
    currencyError =()=>{
        this.setState({
            currencyError: this.state.currency.trim().length == 0 || this.state.currency == "Select Currency" ? true : false,
        })
    }
    paymentTermsError =()=>{
        this.setState({
            paymentTermsError: this.state.paymentTerms.trim().length == 0 || this.state.paymentTerms == "Select Payment Terms" ? true : false,
        })
    }

    billingCountryError =()=>{
        this.setState({
            billingCountryError: this.state.billingCountry.trim().length == 0 || this.state.billingCountry == "Select Country" ? true : false,
        })
    }
    billingStateError =()=>{
        this.setState({
            billingStateError: this.state.billingState.trim().length == 0 || this.state.billingState == "Select State" ? true : false,
        })
    }
    billingCityError =()=>{
        this.setState({
            billingCityError: this.state.billingCity.trim().length == 0 || this.state.billingCity == "Select City" ? true : false,
        })
    }
    billingPincodeError =()=>{
        this.setState({
            billingPincodeError: this.state.billingPincode.trim().length == 0 ? true : false,
        })
    }
    billingAddress1Error =()=>{
        this.setState({
            billingAddress1Error: this.state.billingAddress1.trim().length == 0 ? true : false,
        })
    }
    billingAddress2Error =()=>{
        this.setState({
            billingAddress2Error: this.state.billingAddress2.trim().length == 0 ? true : false,
        })
    }
    shippingCountryError =()=>{
        this.setState({
            shippingCountryError: this.state.shippingCountry.trim().length == 0 || this.state.shippingCountry == "Select Country" ? true : false,
        })
    }
    shippingStateError =()=>{
        this.setState({
            shippingStateError: this.state.shippingState.trim().length == 0 || this.state.shippingState == "Select State" ? true : false,
        })
    }
    shippingCityError =()=>{
        this.setState({
            shippingCityError: this.state.shippingCity.trim().length == 0 || this.state.shippingCity == "Select City" ? true : false,
        })
    }
    shippingPincodeError =()=>{
        this.setState({
            shippingPincodeError: this.state.shippingPincode.trim().length == 0 ? true : false,
        })
    }
    shippingAddress1Error =()=>{
        this.setState({
            shippingAddress1Error: this.state.shippingAddress1.trim().length == 0 ? true : false,
        })
    }
    shippingAddress2Error =()=>{
        this.setState({
            shippingAddress2Error: this.state.shippingAddress2.trim().length == 0 ? true : false,
        })
    }

    submitCustomer =(e)=>{
        this.custSalutationError()
        this.custFirstNameError()
        this.custLastNameError()
        this.custCompanyError()
        this.custDisplayNameError()
        this.custEmailError()
        this.custNumberError()
        this.custWebsiteError()

        this.gstTreatmentError()
        this.gstInError()
        this.gstInPosError()
        this.currencyError()
        this.paymentTermsError()

        this.billingCountryError()
        this.billingStateError()
        this.billingCityError()
        this.billingPincodeError()
        this.billingAddress1Error()
        this.billingAddress2Error()

        this.shippingCountryError()
        this.shippingStateError()
        this.shippingCityError()
        this.shippingPincodeError()
        this.shippingAddress1Error()
        this.shippingAddress2Error()

        setTimeout(() => {
            const{custSalutationError,custFirstNameError,custLastNameError,custCompanyError,custDisplayNameError,
                custEmailError,custNumberError,custWebsiteError,gstTreatmentError,gstInError,gstInPosError,
                currencyError,paymentTermsError,billingCountryError,billingStateError,
                billingCityError,billingPincodeError,billingAddress1Error,billingAddress2Error,shippingCountryError,
                shippingStateError,shippingCityError,shippingPincodeError,shippingAddress1Error,shippingAddress2Error,
                businessTypeCheck,custSalutation,custFirstName,custLastName,taxableCheck,custCompany,custDisplayName,
                custNumber,custEmail,currency,custWebsite,gstIn,gstInPos,gstTreatment,paymentTerms,billingAddress1,
                billingAddress2,billingCountry,billingState,billingCity,billingPincode,shippingAddress1,shippingAddress2,
                shippingCountry,shippingCity,shippingState,shippingPincode,remark} = this.state

            if( !custSalutationError && !custFirstNameError && !custLastNameError && !custCompanyError && !custDisplayNameError &&
                !custEmailError && !custNumberError && !custWebsiteError && !gstTreatmentError && !gstTreatmentError &&
                !gstInError && !gstInPosError && !currencyError && !paymentTermsError && !billingCountryError && !billingStateError &&
                !billingCityError && !billingPincodeError && !billingAddress1Error && !billingAddress2Error && !shippingCountryError &&
                !shippingStateError && !shippingCityError && !shippingPincodeError && !shippingAddress1Error && !shippingAddress2Error) 
            {    

                let payload ={
                    id: this.state.id !== "" ? this.state.id : undefined,
                    custType: businessTypeCheck ? "Business" : "Individual",
                    custSalutation: custSalutation,
                    custFirstName: custFirstName,
                    custLastName: custLastName,
                    taxPreference: taxableCheck ? "Taxable" : "Tax Exempt",
                    custCompany: custCompany,
                    custDisplayName: custDisplayName,
                    custNumber: custNumber,
                    custEmail: custEmail,
                    currency: currency,
                    custWebsite: custWebsite,
                    gstIn: gstIn,
                    gstInPos: gstInPos,
                    gstTreatment: gstTreatment,
                    paymentTerms: paymentTerms,
                    billingAddress1: billingAddress1,
                    billingAddress2: billingAddress2,
                    billingCountry: billingCountry,
                    billingState: billingState,
                    billingCity: billingCity,
                    billingPincode: billingPincode,
                    shippingAddress1: shippingAddress1,
                    shippingAddress2: shippingAddress2,
                    shippingCountry: shippingCountry,
                    shippingCity: shippingCity,
                    shippingState: shippingState,
                    shippingPincode: shippingPincode,
                    remarks: remark,
                }
                if( this.state.id == "")
                  this.props.saveInvoiceManagementCustomerRequest(payload)
                else
                  this.props.updateInvoiceManagementCustomerRequest(payload)
            }
        },10);
    }

    clearCustomer =(e)=>{
       this.setState({
            businessTypeCheck: true,
            individualTypeCheck: false,
            custSalutation: "",
            custSalutationError: false,
            custFirstName: "",
            custFirstNameError: false,
            custLastName: "",
            custLastNameError: false,
            custCompany: "",
            custCompanyError: false,
            custDisplayName: "",
            custDisplayNameError: false,
            custEmail: "",
            custEmailError: false,
            custNumber: "",
            custNumberError: false,
            custWebsite: "",
            custWebsiteError: false,

            gstTreatment: "",
            gstTreatmentError: false,
            gstIn: "",
            gstInError: false,
            gstInPos: "",
            gstInPosError: false,
            currency: "",
            currencyError: false,
            paymentTerms: "",
            paymentTermsError: false,
            taxableCheck: true,
            taxExemptCheck: false,

            billingAndShippingAddSameCheck: false,
            billingCountry: "",
            billingCountryError: false,
            billingState: "",
            billingStateError: false,
            billingCity: "",
            billingCityError: false,
            billingPincode: "",
            billingPincodeError: false,
            billingAddress1: "",
            billingAddress1Error: false,
            billingAddress2: "",
            billingAddress2Error: false,
            billingCities: [],

            shippingCountry: "",
            shippingCountryError: false,
            shippingState: "",
            shippingStateError: false,
            shippingCity: "",
            shippingCityError: false,
            shippingPincode: "",
            shippingPincodeError: false,
            shippingAddress1: "",
            shippingAddress1Error: false,
            shippingAddress2: "",
            shippingAddress2Error: false,
            shippingCities: [],

            remark: "",
       })
    }

    onChangeFun =(e)=>{
        //No need just for ignore warning.
    }

    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/invoiceManagement/customers")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="get-details" onClick={this.submitCustomer}>Save</button>
                                <button type="button" className="ngr-clear" onClick={this.clearCustomer}>Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="create-customer-details">
                        <div className="col-lg-12 pad-0 m-top-20">
                            <div className="gen-pi-formate">
                                <div className="gpf-dbs">
                                    <h3>Customer Type</h3>
                                    <ul className="gpf-radio-list m-top-20">
                                        <li>
                                            <label className="gen-radio-btn">
                                                <input type="radio" name="businessTypeCheck" checked={this.state.businessTypeCheck} onClick={(e)=>this.handleChange(e, "custType")} onChange={this.onChangeFun} />
                                                <span className="checkmark"></span>
                                                Business
                                            </label>
                                        </li>
                                        <li>
                                            <label className="gen-radio-btn">
                                                <input type="radio" name="individualTypeCheck" checked={this.state.individualTypeCheck} onClick={(e)=>this.handleChange(e, "custType")} onChange={this.onChangeFun} />
                                                <span className="checkmark"></span>
                                                Individual
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="ccd-inner">
                            <div className="col-lg-12 pad-0 m-top-20">
                                <h3>Primary Contact</h3>
                                <div className="col-lg-7 pad-0 m-top-20">
                                    <div className="col-lg-3 pad-lft-0">
                                        <label>Solution</label>
                                        <select className="onFocus" value={this.state.custSalutation} onChange={(e)=>this.handleChange(e,"custSalutation")}>
                                            <option value="">Select Solution</option>
                                            <option value="Public Limited">Public Limited</option>
                                            <option value="Private Limited">Private Limited</option>
                                            <option value="LLP">LLP</option>
                                            <option value="Partnership">Partnership</option>
                                            <option value="Proprietorship">Proprietorship</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        {this.state.custSalutationError && <span className="error">Select solution</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>First Name</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.custFirstName} onChange={(e)=>this.handleChange(e, "custFirstName")}/>
                                        {this.state.custFirstNameError && <span className="error">Enter first name</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Last Name</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.custLastName} onChange={(e)=>this.handleChange(e, "custLastName")}/>
                                        {this.state.custLastNameError && <span className="error">Enter last name</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 pad-0 m-top-20">
                                <div className="col-lg-7 pad-0">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Company Name</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.custCompany} onChange={(e)=>this.handleChange(e, "custCompany")}/>
                                        {this.state.custCompanyError && <span className="error">Enter company name</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Customer Display Name</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.custDisplayName} onChange={(e)=>this.handleChange(e, "custDisplayName")}/>
                                        {this.state.custDisplayNameError && <span className="error">Enter display name</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Customer Email</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.custEmail} onChange={(e)=>this.handleChange(e, "custEmail")}/>
                                        {this.state.custEmailError && <span className="error">Enter valid email</span>}
                                    </div>
                                </div>
                                <div className="col-lg-5 pad-0">
                                    <div className="col-lg-6 pad-lft-0">
                                        <label>Customer Phone Number</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.custNumber} onChange={(e)=>this.handleChange(e, "custNumber")}/>
                                        {this.state.custNumberError && <span className="error">Enter mobile number</span>}
                                    </div>
                                    <div className="col-lg-6 pad-lft-0">
                                        <label>Website</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.custWebsite} onChange={(e)=>this.handleChange(e, "custWebsite")}/>
                                        {this.state.custWebsiteError && <span className="error">Enter website</span>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 pad-0">
                    <div className="subscription-tab procurement-setting-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                            <li className="nav-item active">
                                <a className="nav-link st-btn p-l-0" href="#otherdetails" role="tab" data-toggle="tab">Other Details</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#address" role="tab" data-toggle="tab">Address</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#remarks" role="tab" data-toggle="tab">Remarks</a>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-content p-lr-47">
                        <div className="tab-pane fade in active" id="otherdetails" role="tabpanel">
                            <div className="col-lg-12 pad-0">
                                <div className="create-customer-details">
                                    <div className="ccd-inner">
                                        <div className="col-lg-12 pad-0 m-top-30">
                                            <div className="col-lg-7 pad-0">
                                                <div className="col-lg-4 pad-lft-0">
                                                    <label>GST Treatment</label>
                                                    <select className="onFocus" value={this.state.gstTreatment} onChange={(e)=>this.handleChange(e,"gstTreatment")}>
                                                        <option value="">Select GST Treatment</option>
                                                        <option value="Public Limited">Public Limited</option>
                                                        <option value="Private Limited">Private Limited</option>
                                                        <option value="LLP">LLP</option>
                                                        <option value="Partnership">Partnership</option>
                                                        <option value="Proprietorship">Proprietorship</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                    {this.state.gstTreatmentError && <span className="error">Select GST treatment</span>}
                                                </div>
                                                <div className="col-lg-4 pad-lft-0">
                                                    <label>GSTIN Number</label>
                                                    <input type="text" className="ccd-input onFocus" value={this.state.gstIn} onChange={(e)=>this.handleChange(e, "gstIn")}/>
                                                    {this.state.gstInError && <span className="error">Enter valid GST number</span>}
                                                </div>
                                                <div className="col-lg-4 pad-lft-0">
                                                    <label>Place of Supply</label>
                                                    <select className="onFocus" value={this.state.gstInPos} onChange={(e)=>this.handleChange(e,"gstInPos")}>
                                                        <option value="">Select GST Position</option>
                                                        <option value="Public Limited">Public Limited</option>
                                                        <option value="Private Limited">Private Limited</option>
                                                        <option value="LLP">LLP</option>
                                                        <option value="Partnership">Partnership</option>
                                                        <option value="Proprietorship">Proprietorship</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                    {this.state.gstInPosError && <span className="error">Select place</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-5 pad-0">
                                                <div className="col-lg-6 pad-lft-0">
                                                    <label>Currency</label>
                                                    <select className="onFocus" value={this.state.currency} onChange={(e)=>this.handleChange(e,"currency")}>
                                                        <option value="">Select Currency</option>
                                                        <option value="Public Limited">INR</option>
                                                    </select>
                                                    {this.state.currencyError && <span className="error">Select currency</span>}
                                                </div>
                                                <div className="col-lg-6 pad-lft-0">
                                                    <label>Payment Terms</label>
                                                    <select className="onFocus" value={this.state.paymentTerms} onChange={(e)=>this.handleChange(e,"paymentTerms")}>
                                                        <option value="">Select Payment Terms</option>
                                                        <option value="Public Limited">Public Limited</option>
                                                        <option value="Private Limited">Private Limited</option>
                                                        <option value="LLP">LLP</option>
                                                        <option value="Partnership">Partnership</option>
                                                        <option value="Proprietorship">Proprietorship</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                    {this.state.paymentTermsError && <span className="error">Select payment term</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-12 pad-0 m-top-20">
                                                <div className="gen-pi-formate">
                                                    <div className="gpf-dbs">
                                                        <label>Tax Preference</label>
                                                        <ul className="gpf-radio-list m-top-10">
                                                            <li>
                                                                <label className="gen-radio-btn">
                                                                    <input type="radio" name="taxableCheck" checked={this.state.taxableCheck} onClick={(e)=>this.handleChange(e, "taxPreference")} onChange={this.onChangeFun}/>
                                                                    <span className="checkmark"></span>
                                                                    Taxable
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label className="gen-radio-btn">
                                                                    <input type="radio" name="taxExemptCheck" checked={this.state.taxExemptCheck} onClick={(e)=>this.handleChange(e, "taxPreference")} onChange={this.onChangeFun}/>
                                                                    <span className="checkmark"></span>
                                                                    Tax Exempt
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="address" role="tabpanel">
                            <div className="col-lg-12 pad-0">
                                <div className="create-customer-details">
                                    <div className="ccd-inner">
                                        <div className="col-lg-12 pad-0 m-top-30">
                                            <h3 className="ccd-bill-head">Billing Address</h3>
                                        </div>
                                        <div className="col-lg-12 pad-0 m-top-20">
                                            <div className="col-lg-2 pad-lft-0">
                                                <label>Country / Region</label>
                                                <select className="onFocus" value={this.state.billingCountry} onChange={(e)=>this.handleChange(e,"billingCountry")}>
                                                    <option value="">Select Country</option>
                                                    <option value="Public Limited">India</option>
                                                </select>
                                                {this.state.billingCountryError && <span className="error">Select country</span>}
                                            </div>
                                            <div className="col-lg-2 pad-lft-0">
                                                <label>State</label>
                                                <select className="onFocus" value={this.state.billingState} onChange={(e)=>this.handleChange(e,"billingState")}>
                                                    <option value="">Select State</option>
                                                    {allState.map((_, key) => (
                                                        <option value={_} key={key}>{_}</option>
                                                    ))}
                                                </select>
                                                {this.state.billingStateError && <span className="error">Select state</span>}
                                            </div>
                                            <div className="col-lg-2 pad-lft-0">
                                                <label>City</label>
                                                <select className="onFocus" value={this.state.billingCity} onChange={(e)=>this.handleChange(e,"billingCity")}>
                                                    <option value="">Select City</option>
                                                    {this.state.billingState !== ""&& (this.state.billingCities.length && this.state.billingCities.map((_, key) => (
                                                        <option value={_} key={key}>{_}</option>
                                                    )))}
                                                </select>
                                                {this.state.billingCityError && <span className="error">Select city</span>}
                                            </div>
                                            <div className="col-lg-3 pad-0">
                                                <div className="col-lg-6 pad-lft-0">
                                                    <label>Pincode</label>
                                                    <input type="text" className="ccd-input onFocus" value={this.state.billingPincode} onChange={(e)=>this.handleChange(e, "billingPincode")}/>
                                                    {this.state.billingPincodeError && <span className="error">Enter pincode</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-10 pad-0 m-top-20">
                                            <div className="col-lg-5 pad-lft-0">
                                                <label>Address Line 1</label>
                                                <textarea className="onFocus" value={this.state.billingAddress1} onChange={(e)=>this.handleChange(e, "billingAddress1")}></textarea>
                                                {this.state.billingAddress1Error && <span className="error">Enter address</span>}
                                            </div>
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Address Line 2</label>
                                                <textarea className="onFocus" value={this.state.billingAddress2} onChange={(e)=>this.handleChange(e, "billingAddress2")}></textarea>
                                                {this.state.billingAddress2Error && <span className="error">Enter address</span>}                                            
                                            </div>
                                        </div>
                                        <div className="col-lg-12 pad-0 m-top-30">
                                            <h3>Shipping Address</h3>
                                            <div className="siteCheckBox">
                                                <label className="checkBoxLabel0">
                                                    <input type="checkBox" name="billingAndShippingAddSameCheck" checked={this.state.billingAndShippingAddSameCheck} onClick={(e)=>this.handleChange(e, "billingAndShippingAddSameCheck")} onChange={this.onChangeFun}/>
                                                    <span className="checkmark1"></span>
                                                    Shipping Address same as Billing Address
                                                </label>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 pad-0 m-top-20">
                                            <div className="col-lg-2 pad-lft-0">
                                                <label>Country / Region</label>
                                                <select className="onFocus" value={this.state.shippingCountry} onChange={(e)=>this.handleChange(e,"shippingCountry")}>
                                                    <option value="">Select Country</option>
                                                    <option value="Public Limited">India</option>
                                                </select>
                                                {this.state.shippingCountryError && <span className="error">Select country</span>}
                                            </div>
                                            <div className="col-lg-2 pad-lft-0">
                                                <label>State</label>
                                                <select className="onFocus" value={this.state.shippingState} onChange={(e)=>this.handleChange(e,"shippingState")}>
                                                    <option value="">Select State</option>
                                                    {allState.map((_, key) => (
                                                        <option value={_} key={key}>{_}</option>
                                                    ))}
                                                </select>
                                                {this.state.shippingStateError && <span className="error">Select state</span>}
                                            </div>
                                            <div className="col-lg-2 pad-lft-0">
                                                <label>City</label>
                                                <select className="onFocus" value={this.state.shippingCity} onChange={(e)=>this.handleChange(e,"shippingCity")}>
                                                    <option value="">Select City</option>
                                                    {this.state.shippingState !== "" && (this.state.shippingCities.length && this.state.shippingCities.map((_, key) => (
                                                        <option value={_} key={key}>{_}</option>
                                                    )))}
                                                </select>
                                                {this.state.shippingCityError && <span className="error">Select city</span>}
                                            </div>
                                            <div className="col-lg-3 pad-0">
                                                <div className="col-lg-6 pad-lft-0">
                                                    <label>Pincode</label>
                                                    <input type="text" className="ccd-input onFocus" value={this.state.shippingPincode} onChange={(e)=>this.handleChange(e, "shippingPincode")}/>
                                                    {this.state.shippingPincodeError && <span className="error">Enter pincode</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-10 pad-0 m-top-20">
                                            <div className="col-lg-5 pad-lft-0">
                                                <label>Address Line 1</label>
                                                <textarea className="onFocus" value={this.state.shippingAddress1} onChange={(e)=>this.handleChange(e, "shippingAddress1")}></textarea>
                                                {this.state.shippingAddress1Error && <span className="error">Enter address</span>}
                                            </div>
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Address Line 2</label>
                                                <textarea className="onFocus" value={this.state.shippingAddress2} onChange={(e)=>this.handleChange(e, "shippingAddress2")}></textarea>
                                                {this.state.shippingAddress2Error && <span className="error">Enter address</span>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="remarks" role="tabpanel">
                            <div className="col-lg-5 pad-0 m-top-20">
                                <textarea className="ccd-remark onFocus" value={this.state.remark} onChange={(e)=>this.handleChange(e, "remark")} placeholder="Write your remarks here"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateCustomer;