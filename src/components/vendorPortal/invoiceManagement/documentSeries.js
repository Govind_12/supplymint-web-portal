import React from "react";

class DocumentSeries extends React.Component {
    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="global-search-tab gcl-tab">
                        <ul className="nav nav-tabs gst-inner" role="tablist">
                            <li className="nav-item active" >
                                <a className="nav-link gsti-btn" href="#invoice" role="tab" data-toggle="tab">Invoice</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#estimates" role="tab" data-toggle="tab">Estimates</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#gateEntry" role="tab" data-toggle="tab">Delivery Challan</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#note" role="tab" data-toggle="tab">Note</a>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="invoice" role="tabpanel">
                            <div className="col-md-12 pad-0">
                                <div className="col-md-6 pad-0">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="width_100 input-outer-box onFocus" name="prefix" />
                                                    {/* <span className="error">Enter Valid Prefix</span> */}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box onFocus" value="" />
                                                    {/* <span className="error">Enter Valid Number</span> */}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    <div className="samb-asnno">
                                                        <div className="samb-dropdown">
                                                            <select id="asnYearValue" className="width_100 input-outer-box onFocus">
                                                                <option value="">Select</option>
                                                                <option value="YY-YY">YY-YY</option>
                                                                <option value="YYYY">YYYY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    {/* <span className="error">Please Select Correct Format</span> */}
                                                </div>
                                            </div>
                                        </ul>
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Sample : <span className="bold">INV/XXXXXXXXX/19-20</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="estimates" role="tabpanel">
                            <div className="col-md-12 pad-0">
                                <div className="col-md-6 pad-0">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="width_100 input-outer-box onFocus" name="prefix" />
                                                    {/* <span className="error">Enter Valid Prefix</span> */}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box onFocus" value="" />
                                                    {/* <span className="error">Enter Valid Number</span> */}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    <div className="samb-asnno">
                                                        <div className="samb-dropdown">
                                                            <select id="asnYearValue" className="width_100 input-outer-box onFocus">
                                                                <option value="">Select</option>
                                                                <option value="YY-YY">YY-YY</option>
                                                                <option value="YYYY">YYYY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    {/* <span className="error">Please Select Correct Format</span> */}
                                                </div>
                                            </div>
                                        </ul>
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Sample : <span className="bold">INV/XXXXXXXXX/19-20</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="gateEntry" role="tabpanel">
                            <div className="col-md-12 pad-0">
                                <div className="col-md-6 pad-0">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="width_100 input-outer-box onFocus" name="prefix" />
                                                    {/* <span className="error">Enter Valid Prefix</span> */}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box onFocus" value="" />
                                                    {/* <span className="error">Enter Valid Number</span> */}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    <div className="samb-asnno">
                                                        <div className="samb-dropdown">
                                                            <select id="asnYearValue" className="width_100 input-outer-box onFocus">
                                                                <option value="">Select</option>
                                                                <option value="YY-YY">YY-YY</option>
                                                                <option value="YYYY">YYYY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    {/* <span className="error">Please Select Correct Format</span> */}
                                                </div>
                                            </div>
                                        </ul>
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Sample : <span className="bold">INV/XXXXXXXXX/19-20</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="note" role="tabpanel">
                            <div className="col-md-12 pad-0">
                                <div className="col-md-6 pad-0">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="width_100 input-outer-box onFocus" name="prefix" />
                                                    {/* <span className="error">Enter Valid Prefix</span> */}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box onFocus" value="" />
                                                    {/* <span className="error">Enter Valid Number</span> */}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    <div className="samb-asnno">
                                                        <div className="samb-dropdown">
                                                            <select id="asnYearValue" className="width_100 input-outer-box onFocus">
                                                                <option value="">Select</option>
                                                                <option value="YY-YY">YY-YY</option>
                                                                <option value="YYYY">YYYY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    {/* <span className="error">Please Select Correct Format</span> */}
                                                </div>
                                            </div>
                                        </ul>
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Sample : <span className="bold">INV/XXXXXXXXX/19-20</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DocumentSeries;