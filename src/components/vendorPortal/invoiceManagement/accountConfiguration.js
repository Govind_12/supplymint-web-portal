import React from 'react';
import DocumentSeries from './documentSeries';
 
class AccountConfiguration extends React.Component {
    render () {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="subscription-tab procurement-setting-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                            <li className="nav-item active">
                                <a className="nav-link st-btn p-l-0" href="#organisation" role="tab" data-toggle="tab">Organisation</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#generalsetting" role="tab" data-toggle="tab">General Setting</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#documentseries" role="tab" data-toggle="tab">Document Series</a>
                            </li>
                            <li className="pst-button">
                                <div className="pstb-inner">
                                    <button type="button" className="pst-save">Save</button>
                                    <button type="button" className="">Clear</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                    {/* <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <h3>Configuration Details</h3>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="get-details">Save</button>
                                <button type="button" className="ngr-clear">Clear</button>
                            </div>
                        </div>
                    </div> */}
                </div>

                <div className="col-lg-12 p-lr-47">
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="organisation" role="tabpanel">
                            <div className="create-customer-details">
                                <div className="ccd-inner">
                                    <div className="col-lg-12 pad-0 m-top-30">
                                        <h3>Organization Details</h3>
                                        <div className="col-lg-7 pad-0 m-top-20">
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Legal Name</label>
                                                <input type="text" className="ccd-input onFocus" />
                                            </div>
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Legal Type</label>
                                                <select className="onFocus">
                                                    <option></option>
                                                </select>
                                            </div>
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>GSTIN</label>
                                                <input type="text" className="ccd-input onFocus" />
                                            </div>
                                        </div>
                                        <div className="col-lg-5 pad-0 m-top-20">
                                            <div className="col-lg-6 pad-lft-0">
                                                <label>CIN (If Applicable)</label>
                                                <input type="text" className="ccd-input onFocus" />
                                            </div>
                                            <div className="col-lg-6 pad-lft-0">
                                                <label>Pan</label>
                                                <input type="text" className="ccd-input onFocus" />
                                            </div>
                                        </div>
                                        <div className="col-lg-7 pad-0 m-top-20">
                                            <div className="col-lg-8 pad-lft-0">
                                                <label>Legal Address</label>
                                                <textarea className="onFocus"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 pad-0 m-top-30">
                                        <h3>Contact Details</h3>
                                        <div className="col-lg-7 pad-0 m-top-20">
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Contact Name</label>
                                                <input type="text" className="ccd-input onFocus" />
                                            </div>
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Email</label>
                                                <input type="text" className="ccd-input onFocus" />
                                            </div>
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Mobile</label>
                                                <input type="number" className="ccd-input onFocus" />
                                            </div>
                                        </div>
                                        <div className="col-lg-4 pad-lft-0 m-top-20">
                                            <label>Logo</label>
                                            <div className="ccdi-upload-area">
                                                <label className="ccdi-upload">
                                                    <input type="file" />
                                                    <span>Choose File</span>
                                                    <img src={require('../../../assets/uploadNew.svg')} />
                                                </label>
                                                <div className="ccdiu-uploaded-file">
                                                    <img src={require('../../../assets/vmart.jpg')} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="generalsetting" role="tabpanel">
                            <div className="create-customer-details">
                                <div className="ccd-inner">
                                    <div className="col-lg-12 pad-0 m-top-30">
                                        <div className="col-lg-10 pad-0">
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Bank Account Details</label>
                                                <textarea className="onFocus"></textarea>
                                            </div>
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Notes</label>
                                                <textarea className="onFocus"></textarea>
                                            </div>
                                            <div className="col-lg-4 pad-lft-0">
                                                <label>Terms & Condition</label>
                                                <textarea className="onFocus"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="documentseries" role="tabpanel">
                            <DocumentSeries />
                            {/* <div className="create-customer-details">
                                <div className="ccd-inner">
                                    <div className="col-lg-12 pad-0 m-top-30">
                                        <h3>Document Series</h3>
                                        <div className="col-lg-7 pad-0 m-top-20">
                                            <div className="col-lg-2 pad-lft-0">
                                                <label>Prefix</label>
                                                <input type="text" className="ccd-input onFocus" />
                                            </div>
                                            <div className="col-lg-3 pad-lft-0">
                                                <label>Document Serial</label>
                                                <input type="text" className="ccd-input onFocus" />
                                            </div>
                                            <div className="col-lg-3 pad-lft-0">
                                                <label>Year</label>
                                                <select className="onFocus">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 pad-0 m-top-10">
                                            <span className="cdds-sample-text">Sample : <span className="bold">INV/XXXXXXXXX/19-20</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default AccountConfiguration;