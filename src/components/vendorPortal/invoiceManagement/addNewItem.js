import React from 'react';
import Pagination from '../../pagination';

class AddNewItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedItem: -1,
            search: ""
        }
    }
    
    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content add-new-item-modal">
                    <div className="anim-head">
                        <h3>Add New Item</h3>
                        <div className="animh-right">
                            <button type="button" className="animh-done" onClick={() => this.props.selectItem(this.state.selectedItem)}>Done</button>
                            <button type="button" className="animh-close" onClick={this.props.closeNewItem}><img src={require('../../../assets/clearSearch.svg')} /></button>
                        </div>
                    </div>
                    <div className="anim-body">
                        <div className="animb-search m-top-20">
                            <div className="gen-new-pi-history">
                                <form>
                                    <input type="search" placeholder="Type to Search..." class="search_bar" onChange={(e) => this.setState({search: e.target.value})} onKeyDown={(e) => this.props.onSearch(e.keyCode, e.target.value)} />
                                    <button type="button" className="searchWithBar">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                            <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z"></path>
                                        </svg>
                                    </button>
                                    {this.state.search !== "" && <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} onClick={() => {this.setState({search: ""}); this.props.onSearch(27, "")}} /></span>}
                                </form>
                            </div>
                        </div>
                        <div className="vendor-gen-table" >
                            <div className="manage-table">
                                <table className="table gen-main-table">
                                    <thead>
                                        <tr>
                                            <th className="fix-action-btn width40">
                                                <ul className="rab-refresh"></ul>
                                            </th>
                                            <th><label>Item Name</label></th>
                                            <th><label>Item Type</label></th>
                                            <th><label>Item Description</label></th>
                                            <th><label>Item HSN</label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.props.data.invoiceItem.map((item, index) =>
                                            <tr>
                                                <td className="fix-action-btn width40">
                                                    <ul className="table-item-list">
                                                        {/* <li className="til-inner">
                                                            <label className="checkBoxLabel0">
                                                                <input type="checkBox" name="selectEach" />
                                                                <span className="checkmark1"></span>
                                                            </label>
                                                        </li> */}
                                                        <li className="til-inner">
                                                            <label className="gen-radio-btn" onClick={this.changeType}>
                                                                <input type="radio" onClick={() => this.setState({selectedItem: index})} checked={this.state.selectedItem === index} />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td><label>{item.itemName}</label></td>
                                                <td><label>{item.itemType}</label></td>
                                                <td><label>{item.itemDescription}</label></td>
                                                <td><label>{item.itemHSN}</label></td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.props.data.maxPage} min="1" onKeyPress={(e) => this.props.getAnyPage(e, this.state.search)} onChange={(e) => this.props.getAnyPage(e, this.state.search)} value={this.props.data.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.props.data.totalInvoiceItem}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={(e) => this.props.page(e, this.state.search)}
                                            prev={this.props.data.prev} current={this.props.data.current} maxPage={this.props.data.maxPage} next={this.props.data.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddNewItem;