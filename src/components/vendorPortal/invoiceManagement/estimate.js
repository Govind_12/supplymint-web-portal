import React from 'react';
import Pagination from '../../pagination';
import ExpandInvoice from './expandInvoice';
import ToastLoader from '../../loaders/toastLoader';
import VendorFilter from '../vendorFilter';
import ColoumSetting from "../../replenishment/coloumSetting";
import ConfirmationSummaryModal from "../../replenishment/confirmationReset";
import moment from 'moment';
import axios from 'axios';
import { CONFIG } from "../../../config/index";
import ToastError from '../../utils/toastError';
import Reload from '../../../assets/refresh-block.svg';

class Estimate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: "",
            estimateData: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            totalEstimateData: 0,
            jumpPage: 1,

            isReportPageFlag: true, //For Hiding Set/Item Header in Column Setting::
            isInvoicePageFlag: true,

            mainHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "INVOICES_ESTIMATES",
                basedOn: "ALL"
            },

            filterItems: {},
            mainCustomHeaders: [],
            getMainHeaderConfig: [],
            mainAvailableHeaders: [],
            mainFixedHeader: [],
            mainCustomHeadersState: [],
            mainHeaderConfigState: {}, 
            mainFixedHeaderData: [],
            mainHeaderConfigDataState: {},
            mainHeaderSummary: [],
            mainDefaultHeaderMap: [],
            mandateHeaderMain: [],
            headerCondition: false,
            coloumSetting: false,
            dragOn: false,
            changesInMainHeaders: false,
            saveMainState: [],
            headerConfigDataState: {},
            confirmModal: false,
            headerMsg: "",
            tabVal: "1",

            // For Expand Header::
            setHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "INVOICES_SUB_TABLE_HEADER",
                basedOn: "SET"
            },
            getSetHeaderConfig: [],
            setFixedHeader: [],
            setCustomHeadersState: [],
            setCustomHeaders: {},
            setHeaderSummary: [],
            setDefaultHeaderMap: [],
            setFixedHeaderData: [],
            setHeaderConfigState: {},
            setHeaderConfigDataState: {},
            setCustomHeaders: {},
            saveSetState: [],
            setHeaderCondition: false,
            setAvailableHeaders: [],
            //-----------------------------

            filter: false,
            filterBar: false,
            filteredValue: [],
            checkedFilters: [],
            applyFilter: false,
            inputBoxEnable: false,
            tagState: false,
            type: 1,
            filterValueForTag: [],
            filterKey: "",
            filterType: "",
            fromCreationDate: "start date",
            toCreationDate: "end date",
            filterNameForDate: "",

            download: false,
            toastMsg: "",
            toastLoader: false,
            toastError: false,  
            checkedData: [],
            toastErrorMsg : "",

            actionExpand: false,
            expandedId: "",
            dropOpen: true,
            genericType: "estimate",
        }
    }

    componentDidMount() {
        sessionStorage.setItem('currentPage', "VENINVMAIN")
        sessionStorage.setItem('currentPageName', "Estimates")
        let payload = {
            pageNo: 1,
            type: 1,
            search: "",
            filter: "",
            sortedBy: "",
            sortedIn: "",
            genericType: this.state.genericType
        }
        this.props.getInvoiceManagementGenericRequest(payload)

        if (!this.props.replenishment.getMainHeaderConfig.isSuccess) {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
        }
        document.addEventListener("keydown", this.escFunction, false);
    }


    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.invoiceManagement.getInvoiceManagementGeneric.isSuccess) {
            return{
                estimateData: nextProps.invoiceManagement.getInvoiceManagementGeneric.data.resource.response == null ? [] : nextProps.invoiceManagement.getInvoiceManagementGeneric.data.resource.response,
                prev: nextProps.invoiceManagement.getInvoiceManagementGeneric.data.resource.previousPage,
                current: nextProps.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage,
                next: nextProps.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage + 1,
                maxPage: nextProps.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage,
                totalEstimateData: nextProps.invoiceManagement.getInvoiceManagementGeneric.data.resource.totalCount,
                jumpPage: nextProps.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage
            }
        }
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
                nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
                let mainFixedHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]) : []
                let mainCustomHeadersState = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : []
                let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ? 
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                return {
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    mainCustomHeaders: prevState.headerCondition ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] : {},
                    getMainHeaderConfig,
                    mainAvailableHeaders,
                    mainFixedHeader,
                    mainCustomHeadersState,
                    mainHeaderConfigState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : {},
                    mainFixedHeaderData: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
                    mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
                    mainHeaderSummary: nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : [],
                    mainDefaultHeaderMap: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderMain: Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Mandate Headers"])
                };
            }
        }
        if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getSetHeaderConfig.data.resource != null &&
                nextProps.replenishment.getSetHeaderConfig.data.basedOn == "SET") {
                let getSetHeaderConfig = nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]) : []
                let setCustomHeadersState = nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]) : []
                let setAvailableHeaders = setCustomHeadersState.length !== 0 ? 
                Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                return {
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    setCustomHeaders: prevState.setHeaderCondition ? nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] : {},
                    getSetHeaderConfig,
                    setFixedHeader: nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]) : [],
                    setCustomHeadersState,
                    setAvailableHeaders,
                    setHeaderConfigState: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] : {},
                    setFixedHeaderData: nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] : {},
                    setHeaderConfigDataState: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] } : {},
                    setHeaderSummary: nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]) : [],
                    setDefaultHeaderMap: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet: Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Mandate Headers"])
                };
            }
        }
        return null;
    }

    componentDidUpdate(){
        if (this.props.invoiceManagement.getInvoiceManagementGeneric.isSuccess) {
            this.props.getInvoiceManagementGenericClear();
        }
        if (this.props.replenishment.getMainHeaderConfig.isSuccess) {
            if (this.props.replenishment.getMainHeaderConfig.data.resource != null && this.props.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                setTimeout(() => {
                    this.setState({
                        headerCondition: this.state.mainCustomHeadersState.length == 0 ? true : false,
                    })
                }, 1000);

                // view email redirect logic here:::
                let queryParam = sessionStorage.getItem('login_redirect_queryParam') == null || sessionStorage.getItem('login_redirect_queryParam') == undefined ? null :
                    Object.keys(sessionStorage.getItem('login_redirect_queryParam')).length ? JSON.parse(sessionStorage.getItem('login_redirect_queryParam')) : null;
                let filterValue = "";
                let filterKeyValue = "";
                if (queryParam !== null && queryParam !== undefined && Object.keys(queryParam).length != 0) {
                    var allHeaders = this.props.replenishment.getMainHeaderConfig.data.resource["Default Headers"]
                    filterValue = Object.keys(queryParam).map((_) => { return allHeaders[_] });
                    let flag = false;
                    filterValue.map(data => { if (data == undefined) flag = true; });
                    filterKeyValue = Object.values(queryParam);
                    if (!flag && filterValue.length > 0 && filterKeyValue.length > 0 && filterValue.length == filterKeyValue.length) {
                        setTimeout(() => {
                            let payload = {
                                pageNo: 1,
                                type: 2,
                                search: "",
                                sortedBy: "",
                                sortedIn: "",
                                genericType: this.state.genericType,
                                filter: queryParam,
                            }
                            this.props.getInvoiceManagementGenericRequest(payload)
                        }, 500);

                        this.setState({
                            filteredValue: Object.keys(queryParam).length ? queryParam : {},
                            checkedFilters: Object.keys(queryParam).length ? filterValue : [],
                            applyFilter: Object.keys(queryParam).length ? true : false,
                            inputBoxEnable: Object.keys(queryParam).length ? true : false,
                            tagState: true,
                            //[filterValue] : queryParam.shipmentAdviceCode,
                            type: 2,
                            filterValueForTag: Object.keys(queryParam).length ? queryParam : {},

                        }, () => {
                            filterValue.map((data, index) => {
                                this.setState({ [data]: filterKeyValue[index] })
                            })
                        })
                    }
                }
            }
            this.props.getMainHeaderConfigClear()
        }
        if (this.props.replenishment.createMainHeaderConfig.isSuccess && this.props.replenishment.createMainHeaderConfig.data.basedOn == "ALL") {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            this.props.createMainHeaderConfigClear();
        }
        if(this.props.invoiceManagement.deleteInvoiceManagementGeneric.isSuccess) {
            let payload = {
                pageNo: this.state.current,
                type: this.state.type,
                filter: this.state.filteredValue,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                genericType: this.state.genericType,
            }
            this.props.getInvoiceManagementGenericRequest(payload)
            this.props.deleteInvoiceManagementGenericClear();
        }
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "SET") {
            this.props.getSetHeaderConfigRequest(this.state.setHeaderPayload)
            this.props.createSetHeaderConfigClear();
        }
        if (this.props.replenishment.getSetHeaderConfig.isSuccess) {
            if (this.props.replenishment.getSetHeaderConfig.data.resource != null && this.props.replenishment.getSetHeaderConfig.data.basedOn == "SET") {
                setTimeout(() => {
                    this.setState({
                        setHeaderCondition: this.state.setCustomHeadersState.length == 0 ? true : false,
                    })
                }, 1000);

                // view email redirect logic here:::
                let queryParam = sessionStorage.getItem('login_redirect_queryParam') == null || sessionStorage.getItem('login_redirect_queryParam') == undefined ? null :
                    Object.keys(sessionStorage.getItem('login_redirect_queryParam')).length ? JSON.parse(sessionStorage.getItem('login_redirect_queryParam')) : null;
                let filterValue = "";
                let filterKeyValue = "";
                if (queryParam !== null && queryParam !== undefined && Object.keys(queryParam).length != 0) {
                    var allHeaders = this.props.replenishment.getMainHeaderConfig.data.resource["Default Headers"]
                    filterValue = Object.keys(queryParam).map((_) => { return allHeaders[_] });
                    let flag = false;
                    filterValue.map(data => { if (data == undefined) flag = true; });
                    filterKeyValue = Object.values(queryParam);
                    if (!flag && filterValue.length > 0 && filterKeyValue.length > 0 && filterValue.length == filterKeyValue.length) {
                        setTimeout(() => {
                            let payload = {
                                pageNo: 1,
                                type: 2,
                                search: "",
                                sortedBy: "",
                                sortedIn: "",
                                genericType: this.state.genericType,
                                filter: queryParam,
                            }
                            this.props.getInvoiceManagementGenericRequest(payload)
                        }, 500);

                        this.setState({
                            filteredValue: Object.keys(queryParam).length ? queryParam : {},
                            checkedFilters: Object.keys(queryParam).length ? filterValue : [],
                            applyFilter: Object.keys(queryParam).length ? true : false,
                            inputBoxEnable: Object.keys(queryParam).length ? true : false,
                            tagState: true,
                            type: 2,
                            filterValueForTag: Object.keys(queryParam).length ? queryParam : {},

                        }, () => {
                            filterValue.map((data, index) => {
                                this.setState({ [data]: filterKeyValue[index] })
                            })
                        })
                    }
                }
            }
            this.props.getSetHeaderConfigClear()
        }
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
    }

    openDownload(e) {
        e.preventDefault();
        this.setState({
            download: !this.state.download
        }, () => document.addEventListener('click', this.closeDownload));
    }
    closeDownload = () => {
        this.setState({ download: false }, () => {
            document.removeEventListener('click', this.closeDownload);
        });
    }

    onSearch = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                let payload = {
                    pageNo: 1,
                    type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                    search: e.target.value,
                    filter: this.state.filteredValue,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    genericType: this.state.genericType,
                }
                this.props.getInvoiceManagementGenericRequest(payload)
                this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3 })
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
                let payload = {
                    pageNo: 1,
                    type: this.state.type == 4 ? 2 : 1,
                    search: "",
                    filter: this.state.filteredValue,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    genericType: this.state.genericType,
                }
                this.props.getInvoiceManagementGenericRequest(payload)
                this.setState({ search: "", type: this.state.type == 4 ? 2 : 1 })
            }
        })
    }

    searchClear =()=> {
        if (this.state.type === 3 || this.state.type == 4) {
            let payload = {
                pageNo: 1,
                type: this.state.type == 4 ? 2 : 1,
                search: "",
                filter: this.state.filteredValue,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                genericType: this.state.genericType,
            }
            this.props.getInvoiceManagementGenericRequest(payload)
            this.setState({ search: "", type: this.state.type == 4 ? 2 : 1 })
        } else { this.setState({ search: "" }) }
    }

    page =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.previousPage,
                    current: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage,
                    next: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage + 1,
                    maxPage: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage,
                })
                if (this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage != 0) {
                    let payload = {
                        pageNo: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage - 1,
                        type:this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        genericType: this.state.genericType,
                    }
                    this.props.getInvoiceManagementGenericRequest(payload)
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.previousPage,
                current: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage,
                next: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage + 1,
                maxPage: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage,
            })
            if (this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage != this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage) {
                let payload = {
                    pageNo: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage + 1,
                    type:this.state.type,
                    search: this.state.search,
                    filter: this.state.filteredValue,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    genericType: this.state.genericType,
                }
                this.props.getInvoiceManagementGenericRequest(payload)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.previousPage,
                    current: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage,
                    next: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage + 1,
                    maxPage: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage,
                })
                if (this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage <= this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage) {
                    let payload = {
                        pageNo: 1,
                        type:this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        genericType: this.state.genericType,
                    }
                    this.props.getInvoiceManagementGenericRequest(payload)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.previousPage,
                    current: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage,
                    next: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage + 1,
                    maxPage: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage,
                })
                if (this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.currPage <= this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage) {
                    let payload = {
                        pageNo: this.props.invoiceManagement.getInvoiceManagementGeneric.data.resource.maxPage,
                        type:this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        genericType: this.state.genericType,
                    }
                    this.props.getInvoiceManagementGenericRequest(payload)
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        type:this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        genericType: this.state.genericType,
                    }
                    this.props.getInvoiceManagementGenericRequest(payload)
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    getAllData() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let payload = {
            pageNo: this.state.current,
            type:this.state.type,
            search: this.state.search,
            filter: this.state.filteredValue,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            genericType: this.state.genericType,
        }
        let selectAllFlag = true;
        let response = ""
        axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=INVOICE_MANAGEMENT_ITEM&isAllData=true&isOnlyCurrentPage=${selectAllFlag}`, payload, { headers: headers })
            .then(res => {
                response = res
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
                setTimeout(()=>{
                    this.setState({
                        toastError: false
                    })
                }, 5000)
            });
    }

    filterHeader = (event) => {
        var data = event.target.dataset.key 
        if( event.target.closest("th").classList.contains("rotate180"))
            event.target.closest("th").classList.remove("rotate180")
        else
            event.target.closest("th").classList.add("rotate180") 
        var def = {...this.state.mainHeaderConfigDataState};
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ filterKey, filterType: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            let payload = {
                pageNo: this.state.current,
                type:this.state.type,
                search: this.state.search,
                filter: this.state.filteredValue,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                genericType: this.state.genericType,
            }
            this.props.getInvoiceManagementGenericRequest(payload)
        })
    }

    small = (str) => {
        if (str != null) {
            var str = str.toString()
            if (str.length <= 45) {
                return false;
            }
            return true;
        }
    }

    closeToastError = () => {
        this.setState({ toastError: false })
    }

    openFilter =(e)=> {
        e.preventDefault();
        this.setState({filter: !this.state.filter, filterBar: !this.state.filterBar},()=>{
            document.addEventListener("click", this.closeFilterOnClick)
        });
    }

    closeFilter =(e)=> {
        e.preventDefault();
        this.setState({ filter: false, filterBar: false })
    }

    closeFilterOnClick =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")){
            this.setState({ 
                filter: false,
                filterBar: false
            },()=>{
                document.removeEventListener('click', this.closeFilterOnClick)
            })
        }
    }

    clearFilter = () => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                pageNo: this.state.current,
                type:this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                filter: "",
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                genericType: this.state.genericType,
            }
            this.props.getInvoiceManagementGenericRequest(payload)
        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            filterValueForTag: [],
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
        sessionStorage.setItem('login_redirect_queryParam', "")   
    }

    clearTag=(e,index)=>{
        let deleteItem = this.state.checkedFilters;
        let deletedItem = this.state.checkedFilters[index];
        deleteItem.splice(index,1)
        this.setState({
           checkedFilters:deleteItem,
           [deletedItem]: "",
        },()=>{
            if( this.state.checkedFilters.length == 0 )
                this.clearFilter();
            else
                this.submitFilter();
        })
    }
    clearAllTag=(e)=>{
        this.setState({
            checkedFilters:[],
      },()=>{
            this.clearFilter();
            this.clearFilterOutside();
        })
    }
 
   clearFilterOutside=()=>{
         this.setState({
             filteredValue:[],
             checkedFilters:[]
         })
    }

    submitFilter = () => {
        let payload = {}
        let filtervalues =  {}
        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
       
        Object.keys(payload).map((data) => data.includes("Date")
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim()+ "T00:00+05:30", to: payload[data].split("|")[1].trim()+ "T00:00+05:30" }))
        Object.keys(payload).map((data) => (data.includes("Qty") || data.includes("poAmount"))
        && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim() }))
            
        //for handling to and from value on UI level::    
        this.state.checkedFilters.map((data) => (filtervalues[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(filtervalues).map((data) => (data.includes("Qty") || data.includes("poAmount"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: filtervalues[data].split("|")[0].trim(), to: filtervalues[data].split("|")[1].trim()}))
        Object.keys(filtervalues).map((data) => data.includes("Date")
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: moment(filtervalues[data].split("|")[0].trim()).format("DD-MM-YYYY"), to: moment(filtervalues[data].split("|")[1].trim()).format('DD-MM-YYYY') }))

        let data = {
            pageNo: 1,
            type:this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            search: this.state.search,
            filter: payload,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            genericType: this.state.genericType,
        }
        this.props.getInvoiceManagementGenericRequest(data)

        this.setState({
            filter: false,
            filteredValue: payload,
            filterValueForTag: filtervalues,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            tagState: true
        })
    }
   
    handleInput = (event, filterName) => {
          
        if (event != undefined && event.length != undefined) {
            this.setState({
                fromCreationDate: moment(event[0]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                toCreationDate: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
        }
        else if( event != null ){
            this.handleFromAndToValue(event);
        }
    }
    
    handleFromAndToValue = () => {
        var value = "";
        var name = event.target.dataset.value;
        if (name == undefined) {
            value = this.state.fromCreationDate + " | " + this.state.toCreationDate
            name = this.state.filterNameForDate;
        }
        else{     
            value = event.target.value
        }

        if( value === " | " || value === "| " || value === " |")
           value = "";

        if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
        }

        this.setState({ [name]: value, applyFilter: true }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                //this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
                this.setState({ applyFilter: true })
            }
            this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
        })
    }

    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        let len = Object.values(this.state.filterValueForTag).length > 0;
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            delete this.state.filterValueForTag[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)]
            this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = "";
            this.setState({ [data]: ""})
            let flag = array.some(data => this.state[data] == "" || this.state[data] == undefined)
            if(!flag && len){
                this.state.checkedFilters.some( (item,index) => {
                    if( item == data){
                        this.clearTag(e, index)
                    }
                })
            }           
                
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
    }

    handleInputBoxEnable = (e, data) => {
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }

    openColoumSetting =(data)=> {
        this.setState({ actionExpand: false, dropOpen: false})
        if (this.state.tabVal == 1) {
            if (this.state.mainCustomHeadersState.length == 0) {
                this.setState({
                    headerCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
        else if (this.state.tabVal == 2) {
            if (this.state.setCustomHeadersState.length == 0) {
                this.setState({
                    setHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
    }

    closeColumnSetting = (e) => {
        if (e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
           this.setState({ coloumSetting: false }, () =>
               document.removeEventListener('click', this.closeColumnSetting))
       } 
    }

    handleDragStart =(e, key, dragItem, dragNode)=> {
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', this.handleDragEnd(dragItem, dragNode))
        dragItem.current = key;
        this.setState({ dragOn:true })
    }
    handleDragEnd =(dragItem, dragNode)=> {
        dragNode.current.removeEventListener('dragend', this.HandleDragEnd)
        this.setState({ dragOn:false})
        dragItem.current = null
        dragNode.current = null
    }
    handleDragEnter =(e, key, dragItem, dragNode)=> {
        const currentItem = dragItem.current
        if (e.target !== dragItem.current) {
            if (this.state.tabVal == 1) {
                if (this.state.headerCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainDefaultHeaderMap
                        let newMainHeaderConfigList = prevState.getMainHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newMainHeaderConfigList.splice(key, 0, newMainHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newMainHeaderConfigList))
                        let configheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true,  
                            mainCustomHeaders: configheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newMainHeaderConfigList,
                        }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainHeaderSummary
                        let newCustomList = prevState.mainCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomList.splice(key, 0, newCustomList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newCustomList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true, 
                            mainCustomHeaders: customheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newCustomList
                        }
                    })
                }
            }
            if (this.state.tabVal == 2) {
                if (this.state.setHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.setDefaultHeaderMap
                        let newSetHeaderList = prevState.getSetHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newSetHeaderList.splice(key, 0, newSetHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newSetHeaderList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return {
                             changesInSetHeaders: true, 
                             setHeaderSummary: newList, 
                            setCustomHeadersState: newSetHeaderList,
                            setCustomHeaders:customheaderData,
                            }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.setHeaderSummary
                        let newCustomHeaderList = prevState.setCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setCustomHeaders:customheaderData, 
                            changesInSetHeaders: true, 
                            setHeaderSummary: newList, 
                            setCustomHeadersState: newCustomHeaderList,}
                    })

                }
            }
        }
    }
    pushColumnData =(e,data)=> {
        if (this.state.tabVal == 1) {
            e.preventDefault();
            let getHeaderConfig = this.state.getMainHeaderConfig
            let customHeadersState = this.state.mainCustomHeadersState
            let headerConfigDataState = this.state.mainHeaderConfigDataState
            let customHeaders = this.state.mainCustomHeaders
            let saveState = this.state.saveMainState
            let defaultHeaderMap = this.state.mainDefaultHeaderMap
            let headerSummary = this.state.mainHeaderSummary
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (this.state.headerCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig = getHeaderConfig
                    getHeaderConfig.push(data)
                    var even = (_.remove(mainAvailableHeaders), function (n) {
                        return n == data
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
               if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(mainAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeadersState: customHeadersState,
                mainCustomHeaders: customHeaders,
                saveMainState: saveState,
                mainDefaultHeaderMap: defaultHeaderMap,
                mainHeaderSummary: headerSummary,
                mainAvailableHeaders,
                changesInMainHeaders: true,
                headerConfigDataState: customHeadersState,
            })
        }
        if (this.state.tabVal == 2) {
            let getHeaderConfig = this.state.getSetHeaderConfig
            let customHeadersState = this.state.setCustomHeadersState
            let headerConfigDataState = this.state.setHeaderConfigDataState
            let customHeaders = this.state.setCustomHeaders
            let saveState = this.state.saveSetState
            let defaultHeaderMap = this.state.setDefaultHeaderMap
            let headerSummary = this.state.setHeaderSummary
            let fixedHeaderData = this.state.setFixedHeaderData
            let setAvailableHeaders = this.state.setAvailableHeaders

            if (this.state.setHeaderCondition) {

                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(setAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {

                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(setAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }

                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getSetHeaderConfig: getHeaderConfig,
                setCustomHeadersState: customHeadersState,
                setCustomHeaders: customHeaders,
                saveSetState: saveState,
                setDefaultHeaderMap: defaultHeaderMap,
                setHeaderSummary: headerSummary,
                setAvailableHeaders,
                changesInSetHeaders: true,
                headerConfigDataState: customHeadersState,
            })
        }
    }
    closeColumn =(data)=> {
        if (this.state.tabVal == 1) {
            let getHeaderConfig = this.state.getMainHeaderConfig
            let headerConfigState = this.state.mainHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.mainCustomHeadersState
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.mainCustomHeadersState.length == 0) {
                    this.setState({
                        headerCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            mainAvailableHeaders.push(data)
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeaders: headerConfigState,
                mainCustomHeadersState: customHeadersState,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.mainFixedHeaderData))[data];
                let saveState = this.state.saveMainState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.mainHeaderSummary
                let defaultHeaderMap = this.state.mainDefaultHeaderMap
                if (!this.state.headerCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    mainHeaderSummary: headerSummary,
                    mainDefaultHeaderMap: defaultHeaderMap,
                    saveMainState: saveState
                })
            }, 100);
        }
        if (this.state.tabVal == 2) {
            let getHeaderConfig = this.state.getSetHeaderConfig
            let headerConfigState = this.state.setHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.setCustomHeadersState
            let fixedHeaderData = this.state.setFixedHeaderData
            let setAvailableHeaders = this.state.setAvailableHeaders

            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.setCustomHeadersState.length == 0) {
                    this.setState({
                        setHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            setAvailableHeaders.push(data)
            this.setState({
                getSetHeaderConfig: getHeaderConfig,
                setCustomHeaders: headerConfigState,
                setCustomHeadersState: customHeadersState,
                setAvailableHeaders,
                changesInSetHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.setFixedHeaderData))[data];
                let saveState = this.state.saveSetState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.setHeaderSummary
                let defaultHeaderMap = this.state.setDefaultHeaderMap
                if (!this.state.setHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    setHeaderSummary: headerSummary,
                    setDefaultHeaderMap: defaultHeaderMap,
                    saveSetState: saveState
                })
            }, 100);
        }
    }
    saveColumnSetting =(e)=> {
        if (this.state.tabVal == 1) {
            this.setState({
                coloumSetting: false,
                headerCondition: false,
                saveMainState: [],
                changesInMainHeaders: false,
            })
            let payload = {
                basedOn: "ALL",
                module: "INVOICE MANAGEMENT",
                subModule: "ESTIMATES",
                section: "ESTIMATES",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "INVOICES_ESTIMATES",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainCustomHeaders,
            }
            this.props.createMainHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 2) {
            this.setState({
                coloumSetting: false,
                setHeaderCondition: false,
                saveSetState: [],
                changesInSetHeaders: false,
            })
            let payload = {
                basedOn: "SET",
                module: "INVOICES",
                subModule: "INVOICES",
                section: "INVOICE",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "INVOICES_SUB_TABLE_HEADER",
                fixedHeaders: this.state.setFixedHeaderData,
                defaultHeaders: this.state.setHeaderConfigDataState,
                customHeaders: this.state.setCustomHeaders,
            }

            this.props.createSetHeaderConfigRequest(payload)
        }
    }
    resetColumnConfirmation =()=> {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            confirmModal: true,
        })
    }
    resetColumn =()=> {
        const {getMainHeaderConfig,mainFixedHeader,setFixedHeader,
            getSetHeaderConfig} = this.state
        if (this.state.tabVal == 1) {
            let payload = {
                basedOn: "ALL",
                module: "INVOICE MANAGEMENT",
                subModule: "ESTIMATES",
                section: "ESTIMATES",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "INVOICES_ESTIMATES",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainCustomHeaders,
            }
            this.props.createMainHeaderConfigRequest(payload)
            let availableHeaders= mainFixedHeader.filter(function (obj) { return getMainHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                headerCondition: true,
                coloumSetting: false,
                saveMainState: [],
                confirmModal: false,
                mainAvailableHeaders:availableHeaders
            })    
        }
        if (this.state.tabVal == 2) {
            let payload = {
                basedOn: "SET",
                module: "INVOICES",
                subModule: "INVOICES",
                section: "INVOICE",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "INVOICES_SUB_TABLE_HEADER",
                fixedHeaders: this.state.setFixedHeaderData,
                defaultHeaders: this.state.setHeaderConfigDataState,
                customHeaders: this.state.setHeaderConfigDataState,
            }
            this.props.createSetHeaderConfigRequest(payload)
            let availableHeaders= setFixedHeader.filter(function (obj) { return getSetHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                setHeaderCondition: true,
                coloumSetting: false,
                saveSetState: [],
                confirmModal: false,
                setAvailableHeaders:availableHeaders
            })
        }
    }
    closeConfirmModal =(e)=> {
        this.setState({ confirmModal: !this.state.confirmModal,})
    }
    onHeadersTabClick = (tabVal) => {
        this.setState({ tabVal: tabVal }, () => {
            if (tabVal === 1) {
                this.openColoumSetting("true")
                this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            }
            else if (tabVal === 2) {
                this.openColoumSetting("true")
                this.props.getSetHeaderConfigRequest(this.state.setHeaderPayload)
            }
        })
    }

    closingAllModal = () => {
        this.setState({
            filter: false, filterBar: false,
            download: false,
            headerCondition: false, coloumSetting: false,
            confirmModal: false, actionExpand: false, dropOpen: false,
        },()=>{
            document.removeEventListener('click', this.closeDownload)
            document.removeEventListener('click', this.closeFilterOnClick)
            document.removeEventListener('click', this.closeColumnSetting)
        })
    }

    onRefresh = () => {
        this.state.checkedFilters.map((data) => this.setState({ [data]: "" }))
        let payload = {
            pageNo: 1,
            type: 1,
            search: "",
            filter: "",
            sortedBy: "",
            sortedIn: "",
            genericType: this.state.genericType,
        }
        this.props.getInvoiceManagementGenericRequest(payload)
        if (document.getElementsByClassName('rotate180')[0] != undefined) {
            document.getElementsByClassName('rotate180')[0].classList.remove('rotate180')
        }
        this.setState({ filteredValue: [], selectAll: false, checkedFilters: [], checkedData: [] })
        this.closingAllModal();
        this.clearAllTag();
    }

    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.closingAllModal();
        }
    }

    createEstimate = () => {
        this.props.history.push(`/invoiceManagement/createInvoice?type=${this.state.genericType}`)
        sessionStorage.setItem("updatedInvoiceManagementData", "")
    }

    deleteEstimate =(e, data)=>{
        let payload = []
        payload.push(data.id)
        this.props.deleteInvoiceManagementGenericRequest(payload)
    }

    updateEstimate =(e, data)=>{
        let array = [...this.state.checkedData]
        let selectedItem = {
            id: data.id,
            itemType: data.itemType,
            itemName: data.itemName,
            itemUnit: data.itemUnit,
            itemHSN: data.itemHSN,
            taxPreference: data.taxPreference,
            sellingPrice: data.sellingPrice,
            itemDescription: data.itemDescription,
            interTaxRate: data.interTaxRate,
            intraTaxRate: data.intraTaxRate
        }
        if (this.state.checkedData.some((item) => item.id == data.id)) {
            array = array.filter((item) => item.id != data.id)
        } else {
            array.push(selectedItem)
        }  
        this.setState({ checkedData: array},()=>{
            this.props.history.push(`/invoiceManagement/createInvoice?type=${this.state.genericType}`) 
            sessionStorage.setItem("updatedInvoiceManagementData", JSON.stringify(this.state.checkedData))
        })
    }

    expandColumn =(id, e, data)=> {
        if (!this.state.actionExpand || this.state.prevId !== id) {
            let payload = {
                id: id
            }
            this.props.expandInvoiceManagementGenericRequest(payload)
            this.setState({ actionExpand: true, prevId: id, expandedId: id, dropOpen: true})
        } else {
            this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
        }   
    }

    render() {
        const{estimateData, search} = this.state;
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0 m-top-20">
                    <div className="invoice-manage-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="imh-left">
                                <div className="imhl-search">
                                    <input type="search" value={this.state.search} onChange={this.onSearch} onKeyDown={this.onSearch} placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {search != "" ? <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} onClick={this.searchClear} /></span> : null}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="imh-right">
                                <button type="button" className="new-trigger" onClick={this.createEstimate}>
                                    <svg xmlns="http://www.w3.org/2000/svg" id="plus_4_" width="16" height="16" viewBox="0 0 19.846 19.846">
                                        <g id="Group_3035">
                                            <g id="Group_3034">
                                                <path fill="#51aa77" id="Path_948" d="M9.923 0a9.923 9.923 0 1 0 9.923 9.923A9.934 9.934 0 0 0 9.923 0zm0 18.308a8.386 8.386 0 1 1 8.386-8.386 8.4 8.4 0 0 1-8.386 8.386z" className="cls-1"/>
                                            </g>
                                        </g>
                                        <g id="Group_3037" transform="translate(5.311 5.242)">
                                            <g id="Group_3036">
                                                <path fill="#51aa77" id="Path_949" d="M145.477 139.081H142.4v-3.074a.769.769 0 0 0-1.537 0v3.074h-3.074a.769.769 0 0 0 0 1.537h3.074v3.074a.769.769 0 1 0 1.537 0v-3.074h3.074a.769.769 0 0 0 0-1.537z" className="cls-1" transform="translate(-137.022 -135.238)"/>
                                            </g>
                                        </g>
                                    </svg>
                                    Create Estimate
                                </button>
                                {/* <div className="gvpd-download-drop">
                                    <button type="button" className="pi-download" onClick={(e) => this.openDownload(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)"></path>
                                        </svg>
                                    </button>
                                    {this.state.download &&
                                    <ul className="pi-history-download">
                                        <li>
                                            <button className="export-excel" type="button" onClick={()=>this.getAllData()}>
                                                <span className="pi-export-svg">
                                                    <img src={require('../../../assets/downloadAll.svg')} />
                                                </span>
                                            Download All</button>
                                        </li>
                                    </ul>}
                                </div>  */}
                                <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Filter</span>
                                    </button>
                                    {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    {this.state.tagState?this.state.checkedFilters.map((keys,index)=>(
                    <div className="show-applied-filter">
                            {(Object.values(this.state.filterValueForTag)[index]) !== undefined &&  index===0 ?<button type="button" className="saf-clear-all" onClick={(e)=>this.clearAllTag(e)}>Clear All</button>:null}
                            { (Object.values(this.state.filterValueForTag)[index]) !== undefined && <button type="button" className="saf-btn">{keys}
                            <img onClick={(e)=>this.clearTag(e,index)} src={require('../../../assets/clearSearch.svg')} />
                            <span className="generic-tooltip">{typeof (Object.values(this.state.filterValueForTag)[index]) == 'object' ? Object.values(Object.values(this.state.filterValueForTag)[index]).join(',') : Object.values(this.state.filterValueForTag)[index]}</span>
                        </button>}
                    </div>)):''}
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="expand-new-table m-top-30">
                        <div className="manage-table">
                           <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div>
                            <ColoumSetting {...this.props} {...this.state}
                                handleDragStart={this.handleDragStart}
                                handleDragEnter={this.handleDragEnter}
                                onHeadersTabClick={this.onHeadersTabClick}
                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                closeColumn={(e) => this.closeColumn(e)}
                                saveMainState={this.state.saveMainState}
                                saveSetState={this.state.saveSetState}
                                tabVal={this.state.tabVal}
                                pushColumnData={this.pushColumnData}
                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                saveColumnSetting={(e) => this.saveColumnSetting(e)}
                                isReportPageFlag={this.state.isReportPageFlag}
                                isInvoicePageFlag= {this.state.isInvoicePageFlag}
                            />
                            <table className="table gen-main-table">
                                <thead>
                                <tr>
                                    <th className="fix-action-btn">
                                        <ul className="rab-refresh">
                                            <li className="rab-rinner">
                                                <span><img src={Reload} onClick={this.onRefresh}></img></span>
                                            </li>
                                        </ul>
                                    </th>
                                    {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                            <label data-key={data}>{data}</label>
                                            <img src={require('../../../assets/headerFilter.svg')} className="imgHead" data-key={data}/>
                                        </th>
                                        )) : this.state.mainCustomHeadersState.map((data, key) => (
                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                            <label data-key={data}>{data}</label>
                                            <img src={require('../../../assets/headerFilter.svg')} className="imgHead" data-key={data}/>
                                        </th>
                                    ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {estimateData.length != 0 ? estimateData.map((data, key) => (
                                    <React.Fragment key={key}>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner til-delete-btn" onClick={(e)=>this.deleteEstimate(e, data)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                                <li className="til-inner til-edit-btn" onClick={(e)=>this.updateEstimate(e, data)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                    </svg>
                                                </li>
                                                <li className="til-inner til-add-btn" onClick={(e) => this.expandColumn(data.id, e, data)}>
                                                    {this.state.dropOpen && this.state.expandedId == data.id ?
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                            <path fill="#a4b9dd" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z" />
                                                        </svg> :
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                            <path fill="#a4b9dd" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z" />
                                                        </svg>}
                                                    <span className="generic-tooltip">Expand</span>
                                                </li>
                                            </ul>
                                        </td>
                                        {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                            <td key={key}> 
                                                <label>{data[hdata]}</label>{this.small(data[hdata]) && <div className="table-tooltip"><label>{data[hdata]}</label></div>}
                                            </td>
                                        )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                            <td key={keyy} >
                                                <label className="table-td-text">{data[sdata]}</label>
                                                {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                                            </td>
                                        ))}
                                    </tr>
                                    {this.state.dropOpen && this.state.expandedId == data.id && this.state.actionExpand ? <tr><td colSpan="100%" className="pad-0"><ExpandInvoice {...this.state} {...this.props}/></td></tr> : null}
                                    </React.Fragment>
                                    )) : <tr className="tableNoData"><td> NO DATA FOUND </td></tr>}
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalEstimateData}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastError && <ToastError toastErrorMsg={this.state.toastErrorMsg} closeToastError={this.closeToastError} />}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
            </div>
        )
    }
}
export default Estimate;