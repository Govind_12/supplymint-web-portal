import React from 'react';

class ExpandInvoice extends React.Component {
    constructor(props){
        super(props);
        this.state={
            expandedInvocies: [],
        }
    }

    componentDidMount(){
        let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: "INVOICES_SUB_TABLE_HEADER",
            basedOn: "SET"
        }
        this.props.getSetHeaderConfigRequest(payload)
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.invoiceManagement.expandInvoiceManagementGeneric.isSuccess) {
            return {
                expandedInvocies: nextProps.invoiceManagement.expandInvoiceManagementGeneric.data.resource.response == null ? [] : nextProps.invoiceManagement.expandInvoiceManagementGeneric.data.resource.response
            }
        }
        return null;
    }
    componentDidUpdate(){
        if (this.props.invoiceManagement.expandInvoiceManagementGeneric.isSuccess) {
            this.props.expandInvoiceManagementGenericClear();
        }
    }

    render() {
        return (
            <div className="gen-sticky-tab sticky-table-width">
                <div className="gen-sticky-tab-inner">
                    <table className="table">
                        <thead>
                            <tr>
                                {this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                    <th key={key}><label>{data}</label></th>
                                )) : this.props.setCustomHeadersState.map((data, key) => (
                                    <th key={key}><label>{data}</label></th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                           {this.state.expandedInvocies.length != 0 ? this.state.expandedInvocies.map((data, key) => (
                            <React.Fragment key={key}>
                            <tr>
                                {this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                    <td key={key} ><label>{data[hdata]}</label></td>
                                )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                    <td key={keyy} ><label>{data[sdata]}</label></td>
                                ))}  
                            </tr>
                            </React.Fragment>)) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default ExpandInvoice;