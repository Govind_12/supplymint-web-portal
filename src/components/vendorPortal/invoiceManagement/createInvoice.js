import React from 'react';
import AddNewItem from './addNewItem';
import ToastLoader from '../../loaders/toastLoader';
import queryString from 'querystring'

const todayDate = (new Date()).toISOString().split("T")[0];
var tempDate = new Date();
tempDate.setDate(tempDate.getDate() + 7);
const laterDate = tempDate.toISOString().split("T")[0];

class CreateInvoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // credit: true,
            // debit: false,
            addNewItem: false,
            addNewItemIndex: -1,
            type: "",
            toastLoader: false,
            toastMsg: "",

            customerList: [],
            placeOfSupplyList: [],
            termsList: {},
            reasonList: {},
            challanTypeList: {},
            taxList: {},
            invoicesList: [],
            
            itemsData: {},

            headerPayload: {
				// refId: "",
				custId: "",
				custRefId: "",
				custPos: "",
				docNo: "",
                docDate: todayDate,
                refId: "",
				prevInvId: "",
				referenceNo: "",
				reason: "",
				dueDate: laterDate,
				// taxableValue: 0,
				// taxAmount: 0,
				// totalValue: 0,
				expiryDate: laterDate,
				challanType: "",
				status: "",
				termsCond: "",
			    // orgId: 0,
				// entId: 0,
				// createdBy: "",
				// itemQtyS: "",
				// itemRateS: "",
				// itemAmountS: "",
				// itemId: 0,
				// itemIdS: "",
				genericType: ""
            },
            subTotal: 0,
            taxAmount: 0,
            totalAmount: 0,
            itemDetails: [{
                // itemDetailId: "",
                // itemName: "",
                itemDescription: "",
				itemQty: 0,
				itemRate: 0,
				itemHSN: "",
				itemTax: "",
				itemAmount: 0,
            }],

            headerErrors: {
                // refId: false,
				custId: false,
				custRefId: false,
				custPos: false,
				docNo: false,
                docDate: false,
                refId: false,
				prevInvId: false,
				referenceNo: false,
				reason: false,
				dueDate: false,
				// taxableValue: false,
				// taxAmount: false,
				// totalValue: false,
				expiryDate: false,
				challanType: false,
				status: false,
				termsCond: false,
			    // orgId: false,
				// entId: false,
				// createdBy: false,
				// itemQtyS: false,
				// itemRateS: false,
				// itemAmountS: false,
				// itemId: false,
				// itemIdS: false
            },
            rowsErrors: [{
                // itemDetailId: false,
                // itemName: false,
                itemDescription: false,
				itemQty: false,
				itemRate: false,
				itemHSN: false,
				itemTax: false,
				// itemAmount: false,
            }]
        }
    }

    // noteType =(e)=>{
    //     if( e.target.id == "notecredit"){
    //         this.setState({ credit: !this.state.credit, debit: !this.state.debit
    //         });

    //     }else if( e.target.id == "notedebit"){
    //         this.setState({ debit: !this.state.debit, credit: !this.state.credit
    //         });
    //     }
    // }

    componentDidMount() {
        this.props.getInvoiceManagementCustomerRequest({
            pageNo: 1,
            type: 1,
            search: "",
            filter: "",
            sortedBy: "",
            sortedIn: ""
        });
        this.props.getCoreDropdownRequest({type: "Place_Of_Supply"});
        this.props.getCoreDropdownRequest({type: "TAX"});
        
        if (window.location.hash.includes("type")){
            var type = window.location.hash.split("?")[1].split("=")[1]
            this.setState({
                type: type,
                headerPayload: {...this.state.headerPayload, genericType: type}
            });
        }
        switch (type) {
            case "invoice": {
                this.props.getCoreDropdownRequest({type: "TERMS"});
                break;
            }
            case "crnote": {
                this.props.getCoreDropdownRequest({type: "REASON"});
                this.props.searchInvoiceDataRequest({
                    entity: "invoicemain",
                    pageNo: 0,
                    genericType: "crnote"
                });
                break;
            }
            case "drnote": {
                this.props.searchInvoiceDataRequest({
                    entity: "invoicemain",
                    pageNo: 0,
                    genericType: "drnote"
                });
                break;
            }
            case "challan": {
                this.props.getCoreDropdownRequest({type: "CHALLAN_TYPE"});
                break;
            }
        }
    }

    static getDerivedStateFromProps(props, state) {
        let newState = {};

        if (props.invoiceManagement.getInvoiceManagementCustomer.isSuccess) {
            newState = {
                ...newState,
                customerList: props.invoiceManagement.getInvoiceManagementCustomer.data.resource.response === undefined ? [] : props.invoiceManagement.getInvoiceManagementCustomer.data.resource.response
            }
        }
        
        if (props.invoiceManagement.getCoreDropdown.isSuccess) {
            newState = {
                ...newState,
                placeOfSupplyList: props.invoiceManagement.getCoreDropdown.data.Place_Of_Supply === undefined ? [] : props.invoiceManagement.getCoreDropdown.data.Place_Of_Supply.resource,
                termsList: props.invoiceManagement.getCoreDropdown.data.TERMS === undefined ? {} : props.invoiceManagement.getCoreDropdown.data.TERMS.resource[0],
                reasonList: props.invoiceManagement.getCoreDropdown.data.REASON === undefined ? {} : props.invoiceManagement.getCoreDropdown.data.REASON.resource[0],
                challanTypeList: props.invoiceManagement.getCoreDropdown.data.CHALLAN_TYPE === undefined ? {} : props.invoiceManagement.getCoreDropdown.data.CHALLAN_TYPE.resource[0],
                taxList: props.invoiceManagement.getCoreDropdown.data.TAX === undefined ? {} : props.invoiceManagement.getCoreDropdown.data.TAX.resource[0],
            }
        }

        if (props.invoiceManagement.searchInvoiceData.isSuccess) {
            newState = {
                ...newState,
                invoicesList: props.invoiceManagement.searchInvoiceData.data.resource.response === undefined ? [] : props.invoiceManagement.searchInvoiceData.data.resource.response
            }
        }

        if (props.invoiceManagement.getInvoiceManagementItem.isSuccess) {
            newState = {
                ...newState,
                itemsData: {
                    invoiceItem: props.invoiceManagement.getInvoiceManagementItem.data.resource.response === null ? [] : props.invoiceManagement.getInvoiceManagementItem.data.resource.response,
                    prev: props.invoiceManagement.getInvoiceManagementItem.data.resource.prePage,
                    current: props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage,
                    next: props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage + 1,
                    maxPage: props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage,
                    totalInvoiceItem: props.invoiceManagement.getInvoiceManagementItem.data.resource.totalCount,
                    jumpPage: props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage,
                },
                addNewItem: true
            }
        }

        return newState;
    }

    componentDidUpdate() {
        if (this.props.invoiceManagement.getInvoiceManagementItem.isSuccess) {
            this.props.getInvoiceManagementItemClear();
        }
    }

    openNewItem(e, index) {
        e.preventDefault();
        let payload = {
            pageNo: 1,
            type: 1,
            search: "",
            filter: "",
            sortedBy: "",
            sortedIn: "",
        }
        this.props.getInvoiceManagementItemRequest(payload);
        this.setState({
            addNewItemIndex: index
        });
        // this.setState({
        //     addNewItem: !this.state.addNewItem
        // });
    }
    
    closeNewItem = () => {
        this.setState({
            addNewItem: false,
            addNewItemIndex: -1
        });
    }

    page =(e, search)=> {
        if (e.target.id == "prev") {
            if (this.state.itemsData.current == "" || this.state.itemsData.current == undefined || this.state.itemsData.current == 1) {
            } else {
                // this.setState({
                //     prev: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.prePage,
                //     current: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage,
                //     next: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage + 1,
                //     maxPage: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage,
                // })
                if (this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage != 0) {
                    let payload = {
                        pageNo: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage - 1,
                        type: 3,
                        search: search,
                        filter: "",
                        sortedBy: "",
                        sortedIn: "",
                    }
                    this.props.getInvoiceManagementItemRequest(payload)
                }
            }
        } else if (e.target.id == "next") {
            // this.setState({
            //     prev: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.prePage,
            //     current: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage,
            //     next: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage + 1,
            //     maxPage: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage,
            // })
            if (this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage != this.props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage) {
                let payload = {
                    pageNo: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage + 1,
                    type: 3,
                    search: search,
                    filter: "",
                    sortedBy: "",
                    sortedIn: "",
                }
                this.props.getInvoiceManagementItemRequest(payload)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.itemsData.current == 1 || this.state.itemsData.current == "" || this.state.itemsData.current == undefined) {
            }
            else {
                // this.setState({
                //     prev: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.prePage,
                //     current: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage,
                //     next: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage + 1,
                //     maxPage: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage,
                // })
                if (this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage <= this.props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage) {
                    let payload = {
                        pageNo: 1,
                        type: 3,
                        search: search,
                        filter: "",
                        sortedBy: "",
                        sortedIn: "",
                    }
                    this.props.getInvoiceManagementItemRequest(payload)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.itemsData.current == this.state.maxPage || this.state.itemsData.current == undefined) {
            }
            else {
                // this.setState({
                //     prev: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.prePage,
                //     current: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage,
                //     next: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage + 1,
                //     maxPage: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage,
                // })
                if (this.props.invoiceManagement.getInvoiceManagementItem.data.resource.currPage <= this.props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage) {
                    let payload = {
                        pageNo: this.props.invoiceManagement.getInvoiceManagementItem.data.resource.maxPage,
                        type: 3,
                        search: search,
                        filter: "",
                        sortedBy: "",
                        sortedIn: "",
                    }
                    this.props.getInvoiceManagementItemRequest(payload)
                }
            }
        }
    }

    getAnyPage = (_, search) => {
        if (_.target.validity.valid) {
            this.setState({ itemsData: {...this.state.itemsData, jumpPage: _.target.value} })
            if (_.key == "Enter" && _.target.value != this.state.itemsData.current) {
                if (_.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        type: 3,
                        search: search,
                        filter: "",
                        sortedBy: "",
                        sortedIn: "",
                    }
                    this.props.getInvoiceManagementItemRequest(payload)
                }
                else {
                    this.setState({
                        toastMsg: "Page number cannot be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    onSearch = (keyCode, search) => {
        if (keyCode === 13) {
            let payload = {
                pageNo: 1,
                type: 3,
                search: search,
                filter: "",
                sortedBy: "",
                sortedIn: "",
            }
            this.props.getInvoiceManagementItemRequest(payload)
        }
        else if (keyCode === 27) {
            let payload = {
                pageNo: 1,
                type: 3,
                search: "",
                filter: "",
                sortedBy: "",
                sortedIn: "",
            }
            this.props.getInvoiceManagementItemRequest(payload)
        }
    }

    selectItem = (index) => {
        if (index !== -1) {
            let addNewItemIndex = this.state.addNewItemIndex;
            let allRows = [...this.state.itemDetails];
            allRows[addNewItemIndex].itemDescription = this.props.invoiceManagement.getInvoiceManagementItem.data.resource.response[index].itemDescription;
            allRows[addNewItemIndex].itemHSN = this.props.invoiceManagement.getInvoiceManagementItem.data.resource.response[index].itemHSN;
            this.setState({
                itemDetails: allRows,
                addNewItem: false,
                addNewItemIndex: -1
            }, () => {
                this.handleRowError("itemDescription", addNewItemIndex);
                this.handleRowError("itemHSN", addNewItemIndex);
            });
        }
    }

    addNewRow = () => {
        let allRows = [...this.state.itemDetails];
        let complete = true;
        // Object.keys(allRows[allRows.length - 1]).forEach((key) => {
        //     this.handleRowError(key, allRows.length - 1) ? complete = false : "";
        // });
        if (complete) {
            let rowsErrors = [...this.state.rowsErrors];
            let newRow = {
                // itemDetailId: "",
                // itemName: "",
                itemDescription: "",
                itemQty: 0,
                itemRate: 0,
                itemHSN: "",
                itemTax: "",
                itemAmount: 0
            };
            allRows = allRows.concat(newRow);
            let newError = {
                // itemDetailId: false,
                // itemName: false,
                itemDescription: false,
                itemQty: false,
                itemRate: false,
                itemHSN: false,
                itemTax: false,
                // itemAmount: false,
            }
            rowsErrors = rowsErrors.concat(newError);
            this.setState({
                itemDetails: allRows,
                rowsErrors: rowsErrors
            });
        }
    }

    copyRow = (index) => {
        let allRows = [...this.state.itemDetails];
        let selectedRow = {...this.state.itemDetails[index]};
        let complete = true;
        Object.keys(selectedRow).forEach((key) => {
            this.handleRowError(key, index) ? complete = false : "";
        });
        if (complete) {
            let rowsErrors = [...this.state.rowsErrors];
            allRows = allRows.concat(selectedRow);
            let newError = {
                // itemDetailId: false,
                // itemName: false,
                itemDescription: false,
                itemQty: false,
                itemRate: false,
                itemHSN: false,
                itemTax: false,
                // itemAmount: false,
            };
            rowsErrors = rowsErrors.concat(newError);
            this.setState({
                itemDetails: allRows,
                rowsErrors: rowsErrors
            });
        }
    }

    deleteRow = (index) => {
        let allRows = [...this.state.itemDetails];
        let rowsErrors = [...this.state.rowsErrors];
        if (allRows.length === 1) {
            this.setState({
                toastMsg: "Single row cannot be deleted!",
                toastLoader: true
            });
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                });
            }, 3000);
        }
        else {
            allRows.splice(index, 1);
            rowsErrors.splice(index, 1);
            this.setState({
                itemDetails: allRows,
                rowsErrors: rowsErrors
            }, this.calculateTax);
        }
    }

    handleChange = (id, value) => {
        this.setState({
            headerPayload: {...this.state.headerPayload, [id]: value}
        }, () => this.handleError(id));
    }

    handleError = (id) => {
        if (this.state.headerPayload[id] === "" || this.state.headerPayload[id] === null) {
            this.setState((prevState) => ({
                headerErrors: {
                    ...prevState.headerErrors,
                    [id]: true
                }
            }));
            return true;
        }
        else {
            this.setState((prevState) => ({
                headerErrors: {
                    ...prevState.headerErrors,
                    [id]: false
                }
            }));
            return false;
        }
    }

    handleRowChange = (id, value, index) => {
        let rowsData = [...this.state.itemDetails];
        id = id.slice(0, id.indexOf(index));
        if (id === "itemQty" || id === "itemRate") {
            if (value === "" || /[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)/.test(value)) {
                if (id === "itemQty" && rowsData[index].itemRate !== "") {
                    value === "" ? rowsData[index].itemAmount = 0 : rowsData[index].itemAmount = value * rowsData[index].itemRate;
                }
                else if (id === "itemRate" && rowsData[index].itemQty !== "") {
                    value === "" ?  rowsData[index].itemAmount = 0 : rowsData[index].itemAmount = value * rowsData[index].itemQty;
                }
                // if (rowsData[index].itemAmount !== 0 && rowsData[index].itemAmount !== "") {
                //     this.setState({
                //         subTotal: this.state.subTotal + rowsData[index].itemAmount
                //     });
                //     switch (rowsData[index].itemTax) {
                //         case "GST_5%": {
                //             this.setState({
                //                 taxAmount: this.state.taxAmount + (0.05 * rowsData[index].itemAmount),
                //                 totalAmount: this.state.totalAmount + rowsData[index].itemAmount + (0.05 * rowsData[index].itemAmount)
                //             });
                //             break;
                //         }
                //         case "GST_12%": {
                //             this.setState({
                //                 taxAmount: this.state.taxAmount + (0.12 * rowsData[index].itemAmount),
                //                 totalAmount: this.state.totalAmount + rowsData[index].itemAmount + (0.12 * rowsData[index].itemAmount)
                //             });
                //             break;
                //         }
                //         case "GST_18%": {
                //             this.setState({
                //                 taxAmount: this.state.taxAmount + (0.18 * rowsData[index].itemAmount),
                //                 totalAmount: this.state.totalAmount + rowsData[index].itemAmount + (0.18 * rowsData[index].itemAmount)
                //             });
                //             break;
                //         }
                //         case "GST_28%": {
                //             this.setState({
                //                 taxAmount: this.state.taxAmount + (0.28 * rowsData[index].itemAmount),
                //                 totalAmount: this.state.totalAmount + rowsData[index].itemAmount + (0.28 * rowsData[index].itemAmount)
                //             });
                //             break;
                //         }
                //         default: {
                //             this.setState({
                //                 totalAmount: this.state.totalAmount + rowsData[index].itemAmount
                //             });
                //         }
                //     }
                // }
                rowsData[index][id] = value;
                this.setState({
                    itemDetails: rowsData
                }, () => {
                    this.handleRowError(id, index);
                    this.calculateTax();
                });
            }
        }
        else {
            rowsData[index][id] = value;
            this.setState({
                itemDetails: rowsData
            }, () => {
                this.handleRowError(id, index);
                id === "itemTax" ? this.calculateTax() : "";
            });
        }
    }

    handleRowError = (id, index) => {
        console.log(id, index);
        if (this.state.itemDetails[index][id] === "" || this.state.itemDetails[index][id] === null) {
            this.setState((prevState) => {
                let rowsErrors = [...prevState.rowsErrors];
                rowsErrors[index][id] = true;
                return {
                    rowsErrors: rowsErrors
                };
            });
            return true;
        }
        else {
            this.setState((prevState) => {
                let rowsErrors = [...prevState.rowsErrors];
                rowsErrors[index][id] = false;
                return {
                    rowsErrors: rowsErrors
                };
            });
            return false;
        }
    }

    calculateTax = () => {
        let subTotal = 0, taxAmount = 0, totalAmount = 0;
        this.state.itemDetails.forEach((item) => {
            subTotal += item.itemAmount;
            switch (item.itemTax) {
                case "GST_5%": {
                    taxAmount += (0.05 * item.itemAmount);
                    break;
                }
                case "GST_12%": {
                    taxAmount += (0.12 * item.itemAmount);
                    break;
                }
                case "GST_18%": {
                    taxAmount += (0.18 * item.itemAmount);
                    break;
                }
                case "GST_28%": {
                    taxAmount += (0.28 * item.itemAmount);
                    break;
                }
            }
        });
        totalAmount = subTotal + taxAmount;
        this.setState({
            subTotal: subTotal.toFixed(2),
            taxAmount: taxAmount.toFixed(2),
            totalAmount: totalAmount.toFixed(2)
        });
    }

    handleSubmit = () => {
        var valid = true;
        var headerKeys;
        switch(this.state.headerPayload.genericType) {
            case "invoice": {
                headerKeys = [
                    // refId: "",
                    "custId",
                    // custRefId: "",
                    "custPos",
                    "docNo",
                    "docDate",
                    "refId",
                    "dueDate",
                    // taxableValue: 0,
                    // taxAmount: 0,
                    // totalValue: 0,
                    "termsCond",
                    // orgId: 0,
                    // entId: 0,
                    // createdBy: "",
                    // itemQtyS: "",
                    // itemRateS: "",
                    // itemAmountS: "",
                    // itemId: 0,
                    // itemIdS: "",
                ];
                break;
            }
            case "crnote": {
                headerKeys = [
                    // refId: "",
                    "custId",
                    // custRefId: "",
                    "custPos",
                    "docNo",
                    "docDate",
                    "prevInvId",
                    "referenceNo",
                    "reason",
                    // taxableValue: 0,
                    // taxAmount: 0,
                    // totalValue: 0,
                    // orgId: 0,
                    // entId: 0,
                    // createdBy: "",
                    // itemQtyS: "",
                    // itemRateS: "",
                    // itemAmountS: "",
                    // itemId: 0,
                    // itemIdS: "",
                ];
                break;
            }
            case "drnote": {
                headerKeys = [
                    // refId: "",
                    "custId",
                    // custRefId: "",
                    "custPos",
                    "docNo",
                    "docDate",
                    "prevInvId",
                    // taxableValue: 0,
                    // taxAmount: 0,
                    // totalValue: 0,
                    // orgId: 0,
                    // entId: 0,
                    // createdBy: "",
                    // itemQtyS: "",
                    // itemRateS: "",
                    // itemAmountS: "",
                    // itemId: 0,
                    // itemIdS: "",
                ];
                break;
            }
            case "estimate": {
                headerKeys = [
                    // refId: "",
                    "custId",
                    // custRefId: "",
                    "custPos",
                    "docNo",
                    "docDate",
                    "referenceNo",
                    // taxableValue: 0,
                    // taxAmount: 0,
                    // totalValue: 0,
                    "expiryDate",
                    // orgId: 0,
                    // entId: 0,
                    // createdBy: "",
                    // itemQtyS: "",
                    // itemRateS: "",
                    // itemAmountS: "",
                    // itemId: 0,
                    // itemIdS: "",
                ];
                break;
            }
            case "challan": {
                headerKeys = [
                    // refId: "",
                    "custId",
                    // custRefId: "",
                    "custPos",
                    "docNo",
                    "docDate",
                    "referenceNo",
                    // taxableValue: 0,
                    // taxAmount: 0,
                    // totalValue: 0,
                    "challanType",
                    // orgId: 0,
                    // entId: 0,
                    // createdBy: "",
                    // itemQtyS: "",
                    // itemRateS: "",
                    // itemAmountS: "",
                    // itemId: 0,
                    // itemIdS: "",
                ];
                break;
            }
        }
        headerKeys.forEach((key) => {
            this.handleError(key) ? valid = false : "";
        });
        if (valid) {
            let rowKeys = [
                // "itemDetailId",
                // "itemName",
                "itemDescription",
                "itemQty",
                "itemRate",
                "itemHSN",
                "itemTax",
                // "itemAmount"
            ];
            for (let i = 0; i < this.state.itemDetails.length; i++) {
                rowKeys.forEach((key) => {
                    this.handleRowError(key, i) ? valid = false : "";
                });
            }
        }
        if (valid) {
            console.log("FINAL PAYLOAD", this.state.headerPayload);
            console.log("STATE", this.state);
            this.props.createInvoiceRequest({
                ...this.state.headerPayload,
                totalValue: this.state.subTotal,
                taxAmount: this.state.taxAmount,
                taxableValue: this.state.totalAmount,
                itemDetails: this.state.itemDetails
            });
        }
    }

    render () {console.log(this.state);
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                {/* <div className="global-search-tab m0">
                                    <ul className="nav nav-tabs gst-inner" role="tablist">
                                        <li className="nav-item active" >
                                            <a className="nav-link gsti-btn" href="#invoicetab" role="tab" data-toggle="tab">Invoice</a>
                                        </li>
                                        <li className="nav-item" >
                                            <a className="nav-link gsti-btn" href="#creditdebitnote" role="tab" data-toggle="tab"> 
                                            <span className="gsti-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="15.003" height="14.905" viewBox="0 0 16.003 15.905">
                                                    <g id="invoice" transform="translate(0 -1.568)">
                                                        <g id="Group_3537" transform="translate(2.706 16.296)">
                                                            <g id="Group_3536">
                                                                <path fill="#000" id="Rectangle_850" d="M0 0H10.355V1.177H0z" class="cls-1"/>
                                                            </g>
                                                        </g>
                                                        <g id="Group_3539" transform="translate(5.531 6.824)">
                                                            <g id="Group_3538">
                                                                <path fill="#000" id="Path_1477" d="M181.158 169.726h-3.628a.588.588 0 0 0 0 1.177h3.628a.588.588 0 1 0 0-1.177z" class="cls-1" transform="translate(-176.941 -169.726)"/>
                                                            </g>
                                                        </g>
                                                        <g id="Group_3541" transform="translate(5.531 9.569)">
                                                            <g id="Group_3540">
                                                                <path fill="#000" id="Path_1478" d="M183.9 257.569h-6.374a.588.588 0 1 0 0 1.177h6.374a.588.588 0 0 0 0-1.177z" class="cls-1" transform="translate(-176.941 -257.569)"/>
                                                            </g>
                                                        </g>
                                                        <g id="Group_3543" transform="translate(0 1.568)">
                                                            <g id="Group_3542">
                                                                <path fill="#000" id="Path_1479" d="M15.656 1.639a.589.589 0 0 0-.632.1l-1.647 1.453-1.65-1.475a.588.588 0 0 0-.784 0L9.3 3.191 7.648 1.717a.588.588 0 0 0-.784 0L5.215 3.192 3.567 1.735a.588.588 0 0 0-.978.441v10.472h-2a.588.588 0 0 0-.589.589v1.432a2.808 2.808 0 0 0 2.8 2.8V16.3a1.63 1.63 0 0 1-1.628-1.628v-.843h9.218v.843a2.8 2.8 0 1 0 5.609 0V2.176a.588.588 0 0 0-.343-.537zm-.83 13.029a1.628 1.628 0 0 1-3.256 0v-1.431a.588.588 0 0 0-.588-.588H3.765V3.481l1.062.939a.588.588 0 0 0 .782 0l1.647-1.475L8.9 4.418a.588.588 0 0 0 .784 0l1.648-1.473 1.647 1.473a.589.589 0 0 0 .782 0l1.061-.939z" class="cls-1" transform="translate(0 -1.568)"/>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                                            Credit/Debit Note</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#estimates" role="tab" data-toggle="tab">
                                                <span className="gsti-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.422" height="19.563" viewBox="0 0 18.422 22.563">
                                                        <g>
                                                            <g>
                                                                <g>
                                                                    <path fill="#000" d="M62.576.423a.755.755 0 0 0-.821.125l-1.064.942L59.244.2a.766.766 0 0 0-1.022 0l-1.446 1.289L55.33.2a.766.766 0 0 0-1.022 0L52.86 1.49 51.8.548a.761.761 0 0 0-1.267.574v14.124a.762.762 0 0 0 1.269.574l1.064-.942 1.447 1.295a.766.766 0 0 0 1.022 0l1.446-1.294 1.446 1.293a.766.766 0 0 0 1.022 0l1.448-1.295 1.065.942a.761.761 0 0 0 1.267-.574V1.122a.755.755 0 0 0-.453-.699zM61.2 14.3a.765.765 0 0 0-1.018 0l-1.449 1.3-1.446-1.293a.766.766 0 0 0-1.022 0L54.819 15.6l-1.448-1.295a.761.761 0 0 0-1.015 0l-1.061.942V1.12l1.063.942a.765.765 0 0 0 1.018 0L54.819.767l1.446 1.293a.766.766 0 0 0 1.022 0L58.733.767l1.448 1.295a.761.761 0 0 0 1.015 0l1.061-.942V15.244z" transform="translate(-50.529 6.194) translate(50.529) translate(-50.529)"/>
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <path fill="#000" d="M116.386 203.527h-7.267a.383.383 0 1 0 0 .767h7.267a.383.383 0 1 0 0-.767z" transform="translate(-50.529 6.194) translate(52.76 7.801) translate(-108.736 -203.527)"/>
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <path fill="#000" d="M112.936 143.527h-3.817a.383.383 0 0 0 0 .767h3.817a.383.383 0 0 0 0-.767z" transform="translate(-50.529 6.194) translate(52.76 5.501) translate(-108.736 -143.527)"/>
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <path fill="#000" d="M116.386 263.527h-7.267a.383.383 0 0 0 0 .767h7.267a.383.383 0 0 0 0-.767z" transform="translate(-50.529 6.194) translate(52.76 10.101) translate(-108.736 -263.527)"/>
                                                                </g>
                                                            </g>
                                                            <path fill="#000" d="M6.449 1A5.449 5.449 0 1 0 11.9 6.449 5.456 5.456 0 0 0 6.449 1zm1.61 4.211a.372.372 0 0 1 0 .743h-.58a1.609 1.609 0 0 1-1.438 1.113l1.733 1.181a.372.372 0 0 1-.419.614L4.63 7a.372.372 0 0 1 .21-.679h1.114a.865.865 0 0 0 .709-.372H4.839a.372.372 0 0 1 0-.743H6.78a.865.865 0 0 0-.826-.619H4.839a.372.372 0 0 1 0-.743h3.22a.372.372 0 0 1 0 .743h-.753a1.6 1.6 0 0 1 .232.619z" transform="translate(-50.529 6.194) translate(57.052 -7.194)"/>
                                                        </g>
                                                    </svg>
                                                </span>
                                                Estimates</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#deliverychallan" role="tab" data-toggle="tab">
                                                <span className="gsti-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="13.747" height="14.432" viewBox="0 0 11.747 16.432">
                                                        <g>
                                                            <path fill="#000" d="M84.749 3.587a.48.48 0 0 0-.141-.333C81.29-.065 81.506.106 81.414.057A.483.483 0 0 0 81.229 0h-7.263A.964.964 0 0 0 73 .963v14.506a.964.964 0 0 0 .963.963h9.821a.964.964 0 0 0 .963-.963V3.587zm-3.081-1.911l1.437 1.437h-1.437zm-7.7 13.794V.963h6.74v2.632a.481.481 0 0 0 .481.481h2.6v11.393z" transform="translate(-73.003)"/>
                                                            <path fill="#000" d="M291.282 417.006h-1.8a.481.481 0 1 0 0 .963h1.8a.481.481 0 0 0 0-.963z" transform="translate(-73.003) translate(-209.068 -403.623)"/>
                                                            <path fill="#000" d="M144.16 257.006h-6.676a.481.481 0 0 0 0 .963h6.676a.481.481 0 1 0 0-.963z" transform="translate(-73.003) translate(-61.946 -250.758)"/>
                                                            <path fill="#000" d="M144.16 321.006h-6.676a.481.481 0 0 0 0 .963h6.676a.481.481 0 1 0 0-.963z" transform="translate(-73.003) translate(-61.946 -312.704)"/>
                                                        </g>
                                                    </svg>
                                                </span>
                                                Delivery Challan</a>
                                        </li>
                                    </ul>
                                </div> */}
                                {this.state.type == "invoice" || this.state.type == "crnote" || this.state.type == "drnote" ?
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/invoiceManagement/invoices")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>:null}
                                {this.state.type == "estimate" &&
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/invoiceManagement/estimate")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>}
                                {this.state.type == "challan" &&
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/invoiceManagement/deliveryChallan")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>}
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="save-draft">Save as Draft</button>
                                <button type="button" className="get-details" onClick={this.handleSubmit}>Save & Send</button>
                                <button type="button" className="ngr-clear">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.type == "invoice" &&
                <div className="col-lg-12 p-lr-47 m-top-20">
                    <div className="create-customer-details">
                        <div className="ccd-inner">
                            <div className="col-lg-7 pad-0">
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Select Customer</label>
                                    <select className={this.state.headerErrors.custId ? "errorBorder" : "onFocus"} id="custId" value={this.state.headerPayload.custId} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                        <option value="" disabled>Select Option</option>
                                        {this.state.customerList.map((item) => <option value={item.id}>{item.custFirstName + " " + item.custLastName}</option>)}
                                    </select>
                                    {this.state.headerErrors.custId && <span className="error">Customer is required</span>}
                                </div>
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Place of Supply</label>
                                    <select className={this.state.headerErrors.custPos ? "errorBorder" : "onFocus"} id="custPos" value={this.state.headerPayload.custPos} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                        <option value="" disabled>Select Option</option>
                                        {this.state.placeOfSupplyList.map((item) => <option value={item.id}>{item.name}</option>)}
                                    </select>
                                    {this.state.headerErrors.custPos && <span className="error">Place of Supply is required</span>}
                                </div>
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Invoice No</label>
                                    <input type="text" className={this.state.headerErrors.docNo ? "ccd-input errorBorder" : "ccd-input onFocus"} id="docNo" value={this.state.headerPayload.docNo} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                    {this.state.headerErrors.docNo && <span className="error">Invoice No is required</span>}
                                </div>
                            </div>
                            <div className="col-lg-5 pad-0">
                                <div className="col-lg-6 pad-lft-0">
                                    <label>Invoice Date</label>
                                    <input type="date" className={this.state.headerErrors.docDate ? "ccd-date errorBorder" : "ccd-date onFocus"} id="docDate" value={this.state.headerPayload.docDate} placeholder={this.state.headerPayload.docDate} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                    {this.state.headerErrors.docDate && <span className="error">Invoice Date is required</span>}
                                </div>
                                <div className="col-lg-6 pad-lft-0">
                                    <label>Order Number</label>
                                    <input type="text" className={this.state.headerErrors.refId ? "ccd-input errorBorder" : "ccd-input onFocus"} id="refId" value={this.state.headerPayload.refId} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                    {this.state.headerErrors.refId && <span className="error">Order Number is required</span>}
                                </div>
                            </div>
                            <div className="col-lg-12 pad-0 m-top-20">
                                <div className="col-lg-2 pad-lft-0">
                                    <label>Terms</label>
                                    <select className={this.state.headerErrors.termsCond ? "errorBorder" : "onFocus"} id="termsCond" value={this.state.headerPayload.termsCond} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                        <option value="">Select Option</option>
                                        {Object.keys(this.state.termsList).map((key) => <option value={key}>{this.state.termsList[key]}</option>)}
                                    </select>
                                    {this.state.headerErrors.termsCond && <span className="error">Terms is required</span>}
                                </div>
                                <div className="col-lg-2 pad-lft-0">
                                    <label>Due Date</label>
                                    <input type="date" className={this.state.headerErrors.dueDate ? "ccd-date errorBorder" : "ccd-date onFocus"} id="dueDate" value={this.state.headerPayload.dueDate} placeholder={this.state.headerPayload.dueDate} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                    {this.state.headerErrors.dueDate && <span className="error">Due Date is required</span>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
                {/* <div className="gen-pi-formate">
                    <div className="gpf-dbs">
                        <h3>Note Type</h3>
                        <ul className="gpf-radio-list m-top-20">
                            <li>
                                <label className="gen-radio-btn">
                                    <input id="notecredit" type="radio"  name="Note Type" checked={this.state.credit} onClick={this.noteType} />
                                    <span className="checkmark"></span>
                                    Credit
                                </label>
                            </li>
                            <li>
                                <label className="gen-radio-btn">
                                    <input id="notedebit" type="radio" name="Note Type" checked={this.state.debit} onClick={this.noteType} />
                                    <span className="checkmark"></span>
                                    Debit
                                </label>
                            </li>
                        </ul>
                    </div>
                </div> */}
                {this.state.type == "crnote" &&
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="create-customer-details m-top-20">
                        <div className="ccd-inner">
                            <div className="col-lg-12 pad-0">
                                <div className="col-lg-7 pad-0">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Select Customer</label>
                                        <select className={this.state.headerErrors.custId ? "errorBorder" : "onFocus"} id="custId" value={this.state.headerPayload.custId} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                            <option value="" disabled>Select Option</option>
                                            {this.state.customerList.map((item) => <option value={item.id}>{item.custFirstName + " " + item.custLastName}</option>)}
                                        </select>
                                        {this.state.headerErrors.custId && <span className="error">Customer is required</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Place of Supply</label>
                                        <select className={this.state.headerErrors.custPos ? "errorBorder" : "onFocus"} id="custPos" value={this.state.headerPayload.custPos} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                            <option value="" disabled>Select Option</option>
                                            {this.state.placeOfSupplyList.map((item) => <option value={item.id}>{item.name}</option>)}
                                        </select>
                                        {this.state.headerErrors.custPos && <span className="error">Place of Supply is required</span>}
                                    </div>
                                </div>
                                <div className="col-lg-12 pad-0 m-top-20">
                                    <div className="col-lg-7 pad-0">
                                        <div className="col-lg-4 pad-lft-0">
                                            <label>Credit Note</label>
                                            <input type="text" className={this.state.headerErrors.docNo ? "ccd-input errorBorder" : "ccd-input onFocus"} id="docNo" value={this.state.headerPayload.docNo} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                            {this.state.headerErrors.docNo && <span className="error">Credit Note is required</span>}
                                        </div>
                                        <div className="col-lg-4 pad-lft-0">
                                            <label>Credit Note Date</label>
                                            <input type="date" className={this.state.headerErrors.docDate ? "ccd-date errorBorder" : "ccd-date onFocus"} id="docDate" value={this.state.headerPayload.docDate} placeholder={this.state.headerPayload.docDate} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                            {this.state.headerErrors.docDate && <span className="error">Credit Note Date is required</span>}
                                        </div>
                                        <div className="col-lg-4 pad-lft-0">
                                            <label>Invoice</label>
                                            <select className={this.state.headerErrors.prevInvId ? "errorBorder" : "onFocus"} id="prevInvId" value={this.state.headerPayload.prevInvId} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                                <option value="" disabled>Select Option</option>
                                                {this.state.invoicesList.map((item) => <option value={item.id}>{item.doc_no}</option>)}
                                            </select>
                                            {this.state.headerErrors.prevInvId && <span className="error">Invoice is required</span>}
                                        </div>
                                    </div>
                                    <div className="col-lg-5 pad-0">
                                        <div className="col-lg-7 pad-lft-0">
                                            <label>Reason</label>
                                            <select className={this.state.headerErrors.reason ? "errorBorder" : "onFocus"} id="reason" value={this.state.headerPayload.reason} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                                <option value="" disabled>Select Option</option>
                                                {Object.keys(this.state.reasonList).map((key) => <option value={key}>{this.state.reasonList[key]}</option>)}
                                            </select>
                                            {this.state.headerErrors.reason && <span className="error">Reason is required</span>}
                                        </div>
                                        <div className="col-lg-5 pad-lft-0">
                                            <label>Reference</label>
                                            <input type="text" className={this.state.headerErrors.referenceNo ? "ccd-input errorBorder" : "ccd-input onFocus"} id="referenceNo" value={this.state.headerPayload.referenceNo} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                            {this.state.headerErrors.referenceNo && <span className="error">Reference is required</span>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
                {this.state.type == "drnote" &&
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="create-customer-details">
                        <div className="ccd-inner">
                            <div className="col-lg-12 pad-0">
                                <div className="col-lg-7 pad-0">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Select Customer</label>
                                        <select className={this.state.headerErrors.custId ? "errorBorder" : "onFocus"} id="custId" value={this.state.headerPayload.custId} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                            <option value="" disabled>Select Option</option>
                                            {this.state.customerList.map((item) => <option value={item.id}>{item.custFirstName + " " + item.custLastName}</option>)}
                                        </select>
                                        {this.state.headerErrors.custId && <span className="error">Customer is required</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Place of Supply</label>
                                        <select className={this.state.headerErrors.custPos ? "errorBorder" : "onFocus"} id="custPos" value={this.state.headerPayload.custPos} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                            <option value="" disabled>Select Option</option>
                                            {this.state.placeOfSupplyList.map((item) => <option value={item.id}>{item.name}</option>)}
                                        </select>
                                        {this.state.headerErrors.custPos && <span className="error">Place of Supply is required</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Debit Note</label>
                                        <input type="text" className={this.state.headerErrors.docNo ? "ccd-input errorBorder" : "ccd-input onFocus"} id="docNo" value={this.state.headerPayload.docNo} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {this.state.headerErrors.docNo && <span className="error">Debit Note is required</span>}
                                    </div>
                                </div>
                                <div className="col-lg-5 pad-0">
                                    <div className="col-lg-6 pad-lft-0">
                                        <label>Debit Note Date</label>
                                        <input type="date" className={this.state.headerErrors.docDate ? "ccd-date errorBorder" : "ccd-date onFocus"} id="docDate" value={this.state.headerPayload.docDate} placeholder={this.state.headerPayload.docDate} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {this.state.headerErrors.docDate && <span className="error">Debit Note Date is required</span>}
                                    </div>
                                    <div className="col-lg-6 pad-lft-0">
                                        <label>Invoice</label>
                                        <select className={this.state.headerErrors.prevInvId ? "errorBorder" : "onFocus"} id="prevInvId" value={this.state.headerPayload.prevInvId} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                            <option value="" disabled>Select Option</option>
                                            {this.state.invoicesList.map((item) => <option value={item.id}>{item.doc_no}</option>)}
                                        </select>
                                        {this.state.headerErrors.prevInvId && <span className="error">Invoice is required</span>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
                {this.state.type == "estimate" &&
                <div className="col-lg-12 p-lr-47 m-top-20">
                    <div className="create-customer-details">
                        <div className="ccd-inner">
                            <div className="col-lg-7 pad-0">
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Select Customer</label>
                                    <select className={this.state.headerErrors.custId ? "errorBorder" : "onFocus"} id="custId" value={this.state.headerPayload.custId} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                        <option value="" disabled>Select Option</option>
                                        {this.state.customerList.map((item) => <option value={item.id}>{item.custFirstName + " " + item.custLastName}</option>)}
                                    </select>
                                    {this.state.headerErrors.custId && <span className="error">Customer is required</span>}
                                </div>
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Place of Supply</label>
                                    <select className={this.state.headerErrors.custPos ? "errorBorder" : "onFocus"} id="custPos" value={this.state.headerPayload.custPos} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                        <option value="" disabled>Select Option</option>
                                        {this.state.placeOfSupplyList.map((item) => <option value={item.id}>{item.name}</option>)}
                                    </select>
                                    {this.state.headerErrors.custPos && <span className="error">Place of Supply is required</span>}
                                </div>
                            </div>
                            <div className="col-lg-12 pad-0 m-top-20">
                                <div className="col-lg-7 pad-0">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Estimate</label>
                                        <input type="text" className={this.state.headerErrors.docNo ? "ccd-input errorBorder" : "ccd-input onFocus"} id="docNo" value={this.state.headerPayload.docNo} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {this.state.headerErrors.docNo && <span className="error">Estimate is required</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Estimate Date</label>
                                        <input type="date" className={this.state.headerErrors.docDate ? "ccd-date errorBorder" : "ccd-date onFocus"} id="docDate" value={this.state.headerPayload.docDate} placeholder={this.state.headerPayload.docDate} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {this.state.headerErrors.docDate && <span className="error">Estimate Date is required</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Reference</label>
                                        <input type="text" className={this.state.headerErrors.referenceNo ? "ccd-input errorBorder" : "ccd-input onFocus"} id="referenceNo" value={this.state.headerPayload.referenceNo} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {this.state.headerErrors.referenceNo && <span className="error">Reference is required</span>}
                                    </div>
                                </div>
                                <div className="col-lg-5 pad-0">
                                    <div className="col-lg-6 pad-lft-0">
                                        <label>Expiry Date</label>
                                        <input type="date" className={this.state.headerErrors.expiryDate ? "ccd-date errorBorder" : "ccd-date onFocus"} id="expiryDate" value={this.state.headerPayload.expiryDate} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {this.state.headerErrors.expiryDate && <span className="error">Expiry Date is required</span>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
                {this.state.type == "challan" &&
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="create-customer-details">
                        <div className="ccd-inner">
                            <div className="col-lg-7 pad-0">
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Select Customer</label>
                                    <select className={this.state.headerErrors.custId ? "errorBorder" : "onFocus"} id="custId" value={this.state.headerPayload.custId} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                        <option value="" disabled>Select Option</option>
                                        {this.state.customerList.map((item) => <option value={item.id}>{item.custFirstName + " " + item.custLastName}</option>)}
                                    </select>
                                    {this.state.headerErrors.custId && <span className="error">Customer is required</span>}
                                </div>
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Place of Supply</label>
                                    <select className={this.state.headerErrors.custPos ? "errorBorder" : "onFocus"} id="custPos" value={this.state.headerPayload.custPos} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                        <option value="" disabled>Select Option</option>
                                        {this.state.placeOfSupplyList.map((item) => <option value={item.id}>{item.name}</option>)}
                                    </select>
                                    {this.state.headerErrors.custPos && <span className="error">Place of Supply is required</span>}
                                </div>
                            </div>
                            <div className="col-lg-12 pad-0 m-top-20">
                                <div className="col-lg-7 pad-0">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Delivery Challan</label>
                                        <input type="text" className={this.state.headerErrors.docNo ? "ccd-input errorBorder" : "ccd-input onFocus"} id="docNo" value={this.state.headerPayload.docNo} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {this.state.headerErrors.docNo && <span className="error">Delivery Challan is required</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Delivery Date</label>
                                        <input type="date" className={this.state.headerErrors.docDate ? "ccd-date errorBorder" : "ccd-date onFocus"} id="docDate" value={this.state.headerPayload.docDate} placeholder={this.state.headerPayload.docDate} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {this.state.headerErrors.docDate && <span className="error">Delivery Date is required</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Reference</label>
                                        <input type="text" className={this.state.headerErrors.referenceNo ? "ccd-input errorBorder" : "ccd-input onFocus"} id="referenceNo" value={this.state.headerPayload.referenceNo} onChange={(e) => this.handleChange(e.target.id, e.target.value)} />
                                        {/* <select className="onFocus">
                                            <option></option>
                                        </select> */}
                                        {this.state.headerErrors.referenceNo && <span className="error">Reference is required</span>}
                                    </div>
                                </div>
                                <div className="col-lg-5 pad-0">
                                    <div className="col-lg-6 pad-lft-0">
                                        <label>Challan Type</label>
                                        <select className={this.state.headerErrors.challanType ? "errorBorder" : "onFocus"} id="challanType" value={this.state.headerPayload.challanType} onChange={(e) => this.handleChange(e.target.id, e.target.value)}>
                                            <option value="" disabled>Select Option</option>
                                            {Object.keys(this.state.challanTypeList).map((key) => <option value={key}>{this.state.challanTypeList[key]}</option>)}
                                        </select>
                                        {this.state.headerErrors.challanType && <span className="error">Challan Type is required</span>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
                <div className="col-lg-12 p-lr-47 border-top">
                    <div className="invoice-detail-add-btn m-top-30">
                        <button type="button" onClick={this.addNewRow}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="11" height="11" viewBox="0 0 15 15">
                                <path fill="#41aca2" fill-rule="evenodd" d="M14.495 8.283V6.717H8.283V.505H6.717v6.212H.505v1.566h6.212v6.212h1.566V8.283h6.212"/>
                            </svg>
                            Add New Item
                        </button>
                    </div>
                    <div className="invoice-detail-table">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th className="fix-action-btn"></th>
                                    <th><label>Description</label></th>
                                    <th><label>Quantity</label></th>
                                    <th><label>Rate</label></th>
                                    <th><label>HSN/SAC</label></th>
                                    <th><label>Tax</label></th>
                                    <th><label>Amount</label></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.itemDetails.map((item, index) =>
                                    <tr key={index}>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner til-copy-btn" onClick={() => this.copyRow(index)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.079" height="14.079" viewBox="0 0 13.079 15.537">
                                                        <path fill="#2681ff" d="M8.224 15.537h-5.8A2.43 2.43 0 0 1 0 13.11V4.886a2.43 2.43 0 0 1 2.428-2.428h5.8a2.43 2.43 0 0 1 2.428 2.428v8.224a2.43 2.43 0 0 1-2.432 2.427zM2.428 3.672a1.215 1.215 0 0 0-1.214 1.214v8.224a1.215 1.215 0 0 0 1.214 1.214h5.8a1.215 1.215 0 0 0 1.21-1.214V4.886a1.215 1.215 0 0 0-1.214-1.214zm10.652 7.92V2.428A2.43 2.43 0 0 0 10.652 0H3.915a.607.607 0 1 0 0 1.214h6.737a1.215 1.215 0 0 1 1.214 1.214v9.165a.607.607 0 0 0 1.214 0zm0 0"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Copy</span>
                                                </li>
                                                <li className="til-inner til-edit-btn" onClick={() => this.deleteRow(index)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="10.514" height="10.514" viewBox="0 0 10.514 10.514">
                                                        <g id="prefix__Group_2093" data-name="Group 2093" transform="rotate(45 7.32 4.403)">
                                                            <rect fill="#12203c" id="prefix__Rectangle_304" width="1.939" height="12.93" class="prefix__cls-1" data-name="Rectangle 304" rx=".97" transform="translate(5.495)"/>
                                                            <rect fill="#12203c" id="prefix__Rectangle_305" width="1.939" height="12.93" class="prefix__cls-1" data-name="Rectangle 305" rx=".97" transform="rotate(90 3.717 9.213)"/>
                                                        </g>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        <td>
                                            <textarea className={this.state.rowsErrors[index].itemDescription ? "errorBorder" : "onFocus"} id={"itemDescription" + index} value={item.itemDescription} onChange={(e) => this.handleRowChange(e.target.id, e.target.value, index)}></textarea>
                                            <button type="button" onClick={(e) => this.openNewItem(e, index)}><img src={require('../../../assets/downArrowNew.svg')} /></button>
                                            {this.state.rowsErrors[index].itemDescription && <span className="error">Description is required</span>}
                                        </td>
                                        <td><label>
                                            <input type="text" className={this.state.rowsErrors[index].itemQty ? "errorBorder" : "onFocus"} id={"itemQty" + index} value={item.itemQty} onChange={(e) => this.handleRowChange(e.target.id, e.target.value, index)}></input>
                                            {this.state.rowsErrors[index].itemQty && <span className="error">Quantity is required</span>}
                                        </label></td>
                                        <td><label>
                                            <input type="text" className={this.state.rowsErrors[index].itemRate ? "errorBorder" : "onFocus"} id={"itemRate" + index} value={item.itemRate} onChange={(e) => this.handleRowChange(e.target.id, e.target.value, index)}></input>
                                            {this.state.rowsErrors[index].itemRate && <span className="error">Rate is required</span>}
                                        </label></td>
                                        <td><label>
                                            <input type="text" className={this.state.rowsErrors[index].itemHSN ? "errorBorder" : "onFocus"} id={"itemHSN" + index} value={item.itemHSN} onChange={(e) => this.handleRowChange(e.target.id, e.target.value, index)}></input>
                                            {this.state.rowsErrors[index].itemHSN && <span className="error">HSN is required</span>}
                                        </label></td>
                                        <td>
                                            <select className={this.state.rowsErrors[index].itemTax ? "errorBorder" : "onFocus"} id={"itemTax" + index} value={item.itemTax} onChange={(e) => this.handleRowChange(e.target.id, e.target.value, index)}>
                                                <option value="">Select Option</option>
                                                {Object.keys(this.state.taxList).map((key) => <option value={key}>{this.state.taxList[key]}</option>)}
                                            </select>
                                            {this.state.rowsErrors[index].itemTax && <span className="error">Tax is required</span>}
                                        </td>
                                        <td><span className="rupee">&#x20B9; </span><label>{item.itemAmount}</label></td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                    <div className="ci-total">
                        <div className="cit-inner">
                            <div className="citi-row">
                                <span className="citi-st">Sub Total</span>
                                <span className="citi-amount"><span className="rupee">&#x20B9; </span>{this.state.subTotal}</span>
                            </div>
                            <div className="citi-row">
                                <span className="citi-st">Tax</span>
                                <span className="citi-amount"><span className="rupee">&#x20B9; </span>{this.state.taxAmount}</span>
                            </div>
                            <div className="citi-row m-top-30">
                                <span className="citi-total">Total</span>
                                <span className="citi-tamount"><span className="rupee">&#x20B9; </span>{this.state.totalAmount}</span>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.addNewItem && <AddNewItem closeNewItem={this.closeNewItem} selectItem={this.selectItem} page={this.page} getAnyPage={this.getAnyPage} onSearch={this.onSearch} data={this.state.itemsData} />}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default CreateInvoice;