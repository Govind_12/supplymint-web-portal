import React from 'react';

class CreateItems extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            itemGoodsType: true,
            itemServiceType: false,
            itemName: "",
            itemNameError: false,
            itemUnit: "",
            itemUnitError: false,
            itemHSN: "",
            itemHSNError: false,
            taxableCheck: true,
            taxExemptCheck: false,
            sellingPrice: "",
            sellingPriceError: false,
            itemDescription: "",
            // itemDescriptionError: false,
            interTaxRate: "",
            interTaxRateError: false,
            intraTaxRate: "",
            intraTaxRateError: false,
            updatedInvoiceManagementData: sessionStorage.getItem("updatedInvoiceManagementData") == "" ? [] : JSON.parse(sessionStorage.getItem("updatedInvoiceManagementData")),
            id: "",
        }
    }

    componentDidMount(){
        this.state.updatedInvoiceManagementData.length && this.state.updatedInvoiceManagementData.map( data =>{
            this.setState({
                id: data.id,
                itemGoodsType: data.itemType == "Goods" ? true : false,
                itemServiceType: data.itemType == "Service" ? true : false,
                itemName: data.itemName == null ? "" : data.itemName,
                itemUnit: data.itemUnit == null ? "" : data.itemUnit,
                itemHSN: data.itemHSN == null ? "" : data.itemHSN,
                taxableCheck: data.taxPreference == "Taxable" ? true : false,
                taxExemptCheck: data.taxPreference == "Tax Exempt" ? true : false,
                sellingPrice: data.sellingPrice == null ? "" : data.sellingPrice,
                itemDescription: data.itemDescription == null ? "" : data.itemDescription,
                interTaxRate: Number(data.interTaxRate),
                intraTaxRate: Number(data.intraTaxRate)
            })
        })
    }

    handleChange =(e, id)=>{
        let value = e.target.value;

        if( id == "itemType")
           this.setState({ itemGoodsType: !this.state.itemGoodsType , itemServiceType: !this.state.itemServiceType})
        if( id == "itemName")
           this.setState({ itemName: value},()=>this.itemNameError())
        if( id == "itemUnit")
           this.setState({ itemUnit: value == "" ? "Select Unit" : value},()=>this.itemUnitError())
        
        if( id == "itemHSN")
            this.setState({ itemHSN: value},()=>this.itemHSNError())
        if( id == "taxPreference"){
            this.setState({ taxableCheck: !this.state.taxableCheck , taxExemptCheck: !this.state.taxExemptCheck})
        }
        if( id == "sellingPrice")
           this.setState({ sellingPrice: value},()=>this.sellingPriceError())
        if( id == "itemDescription")
            this.setState({"itemDescription": value}) 
        if( id == "interTaxRate")
            this.setState({ interTaxRate: value == "" ? "Select Tax Rate" : value},()=>this.interTaxRateError()) 
        if( id == "intraTaxRate")
            this.setState({ intraTaxRate: value == "" ? "Select Tax Rate" : value},()=>this.intraTaxRateError())
         
    }

    itemNameError =()=>{
        this.setState({
            itemNameError: this.state.itemName.trim().length > 0 ? false : true,
        })
    }
    itemUnitError =()=>{
        this.setState({
            itemUnitError: this.state.itemUnit.trim().length == 0 || this.state.itemUnit == "Select Unit" ? true : false,
        })
    }
    itemHSNError =()=>{
        this.setState({
            itemHSNError: this.state.itemHSN.trim().length > 0 ? false : true,
        })
    }
    sellingPriceError =()=>{
        let decimalPatternWithNumber = /^\d+(\.\d{1,2})?$/
        this.setState({
            sellingPriceError: decimalPatternWithNumber.test(this.state.sellingPrice) ? false : true,
        })
    }
    interTaxRateError =()=>{
        this.setState({
            interTaxRateError: this.state.interTaxRate.toString().trim().length == 0 || this.state.interTaxRate == "Select Tax Rate" ? true : false,
        })
    }
    intraTaxRateError =()=>{
        this.setState({
            intraTaxRateError: this.state.intraTaxRate.toString().trim().length == 0 || this.state.intraTaxRate == "Select Tax Rate" ? true : false,
        })
    }


    submitItem =(e)=>{
        this.itemNameError()
        this.itemUnitError()
        this.itemHSNError()
        this.sellingPriceError()
        this.interTaxRateError()
        this.intraTaxRateError()

        setTimeout(() => {
            const{itemGoodsType,itemServiceType,itemName,itemNameError,itemUnit,itemUnitError,itemHSN,
                itemHSNError,taxableCheck,taxExemptCheck,sellingPrice,sellingPriceError,itemDescription,
                interTaxRate,interTaxRateError,intraTaxRate,intraTaxRateError} = this.state

            if( !itemNameError && !itemUnitError && !itemHSNError && !sellingPriceError && !interTaxRateError && 
                !intraTaxRateError ) 
            {    

                let payload ={
                    id: this.state.id !== "" ? this.state.id : undefined,
                    itemType: itemGoodsType ? "Goods" : "Service",
                    itemName: itemName,
                    itemUnit: itemUnit,
                    itemHSN: itemHSN,
                    taxPreference: taxableCheck ? "Taxable" : "Tax Exempt",
                    sellingPrice: sellingPrice,
                    itemDescription: itemDescription,
                    interTaxRate: interTaxRate.toString(),
                    intraTaxRate: intraTaxRate.toString()
                }
                if( this.state.id == "")
                  this.props.saveInvoiceManagementItemRequest(payload)
                else
                  this.props.updateInvoiceManagementItemRequest(payload)
            }
        },10);
    }

    cancelItem =(e)=>{
       this.setState({
            itemGoodsType: true,
            itemServiceType: false,
            itemName: "",
            itemNameError: false,
            itemUnit: "",
            itemUnitError: false,
            itemHSN: "",
            itemHSNError: false,
            taxableCheck: true,
            taxExemptCheck: false,
            sellingPrice: "",
            sellingPriceError: false,
            itemDescription: "",
            // itemDescriptionError: false,
            interTaxRate: "",
            interTaxRateError: false,
            intraTaxRate: "",
            intraTaxRateError: false,
       })
    }

    onChangeFun =(e)=>{
        //No need just for ignore warning.
    }

    render () {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/invoiceManagement/items")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="get-details" onClick={this.submitItem}>Save</button>
                                <button type="button" className="ngr-clear" onClick={this.cancelItem}>Clear</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-lg-12 p-lr-47">
                    <div className="create-customer-details">
                        <div className="col-lg-12 pad-0 m-top-20">
                            <div className="gen-pi-formate">
                                <div className="gpf-dbs">
                                    <h3>Customer Type</h3>
                                    <ul className="gpf-radio-list m-top-20">
                                        <li>
                                            <label className="gen-radio-btn">
                                                <input type="radio" name="itemGoodsType" checked={this.state.itemGoodsType} onClick={(e)=>this.handleChange(e, "itemType")} onChange={this.onChangeFun} />
                                                <span className="checkmark"></span>
                                                Goods
                                            </label>
                                        </li>
                                        <li>
                                            <label className="gen-radio-btn">
                                                <input type="radio" name="itemServiceType" checked={this.state.itemServiceType} onClick={(e)=>this.handleChange(e, "itemType")} onChange={this.onChangeFun}/>
                                                <span className="checkmark"></span>
                                                Services
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="ccd-inner">
                            <div className="col-lg-12 pad-0 m-top-10">
                                <div className="col-lg-8 pad-0">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Name</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.itemName} onChange={(e)=>this.handleChange(e, "itemName")}/>
                                        {this.state.itemNameError && <span className="error">Enter name</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>Unit</label>
                                        <select className="onFocus" value={this.state.itemUnit} onChange={(e)=>this.handleChange(e,"itemUnit")}>
                                            <option value="">Select Unit</option>
                                            <option value="Public Limited">Public Limited</option>
                                            <option value="Private Limited">Private Limited</option>
                                            <option value="LLP">LLP</option>
                                            <option value="Partnership">Partnership</option>
                                            <option value="Proprietorship">Proprietorship</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        {this.state.itemUnitError && <span className="error">Select unit</span>}
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label>HSN Code</label>
                                        <input type="text" className="ccd-input onFocus" value={this.state.itemHSN} onChange={(e)=>this.handleChange(e, "itemHSN")}/>
                                        {this.state.itemHSNError && <span className="error">Enter HSN</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 pad-0 m-top-20">
                                <label>Tax Preference</label>
                                <div className="gen-pi-formate">
                                    <div className="gpf-dbs">
                                        <ul className="gpf-radio-list">
                                            <li>
                                                <label className="gen-radio-btn">
                                                    <input type="radio" name="taxableCheck" checked={this.state.taxableCheck} onClick={(e)=>this.handleChange(e, "taxPreference")} onChange={this.onChangeFun}/>
                                                    <span className="checkmark"></span>
                                                    Taxable
                                                </label>
                                            </li>
                                            <li>
                                                <label className="gen-radio-btn">
                                                    <input type="radio" name="taxExemptCheck" checked={this.state.taxExemptCheck} onClick={(e)=>this.handleChange(e, "taxPreference")} onChange={this.onChangeFun}/>
                                                    <span className="checkmark"></span>
                                                    Tax Exempt
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 border-top">
                    <div className="create-customer-details">
                        <div className="ccd-inner">
                            <div className="col-lg-8 pad-0 m-top-30">
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Selling Price <span className="mandatory">*</span></label>
                                    <input type="text" className="ccd-input onFocus" value={this.state.sellingPrice} onChange={(e)=>this.handleChange(e, "sellingPrice")}/>
                                    {this.state.sellingPriceError && <span className="error">Enter valid selling price</span>}
                                </div>
                            </div>
                            <div className="col-lg-8 pad-0 m-top-20">
                                <div className="col-lg-8 pad-lft-0">
                                    <label>Description</label>
                                    <textarea className="onFocus" value={this.state.itemDescription} onChange={(e)=>this.handleChange(e, "itemDescription")}></textarea>
                                </div>
                            </div>
                            <div className="col-lg-8 pad-0 m-top-30">
                                <h3>Default Tax Rates</h3>
                                <div className="col-lg-4 pad-lft-0 m-top-20">
                                    <label>Inter State Tax Rate</label>
                                    <select className="onFocus" value={this.state.interTaxRate} onChange={(e)=>this.handleChange(e,"interTaxRate")}>
                                        <option value="">Select Tax Rate</option>
                                        <option value="1.2">1.2</option>
                                        <option value="4.8">4.8</option>
                                        <option value="9.0">9.0</option>
                                        <option value="12.7">12.7</option>
                                        <option value="9.9">9.9</option>
                                        <option value="9.2">9.2</option>
                                    </select>
                                    {this.state.interTaxRateError && <span className="error">Select tax rate</span>}
                                </div>
                            </div>
                            <div className="col-lg-8 pad-0 m-top-20">
                                <div className="col-lg-4 pad-lft-0">
                                    <label>Intra State Tax Rate</label>
                                    <select className="onFocus" value={this.state.intraTaxRate} onChange={(e)=>this.handleChange(e,"intraTaxRate")}>
                                        <option value="">Select Tax Rate</option>
                                        <option value="1.2">1.2</option>
                                        <option value="4.8">4.8</option>
                                        <option value="9.0">9.0</option>
                                        <option value="12.7">12.7</option>
                                        <option value="9.9">9.9</option>
                                        <option value="9.2">9.2</option>
                                    </select>
                                    {this.state.intraTaxRateError && <span className="error">Select tax rate</span>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateItems;