import React from 'react'
import arrowIcon from '../../../../assets/prev-arrow.svg';
import fileIcon from '../../../../assets/fileIcon.svg';
import refreshIcon from '../../../../assets/refresh-block.svg';
import cloudIcon from '../../../../assets/cloud.svg';
import sendIcon from '../../../../assets/send.svg';
import userIcon from '../../../../assets/user.svg';
import checkIcon from '../../../../assets/success-summary.svg';
import { CONFIG } from "../../../../config/index";
import moment from 'moment';
import FilterLoader from "../../../loaders/filterLoader";
import ToastLoader from "../../../loaders/toastLoader";
import axios from "axios";

export default class QualityCheckVendorModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: "",
            allComment: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
            callAddComments: false,
            fileUploadQc: "",
            qualitySatisfy: "",
            reason: "",
            QC: [],
            invoice: [],
            isQCDone: "",
            isInvoiceUploaded: "FALSE",
            fileUploadedNew: false,
            toastLoader: false,
            toastMsg: "",
            invoiceNumber: "",
            invoiceDate: "",
            invoiceAmount: "",
            cartonCount: "",
            invoiceNumberError: "",
            invoiceDateError: "",
            invoiceAmountError: "",
            cartonCountError: "",
        }
    }
    handleChange(e) {
        this.setState({
            comment: e.target.value
        })

    }
    loadComments() {
        let payload = {
            shipmentId: this.props.checkedShipConfirmed[0].shipmentId,
            // poId: this.props.checkedShipConfirmed[0].poId,
            module: "SHIPMENT_CONFIRMED",
            orderNumber: this.props.orderNumber,
            orderCode: this.props.orderCode
        }
        this.props.getAllCommentRequest(payload)
    }
    // componentDidMount(){
    //     let payload = {
    //         shipmentId: this.props.checkedShipConfirmed[0].shipmentId,
    //         poId : this.props.checkedShipConfirmed[0].poId,
    //         module:"SHIPMENT_CONFIRMED",
    //         no : this.state.current + 1
    //     }
    //     this.props.getAllCommentRequest(payload)
    // }
    componentDidMount() {
        this.setState({
            poDetailsShipment: this.props.poDetailsShipment
        })
        let payload = {
            shipmentId: this.props.checkedShipConfirmed[0].shipmentId,
            // poId: this.props.checkedShipConfirmed[0].poId,
            module: "SHIPMENT_CONFIRMED",
            no: this.state.current + 1,
            orderNumber: this.props.checkedShipConfirmed[0].orderNumber,
            orderCode: this.props.checkedShipConfirmed[0].orderCode
        }
        this.props.getAllCommentRequest(payload)

    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getAllComment.isSuccess) {
            if (nextProps.shipment.getAllComment.data.resource != null) {
                let allComment = this.state.allComment
                let commentData = this.state.allComment.length == 0 ? nextProps.shipment.getAllComment.data.resource.COMMENTS : { ...allComment.reverse(), ...nextProps.shipment.getAllComment.data.resource.COMMENTS }
                let QCData = nextProps.shipment.getAllComment.data.resource.QC

                QCData.map((data) => data.qcVersion = "OLD")

                let invoice = nextProps.shipment.getAllComment.data.resource.INVOICE
                invoice.map((data) => data.invoiceVersion = "OLD")

                let reverseData = commentData.reverse()
                for (let i = 0; i < reverseData.length; i++) {
                    reverseData[i].commentVersion == "OLD"
                }
                this.setState({
                    allComment: reverseData,
                    isInvoiceUploaded: nextProps.shipment.getAllComment.data.resource.isInvoiceUploaded,
                    isQCDone: nextProps.shipment.getAllComment.data.resource.isQCDone,
                    qualitySatisfy: "",
                    QC: QCData,
                    invoice,
                    current: nextProps.shipment.getAllComment.data.currPage,
                    maxPage: nextProps.shipment.getAllComment.data.maxPage,
                }
                    // , () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight }
                )
            } else {
                this.setState({
                    allComment: [],

                })
            }
        }

    }

    handleComments = (e) => {
        this.setState({ comment: e.target.value })
    }

    addKey(e) {
        if (e.key == "Enter") {
            this.addComment(e.target.value)
        }
    }
    addComment(data) {
        if (data != "") {
            let date = new Date()

            let allComment = this.state.allComment
            let payload = {
                commentedBy: sessionStorage.getItem("prn"),
                comments: data,
                commentDate: moment(date).format("DD MMM YYYY HH:MM"),
                commentType: "TEXT",
                folderDirectory: "",
                folderName: "",
                filePath: "",
                fileName: "",
                fileURL: "",
                urlExpirationTime: "",
                isUploaded: "FALSE",
                uploadedOn: "",
                commentVersion: "NEW"
            }
            allComment.push(payload)
            this.setState(
                {
                    allComment: allComment,
                    callAddComments: true,
                    comment: ""
                }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
        }

    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    // onFileUpload(e ,subModule) {
    //     var id = e.target.id
    //     let files = Object.values(e.target.files)
    //     const fd = new FormData();
    //     let headers = {
    //         'X-Auth-Token': sessionStorage.getItem('token'),
    //         'Content-Type': 'multipart/form-data'
    //     }
    //     for (var i = 0; i < files.length; i++) {
    //         fd.append("files", files[i]);
    //     }
    //     axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload?poId=${this.props.checkedShipConfirmed[0].poId}&shipmentId=${this.props.checkedShipConfirmed[0].shipmentId}&module=SHIPMENT_CONFIRMED&subModule=${subModule}`, fd, { headers: headers })
    //         .then(res => {
    //             let result = res.data.data.resource
    //             if (id == "fileUpload") {
    //                 let QC = this.state.QC
    //                 for (let i = 0; i < result.length; i++) {
    //                     let payload = {
    //                         commentedBy: sessionStorage.getItem("prn"),
    //                         comments: "",
    //                         commentDate: "",
    //                         commentType: "URL",
    //                         folderDirectory: result[i].folderDirectory,
    //                         folderName: result[i].folderName,
    //                         filePath: result[i].filePath,
    //                         fileName: result[i].fileName,
    //                         fileURL: result[i].fileURL,
    //                         urlExpirationTime: result[i].urlExpirationTime,
    //                         qcVersion: "NEW",
    //                         uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
    //                         submodule: subModule

    //                     }
    //                     QC.push(payload)
    //                 }

    //                 this.setState({
    //                     QC
    //                 })
    //             } else {
    //                 let allComment = this.state.allComment

    //                 for (let i = 0; i < result.length; i++) {
    //                     let payload = {
    //                         commentedBy: sessionStorage.getItem("prn"),
    //                         comments: "",
    //                         commentDate: "",
    //                         commentType: "URL",
    //                         folderDirectory: result[i].folderDirectory,
    //                         folderName: result[i].folderName,
    //                         filePath: result[i].filePath,
    //                         fileName: result[i].fileName,
    //                         fileURL: result[i].fileURL,
    //                         urlExpirationTime: result[i].urlExpirationTime,
    //                         isUploaded: "TRUE",
    //                         commentVersion: "NEW",
    //                         uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
    //                         submodule: subModule
    //                     }
    //                     allComment.push(payload)
    //                 }

    //                 this.setState({
    //                     allComment,
    //                     callAddComments: true,
    //                 }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
    //             }

    //         }).catch((error) => {
    //             this.setState({
    //                 loader: false
    //             })
    //         })
    // }

    onFileUpload(e, subModule) {
        let files = Object.values(e.target.files)
        let date = new Date()
        let id = e.target.id;
        var allPdf = files.some((data) => (data.name.split('.').pop().toLowerCase() != "pdf"))
        if (!allPdf && id == "fileUpload" || id == "commentUpload") {
            let uploadNode = {
                orderCode: this.props.checkedShipConfirmed[0].orderCode,
                shipmentId: this.props.checkedShipConfirmed[0].shipmentId,
                orderNumber: this.props.checkedShipConfirmed[0].orderNumber,
                module: "SHIPMENT_CONFIRMED",
                subModule: subModule,
            }
            const fd = new FormData();
            this.setState({
                loader: true
            })
            let headers = {
                'X-Auth-Token': sessionStorage.getItem('token'),
                'Content-Type': 'multipart/form-data'
            }
            for (var i = 0; i < files.length; i++) {
                fd.append("files", files[i]);
            }
            fd.append("uploadNode", JSON.stringify(uploadNode))

            axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
                .then(res => {
                    this.setState({
                        loader: false
                    })
                    let result = res.data.data.resource

                    if (id == "fileUpload") {
                        let invoice = this.state.invoice
                        for (let i = 0; i < result.length; i++) {
                            let payload = {
                                commentedBy: sessionStorage.getItem("prn"),
                                comments: "",
                                commentDate: "",
                                commentType: "URL",
                                folderDirectory: result[i].folderDirectory,
                                folderName: result[i].folderName,
                                filePath: result[i].filePath,
                                fileName: result[i].fileName,
                                fileURL: result[i].fileURL,
                                urlExpirationTime: result[i].urlExpirationTime,
                                invoiceVersion: "NEW",
                                uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                                submodule: subModule

                            }
                            invoice.push(payload)
                        }
                        this.setState({
                            invoice,
                            loader: false,
                            fileUploadedNew: true
                        })
                    } else {
                        let allComment = this.state.allComment
                        for (let i = 0; i < result.length; i++) {
                            let payload = {
                                commentedBy: sessionStorage.getItem("prn"),
                                comments: "",
                                commentDate: "",
                                commentType: "URL",
                                folderDirectory: result[i].folderDirectory,
                                folderName: result[i].folderName,
                                filePath: result[i].filePath,
                                fileName: result[i].fileName,
                                fileURL: result[i].fileURL,
                                urlExpirationTime: result[i].urlExpirationTime,
                                isUploaded: "TRUE",
                                uploadedBy: sessionStorage.getItem('prn'),
                                commentVersion: "NEW",
                                uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                                submodule: subModule
                            }
                            allComment.push(payload)
                        }
                        this.setState({
                            loader: false,
                            allComment,
                            callAddComments: true,
                        }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
                    }
                }).catch((error) => {
                    this.setState({
                        loader: false
                    })
                })

        } else{
            this.setState({
                toastMsg: "Please Upload PDF file only ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        }
    }

    handleForm = (e) => {
        var name = e.target.name
        var value = e.target.value
        this.setState({
            [name]: value
        }, () => { 
            if(value == ""){
                this.setState({[name+"Error"] : true})
            }else{
                this.setState({[name+"Error"] : false})
            }
        })
    }
    handleValidation = () => {
        if (!this.state.cartonCount) {
            this.setState({ cartonCountError: true })
        }
        if (!this.state.invoiceDate) {
            this.setState({ invoiceDateError: true })
        }
        if (!this.state.invoiceAmount) {
            this.setState({ invoiceAmountError: true })
        }
        if (!this.state.invoiceNumber) {
            this.setState({ invoiceNumberError: true })
        }
    }
    submitForm = () => {
        this.handleValidation()
        setTimeout(() => {
            const { invoiceDateError, invoiceAmountError, invoiceNumberError, cartonCountError } = this.state
            if (!invoiceAmountError && !invoiceDateError && !invoiceNumberError && !cartonCountError) {
                let payload = {
                    shipmentId: this.props.checkedShipConfirmed[0].shipmentId,
                    invoiceNumber: this.state.invoiceNumber,
                    invoiceDate: this.state.invoiceDate + "T00:00+05:30",
                    invoiceAmount: this.state.invoiceAmount,
                    unitCount: this.state.cartonCount,
                }
                this.props.updateInvoiceRequest(payload)
            }
        }, 10)

    }

    onCloseModal() {
        // if (this.state.callAddComments || this.state.fileUploadedNew) {
        //     let commentData = []
        //     let allComment = this.state.allComment
        //     allComment.forEach((item) => {
        //         if (item.commentVersion == "NEW") {
        //             let data = {
        //                 commentedBy: item.commentedBy,
        //                 comments: item.comments,
        //                 commentDate: item.commentDate,
        //                 commentType: item.commentType,
        //                 folderDirectory: item.folderDirectory,
        //                 folderName: item.folderName,
        //                 filePath: item.filePath,
        //                 fileName: item.fileName,
        //                 fileURL: item.fileURL,
        //                 urlExpirationTime: item.urlExpirationTime,
        //                 isUploaded: item.isUploaded,
        //                 uploadedOn: item.uploadedOn,
        //                 subModule: "COMMENTS"
        //             }
        //             commentData.push(data)
        //         }
        //     })
        //     let payload = {
        //         poId: this.props.checkedShipConfirmed[0].poId,
        //         shipmentId: this.props.checkedShipConfirmed[0].shipmentId,
        //         module: "SHIPMENT_CONFIRMED",
        //         isQCDone: this.state.isQCDone,
        //         QCStatus: this.state.qualitySatisfy,
        //         isInvoiceUploaded: this.state.invoice.length != 0 ? "TRUE" : this.state.isInvoiceUploaded,
        //         orderNumber: this.props.checkedShipConfirmed[0].orderNumber,
        //         orderCode: this.props.checkedShipConfirmed[0].orderCode,
        //         reason: this.state.reason,
        //         SHIPMENT_CONFIRMED: {
        //             QC: [],
        //             COMMENTS: commentData,
        //             INVOICE: []
        //         }
        //     }
        //     this.props.qcAddCommentRequest(payload)
        // }

        this.props.handleModal()
    }

    deleteUploads(data) {
        let invoice = this.state.invoice.filter((check) => check.fileName != data.fileName)
        let files = []
        let a = {
            fileName: data.fileName,
            isActive: "FALSE"
        }
        files.push(a)
        let payload = {
            poId: this.props.checkedShipConfirmed[0].poId,
            shipmentId: this.props.checkedShipConfirmed[0].shipmentId,
            module: "SHIPMENT_CONFIRMED",
            subModule: "INVOICE",
            files: files
        }

        this.setState({ invoice })
        this.props.deleteUploadsRequest(payload)
    }

    openDownloadFile = data => e => {
        e.stopPropagation();
        window.open(data.fileURL);
    }
    render() {
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>
                <div className=" display_block">
                    <div className="modal-content modalRight createShipmentModal qualityCheckRight text-initial chatModalHeight">
                        <div className="col-md-12 pad-0">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle text-left chatModalTop">
                                    <div className="col-md-6 pad-0 alignMiddle">
                                        <img src={arrowIcon} onClick={(e) => this.onCloseModal(e)} className="displayPointer m-rgt-15" />
                                        <h2 className="displayInline m-rgt-30 removeAfter">Comments & Invoice</h2>
                                        {/* <p className="displayInline"><img src={checkIcon} />Confirmed by Vmart for final Shipment</p> */}
                                    </div>
                                    <div className="col-md-6 pad-0">
                                        <div className="float_Right">
                                            <button type="button" className="discardBtns m-rgt-10" onClick={(e) => this.onCloseModal(e)}>close</button>
                                            <button type="button" className="saveBtnBlue" onClick={this.submitForm}>Confirm</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="chatModalMid heightAuto">
                                    <div className="row m0">
                                        <div className="col-md-12 childHeightEqual">
                                            <div className="col-md-6 pad-lft-40">
                                                <div className="inputMainLabel">
                                                    <label className="displayBlock labelGlobal">Invoice Number<span className="mandatory">*</span></label>
                                                    <input type="text" className="inputGenericEvent" name="invoiceNumber" value={this.state.invoiceNumber} onChange={this.handleForm} placeholder={this.state.invoiceNumber == "" ? "Invoice Number" : this.state.invoiceNumber} />
                                                    {this.state.invoiceNumberError && <span className="error">Enter Invoice Number</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-6 pad-lft-40">
                                                <div className="inputMainLabel">
                                                    <label className="displayBlock labelGlobal">Invoice Date<span className="mandatory">*</span></label>
                                                    <input type="Date" className="inputGenericEvent" name="invoiceDate" value={this.state.invoiceDate} onChange={this.handleForm} placeholder={this.state.invoiceDate == "" ? "Invoice Date" : this.state.invoiceDate} />
                                                    {this.state.invoiceDateError && <span className="error">Enter Invoice Date</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-6 pad-lft-40">
                                                <div className="inputMainLabel">
                                                    <label className="displayBlock labelGlobal">Invoice Amount<span className="mandatory">*</span></label>
                                                    <input type="text" pattern="[0-9.]*" className="inputGenericEvent" name="invoiceAmount" value={this.state.invoiceAmount} onChange={this.handleForm} placeholder={this.state.invoiceAmount == "" ? "Invoice Amount" : this.state.invoiceAmount} />
                                                    {this.state.invoiceAmountError && <span className="error">Enter Invoice Amount</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-6 pad-lft-40">
                                                <div className="inputMainLabel">
                                                    <label className="displayBlock labelGlobal">No. of Cartons<span className="mandatory">*</span></label>
                                                    <input type="text" pattern="[0-9.]*" className="inputGenericEvent" name="cartonCount" value={this.state.cartonCount} onChange={this.handleForm} placeholder={this.state.cartonCount == "" ? "No. of Cartons" : this.state.cartonCount} />
                                                    {this.state.cartonCountError && <span className="error">Enter No. of Cartons</span>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-xs-12 pad-0 m-top-20">
                                        <div className="uploadInvoiceLeft heightAuto">
                                            <h3>Upload Invoice</h3>
                                            <div className="bottomToolTip displayInline">
                                                <label className="file customInput m-top-5 hoverMe" >
                                                    <input type="file" id="fileUpload" multiple="multiple" accept=".pdf" onChange={(e) => this.onFileUpload(e, "INVOICE")} />
                                                    <span >Choose file</span>
                                                    <img src={fileIcon} />
                                                </label>
                                                <p className="displayMe"><span></span></p>
                                                <div className="fileUploads">
                                                    {this.state.invoice.map((data, key) => <p key={key}>{data.fileName}<span onClick={() => this.deleteUploads(data)}>&#10005;</span></p>)}
                                                </div>
                                            </div>
                                            {/* <button type="button" className="uploadBtn">Upload</button> */}
                                        </div>
                                        <div className="uploadInvoiceRight verticalTop heightAuto">
                                            {this.state.QC.length == 0 ? <h4 className="textWarning">QC Not Available</h4> : <h4>QC is Available</h4>}
                                            <div className="posRelative">
                                                {this.state.QC.length == 0 ? <button type="button" className="downloadQcBtn btnDisabled removeAfter" ><img src={cloudIcon} className="m-rgt-10" />Download QC</button>
                                                    : <button type="button" className="downloadQcBtn dropdown-toggle removeAfter" data-toggle="dropdown" ><img src={cloudIcon} className="m-rgt-10" />Download QC</button>}
                                                <div className="filesDownloadDrop dropdown-menu">
                                                    <div className="fileUploads">
                                                        {this.state.QC.map((data, key) => (<p key={key} onClick={this.openDownloadFile(data)}>{data.fileName}</p>))}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="commentSection">
                                        <div className="commentHeader">
                                            <h2>Comment Section</h2>
                                            {this.state.current + 1 <= this.state.maxPage ? <button type="button" className="oldCommentBtn" onClick={(e) => this.loadComments(e)}><img src={refreshIcon} />Load old Comments</button> : null}

                                        </div>
                                        <div className="comments comment_overflow" id="scrollBot">
                                            <div className="messages displayInline width100">
                                                {this.state.allComment.length != 0 ? this.state.allComment.map((data, key) => (
                                                    <div key={key} className={sessionStorage.getItem("prn") == data.commentedBy ? "messageReceiver displayInline width100" : "messageSender displayInline width100"}>
                                                        <div className="each-comment m-top-15">
                                                            <div className="msgBg">
                                                                <img src={userIcon} />
                                                                {data.commentType == "TEXT" ? <label>{data.comments}</label> : <div><a href={data.fileURL} className="fileNameQc" target="_blank" >{data.fileName}</a></div>}
                                                            </div>
                                                            {data.commentType == "TEXT" ? <span>{data.commentDate}</span> : <span>{data.uploadedOn}</span>}
                                                        </div>

                                                    </div>
                                                )) : null}
                                            </div>
                                        </div>
                                        <div className="writeComment">
                                            <div className="fileInput">
                                                <input type="text" placeholder="Write your comment..." value={this.state.comment} onKeyDown={(e) => this.addKey(e)} onChange={(e) => this.handleChange(e)} />
                                                <label className="custom-file-Icon">
                                                    <input type="file" id="commentUpload" multiple="multiple" onChange={(e) => this.onFileUpload(e, "COMMENTS")} />
                                                    <img src={fileIcon} />
                                                </label>
                                                <img src={sendIcon} className="sendIcon" onClick={(e) => this.addComment(this.state.comment)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>

        )
    }
}