import React, { Component } from 'react'
import exclaimIcon from '../../../../assets/exclain.svg';
import moment from 'moment';
import { handleChangeModal } from '../../../../helper/index'
import QualityCheckVendorModal from './qualityCheckVendorModal';
import Pagination from '../../../pagination';
import searchIcon from '../../../../assets/close-recently.svg';
import Close from '../../../../assets/close-red.svg';
import axios, { post } from "axios";
import { CONFIG } from "../../../../config/index";
import ToastLoader from '../../../loaders/toastLoader';
import fileIcon from '../../../../assets/fileIcon.svg';
import cloudIcon from '../../../../assets/iconmonstr-note.svg';
import Eye from '../../../../assets/eye-icon.svg';
import Barcode from '../../../../assets/barcode.svg';
import FilterLoader from '../../../loaders/filterLoader';
import CreateAsnUpload from '../../vendor/orders/createAsnUpload';

const todayDate = moment(new Date()).format("YYYY-MM-DD"); 

export default class ConfirmShipmentVendorModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rangeVal: 1,
            poDetailsShipment: [],
            sliderDisable: false,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            qualityModal: false,
            qcUpload: true,
            toastLoader: false,
            toastMsg: "",

            allQC: "",
            invoice: [],
            packagingInvoice: [],
            QC: [],
            multipleInvoice: [...this.props.checkedShipConfirmed],
            showDrop: false,
            prevId: "",
            showInvoice: false,
            showPackagingInvoice: false,
            prevInvoiceID: "",

            fileNames: [],
            packagingFileName: [],
            barcodeDrop: false,
            barcode: [],

            invoiceUplaodFlag: this.props.invoiceUplaodFlag,
            allowPackingList: this.props.allowPackingList,

            ppsUploadModal: false,
            currentAsnId: 0,
        }
    }
    componentDidMount() {
        var data = this.state.multipleInvoice
        data.map((data) => (
            data.invoiceNo = "",
            data.invoiceNoErr = "",
            data.invoiceDate = "",
            data.invoiceDateErr = "",
            data.invoiceAmt = data.basicAmount != null ? data.basicAmount : 0,
            data.invoiceAmtErr = "",
            data.noCartoon = "",
            data.noCartoonErr = "",
            data.invoiceUploadErr = "",
            data.isAlreadyUploadedPacking = data.isPackagingUploaded,
            data.invoiceArray = [],
            data.packingArray = [] 
        ))
        this.setState({ multipleInvoice: data })
        // this.props.getStatusButtonActiveRequest('asd');
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getAllComment.isSuccess) {
            if (nextProps.shipment.getAllComment.data.resource != null) {
                this.setState({
                    QC: nextProps.shipment.getAllComment.data.resource.QC,
                    barcode: nextProps.shipment.getAllComment.data.resource.BARCODE,
                    allQC: nextProps.shipment.getAllComment.data.resource,
                    //invoice: nextProps.shipment.getAllComment.data.resource.INVOICE,
                    //packagingInvoice: nextProps.shipment.getAllComment.data.resource.INVOICE,
                })
            }
            if (nextProps.shipment.getAllComment.data.resource.INVOICE.length > 0){
                
                if (nextProps.shipment.getAllComment.data.resource.INVOICE[0].subModule == "INVOICE"){
                    this.setState({ 
                        invoice: nextProps.shipment.getAllComment.data.resource.INVOICE,  
                    })
                }
                else{
                    this.setState({ 
                        packagingInvoice: nextProps.shipment.getAllComment.data.resource.INVOICE,  
                    })
                }
            }
            this.props.getAllCommentClear();
        }

        if (nextProps.shipment.deleteUploads.isSuccess) {
            var multipleInvoice = [...this.state.multipleInvoice]
            multipleInvoice.map((data) => {
                if (data.id == this.state.deletedId) {
                    data.isInvoiceUploaded = nextProps.shipment.deleteUploads.data.isInvoiceAvailable;
                    data.isPackagingUploaded = nextProps.shipment.deleteUploads.data.isPackagingAvailable;
                    if (nextProps.shipment.deleteUploads.data.isInvoiceAvailable == "FALSE") {
                        this.setState({ showDrop: false, showInvoice: false, })
                    }
                    if( nextProps.shipment.deleteUploads.data.isPackagingAvailable == "FALSE")
                       this.setState({ showDrop: false, showPackagingInvoice: false, })
                }
            })
            this.setState({ multipleInvoice })
            this.props.deleteUploadsClear();
        }

        // if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) {
        //     if( nextProps.logistic.getButtonActiveConfig.data.resource.invoiceUploadRequired != undefined ){
        //         this.setState({ invoiceUplaodFlag: nextProps.logistic.getButtonActiveConfig.data.resource.invoiceUploadRequired === 'TRUE' ? true : false},()=>{
        //             if( !this.state.invoiceUplaodFlag ){
        //                 this.props.getInspOnOffConfigRequest('asd');
        //             }
        //         })
        //         this.props.getStatusButtonActiveClear()
        //     }
        // }
        // if (nextProps.replenishment.inspectionOnOff.isSuccess) {
        //     if (nextProps.replenishment.inspectionOnOff.data.invoice != null && nextProps.replenishment.inspectionOnOff.data.invoice != "") {
        //          this.setState({
        //             invoiceUplaodFlag: nextProps.replenishment.inspectionOnOff.data.invoice === "TRUE" ? true : false,  
        //          })
        //     }
        //     this.props.getInspOnOffConfigClear();
        // }
    }

    onDelete = (e, shipmentId) => {
        let multipleInvoice = [...this.state.multipleInvoice]
        if (multipleInvoice.length > 1) {
            multipleInvoice = multipleInvoice.filter((data) => data.shipmentId != shipmentId)
            this.setState({ multipleInvoice, fileNames: [], packagingFileName: [] })
        } else {
            this.setState({
                toastMsg: "Cannot be deleted",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        }
    }
    invoiceNo = (shipmentId) => {
        let multipleInvoice = [...this.state.multipleInvoice]
        if (shipmentId != undefined) {
            multipleInvoice.map((data) => data.invoiceNo == "" && shipmentId == data.shipmentId ? (data.invoiceNoErr = true) : (data.invoiceNoErr = false))
        } else {
            multipleInvoice.map((data) => data.invoiceNo == "" ? (data.invoiceNoErr = true) : (data.invoiceNoErr = false))
        }
        this.setState({ multipleInvoice })
    }
    invoiceDate = (shipmentId) => {
        let multipleInvoice = [...this.state.multipleInvoice]
        if (shipmentId != undefined) {
            multipleInvoice.map((data) => data.invoiceDate == "" && shipmentId == data.shipmentId ? (data.invoiceDateErr = true) : (data.invoiceDateErr = false))
        } else {
            multipleInvoice.map((data) => data.invoiceDate == "" ? (data.invoiceDateErr = true) : (data.invoiceDateErr = false))
        }
        this.setState({ multipleInvoice })
    }
    invoiceAmt = (shipmentId) => {   
        let decimalPattern = /^\d+(\.\d{1,2})?$/
        let multipleInvoice = [...this.state.multipleInvoice]
        if (shipmentId != undefined) {
            multipleInvoice.map((data) => (data.invoiceAmt == "" || data.invoiceAmt <= 0 || !decimalPattern.test(data.invoiceAmt)) && shipmentId == data.shipmentId ? (data.invoiceAmtErr = true) : (data.invoiceAmtErr = false))
        } else {
            multipleInvoice.map((data) => (data.invoiceAmt == "" || data.invoiceAmt <= 0 || !decimalPattern.test(data.invoiceAmt)) ? (data.invoiceAmtErr = true) : (data.invoiceAmtErr = false))
        }
        this.setState({ multipleInvoice })
    }
    noCartoon = (shipmentId) => {
        let multipleInvoice = [...this.state.multipleInvoice]
        let numberPattern = /^[0-9]+$/;
        if (shipmentId != undefined) {
            multipleInvoice.map((data) => (data.noCartoon == "" || data.noCartoon <= 0 || !numberPattern.test(data.noCartoon)) && shipmentId == data.shipmentId ? (data.noCartoonErr = true) : (data.noCartoonErr = false))
        } else {
            multipleInvoice.map((data) => (data.noCartoon == "" || data.noCartoon <= 0 || !numberPattern.test(data.noCartoon)) ? (data.noCartoonErr = true) : (data.noCartoonErr = false))
        }
        this.setState({ multipleInvoice })
    }
    handleFields = (e, shipmentId, type) => {
        let multipleInvoice = [...this.state.multipleInvoice]
        if (type == "invoiceNo") {
            if (e.target.validity.valid) {
                multipleInvoice.map((data) => (data.shipmentId == shipmentId && (data.invoiceNo = e.target.value)))
            }
            this.setState({ multipleInvoice }, () => this.invoiceNo(shipmentId))
        } else if (type == "invoiceDate") {
            multipleInvoice.map((data) => (data.shipmentId == shipmentId && (data.invoiceDate = e.target.value)))
            this.setState({ multipleInvoice }, () => this.invoiceDate(shipmentId))
        } else if (type == "invoiceAmt") {
            multipleInvoice.map((data) => (data.shipmentId == shipmentId && (data.invoiceAmt = e.target.value)))
            this.setState({ multipleInvoice }, () => this.invoiceAmt(shipmentId))
        } else if (type == "noCartoon") {
            multipleInvoice.map((data) => (data.shipmentId == shipmentId && (data.noCartoon = e.target.value)))
            this.setState({ multipleInvoice }, () => this.noCartoon(shipmentId))
        // }else if ( type == "invoiceUpload") {
        //     multipleInvoice.map((data) => (data.shipmentId == shipmentId && (data.invoiceUploadErr = data.isInvoiceUploaded == "TRUE" ? "FALSE" : "TRUE")))
        //     this.setState({ multipleInvoice })
        }
    }

    onFileUpload(e, subModule, data) {
        let files = Object.values(e.target.files)
        let multipleInvoice = [...this.state.multipleInvoice]
        let date = new Date()
        let id = e.target.id;
        var fileNames = files.map((item, index) => Object.values(e.target.files)[index].name)

        // var allPdf = files.some((data) => (data.name.split('.').pop().toLowerCase() != "pdf"))
        // if (!allPdf && id == "fileUpload" || id == "commentUpload") {
        let uploadNode = {
            orderCode: data.orderCode,
            shipmentId: data.shipmentId,
            orderNumber: data.orderNumber,
            module: "SHIPMENT",
            subModule: subModule,
            orderId: data.orderId,
            documentNumber: data.documentNumber,
            commentId: data.id,
            commentCode: data.shipmentAdviceCode,
            vendorCode: data.vendorCode,
        }
        const fd = new FormData();

        if( subModule == "PACKAGING")
           this.setState({ loader: true, packagingFileName: fileNames,expandedID: data.id, showPackagingInvoice: false,})
        else if (subModule == "INVOICE")   
           this.setState({ loader: true, fileNames,expandedID: data.id, showInvoice: false, })

        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        for (var i = 0; i < files.length; i++) {
            fd.append("files", files[i]);
        }
        fd.append("uploadNode", JSON.stringify(uploadNode))
        axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
            .then(res => {
                this.setState({ loader: false })
                let result = res.data.data.resource
                if (res.data.status == "2000") {
                    multipleInvoice.map((hdata) => {
                        if (hdata.id == data.id) {
                            hdata.isInvoiceUploaded = res.data.data.resource[0].isInvoiceAvailable;
                            hdata.isPackagingUploaded = res.data.data.resource[0].isPackagingAvailable;
                            data.invoiceUploadErr = res.data.data.resource[0].isInvoiceAvailable === "TRUE" ? "FALSE" : "TRUE";
                        }
                    })
                }
                if (id == "fileUpload") {
                    let invoice = this.state.invoice;
                    let packagingInvoice = this.state.packagingInvoice;

                    for (let i = 0; i < result.length; i++) {
                        let payload = {
                            commentedBy: sessionStorage.getItem("prn"),
                            comments: "",
                            commentDate: "",
                            commentType: "URL",
                            folderDirectory: result[i].folderDirectory,
                            folderName: result[i].folderName,
                            filePath: result[i].filePath,
                            fileName: result[i].fileName,
                            fileURL: result[i].fileURL,
                            urlExpirationTime: result[i].urlExpirationTime,
                            invoiceVersion: "NEW",
                            uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                            submodule: subModule

                        }
                        if( subModule == "PACKAGING")
                           packagingInvoice.push(payload)
                        else if( subModule == "INVOICE")
                           invoice.push(payload)
                    }
                    this.setState({
                        invoice,
                        packagingInvoice,
                        loader: false,
                        fileUploadedNew: true,
                        multipleInvoice
                    })
                } else {
                    let allComment = this.state.allComment
                    for (let i = 0; i < result.length; i++) {
                        let payload = {
                            commentedBy: sessionStorage.getItem("prn"),
                            comments: "",
                            commentDate: "",
                            commentType: "URL",
                            folderDirectory: result[i].folderDirectory,
                            folderName: result[i].folderName,
                            filePath: result[i].filePath,
                            fileName: result[i].fileName,
                            fileURL: result[i].fileURL,
                            urlExpirationTime: result[i].urlExpirationTime,
                            isUploaded: "TRUE",
                            uploadedBy: sessionStorage.getItem('prn'),
                            commentVersion: "NEW",
                            uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                            submodule: subModule
                        }
                        allComment.push(payload)
                    }
                    this.setState({
                        loader: false,
                        allComment,
                        callAddComments: true,
                        multipleInvoice
                    }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
                }
            }).catch((error) => {
                this.setState({
                    loader: false
                })
                this.props.openToastError()
            })
        e.target.value = ''
    }

    onSubmit = () => {
        this.invoiceAmt()
        this.invoiceDate()
        this.noCartoon()
        this.invoiceNo()
        this.invoiceUpload()
        let flag = false
        setTimeout(() => {
            let multipleInvoice = this.state.multipleInvoice
            multipleInvoice.forEach((data) => {
                if (data.invoiceAmtErr == true || data.invoiceDateErr == true || data.noCartoonErr == true || data.invoiceNoErr == true || data.invoiceUploadErr == "TRUE") {
                    flag = true
                    return;
                }
            })
            if (!flag) {
                let push = []
                multipleInvoice.map((hdata) => {
                    let payload = {
                        "shipmentId": hdata.shipmentId,
                        "invoiceNumber": hdata.invoiceNo,
                        "invoiceDate": hdata.invoiceDate + "T00:00+05:30",
                        "invoiceAmount": hdata.invoiceAmt,
                        "unitCount": hdata.noCartoon,
                        "shipmentAdviceCode": hdata.shipmentAdviceCode,

                        "vendorName": hdata.vendorName,
                        "documentNumber": hdata.documentNumber,
                        "orderNumber": hdata.orderNumber,
                        "siteDetails": hdata.siteDetails,
                        "isInvoiceUploaded": hdata.isInvoiceUploaded,
                        "isPackagingUploaded": hdata.isPackagingUploaded
                    }
                    push.push(payload)
                })
                let finalPayLoad = {
                    updateInvoiceList: push
                }
                this.props.updateInvoiceRequest(finalPayLoad)
                //Calling File upload after submission::
                this.props.onFileUploadProps(multipleInvoice)
            }
        }, 10)
    }


    dowloadInvoice(e, data, subModule) {

        if (!this.state.showInvoice || !this.state.showPackagingInvoice || this.state.prevInvoiceID !== data.shipmentId) {
            let payload = {
                //shipmentId: data.shipmentId,
                module: "SHIPMENT",
                pageNo: 1,
                //orderNumber: data.orderNumber,
                //orderCode: data.orderCode,
                subModule: subModule,
                //refresh: "fullPage"
                isAllAttachment: "",
                type: 1,
                search: "",
                commentId: data.id,
                commentCode: data.shipmentAdviceCode

            }
            this.props.getAllCommentRequest(payload)
            if( subModule == "INVOICE")
               this.setState({ showInvoice: !this.state.showInvoice })
            else if( subModule == "PACKAGING")
               this.setState({ showPackagingInvoice: !this.state.showPackagingInvoice })   
            this.setState({ prevInvoiceID: data.shipmentId, expandedID: data.id, fileNames: [], packagingFileName: [] })
        } else {
            if( subModule == "INVOICE")
               this.setState({ showInvoice: false })
            else if( subModule == "PACKAGING")
               this.setState({ showPackagingInvoice: false}) 
        }
    }
    viewAllAttachment = (data, type) => {
        if (!this.state.showDrop || this.state.prevId !== data.shipmentId) {
            let payload = {
               //shipmentId: data.shipmentId,
               module: "SHIPMENT",
               pageNo: 1,
               //orderNumber: data.orderNumber,
               //orderCode: data.orderCode,
               subModule: type,
               //refresh: "fullPage"
               isAllAttachment: "TRUE",
               type: 1,
               search: "",
               commentId: data.id,
               commentCode: data.shipmentAdviceCode
            }
            this.props.getAllCommentRequest(payload)
            this.setState({ showDrop: true, barcodeDrop: false, prevId: data.shipmentId, expandedID: data.id, fileNames: [] })
        } else {
            this.setState({ showDrop: false })
        }
    }

    dowloadBarcode = (data) => {
        if (!this.state.barcodeDrop || this.state.prevId !== data.id) {
            let payload = {
                module: "SHIPMENT",
                pageNo: 1,
                subModule: "BARCODE",
                isAllAttachment: "TRUE",
                type: 1,
                search: "",
                commentId: data.id,
                commentCode: data.shipmentAdviceCode

            }
            this.props.getAllCommentRequest(payload)
            this.setState({ showDrop: false, barcodeDrop: true, prevId: data.id, expandedID: data.id, fileNames: [], packagingFileName: [] })
        } else {
            this.setState({ barcodeDrop: false })
        }
        this.setState({ barcodeDrop: !this.state.barcodeDrop })
    }

    deleteUploads(data, multipleInvoice, subModule) {
        let invoice = this.state.invoice.filter((check) => check.fileURL != data.fileURL)
        let packagingInvoice = this.state.packagingInvoice.filter((check) => check.fileURL != data.fileURL)
        let files = []
        let a = {
            fileName: data.fileName,
            isActive: "FALSE",
            //packagingFileName: data.packagingFileName
        }
        files.push(a)
        let payload = {
            shipmentId: multipleInvoice.shipmentId,
            module: "SHIPMENT",
            subModule: subModule,
            isQCAvailable: multipleInvoice.isQCUploaded,
            isInvoiceAvailable: multipleInvoice.isInvoiceUploaded,
            isBarcodeAvailable: multipleInvoice.isBarcodeUploaded,
            isPackagingAvailable: multipleInvoice.isPackagingUploaded,
            files: files,
            commentId: multipleInvoice.id
        }
        this.setState({
            selectedFiles: files,
            invoice,
            packagingInvoice,
            deletedId: multipleInvoice.id
        })
        this.props.deleteUploadsRequest(payload)
    }

    invoiceUpload =(e)=>{
        let multipleInvoice = [...this.state.multipleInvoice]
        if( this.state.invoiceUplaodFlag ){
              multipleInvoice.map((data) => { 
                 if(data.isInvoiceUploaded == "FALSE")
                    data.invoiceUploadErr = "TRUE"
              });
        }else{
            multipleInvoice.map((data) => data.invoiceUploadErr = "FALSE" );
        }
    }

    //---------------------------------------- New File Upload ---------------------------// 
    openPpsUploadModal =(e, data, fileType)=> {
        e.preventDefault();
        let multipleInvoice = this.state.multipleInvoice
        multipleInvoice.map( item => {
            if(data.id == item.id){
                data.uploadFileType = fileType
            }  
        })
        this.setState({
            multipleInvoice,
            ppsUploadModal: !this.state.ppsUploadModal,
            currentAsnId: data.id,
        },()=> document.addEventListener('click', this.clickClosePpsModal));
    }

    onFileUploadSubmit = (fileList, moduleName) =>{
        let multipleInvoice = this.state.multipleInvoice
        if(moduleName == "INVOICE"){ 
            multipleInvoice.map( data => {
                if(data.id == this.state.currentAsnId){
                    data.invoiceArray = fileList
                    data.fileType = "INVOICE"
                    data.isInvoiceUploaded = fileList.length ? "TRUE" : "FALSE"
                    data.invoiceUploadErr = fileList.length ? "FALSE" : this.state.invoiceUplaodFlag ? "TRUE" : "FALSE"
                }  
            })  
        }
        if(moduleName == "PACKAGING"){ 
            multipleInvoice.map( data => {
                if(data.id == this.state.currentAsnId){
                    data.packingArray = fileList
                    data.fileType = "PACKAGING"
                }  
            })  
        }
        this.setState({
            multipleInvoice,
            ppsUploadModal: false,
        },()=> document.removeEventListener('click', this.clickClosePpsModal))
    }

    CancelPpsUploadModal = (e) => {
        this.setState({
            ppsUploadModal: false,
        },()=> document.removeEventListener('click', this.clickClosePpsModal))
    }

    clickClosePpsModal =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop")){
            this.setState({ ppsUploadModal: false})
        }
    }
    //-----------------------------------------------------------------------------------------//

    render() {
        const { qualityModal } = this.state
        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block modal-backdrop-new"></div>
                    <div className="display_block">
                        <div className="col-md-12 col-sm-12 modal-content modalShow pad-0 vendor-ship-aprv-asn">
                            <div className="modal_Color selectVendorPopUp pad-bot-20">
                                <div className="modalTop alignMiddle shadowBox">
                                    <div className="col-md-4 pad-0">
                                        <h2 className="displayInline m-rgt-20">Upload Invoice</h2>
                                        {/* <span className="shipmentNo m-rgt-20">{this.state.poDetailsShipment.orderNumber}</span> */}
                                    </div>
                                    <div className="col-md-8 pad-0">
                                        <div className="float_Right">
                                            {/* <button type="button" className="addCommentBtn m-rgt-35" onClick={(e) => this.handleModal(e)}>Invoice Upload & Chat</button> */}
                                            <button type="button" className="discardBtns m-rgt-10 imgFocus" onClick={this.props.CancelShipment}>Cancel</button>
                                            {/* {this.state.poDetailsShipment.isInvoiceUploaded == "TRUE" ? <button type="button" className="confirmBtnBlue" onClick={this.confirmShipment}>Confirm</button>
                                                : <button type="button" className="confirmBtnBlue btnDisabled">Confirm</button>} */}
                                            <button className="confirmBtnBlue" onClick={() => this.onSubmit()}>Confirm</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="vendor-ship-asn-aprv">
                                    <div className="vsaa-body">
                                        <div className="responsive-table">
                                            <table className="table modal-table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">PO NUMBER</label>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">ASN No. </label>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">Invoice Number<span className="mandatory">*</span></label>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">Invoice Date<span className="mandatory">*</span></label>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">Invoice Amount<span className="mandatory">*</span></label>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">No of Cartons<span className="mandatory">*</span></label>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div className="vsaa-inner-item">
                                                                 <label className="lbl-bold">Upload Invoice{this.state.invoiceUplaodFlag && <span className="mandatory">*</span>}</label>
                                                            </div>
                                                        </th>
                                                        {this.state.allowPackingList && <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">Packing List</label>
                                                            </div>
                                                        </th>}
                                                        {/* <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">Download Barcode</label>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div className="vsaa-inner-item">
                                                                <label className="lbl-bold">Download QC</label>
                                                            </div>
                                                        </th> */}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.multipleInvoice.map((data) => (<tr>
                                                        <td>
                                                            <div className="vsaa-inner-item">
                                                                <label>{data.orderNumber}</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="vsaa-inner-item">
                                                                <label>{data.shipmentAdviceCode}</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="vsaa-inner-item">
                                                                <input type="text" pattern="^[a-zA-Z0-9\-\/]+$" maxLength="16" value={data.invoiceNo} onChange={(e) => this.handleFields(e, data.shipmentId, "invoiceNo")} />
                                                                {data.invoiceNoErr && <span className="error">Enter Invoice Number</span>}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="vsaa-inner-item">
                                                                <input type="Date" value={data.invoiceDate} placeholder={data.invoiceDate == "" ? "Select Date" : data.invoiceDate} onChange={(e) => this.handleFields(e, data.shipmentId, "invoiceDate")} max={todayDate > this.props.checkedShipConfirmed.validToSelectionDate ? this.props.checkedShipConfirmed.validToSelectionDate : todayDate}/>
                                                                {data.invoiceDateErr && <span className="error">Select Invoice Date</span>}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="vsaa-inner-item">
                                                                <input type="text" value={data.invoiceAmt} onChange={(e) => this.handleFields(e, data.shipmentId, "invoiceAmt")} />
                                                                {data.invoiceAmtErr && <span className="error">Enter Valid Invoice Amount</span>}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="vsaa-inner-item">
                                                                <input type="text" value={data.noCartoon} onChange={(e) => this.handleFields(e, data.shipmentId, "noCartoon")} />
                                                                {data.noCartoonErr && <span className="error">Enter Valid No of Cartoon</span>}
                                                            </div>
                                                        </td>
                                                        <td>
                                                           <div className="vsaa-inner-item">
                                                                <React.Fragment>
                                                                    <div className="caffb-upload-file">
                                                                        <label onClick={(e) => this.openPpsUploadModal(e, data, "INVOICE")}>
                                                                            { data.invoiceArray !== undefined && (data.invoiceArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{data.invoiceArray.length} Files</span>)}
                                                                            <img src={require('../../../../assets/attach.svg')} />
                                                                        </label>
                                                                        <span id="ppsfileLabel"></span>
                                                                        {data.invoiceUploadErr === "TRUE" && <span className="error">Upload invoice</span>}
                                                                    </div>
                                                                </React.Fragment>
                                                            </div>
                                                            {/* <div className="vsaa-inner-item">
                                                                <div className="uploadInvoiceLeft pad-0 heightAuto">
                                                                    <div className="bottomToolTip displayInline">
                                                                        <label className="file width115px displayPointer customInput m-top-5 hoverMe" >
                                                                            <input type="file" id="fileUpload" onChange={(e) => {this.onFileUpload(e, "INVOICE", data), this.handleFields(e, data.shipmentId, "invoiceUpload")}} multiple="multiple" />
                                                                            <span >Choose file</span>
                                                                            <img src={fileIcon} />
                                                                        </label>
                                                                        <div className="fileUploads">
                                                                            {this.state.fileNames.length != 0 && data.id == this.state.expandedID && data.invoiceUploadErr !== "TRUE" && <label>{this.state.fileNames.length}-Files Uploaded</label>}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {data.invoiceUploadErr === "TRUE" && <span className="error">Upload invoice</span>}
                                                            </div>
                                                            <div className="uploadInvoiceRight">
                                                                {data.isInvoiceUploaded == "TRUE" ? <button type="button" className="removeAfter eye-icon-new" data-toggle="dropdown" data-name="invoice" onClick={(e) => this.dowloadInvoice(e, data, "INVOICE")}><span className="tooltip-shipment-asn">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                        <g id="prefix__eye" transform="translate(-4.736 -19.47)">
                                                                            <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                            <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#000" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                        </g>
                                                                    </svg>
                                                                    <span className="tooltip-content-shipment">View Attachment</span></span></button>
                                                                    : <button type="button" className="removeAfter eye-icon-new pointerNone" data-toggle="dropdown" ><span className="tooltip-shipment-asn">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                            <g id="prefix__eye" transform="translate(-4.736 -19.47)">
                                                                                <path id="prefix__Path_416" fill="#ccd8ed" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                                <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#ccd8ed" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                            </g>
                                                                        </svg>
                                                                        <span className="tooltip-content-shipment">View Attachment</span></span></button>}
                                                                {this.state.showInvoice && data.id == this.state.expandedID && <div className="filesDownloadDrop">
                                                                    <div className="fileUploads">
                                                                        {this.state.invoice.length != 0 ? this.state.invoice.map((hdata, key) => (<React.Fragment><div className="asn-uploaded-file"><div className="asn-up-file-name"><p key={key} onClick={() => window.open(hdata.fileURL)}>{hdata.fileName}</p></div><span className="displayPointer" onClick={(e) => this.deleteUploads(hdata, data, "INVOICE")}>&#10005;</span></div></React.Fragment>))
                                                                            : <label>No Data Found</label>}
                                                                    </div>
                                                                </div>}
                                                            </div> */}
                                                        </td>
                                                        {/* Code for packaging  */}
                                                        {this.state.allowPackingList && ((data.isAlreadyUploadedPacking === "FALSE" || data.isPackagingUploaded == null) ?
                                                        <td>
                                                            <div className="vsaa-inner-item">
                                                                <React.Fragment>
                                                                    <div className="caffb-upload-file">
                                                                        <label onClick={(e) => this.openPpsUploadModal(e, data, "PACKAGING")}>
                                                                            { data.packingArray !== undefined && (data.packingArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{data.packingArray.length} Files</span>)}
                                                                            <img src={require('../../../../assets/attach.svg')} />
                                                                        </label>
                                                                        <span id="ppsfileLabel"></span>
                                                                    </div>
                                                                </React.Fragment>
                                                            </div>
                                                            {/* <div className="vsaa-inner-item">
                                                                <div className="uploadInvoiceLeft pad-0 heightAuto">
                                                                    <div className="bottomToolTip displayInline">
                                                                        <label className="file width115px displayPointer customInput m-top-5 hoverMe" >
                                                                            <input type="file" id="fileUpload" multiple="multiple" onChange={(e) => this.onFileUpload(e, "PACKAGING", data)} />
                                                                            <span >Choose file</span>
                                                                            <img src={fileIcon} />
                                                                        </label>
                                                                        <div className="fileUploads">
                                                                            {this.state.packagingFileName.length != 0 && data.id == this.state.expandedID && <label>{this.state.packagingFileName.length}-Files Uploaded</label>}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="uploadInvoiceRight">
                                                                {data.isPackagingUploaded == "TRUE" ? <button type="button" className="removeAfter eye-icon-new" data-toggle="dropdown" data-name="packaging" onClick={(e) => this.dowloadInvoice(e, data, "PACKAGING")}><span className="tooltip-shipment-asn">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                        <g id="prefix__eye" transform="translate(-4.736 -19.47)">
                                                                            <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                            <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#000" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                        </g>
                                                                    </svg>
                                                                    <span className="tooltip-content-shipment">View Attachment</span></span></button>
                                                                    : <button type="button" className="removeAfter eye-icon-new pointerNone" data-toggle="dropdown" ><span className="tooltip-shipment-asn">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                            <g id="prefix__eye" transform="translate(-4.736 -19.47)">
                                                                                <path id="prefix__Path_416" fill="#ccd8ed" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                                <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#ccd8ed" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                            </g>
                                                                        </svg>
                                                                        <span className="tooltip-content-shipment">View Attachment</span></span></button>}
                                                                {this.state.showPackagingInvoice && data.id == this.state.expandedID && <div className="filesDownloadDrop">
                                                                    <div className="fileUploads">
                                                                        {this.state.packagingInvoice.length != 0 ? this.state.packagingInvoice.map((hdata, key) => (<React.Fragment><div className="asn-uploaded-file"><div className="asn-up-file-name"><p key={key} onClick={() => window.open(hdata.fileURL)}>{hdata.fileName}</p></div><span className="displayPointer" onClick={(e) => this.deleteUploads(hdata, data, "PACKAGING")}>&#10005;</span></div></React.Fragment>))
                                                                            : <label>No Data Found</label>}
                                                                    </div>
                                                                </div>}
                                                            </div> */}
                                                        </td>: <td><div className="vsaa-inner-item"><label>Packing Uploaded</label></div></td>)} 
                                                        <td>
                                                            <div className="close-flex">
                                                                <span className="close-btn displayPointer" onClick={(e) => this.onDelete(e, data.shipmentId)}><img src={Close}></img></span>
                                                            </div>
                                                        </td>
                                                    </tr>))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                {this.state.ppsUploadModal && this.state.multipleInvoice.map( item => item.id == this.state.currentAsnId && <CreateAsnUpload CancelPpsUploadModal={this.CancelPpsUploadModal} onFileUploadSubmit={this.onFileUploadSubmit} newInvoiceArray={item.invoiceArray} newPackingArray={item.packingArray} {...this.state} {...this.props} uploadFileType={item.uploadFileType}/>)}
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {qualityModal ? <QualityCheckVendorModal {...this.state} {...this.props} qcUpload={this.state.qcUpload} poId={this.props.poId} handleModal={(e) => this.handleModal(e)} shipmentId={this.props.shipmentId} poDetailsShipment={this.state.poDetailsShipment} /> : null}
            </div >
        )
    }
}
