import React from "react";
class UpdateAsnUpload extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fileArray:[],
            oldFileArray:[]
        }
    }
    onMmltipleFileUpload = (evt,subModule) =>{
            var files = Object.values(evt.target.files); // FileList object
            this.setState({
                fileArray:this.state.fileArray.concat(files).reverse(),
            })
    }
    componentDidMount(){
        const {uploadFileType}=this.props
        this.setState({
            fileArray: uploadFileType == "PPS" ? this.props.newPPSArray : uploadFileType == "REPORT" ? this.props.newReportArray : this.props.newPackingArray,
            oldFileArray: uploadFileType == "PPS" ? this.props.oldPPSArray : uploadFileType == "REPORT" ? this.props.oldReportArray : this.props.oldPackingArray
        })
    }
    removeFiles(index){
        const fileArray = this.state.fileArray
        fileArray.splice(index,1)
        this.setState({
            fileArray
        })
    }
    componentWillReceiveProps(nextProps){
        const {uploadFileType}=this.props
        if (nextProps.shipment.deleteUploads.isSuccess) {
            this.setState({
                oldFileArray: uploadFileType == "PPS" ? nextProps.oldPpsFiles : uploadFileType == "REPORT" ? nextProps.oldReportFiles : nextProps.oldPackingFiles
            })
        }
    }

    removeOldFiles =(index)=>{
        const oldFileArray = this.state.oldFileArray
        oldFileArray.splice(index,1)
        this.setState({
            oldFileArray
        })
    }

    render() {
        const {uploadFileType,PPSArray}=this.props
        const {fileArray,oldFileArray}=this.state
        return(
            <div className="modal">
                <div className="backdrop-transparent"></div>
                <div className="modal-content create-asn-upload-modal">
                    <div className="caum-head">
                        <h3>{uploadFileType == "PPS" ? "Upload PPS " : uploadFileType == "REPORT" ? "Upload Test Report" : "Upload Packing List"}</h3>
                        <div className="caumh-btns">
                            <button type="button" onClick={(e) =>this.props.CancelPpsUploadModal(e, uploadFileType)}>Cancel</button>
                            <button type="button" className="caumhb-done" onClick={()=> this.props.onFileUploadSubmit(fileArray,uploadFileType)} >Done</button>
                        </div>
                    </div>
                    <div className="caum-body">
                        <div className="caumb-upload-file">
                            <label>
                                <input type="file" id="FileUpload" multiple="multiple" onClick={(e) => e.currentTarget.value = null }  onChange={(e) => this.onMmltipleFileUpload(e,uploadFileType)}/>
                                <span>{uploadFileType == "PPS" ? "Choose PPS " : uploadFileType == "REPORT" ? "Choose Test Report" : "Choose Packing List"}</span>
                                <img src={require('../../../../assets/upload.svg')} />
                            </label>
                            <span id="FileLabel"></span>
                        </div>
                        <div className="caumb-file">
                            {
                                fileArray.length != 0 ?
                                fileArray.map((item,index)=>{
                                    return(
                                        <div className="caumbf-inner" key={index}>
                                            <p>{item.name}</p>
                                            <span className="caumbf-remove" onClick={()=>this.removeFiles(index)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" id="close_6_" width="19.771" height="19.77" data-name="close (6)" viewBox="0 0 19.771 19.77">
                                                    <g id="Group_3015" data-name="Group 3015">
                                                        <path fill="#21314b" id="Path_926" d="M9.886 19.781A9.885 9.885 0 0 1 2.892 2.9a9.891 9.891 0 1 1 6.994 16.881zm-5.91-15.8a8.359 8.359 0 1 0 11.821 0 8.37 8.37 0 0 0-11.821 0z" class="cls-1" data-name="Path 926" transform="translate(0 -.011)"/>
                                                    </g>
                                                    <g id="Group_3016" data-name="Group 3016" transform="translate(6.459 6.361)">
                                                        <path fill="#21314b" id="Path_927" d="M168.038 171.684a.766.766 0 0 1-.542-1.308l5.416-5.416a.766.766 0 0 1 1.088 1.083l-5.416 5.416a.765.765 0 0 1-.546.225z" class="cls-1" data-name="Path 927" transform="translate(-167.27 -164.735)"/>
                                                        <path fill="#21314b" id="Path_928" d="M173.434 171.684a.764.764 0 0 1-.542-.224l-5.416-5.416a.766.766 0 0 1 1.083-1.083l5.416 5.416a.766.766 0 0 1-.542 1.308z" class="cls-1" data-name="Path 928" transform="translate(-167.251 -164.735)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                    )
                                })
                                :null
                            }
                            {
                                oldFileArray.length != 0 ?
                                oldFileArray.map((item,index)=>{
                                    return(
                                        <div className="caumbf-inner" onClick={()=>{this.props.deleteUploads(item,uploadFileType); this.removeOldFiles(index)}}>
                                            <p>{item.fileName}</p>
                                            <span className="caumbf-remove">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="close_6_" width="19.771" height="19.77" data-name="close (6)" viewBox="0 0 19.771 19.77">
                                                    <g id="Group_3015" data-name="Group 3015">
                                                        <path fill="#21314b" id="Path_926" d="M9.886 19.781A9.885 9.885 0 0 1 2.892 2.9a9.891 9.891 0 1 1 6.994 16.881zm-5.91-15.8a8.359 8.359 0 1 0 11.821 0 8.37 8.37 0 0 0-11.821 0z" class="cls-1" data-name="Path 926" transform="translate(0 -.011)"/>
                                                    </g>
                                                    <g id="Group_3016" data-name="Group 3016" transform="translate(6.459 6.361)">
                                                        <path fill="#21314b" id="Path_927" d="M168.038 171.684a.766.766 0 0 1-.542-1.308l5.416-5.416a.766.766 0 0 1 1.088 1.083l-5.416 5.416a.765.765 0 0 1-.546.225z" class="cls-1" data-name="Path 927" transform="translate(-167.27 -164.735)"/>
                                                        <path fill="#21314b" id="Path_928" d="M173.434 171.684a.764.764 0 0 1-.542-.224l-5.416-5.416a.766.766 0 0 1 1.083-1.083l5.416 5.416a.766.766 0 0 1-.542 1.308z" class="cls-1" data-name="Path 928" transform="translate(-167.251 -164.735)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div> 
                                    )
                                })
                                :null
                            }
                            {/* <div className="caumbf-inner">
                                <p>Test_Report_2019.xls</p>
                                <span className="caumbf-remove">
                                    <svg xmlns="http://www.w3.org/2000/svg" id="close_6_" width="19.771" height="19.77" data-name="close (6)" viewBox="0 0 19.771 19.77">
                                        <g id="Group_3015" data-name="Group 3015">
                                            <path fill="#21314b" id="Path_926" d="M9.886 19.781A9.885 9.885 0 0 1 2.892 2.9a9.891 9.891 0 1 1 6.994 16.881zm-5.91-15.8a8.359 8.359 0 1 0 11.821 0 8.37 8.37 0 0 0-11.821 0z" class="cls-1" data-name="Path 926" transform="translate(0 -.011)"/>
                                        </g>
                                        <g id="Group_3016" data-name="Group 3016" transform="translate(6.459 6.361)">
                                            <path fill="#21314b" id="Path_927" d="M168.038 171.684a.766.766 0 0 1-.542-1.308l5.416-5.416a.766.766 0 0 1 1.088 1.083l-5.416 5.416a.765.765 0 0 1-.546.225z" class="cls-1" data-name="Path 927" transform="translate(-167.27 -164.735)"/>
                                            <path fill="#21314b" id="Path_928" d="M173.434 171.684a.764.764 0 0 1-.542-.224l-5.416-5.416a.766.766 0 0 1 1.083-1.083l5.416 5.416a.766.766 0 0 1-.542 1.308z" class="cls-1" data-name="Path 928" transform="translate(-167.251 -164.735)"/>
                                        </g>
                                    </svg>
                                </span>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default UpdateAsnUpload;