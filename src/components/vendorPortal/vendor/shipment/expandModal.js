import React, { Component } from 'react';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';
import plusIcon from '../../../../assets/plus-blue.svg';
import searchIcon from '../../../../assets/close-recently.svg';

import SetLevelConfigHeader from '../../setLevelConfigHeader'
import SetLevelConfirmationModal from '../../setLevelConfirModal'
import ItemLevelDetails from '../../modals/itemLevelDetails';
import moment, { relativeTimeThreshold } from 'moment';
import removeIcon from '../../../../assets/removeIcon.svg';
import LocationModal from './../../vendor/orders/locationModal';
import AddressModal from './../../vendor/orders/addressModal';
import FilterLoader from '../../../loaders/filterLoader';
import { CONFIG } from "../../../../config/index";
import axios from 'axios';
import clearSearch from '../../../../assets/clearSearch.svg';
import UpdateAsnUpload from './updateAsnUpload';
const todayDate = moment(new Date()).format("YYYY-MM-DD");
export default class ExpandModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            poDetailsShipment: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            toastLoader: false,
            toastMsg: "",
            dropOpen: true,
            expandedId: "",
            itemBaseArray: [],
            expandedLineItems: {},
            actionExpand: false,
            setLvlGetHeaderConfig: [],
            setLvlFixedHeader: [],
            setLvlCustomHeaders: {},
            setLvlHeaderConfigState: {},
            setLvlHeaderConfigDataState: {},
            setLvlFixedHeaderData: [],
            setLvlCustomHeadersState: [],
            setLvlHeaderState: {},
            setLvlHeaderSummary: [],
            setLvlDefaultHeaderMap: [],
            setLvlConfirmModal: false,
            setLvlHeaderMsg: "",
            setLvlParaMsg: "",
            setLvlHeaderCondition: false,
            setLvlSaveState: [],
            setLvlColoumSetting: false,

            setBaseExpandDetails: [],
            setRequestedQty: "",
            setBasedArray: [],
            shipmentDetails: "",
            qcFromDate: "",
            qcFromDateErr: "",
            qcToDate: "",
            qcToDateErr: "",
            dateerr: "",
            remarks: "",
            qcFromFormatted: "",
            formattedDate: "",
            saveShow: false,
            shipmentRequestSelectionDate: "",
            validToDateSelect: "",
            validFromDateSelect: "",
            poType: this.props.poType,
            editAsn: false,
            mandateHeaderSet : [],
            maxQCToDate: todayDate,
            isQcOn: this.props.isQcOn,
            status: "",
            selectAddress: false,
            selectLocation: false,
            contactName:"",
            contactNameErr:false,
            contactName:"",
            contactNameErr:false,
            contactNumber:"",
            contactNumberErr:false,
            location:"",
            locationErr:false,
            address:"",
            addressErr:false,
            addressModal: false,
            isModalShow:false,
            locationModal: false,
            AddressModalAnimation:false,
            PPSArray:[],
            ReportArray:[],
            addressSearch: "",
            isPPSAvailable:true,
            isTestReportAvailable:false,
            ppsFilesErrors:false,
            reportFilesErrors:false,
            oldReportFiles:[],
            oldPpsFiles:[],
            ppsUploadModal: false,
            uploadFileType:"",

            calculatedPercent: 100,
            noOfPercentFulfil: this.props.noOfPercentFulfil,
            createAsnFromToValidity: this.props.createAsnFromToValidity,
            asnBufferDays: this.props.asnBufferDays,
            isAsnMask: this.props.isAsnMask,

            packingArray:[],
            oldPackingFiles: [],
            packingFilesErrors: false,
            isPackingAvailable: false,

            isMultipleAsnAllow: this.props.isMultipleAsnAllow,
            addressCity: "",
            baseLocation: "",

            cancelEditAsn: false,
            toBeDeletedFilesPayload: [],
            cancelLinePendingQty: this.props.cancelLinePendingQty,
            allowPackingList: this.props.allowPackingList,
            allowMultipleAsn: this.props.allowMultipleAsn,
            allowAsnBufferDays: this.props.allowAsnBufferDays,
            allowNoOfPercentFulfil: this.props.allowNoOfPercentFulfil,
        }
    }
    openPpsUploadModal(e,fileType) {
        e.preventDefault();
        this.setState({
            uploadFileType:fileType,
            ppsUploadModal: !this.state.ppsUploadModal
        });
    }
    CancelPpsUploadModal = (e, subModule) => {
        this.setState({
            ppsUploadModal: false,
        })
        let payload = {
            shipmentId: this.props.shipment.getShipmentDetails.data.resource.shipmentId,
            module: "SHIPMENT",
            pageNo: 1,
            orderNumber: this.props.shipment.getShipmentDetails.data.resource.orderNumber,
            isAllAttachment: "TRUE",
            subModule: subModule,
            refresh: "whole",
            orderId: this.props.shipment.getShipmentDetails.data.resource.orderId,
            documentNumber: this.props.shipment.getShipmentDetails.data.resource.documentNumber
        }
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.post(`${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/find/all/comqcinvoice`, payload, { headers: headers })
        .then(res => {
            if( subModule == "PPS")
              this.setState({ oldPpsFiles: res.data.data.resource.PPS})
            if( subModule == "REPORT")
              this.setState({ oldReportFiles: res.data.data.resource.REPORT})
            if( subModule == "PACKAGING")
              this.setState({ oldPackingFiles: res.data.data.resource.INVOICE})

        }).catch((error) => {
            this.setState({
                toastError: true
            })
        })
    }
    addressData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectAddress: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectAddress: false })
        }
    }
    locationData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectLocation: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectLocation: false })
        }
    }
    componentDidMount() {
        if(this.props.poType == "poicode"){
            
                let payload = {
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                    attributeType: "TABLE HEADER",
                    displayName:  this.props.item_Display,
                    basedOn: "SET"
                }
                this.props.getSetHeaderConfigRequest(payload)    
        }else{
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName:   this.props.set_Display,
                basedOn: "SET"
            }
            this.props.getSetHeaderConfigRequest(payload)
        }
        //this.props.getSetHeaderConfigRequest(this.props.setHeaderPayload)
        //this.props.getStatusButtonActiveRequest('asd');
        this.props.onExpandRef(this);
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }

    componentWillUnmount(){
        this.props.onExpandRef(undefined);
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }

    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent"))){
          this.setState({ addressModal: false, locationModal: false, })
        }
    }

    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "SET") {
            this.props.getSetHeaderConfigRequest(this.props.setHeaderPayload)
        }
        // if (this.props.replenishment.inspectionOnOff.isSuccess && this.props.replenishment.inspectionOnOff.data.resource != null) {
        //     if (this.props.replenishment.inspectionOnOff.data.qc != undefined ) {
        //          this.setState({
        //             isQcOn: this.props.replenishment.inspectionOnOff.data.qc,  
        //          })
        //     }
        //     this.props.getInspOnOffConfigClear()
        // }

        // if (this.props.logistic.getButtonActiveConfig.isSuccess && this.props.logistic.getButtonActiveConfig.data.resource != null) {
        //     if( this.props.logistic.getButtonActiveConfig.data.resource.inspectionRequired != undefined ){ 
        //         this.setState({ 
        //             isQcOn: this.props.logistic.getButtonActiveConfig.data.resource.inspectionRequired,
        //             noOfPercentFulfil: this.props.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != undefined && this.props.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != null ? Number(this.props.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment) : 0,
        //             asnBufferDays: this.props.logistic.getButtonActiveConfig.data.resource.asnBufferDays != undefined && this.props.logistic.getButtonActiveConfig.data.resource.asnBufferDays != null ? Number(this.props.logistic.getButtonActiveConfig.data.resource.asnBufferDays) : 0, 
        //             createAsnFromToValidity: this.props.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablility == "TRUE" ? true : false ,
        //             isAsnMask: this.props.logistic.getButtonActiveConfig.data.resource.asnMask == "TRUE" ? true : false ,
        //             isMultipleAsnAllow: this.props.logistic.getButtonActiveConfig.data.resource.isPartialPORequired == "TRUE" ? true : false , 
        //         },()=>{
        //             if( this.state.isQcOn == "FALSE")
        //             this.props.getInspOnOffConfigRequest('asd');
        //         })    
        //     }
        //     this.props.getStatusButtonActiveClear()
        // }
        if (this.props.shipment.getShipmentDetails.isSuccess && this.state.actionExpand == false) {
            if (this.props.shipment.getShipmentDetails.data.resource != null) {
                if(this.props.shipment.getShipmentDetails.data.resource.isTestReportAvailable =="TRUE" ||
                this.props.shipment.getShipmentDetails.data.resource.isTestReportAvailable =="true"){
                    let payload = {
                        shipmentId: this.props.shipment.getShipmentDetails.data.resource.shipmentId,
                        module: "SHIPMENT",
                        pageNo: 1,
                        // orderCode: this.props.shipment.getShipmentDetails.data.resource.orderCode,
                        orderNumber: this.props.shipment.getShipmentDetails.data.resource.orderNumber,
                        isAllAttachment: "TRUE",
                        subModule: "REPORT",
                        refresh: "whole",
                        orderId: this.props.shipment.getShipmentDetails.data.resource.orderId,
                        documentNumber: this.props.shipment.getShipmentDetails.data.resource.documentNumber,
                        commentId: this.props.shipment.getShipmentDetails.data.resource.shipmentId,
                        commentCode: this.props.shipment.getShipmentDetails.data.resource.shipmentAdviceCode
                    }
                    let headers = {
                        'X-Auth-Token': sessionStorage.getItem('token'),
                        'Content-Type': 'application/json'
                    }
                    axios.post(`${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/find/all/comqcinvoice`, payload, { headers: headers })
                    .then(res => {
                        this.setState({
                            oldReportFiles: res.data.data.resource.REPORT
                        })
                    }).catch((error) => {
                        this.setState({
                            toastError: true
                        })
                    })
                   // this.props.getAllCommentRequest(payload)
                }if(this.props.shipment.getShipmentDetails.data.resource.isPPSAvailable =="TRUE" ||
                this.props.shipment.getShipmentDetails.data.resource.isPPSAvailable =="true"){
                    let payload = {
                        shipmentId: this.props.shipment.getShipmentDetails.data.resource.shipmentId,
                        module: "SHIPMENT",
                        pageNo: 1,
                        // orderCode: this.props.shipment.getShipmentDetails.data.resource.orderCode,
                        orderNumber: this.props.shipment.getShipmentDetails.data.resource.orderNumber,
                        isAllAttachment: "TRUE",
                        subModule: "PPS",
                        refresh: "whole",
                        orderId: this.props.shipment.getShipmentDetails.data.resource.orderId,
                        documentNumber: this.props.shipment.getShipmentDetails.data.resource.documentNumber,
                        commentId: this.props.shipment.getShipmentDetails.data.resource.shipmentId,
                        commentCode: this.props.shipment.getShipmentDetails.data.resource.shipmentAdviceCode
                    }
                    let headers = {
                        'X-Auth-Token': sessionStorage.getItem('token'),
                        'Content-Type': 'application/json'
                    }
                    axios.post(`${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/find/all/comqcinvoice`, payload, { headers: headers })
                    .then(res => {
                        this.setState({
                            oldPpsFiles: res.data.data.resource.PPS
                        })
                    }).catch((error) => {
                        this.setState({
                            toastError: true
                        })
                    })
                    
                }
                if(this.props.expandedPo.isPackagingUploaded =="TRUE" ||
                    this.props.expandedPo.isPackagingUploaded =="true"){
                    let payload = {
                        shipmentId: this.props.shipment.getShipmentDetails.data.resource.shipmentId,
                        module: "SHIPMENT",
                        pageNo: 1,
                        orderNumber: this.props.shipment.getShipmentDetails.data.resource.orderNumber,
                        isAllAttachment: "TRUE",
                        subModule: "PACKAGING",
                        refresh: "whole",
                        orderId: this.props.shipment.getShipmentDetails.data.resource.orderId,
                        documentNumber: this.props.shipment.getShipmentDetails.data.resource.documentNumber,
                        commentId: this.props.shipment.getShipmentDetails.data.resource.shipmentId,
                        commentCode: this.props.shipment.getShipmentDetails.data.resource.shipmentAdviceCode
                    }
                    let headers = {
                        'X-Auth-Token': sessionStorage.getItem('token'),
                        'Content-Type': 'application/json'
                    }
                    axios.post(`${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/find/all/comqcinvoice`, payload, { headers: headers })
                    .then(res => {
                        this.setState({
                            oldPackingFiles: res.data.data.resource.INVOICE
                        })
                    }).catch((error) => {
                        this.setState({
                            toastError: true
                        })
                    })   
                }
            }
        }
        if (this.props.shipment.deleteUploads.isSuccess) {
            this.setState({
                isPPSAvailable: this.props.shipment.deleteUploads.data.isPPSAvailable == "TRUE" ? true : false,
                isTestReportAvailable: this.props.shipment.deleteUploads.data.isTestReportAvailable == "TRUE" ? true : false,
                isPackingAvailable: this.props.shipment.deleteUploads.data.isPackagingAvailable == "TRUE" ? true : false,
            },()=> this.fileFun())
            this.props.deleteUploadsClear()
        }

        if (this.props.shipment.getShipmentDetails.isSuccess && this.state.cancelEditAsn) {
            var poDetails = this.props.shipment.getShipmentDetails.data.resource == null ? [] : this.props.shipment.getShipmentDetails.data.resource
            poDetails.length == 0 ? [] : poDetails.poDetails.map((data) => {
                data.cancelledQty = data.shipmentCancelledQty, data.pendingQty = data.shipmentPendingQty, data.requestedQty = data.shipmentRequestedQty, data.error = false
            })

            let itemBaseArray = this.props.shipment.getShipmentDetails.data.resource.poDetails
                itemBaseArray.map((data) => (data.totalPendingQty = 0))
            let itemDetails = this.props.shipment.getShipmentDetails.data.resource.poDetails
            let id = this.props.expandedId;

            if (this.props.shipment.getShipmentDetails.data.resource != null) {
                this.setState({
                    poDetailsShipment: this.props.shipment.getShipmentDetails.data.resource,
                    prev: this.props.shipment.getShipmentDetails.data.prePage,
                    current: this.props.shipment.getShipmentDetails.data.currPage,
                    next: this.props.shipment.getShipmentDetails.data.currPage + 1,
                    maxPage: this.props.shipment.getShipmentDetails.data.maxPage,
                    setBasedArray: poDetails.poDetails,
                    formattedDate: moment(this.props.shipment.getShipmentDetails.data.resource.shipmentRequestSelectionDate, "YYYY-MM-DD").format("DD-MM-YYYY"),
                    shipmentRequestedDate: this.props.shipment.getShipmentDetails.data.resource.shipmentRequestSelectionDate !== null ? this.props.shipment.getShipmentDetails.data.resource.shipmentRequestSelectionDate : todayDate,
                    validFromDateSelect: this.props.shipment.getShipmentDetails.data.resource.validFromSelectionDate !== null ? this.props.shipment.getShipmentDetails.data.resource.validFromSelectionDate : todayDate,
                    validToDateSelect: this.props.shipment.getShipmentDetails.data.resource.validToSelectionDate !== null ? this.props.shipment.getShipmentDetails.data.resource.validToSelectionDate : todayDate,
                    qcFromFormatted: this.props.shipment.getShipmentDetails.data.resource.qcFromDateSelection == null ? todayDate : moment(this.props.shipment.getShipmentDetails.data.resource.qcFromDateSelection, "YYYY-MM-DD").format("DD-MM-YYYY"),
                    qcToFormatted: this.props.shipment.getShipmentDetails.data.resource.qcToDateSelection == null ? todayDate : moment(this.props.shipment.getShipmentDetails.data.resource.qcToDateSelection, "YYYY-MM-DD").format("DD-MM-YYYY"),
                    remarks: this.props.shipment.getShipmentDetails.data.resource.remarks,
                    qcFromDate: this.props.shipment.getShipmentDetails.data.resource.qcFromDateSelection !== null ? this.props.shipment.getShipmentDetails.data.resource.qcFromDateSelection : todayDate,
                    qcToDate: this.props.shipment.getShipmentDetails.data.resource.qcToDateSelection !== null ? this.props.shipment.getShipmentDetails.data.resource.qcToDateSelection : todayDate,
                    status: this.props.shipment.getShipmentDetails.data.resource.status,
                    contactName:this.props.shipment.getShipmentDetails.data.resource.contactPerson !== null ? this.props.shipment.getShipmentDetails.data.resource.contactPerson : "",
                    contactNumber:this.props.shipment.getShipmentDetails.data.resource.contactNo !== null ? this.props.shipment.getShipmentDetails.data.resource.contactNo : "",
                    address:this.props.shipment.getShipmentDetails.data.resource.address !== null ? this.props.shipment.getShipmentDetails.data.resource.address : "",
                    location:this.props.shipment.getShipmentDetails.data.resource.location !== null ? this.props.shipment.getShipmentDetails.data.resource.location : "",
                    isPPSAvailable:this.props.shipment.getShipmentDetails.data.resource.isPPSAvailable =="true" ||this.props.shipment.getShipmentDetails.data.resource.isPPSAvailable =="TRUE" ? true : false,
                    isTestReportAvailable:this.props.shipment.getShipmentDetails.data.resource.isTestReportAvailable =="true" || this.props.shipment.getShipmentDetails.data.resource.isTestReportAvailable =="TRUE" ? true : false,
                    isPackingAvailable: this.props.shipment.getShipmentDetails.data.resource.isPackagingUploaded =="true" || this.props.shipment.getShipmentDetails.data.resource.isPackagingUploaded =="TRUE" ? true : false,

                    maxQCToDate : moment(this.props.shipment.getShipmentDetails.data.resource.qcFromDateSelection !== null ? this.props.shipment.getShipmentDetails.data.resource.qcFromDateSelection : todayDate).add(2,'days').format("YYYY-MM-DD"),
                    addressCity: this.props.shipment.getShipmentDetails.data.resource.address,
                    baseLocation: this.props.shipment.getShipmentDetails.data.resource.location,

                    expandedLineItems: { ...this.props.expandedLineItems, [id]: itemDetails },
                    itemBaseArray: itemBaseArray,

                    cancelEditAsn: false,
                    
                })  
            } 
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.shipment.getShipmentDetails.isSuccess && prevState.actionExpand == false) {
            var poDetails = nextProps.shipment.getShipmentDetails.data.resource == null ? [] : nextProps.shipment.getShipmentDetails.data.resource
            poDetails.length == 0 ? [] : poDetails.poDetails.map((data) => {
                data.cancelledQty = data.shipmentCancelledQty, data.pendingQty = data.shipmentPendingQty, data.requestedQty = data.shipmentRequestedQty, data.error = false
            })
            if (nextProps.shipment.getShipmentDetails.data.resource != null) {
                return{
                    poDetailsShipment: nextProps.shipment.getShipmentDetails.data.resource,
                    prev: nextProps.shipment.getShipmentDetails.data.prePage,
                    current: nextProps.shipment.getShipmentDetails.data.currPage,
                    next: nextProps.shipment.getShipmentDetails.data.currPage + 1,
                    maxPage: nextProps.shipment.getShipmentDetails.data.maxPage,
                    setBasedArray: poDetails.poDetails,
                    formattedDate: moment(nextProps.shipment.getShipmentDetails.data.resource.shipmentRequestSelectionDate, "YYYY-MM-DD").format("DD-MM-YYYY"),
                    shipmentRequestedDate: nextProps.shipment.getShipmentDetails.data.resource.shipmentRequestSelectionDate !== null ? nextProps.shipment.getShipmentDetails.data.resource.shipmentRequestSelectionDate : todayDate,
                    validFromDateSelect: nextProps.shipment.getShipmentDetails.data.resource.validFromSelectionDate !== null ? nextProps.shipment.getShipmentDetails.data.resource.validFromSelectionDate : todayDate,
                    validToDateSelect: nextProps.shipment.getShipmentDetails.data.resource.validToSelectionDate !== null ? nextProps.shipment.getShipmentDetails.data.resource.validToSelectionDate : todayDate,
                    qcFromFormatted: nextProps.shipment.getShipmentDetails.data.resource.qcFromDateSelection == null ? todayDate : moment(nextProps.shipment.getShipmentDetails.data.resource.qcFromDateSelection, "YYYY-MM-DD").format("DD-MM-YYYY"),
                    qcToFormatted: nextProps.shipment.getShipmentDetails.data.resource.qcToDateSelection == null ? todayDate : moment(nextProps.shipment.getShipmentDetails.data.resource.qcToDateSelection, "YYYY-MM-DD").format("DD-MM-YYYY"),
                    remarks: nextProps.shipment.getShipmentDetails.data.resource.remarks,
                    qcFromDate: nextProps.shipment.getShipmentDetails.data.resource.qcFromDateSelection !== null ? nextProps.shipment.getShipmentDetails.data.resource.qcFromDateSelection : todayDate,
                    qcToDate: nextProps.shipment.getShipmentDetails.data.resource.qcToDateSelection !== null ? nextProps.shipment.getShipmentDetails.data.resource.qcToDateSelection : todayDate,
                    status: nextProps.shipment.getShipmentDetails.data.resource.status,
                    contactName:nextProps.shipment.getShipmentDetails.data.resource.contactPerson !== null ? nextProps.shipment.getShipmentDetails.data.resource.contactPerson : "",
                    contactNumber:nextProps.shipment.getShipmentDetails.data.resource.contactNo !== null ? nextProps.shipment.getShipmentDetails.data.resource.contactNo : "",
                    address:nextProps.shipment.getShipmentDetails.data.resource.address !== null ? nextProps.shipment.getShipmentDetails.data.resource.address : "",
                    location:nextProps.shipment.getShipmentDetails.data.resource.location !== null ? nextProps.shipment.getShipmentDetails.data.resource.location : "",
                    isPPSAvailable:nextProps.shipment.getShipmentDetails.data.resource.isPPSAvailable =="true" ||nextProps.shipment.getShipmentDetails.data.resource.isPPSAvailable =="TRUE" ? true : false,
                    isTestReportAvailable:nextProps.shipment.getShipmentDetails.data.resource.isTestReportAvailable =="true" || nextProps.shipment.getShipmentDetails.data.resource.isTestReportAvailable =="TRUE" ? true : false,
                    isPackingAvailable: nextProps.shipment.getShipmentDetails.data.resource.isPackagingUploaded =="true" || nextProps.shipment.getShipmentDetails.data.resource.isPackagingUploaded =="TRUE" ? true : false,

                    maxQCToDate : moment(nextProps.shipment.getShipmentDetails.data.resource.qcFromDateSelection !== null ? nextProps.shipment.getShipmentDetails.data.resource.qcFromDateSelection : todayDate).add(2,'days').format("YYYY-MM-DD"),
                    addressCity: nextProps.shipment.getShipmentDetails.data.resource.address,
                    baseLocation: nextProps.shipment.getShipmentDetails.data.resource.location,
                    
                }
                
            } else {    
                return {
                    shipmentDetails: nextProps.shipment.getShipmentDetails.data.resource,
                    poDetailsShipment: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                }
            }
        }
        if (prevState.actionExpand && nextProps.shipment.getShipmentDetails.isSuccess) {
            let itemBaseArray = nextProps.shipment.getShipmentDetails.data.resource.poDetails
            itemBaseArray.map((data) => (data.totalPendingQty = 0))

            let itemDetails = nextProps.shipment.getShipmentDetails.data.resource.poDetails
            let id = prevState.expandedId;
            return {
                expandedLineItems: { ...prevState.expandedLineItems, [id]: itemDetails },
                itemBaseArray,
            }

        }
        // if (nextProps.replenishment.inspectionOnOff.isSuccess) {
        //     if (nextProps.replenishment.inspectionOnOff.data.qc != null && nextProps.replenishment.inspectionOnOff.data.qc != "") {
        //         return {
        //             isQcOn: nextProps.replenishment.inspectionOnOff.data.qc,  
        //          }
        //     }
        // }
        // if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) {
        //     if( nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired != undefined ){
        //         return{
        //             isQcOn: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired,
        //             noOfPercentFulfil: nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment) : 0,
        //             asnBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays) : 0, 
        //             createAsnFromToValidity: nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablility == "TRUE" ? true : false ,
        //             isAsnMask: nextProps.logistic.getButtonActiveConfig.data.resource.asnMask == "TRUE" ? true : false ,   
        //         }
        //     }
        // }
        return null;
    }
    expandColumn(id, e, data) {
        var img = e.target
        if (this.state.expandedLineItems.hasOwnProperty(id)) {
            let arr = [];
            arr = this.state.expandedLineItems[id]
            this.setState({
                itemBaseArray: arr,
                expandedId: id,
                dropOpen: !this.state.dropOpen
            })
        }
        else {
            if (!this.state.actionExpand || this.state.prevId !== e.target.id) {
                let payload = {
                    orderId: this.props.orderId,
                    setHeaderId: data.setHeaderId,
                    detailType: "item",
                    poType: this.props.poType,
                    shipmentStatus: this.props.moduleStatus,
                    shipmentId: this.props.shipmentId
                }
                this.props.getShipmentDetailsRequest(payload)
                this.props.getItemHeaderConfigRequest(this.props.itemHeaderPayload)
                this.setState({ actionExpand: true, prevId: e.target.id, expandedId: id, dropOpen: true, order: { orderCode: data.orderCode, orderNumber: data.orderNumber } },
                )
            }
            else {
                this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
            }
        }
    }
    setLvlOpenColoumSetting(data) {
        if (this.state.setLvlCustomHeadersState.length == 0) {
            this.setState({
                setLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                setLvlColoumSetting: true
            })
        } else {
            this.setState({
                setLvlColoumSetting: false
            })
        }
    }
    setLvlpushColumnData(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlHeaderConfigDataState = this.state.setLvlHeaderConfigDataState
        let setLvlCustomHeaders = this.state.setLvlCustomHeaders
        let setLvlSaveState = this.state.setLvlSaveState
        let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
        let setLvlHeaderSummary = this.state.setLvlHeaderSummary
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (this.state.setLvlHeaderCondition) {

            if (!data.includes(setLvlGetHeaderConfig) || setLvlGetHeaderConfig.length == 0) {
                setLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(setLvlHeaderConfigDataState))) {

                    let invert = _.invert(setLvlFixedHeaderData)

                    let keyget = invert[data];

                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }

                if (!Object.keys(setLvlCustomHeaders).includes(setLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];


                    setLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {

            if (!data.includes(setLvlCustomHeadersState) || setLvlCustomHeadersState.length == 0) {

                setLvlCustomHeadersState.push(data)

                if (!setLvlCustomHeadersState.includes(setLvlHeaderConfigDataState)) {

                    let keyget = (_.invert(setLvlFixedHeaderData))[data];


                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)

                }

                if (!Object.keys(setLvlCustomHeaders).includes(setLvlHeaderSummary)) {

                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];

                    setLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeadersState,
            setLvlCustomHeaders,
            setLvlSaveState,
            setLvlDefaultHeaderMap,
            setLvlHeaderSummary
        })
    }
    setLvlCloseColumn(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlHeaderConfigState = this.state.setLvlHeaderConfigState
        let setLvlCustomHeaders = []
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (!this.state.setLvlHeaderCondition) {
            for (let j = 0; j < setLvlCustomHeadersState.length; j++) {
                if (data == setLvlCustomHeadersState[j]) {
                    setLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlCustomHeadersState.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
            if (this.state.setLvlCustomHeadersState.length == 0) {
                this.setState({
                    setLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < setLvlGetHeaderConfig.length; i++) {
                if (data == setLvlGetHeaderConfig[i]) {
                    setLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlGetHeaderConfig.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
        }
        setLvlCustomHeaders.forEach(e => delete setLvlHeaderConfigState[e]);
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeaders: setLvlHeaderConfigState,
            setLvlCustomHeadersState,
        })
        setTimeout(() => {
            let keygetvalue = (_.invert(this.state.setLvlFixedHeaderData))[data];
            let setLvlSaveState = this.state.setLvlSaveState
            setLvlSaveState.push(keygetvalue)
            let setLvlHeaderSummary = this.state.setLvlHeaderSummary
            let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
            if (!this.state.setLvlHeaderCondition) {
                for (let j = 0; j < setLvlHeaderSummary.length; j++) {
                    if (keygetvalue == setLvlHeaderSummary[j]) {
                        setLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < setLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == setLvlDefaultHeaderMap[i]) {
                        setLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                setLvlHeaderSummary,
                setLvlDefaultHeaderMap,
                setLvlSaveState
            })
        }, 100);
    }
    setLvlsaveColumnSetting(e) {
        this.setState({
            setLvlColoumSetting: false,
            setLvlHeaderCondition: false,
            setLvlSaveState: []
        })
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "SHIPMENT",
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    setLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // setLvlParaMsg: "Click confirm to continue.",
            setLvlConfirmModal: true,
        })
    }
    setLvlResetColumn() {
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "SHIPMENT",
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            setLvlHeaderCondition: true,
            setLvlColoumSetting: false,
            setLvlSaveState: []
        })
    }
    setLvlCloseConfirmModal(e) {
        this.setState({
            setLvlConfirmModal: !this.state.setLvlConfirmModal,
        })
    }
    /* componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createHeaderConfig.isSuccess && this.props.replenishment.createHeaderConfig.data.basedOn == "SET") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.set_Display,
                basedOn: "SET"
            }
            this.props.getHeaderConfigRequest(payload)
        }
    } */


    date() {
        if (this.state.shipmentRequestedDate != "") {
            this.setState({
                dateerr: false,
            })
        } else {
            this.setState({
                dateerr: true,
            })
        }
    }
    fromDate() {
        if (this.state.qcFromFormatted != "") {
            this.setState({
                qcFromDateErr: false,
            })
        } else {
            if( this.state.isQcOn === "TRUE" )
            this.setState({ qcFromDateErr: true })
         else
            this.setState({ qcFromDateErr: false })
        }
    }
    toDate() {
        if (this.state.qcToFormatted != "") {
            this.setState({
                qcToDateErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE")
               this.setState({ qcToDateErr: true,editAsn:true })
            else
               this.setState({ qcToDateErr: false,editAsn:false }) 
        }
    }
    contactNameFun() {
        let contactNamePattern =  /^[A-Za-z0-9\.\-\_\s]+$/
        if (this.state.contactName != "" && contactNamePattern.test(this.state.contactName)) {
            this.setState({
                contactNameErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE")
               this.setState({ contactNameErr: true,editAsn:true })
            else
               this.setState({ contactNameErr: false,editAsn:false })   
        }
    }
    contactNumberFun () {
        let numberPattern = /^[0-9]+$/;
        if (this.state.contactNumber != "" && Number(this.state.contactNumber.length) == 10 && numberPattern.test(this.state.contactNumber)) {
            this.setState({
                contactNumberErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE")
               this.setState({ contactNumberErr: true,editAsn:true })
            else
               this.setState({ contactNumberErr: false,editAsn:false })
        }
    }
    locationFun() {
        if (this.state.location != "" && this.state.baseLocation !== "") {
            this.setState({
                locationErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE")
               this.setState({ locationErr: true,editAsn:true })
            else
               this.setState({ locationErr: false,editAsn:false })  
        }
    }
    addressFun () {
        if (this.state.address != "" && this.state.addressCity !== "") {
            this.setState({
                addressErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE")
              this.setState({ addressErr: true,editAsn:true })
            else
              this.setState({ addressErr: false,editAsn:false })
        }
    }
    fileFun =()=> {
        if (this.state.isPPSAvailable) {
            if(this.state.PPSArray.length == 0 && this.state.oldPpsFiles.length == 0){
                this.setState({
                    ppsFilesErrors: true
                })
            }else{

                this.setState({
                    ppsFilesErrors: false
                })
            }
        } else if (!this.state.isPPSAvailable){
           this.setState({ ppsFilesErrors: false })   
        } else if (this.state.isTestReportAvailable){
            if(this.state.ReportArray.length == 0 && this.state.oldReportFiles.length == 0){
                this.setState({
                    reportFilesErrors: true
                })
            }else{
                this.setState({
                    reportFilesErrors: false
                })
            } 
        } else if (!this.state.isTestReportAvailable){
            this.setState({ reportFilesErrors: false })   
        }else if (this.state.isPackingAvailable){
            if(this.state.packingArray.length == 0 && this.state.oldPackingFiles.length == 0){
                this.setState({
                    packingFilesErrors: true
                })
            }else{
                this.setState({
                    packingFilesErrors: false
                })
            } 
        } else if (!this.state.isPackingAvailable){
            this.setState({ packingFilesErrors: false })   
        }
    }

    createFinalShipment() {
        if( this.state.poDetailsShipment.isCreateASNAllowed !== undefined && this.state.poDetailsShipment.isCreateASNAllowed == 0){
            this.props.toastMsgForFromToValidity();
        }else{
            this.date()
            this.fromDate()
            this.toDate()
            this.contactNameFun()
            this.contactNumberFun()
            this.addressFun()
            this.locationFun()
            this.fileFun()
            setTimeout(() => {
                if (!this.state.dateerr && !this.state.qcFromDateErr && !this.state.qcToDateErr
                    && !this.state.contactNameErr
                    && !this.state.contactNumberErr
                    && !this.state.addressErr
                    && !this.state.locationErr
                    && !this.state.ppsFilesErrors
                    && !this.state.reportFilesErrors
                    ) {
                    let shipmentDetails = this.state.shipmentDetails
                    var changelineitems = [], totalRequestQty = 0, basicLineAmount = 0, basicAmount = 0, taxAmount = 0, setTotalOrderQty = 0, totalOrderQty = 0, totalCancelledQty = 0, totalPendingQty = 0;
                    changelineitems = this.state.setBasedArray.map((item) => {
                        totalRequestQty = totalRequestQty + parseInt(item.requestedQty == "" ? 0 : item.requestedQty)
                        totalCancelledQty = totalCancelledQty + parseInt(item.cancelledQty == "" ? 0 : item.cancelledQty)
                        totalOrderQty = totalOrderQty + parseInt(item.orderQty)
                        basicLineAmount = parseInt(item.rate) * parseInt(item.requestedQty == "" ? 0 : item.requestedQty)
                        totalPendingQty = totalPendingQty + parseInt(item.pendingQty),
                        basicAmount = basicAmount + basicLineAmount
                        taxAmount = taxAmount + basicLineAmount * (parseInt(item.gst) / 100)
                        return {
                            orderDetailId: item.orderDetailId == undefined ? 0 : item.orderDetailId,
                            setHeaderId: item.setHeaderId,
                            requestedQty: parseInt(item.requestedQty == "" ? 0 : item.requestedQty),
                            orderQty: item.totalNoOfSet == undefined ? item.orderQty : item.totalNoOfSet,
                            pendingQty: item.pendingQty,
                            cancelQty: parseInt(item.cancelledQty == "" ? 0 : item.cancelledQty),
                        }
                    })
                    let payload = {
                        shipmentId: this.props.shipmentId,
                        basicAmount,
                        taxAmount,
                        shipmentAdviceCode: this.props.expandedPo.shipmentAdviceCode,
                        // poId: shipmentDetails.id,
                        // shipmentAdviceCode: shipmentDetails.shipmentAdviceCode,
                        // shipmentRequestOnDate: todayDate + "T00:00+05:30",
                        shipmentRequestDate: this.state.shipmentRequestedDate + "T00:00+05:30",
                        shipmentRequestOnDate: todayDate + "T00:00+05:30",
                        shipmentTotalRequestedQty: totalRequestQty,
                        totalPendingQty: totalPendingQty,
                        shipmentTotalCancelledQty: totalCancelledQty,
                        dueInDays: this.props.expandedPo.dueDays,
                        transporter: this.props.expandedPo.transporterName,
                        isShipConfirmed: "FALSE",
                        poDetails: changelineitems,
                        orderCode: this.props.order.orderCode,
                        orderNumber: this.props.order.orderNumber,
                        qcFromDate: (this.state.qcFromDate == "" || this.state.isQcOn == "FALSE") ? "" : this.state.qcFromDate + "T00:00+05:30",
                        qcToDate: (this.state.qcToDate == "" || this.state.isQcOn == "FALSE") ? "" : this.state.qcToDate + "T00:00+05:30",
                        remarks: this.state.remarks,
                        documentNumber: this.props.documentNumber,
                        poType: this.state.poType,
                        orderId: this.props.orderId,
                        contactPerson:this.state.contactName,
                        contactNo:this.state.contactNumber,
                        address:this.state.address,
                        location:this.state.location,
                        isPPSAvailable:(this.state.isPPSAvailable.toString()).toUpperCase(),
                        isTestReportAvailable:(this.state.isTestReportAvailable.toString()).toUpperCase(),
                        isPackagingUploaded: (this.state.isPackingAvailable.toString()).toUpperCase(),
                    }
                    this.props.updateVendorShipmentRequest(payload)
                    if(this.state.isPPSAvailable){
                        this.onFileUpload("PPS")
                    }
                    if(this.state.isTestReportAvailable){
                        this.onFileUpload("REPORT")
                    }
                    if(this.state.isPackingAvailable){
                        this.onFileUpload("PACKAGING")
                    }

                    //To delete already uploaded files::
                    this.state.toBeDeletedFilesPayload.map(payload =>{
                        this.props.deleteUploadsRequest(payload);
                    })

                    this.setState({
                        editAsn:false,saveShow:false
                    })
                }
            }, 10);
        }
    }

    handleChange = (e) => {
        var formattedDate = moment(e.target.value, "YYYY-MM-DD").format("DD-MM-YYYY")
        var maxQCToDate = moment(e.target.value).add(2,'days').format("YYYY-MM-DD")
        if (e.target.id == "qcFromDate") {
            this.setState({ qcFromDate: e.target.value, qcToDate: "", qcToFormatted: "", qcFromFormatted: formattedDate == "Invalid date" ? "" : formattedDate, maxQCToDate : maxQCToDate, }, () => { this.fromDate() })
        } else if (e.target.id == "qcToDate") {
            this.setState({ qcToDate: e.target.value, qcToFormatted: formattedDate == "Invalid date" ? "" : formattedDate }, () => { this.toDate() })
        } else if (e.target.id == "remarks") {
            this.setState({ remarks: e.target.value })
        } else if (e.target.id == "contactName") {
            this.setState({ contactName: e.target.value },() => { this.contactNameFun() })
        } else if (e.target.id == "contactNumber") {
            if( e.target.value.length <= 10 )
            this.setState({ contactNumber: e.target.value },() => { this.contactNumberFun() })
        } else if (e.target.id == "address") {
            this.setState({ address: e.target.value,
                addressSearch: this.state.isModalShow ? "" : e.target.value,
                addressCity: ""})
        } else if (e.target.id == "location") {
            this.setState({ location: e.target.value, baseLocation: "" })
        } else if (e.target.id == "ppsCheckBox") {

            if(this.state.isPPSAvailable){
                this.setState({ isPPSAvailable: false,ppsFilesErrors:false })
            }else{
                this.setState({ isPPSAvailable: true })
            }
        } else if (e.target.id == "testReportCheckBox") {
            if(this.state.isTestReportAvailable){
                this.setState({ isTestReportAvailable: false,reportFilesErrors:false })
            }else{
                this.setState({ isTestReportAvailable: true })
            }
        }
        else if (e.target.id == "packingCheckBox") {
            if(this.state.isPackingAvailable){
                this.setState({ isPackingAvailable: false, packingFilesErrors:false })
            }else{
                this.setState({ isPackingAvailable: true })
            }
        }
    }

    handleQuantityChange =(e, data)=> {
        var setBasedArray = [...this.state.setBasedArray]
        var expandedLineItems = { ...this.state.expandedLineItems }
        var id = this.props.poType == "poicode" ? data.orderDetailId : data.setBarCode
        var indexId = this.props.poType == "poicode" ? "orderDetailId" : "setBarCode"
        let index = setBasedArray.findIndex((obj => obj[indexId] == data[indexId]));
        let numberPattern = /^[0-9]+$/;
        if (e.target.id == "requestedQty") {

            //For Checking Pending Qty is less than zero or not::
            let pendingQtyValue = (parseInt(setBasedArray[index].setExcludeQty == undefined ? setBasedArray[index].excludeQty : setBasedArray[index].setExcludeQty) 
                                  - (parseInt(numberPattern.test(e.target.value) ? e.target.value : 0) 
                                  + parseInt(setBasedArray[index].cancelledQty == "" ? 0 : setBasedArray[index].cancelledQty)))

            if( pendingQtyValue >= 0 ){  
                
                //For set's item qty calculation::
                var arr = [...this.state.itemBaseArray]
                arr.map((item) => (
                    item.requestQty = Number(item.nbset) * Number(numberPattern.test(e.target.value) ? e.target.value : 0),
                    item.totalPendingQty = Number(item.orderQty) - (Number(item.cancelQty) + Number(item.requestQty) + Number(item.totalRequestedQty) + Number(item.totalCancelledQty))
                ))
                this.setState({
                    expandedLineItems: { ...this.state.expandedLineItems, [id]: arr },
                    itemBaseArray: arr,
                })

                //For set qty calculation::
                setBasedArray[index].requestedQty = numberPattern.test(e.target.value) ? e.target.value : "";
                setBasedArray[index].pendingQty = (parseInt(setBasedArray[index].setExcludeQty == undefined ? setBasedArray[index].excludeQty : setBasedArray[index].setExcludeQty) - (parseInt(numberPattern.test(e.target.value) ? e.target.value : 0) + parseInt(setBasedArray[index].cancelledQty == "" ? 0 : setBasedArray[index].cancelledQty)))

                if(Number(setBasedArray[index].pendingQty < 0)){
                    setBasedArray[index].error = true
                }

                //Percent Fulfilment logic::
                //If multiple ASN Creation False need to add cancelled qty in pending qty::
                let finalCancelledQty = !this.state.isMultipleAsnAllow ? Number(setBasedArray[index].cancelledQty) : Number(0);
                let finalReqestedQty = setBasedArray[index].requestedQty == "" ? 0 : Number(setBasedArray[index].requestedQty);
                let finalTotalQty = (Number(finalReqestedQty) + Number(setBasedArray[index].pendingQty == "" ? 0 : setBasedArray[index].pendingQty) + finalCancelledQty);
                if(finalTotalQty > 0 && numberPattern.test(setBasedArray[index].pendingQty)){
                    let calculatedPercent = parseInt(finalReqestedQty * 100 / finalTotalQty);
                    if(this.state.allowNoOfPercentFulfil && calculatedPercent < this.state.noOfPercentFulfil){
                        // If one PO has multiple set, then if requested qty is 0::
                        if( this.state.cancelLinePendingQty && setBasedArray.length > 1 && finalReqestedQty == 0){
                            setBasedArray[index].error = false;
                            setBasedArray[index].percentFulfilFlag = false;
                            this.props.toastMsgForPercentageFulfil(e, this.state.noOfPercentFulfil, 0)
                        }else{
                            this.props.toastMsgForPercentageFulfil(e, this.state.noOfPercentFulfil, 1)
                            setBasedArray[index].error = true;
                            setBasedArray[index].percentFulfilFlag = true;
                        }    
                    }else{
                        setBasedArray[index].percentFulfilFlag = false;
                        this.props.toastMsgForPercentageFulfil(e, this.state.noOfPercentFulfil, 0)
                    }
                    this.setState({ calculatedPercent: parseInt(finalReqestedQty * 100 / finalTotalQty)})
                }
            }

        } else if (e.target.id == "cancelQty") {

            //For Checking Pending Qty is less than zero or not::
            let pendingQtyValue = parseInt(setBasedArray[index].setExcludeQty == undefined ? setBasedArray[index].excludeQty : setBasedArray[index].setExcludeQty) 
                                  - ((parseInt(numberPattern.test(e.target.value) ? e.target.value : 0) 
                                  + parseInt(setBasedArray[index].requestedQty == "" ? 0 : setBasedArray[index].requestedQty)))

            if( pendingQtyValue >= 0 ){ 

                //For set's item qty calculation::
                if (expandedLineItems[id] != undefined) {
                    var arr = [...expandedLineItems[id]]
                    arr.map((item) => (
                        item.cancelQty = Number(item.nbset) * Number(numberPattern.test(e.target.value) ? e.target.value : 0),
                        item.totalPendingQty = Number(item.orderQty) - (Number(item.cancelQty) + Number(item.requestQty) + Number(item.totalRequestedQty) + Number(item.totalCancelledQty))
                    ))
                    this.setState({
                        expandedLineItems: { ...this.state.expandedLineItems, [id]: arr },
                        itemBaseArray: arr,
                    })
                }

                //For set qty calculation::
                setBasedArray[index].pendingQty = parseInt(setBasedArray[index].setExcludeQty == undefined ? setBasedArray[index].excludeQty : setBasedArray[index].setExcludeQty) - ((parseInt(numberPattern.test(e.target.value) ? e.target.value : 0) + parseInt(setBasedArray[index].requestedQty == "" ? 0 : setBasedArray[index].requestedQty)))
                setBasedArray[index].cancelledQty = numberPattern.test(e.target.value) ? e.target.value : ""
                if (Number(setBasedArray[index].pendingQty) < 0) {
                    setBasedArray[index].error = true
                }
                // if (Number(setBasedArray[index].cancelledQty) && Number(setBasedArray[index].cancelledQty) > (Number(setBasedArray[index].totalNoOfSet) - Number(setBasedArray[index].requestedQty) - Number(setBasedArray[index].requestedQty) - Number(setBasedArray[index].cancelledQty))) {
                //     setBasedArray[index].error = true
                // }

                //Percent Fulfilment logic::
                let finalCancelledQty = !this.state.isMultipleAsnAllow ? Number(setBasedArray[index].cancelledQty) : Number(0);
                let finalReqestedQty = setBasedArray[index].requestedQty == "" ? 0 : Number(setBasedArray[index].requestedQty);
                let finalTotalQty = (Number(finalReqestedQty) + Number(setBasedArray[index].pendingQty == "" ? 0 : setBasedArray[index].pendingQty) + finalCancelledQty);
                if(finalTotalQty > 0 && numberPattern.test(setBasedArray[index].pendingQty)){
                    let calculatedPercent = parseInt(finalReqestedQty * 100 / finalTotalQty);
                    if(this.state.allowNoOfPercentFulfil && calculatedPercent < this.state.noOfPercentFulfil){
                        this.props.toastMsgForPercentageFulfil(e, this.state.noOfPercentFulfil, 1)
                        setBasedArray[index].error = true;
                        setBasedArray[index].percentFulfilFlag = true;
                    }else{
                        setBasedArray[index].percentFulfilFlag = false;
                        this.props.toastMsgForPercentageFulfil(e, this.state.noOfPercentFulfil, 0)
                    }
                    this.setState({ calculatedPercent: parseInt(finalReqestedQty * 100 / finalTotalQty)})
                }
            }

        }
        else if (e.target.id == "newRequestedQty") {
            setBasedArray[index].totalPendingQty = e.target.value
        }
        if (Number(setBasedArray[index].pendingQty) >= 0 && !setBasedArray[index].percentFulfilFlag) {
            setBasedArray[index].error = false
        }
        // if (!(Number(setBasedArray[index].requestedQty) > (Number(setBasedArray[index].setExcludeQty))) && !(Number(setBasedArray[index].cancelledQty) && Number(setBasedArray[index].cancelledQty) > (Number(setBasedArray[index].setExcludeQty) - Number(setBasedArray[index].requestedQty)))) {
        //     setBasedArray[index].error = false
        // }

        this.setState({
            setBasedArray,
            saveShow: !setBasedArray.some((data) => data.error == true),
            emptyShow: !setBasedArray.every((item) => ((Number(item.setRequestedQty) == 0 || "") && (Number(item.setCancelledQty) == 0 || ""))),
            nullShow: setBasedArray.every((item) => ((Number(item.setRequestedQty) == "") || (Number(item.setCancelledQty) == "")))
        })
    }
    handleRequestedDate = (e) => {
        var formattedDate = moment(e.target.value, "YYYY-MM-DD").format("DD-MM-YYYY")
        this.setState({ shipmentRequestedDate: e.target.value, qcFromFormatted: "", qcToFormatted: "", formattedDate: formattedDate == "Invalid date" ? "" : formattedDate }, () => {
            this.date()
        }
        )
    }
    openAddressModal(e, id) {
        this.onEsc()
        let data = {
            type: this.state.address == "" || this.state.isModalShow ? 1 : 3,
            pageNo: 1,
            search: this.state.isModalShow ? "" : this.state.address,
            sortedBy: "",
            sortedIn: ""
        }
        this.props.getAddressRequest(data)

        this.setState({
            addressSearch: this.state.isModalShow ? "" : this.state.address,
            addressModal: true,
            AddressModalAnimation: !this.state.AddressModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    openLocationModal(e, id) {
        this.onEsc()
        let data = {
            type: this.state.location == "" || this.state.isModalShow ? 1 : 3,
            pageNo: 1,
            search: this.state.isModalShow ? "" : this.state.location,
            sortedBy: "",
            sortedIn: ""
        }
        this.props.getLocationRequest(data)

        this.setState({
            locationSearch: this.state.isModalShow ? "" : this.state.location,
            locationModal: true,
            LocationModalAnimation: !this.state.LocationModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    onEsc = () => {
        this.setState({
            addressModal: false,
            locationModal: false,
            ppsUploadModal: false,
        })//  this.escChild.current.childEsc();
    }
    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(id).click();
        } else if (e.key === "Enter") {
            if (id == "address") {
              this.openAddressModal() 
            }
            else if (id == "location") {
                this.openLocationModal()
            }
        } else {
            document.getElementById(id).focus()
        }
    }
    onCloseAddressModal(e) {
        this.setState({
            addressModal: false,
            AddressModalAnimation: !this.state.AddressModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("address").focus()
    }
    addressVal = (city, data) => {
        this.setState({
            address:data,
            addressModal:false,
            addressCity: city,
        },
        ()=>{
            this.addressFun()
        })
    }
    onCloseLocationModal(e) {
        this.setState({
            locationModal: false,
            LocationModalAnimation: !this.state.LocationModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("address").focus()
    }
    locationVal = (baseLocation, data) => {
        this.setState({
            location:data,
            locationModal:false,
            baseLocation: baseLocation,
        },
        ()=>{
            this.locationFun()
        })
    } 
    onMmltipleFileUpload = (evt,subModule) =>{
        if(subModule=="PPS"){
            var files = evt.target.files; // FileList object
            var totalFiles = [];
            for (var i = 0, f; f = files[i]; i++) {
                const scope = this
                totalFiles.push(f)
                this.setState({
                    PPSArray:this.state.PPSArray.concat(totalFiles).reverse(),
                },(e)=>{
                });
            
            var reader = new FileReader();
            reader.onload = (f,function(theFile) {
                return (e) => {
                var span = document.createElement('span');
                var img = document.createElement('img');
                img.className = "remove";
                img.src=clearSearch
                img.style.display="inherit"
                span.className = "caffbuf-file-name";
                span.innerHTML = 
                ['<span>'+theFile.name+'</span>',img.outerHTML]
                .join('')
                document.getElementById('ppsfileLabel').insertBefore(span, null);
                $(".remove").click((clickEvent)=>{
                    var index = Array.from(document.getElementById('ppsfileLabel').children).indexOf(clickEvent.target.parentNode)
                    var remove = document.querySelectorAll('#ppsfileLabel span')[index]
                    document.querySelector("#ppsfileLabel").removeChild(remove);
                    scope.state.PPSArray.splice(index, 1)
                })
                };
            })(f);                                                                                                                                                                                                                                                                
            
            reader.readAsDataURL(f);
            }
        }else if(subModule=="REPORT"){
            var files = evt.target.files; // FileList object
            var totalFiles = [];
            for (var i = 0, f; f = files[i]; i++) {
                const scope = this
                totalFiles.push(f)
                this.setState({
                ReportArray:this.state.ReportArray.concat(totalFiles).reverse(),
                },(e)=>{
                });
            
            var reader = new FileReader();
            reader.onload = (f,function(theFile) {
                return (e) => {
                var span = document.createElement('span');
                var img = document.createElement('img');
                img.className = "remove";
                img.src=clearSearch
                img.style.display="inherit"
                span.className = "caffbuf-file-name";
                span.innerHTML = 
                ['<span>'+theFile.name+'</span>',img.outerHTML]
                .join('')
                document.getElementById('reportFileLabel').insertBefore(span, null);
                $(".remove").click((clickEvent)=>{
                    var index = Array.from(document.getElementById('reportFileLabel').children).indexOf(clickEvent.target.parentNode)
                    var remove = document.querySelectorAll('#reportFileLabel span')[index]
                    document.querySelector("#reportFileLabel").removeChild(remove);
                    scope.state.ReportArray.splice(index, 1)
                })
                };
            })(f);                                                                                                                                                                                                                                                                
            
            reader.readAsDataURL(f);
            }
        }
        
       
    }
    onFileUpload(subModule) {
        
        let files = subModule == "PPS" ? this.state.PPSArray : subModule == "REPORT" ? this.state.ReportArray : this.state.packingArray
        let date = new Date()
        const fd = new FormData();
        let uploadNode = {
            shipmentId: this.props.shipmentId,
            orderNumber: this.props.order.orderNumber,
            module: "SHIPMENT",
            subModule: subModule,
            isQCAvailable: "FALSE",
            isInvoiceAvailable: "FALSE",
            isBarcodeAvailable: "TRUE",
            orderId: this.props.orderId,
            documentNumber: this.props.documentNumber,
            commentId: this.props.shipmentId,
            commentCode: this.props.shipmentAdviceCode,
            vendorCode: this.props.vendorCode,
        }
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        for (var i = 0; i < files.length; i++) {
            fd.append("files", files[i]);
        }
        fd.append("uploadNode", JSON.stringify(uploadNode))
        // this.loaderUpload("true")
        if(files.length){
            axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
                .then(res => {
                    }).catch((error) => {
                    this.setState({
                        loader: false
                    })
                    this.props.openToastError()
                })
        }
    }
    deleteUploads=(data, subModule) =>{
        if(subModule =="PPS"){
            let oldPpsFiles = this.state.oldPpsFiles.filter((check) => check.fileURL != data.fileURL)
            let files = []
            let a = {
                fileName: data.fileName,
                isActive: "FALSE"
            }
            files.push(a)
            let payload = {
                shipmentId: this.props.shipmentId,
                module: "SHIPMENT",
                subModule: "PPS",
                files: files,
                commentId: this.props.shipmentId
            }
            this.setState({
                oldPpsFiles,
            },()=> //this.props.deleteUploadsRequest(payload)
            this.state.toBeDeletedFilesPayload.push(payload))
        }else if(subModule == "REPORT"){
            let oldReportFiles = this.state.oldReportFiles.filter((check) => check.fileURL != data.fileURL)
            let files = []
            let a = {
                fileName: data.fileName,
                isActive: "FALSE"
            }
            files.push(a)
            let payload = {
                shipmentId: this.props.shipmentId,
                module: "SHIPMENT",
                subModule: "REPORT",
                files: files,
                commentId: this.props.shipmentId
            }
            this.setState({
                oldReportFiles,
            },()=> //this.props.deleteUploadsRequest(payload)
            this.state.toBeDeletedFilesPayload.push(payload))   
        }
        else{
            let oldPackingFiles = this.state.oldPackingFiles.filter((check) => check.fileURL != data.fileURL)
            let files = []
            let a = {
                fileName: data.fileName,
                isActive: "FALSE"
            }
            files.push(a)
            let payload = {
                shipmentId: this.props.shipmentId,
                module: "SHIPMENT",
                subModule: "PACKAGING",
                files: files,
                commentId: this.props.shipmentId
            }
            this.setState({
                oldPackingFiles,
            },()=> //this.props.deleteUploadsRequest(payload)
            this.state.toBeDeletedFilesPayload.push(payload))   
        }
        
    }
    onFileUploadSubmit = (fileList,moduleName) =>{
        if(moduleName == "PPS"){
            this.setState({
                PPSArray:fileList,
                ppsUploadModal: false,
                ppsFilesErrors:fileList.length != 0 || this.state.oldPpsFiles.length != 0 ? false: true
            })
        }else if(moduleName == "REPORT"){
            this.setState({
                ReportArray:fileList,
                ppsUploadModal: false,
                reportFilesErrors:fileList.length != 0 || this.state.oldReportFiles.length != 0 ? false: true
            })
        }
        else{
            this.setState({
                packingArray: fileList,
                ppsUploadModal: false,
                packingFilesErrors: fileList.length != 0 || this.state.oldPackingFiles.length != 0 ? false: true
            })
        }
    }

    calculatedPercent =(e, data)=>{
        var setBasedArray = [...this.state.setBasedArray]
        var indexId = this.props.poType == "poicode" ? "orderDetailId" : "setBarCode"
        let index = setBasedArray.findIndex((obj => obj[indexId] == data[indexId]));
        let numberPattern = /^[0-9]+$/;

        //Percent Fulfilment logic::
        let finalCancelledQty = !this.state.isMultipleAsnAllow ? Number(setBasedArray[index].cancelledQty) : Number(0);
        let finalReqestedQty = setBasedArray[index].requestedQty == "" ? 0 : Number(setBasedArray[index].requestedQty);
        let finalTotalQty = (Number(finalReqestedQty) + Number(setBasedArray[index].pendingQty == "" ? 0 : setBasedArray[index].pendingQty) + finalCancelledQty);
        if(finalTotalQty > 0 && numberPattern.test(setBasedArray[index].pendingQty)){
            this.setState({ calculatedPercent: parseInt(finalReqestedQty * 100 / finalTotalQty)})
        }
    }

    onRefresh =(e)=>{
        if(this.state.editAsn){
            this.setState({ cancelEditAsn: true, dropOpen: false,
                            PPSArray: [],
                            ReportArray: [],
                            packingArray: [],
                            ppsFilesErrors: false,
                            reportFilesErrors: false,
                            packingFilesErrors: false,
                            toBeDeletedFilesPayload: [],
            },()=>{
                let payload = {
                    orderId: this.props.orderId,
                    setHeaderId: "",
                    detailType: this.props.poType == "poicode" ? "item" : "set",
                    poType: this.props.poType,
                    shipmentStatus: this.props.poStatus,
                    shipmentId: this.props.shipmentId
                }
                this.props.getShipmentDetailsRequest(payload)
                this.props.toastMsgForPercentageFulfil(e, this.state.noOfPercentFulfil, 0)
            })     
        }
    }

    render() {
        const { qcFromDateErr, 
            qcToDateErr, 
            dateerr,
            contactNumberErr,
            contactNameErr,
            addressErr,
            locationErr,
            address,
            location,
            contactName,
            contactNumber,
            reportFilesErrors,
            ppsFilesErrors } = this.state
        return (
            <div>
                <div className="gen-vend-sticky-table">
                    <div className="gvst-expend" id="expandTableMain">
                        <div className="col-md-12 pad-0">
                            {this.props.inputValues == "true" ? <div className="create-asn-form">
                                <form>
                                    <div className="col-md-3 pad-0 pr-15">
                                        <div className="caf-details">
                                            <div className="cafd-row">
                                                <label className="head-label">PO Number</label>
                                                <span className="cafd-number">{this.props.expandedPo.orderNumber}</span>
                                            </div>
                                            <div className="cafd-row">
                                                <label className="head-label">ASN/Inspection Number</label>
                                                <span className="cafd-number"> {this.state.isAsnMask ? this.props.expandedPo.asnMask : this.props.expandedPo.shipmentAdviceCode}</span>
                                            </div>
                                            <div className="cafd-row">
                                                <div className="cafdr-inner">
                                                    <label className="head-label">PO Valid From</label>
                                                    <span className="cafd-number">{this.props.expandedPo.validFromDate}</span>
                                                </div>
                                                <div className="cafdr-inner">
                                                    <label className="head-label">PO Valid To</label>
                                                    <span className="cafd-number"> {this.props.expandedPo.validToDate}</span>
                                                </div>
                                                {this.state.createAsnFromToValidity && <div className="cafd-row">
                                                    <div className="cafdr-inner">
                                                        <label className="head-label">ASN Valid From</label>
                                                        <span className="cafd-number">{this.state.poDetailsShipment.asnFromDate}</span>
                                                    </div>
                                                    <div className="cafdr-inner">
                                                        <label className="head-label">ASN Valid To</label>
                                                        <span className="cafd-number"> {this.state.poDetailsShipment.asnToDate}</span>
                                                    </div>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-9 pad-0">
                                        <div className="caf-fields">
                                            <div className={this.state.isQcOn == "TRUE" ? "caff-top": "caff-top caff-set-upload-packing"}>
                                                <div className="cafft-input dateFormat">
                                                    <label className="caffti-label">Expected Delivery Date<span className="mandatory">*</span></label>
                                                    <input type="date" 
                                                    disabled={!this.state.editAsn} 
                                                    className={!this.state.editAsn ? "bgDisable  inputDate" : "inputDate"} 
                                                    min={(this.state.createAsnFromToValidity && this.state.poDetailsShipment.isCreateASNAllowed == 1) ? (todayDate > this.state.poDetailsShipment.asnFromSelectionDate ? todayDate : this.state.poDetailsShipment.asnFromSelectionDate ): (todayDate > this.state.validFromDateSelect ? todayDate : this.state.validFromDateSelect)} 
                                                    max={this.state.createAsnFromToValidity ? this.state.poDetailsShipment.asnToSelectionDate : this.state.validToDateSelect} 
                                                    data-date={this.state.formattedDate == "" ? "Select Date" : this.state.formattedDate} 
                                                    onChange={this.handleRequestedDate} />
                                                    {/* <input type="date" className="inputDate" min={todayDate} max={this.state.maxValue} placeholder={this.state.shipmentRequestedDate == "" ? "Select Date" : this.state.shipmentRequestedDate} value={this.state.shipmentRequestedDate} onChange={this.handleRequestedDate} /> */}
                                                    {dateerr ? (
                                                        <span className="error">
                                                            Select Date
                                                            </span>
                                                    ) : null}
                                                </div>
                                                {this.state.isQcOn == "TRUE" ? null : this.state.allowPackingList &&<div className="caff-check">
                                                     <div className="caffc-fields">
                                                        <label className="checkBoxLabel0">
                                                            <input type="checkBox" disabled={!this.state.editAsn} name="selectEach" checked ={this.state.isPackingAvailable} id="packingCheckBox" onChange={this.handleChange}/>
                                                            <span className={!this.state.editAsn ? "bgDisable  checkmark1" : "checkmark1"}></span>
                                                            Packing List
                                                        </label>
                                                        { this.state.isPackingAvailable  ?
                                                        <div className="caffb-upload-file">
                                                            <label onClick={(e) => !this.state.editAsn ? null : this.openPpsUploadModal(e,"PACKAGING")} className={!this.state.editAsn ? "bgDisable" : ""}>
                                                                {this.state.packingArray.length == 0 && this.state.oldPackingFiles.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{this.state.packingArray.length+this.state.oldPackingFiles.length} Packing List</span>}
                                                                <img src={require('../../../../assets/attach.svg')} />
                                                            </label>
                                                            <span id="reportFileLabel"></span>
                                                            {this.state.packingFilesErrors ? (
                                                                <span className="error">
                                                                    Add files
                                                            </span>
                                                            ) : null}
                                                        </div>
                                                        :null}
                                                    </div>
                                                </div>}
                                                {(this.state.qcFromFormatted !== "Invalid date" && this.state.isQcOn == "TRUE") &&<div className="cafft-input dateFormat">
                                                    <label className="caffti-label">Insp from date<span className="mandatory">*</span></label>
                                                    <input type="date" 
                                                    disabled={!this.state.editAsn} 
                                                    className={!this.state.editAsn ? "bgDisable  inputDate" : "inputDate"} 
                                                    id="qcFromDate" 
                                                    data-date={this.state.qcFromFormatted == "" || this.state.qcFromFormatted == null ? "Select Date" : this.state.qcFromFormatted} 
                                                    min={ (this.state.createAsnFromToValidity && this.state.poDetailsShipment.isCreateASNAllowed == 1) ? (todayDate > this.state.poDetailsShipment.asnFromSelectionDate ? todayDate : this.state.poDetailsShipment.asnFromSelectionDate ) : (this.state.validFromDateSelect < todayDate ? todayDate : this.state.validFromDateSelect)} 
                                                    max={this.state.shipmentRequestedDate == "" ? this.state.validToDateSelect : this.state.shipmentRequestedDate} 
                                                    onChange={this.handleChange} />
                                                    {/* <input type="date" className="inputDate" id="qcFromDate" min={todayDate} max={this.state.maxValue} placeholder={this.state.qcFromDate == "" ? "Select Date" : this.state.qcFromDate} value={this.state.qcFromDate} onChange={this.handleChange} /> */}
                                                    {qcFromDateErr ? (
                                                        <span className="error">
                                                            Select Date
                                                            </span>
                                                    ) : null}
                                                </div>}
                                                {(this.state.qcToFormatted !== "Invalid date" && this.state.isQcOn == "TRUE") && <div className="cafft-input dateFormat">
                                                    <label className="caffti-label">Insp to date<span className="mandatory">*</span></label>
                                                    {this.state.qcFromDate == "" ? <input type="date" className={!this.state.editAsn ? "bgDisable  inputDate" : "inputDate"} disabled value="" data-date={this.state.qcToFormatted == "" ? "Select Date" : this.state.qcToFormatted} /> : <input type="date" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable  inputDate" : "inputDate"} id="qcToDate" min={this.state.maxQCToDate} max={this.state.shipmentRequestedDate == "" ? this.state.validToDateSelect : this.state.shipmentRequestedDate} data-date={this.state.qcToFormatted == "" ? "Select Date" : this.state.qcToFormatted} onChange={this.handleChange} />}
                                                    {/* {this.state.qcFromDate == "" ? <input type="date" className="inputDate" disabled value="" placeholder={this.state.qcToDate == "" ? "Select Date" : this.state.qcToDate} /> : <input type="date" className="inputDate" id="qcToDate" min={this.state.qcFromDate} max={this.state.maxValue} placeholder={this.state.qcToDate == "" ? "Select Date" : this.state.qcToDate} value={this.state.qcToDate} onChange={this.handleChange} />} */}
                                                    {qcToDateErr ? (
                                                        <span className="error">
                                                            Select Date
                                                            </span>
                                                    ) : null}
                                                </div>}
                                                {this.state.isQcOn == "TRUE" && <div className="cafft-input">
                                                    <label className="caffti-label">Contact Person<span className="mandatory">*</span></label>
                                                    <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable  caffti-input" : "caffti-input"}  id="contactName" value={contactName} onChange={this.handleChange}/>
                                                    {contactNameErr ? (
                                                        <span className="error">
                                                            Enter contact name
                                                    </span>
                                                    ) : null}
                                                </div>}
                                                {this.state.isQcOn == "TRUE" && <div className="cafft-input">
                                                    <label className="caffti-label">Contact Number<span className="mandatory">*</span></label>
                                                    <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable  caffti-input" : "caffti-input"} id="contactNumber" value={contactNumber} onChange={this.handleChange}/>
                                                    {contactNumberErr ? (
                                                        <span className="error">
                                                            Enter contact number
                                                    </span>
                                                    ) : null}
                                                </div>}
                                            </div>
                                            {this.state.isQcOn == "TRUE" &&
                                            <div className="caff-mid">
                                                <div className="caffm-search">
                                                    <label className="caffti-label">Address<span className="mandatory">*</span></label>
                                                    <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable  caffti-input" : "caffti-input"} placeholder="Type to Search" autoComplete="off" value={address} id="address" onChange={this.handleChange} onKeyDown={(e) => this._handleKeyPress(e, "address")}/>
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick ={(e) => this.openAddressModal(e, "address")}>
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                    {addressErr ? (
                                                        <span className="error">
                                                            Select valid address
                                                    </span>
                                                    ) : null}
                                                    {!this.state.isModalShow ? this.state.addressModal ? <div className="backdrop-transparent"></div> : null : null}
                                                    {!this.state.isModalShow ? this.state.addressModal ? <AddressModal {...this.props} {...this.state} closeSupplier={(e) => this.openAddressModal(e, "address")} addressSearch={this.state.addressSearch} addressVal={this.addressVal} onCloseAddressModal={(e) => this.onCloseAddressModal(e)} /> : null : null}
                                                    
                                                </div>
                                                <div className="caffm-search">
                                                    <label className="caffti-label">Location<span className="mandatory">*</span></label>
                                                    <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable  caffti-input" : "caffti-input"} placeholder="Type to Search" autoComplete="off" value={location} onKeyDown={(e) => this._handleKeyPress(e, "location")} id="location" onChange={this.handleChange}/>
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick ={(e) => this.openLocationModal(e, "location")}>
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                    {locationErr ? (
                                                        <span className="error">
                                                            Select valid location
                                                    </span>
                                                    ) : null}
                                                    {!this.state.isModalShow ? this.state.locationModal ? <div className="backdrop-transparent"></div> : null : null}
                                                    {!this.state.isModalShow ? this.state.locationModal ? <LocationModal {...this.props} {...this.state} locationVal={this.locationVal} onCloseLocationModal={(e) => this.onCloseLocationModal(e)} /> : null : null}
                                                    
                                                </div>
                                            </div>
                                            }
                                            {this.state.isQcOn == "TRUE" &&
                                            <div className="caff-check">
                                                <div className="caffc-fields">
                                                    <label  className="checkBoxLabel0" >
                                                        <input type="checkBox" name="selectEach" checked ={this.state.isPPSAvailable} disabled={!this.state.editAsn} id="ppsCheckBox" onChange={this.handleChange}/>
                                                        <span className={!this.state.editAsn ? "bgDisable  checkmark1" : "checkmark1"}></span>
                                                        PPS Available
                                                    </label>
                                                    { this.state.isPPSAvailable  ?
                                                    <div className= "caffb-upload-file">
                                                        <label onClick={(e) => !this.state.editAsn ? null : this.openPpsUploadModal(e,"PPS")} className={!this.state.editAsn ? "bgDisable" : ""}>
                                                            {/* <input type="file" disabled={!this.state.editAsn} id="fileUpload"  onChange={(e) => this.onMmltipleFileUpload(e,"PPS")} multiple="multiple"   /> */}
                                                            {this.state.PPSArray.length == 0 &&  this.state.oldPpsFiles.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{this.state.PPSArray.length+this.state.oldPpsFiles.length} PPS Files</span>}
                                                            <img src={require('../../../../assets/attach.svg')} />
                                                        </label>
                                                        <span id="ppsfileLabel"></span>
                                                        {ppsFilesErrors ? (
                                                            <span className="error">
                                                                Add files
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                    :null}
                                                </div>
                                                <div className="caffc-fields">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" disabled={!this.state.editAsn} name="selectEach" checked ={this.state.isTestReportAvailable} id="testReportCheckBox" onChange={this.handleChange}/>
                                                        <span className={!this.state.editAsn ? "bgDisable  checkmark1" : "checkmark1"}></span>
                                                        Test Report
                                                    </label>
                                                    { this.state.isTestReportAvailable  ?
                                                    <div className="caffb-upload-file">
                                                        <label onClick={(e) => !this.state.editAsn ? null : this.openPpsUploadModal(e,"REPORT")} className={!this.state.editAsn ? "bgDisable" : ""}>
                                                            {/* <input type="file" disabled={!this.state.editAsn} id="fileUpload" onChange={(e) => this.onMmltipleFileUpload(e, "REPORT")} multiple="multiple" /> */}
                                                            {this.state.ReportArray.length == 0 && this.state.oldReportFiles.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{this.state.ReportArray.length+this.state.oldReportFiles.length} Test Reports</span>}
                                                            <img src={require('../../../../assets/attach.svg')} />
                                                        </label>
                                                        <span id="packingFileLabel"></span>
                                                        {reportFilesErrors ? (
                                                            <span className="error">
                                                                Add files
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                    :null}
                                                </div>
                                                {this.state.allowPackingList && <div className="caffc-fields">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" disabled={!this.state.editAsn} name="selectEach" checked ={this.state.isPackingAvailable} id="packingCheckBox" onChange={this.handleChange}/>
                                                        <span className={!this.state.editAsn ? "bgDisable  checkmark1" : "checkmark1"}></span>
                                                         Packing List
                                                    </label>
                                                    { this.state.isPackingAvailable  ?
                                                    <div className="caffb-upload-file">
                                                        <label onClick={(e) => !this.state.editAsn ? null : this.openPpsUploadModal(e,"PACKAGING")} className={!this.state.editAsn ? "bgDisable" : ""}>
                                                            {this.state.packingArray.length == 0 && this.state.oldPackingFiles.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{this.state.packingArray.length+this.state.oldPackingFiles.length} Packing List</span>}
                                                            <img src={require('../../../../assets/attach.svg')} />
                                                        </label>
                                                        <span id="reportFileLabel"></span>
                                                        {this.state.packingFilesErrors ? (
                                                            <span className="error">
                                                                Add files
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                    :null}
                                                </div>}
                                            </div>}
                                            <div className="caff-bottom">
                                                    {this.state.status == "SHIPMENT_REQUESTED" || this.state.status == "PENDING_QC" || this.state.status == "PENDING_QC_CONFIRM" ?
                                                        <div className="gvst-form-action">
                                                            <button type="button" className="saveNew  m-rgt-20" onClick={(e) => { this.onRefresh(e); this.setState({ editAsn: !this.state.editAsn },()=>this.setState({saveShow: this.state.editAsn ? true : false}))}}>{ this.state.editAsn ? " Cancel Edit" : "Edit"}</button>
                                                            {this.state.saveShow ? <button type="button" className="saveNew" onClick={() => {this.createFinalShipment()}}>Update</button>
                                                                : <button type="button" className="saveNew btnDisabled">Update</button>}
                                                        </div>:null
                                                    }
                                            </div>
                                        </div>
                                        
                                        {/* <div className="col-md-3">
                                            <div className="col-md-12 pad-0">
                                                <label className="head">Remarks</label>
                                            </div>
                                            <div className="col-md-12 pad-0">
                                                <textarea className={!this.state.editAsn ? "bgDisable inputBox width80per borderRadius4 m-top-10" : "inputBox width80per borderRadius4 m-top-10"} disabled={!this.state.editAsn} value={this.state.remarks} rows="2" id="remarks" onChange={this.handleChange} />
                                            </div>
                                        </div> */}
                                    </div>
                                </form>
                            </div> : null}

                            {/* -----------------------------------------------------------expanded Table----------------------------- */}

                            <div className="col-md-12 pad-0" id="expandedTable" >
                                {/* <SetLevelConfigHeader {...this.props} {...this.state} setLvlColoumSetting={this.state.setLvlColoumSetting} setLvlGetHeaderConfig={this.state.setLvlGetHeaderConfig} setLvlResetColumnConfirmation={(e) => this.setLvlResetColumnConfirmation(e)} setLvlOpenColoumSetting={(e) => this.setLvlOpenColoumSetting(e)} setLvlCloseColumn={(e) => this.setLvlCloseColumn(e)} setLvlPushColumnData={(e) => this.setLvlpushColumnData(e)} setLvlsaveColumnSetting={(e) => this.setLvlsaveColumnSetting(e)} />
                                 */}   
                                  <div className="gvst-inner">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th className="fix-action-btn"><label></label></th>
                                                {/* {this.state.setLvlCustomHeadersState.length == 0 ? this.state.setLvlGetHeaderConfig.map((data, key) => (
                                                    <th key={key}>
                                                        <label>{data}</label>
                                                    </th>
                                                )) : this.state.setLvlCustomHeadersState.map((data, key) => (
                                                    <th key={key}>
                                                        <label>{data}</label>
                                                    </th>
                                                ))} */}
                                                {this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            )) : this.props.setCustomHeadersState.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            ))}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.poDetailsShipment.length != 0 ?
                                                this.state.poDetailsShipment.poDetails.map((data, key) => (
                                                    <React.Fragment key={key}>
                                                        <tr>
                                                            <td className="fix-action-btn">
                                                                <ul className="table-item-list">
                                                                    {this.props.poType != "poicode" && <li className="til-inner" id={data.setBarCode} onClick={(e) => this.expandColumn(data.setBarCode, e, data)} ref={node => this.openExpand = node}> 
                                                                    {this.state.dropOpen && this.state.expandedId == data.setBarCode ? 
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                        <path fill="#a4b9dd" fillRule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"/>
                                                                    </svg>  : 
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                        <path fill="#a4b9dd" fillRule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                                                    </svg>}
                                                                    </li>}
                                                                </ul>
                                                            </td>
                                                            {/* {this.state.setLvlHeaderSummary.length == 0 ? this.state.setLvlDefaultHeaderMap.map((hdata, key) => (
                                                                <td key={key} >
                                                                    {hdata == "shipmentRequestedQty" || hdata == "requestQty" ? <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable" : ""} value={data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                        hdata == "shipmentPendingQty" || hdata == "totalPendingQty" ? <label>{data["pendingQty"]}</label> :
                                                                            hdata == "shipmentCancelledQty" || hdata == "cancelQty" ? <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable" : ""} value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[hdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                                <label>{data[hdata]}</label>}
                                                                </td>
                                                            )) : this.state.setLvlHeaderSummary.map((sdata, keyy) => (
                                                                <td key={keyy} >
                                                                    {sdata == "shipmentRequestedQty" || sdata == "requestQty" ? <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable" : ""} value={data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                        sdata == "shipmentPendingQty" || sdata == "totalPendingQty" ? <label>{data["pendingQty"]}</label> :
                                                                            sdata == "shipmentCancelledQty" || sdata == "cancelQty" ? <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable" : ""} value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[sdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                                <label>{data[sdata]}</label>}
                                                                </td>
                                                            ))} */}
                                                            {this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                                                <td key={key} >
                                                                {hdata == "shipmentRequestedQty" || hdata == "requestQty" ? <span className="table-per-tooptip"><input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable" : ""} value={data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]} id="requestedQty" className={data["percentFulfilFlag"] ? "errorBorder" : ""} onChange={(e) => this.handleQuantityChange(e, data)} onMouseOver={(e) => this.calculatedPercent(e,data)} placeholder={data["requestedQty"] != undefined ? (data["requestedQty"] == "" ? 0 : data["requestedQty"]) : (data[hdata] == "" ? 0 : data[hdata])} autoComplete="off"/><span className="generic-tooltip">{this.state.calculatedPercent} %</span></span> :
                                                                    hdata == "shipmentPendingQty" || hdata == "totalPendingQty" ? <label>{this.props.allowMultipleAsn ? data["pendingQty"] : 0 }</label> :
                                                                        hdata == "shipmentCancelledQty" || hdata == "cancelQty" ? (this.props.allowMultipleAsn ? <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable" : ""} value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[hdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data["cancelledQty"] != undefined ? (data["cancelledQty"] == "" ? 0 : data["cancelledQty"]) : (data[hdata] == "" ? 0 : data[hdata])} autoComplete="off"/> : <label>{data["cancelledQty"] != undefined ? data["pendingQty"] : data[hdata]}</label> ) :
                                                                            <label>{data[hdata]}</label>}
                                                            </td>
                                                            )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                                                <td key={keyy} >
                                                                    {sdata == "shipmentRequestedQty" || sdata == "requestQty" ? <span className="table-per-tooptip"><input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable" : ""} value={data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]} id="requestedQty" className={data["percentFulfilFlag"] ? "errorBorder" : ""} onChange={(e) => this.handleQuantityChange(e, data)} onMouseOver={(e) => this.calculatedPercent(e,data)} placeholder={data["requestedQty"] != undefined ? (data["requestedQty"] == "" ? 0 : data["requestedQty"]) : (data[sdata] == "" ? 0 : data[sdata])} autoComplete="off"/><span className="generic-tooltip">{this.state.calculatedPercent} %</span></span> :
                                                                        sdata == "shipmentPendingQty" || sdata == "totalPendingQty" ? <label>{this.props.allowMultipleAsn ? data["pendingQty"] : 0 }</label> :
                                                                            sdata == "shipmentCancelledQty" || sdata == "cancelQty"  ? (this.props.allowMultipleAsn ? <input type="text" disabled={!this.state.editAsn} className={!this.state.editAsn ? "bgDisable" : ""} value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[sdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data["cancelledQty"] != undefined ? (data["cancelledQty"] == "" ? 0 : data["cancelledQty"]) : (data[sdata] == "" ? 0 : data[sdata])} autoComplete="off"/> : <label>{data["cancelledQty"] != undefined ? data["pendingQty"] : data[hdata]}</label> ) :
                                                                                <label>{data[sdata]}</label>}
                                                                </td>
                                                            ))}  
                                                        </tr>
                                                        {this.state.dropOpen && this.state.expandedId == data.setBarCode ? <tr><td colSpan="100%" className="pad-0"> <ItemLevelDetails {...this.state}  {...this.props} item_Display={this.props.item_Display} /> </td></tr> : null}
                                                    </React.Fragment>
                                                )) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                        </tbody>
                                    </table>
                                </div>
                                {/* <div className="pagerDiv newPagination text-right justifyEnd alignMiddle displayFlex pad-top-35 expandPagination">
                                <Pagination {...this.props} {...this.state} page={this.page} />
                            </div> */}
                            </div>
                            {this.state.ppsUploadModal && <UpdateAsnUpload onFileUploadSubmit={this.onFileUploadSubmit} newPPSArray ={this.state.PPSArray} newReportArray = {this.state.ReportArray} newPackingArray={this.state.packingArray}  {...this.props} {...this.state} CancelPpsUploadModal={this.CancelPpsUploadModal} deleteUploads={this.deleteUploads} oldReportArray={this.state.oldReportFiles} oldPPSArray={this.state.oldPpsFiles} oldPackingArray={this.state.oldPackingFiles}/>}
                            {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
                            {/* -----------------------------------------------------------expanded Table End----------------------------- */}
                            {this.state.setLvlConfirmModal ? <SetLevelConfirmationModal {...this.state} {...this.props} setLvlCloseConfirmModal={(e) => this.setLvlCloseConfirmModal(e)} setLvlResetColumn={(e) => this.setLvlResetColumn(e)} /> : null}
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}