import React from 'react';
import Pagination from '../../../pagination';
import VendorFilter from '../../vendorFilter';
import ColoumSetting from "../../../replenishment/coloumSetting";
import ToastError from '../../../utils/toastError';
import ToastLoader from '../../../loaders/toastLoader';
import ConfirmationSummaryModal from "../../../replenishment/confirmationReset";
import Reload from '../../../../assets/refresh-block.svg';
import moment from 'moment';
import axios from 'axios';

class VendorLoginLogoutReport extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            totalItems: 0,
            selectedItems: 0,
            jumpPage: 1,
            isReportPageFlag: true, //For Hiding Set/Item Header in Column Setting::

            mainHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "VENDOR_LOGS_REPORTS",
                basedOn: "ALL"
            },

            filterItems: {},
            mainCustomHeaders: [],
            getMainHeaderConfig: [],
            mainAvailableHeaders: [],
            mainFixedHeader: [],
            mainCustomHeadersState: [],
            mainHeaderConfigState: {}, 
            mainFixedHeaderData: [],
            mainHeaderConfigDataState: {},
            mainHeaderSummary: [],
            mainDefaultHeaderMap: [],
            mandateHeaderMain: [],
            headerCondition: false,
            coloumSetting: false,
            dragOn: false,
            changesInMainHeaders: false,
            saveMainState: [],
            headerConfigDataState: {},
            confirmModal: false,
            headerMsg: "",
            tabVal: "1",

            filter: false,
            filterBar: false,
            filteredValue: [],
            checkedFilters: [],
            applyFilter: false,
            inputBoxEnable: false,
            tagState: false,
            type: 1,
            filterValueForTag: [],
            filterKey: "",
            filterType: "",
            fromCreationDate: "start date",
            toCreationDate: "end date",
            filterNameForDate: "",
            fromCredit: "",
            toCredit: "",
            fromDebit: "",
            toDebit: "",
            fromBalance: "",
            toBalance: "",

            toastMsg: "",
            toastLoader: false,
            toastError: false,
            search: "",   
            exportToExcel: false,
            downloadReport: false,
            checkedData: [],
            selectAll: false,
            toastErrorMsg : "",

            //UPPER FILTER:::
            upperFilter: false,
            upperFilterFromDate: "",
            upperFilterToDate: "",

            filterOption: {
                slName: [],
            },
            searchSlNameFilter: "",
            filterData: [],
            openSlNameFilter: false,

            arsCurrentPage: 0,
            arsMaxPage: 0,
            noOfAppliedFilter: 0,
            //------------------------//

            upperFilterPayload: {},

            //QUICK FILTER
            // quickFilter: false,
            // quickFilterApplied: false,
            // quickFilterData: [],
            // checkedFiltersData: {}
        }
    }

    componentDidMount() {
        let payload = {
            pageNo: 1,
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "",
            filter: "",
            viewType: "Self"
        }
        this.props.getVendorLogsRequest(payload);

        if (!this.props.replenishment.getMainHeaderConfig.isSuccess) {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
        }
        // this.props.quickFilterGetRequest({
        //     name: "VENDOR_LOGS_REPORTS",
        //     filterName: ""
        // });
        document.addEventListener("keydown", this.escFunction, false);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.orders.getVendorLogs.isSuccess) {
            return{
                tableData: nextProps.orders.getVendorLogs.data.resource == null ? [] : nextProps.orders.getVendorLogs.data.resource,
                prev: nextProps.orders.getVendorLogs.data.prePage,
                current: nextProps.orders.getVendorLogs.data.currPage,
                next: nextProps.orders.getVendorLogs.data.currPage + 1,
                maxPage: nextProps.orders.getVendorLogs.data.maxPage,
                totalItems: nextProps.orders.getVendorLogs.data.statusCount,
                selectedItems: nextProps.orders.getVendorLogs.data.resultedDataCount == undefined || nextProps.orders.getVendorLogs.data.resultedDataCount == null 
                               ? 0 : nextProps.orders.getVendorLogs.data.resultedDataCount,
                jumpPage: nextProps.orders.getVendorLogs.data.currPage
            }
        }
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
                nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
                let mainFixedHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]) : []
                let mainCustomHeadersState = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : []
                let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ? 
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                return {
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    mainCustomHeaders: prevState.headerCondition ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] : {},
                    getMainHeaderConfig,
                    mainAvailableHeaders,
                    mainFixedHeader,
                    mainCustomHeadersState,
                    mainHeaderConfigState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : {},
                    mainFixedHeaderData: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
                    mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
                    mainHeaderSummary: nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : [],
                    mainDefaultHeaderMap: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderMain: Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Mandate Headers"])
                };
            }
        }
        // if (nextProps.replenishment.quickFilterGet.isSuccess) {
        //     if (nextProps.replenishment.quickFilterGet.data.resource !== null) {
        //         return {
        //             quickFilterData: nextProps.replenishment.quickFilterGet.data.resource
        //         };
        //     }
        // }
        return null;
    }

    componentDidUpdate(){
        if (this.props.orders.getVendorLogs.isSuccess) {
            this.props.getVendorLogsClear();
        }
        if (this.props.replenishment.getMainHeaderConfig.isSuccess) {
            if (this.props.replenishment.getMainHeaderConfig.data.resource != null && this.props.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                setTimeout(() => {
                    this.setState({
                        headerCondition: this.state.mainCustomHeadersState.length == 0 ? true : false,
                    })
                }, 1000);

                // view email redirect logic here:::
                let queryParam = sessionStorage.getItem('login_redirect_queryParam') == null || sessionStorage.getItem('login_redirect_queryParam') == undefined ? null :
                    Object.keys(sessionStorage.getItem('login_redirect_queryParam')).length ? JSON.parse(sessionStorage.getItem('login_redirect_queryParam')) : null;
                let filterValue = "";
                let filterKeyValue = "";
                if (queryParam !== null && queryParam !== undefined && Object.keys(queryParam).length != 0) {
                    var allHeaders = this.props.replenishment.getMainHeaderConfig.data.resource["Default Headers"]
                    filterValue = Object.keys(queryParam).map((_) => { return allHeaders[_] });
                    let flag = false;
                    filterValue.map(data => { if (data == undefined) flag = true; });
                    filterKeyValue = Object.values(queryParam);
                    if (!flag && filterValue.length > 0 && filterKeyValue.length > 0 && filterValue.length == filterKeyValue.length) {
                        setTimeout(() => {
                            let payload = {
                                pageNo: 1,
                                type: 2,
                                search: "",
                                sortedBy: "",
                                sortedIn: "",
                                filter: queryParam,
                                viewType: "Self"
                            }
                            this.props.getVendorLogsRequest(payload)
                        }, 500);

                        this.setState({
                            filteredValue: Object.keys(queryParam).length ? queryParam : {},
                            checkedFilters: Object.keys(queryParam).length ? filterValue : [],
                            applyFilter: Object.keys(queryParam).length ? true : false,
                            inputBoxEnable: Object.keys(queryParam).length ? true : false,
                            tagState: true,
                            //[filterValue] : queryParam.shipmentAdviceCode,
                            type: 2,
                            filterValueForTag: Object.keys(queryParam).length ? queryParam : {},

                        }, () => {
                            filterValue.map((data, index) => {
                                this.setState({ [data]: filterKeyValue[index] })
                            })
                        })
                    }
                }
            }
            this.props.getMainHeaderConfigClear()
        }
        if (this.props.replenishment.createMainHeaderConfig.isSuccess && this.props.replenishment.createMainHeaderConfig.data.basedOn == "ALL") {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            this.props.createMainHeaderConfigClear();
        }
        // if (this.props.replenishment.quickFilterGet.isSuccess) {
        //     this.props.quickFilterGetClear();
        // }
        // if (this.props.replenishment.quickFilterSave.isSuccess) {
        //     this.props.quickFilterGetRequest({
        //         name: "VENDOR_LOGS_REPORTS",
        //         filterName: ""
        //     });
        //     this.props.quickFilterSaveClear();
        // }
        // if (this.props.vendor.getAllManageVendor.isSuccess) {
        //     this.setState({
        //         filterData: this.props.vendor.getAllManageVendor.data.resource == null ? [] : 
        //                     this.props.vendor.getAllManageVendor.data.resource.filter((v,i,a)=>a.findIndex(t=> (t.slName === v.slName))===i),
        //         arsCurrentPage: this.props.vendor.getAllManageVendor.data.currPage,
        //         arsMaxPage: this.props.vendor.getAllManageVendor.data.maxPage
        //     })
        //     this.props.getAllManageVendorClear();
        // }
    }

    openDownloadReport(e) {
        e.preventDefault();
        this.setState({
            downloadReport: !this.state.downloadReport
        }, () => document.addEventListener('click', this.closeDownloadReport));
    }
    closeDownloadReport = () => {
        this.setState({ downloadReport: false }, () => {
            document.removeEventListener('click', this.closeDownloadReport);
        });
    }

    openFilter =(e)=> {
        e.preventDefault();
        Object.keys(this.state.upperFilterPayload).length > 0 && this.clearUpperFilter(e)
        this.setState({filter: !this.state.filter, filterBar: !this.state.filterBar, upperFilter: false},()=>{
            document.addEventListener("click", this.closeFilterOnClick)
        });
    }
    closeFilter =(e)=> {
        e.preventDefault();
        this.setState({ filter: false, filterBar: false })
    }

    // openQuickFilter(e) {
    //     // e.preventDefault();
    //     this.setState({
    //         quickFilter: !this.state.quickFilter
    //     }, () => document.addEventListener('click', this.closeQuickFilter));
    // }
    // closeQuickFilter = () => {
    //     this.setState({ quickFilter: false }, () => {
    //         document.removeEventListener('click', this.closeQuickFilter);
    //     });
    // }

    onSearch = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                let payload = {
                    pageNo: 1,
                    type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                    search: e.target.value,
                    filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    viewType: "Self"
                }
                this.props.getVendorLogsRequest(payload)
                this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3 })
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
                let payload = {
                    pageNo: 1,
                    type: this.state.type == 4 ? 2 : 1,
                    search: "",
                    filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    viewType: "Self"
                }
                this.props.getVendorLogsRequest(payload)
                this.setState({ search: "", type: this.state.type == 4 ? 2 : 1 })
            }
        })
    }

    searchClear =()=> {
        if (this.state.type === 3 || this.state.type == 4) {
            let payload = {
                pageNo: 1,
                type: this.state.type == 4 ? 2 : 1,
                search: "",
                filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                viewType: "Self"
            }
            this.props.getVendorLogsRequest(payload)
            this.setState({ search: "", type: this.state.type == 4 ? 2 : 1 })
        } else { this.setState({ search: "" }) }
    }

    page =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.orders.getVendorLogs.data.prePage,
                    current: this.props.orders.getVendorLogs.data.currPage,
                    next: this.props.orders.getVendorLogs.data.currPage + 1,
                    maxPage: this.props.orders.getVendorLogs.data.maxPage,
                })
                if (this.props.orders.getVendorLogs.data.currPage != 0) {
                    let payload = {
                        pageNo: this.props.orders.getVendorLogs.data.currPage - 1,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        viewType: "Self"
                    }
                    this.props.getVendorLogsRequest(payload)
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.orders.getVendorLogs.data.prePage,
                current: this.props.orders.getVendorLogs.data.currPage,
                next: this.props.orders.getVendorLogs.data.currPage + 1,
                maxPage: this.props.orders.getVendorLogs.data.maxPage,
            })
            if (this.props.orders.getVendorLogs.data.currPage != this.props.orders.getVendorLogs.data.maxPage) {
                let payload = {
                    pageNo: this.props.orders.getVendorLogs.data.currPage + 1,
                    type: this.state.type,
                    search: this.state.search,
                    filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    viewType: "Self"
                }
                this.props.getVendorLogsRequest(payload)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.orders.getVendorLogs.data.prePage,
                    current: this.props.orders.getVendorLogs.data.currPage,
                    next: this.props.orders.getVendorLogs.data.currPage + 1,
                    maxPage: this.props.orders.getVendorLogs.data.maxPage,
                })
                if (this.props.orders.getVendorLogs.data.currPage <= this.props.orders.getVendorLogs.data.maxPage) {
                    let payload = {
                        pageNo: 1,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        viewType: "Self"
                    }
                    this.props.getVendorLogsRequest(payload)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.orders.getVendorLogs.data.prePage,
                    current: this.props.orders.getVendorLogs.data.currPage,
                    next: this.props.orders.getVendorLogs.data.currPage + 1,
                    maxPage: this.props.orders.getVendorLogs.data.maxPage,
                })
                if (this.props.orders.getVendorLogs.data.currPage <= this.props.orders.getVendorLogs.data.maxPage) {
                    let payload = {
                        pageNo: this.props.orders.getVendorLogs.data.maxPage,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        viewType: "Self"
                    }
                    this.props.getVendorLogsRequest(payload)
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        viewType: "Self"
                    }
                    this.props.getVendorLogsRequest(payload)
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    xlscsv =()=> {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let payload = {
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            viewType: "Self"
        }
        let selectAllFlag = this.state.selectAll ? true : false;
        let response = ""
        axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=VENDOR_LOGS_REPORTS&isAllData=false&isOnlyCurrentPage=${selectAllFlag}`, payload, { headers: headers })
            .then(res => {
                response = res
                window.location.href = res.data.data.resource;
            }).catch((error) => {
                this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
                setTimeout(()=>{
                    this.setState({
                        toastError: false
                    })
                }, 5000)
            });

    }

    filterHeader = (event) => {
        var data = event.target.dataset.key 
        if( event.target.closest("th").classList.contains("rotate180"))
            event.target.closest("th").classList.remove("rotate180")
        else
            event.target.closest("th").classList.add("rotate180") 
        var def = {...this.state.mainHeaderConfigDataState};
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ filterKey, filterType: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            let payload = {
                pageNo: this.state.current,
                type: this.state.type,
                search: this.state.search,
                filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                viewType: "Self"
            }
            this.props.getVendorLogsRequest(payload)
        })
    }

    small = (str) => {
        if (str != null) {
            var str = str.toString()
            if (str.length <= 45) {
                return false;
            }
            return true;
        }
    }

    closeToastError = () => {
        this.setState({ toastError: false })
    }

    closeFilterOnClick =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")){
            this.setState({ 
                filter: false,
                filterBar: false
            },()=>{
                document.removeEventListener('click', this.closeFilterOnClick)
            })
        }
    }

    clearFilter = () => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                pageNo: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                filter: "",
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                viewType: "Self"
            }
            this.props.getVendorLogsRequest(payload)
        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            filterValueForTag: [],
            fromCredit: "",
            toCredit: "",
            fromDebit: "",
            toCredit: "",
            fromBalance: "",
            toBalance: "",
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
        sessionStorage.setItem('login_redirect_queryParam', "")   
    }

    clearTag=(e,index)=>{
        let deleteItem = this.state.checkedFilters;
        let deletedItem = this.state.checkedFilters[index];
        deleteItem.splice(index,1)
        this.setState({
           checkedFilters:deleteItem,
           [deletedItem]: "",
        },()=>{
            if( this.state.checkedFilters.length == 0 )
                this.clearFilter();
            else
                this.submitFilter();
        })
    }
    clearAllTag=(e)=>{
        this.setState({
            checkedFilters:[],
      },()=>{
            this.clearFilter();
            this.clearFilterOutside();
        })
    }
 
   clearFilterOutside=()=>{
         this.setState({
             filteredValue:[],
             checkedFilters:[]
         })
    }

    submitFilter = () => {
        let payload = {}
        let filtervalues =  {}
        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))

        //reset min and max value field that are not checked::
        if (payload.credit === undefined) {
            this.setState({ fromCredit: "", toCredit: "" })
        }
        if (payload.debit === undefined) {
            this.setState({ fromDebit: "", toDebit: "" })
        }
        if (payload.balance === undefined) {
            this.setState({ fromBalance: "", toBalance: "" })
        }
       
        // Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data] + "T00:00+05:30")))
        Object.keys(payload).map((data) => ((data.includes("Date") || data.includes("validFromDate") || data.includes("validToDate"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim()+ "T00:00+05:30", to: payload[data].split("|")[1].trim()+ "T00:00+05:30" })))
        Object.keys(payload).map((data) => (data.includes("Qty") || data.includes("poAmount") || data.includes("credit") || data.includes("debit") || data.includes("balance"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim() }))
        Object.keys(payload).map((data) => (data.includes("Time"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim()}))     
    
       
        //for handling to and from value on UI level::    
        this.state.checkedFilters.map((data) => (filtervalues[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(filtervalues).map((data) => (data.includes("Qty") || data.includes("poAmount") || data.includes("credit") || data.includes("debit") || data.includes("balance"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: filtervalues[data].split("|")[0].trim(), to: filtervalues[data].split("|")[1].trim()}))
        Object.keys(filtervalues).map((data) => (data.includes("Date") || data.includes("validFromDate") || data.includes("validToDate"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: moment(filtervalues[data].split("|")[0].trim()).format("DD-MM-YYYY"), to: moment(filtervalues[data].split("|")[1].trim()).format('DD-MM-YYYY') }))
        Object.keys(filtervalues).map((data) => ((data.includes("Time"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: moment(filtervalues[data].split("|")[0].trim()).format("DD-MM-YYYY HH:mm"), to: moment(filtervalues[data].split("|")[1].trim()).format("DD-MM-YYYY HH:mm") }))) 
      
        let data = {
            pageNo: 1,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            search: this.state.search,
            filter: payload,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            viewType: "Self"
        }
        this.props.getVendorLogsRequest(data)

        this.setState({
            filter: false,
            filteredValue: payload,
            filterValueForTag: filtervalues,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            tagState: true
        })
    }
   
    handleInput = (event, filterName) => {
           
        if (event != undefined && event.length != undefined) {
            this.setState({
                fromCreationDate: moment(event[0]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                toCreationDate: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            if(filterName == "Creation Time"){
                this.setState({
                    fromCreationDate: moment(event[0]._d).format(),
                    filterNameForDate: filterName
                }, () => this.handleFromAndToValue(event))  
                this.setState({
                    toCreationDate: moment(event[1]._d).format(),
                    filterNameForDate: filterName
                }, () => this.handleFromAndToValue(event))
            } 
        }
        else if( event != null ){ 
            let id = event.target.id;
            let value = event.target.value;
            if (event.target.id === "fromcredit"){
                this.setState({ fromCredit: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromCredit);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "tocredit") {
                this.setState({ toCredit: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toCredit);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "fromdebit") {
                this.setState({ fromDebit: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromDebit);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "todebit") {
                this.setState({ toDebit: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toDebit);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "frombalance") {
                this.setState({ fromBalance: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromBalance);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "tobalance") {
                this.setState({ toBalance: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toBalance);
                    this.handleFromAndToValue(event)
                })
            }
            else
                this.handleFromAndToValue(event);
        }
    }
    
    handleFromAndToValue = () => {
        var value = "";
        var name = event.target.dataset.value;
        if (name == undefined) {
            value = this.state.fromCreationDate + " | " + this.state.toCreationDate
            name = this.state.filterNameForDate;
        }
        else{    
            if (event.target.id === "fromcredit" || event.target.id === "tocredit")
                value = this.state.fromCredit + " | " + this.state.toCredit
            else if (event.target.id === "fromdebit" || event.target.id === "todebit")
                value = this.state.fromDebit + " | " + this.state.toDebit
            else if (event.target.id === "frombalance" || event.target.id === "tobalance")
                value = this.state.fromBalance + " | " + this.state.toBalance    
            else
                value = event.target.value
        }

        if( value === " | " || value === "| " || value === " |")
           value = "";

        if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
        }

        this.setState({ [name]: value, applyFilter: true }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                //this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
                this.setState({ applyFilter: true })
            }
            this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
        })
    }

    handleCheckedItems = (e, data) => {
        if(data == "onlyToSetState"){
            this.populateFilterValues()
        }
        else {
            let array = [...this.state.checkedFilters]
            let len = Object.values(this.state.filterValueForTag).length > 0;
            if (this.state.checkedFilters.some((item) => item == data)) {
                array = array.filter((item) => item != data)
                delete this.state.filterValueForTag[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)]
                this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = "";
                this.setState({ [data]: ""})
                let flag = array.some(data => this.state[data] == "" || this.state[data] == undefined)
                if(!flag && len){
                    this.state.checkedFilters.some( (item,index) => {
                        if( item == data){
                            this.clearTag(e, index)
                        }
                    })
                }           
                    
            } else {
                array.push(data)
            }
            var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
            // this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
            this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true,  quickFilterApplied: false}, () => {
                this.populateFilterValues()
            })
        }
    }

    handleInputBoxEnable = (e, data) => {
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }

    openColoumSetting =(data)=> {
        if (this.state.tabVal == 1) {
            if (this.state.mainCustomHeadersState.length == 0) {
                this.setState({
                    headerCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
    }

    closeColumnSetting = (e) => {
        if (e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
           this.setState({ coloumSetting: false }, () =>
               document.removeEventListener('click', this.closeColumnSetting))
       } 
    }

    handleDragStart =(e, key, dragItem, dragNode)=> {
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', this.handleDragEnd(dragItem, dragNode))
        dragItem.current = key;
        this.setState({ dragOn:true })
    }
    handleDragEnd =(dragItem, dragNode)=> {
        dragNode.current.removeEventListener('dragend', this.HandleDragEnd)
        this.setState({ dragOn:false})
        dragItem.current = null
        dragNode.current = null
    }
    handleDragEnter =(e, key, dragItem, dragNode)=> {
        const currentItem = dragItem.current
        if (e.target !== dragItem.current) {
            if (this.state.tabVal == 1) {
                if (this.state.headerCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainDefaultHeaderMap
                        let newMainHeaderConfigList = prevState.getMainHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newMainHeaderConfigList.splice(key, 0, newMainHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newMainHeaderConfigList))
                        let configheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true,  
                            mainCustomHeaders: configheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newMainHeaderConfigList,
                        }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainHeaderSummary
                        let newCustomList = prevState.mainCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomList.splice(key, 0, newCustomList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newCustomList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true, 
                            mainCustomHeaders: customheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newCustomList
                        }
                    })
                }
            }
        }
    }
    pushColumnData =(e,data)=> {
        if (this.state.tabVal == 1) {
            e.preventDefault();
            let getHeaderConfig = this.state.getMainHeaderConfig
            let customHeadersState = this.state.mainCustomHeadersState
            let headerConfigDataState = this.state.mainHeaderConfigDataState
            let customHeaders = this.state.mainCustomHeaders
            let saveState = this.state.saveMainState
            let defaultHeaderMap = this.state.mainDefaultHeaderMap
            let headerSummary = this.state.mainHeaderSummary
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (this.state.headerCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig = getHeaderConfig
                    getHeaderConfig.push(data)
                    var even = (_.remove(mainAvailableHeaders), function (n) {
                        return n == data
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
               if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(mainAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeadersState: customHeadersState,
                mainCustomHeaders: customHeaders,
                saveMainState: saveState,
                mainDefaultHeaderMap: defaultHeaderMap,
                mainHeaderSummary: headerSummary,
                mainAvailableHeaders,
                changesInMainHeaders: true,
                headerConfigDataState: customHeadersState,
            })
        }
    }
    closeColumn =(data)=> {
        if (this.state.tabVal == 1) {
            let getHeaderConfig = this.state.getMainHeaderConfig
            let headerConfigState = this.state.mainHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.mainCustomHeadersState
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.mainCustomHeadersState.length == 0) {
                    this.setState({
                        headerCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            mainAvailableHeaders.push(data)
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeaders: headerConfigState,
                mainCustomHeadersState: customHeadersState,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.mainFixedHeaderData))[data];
                let saveState = this.state.saveMainState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.mainHeaderSummary
                let defaultHeaderMap = this.state.mainDefaultHeaderMap
                if (!this.state.headerCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    mainHeaderSummary: headerSummary,
                    mainDefaultHeaderMap: defaultHeaderMap,
                    saveMainState: saveState
                })
            }, 100);
        }
    }
    saveColumnSetting =(e)=> {
        if (this.state.tabVal == 1) {
            this.setState({
                coloumSetting: false,
                headerCondition: false,
                saveMainState: [],
                changesInMainHeaders: false,
            })
            let payload = {//CHANGE
                basedOn: "ALL",
                module: "VENDOR LOGS",
                subModule: "VENDOR LOGS",
                section: "VENDOR LOGS",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "VENDOR_LOGS_REPORTS",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainCustomHeaders,
            }
            this.props.createMainHeaderConfigRequest(payload)
        }
    }
    resetColumnConfirmation =()=> {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            confirmModal: true,
        })
    }
    resetColumn =()=> {//CHANGE
        const {getMainHeaderConfig,mainFixedHeader} = this.state
        if (this.state.tabVal == 1) {
            let payload = {
                basedOn: "ALL",
                module: "VENDOR LOGS",
                subModule: "VENDOR LOGS",
                section: "VENDOR LOGS",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "VENDOR_LOGS_REPORTS",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainHeaderConfigDataState,
            }
            this.props.createMainHeaderConfigRequest(payload)
            let availableHeaders= mainFixedHeader.filter(function (obj) { return getMainHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                headerCondition: true,
                coloumSetting: false,
                saveMainState: [],
                confirmModal: false,
                mainAvailableHeaders:availableHeaders
            })    
        }
    }
    closeConfirmModal =(e)=> {
        this.setState({ confirmModal: !this.state.confirmModal,})
    }
    onHeadersTabClick = (tabVal) => {
        this.setState({ tabVal: tabVal }, () => {
            if (tabVal === 1) {
                this.openColoumSetting("true")
                this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            }
        })
    }

    handleCheckedData =(e, data)=>{
        let array = [...this.state.checkedData]
        if (e.target.name == "selectEach" ) {
            let deletedItem = {
                balance: data.balance, creationTime: data.creationTime, credit: data.credit, debit: data.debit,
                id: data.id, particulars: data.particulars, slCode: data.slCode, slName: data.slName, transactionDate: data.transactionDate
            }
            if (this.state.checkedData.some((item) => item.id == data.id)) {
                array = array.filter((item) => item.id != data.id)
            } else {
                array.push(deletedItem)
            }
            if( array.length === this.state.tableData.length)
               this.setState({ checkedData: array, selectAll: true })
            else    
               this.setState({ checkedData: array, selectAll: false })
        }
    }

    selectAllAction =(e)=>{
        this.setState({ selectAll: !this.state.selectAll, checkedData: [] },()=>{
            if( this.state.selectAll ){
               let array = []; 
               this.state.tableData.map( data =>{
                    data.balance = data.balance;
                    data.creationTime = data.creationTime;
                    data.credit = data.credit;
                    data.debit = data.debit;
                    data.id = data.id; 
                    data.particulars = data.particulars;
                    data.slCode = data.slCode;
                    data.slName = data.slName; 
                    data.transactionDate = data.transactionDate;  

                    array.push(data);
               })
               this.setState({ checkedData: array })
            }
        })
        this.closingAllModal();
    }

    closingAllModal = () => {
        this.setState({
            filter: false, exportToExcel: false, filterBar: false,
            downloadReport: false,
            headerCondition: false, coloumSetting: false,
            confirmModal: false
        },()=>{
            document.removeEventListener('click', this.closeExportToExcel)
            document.removeEventListener('click', this.closeDownloadReport)
            document.removeEventListener('click', this.closeFilterOnClick)
            document.removeEventListener('click', this.closeColumnSetting)
        })
    }

    onRefresh = () => {
        this.state.checkedFilters.map((data) => this.setState({ [data]: "" }))
         let payload = {
            pageNo: 1,
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "",
            filter: "",
            viewType: "Self"
        }
        this.props.getVendorLogsRequest(payload)
        if (document.getElementsByClassName('rotate180')[0] != undefined) {
            document.getElementsByClassName('rotate180')[0].classList.remove('rotate180')
        }
        this.setState({ filteredValue: [], selectAll: false, checkedFilters: [], checkedData: [] })
        this.closingAllModal();
        this.clearAllTag();
    }

    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.closingAllModal();
        }
    }

    onSearchFilter =(e, searchFilterVariable, flag) => {
        let keyCode = e.keyCode
        this.state.filterOption.slName = []
        this.setState({ 
            [searchFilterVariable]: e.target.value,
            [flag]: false,
            filterData: []
        }, () => {
            if( this.state[searchFilterVariable].trim().length > 0 && keyCode == 13){
                let payload = {
                    pageNo: 1,
                    type: this.state.searchSlNameFilter !== "" ? 3 : 1,
                    search: this.state.searchSlNameFilter,
                    sortedBy: "",
                    sortedIn: "",
                    filter: {},
                    key: "",
                }
                this.props.getAllManageVendorRequest(payload)
                this.setState({
                    [flag]: true,
                })
            }
        })
        document.addEventListener('click', this.onClickCloseArsFilter)
    }

    onSearchFilterClear =(e, searchFilterVariable, flag, filterTypeArray)=> {
        this.state.filterOption[filterTypeArray] = []
        let payload = {
            pageNo: 1,
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "",
            filter: {},
            key: "",
        }
        this.props.getAllManageVendorRequest(payload)
        this.setState({
            [searchFilterVariable]: "",
            [flag]: false,
        })
    }

    handleChange =(e, key)=> {
        e.persist();
        let value = e.target.closest("li").getAttribute("value");
        const objectFilterOption = this.state.filterOption;
        if(objectFilterOption[key].indexOf(value) == -1) {
            objectFilterOption[key].pop();
            objectFilterOption[key].push(value);
            this.setState({
                filterOption: objectFilterOption,
            })
        } else {
            objectFilterOption[key].splice(objectFilterOption[key].indexOf(value), 1);
            this.setState({
                filterOption: objectFilterOption,
            })
        }
    }

    applyFilter =(e)=>{
        let vendorCode = Object.values(this.state.filterData).filter((k) => k.slName == this.state.filterOption["slName"][0])
        if( this.state.upperFilterFromDate !== "" && this.state.upperFilterToDate !== "" && vendorCode.length > 0){
            let lenS = this.state.search.length
            let lenF = Object.keys(this.state.filterOption).filter((k) => this.state.filterOption[k].length > 0).length

            if( this.state.upperFilterFromDate !== "" && this.state.upperFilterToDate !== "")
               this.setState({ noOfAppliedFilter : (lenF + 1)})
            else  
               this.setState({ noOfAppliedFilter : lenF})  
    
            let payload ={
                vendorCode : vendorCode[0].slCode,
                dateFrom : this.state.upperFilterFromDate,
                dateTo : this.state.upperFilterToDate
            }
            this.props.getLedgerFilterReportRequest(payload)
            this.onCloseArsFilter();

            let filteredPayload ={
                slCode: vendorCode[0].slCode,
                transactionDate: {
                    from: this.state.upperFilterFromDate + "T00:00+05:30",
                    to: this.state.upperFilterToDate + "T00:00+05:30"
                }
            }
            this.setState({ upperFilterPayload: filteredPayload, type: this.state.type == 3 || this.state.type == 4 ? 4 : 2})
        }
        else{
            this.setState({
                toastMsg: "All fields are mandatory",
                toastLoader: true,
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 7000)
        }    
    }

    removeFilter =(e)=> {
        e.persist();
        const objectFilterOption = this.state.filterOption;
        Object.keys(objectFilterOption).forEach(v => objectFilterOption[v] = []);
        let lenS = this.state.search.length
        let lenF = this.state.filterOption.length
        this.setState({
            filterOption: objectFilterOption,
            noOfAppliedFilter: 0,
            type: lenS && lenF ? 4 : lenS ? 3 : lenF ? 2 : 1,
        }, () => {  
            let payload = {
                pageNo: 1,
                type: lenS && lenF ? 4 : lenS ? 3 : lenF ? 2 : 1,
                search: this.state.search,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: {},
            }
            this.props.getAllLedgerReportRequest(payload)
        })
    }

    clearUpperFilter =(e)=>{
        this.state.filterOption.slName = []

        this.setState({
            searchSlNameFilter: "",
            upperFilterFromDate: "",
            upperFilterToDate: "",
            noOfAppliedFilter: 0,
            upperFilterPayload: {},
        })
        this.onCloseArsFilter();
        this.removeFilter(e)
    }

    resetFilterData =(payload)=>{
        this.setState({ filterData: []},()=>{
            this.props.getAllManageVendorRequest(payload)
        })
    }

    // handleQuickFilterClick = (e, data) => {
    //     e.preventDefault();
    //     let selectedFilterData = {}
    //     console.log(this.state.quickFilterData, data);
    //     this.state.quickFilterData.map((data2, key2) => {
    //         if(data == key2){
    //             selectedFilterData = data2.filterValue
    //         }
    //     })

    //     Object.keys(selectedFilterData).map((data4, key4) => {
    //         this.setState({
    //             [this.state.filterItems[data4]]: selectedFilterData[data4]
    //         })
    //     })

    //     // let payload = {
    //     //     no: 1,
    //     //     type: 2,
    //     //     search: "",
    //     //     status: "APPROVED",
    //     //     poDate: "",
    //     //     createdOn: "",
    //     //     poNumber: "",
    //     //     vendorName: "",
    //     //     userType: userType,
    //     //     isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
    //     //     filter: selectedFilterData,
    //     //     filterItems: { ...this.props.location.filterItems }
    //     // }
    //     // this.props.EnterpriseApprovedPoRequest(payload)



    //     let checkedFilters = Object.keys(selectedFilterData).map(key => this.state.filterItems[key]);
    //     this.setState({
    //         quickFilterApplied: true,
    //         checkedFilters: checkedFilters,
    //         filteredValue: Object.values(selectedFilterData),
    //         tagState: true,
    //         type: 2
    //     }, () => {
    //         this.submitFilter()
    //     })
    // }

    // populateFilterValues = () => {
    //     console.log("populate");
    //     let array = _.cloneDeep(this.state.checkedFilters)
    //     let payload = {}

    //     if(this.state.quickFilterApplied) {
    //         array.map((data) => {
    //             return (payload[Object.keys(this.state.filterItems).find(key => key === data)] = this.state[data])
    //         })
    //     }
    //     else{
    //         array.map((data) => {
    //             return (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data])
    //         })
    //     }
    //     // Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data] == "" ? "" : payload[data] + "T00:00+05:30")))
    //     Object.keys(payload).map((data) => ((data.includes("poQuantity") || data.includes("poAmount") || data.includes("indentDate") || data.includes("createdTime") || data.includes("updationTime"))
    //         && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim() })))
    //         console.log(payload);
    //         this.setState({checkedFiltersData: payload })
    // }

    // saveAndExit = (newFilter, filterNameEntered, selectedExistingFilter) => {
    //     let selectedFilterData = {}
    //     this.state.quickFilterData != undefined ? this.state.quickFilterData.length != 0 ? this.state.quickFilterData.map((data2, key2) => (
    //         data2.filterName == selectedExistingFilter ? 
    //             selectedFilterData = data2.filterValue: null
    //     )): null : null

    //     let payload = {}
    //     payload = {
    //         "module":"VENDOR LOGS",
    //         "subModule": "VENDOR LOGS",
    //         "section":"VENDOR LOGS",
    //         "attributeType": "VENDOR_LOGS_REPORTS",
    //         "filterName": newFilter ? filterNameEntered : selectedExistingFilter,
    //         "filterValue": newFilter ? this.state.checkedFiltersData : selectedFilterData
    //     }
    //     this.props.quickFilterSaveRequest(payload);
    // }

    render() {
        const{tableData, search,filterOption, noOfAppliedFilter} = this.state
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47 m-top-20">
                    <div className="otb-reports-content">
                        <div className="otbrc-left">
                            <div className="otbrc-search">
                                <input type="search" value={this.state.search} onChange={this.onSearch} onKeyDown={this.onSearch} placeholder="Type To Search" />
                                <img className="search-image" src={require('../../../../assets/searchicon.svg')} />
                                {search != "" ? <span className="closeSearch"><img src={require('../../../../assets/clearSearch.svg')} onClick={this.searchClear} /></span> : null}
                            </div>
                            {/* <div className="quick-filter">
                                <button id = "quickFilterId" type="button" className="qf-btn" onClick={() => this.openQuickFilter()}>
                                    <span className="qf-img">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15.728" height="15.737" viewBox="0 0 15.728 15.737">
                                            <g id="quick" transform="translate(-26.528 -26.5)">
                                                <path id="Path_1541" data-name="Path 1541" d="M34.388,26.5a7.849,7.849,0,0,0-5.565,13.394L27.356,41.38a.5.5,0,0,0,.355.857h6.677a7.869,7.869,0,1,0,0-15.737ZM37.6,34.339l-4.949,4.952a.248.248,0,0,1-.268.1.208.208,0,0,1-.164-.167.358.358,0,0,1,0-.157c.368-1.252,1.249-4.229,1.249-4.229H31.374a.248.248,0,0,1-.261-.144.241.241,0,0,1,.084-.278l4.939-4.969a.244.244,0,0,1,.261-.1.208.208,0,0,1,.161.171.368.368,0,0,1,0,.157c-.3,1-1.249,4.229-1.249,4.229h2.133a.244.244,0,0,1,.261.147.241.241,0,0,1-.107.288Z" fill="#12203c" />
                                            </g>
                                        </svg>
                                    </span>
                                        Quick Filters
                                        <span className="qf-down-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.293" height="8.147" viewBox="0 0 14.293 8.147">
                                            <path fill="none" stroke="#12203c" strokeWidth="2px" strokeLinejoin="round" strokeLinecap="round" id="prefix__Path_524" d="M6 9l5.732 5.732L17.465 9" data-name="Path 524" transform="translate(-4.586 -7.586)" />
                                        </svg>
                                    </span>
                                </button>
                                {this.state.quickFilter && <div className="qf-drop">
                                    <ul>
                                        {
                                            this.state.quickFilterData != undefined ? this.state.quickFilterData.length != 0 ? this.state.quickFilterData.map((data2, key2) => (
                                                <li key = {key2} onClick = {(e) => this.handleQuickFilterClick(e, key2)}>
                                                    <h3>{data2.filterName}</h3>
                                                    <p>{Object.keys(data2.filterValue).join(", ")}</p>
                                                    <span>{data2.time}</span>
                                                </li>
                                            )): <li>No Data Found</li>: <li>No Data Found</li>
                                        }
                                    </ul>
                                </div>}
                            </div> */}
                        </div>
                        <div className="otbrc-right">
                            <button type="button" className="otbrc-email">Email
                                <span>
                                    <img src={require('../../../../assets/email.svg')} />
                                </span>
                            </button>
                            <div className="gvpd-download-drop">
                                <button type="button" className="otbrc-down-report" onClick={(e) => this.openDownloadReport(e)}>Download Report
                                    <span className="otbrcdr-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </span>
                                </button>
                                {this.state.downloadReport &&
                                <ul className="pi-history-download">
                                    <li>
                                        <button className="export-excel" type="button" onClick={() => this.xlscsv()}>
                                            <span className="pi-export-svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                    <g id="prefix__files" transform="translate(0 2)">
                                                        <g id="prefix__Group_2456" data-name="Group 2456">
                                                            <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                <text fontSize="7px" fontFamily="ProximaNova-Bold,Proxima Nova" fontWeight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                    <tspan x="0" y="0">XLS</tspan>
                                                                </text>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                                            Export to Excel
                                        </button>
                                    </li>
                                </ul>}
                            </div>
                            <div className="gvpd-filter">
                                <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                    </svg>
                                    <span className="generic-tooltip">Filter</span>
                                </button>
                                {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} saveAndExit={this.saveAndExit} />}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    {this.state.tagState?this.state.checkedFilters.map((keys,index)=>(
                    <div className="show-applied-filter">
                            {(Object.values(this.state.filterValueForTag)[index]) !== undefined &&  index===0 ?<button type="button" className="saf-clear-all" onClick={(e)=>this.clearAllTag(e)}>Clear All</button>:null}
                            { (Object.values(this.state.filterValueForTag)[index]) !== undefined && <button type="button" className="saf-btn">{keys}
                              <img onClick={(e)=>this.clearTag(e,index)} src={require('../../../../assets/clearSearch.svg')} />
                              <span className="generic-tooltip">{typeof (Object.values(this.state.filterValueForTag)[index]) == 'object' ? Object.values(Object.values(this.state.filterValueForTag)[index]).join(',') : Object.values(this.state.filterValueForTag)[index]}</span>
                        </button>}
                    </div>)):''}
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table vgtbg-fcfdfe" >
                        <div className="manage-table">
                            <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div>
                            <ColoumSetting {...this.props} {...this.state}
                                handleDragStart={this.handleDragStart}
                                handleDragEnter={this.handleDragEnter}
                                onHeadersTabClick={this.onHeadersTabClick}
                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                closeColumn={(e) => this.closeColumn(e)}
                                saveMainState={this.state.saveMainState}
                                tabVal={this.state.tabVal}
                                pushColumnData={this.pushColumnData}
                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                saveColumnSetting={(e) => this.saveColumnSetting(e)}
                                isReportPageFlag={this.state.isReportPageFlag}
                            />
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn width62">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img src={Reload} onClick={this.onRefresh}></img></span>
                                                </li>
                                                <li className="rab-rinner">
                                                    {tableData.length != 0 && <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.selectAll} name="selectAll" onChange={(e) => this.selectAllAction(e)} /><span className="checkmark1"></span></label>}
                                                </li>
                                            </ul>
                                        </th>
                                        {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                            <label data-key={data}>{data}</label>
                                            <img src={require('../../../../assets/headerFilter.svg')} className="imgHead" data-key={data}/>
                                        </th>
                                        )) : this.state.mainCustomHeadersState.map((data, key) => (
                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                            <label data-key={data}>{data}</label>
                                            <img src={require('../../../../assets/headerFilter.svg')} className="imgHead" data-key={data}/>
                                        </th>
                                        ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {tableData.length != 0 ? tableData.map((data, key) => (
                                    <React.Fragment key={key}>
                                    <tr className={this.state.checkedData.some((item) => item.id == data.id) ? "vgt-tr-bg" : ""}>
                                        <td className="fix-action-btn width62">
                                            <ul className="table-item-list">
                                                <li key={key} className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" checked={this.state.checkedData.some((item) => item.id == data.id)} id={data.id} onChange={(e) => this.handleCheckedData(e, data)} />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                            {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                                <td key={key} >
                                                    <label>{data[hdata]}</label>
                                                </td>
                                            )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                                <td key={keyy} >
                                                    <label>{data[sdata]}</label>
                                                    {/* {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>} */}
                                                </td>
                                            ))}
                                    </tr>
                                    </React.Fragment>
                                    )) : <tr className="tableNoData"><td> NO DATA FOUND </td></tr>}
                                </tbody>
                                {/* <thead>
                                    <tr>
                                        <th><label>Vendor Code</label><img src={require('../../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                        <th><label>Vendor Name</label><img src={require('../../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                        <th><label>User Name</label><img src={require('../../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                        <th><label>Email Id</label><img src={require('../../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                        <th><label>Mobile Number</label><img src={require('../../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                        <th><label>Login</label><img src={require('../../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                        <th><label>Logout</label><img src={require('../../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                        <th><label>Duration</label><img src={require('../../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>MFN0053</label></td>
                                        <td><label>RAGHAV ENTERPRISE -G...</label></td>
                                        <td><label>545148215214</label></td>
                                        <td><label>example@gmail.com</label></td>
                                        <td><label>8760909087</label></td>
                                        <td><label>9:30am</label></td>
                                        <td><label>6:30pm</label></td>
                                        <td><label>2Hrs 30Min</label></td>
                                    </tr>
                                </tbody> */}
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.selectedItems}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastError && <ToastError toastErrorMsg={this.state.toastErrorMsg} closeToastError={this.closeToastError} />}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
            </div>
        )
    }
}
export default VendorLoginLogoutReport;