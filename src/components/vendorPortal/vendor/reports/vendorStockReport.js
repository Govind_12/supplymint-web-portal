import React from 'react';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';
import VendorFilter from '../../vendorFilter';
import ColoumSetting from "../../../replenishment/coloumSetting";
import ConfirmationSummaryModal from "../../../replenishment/confirmationReset";
import moment from 'moment';
import axios from 'axios';
import { CONFIG } from "../../../../config/index";
import ToastError from '../../../utils/toastError';
import MultipleDownload from '../../multipleDownload';
import Reload from '../../../../assets/refresh-block.svg';
import ArsFilterModal from '../../../analytics/arsFilterModal';
import searchIcon from '../../../../assets/clearSearch.svg';

class VendorStockReport extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            stockReport: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            totalStockReport: 0,
            selectedItems: 0,
            jumpPage: 1,
            isReportPageFlag: true, //For Hiding Set/Item Header in Column Setting::

            mainHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "VEN_STOCK_ITEM_REPORT",
                basedOn: "ALL"
            },

            filterItems: {},
            mainCustomHeaders: [],
            getMainHeaderConfig: [],
            mainAvailableHeaders: [],
            mainFixedHeader: [],
            mainCustomHeadersState: [],
            mainHeaderConfigState: {}, 
            mainFixedHeaderData: [],
            mainHeaderConfigDataState: {},
            mainHeaderSummary: [],
            mainDefaultHeaderMap: [],
            mandateHeaderMain: [],
            headerCondition: false,
            coloumSetting: false,
            dragOn: false,
            changesInMainHeaders: false,
            saveMainState: [],
            headerConfigDataState: {},
            confirmModal: false,
            headerMsg: "",
            tabVal: "1",

            filter: false,
            filterBar: false,
            filteredValue: [],
            checkedFilters: [],
            applyFilter: false,
            inputBoxEnable: false,
            tagState: false,
            type: 1,
            filterValueForTag: [],
            filterKey: "",
            filterType: "",
            fromCreationDate: "start date",
            toCreationDate: "end date",
            filterNameForDate: "",
            fromQty: "",
            toQty: "",

            fromArticleMRP: "",
            toArticleMRP: "", 
            fromArticleMRPRangeFrom: "", 
            toArticleMRPRangeFrom: "",
            fromArticleMRPRangeTo: "",
            toArticleMRPRangeTo: "",
            fromMrp: "",
            toMrp: "",
            fromRsp: "",
            toRsp: "",
            fromCostRate: "",
            toCostRate: "",
            fromWsp: "",
            toWsp: "",
            fromBasis: "",
            toBasis: "",
            fromPriceBasis: "",
            toPriceBasis: "",
            fromRate: "",
            toRate: "",
            fromSaleMRP: "",
            toSaleMRP: "",

            toastMsg: "",
            toastLoader: false,
            toastError: false,
            search: "",   
            exportToExcel: false,
            downloadReport: false,
            checkedData: [],
            selectAll: false,
            toastErrorMsg : "",

            //UPPER FILTER:::
            upperFilter: false,
            filterOption: {
                siteCode: [],
                hl1Name: [],
                hl2Name: [],
                hl3Name:[],
                hl4Code: [],
                slName: [],
            },

            searchStoreCodeFilter: "",
            searchDivisionFilter: "",
            searchSectionFilter: "",
            searchDepartmentFilter: "",
            searchArticleFilter: "",
            searchSlNameFilter: "",

            filterData: [],

            openStoreCodeFilter: false,
            openDivisionFilter: false,
            openSectionFilter: false,
            openDepartmentFilter: false,
            openArticleFilter: false,
            openSlNameFilter: false,

            arsCurrentPage: 0,
            arsMaxPage: 0,
            noOfAppliedFilter: 0,
            vendorCode: "",
            //------------------------//

            upperFilterPayload: {},
        }
    }
    componentDidMount() {
        sessionStorage.setItem('currentPage', "VDVANALY")
        sessionStorage.setItem('currentPageName', "Stocks Report")
        let payload = {
            pageNo: 1,
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "",
            filter: ""
        }
        this.props.getAllStockReportRequest(payload)

        if (!this.props.replenishment.getMainHeaderConfig.isSuccess) {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
        }
        document.addEventListener("keydown", this.escFunction, false);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.orders.getAllStockReport.isSuccess) {
            return{
                stockReport: nextProps.orders.getAllStockReport.data.resource == null ? [] : nextProps.orders.getAllStockReport.data.resource,
                prev: nextProps.orders.getAllStockReport.data.prePage,
                current: nextProps.orders.getAllStockReport.data.currPage,
                next: nextProps.orders.getAllStockReport.data.currPage + 1,
                maxPage: nextProps.orders.getAllStockReport.data.maxPage,
                totalSaleReport: nextProps.orders.getAllStockReport.data.statusCount,
                selectedItems: nextProps.orders.getAllStockReport.data.resultedDataCount == undefined || nextProps.orders.getAllStockReport.data.resultedDataCount == null 
                               ? 0 : nextProps.orders.getAllStockReport.data.resultedDataCount,
                jumpPage: nextProps.orders.getAllStockReport.data.currPage
            }
        }
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
                nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
                let mainFixedHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]) : []
                let mainCustomHeadersState = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : []
                let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ? 
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                return {
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    mainCustomHeaders: prevState.headerCondition ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] : {},
                    getMainHeaderConfig,
                    mainAvailableHeaders,
                    mainFixedHeader,
                    mainCustomHeadersState,
                    mainHeaderConfigState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : {},
                    mainFixedHeaderData: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
                    mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
                    mainHeaderSummary: nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : [],
                    mainDefaultHeaderMap: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderMain: Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Mandate Headers"])
                };
            }
        }
        return null;
    }

    componentDidUpdate(){
        if (this.props.orders.getAllStockReport.isSuccess) {
            this.props.getAllStockReportClear();
        }
        if (this.props.replenishment.getMainHeaderConfig.isSuccess) {
            if (this.props.replenishment.getMainHeaderConfig.data.resource != null && this.props.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                setTimeout(() => {
                    this.setState({
                        headerCondition: this.state.mainCustomHeadersState.length == 0 ? true : false,
                    })
                }, 1000);

                // view email redirect logic here:::
                let queryParam = sessionStorage.getItem('login_redirect_queryParam') == null || sessionStorage.getItem('login_redirect_queryParam') == undefined ? null :
                    Object.keys(sessionStorage.getItem('login_redirect_queryParam')).length ? JSON.parse(sessionStorage.getItem('login_redirect_queryParam')) : null;
                let filterValue = "";
                let filterKeyValue = "";
                if (queryParam !== null && queryParam !== undefined && Object.keys(queryParam).length != 0) {
                    var allHeaders = this.props.replenishment.getMainHeaderConfig.data.resource["Default Headers"]
                    filterValue = Object.keys(queryParam).map((_) => { return allHeaders[_] });
                    let flag = false;
                    filterValue.map(data => { if (data == undefined) flag = true; });
                    filterKeyValue = Object.values(queryParam);
                    if (!flag && filterValue.length > 0 && filterKeyValue.length > 0 && filterValue.length == filterKeyValue.length) {
                        setTimeout(() => {
                            let payload = {
                                pageNo: 1,
                                type: 2,
                                search: "",
                                sortedBy: "",
                                sortedIn: "",
                                filter: queryParam
                            }
                            this.props.getAllStockReportRequest(payload)
                        }, 500);

                        this.setState({
                            filteredValue: Object.keys(queryParam).length ? queryParam : {},
                            checkedFilters: Object.keys(queryParam).length ? filterValue : [],
                            applyFilter: Object.keys(queryParam).length ? true : false,
                            inputBoxEnable: Object.keys(queryParam).length ? true : false,
                            tagState: true,
                            //[filterValue] : queryParam.shipmentAdviceCode,
                            type: 2,
                            filterValueForTag: Object.keys(queryParam).length ? queryParam : {},

                        }, () => {
                            filterValue.map((data, index) => {
                                this.setState({ [data]: filterKeyValue[index] })
                            })
                        })
                    }
                }
            }
            this.props.getMainHeaderConfigClear()
        }
        if (this.props.replenishment.createMainHeaderConfig.isSuccess && this.props.replenishment.createMainHeaderConfig.data.basedOn == "ALL") {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            this.props.createMainHeaderConfigClear();
        }
        if (this.props.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                filterData: (this.state.openStoreCodeFilter && this.props.seasonPlanning.getArsGenericFilters.data.resource.searchResultNew !== undefined) ? this.props.seasonPlanning.getArsGenericFilters.data.resource.searchResultNew : this.props.seasonPlanning.getArsGenericFilters.data.resource.searchResult == null ? [] : this.props.seasonPlanning.getArsGenericFilters.data.resource.searchResult,
                arsCurrentPage: this.props.seasonPlanning.getArsGenericFilters.data.resource.currPage,
                arsMaxPage: this.props.seasonPlanning.getArsGenericFilters.data.resource.maxPage
            })
            this.props.getArsGenericFiltersClear();
        }
        if (this.props.vendor.getAllManageVendor.isSuccess) {
            this.setState({
                filterData: this.props.vendor.getAllManageVendor.data.resource == null ? [] : this.props.vendor.getAllManageVendor.data.resource,
                arsCurrentPage: this.props.vendor.getAllManageVendor.data.currPage,
                arsMaxPage: this.props.vendor.getAllManageVendor.data.maxPage
            })
            this.props.getAllManageVendorClear();
        }
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () => {
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }
    openDownloadReport(e) {
        e.preventDefault();
        this.setState({
            downloadReport: !this.state.downloadReport
        }, () => document.addEventListener('click', this.closeDownloadReport));
    }
    closeDownloadReport = () => {
        this.setState({ downloadReport: false }, () => {
            document.removeEventListener('click', this.closeDownloadReport);
        });
    }

    onSearch = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                let payload = {
                    pageNo: 1,
                    type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                    search: e.target.value,
                    filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                }
                this.props.getAllStockReportRequest(payload)
                this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3 })
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
                let payload = {
                    pageNo: 1,
                    type: this.state.type == 4 ? 2 : 1,
                    search: "",
                    filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                }
                this.props.getAllStockReportRequest(payload)
                this.setState({ search: "", type: this.state.type == 4 ? 2 : 1 })
            }
        })
    }

    searchClear =()=> {
        if (this.state.type === 3 || this.state.type == 4) {
            let payload = {
                pageNo: 1,
                type: this.state.type == 4 ? 2 : 1,
                search: "",
                filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.getAllStockReportRequest(payload)
            this.setState({ search: "", type: this.state.type == 4 ? 2 : 1 })
        } else { this.setState({ search: "" }) }
    }

    page =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.orders.getAllStockReport.data.prePage,
                    current: this.props.orders.getAllStockReport.data.currPage,
                    next: this.props.orders.getAllStockReport.data.currPage + 1,
                    maxPage: this.props.orders.getAllStockReport.data.maxPage,
                })
                if (this.props.orders.getAllStockReport.data.currPage != 0) {
                    let payload = {
                        pageNo: this.props.orders.getAllStockReport.data.currPage - 1,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                    }
                    this.props.getAllStockReportRequest(payload)
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.orders.getAllStockReport.data.prePage,
                current: this.props.orders.getAllStockReport.data.currPage,
                next: this.props.orders.getAllStockReport.data.currPage + 1,
                maxPage: this.props.orders.getAllStockReport.data.maxPage,
            })
            if (this.props.orders.getAllStockReport.data.currPage != this.props.orders.getAllStockReport.data.maxPage) {
                let payload = {
                    pageNo: this.props.orders.getAllStockReport.data.currPage + 1,
                    type: this.state.type,
                    search: this.state.search,
                    filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                }
                this.props.getAllStockReportRequest(payload)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.orders.getAllStockReport.data.prePage,
                    current: this.props.orders.getAllStockReport.data.currPage,
                    next: this.props.orders.getAllStockReport.data.currPage + 1,
                    maxPage: this.props.orders.getAllStockReport.data.maxPage,
                })
                if (this.props.orders.getAllStockReport.data.currPage <= this.props.orders.getAllStockReport.data.maxPage) {
                    let payload = {
                        pageNo: 1,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                    }
                    this.props.getAllStockReportRequest(payload)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.orders.getAllStockReport.data.prePage,
                    current: this.props.orders.getAllStockReport.data.currPage,
                    next: this.props.orders.getAllStockReport.data.currPage + 1,
                    maxPage: this.props.orders.getAllStockReport.data.maxPage,
                })
                if (this.props.orders.getAllStockReport.data.currPage <= this.props.orders.getAllStockReport.data.maxPage) {
                    let payload = {
                        pageNo: this.props.orders.getAllStockReport.data.maxPage,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                    }
                    this.props.getAllStockReportRequest(payload)
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                    }
                    this.props.getAllStockReportRequest(payload)
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    xlscsv =()=> {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let payload = {
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
        }
        let selectAllFlag = this.state.selectAll ? true : false;
        let response = ""
        axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=VEN_STOCK_ITEM_REPORT&isAllData=false&isOnlyCurrentPage=${selectAllFlag}`, payload, { headers: headers })
            .then(res => {
                response = res
                window.location.href = res.data.data.resource;
            }).catch((error) => {
                this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
                setTimeout(()=>{
                    this.setState({
                        toastError: false
                    })
                }, 5000)
            });

    }

    getAllData =()=> {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let payload = {
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
        }
        let selectAllFlag = false;
        let response = ""
        axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=VEN_STOCK_ITEM_REPORT&isAllData=true&isOnlyCurrentPage=${selectAllFlag}`, payload, { headers: headers })
            .then(res => {
                response = res
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
                setTimeout(()=>{
                    this.setState({
                        toastError: false
                    })
                }, 5000)
            });
    }

    filterHeader = (event) => {
        var data = event.target.dataset.key 
        if( event.target.closest("th").classList.contains("rotate180"))
            event.target.closest("th").classList.remove("rotate180")
        else
            event.target.closest("th").classList.add("rotate180") 
        var def = {...this.state.mainHeaderConfigDataState};
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ filterKey, filterType: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            let payload = {
                pageNo: this.state.current,
                type: this.state.type,
                search: this.state.search,
                filter: this.state.filteredValue.length !== 0 ? this.state.filteredValue : Object.keys(this.state.upperFilterPayload).length > 0 ? this.state.upperFilterPayload : {},
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.getAllStockReportRequest(payload)
        })
    }

    status = (data) => {
        return (
            <div className="poStatus"> {data.status == "PARTIAL" ? <span className="partialTable partiaTag labelEvent">P</span> : ""}
                {data.poType == "poicode" ? <span className="partialTable setTag labelEvent">N</span>
                    : <span className="partialTable setTag labelEvent">S</span>}
                <div className="poStatusToolTip">
                    {data.status == "PARTIAL" ? <div className="each-tag"><span className="partialTable partiaTag labelEvent">P</span><label>Partial</label></div> : ""}
                    {data.poType == "poicode" ? <div className="each-tag"><span className="partialTable setTag labelEvent">N</span><label>Non-set</label></div> : <div className="each-tag"><span className="partialTable setTag labelEvent">S</span><label>Set</label></div>}
                </div>
            </div>
        )
    }

    small = (str) => {
        if (str != null) {
            var str = str.toString()
            if (str.length <= 45) {
                return false;
            }
            return true;
        }
    }

    closeToastError = () => {
        this.setState({ toastError: false })
    }

    openFilter =(e)=> {
        e.preventDefault();
        Object.keys(this.state.upperFilterPayload).length > 0 && this.clearUpperFilter(e)
        this.setState({filter: !this.state.filter, filterBar: !this.state.filterBar, upperFilter: false},()=>{     
            document.addEventListener("click", this.closeFilterOnClick)
        });
    }

    closeFilter =(e)=> {
        e.preventDefault();
        this.setState({ filter: false, filterBar: false })
    }

    closeFilterOnClick =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")){
            this.setState({ 
                filter: false,
                filterBar: false
            },()=>{
                document.removeEventListener('click', this.closeFilterOnClick)
            })
        }
    }

    clearFilter = () => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                pageNo: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                filter: "",
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.getAllStockReportRequest(payload)
        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            filterValueForTag: [],
            fromQty: "",
            toQty: "",
            fromArticleMRP: "",
            toArticleMRP: "", 
            fromArticleMRPRangeFrom: "", 
            toArticleMRPRangeFrom: "",
            fromArticleMRPRangeTo: "",
            toArticleMRPRangeTo: "",
            fromMrp: "",
            toMrp: "",
            fromRsp: "",
            toRsp: "",
            fromCostRate: "",
            toCostRate: "",
            fromWsp: "",
            toWsp: "",
            fromBasis: "",
            toBasis: "",
            fromPriceBasis: "",
            toPriceBasis: "",
            fromRate: "",
            toRate: "",
            fromSaleMRP: "",
            toSaleMRP: "",
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
        sessionStorage.setItem('login_redirect_queryParam', "")   
    }

    clearTag=(e,index)=>{
        let deleteItem = this.state.checkedFilters;
        let deletedItem = this.state.checkedFilters[index];
        deleteItem.splice(index,1)
        this.setState({
           checkedFilters:deleteItem,
           [deletedItem]: "",
        },()=>{
            if( this.state.checkedFilters.length == 0 )
                this.clearFilter();
            else
                this.submitFilter();
        })
    }
    clearAllTag=(e)=>{
        this.setState({
            checkedFilters:[],
      },()=>{
            this.clearFilter();
            this.clearFilterOutside();
        })
    }
 
   clearFilterOutside=()=>{
         this.setState({
             filteredValue:[],
             checkedFilters:[]
         })
    }

    submitFilter = () => {
        let payload = {}
        let filtervalues =  {}
        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))

        if (payload.qty === undefined) {
            this.setState({ fromQty: "", toQty: "" })
        }
        
        if (payload.articleMRP === undefined)
            this.setState({ fromArticleMRP: "", toArticleMRP: "" })
        if (payload.articleMRPRangeFrom === undefined)
            this.setState({ fromArticleMRPRangeFrom: "", toArticleMRPRangeFrom: "" })
        if (payload.articleMRPRangeTo === undefined)
            this.setState({ fromArticleMRPRangeTo: "", toArticleMRPRangeTo: "" })
        if (payload.mrp === undefined)
            this.setState({ fromMrp: "", toMrp: "" })
        if (payload.rsp === undefined)
            this.setState({ fromRsp: "", toRsp: "" })

        if (payload.costRate === undefined)
            this.setState({ fromCostRate: "", toCostRate: "" })
        if (payload.wsp === undefined)
            this.setState({ fromWsp: "", toWsp: "" })
        if (payload.basis === undefined)
            this.setState({ fromBasis: "", toBasis: "" })

        if (payload.priceBasis === undefined)
            this.setState({ fromPriceBasis: "", toPriceBasis: "" })
        if (payload.rate === undefined)
            this.setState({ fromRate: "", toRate: "" })
        if (payload.saleMRP === undefined)
            this.setState({ fromSaleMRP: "", toSaleMRP: "" })
       
        // Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data] + "T00:00+05:30")))
        Object.keys(payload).map((data) => ((data.includes("Date") || data.includes("validFromDate") || data.includes("validToDate"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim()+ "T00:00+05:30", to: payload[data].split("|")[1].trim()+ "T00:00+05:30" })))
        Object.keys(payload).map((data) => (data.includes("Qty") || data.includes("poAmount") || data.includes("qty") || data.includes("MRP") || data.includes("mrp") || data.includes("rsp") || data.includes("Rate") || data.includes("rate") || data.includes("wsp") || data.includes("basis") || data.includes("Basis"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim() }))
        Object.keys(payload).map((data) => (data.includes("Time"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim()}))     
    
       
        //for handling to and from value on UI level::    
        this.state.checkedFilters.map((data) => (filtervalues[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(filtervalues).map((data) => (data.includes("Qty") || data.includes("poAmount") || data.includes("qty") || data.includes("MRP") || data.includes("mrp") || data.includes("rsp") || data.includes("Rate") || data.includes("rate") || data.includes("wsp") || data.includes("basis") || data.includes("Basis"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: filtervalues[data].split("|")[0].trim(), to: filtervalues[data].split("|")[1].trim()}))
        Object.keys(filtervalues).map((data) => (data.includes("Date") || data.includes("validFromDate") || data.includes("validToDate"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: moment(filtervalues[data].split("|")[0].trim()).format("DD-MM-YYYY"), to: moment(filtervalues[data].split("|")[1].trim()).format('DD-MM-YYYY') }))
        Object.keys(filtervalues).map((data) => ((data.includes("Time"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: moment(filtervalues[data].split("|")[0].trim()).format("DD-MM-YYYY HH:mm"), to: moment(filtervalues[data].split("|")[1].trim()).format("DD-MM-YYYY HH:mm") }))) 
      
        let data = {
            pageNo: 1,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            search: this.state.search,
            filter: payload,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
        }
        this.props.getAllStockReportRequest(data)

        this.setState({
            filter: false,
            filteredValue: payload,
            filterValueForTag: filtervalues,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            tagState: true
        })
    }
   
    handleInput = (event, filterName) => {
           
        if (event != undefined && event.length != undefined) {
            this.setState({
                fromCreationDate: moment(event[0]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                toCreationDate: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            if(filterName == "Creation Time"){
                this.setState({
                    fromCreationDate: moment(event[0]._d).format(),
                    filterNameForDate: filterName
                }, () => this.handleFromAndToValue(event))  
                this.setState({
                    toCreationDate: moment(event[1]._d).format(),
                    filterNameForDate: filterName
                }, () => this.handleFromAndToValue(event))
            } 
        }
        else if( event != null ){ 
            let id = event.target.id;
            let value = event.target.value;
            if (event.target.id === "fromqty") {
                this.setState({ fromQty: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromQty);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "toqty") {
                this.setState({ toQty: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toQty);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "fromarticleMRP") {
                this.setState({ fromArticleMRP: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromArticleMRP);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "toarticleMRP") {
                this.setState({ toArticleMRP: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toArticleMRP);
                    this.handleFromAndToValue(event)
                })
            } 
            else if (event.target.id === "fromarticleMRPRangeFrom") {
                this.setState({ fromArticleMRPRangeFrom: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromArticleMRPRangeFrom);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "toarticleMRPRangeFrom") {
                this.setState({ toArticleMRPRangeFrom: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toArticleMRPRangeFrom);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "fromarticleMRPRangeTo") {
                this.setState({ fromArticleMRPRangeTo: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromArticleMRPRangeTo);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "toarticleMRPRangeTo") {
                this.setState({ toArticleMRPRangeTo: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toArticleMRPRangeTo);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "frommrp") {
                this.setState({ fromMrp: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromMrp);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "tomrp") {
                this.setState({ toMrp: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toMrp);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "fromrsp") {
                this.setState({ fromRsp: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromRsp);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "torsp") {
                this.setState({ toRsp: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toRsp);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "fromcostRate") {
                this.setState({ fromCostRate: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromCostRate);
                    this.handleFromAndToValue(event)
                })
            } 
            else if (event.target.id === "tocostRate") {
                this.setState({ toCostRate: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toCostRate);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "fromwsp") {
                this.setState({ fromWsp: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromWsp);
                    this.handleFromAndToValue(event)
                })
            } 
            else if (event.target.id === "towsp") {
                this.setState({ toWsp: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toWsp);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "frombasis") {
                this.setState({ fromBasis: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromBasis);
                    this.handleFromAndToValue(event)
                })
            } 
            else if (event.target.id === "tobasis") {
                this.setState({ toBasis: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toBasis);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "frompriceBasis") {
                this.setState({ fromPriceBasis: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromPriceBasis);
                    this.handleFromAndToValue(event)
                })
            } 
            else if (event.target.id === "topriceBasis") {
                this.setState({ toPriceBasis: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toPriceBasis);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "fromrate") {
                this.setState({ fromRate: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromRate);
                    this.handleFromAndToValue(event)
                })
            } 
            else if (event.target.id === "torate") {
                this.setState({ toRate: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toRate);
                    this.handleFromAndToValue(event)
                })
            }
            else if (event.target.id === "fromsaleMRP") {
                this.setState({ fromSaleMRP: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.fromSaleMRP);
                    this.handleFromAndToValue(event)
                })
            } 
            else if (event.target.id === "tosaleMRP") {
                this.setState({ toSaleMRP: value }, () => {
                    document.getElementById(id).setAttribute("value", this.state.toSaleMRP);
                    this.handleFromAndToValue(event)
                })
            }
            else
                this.handleFromAndToValue(event);
        }
    }
    
    handleFromAndToValue = () => {
        var value = "";
        var name = event.target.dataset.value;
        if (name == undefined) {
            value = this.state.fromCreationDate + " | " + this.state.toCreationDate
            name = this.state.filterNameForDate;
        }
        else{    
            if (event.target.id === "fromqty" || event.target.id === "toqty")
                value = this.state.fromQty + " | " + this.state.toQty 

            else if (event.target.id === "fromarticleMRP" || event.target.id === "toarticleMRP")
                value = this.state.fromArticleMRP + " | " + this.state.toArticleMRP 
            else if (event.target.id === "fromarticleMRPRangeFrom" || event.target.id === "toarticleMRPRangeFrom")
                value = this.state.fromArticleMRPRangeFrom + " | " + this.state.toArticleMRPRangeFrom 
            else if (event.target.id === "fromarticleMRPRangeTo" || event.target.id === "toarticleMRPRangeTo")
                value = this.state.fromArticleMRPRangeTo + " | " + this.state.toArticleMRPRangeTo 
            else if (event.target.id === "frommrp" || event.target.id === "tomrp")
                value = this.state.fromMrp + " | " + this.state.toMrp 

            else if (event.target.id === "fromrsp" || event.target.id === "torsp")
                value = this.state.fromRsp + " | " + this.state.toRsp 
            else if (event.target.id === "fromcostRate" || event.target.id === "tocostRate")
                value = this.state.fromCostRate + " | " + this.state.toCostRate 
            else if (event.target.id === "fromwsp" || event.target.id === "towsp")
                value = this.state.fromWsp + " | " + this.state.toWsp 
            else if (event.target.id === "frombasis" || event.target.id === "tobasis")
                value = this.state.fromBasis + " | " + this.state.toBasis 

            else if (event.target.id === "frompriceBasis" || event.target.id === "topriceBasis")
                value = this.state.fromPriceBasis + " | " + this.state.toPriceBasis 
            else if (event.target.id === "fromrate" || event.target.id === "torate")
                value = this.state.fromRate + " | " + this.state.toRate 
            else if (event.target.id === "fromsaleMRP" || event.target.id === "tosaleMRP")
                value = this.state.fromSaleMRP + " | " + this.state.toSaleMRP 

            else  
                value = event.target.value
        }

        if( value === " | " || value === "| " || value === " |")
           value = "";

        if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
        }

        this.setState({ [name]: value, applyFilter: true }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                //this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
                this.setState({ applyFilter: true })
            }
            this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
        })
    }

    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        let len = Object.values(this.state.filterValueForTag).length > 0;
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            delete this.state.filterValueForTag[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)]
            this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = "";
            this.setState({ [data]: ""})
            let flag = array.some(data => this.state[data] == "" || this.state[data] == undefined)
            if(!flag && len){
                this.state.checkedFilters.some( (item,index) => {
                    if( item == data){
                        this.clearTag(e, index)
                    }
                })
            }           
                
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
    }

    handleInputBoxEnable = (e, data) => {
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }

    openColoumSetting =(data)=> {
        if (this.state.tabVal == 1) {
            if (this.state.mainCustomHeadersState.length == 0) {
                this.setState({
                    headerCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
    }

    closeColumnSetting = (e) => {
        if (e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
           this.setState({ coloumSetting: false }, () =>
               document.removeEventListener('click', this.closeColumnSetting))
       } 
    }

    handleDragStart =(e, key, dragItem, dragNode)=> {
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', this.handleDragEnd(dragItem, dragNode))
        dragItem.current = key;
        this.setState({ dragOn:true })
    }
    handleDragEnd =(dragItem, dragNode)=> {
        dragNode.current.removeEventListener('dragend', this.HandleDragEnd)
        this.setState({ dragOn:false})
        dragItem.current = null
        dragNode.current = null
    }
    handleDragEnter =(e, key, dragItem, dragNode)=> {
        const currentItem = dragItem.current
        if (e.target !== dragItem.current) {
            if (this.state.tabVal == 1) {
                if (this.state.headerCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainDefaultHeaderMap
                        let newMainHeaderConfigList = prevState.getMainHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newMainHeaderConfigList.splice(key, 0, newMainHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newMainHeaderConfigList))
                        let configheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true,  
                            mainCustomHeaders: configheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newMainHeaderConfigList,
                        }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainHeaderSummary
                        let newCustomList = prevState.mainCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomList.splice(key, 0, newCustomList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newCustomList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true, 
                            mainCustomHeaders: customheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newCustomList
                        }
                    })
                }
            }
        }
    }
    pushColumnData =(e,data)=> {
        if (this.state.tabVal == 1) {
            e.preventDefault();
            let getHeaderConfig = this.state.getMainHeaderConfig
            let customHeadersState = this.state.mainCustomHeadersState
            let headerConfigDataState = this.state.mainHeaderConfigDataState
            let customHeaders = this.state.mainCustomHeaders
            let saveState = this.state.saveMainState
            let defaultHeaderMap = this.state.mainDefaultHeaderMap
            let headerSummary = this.state.mainHeaderSummary
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (this.state.headerCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig = getHeaderConfig
                    getHeaderConfig.push(data)
                    var even = (_.remove(mainAvailableHeaders), function (n) {
                        return n == data
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
               if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(mainAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeadersState: customHeadersState,
                mainCustomHeaders: customHeaders,
                saveMainState: saveState,
                mainDefaultHeaderMap: defaultHeaderMap,
                mainHeaderSummary: headerSummary,
                mainAvailableHeaders,
                changesInMainHeaders: true,
                headerConfigDataState: customHeadersState,
            })
        }
    }
    closeColumn =(data)=> {
        if (this.state.tabVal == 1) {
            let getHeaderConfig = this.state.getMainHeaderConfig
            let headerConfigState = this.state.mainHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.mainCustomHeadersState
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.mainCustomHeadersState.length == 0) {
                    this.setState({
                        headerCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            mainAvailableHeaders.push(data)
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeaders: headerConfigState,
                mainCustomHeadersState: customHeadersState,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.mainFixedHeaderData))[data];
                let saveState = this.state.saveMainState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.mainHeaderSummary
                let defaultHeaderMap = this.state.mainDefaultHeaderMap
                if (!this.state.headerCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    mainHeaderSummary: headerSummary,
                    mainDefaultHeaderMap: defaultHeaderMap,
                    saveMainState: saveState
                })
            }, 100);
        }
    }
    saveColumnSetting =(e)=> {
        if (this.state.tabVal == 1) {
            this.setState({
                coloumSetting: false,
                headerCondition: false,
                saveMainState: [],
                changesInMainHeaders: false,
            })
            let payload = {
                basedOn: "ALL",
                module: "Analytics",
                subModule: "Analytics",
                section: "Analytics",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "VEN_STOCK_ITEM_REPORT",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainCustomHeaders,
            }
            this.props.createMainHeaderConfigRequest(payload)
        }
    }
    resetColumnConfirmation =()=> {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            confirmModal: true,
        })
    }
    resetColumn =()=> {
        const {getMainHeaderConfig,mainFixedHeader} = this.state
        if (this.state.tabVal == 1) {
            let payload = {
                basedOn: "ALL",
                module: "Analytics",
                subModule: "Analytics",
                section: "Analytics",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "VEN_STOCK_ITEM_REPORT",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainHeaderConfigDataState,
            }
            this.props.createMainHeaderConfigRequest(payload)
            let availableHeaders= mainFixedHeader.filter(function (obj) { return getMainHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                headerCondition: true,
                coloumSetting: false,
                saveMainState: [],
                confirmModal: false,
                mainAvailableHeaders:availableHeaders
            })    
        }
    }
    closeConfirmModal =(e)=> {
        this.setState({ confirmModal: !this.state.confirmModal,})
    }
    onHeadersTabClick = (tabVal) => {
        this.setState({ tabVal: tabVal }, () => {
            if (tabVal === 1) {
                this.openColoumSetting("true")
                this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            }
        })
    }

    handleCheckedData =(e, data)=>{
        let array = [...this.state.checkedData]
        if (e.target.name == "selectEach" ) {
            let deletedItem = {
                id: data.id, slCode: data.slCode, slName: data.slName,
            }
            if (this.state.checkedData.some((item) => item.id == data.id)) {
                array = array.filter((item) => item.id != data.id)
            } else {
                array.push(deletedItem)
            }
            if( array.length === this.state.stockReport.length)
               this.setState({ checkedData: array, selectAll: true })
            else    
               this.setState({ checkedData: array, selectAll: false })
        }
    }

    selectAllAction =(e)=>{
        this.setState({ selectAll: !this.state.selectAll, checkedData: [] },()=>{
            if( this.state.selectAll ){
               let array = []; 
               this.state.stockReport.map( data =>{
                    data.id = data.id; 
                    data.slCode = data.slCode;
                    data.slName = data.slName; 

                    array.push(data);
               })
               this.setState({ checkedData: array })
            }
        })
        this.closingAllModal();
    }

    closingAllModal = () => {
        this.setState({
            filter: false, exportToExcel: false, filterBar: false,
            downloadReport: false,
            headerCondition: false, coloumSetting: false,
            confirmModal: false
        },()=>{
            document.removeEventListener('click', this.closeExportToExcel)
            document.removeEventListener('click', this.closeDownloadReport)
            document.removeEventListener('click', this.closeFilterOnClick)
            document.removeEventListener('click', this.closeColumnSetting)
        })
    }

    onRefresh = () => {
        this.state.checkedFilters.map((data) => this.setState({ [data]: "" }))
         let payload = {
            pageNo: 1,
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "",
            filter: ""
        }
        this.props.getAllStockReportRequest(payload)
        if (document.getElementsByClassName('rotate180')[0] != undefined) {
            document.getElementsByClassName('rotate180')[0].classList.remove('rotate180')
        }
        this.setState({ filteredValue: [], selectAll: false, checkedFilters: [], checkedData: [] })
        this.closingAllModal();
        this.clearAllTag();
    }

    upperFilter =(e)=>{
        Object.keys(this.state.filteredValue).length > 0 && this.clearAllTag(e)
        this.setState({upperFilter: !this.state.upperFilter})
    }

    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.closingAllModal();
        }
    }

    openArsFilter =(e, searchFilterVariable, flag)=>{
        this.setState({ 
            openStoreCodeFilter: false,
            openDivisionFilter: false,
            openSectionFilter: false,
            openDepartmentFilter: false,
            openArticleFilter: false,
            openSlNameFilter: false,
        },()=>{
            this.setState({ 
                    [flag]: true,
                    filterData: [],
            },()=>{
                if( flag === "openSlNameFilter"){
                    let payload = {
                        pageNo: 1,
                        type: this.state.searchSlNameFilter !== "" ? 3 : 1,
                        search: this.state.searchSlNameFilter,
                        sortedBy: "",
                        sortedIn: "",
                        filter: {},
                        key: "",
                    }
                    this.props.getAllManageVendorRequest(payload)
                }else{
                    let payload = {};
                    if( flag !== "openStoreCodeFilter" ){
                        payload ={
                            entity: "item",
                            key: flag == "openDivisionFilter" ? "hl1_name" : flag == "openSectionFilter" ? "hl2_name" : flag == "openDepartmentFilter" ? "hl3_name" : 
                                flag == "openArticleFilter" && "hl4_code",
                            search: "",
                            pageNo: 1
                        }
                    }else{
                        payload ={
                            entity: "site",
                            key: "name1", 
                            code: "site_code",
                            search: "",
                            pageNo: 1
                        }
                    }
                    this.props.getArsGenericFiltersRequest(payload);
                } 
                document.addEventListener('click', this.onClickCloseArsFilter)
            })
        })
    }

    onCloseArsFilter =(e)=>{
       this.setState({ 
            openStoreCodeFilter: false,
            openDivisionFilter: false,
            openSectionFilter: false,
            openDepartmentFilter: false,
            openArticleFilter: false,
            openSlNameFilter: false,
        },()=>{
            document.removeEventListener('click', this.onClickCloseArsFilter)
       })
    }

    onClickCloseArsFilter =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop")){
            this.setState({
                openStoreCodeFilter: false,
                openDivisionFilter: false,
                openSectionFilter: false,
                openDepartmentFilter: false,
                openArticleFilter: false,
                openSlNameFilter: false,
            },()=>{
                document.removeEventListener('click', this.onClickCloseArsFilter)
            })
        }
    }

    onSearchFilter =(e, searchFilterVariable, flag) => {
        let keyCode = e.keyCode

        if(searchFilterVariable == "searchStoreCodeFilter")
            this.state.filterOption.siteCode = []
        if(searchFilterVariable == "searchDivisionFilter")
            this.state.filterOption.hl1Name = []
        if(searchFilterVariable == "searchSectionFilter")
            this.state.filterOption.hl2Name = []
        if(searchFilterVariable == "searchDepartmentFilter")
            this.state.filterOption.hl3Name = []
        if(searchFilterVariable == "searchArticleFilter")
            this.state.filterOption.hl4Code = []
        if(searchFilterVariable == "searchSlNameFilter") 
            this.state.filterOption.slName = []
            
        this.setState({ 
            [searchFilterVariable]: e.target.value,
            [flag]: false,
            filterData: [],
        }, () => {
            if(keyCode == 13){
                this.setState({ 
                    openStoreCodeFilter: false,
                    openDivisionFilter: false,
                    openSectionFilter: false,
                    openDepartmentFilter: false,
                    openArticleFilter: false,
                    openSlNameFilter: false,
                },()=>{
                    if( flag == "openSlNameFilter"){
                        let payload = {
                            pageNo: 1,
                            type: this.state.searchSlNameFilter !== "" ? 3 : 1,
                            search: this.state.searchSlNameFilter,
                            sortedBy: "",
                            sortedIn: "",
                            filter: {},
                            key: "",
                        }
                        this.props.getAllManageVendorRequest(payload)
                    }else{
                        let payload = {};
                        if( flag !== "openStoreCodeFilter" ){
                            payload ={
                                entity: "item",
                                key: flag == "openDivisionFilter" ? "hl1_name" : flag == "openSectionFilter" ? "hl2_name" : flag == "openDepartmentFilter" ? "hl3_name" : 
                                    flag == "openArticleFilter" && "hl4_code",
                                search: this.state[searchFilterVariable].trim(),
                                pageNo: 1
                            }
                        }else{
                            payload ={
                                entity: "site",
                                key: "name1", 
                                code: "site_code",
                                search: this.state[searchFilterVariable].trim(),
                                pageNo: 1
                            }
                        }
                        this.props.getArsGenericFiltersRequest(payload); 
                    } 
                    this.setState({
                        [flag]: true,
                    })
                })  
            }
        })
        document.addEventListener('click', this.onClickCloseArsFilter)
    }

    onSearchFilterClear =(e, searchFilterVariable, flag, filterTypeArray)=> {
        this.state.filterOption[filterTypeArray] = []
        let lenS = this.state.search.length
        let lenF = Object.keys(this.state.filterOption).filter((k) => this.state.filterOption[k].length > 0).length
        if( flag === "openSlNameFilter"){
            let payload = {
                pageNo: 1,
                type: 1,
                search: "",
                sortedBy: "",
                sortedIn: "",
                filter: {},
                key: "",
            }
            this.props.getAllManageVendorRequest(payload)
            this.setState({ vendorCode: ""})
        }else{
            let payload = {};
            if( flag !== "openStoreCodeFilter" ){
                payload ={
                    entity: "item",
                    key: flag == "openDivisionFilter" ? "hl1_name" : flag == "openSectionFilter" ? "hl2_name" : flag == "openDepartmentFilter" ? "hl3_name" : 
                        flag == "openArticleFilter" && "hl4_code",
                    search: "",
                    pageNo: 1
                }
            }else{
                payload ={
                    entity: "site",
                    key: "name1", 
                    code: "site_code",
                    search: "",
                    pageNo: 1
                }
            }
            this.props.getArsGenericFiltersRequest(payload);
        }  
        this.setState({
            [searchFilterVariable]: "",
            [flag]: false,
            type: lenS && lenF ? 4 : lenS ? 3 : lenF ? 2 : 1,
            noOfAppliedFilter: lenF
        },()=>this.applyFilter())

        if( lenF == 0){
            this.removeFilter(e);
        }
    }

    handleChange =(e, key)=> {
        e.persist();
        let value = e.target.closest("li").getAttribute("value");
        let siteCodeAndName = e.target.closest("li").getAttribute("name");
        const objectFilterOption = this.state.filterOption;
        if(objectFilterOption[key].indexOf(value) == -1) {
            objectFilterOption[key].pop();
            objectFilterOption[key].push(value);
            this.setState({
                filterOption: objectFilterOption,
                vendorCode: key == "slName" ? Object.values(this.state.filterData).filter((k) => k.slName == objectFilterOption["slName"][0])[0].slCode : this.state.vendorCode,
                searchStoreCodeFilter: this.state.openStoreCodeFilter ? siteCodeAndName : this.state.searchStoreCodeFilter

            })
        } else {
            objectFilterOption[key].splice(objectFilterOption[key].indexOf(value), 1);
            this.setState({
                filterOption: objectFilterOption,
                vendorCode: key == "slName" ? "" : this.state.vendorCode,
                searchStoreCodeFilter: this.state.openStoreCodeFilter ? "" : this.state.searchStoreCodeFilter

            })
        }
    }

    applyFilter =(e)=>{
        let lenS = this.state.search.length
        let lenF = Object.keys(this.state.filterOption).filter((k) => this.state.filterOption[k].length > 0).length
        this.setState({ noOfAppliedFilter : lenF})  

        let payload = {
            pageNo: 1,
            type: lenS && lenF ? 4 : lenS ? 3 : lenF ? 2 : 1,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: {
                siteCode: this.state.filterOption["siteCode"].length ? this.state.filterOption["siteCode"][0] : undefined,
                hl1Name: this.state.filterOption["hl1Name"].length ? this.state.filterOption["hl1Name"][0] : undefined,
                hl2Name: this.state.filterOption["hl2Name"].length ? this.state.filterOption["hl2Name"][0] : undefined,
                hl3Name: this.state.filterOption["hl3Name"].length ? this.state.filterOption["hl3Name"][0] : undefined,
                hl4Code: this.state.filterOption["hl4Code"].length ? this.state.filterOption["hl4Code"][0] : undefined,
                //slName: this.state.filterOption["slName"].length ? this.state.filterOption["slName"][0] : undefined,
                //slCode: this.state.vendorCode !== "" ? this.state.vendorCode : undefined
            },
        }
        if(payload.filter.siteCode !== undefined || payload.filter.hl1Name !== undefined || payload.filter.hl2Name !== undefined
            || payload.filter.hl3Name !== undefined || payload.filter.hl4Code !== undefined )
        {
            this.props.getAllStockReportRequest(payload)
            this.onCloseArsFilter();

            let filterPayload ={
                siteCode: this.state.filterOption["siteCode"].length ? this.state.filterOption["siteCode"][0] : undefined,
                hl1Name: this.state.filterOption["hl1Name"].length ? this.state.filterOption["hl1Name"][0] : undefined,
                hl2Name: this.state.filterOption["hl2Name"].length ? this.state.filterOption["hl2Name"][0] : undefined,
                hl3Name: this.state.filterOption["hl3Name"].length ? this.state.filterOption["hl3Name"][0] : undefined,
                hl4Code: this.state.filterOption["hl4Code"].length ? this.state.filterOption["hl4Code"][0] : undefined,
                //slName: this.state.filterOption["slName"].length ? this.state.filterOption["slName"][0] : undefined,
                //slCode: this.state.vendorCode !== "" ? this.state.vendorCode : undefined
            }
            this.setState({ upperFilterPayload: filterPayload, type: this.state.type == 3 || this.state.type == 4 ? 4 : 2})
        }
    }

    removeFilter =(e)=> {
        e.persist();
        const objectFilterOption = this.state.filterOption;
        Object.keys(objectFilterOption).forEach(v => objectFilterOption[v] = []);
        let lenS = this.state.search.length
        let lenF = this.state.filterOption.length
        if(this.state.type == 4 || this.state.type == 2){
            this.setState({
                filterOption: objectFilterOption,
                vendorCode: "",
                noOfAppliedFilter: 0,
                type: lenS && lenF ? 4 : lenS ? 3 : lenF ? 2 : 1,
            }, () => {
                let payload = {
                    pageNo: 1,
                    type: lenS && lenF ? 4 : lenS ? 3 : lenF ? 2 : 1,
                    search: this.state.search,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    filter: {},
                }
                this.props.getAllStockReportRequest(payload)
            })
        }
    }

    clearUpperFilter =(e)=>{
        this.state.filterOption.siteCode = []
        this.state.filterOption.hl1Name = []
        this.state.filterOption.hl2Name = []
        this.state.filterOption.hl3Name = []
        this.state.filterOption.hl4Code = []
        this.state.filterOption.slName = []

        this.setState({
            searchStoreCodeFilter: "",
            searchDivisionFilter: "",
            searchSectionFilter: "",
            searchDepartmentFilter: "",
            searchArticleFilter: "",
            searchSlNameFilter: "",
            vendorCode: "",
            noOfAppliedFilter: 0,
            upperFilterPayload: {},
        })
        this.onCloseArsFilter();
        this.removeFilter(e);
    }

    resetFilterData =(payload)=>{
       this.setState({ filterData: []},()=>{
         if(payload.key)
           this.props.getArsGenericFiltersRequest(payload);
         else  
           this.props.getAllManageVendorRequest(payload)
       })
    }

    render() {
        const{stockReport, search,filterOption, noOfAppliedFilter} = this.state
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="otb-reports-filter p-lr-47">
                        <div className="orf-head">
                            <div className="orfh-left">
                                <div className="orfhl-inner">
                                    <h3 className={this.state.upperFilter === false ? "h3After" : ""}>Filters</h3>
                                    {this.state.upperFilter === false ? <span>{noOfAppliedFilter+" "} filters applied</span>: null}
                                </div>
                               {this.state.upperFilter === false && noOfAppliedFilter > 0 ? <button type="button" onClick={this.removeFilter}>Remove</button>: null}
                            </div>
                            <div className="orfh-rgt">
                                <button className={this.state.upperFilter === false ? "" : "orfh-btn"} onClick={(e) => this.upperFilter(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.806" height="8.403" viewBox="0 0 14.806 8.403">
                                        <path id="Path_943" fill="none" stroke="#3a5074" strokeLinecap="round" strokeLinecap="round" strokeWidth="2px" d="M6 9l5.989 5.989L17.978 9" data-name="Path 943" transform="translate(-4.586 -7.586)"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        {this.state.upperFilter && <div className="orf-body m-top-10">
                            <form>
                                <div className="col-lg-12 pad-0">
                                    {/* <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Vendor Name</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.slName.length ? filterOption.slName : this.state.searchSlNameFilter} className="onFocus pnl-purchase-input" onChange={(e)=>this.onSearchFilter(e, "searchSlNameFilter", "openSlNameFilter")} onKeyDown={(e)=>this.onSearchFilter(e, "searchSlNameFilter", "openSlNameFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, "searchSlNameFilter", "openSlNameFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchSlNameFilter != "" || filterOption.slName.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, "searchSlNameFilter", "openSlNameFilter", "slName")} /></span> : null}
                                            {this.state.openSlNameFilter && this.state.filterData.length > 0 && <ArsFilterModal {...this.state} {...this.props} filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="slName" checkedData={this.state.filterOption} handleChange={this.handleChange} current={this.state.arsCurrentPage} max={this.state.arsMaxPage} flag="openSlNameFilter" search={this.state.searchSlNameFilter} resetFilterData={this.resetFilterData}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label className="pd-filter-label" fillRule="pddivision">Vendor Code</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-read" disabled value={this.state.vendorCode}/>
                                        </div>
                                    </div> */}
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Site</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={this.state.searchStoreCodeFilter} className="onFocus pnl-purchase-input" onChange={(e)=>this.onSearchFilter(e, "searchStoreCodeFilter", "openStoreCodeFilter")} onKeyDown={(e)=>this.onSearchFilter(e, "searchStoreCodeFilter", "openStoreCodeFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, "searchStoreCodeFilter", "openStoreCodeFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchStoreCodeFilter != "" || filterOption.siteCode.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, "searchStoreCodeFilter", "openStoreCodeFilter", "siteCode")} /></span> : null}
                                            {this.state.openStoreCodeFilter && this.state.filterData.length > 0 && <ArsFilterModal {...this.state} {...this.props} filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="siteCode" checkedData={this.state.filterOption} handleChange={this.handleChange} current={this.state.arsCurrentPage} max={this.state.arsMaxPage} flag="openStoreCodeFilter" search={this.state.searchStoreCodeFilter} resetFilterData={this.resetFilterData}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Division</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.hl1Name.length ? filterOption.hl1Name : this.state.searchDivisionFilter} className="onFocus pnl-purchase-input" onChange={(e)=>this.onSearchFilter(e, "searchDivisionFilter", "openDivisionFilter")} onKeyDown={(e)=>this.onSearchFilter(e, "searchDivisionFilter", "openDivisionFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, "searchDivisionFilter", "openDivisionFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchDivisionFilter != "" || filterOption.hl1Name.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, "searchDivisionFilter", "openDivisionFilter", "hl1Name")} /></span> : null}
                                            {this.state.openDivisionFilter && this.state.filterData.length > 0 && <ArsFilterModal {...this.state} {...this.props} filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="hl1Name" checkedData={this.state.filterOption} handleChange={this.handleChange} current={this.state.arsCurrentPage} max={this.state.arsMaxPage} flag="openDivisionFilter" search={this.state.searchDivisionFilter} resetFilterData={this.resetFilterData}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Section</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.hl2Name.length ? filterOption.hl2Name : this.state.searchSectionFilter} className="onFocus pnl-purchase-input" onChange={(e)=>this.onSearchFilter(e, "searchSectionFilter", "openSectionFilter")} onKeyDown={(e)=>this.onSearchFilter(e, "searchSectionFilter", "openSectionFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, "searchSectionFilter", "openSectionFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchSectionFilter != "" || filterOption.hl2Name.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, "searchSectionFilter", "openSectionFilter", "hl2Name")} /></span> : null}
                                            {this.state.openSectionFilter && this.state.filterData.length > 0 && <ArsFilterModal {...this.state} {...this.props} filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="hl2Name" checkedData={this.state.filterOption} handleChange={this.handleChange} current={this.state.arsCurrentPage} max={this.state.arsMaxPage} flag="openSectionFilter" search={this.state.searchSectionFilter} resetFilterData={this.resetFilterData}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Department</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.hl3Name.length ? filterOption.hl3Name : this.state.searchDepartmentFilter} className="onFocus pnl-purchase-input" onChange={(e)=>this.onSearchFilter(e, "searchDepartmentFilter", "openDepartmentFilter")} onKeyDown={(e)=>this.onSearchFilter(e, "searchDepartmentFilter", "openDepartmentFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, "searchDepartmentFilter", "openDepartmentFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchDepartmentFilter != "" || filterOption.hl3Name.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, "searchDepartmentFilter", "openDepartmentFilter", "hl3Name")} /></span> : null}
                                            {this.state.openDepartmentFilter && this.state.filterData.length > 0 && <ArsFilterModal {...this.state} {...this.props} filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="hl3Name" checkedData={this.state.filterOption} handleChange={this.handleChange} current={this.state.arsCurrentPage} max={this.state.arsMaxPage} flag="openDepartmentFilter" search={this.state.searchDepartmentFilter} resetFilterData={this.resetFilterData}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Article Code</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.hl4Code.length ? filterOption.hl4Code : this.state.searchArticleFilter} className="onFocus pnl-purchase-input" onChange={(e)=>this.onSearchFilter(e, "searchArticleFilter", "openArticleFilter")} onKeyDown={(e)=>this.onSearchFilter(e, "searchArticleFilter", "openArticleFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, "searchArticleFilter", "openArticleFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchArticleFilter != "" || filterOption.hl4Code.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, "searchArticleFilter", "openArticleFilter", "hl4Code")} /></span> : null}
                                            {this.state.openArticleFilter && this.state.filterData.length > 0 && <ArsFilterModal {...this.state} {...this.props} filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="hl4Code" checkedData={this.state.filterOption} handleChange={this.handleChange} current={this.state.arsCurrentPage} max={this.state.arsMaxPage} flag="openArticleFilter" search={this.state.searchArticleFilter} resetFilterData={this.resetFilterData}/>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30">
                                    <div className="orfb-submit-btn">
                                        <button type="button" className="orfbab-submit" onClick={this.applyFilter}>Submit</button>
                                        <button type="button" className="" onClick={this.clearUpperFilter}>Clear</button>
                                    </div>
                                </div>
                            </form>
                        </div>}
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="otb-reports-content">
                        <div className="otbrc-left">
                            <div className="otbrc-search">
                                <input type="search" value={this.state.search} onChange={this.onSearch} onKeyDown={this.onSearch} placeholder="Type To Search" />
                                <img className="search-image" src={require('../../../../assets/searchicon.svg')} />
                                {search != "" ? <span className="closeSearch"><img src={require('../../../../assets/clearSearch.svg')} onClick={this.searchClear} /></span> : null}
                            </div>
                        </div> 
                        <div className="otbrc-right">
                            <div className="gvpd-download-drop">
                                <button type="button" className="otbrc-down-report" onClick={(e) => this.openDownloadReport(e)}>Download Report
                                    <span className="otbrcdr-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </span>
                                </button>
                                {this.state.downloadReport &&
                                <ul className="pi-history-download">
                                    <li>
                                        <button className="export-excel" type="button" onClick={() => this.xlscsv()}>
                                            <span className="pi-export-svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                    <g id="prefix__files" transform="translate(0 2)">
                                                        <g id="prefix__Group_2456" data-name="Group 2456">
                                                            <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                <text fontSize="7px" fontFamily="ProximaNova-Bold,Proxima Nova" fontWeight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                    <tspan x="0" y="0">XLS</tspan>
                                                                </text>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                                            Export to Excel
                                        </button>
                                    </li>
                                    {/* <li>
                                        <button className="export-excel" type="button" onClick={()=>this.getAllData()}>
                                            <span className="pi-export-svg">
                                                <img src={require('../../../../assets/downloadAll.svg')} />
                                            </span>
                                        Download All</button>
                                    </li> */}
                                </ul>}
                            </div>
                            {/* <div className="gvpd-download-drop">
                                {this.state.checkedData.length >= 1 ? <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e)=>this.openExportToExcel(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                        <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                    </svg>
                                </button>
                                    : <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download2"} type="button">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#d8d3d3" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </button>}
                                {this.state.exportToExcel ? (
                                    <MultipleDownload  {...this.props} drop="stockReport" checkItems={this.state.checkedData} />
                                ) : (null)}
                            </div> */}
                            <div className="gvpd-filter">
                                <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                    </svg>
                                    <span className="generic-tooltip">Filter</span>
                                </button>
                                {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    {this.state.tagState?this.state.checkedFilters.map((keys,index)=>(
                    <div className="show-applied-filter">
                            {(Object.values(this.state.filterValueForTag)[index]) !== undefined &&  index===0 ?<button type="button" className="saf-clear-all" onClick={(e)=>this.clearAllTag(e)}>Clear All</button>:null}
                            { (Object.values(this.state.filterValueForTag)[index]) !== undefined && <button type="button" className="saf-btn">{keys}
                              <img onClick={(e)=>this.clearTag(e,index)} src={require('../../../../assets/clearSearch.svg')} />
                              <span className="generic-tooltip">{typeof (Object.values(this.state.filterValueForTag)[index]) == 'object' ? Object.values(Object.values(this.state.filterValueForTag)[index]).join(',') : Object.values(this.state.filterValueForTag)[index]}</span>
                        </button>}
                    </div>)):''}
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table vgtbg-fcfdfe" >
                        <div className="manage-table">
                            <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div>
                            <ColoumSetting {...this.props} {...this.state}
                                handleDragStart={this.handleDragStart}
                                handleDragEnter={this.handleDragEnter}
                                onHeadersTabClick={this.onHeadersTabClick}
                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                closeColumn={(e) => this.closeColumn(e)}
                                saveMainState={this.state.saveMainState}
                                tabVal={this.state.tabVal}
                                pushColumnData={this.pushColumnData}
                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                saveColumnSetting={(e) => this.saveColumnSetting(e)}
                                isReportPageFlag={this.state.isReportPageFlag}
                            />
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                    <th className="fix-action-btn width62">
                                        <ul className="rab-refresh">
                                            <li className="rab-rinner">
                                                <span><img src={Reload} onClick={this.onRefresh}></img></span>
                                            </li>
                                            <li className="rab-rinner">
                                                {stockReport.length != 0 && <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.selectAll} name="selectAll" onChange={(e) => this.selectAllAction(e)} /><span className="checkmark1"></span></label>}
                                            </li>
                                        </ul>
                                    </th>
                                    {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                    <th key={key} data-key={data} onClick={this.filterHeader}>
                                        <label data-key={data}>{data}</label>
                                        <img src={require('../../../../assets/headerFilter.svg')} className="imgHead" data-key={data}/>
                                    </th>
                                    )) : this.state.mainCustomHeadersState.map((data, key) => (
                                    <th key={key} data-key={data} onClick={this.filterHeader}>
                                        <label data-key={data}>{data}</label>
                                        <img src={require('../../../../assets/headerFilter.svg')} className="imgHead" data-key={data}/>
                                    </th>
                                    ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {stockReport.length != 0 ? stockReport.map((data, key) => (
                                    <React.Fragment key={key}>
                                    <tr className={this.state.checkedData.some((item) => item.id == data.id) ? "vgt-tr-bg" : ""}>
                                        <td className="fix-action-btn width62">
                                            <ul className="table-item-list">
                                                <li key={key} className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" checked={this.state.checkedData.some((item) => item.id == data.id)} id={data.id} onChange={(e) => this.handleCheckedData(e, data)} />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                            {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                                <td key={key} >
                                                    {hdata == "orderNumber" ? <React.Fragment><label className="fontWeig600  whiteUnset wordBreakInherit">{data["orderNumber"]} </label>{this.status(data)}</React.Fragment>
                                                        : <label>{data[hdata]}</label>}
                                                </td>
                                            )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                                <td key={keyy} >
                                                    {sdata == "orderNumber" ? <React.Fragment><label className="table-td-text fontWeig600  whiteUnset wordBreakInherit">{data["orderNumber"]} </label>{this.status(data)}</React.Fragment>
                                                        : <label className="table-td-text">{data[sdata]}</label>}
                                                    {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                                                </td>
                                            ))}
                                    </tr>
                                    </React.Fragment>
                                    )) : <tr className="tableNoData"><td> NO DATA FOUND </td></tr>}
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.selectedItems}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.toastError && <ToastError toastErrorMsg={this.state.toastErrorMsg} closeToastError={this.closeToastError} />}
                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                    {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                </div>
            </div>
        )
    }
}
export default VendorStockReport;