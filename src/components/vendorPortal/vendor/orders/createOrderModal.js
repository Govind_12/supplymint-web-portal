import React from 'react';
import VendorColorModal from './vendorColorModal';
import moment from 'moment';

var d = new Date();
d.setDate(d.getDate() + 15);

class CreateOrder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            validFrom: moment(new Date()).format("YYYY-MM-DD"),
            validTo: moment(d).format("YYYY-MM-DD"),
            siteCode: "",
            siteName: "",

            formIndex: "",
            designNo: "",
            selectedColor: Array(this.props.selectedData.length).fill({
                color: "",
                itemcode: "",
                hsn: "",
                rate: "",
                qty: "",
                totalQty: "",
                deliveryDate: ""
            }),

            errorSite: false,
            errorValidTo: false,
            errorRows: Array(this.props.selectedData.length).fill(false),
            colorModal: false
        }
    }

    setSelectedColor = (stringValue, index) => {
        let selectedColor = [...this.state.selectedColor];
        selectedColor[index] = stringValue;
        this.setState({
            selectedColor
        }, () => this.validateRows(stringValue.color, index));
    }

    handleSiteChange = (codeAndName) => {
        codeAndName = JSON.parse(codeAndName);
        this.setState({
            siteCode: codeAndName.code,
            siteName: codeAndName.name
        }, this.validateSite);
    }

    validateSite = () => {
        if (this.state.siteName === "") {
            this.setState({
                errorSite: true
            });
            return true;
        }
        else {
            this.setState({
                errorSite: false
            });
            return false;
        }
    }

    validateDate = () => {
        let fromDate = new Date(this.state.validFrom);
        let toDate = new Date(this.state.validTo);
        if (fromDate.getTime() > toDate.getTime()) {
            this.setState({
                errorValidTo: true
            });
            return true;
        }
        else {
            this.setState({
                errorValidTo: false
            });
            return false;
        }
    }

    validateRows = (color, index) => {
        if (color === "") {
            this.setState(prevState => {
                let errorRows = [...prevState.errorRows];
                errorRows[index] = true;
                return {errorRows: errorRows};
            });
            return true;
        }
        else {
            this.setState(prevState => {
                let errorRows = [...prevState.errorRows];
                errorRows[index] = false;
                return {errorRows: errorRows};
            });
            return false;
        }
    }

    handleSubmit = () => {
        let valid = true;
        if (this.validateSite() || this.validateDate() || this.state.selectedColor.filter((item, index) => this.validateRows(item.color, index)).length > 0) {
            valid = false;
        }
        if (valid) {
            console.log("VALID");
            var poData = []; 
            this.props.selectedData.forEach((item, index) => {
                let colorArray = this.state.selectedColor[index].color.split(", ");
                let icodeArray = this.state.selectedColor[index].itemcode.split(", ");
                let qtyArray = this.state.selectedColor[index].qty.split(", ");
                let dateArray = this.state.selectedColor[index].deliveryDate.split(", ");
                let rateArray = this.state.selectedColor[index].rate.split(", ");
                colorArray.forEach((innerItem, innerIndex) => {
                    poData.push({
                        iCode: icodeArray[innerIndex],
                        deliveryDate: dateArray[innerIndex],
                        color: innerItem,
                        design: item.design_no,
                        qty: qtyArray[innerIndex],
                        rate: rateArray[innerIndex]
                    });
                });
            });
            this.props.createPoArticleRequest({
                slCode: this.props.selectedData[0].slcode,
                slName: this.props.selectedData[0].slname,
                poType: "poIcodeNewFeature",
                validFrom: this.state.validFrom,
                validTo: this.state.validTo,
                siteCode: this.state.siteCode,
                siteName: this.state.siteName,
                isSet: false,
                typeOfBuying: "Planned",
                poData: poData
            });
        }
    }

    openColorModal(e, index, designNo) {
        e.preventDefault();
        this.setState({
            formIndex: index,
            designNo: designNo,
            colorModal: !this.state.colorModal
        });
    }
    closeColorModal = () => {
        this.setState({
            colorModal: false,
            formIndex: "",
            designNo: ""
        });
    }

    render() {
        return (
            <div className="modal">
                <div className="modal-content create-order-modal">
                    <div className="com-head">
                        <div className="comht-left">
                            <h3>Create Order</h3>
                            {/* <p>You can select multiple colors from below list</p> */}
                        </div>
                        <div className="comh-right">
                            <button type="button" className="gcmht-close" onClick={this.props.closeCreateOrder}>Close</button>
                            <button type="button" className="comh-done" id="doneButton" onClick={this.handleSubmit}>Submit</button>
                        </div>
                    </div>
                    <div className="com-body">
                        <div className="col-lg-12 col-md-12 p-lr-47 m-top-30">
                            <div className="pi-new-layout">
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0 mb10">
                                    <label className="pnl-purchase-label">Vendor Code</label>
                                    <div className="inputTextKeyFucMain">
                                        <input type="text" className="onFocus pnl-purchase-read" value={this.props.selectedData[0].slcode === undefined || this.props.selectedData[0].slcode === null || this.props.selectedData[0].slcode === "" ? sessionStorage.getItem("partnerEnterpriseCode") : this.props.selectedData[0].slcode} disabled />
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0 mb10">
                                    <label className="pnl-purchase-label">Vendor Name</label>
                                    <div className="inputTextKeyFucMain">
                                        <input type="text" className="onFocus pnl-purchase-read" value={this.props.selectedData[0].slname === undefined || this.props.selectedData[0].slname === null || this.props.selectedData[0].slname === "" ? sessionStorage.getItem("partnerEnterpriseName") : this.props.selectedData[0].slname} disabled />
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0 mb10">
                                    <label className="pnl-purchase-label">Delivery Site</label>
                                    <select className={this.state.errorSite ? "pnl-purchase-select errorBorder" : "pnl-purchase-select onFocus"} value={JSON.stringify({code: this.state.siteCode, name: this.state.siteName})} onChange={(e) => this.handleSiteChange(e.target.value)}>
                                        <option value={JSON.stringify({code: "", name: ""})} disabled>Select Delivery Site</option>
                                        {this.props.deliverySiteData.map((item) =>
                                            <option key={item.code} value={JSON.stringify({code: item.code, name: item.name})}>{item.name}</option>
                                        )}
                                    </select>
                                    {this.state.errorSite && <span className="error">Select Delivery Site</span>}
                                </div>
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0">
                                    <label className="pnl-purchase-label">PO Valid From</label>
                                    <input type="date" className="pnl-purchase-date onFocus" placeholder={this.state.validFrom} value={this.state.validFrom} onChange={(e) => this.setState({validFrom: e.target.value})} />
                                </div>
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0">
                                    <label className="pnl-purchase-label">PO Valid To</label>
                                    <input type="date" className={this.state.errorValidTo ? "pnl-purchase-date errorBorder" : "pnl-purchase-date onFocus"} placeholder={this.state.validTo} value={this.state.validTo} onChange={(e) => this.setState({validTo: e.target.value}, this.validateDate)} />
                                    {this.state.errorValidTo && <span className="error">Select valid date</span>}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                            <div className="create-order-table">
                                <div className="cot-manage">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th><label>Design Number</label></th>
                                                <th><label>Sale Order Qty</label></th>
                                                <th><label>Color</label></th>
                                                <th><label>ICode</label></th>
                                                <th><label>HSN</label></th>
                                                <th><label>Rate</label></th>
                                                <th><label>Qty</label></th>
                                                <th><label>Total Qty</label></th>
                                                <th><label>Delivery Date</label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.props.selectedData.map((data, index) =>
                                                <tr key={index}>
                                                    <td className="cot-read"><label>{data.design_no}</label></td>
                                                    <td className="cot-read"><label>{data.order_qty}</label></td>
                                                    <td className="cot-type onFocus">
                                                        <input type="text" readOnly value={this.state.errorRows[index] || this.state.selectedColor[index].color === "" ? "Select Color" : this.state.selectedColor[index].color} style={this.state.errorRows[index] ? {color: "#c44569"} : {}} />
                                                        <div className="modal-search-btn-table" onClick={(e) => this.openColorModal(e, index, data.design_no)}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                <path fill="#97abbf" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                            </svg>
                                                        </div>
                                                    </td>
                                                    <td className="cot-read"><label>{this.state.selectedColor[index].itemcode}</label></td>
                                                    <td className="cot-type onFocus">
                                                        <input type="text" readOnly value="Select HSN" />
                                                        <div className="modal-search-btn-table">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                <path fill="#97abbf" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                            </svg>
                                                        </div>
                                                    </td>
                                                    <td className="cot-read"><label>{this.state.selectedColor[index].rate}</label></td>
                                                    <td className="cot-read"><label>{this.state.selectedColor[index].qty}</label></td>
                                                    <td className="cot-read"><label>{this.state.selectedColor[index].totalQty}</label></td>
                                                    <td className="cot-read"><label>{this.state.selectedColor[index].deliveryDate}</label></td>
                                                </tr>
                                            )}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.colorModal && <VendorColorModal {...this.props} formIndex={this.state.formIndex} designNo={this.state.designNo} selectedColor={this.state.selectedColor} setSelectedColor={this.setSelectedColor} closeColorModal={this.closeColorModal} />}
            </div>
        )
    }
}
export default CreateOrder;