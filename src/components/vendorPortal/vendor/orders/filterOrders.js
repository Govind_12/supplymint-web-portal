import React from "react";

class FilterOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 1,
            no: 1,
            poNumber: this.props.poNumber,
            status: this.props.status,
            vendorName: this.props.vendorName,
            vendorId: this.props.vendorId,
            vendorCity: this.props.vendorCity,
            vendorState: this.props.vendorState,
            poDate: this.props.poDate,
            userType: this.props.userType,
            validTo: this.props.validTo,
            siteDetail: this.props.siteDetail,
            count: this.props.filterCount,
            orderCode : this.props.orderCode
        };
    }
    componentWillMount() {
        this.setState({
            poNumber: this.props.poNumber,
            status: this.props.status,
            vendorName: this.props.vendorName,
            userType: this.props.userType,
            vendorId: this.props.vendorId,
            vendorCity: this.props.vendorCity,
            vendorState: this.props.vendorState,
            poDate: this.props.poDate,
            validTo: this.props.validTo,
            siteDetail: this.props.siteDetail,
            count: this.props.filterCount,
            orderCode : this.props.orderCode
        })
    }

    handleChange(e) {
        if (e.target.id == "poNumber") {
            this.setState({
                poNumber: e.target.value
            });
        } else if (e.target.id == "vendorName") {
            this.setState({
                vendorName: e.target.value
            })

        } else if (e.target.id == "vendorId") {
            this.setState({
                vendorId: e.target.value
            });
        } else if (e.target.id == "vendorCity") {
            this.setState({
                vendorCity: e.target.value
            });
        } else if (e.target.id == "poDate") {

            this.setState({
                poDate: e.target.value
            });

        } else if (e.target.id == "vendorState") {
            this.setState({
                vendorState: e.target.value
            })

        } else if (e.target.id == "validTo") {
            this.setState({
                validTo: e.target.value
            })

        } else if (e.target.id == "siteDetail") {
            this.setState({
                siteDetail: e.target.value
            });
        }else if(e.target.id == "orderCode"){
            this.setState({orderCode : e.target.value})
        }
    }

    clearFilter(count) {
        this.setState({
            poNumber: "",
            status: "",
            vendorName: "",
            vendorId: "",
            vendorCity: "",
            vendorState: "",
            poDate: "",
            validTo: "",
            siteDetail: "",
            count: 0,
            orderCode : ""
        })
        this.props.filterCount != 0 ? this.props.clearFilter() : null
    }

    onSubmit(count) {
        // e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            poNumber: this.state.poNumber,
            status: this.state.status,
            vendorName: this.state.vendorName,
            vendorId: this.state.vendorId,
            vendorCity: this.state.vendorCity,
            poDate: this.state.poDate,
            vendorState: this.state.vendorState,
            validTo: this.state.validTo,
            siteDetail: this.state.siteDetail,
            userType: this.props.userType,
            filterCount: count,
            orderCode : this.state.orderCode
        }
        this.props.EnterpriseApprovedPoRequest(data);
        this.props.closeFilter();
        this.props.updateFilter(data)
    }

    render() {
        let count = 0;

        if (this.state.poNumber != "") {
            count++;
        }
        if (this.state.vendorName != "") {
            count++
        }
        if (this.state.vendorId != "") {
            count++
        }
        if (this.state.vendorCity != "") {
            count++
        }
        if (this.state.poDate != "") {
            count++;
        }
        if (this.state.vendorState != "") {
            count++;
        }
        if (this.state.siteDetail != "") {
            count++;
        }
        if (this.state.validTo != "") {
            count++;
        }
        if (this.state.orderCode != "") {
            count++;
        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(count)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS
                                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                                     </label>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">
                                <ul className="list-inline m-top-20">
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="poNumber" value={this.state.poNumber} placeholder="PO Number" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorName" value={this.state.vendorName} placeholder="Vendor Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorId" value={this.state.vendorId} placeholder="Vendor Id" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorCity" value={this.state.vendorCity} placeholder="Vendor City" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorState" value={this.state.vendorState} placeholder="Vendor State" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="poDate" value={this.state.poDate} placeholder={this.state.poDate != "" ? this.state.poDate : "PO Date"} className="organistionFilterModal" />
                                    </li>
                                    
                                </ul></div></div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">
                                <ul className="list-inline m-top-20">
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="validTo" value={this.state.validTo} placeholder={this.state.validTo != "" ? this.state.validTo : "Valid To"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="siteDetail" value={this.state.siteDetail} placeholder="Site Details" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="orderCode" value={this.state.orderCode} placeholder="Order Code" className="organistionFilterModal" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.state.vendorId == "" && this.state.orderCode == "" && this.state.poNumber == "" && this.state.vendorName == "" && this.state.vendorCity == "" && this.state.vendorState == "" && this.state.poDate == "" && this.state.validTo == "" && this.state.siteDetail == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button type="button" onClick={(e) => this.clearFilter(0)} className="modal_clear_btn">
                                                CLEAR FILTER
                                         </button>}
                                    </li>
                                    <li>
                                        {this.state.vendorId != ""||this.state.orderCode != "" || this.state.poNumber != "" || this.state.vendorName != "" || this.state.vendorCity != "" || this.state.vendorState != "" || this.state.poDate != "" || this.state.validTo != "" || this.state.siteDetail != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default FilterOrders;
