import React from "react";
class LocationModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            locationSearch: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            locationState: [],
        }
    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }
    componentWillMount() {
        this.setState({
            locationSearch: this.props.isModalShow ? "" : this.props.locationSearch,
            type: this.props.isModalShow || this.props.locationSearch == "" ? 1 : 3
        })
        if (this.props.orders.getLocationData.isSuccess) {

            if (this.props.orders.getLocationData.data.resource != null) {

                this.setState({
                    locationState: this.props.orders.getLocationData.data.resource,
                    prev: this.props.orders.getLocationData.data.prePage,
                    current: this.props.orders.getLocationData.data.currPage,
                    next: this.props.orders.getLocationData.data.currPage + 1,
                    maxPage: this.props.orders.getLocationData.data.maxPage,
                })
            } else {
                this.setState({
                    locationState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.getLocationData.isSuccess) {

            if (nextProps.orders.getLocationData.data.resource != null) {

                this.setState({
                    locationState: nextProps.orders.getLocationData.data.resource,
                    prev: nextProps.orders.getLocationData.data.prePage,
                    current: nextProps.orders.getLocationData.data.currPage,
                    next: nextProps.orders.getLocationData.data.currPage + 1,
                    maxPage: nextProps.orders.getLocationData.data.maxPage,
                })
            } else {
                this.setState({
                    locationState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            if (window.screen.width > 1200) {
                if (!this.props.isModalShow) {
                    if (nextProps.orders.getLocationData.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.orders.getLocationData.data.resource[0].code,
                            locationSearch: this.props.isModalShow ? "" : this.props.locationSearch,
                            type: this.props.isModalShow || this.props.locationSearch == "" ? 1 : 3

                        })
                        document.getElementById(nextProps.orders.getLocationData.data.resource[0].code) != null ? document.getElementById(nextProps.orders.getLocationData.data.resource[0].code).focus() : null
                    }


                }
            }

        }
        
    }
    closeLocationModal(e) {
        this.props.onCloseLocationModal()
        const t = this

        t.setState({
            locationState: [],
            locationSearch: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })

        // document.getElementById("vendorBasedOn").value = ""

    }
    selectedData = (baseLocation,location,email) => {
        this.props.locationVal(baseLocation,location,email)
    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.locationState.length != 0 ?
                            document.getElementById(this.state.locationState[0].code).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.locationState.length != 0) {
                    document.getElementById(this.state.locationState[0].code).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.closeLocationModal(e)
        }
    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.orders.getLocationData.data.prePage,
                current: this.props.orders.getLocationData.data.currPage,
                next: this.props.orders.getLocationData.data.currPage + 1,
                maxPage: this.props.orders.getLocationData.data.maxPage,
            })
            if (this.props.orders.getLocationData.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    pageNo: this.props.orders.getLocationData.data.currPage - 1,
                    search: this.state.vendorSearch,
                    sortedBy: "",
                    sortedIn: ""
                }
                this.props.getLocationRequest(data)
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.orders.getLocationData.data.prePage,
                current: this.props.orders.getLocationData.data.currPage,
                next: this.props.orders.getLocationData.data.currPage + 1,
                maxPage: this.props.orders.getLocationData.data.maxPage,
            })
            if (this.props.orders.getLocationData.data.currPage != this.props.orders.getLocationData.data.maxPage) {
                let data = {
                    type: this.state.type,
                    pageNo: this.props.orders.getLocationData.data.currPage + 1,
                    search: this.state.vendorSearch,
                    sortedBy: "",
                    sortedIn: ""
                }
                this.props.getLocationRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.orders.getLocationData.data.prePage,
                current: this.props.orders.getLocationData.data.currPage,
                next: this.props.orders.getLocationData.data.currPage + 1,
                maxPage: this.props.orders.getLocationData.data.maxPage,
            })
            if (this.props.orders.getLocationData.data.currPage <= this.props.orders.getLocationData.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.vendorSearch,
                    sortedBy: "",
                    sortedIn: ""

                }
                this.props.getLocationRequest(data)
            }

        }
    }
    render(){
        return (
            <div className="gen-drop-search width810">
            <div className="gds-headers">
            <span className="div-col-5 gdsh-text">Insp. Base Location</span>
            <span className="div-col-5 gdsh-text">Insp. Location Short Name</span>
            <span className="gdsh-text gdsbi-email">Email</span>
            <span className="gdsh-text gdsbi-cont">Contact Person</span>
            <span className="gdsh-text gdsbi-num">Contact Number</span>
            </div>
            <ul className="gds-body-items">
            {this.state.locationState == undefined || this.state.locationState.length == 0 ? <li><span colSpan="4"> NO DATA FOUND </span></li> : this.state.locationState.length == 0 ? <li><span colSpan="4"> NO DATA FOUND </span></li> : this.state.locationState.map((data, key) => (
                <li key={key} onClick={(e) => this.selectedData(data.baseLocation,data.location,data.email)}>
                <span className="gdsbi-text div-col-5" id='li'>{data.baseLocation}</span>
                <span className="gdsbi-text div-col-5" id='li'>{data.shortName}</span>
                <span className="gdsbi-text gdsbi-email" id='li'>{data.email}</span>
                <span className="gdsbi-text gdsbi-cont" id='li'>{data.contactPerson}</span>
                <span className="gdsbi-text gdsbi-num" id='li'>{data.contactNo}</span>
                </li>
                 ))}
            </ul>
            {/* <div className="gen-dropdown-pagination">
                <div className="page-close">
                    <button className="btn-close" type="button" id="btn-close">Close</button>
                </div>
                <div className="page-next-prew-btn">
                    <button className="pnpb-prev" type="button" id="prev">
                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                        </svg>
                    </button>
                    <button className="pnpb-no" type="button" id='li'>0/0</button>
                    <button className="pnpb-next" type="button"  id="next">
                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                            <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                        </svg>
                    </button>
                </div>
            </div> */}
            <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" onClick={(e) => this.closeLocationModal(e)} id="btn-close">Close</button>
                    </div>
                    <div className="page-next-prew-btn">
                        {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg></button>}
                        <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button>
                            : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg>
                            </button> : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg></button>}
                    </div>
                </div>
        </div>
        )
    }
}
export default LocationModal;