import React from 'react';
import CreateOrder from './createOrderModal';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';

class OrderRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tablePayload: {
                pageNo: 1,
                type: 1,
                sortedBy: "",
                sortedIn: "DESC",
                search: "",
                filter: {}
            },
            tableHeader: {},
            tableData: [],
            deliverySiteData: [],
            previousPage: 0,
            currentPage: 1,
            nextPage: 1,
            maxPage: 1,
            jumpPage: 1,
            totalItems: 0,
            selectedData: [],

            toastMsg: "",
            toastLoader: false,
            createOrderModal: false
        }
    }

    componentDidMount() {
        this.props.getMainHeaderConfigRequest({
            enterpriseName: "TURNINGCLOUD",
            attributeType: "VENDOR_TABLE_TABLE",
            displayName: "TABLE HEADER"
        });
        this.props.getPoStockReportRequest(this.state.tablePayload);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            return {
                tableHeader: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]
            };
        }

        if (nextProps.orders.getPoStockReport.isSuccess) {
            return {
                tableData: nextProps.orders.getPoStockReport.data.resource === null ? [] : nextProps.orders.getPoStockReport.data.resource,
                deliverySiteData: nextProps.orders.getPoStockReport.data.deliverySite === null ? [] : nextProps.orders.getPoStockReport.data.deliverySite,
                previousPage: isNaN(parseInt(nextProps.orders.getPoStockReport.data.prePage)) ? 0 : parseInt(nextProps.orders.getPoStockReport.data.prePage),
                currentPage: isNaN(parseInt(nextProps.orders.getPoStockReport.data.currPage)) ? 1 : parseInt(nextProps.orders.getPoStockReport.data.currPage),
                nextPage: isNaN(parseInt(nextProps.orders.getPoStockReport.data.currPage)) ? 1 : parseInt(nextProps.orders.getPoStockReport.data.currPage) + 1,
                maxPage: isNaN(parseInt(nextProps.orders.getPoStockReport.data.maxPage))? 1 : parseInt(nextProps.orders.getPoStockReport.data.maxPage),
                jumpPage: isNaN(parseInt(nextProps.orders.getPoStockReport.data.currPage))? 1 : parseInt(nextProps.orders.getPoStockReport.data.currPage),
                totalItems: isNaN(parseInt(nextProps.orders.getPoStockReport.data.totalItems))? 0 : parseInt(nextProps.orders.getPoStockReport.data.totalItems)
            };
        }

        if (nextProps.orders.createPoArticle.isSuccess) {
            return {
                selectedData: [],
                createOrderModal: false
            };
        } 

        return null;
    }

    componentDidUpdate() {
        if (this.props.replenishment.getMainHeaderConfig.isSuccess || this.props.replenishment.getMainHeaderConfig.isError) {
            this.props.getMainHeaderConfigClear();
        }
        if (this.props.orders.getPoStockReport.isSuccess || this.props.orders.getPoStockReport.isError) {
            this.props.getPoStockReportClear();
        }
        if (this.props.orders.createPoArticle.isSuccess || this.props.orders.createPoArticle.isError) {
            this.props.createPoArticleClear();
        }
    }

    page = (e) => {
        if (e.target.id === "first") {
            if (this.state.currentPage !== 1) {
                this.props.getPoStockReportRequest({
                    ...this.state.tablePayload,
                    pageNo: 1
                });
            }
        }
        else if (e.target.id === "prev") {
            if (this.state.currentPage !== 1) {
                this.props.getPoStockReportRequest({
                    ...this.state.tablePayload,
                    pageNo: this.state.previousPage
                });
            }
        }
        else if (e.target.id === "next") {
            if (this.state.currentPage !== this.state.maxPage) {
                this.props.getPoStockReportRequest({
                    ...this.state.tablePayload,
                    pageNo: this.state.nextPage
                });
            }
        }
        else if (e.target.id === "last") {
            if (this.state.currentPage !== this.state.maxPage) {
                this.props.getPoStockReportRequest({
                    ...this.state.tablePayload,
                    pageNo: this.state.maxPage
                });
            }
        }
    }

    getAnyPage = e => {
        if (e.target.validity.valid) {
            this.setState({
                jumpPage: parseInt(e.target.value)
            });
            if (e.key === "Enter" && parseInt(e.target.value) !== this.state.current) {
                if (e.target.value !== "") {
                    this.props.getPoStockReportRequest({
                        ...this.state.tablePayload,
                        pageNo: e.target.value
                    });
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 5000);
                }
            }
        }
    }

    onReload = () => {
        this.setState({
            tablePayload: {
                pageNo: 1,
                type: 1,
                sortedBy: "",
                sortedIn: "DESC",
                search: "",
                filter: {}
            }
        }, () => {
            this.props.getMainHeaderConfigRequest({
                enterpriseName: "TURNINGCLOUD",
                attributeType: "VENDOR_TABLE_TABLE",
                displayName: "TABLE HEADER"
            });
            this.props.getPoStockReportRequest(this.state.tablePayload);
        });
    }

    sortData = (key) => {
        this.props.getPoStockReportRequest({
            ...this.state.tablePayload,
            sortedBy: key,
            sortedIn: this.state.tablePayload.sortedBy === key && this.state.tablePayload.sortedIn === "ASC" ? "DESC" : "ASC" 
        });
        this.setState((prevState) => ({
            tablePayload: {
                ...prevState.tablePayload,
                sortedBy: key,
                sortedIn: prevState.tablePayload.sortedBy === key && prevState.tablePayload.sortedIn === "ASC" ? "DESC" : "ASC"
            }
        }));
    }

    selectData = (data) => {
        let selectedData = [...this.state.selectedData];
        let index = selectedData.indexOf(data);
        if (index === -1) {
            selectedData.push(data);
        }
        else {
            selectedData.splice(index, 1);
        }
        this.setState({
            selectedData
        });
    }

    openCreateOrder = (e) => {
        e.preventDefault();
        this.setState({
            createOrderModal: !this.state.createOrderModal,
        });
    }
    closeCreateOrder = () => {
        this.setState({
            createOrderModal: false,
        });
    }

    render() {
        return (
            <div className="container-fluid p-lr-0">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <button className={this.state.selectedData.length === 0 ? "btnDisabled" : ""} disabled={this.state.selectedData.length === 0 ? "disabled" : ""} onClick={() => this.setState({selectedData: []})}>Cancel</button>
                                <button className={this.state.selectedData.length === 0 ? "gen-save btnDisabled" : "gen-save"} disabled={this.state.selectedData.length === 0 ? "disabled" : ""} onClick={(e) => this.openCreateOrder(e)}>Create Order</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table">
                        <div className="manage-table">
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn width40">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span onClick={this.onReload}><img src={require('../../../../assets/refresh-block.svg')}></img></span>
                                                </li>
                                            </ul>
                                        </th>
                                        {Object.keys(this.state.tableHeader).length === 0 ? null : 
                                        Object.keys(this.state.tableHeader).map(key =>
                                            <th key={key} className={this.state.sortedBy === key && this.state.sortedIn === "ASC" ? "rotate180" : ""} onClick={() => this.sortData(key)}><label>{this.state.tableHeader[key]}</label><img src={require('../../../../assets/headerFilter.svg')} /></th>
                                        )}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.tableData.length === 0 ?
                                    Object.keys(this.state.tableHeader).length === 0 ?
                                    <tr><td align="center"><label>No data found!</label></td></tr> :
                                    <tr><td align="center" colSpan={Object.keys(this.state.tableHeader).length + 1}><label>No data found!</label></td></tr> :
                                    this.state.tableData.map((data, index) =>
                                        <tr key={index}>
                                            <td className="fix-action-btn width40">
                                                <ul className="table-item-list">
                                                    <li className="til-inner">
                                                        <label className="checkBoxLabel0">
                                                            <input type="checkBox" name="selectEach" checked={this.state.selectedData.includes(data)} onChange={() => this.selectData(data)} />
                                                            <span className="checkmark1"></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </td>
                                            {Object.keys(this.state.tableHeader).map(key =>
                                                <td><label>{data[key]}</label></td>
                                            )}
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page:</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items: </span> <span className="bold">{this.state.totalItems}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.previousPage} current={this.state.currentPage} maxPage={this.state.maxPage} next={this.state.nextPage} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.createOrderModal && <CreateOrder {...this.props} deliverySiteData={this.state.deliverySiteData} selectedData={this.state.selectedData} closeCreateOrder={this.closeCreateOrder} />}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default OrderRequest;