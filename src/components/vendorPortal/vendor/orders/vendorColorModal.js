import React from 'react';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';

class VendorColorModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tablePayload: {
                desinNo: [this.props.designNo.toString()],
                pageNo: 1,
                type: 1,
                sortedBy: "",
                sortedIn: "DESC",
                search: "",
                filter: {}
            },
            tableHeader: {},
            tableData: [],
            previousPage: 0,
            currentPage: 1,
            nextPage: 1,
            maxPage: 1,
            jumpPage: 1,
            totalItems: 0,
            selectedData: [],
            quantity: {},
            errorQuantity: {},
            deliveryDate: {},
            errorDate: {},

            toastMsg: "",
            toastLoader: false,

            search: ""
        }
    }

    componentDidMount() {
        this.props.getPoColorMapRequest(this.state.tablePayload);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.orders.getPoColorMap.isSuccess) {
            if (nextProps.orders.getPoColorMap.data.resource !== null) {
                let itemcodes = nextProps.selectedColor[nextProps.formIndex].itemcode.split(", ");
                let quantities = nextProps.selectedColor[nextProps.formIndex].qty.split(", ");
                let dates = nextProps.selectedColor[nextProps.formIndex].deliveryDate.split(", ");
                let selectedData = [];
                let quantity = {};
                let errorQuantity = {};
                let deliveryDate = {};
                let errorDate = {};
                nextProps.orders.getPoColorMap.data.resource.forEach((data, index) => {
                    if (itemcodes.includes(data.itemcode)) {
                        selectedData.push(data);
                        quantity[data.itemcode] = quantities[itemcodes.indexOf(data.itemcode)];
                        errorQuantity[data.itemcode] = false;
                        deliveryDate[data.itemcode] = dates[itemcodes.indexOf(data.itemcode)];
                        errorDate[data.itemcode] = false;
                    }
                    else {
                        let today = new Date();
                        today.setDate(today.getDate() + 7)
                        deliveryDate[data.itemcode] = today.toISOString().split("T")[0];
                        errorDate[data.itemcode] = false;
                    }
                });
                return {
                    tableData: nextProps.orders.getPoColorMap.data.resource,
                    previousPage: isNaN(parseInt(nextProps.orders.getPoColorMap.data.prePage)) ? 0 : parseInt(nextProps.orders.getPoColorMap.data.prePage),
                    currentPage: isNaN(parseInt(nextProps.orders.getPoColorMap.data.currPage)) ? 1 : parseInt(nextProps.orders.getPoColorMap.data.currPage),
                    nextPage: isNaN(parseInt(nextProps.orders.getPoColorMap.data.currPage)) ? 1 : parseInt(nextProps.orders.getPoColorMap.data.currPage) + 1,
                    maxPage: isNaN(parseInt(nextProps.orders.getPoColorMap.data.maxPage))? 1 : parseInt(nextProps.orders.getPoColorMap.data.maxPage),
                    jumpPage: isNaN(parseInt(nextProps.orders.getPoColorMap.data.currPage))? 1 : parseInt(nextProps.orders.getPoColorMap.data.currPage),
                    totalItems: isNaN(parseInt(nextProps.orders.getPoColorMap.data.totalItems))? 0 : parseInt(nextProps.orders.getPoColorMap.data.totalItems),
                    selectedData: selectedData,
                    quantity: quantity,
                    errorQuantity: errorQuantity,
                    deliveryDate: deliveryDate,
                    errorDate: errorDate
                }
            }
        }

        return null;
    }

    componentDidUpdate() {
        if (this.props.orders.getPoColorMap.isSuccess) {
            this.props.getPoColorMapClear();
        }
        else if (this.props.orders.getPoColorMap.isError) {
            this.props.getPoColorMapClear();
            this.props.closeColorModal();
        }
    }

    page = (e) => {
        if (e.target.id === "first") {
            if (this.state.currentPage !== 1) {
                this.props.getPoColorMapRequest({
                    ...this.state.tablePayload,
                    pageNo: 1
                });
            }
        }
        else if (e.target.id === "prev") {
            if (this.state.currentPage !== 1) {
                this.props.getPoColorMapRequest({
                    ...this.state.tablePayload,
                    pageNo: this.state.previousPage
                });
            }
        }
        else if (e.target.id === "next") {
            if (this.state.currentPage !== this.state.maxPage) {
                this.props.getPoColorMapRequest({
                    ...this.state.tablePayload,
                    pageNo: this.state.nextPage
                });
            }
        }
        else if (e.target.id === "last") {
            if (this.state.currentPage !== this.state.maxPage) {
                this.props.getPoColorMapRequest({
                    ...this.state.tablePayload,
                    pageNo: this.state.maxPage
                });
            }
        }
    }

    getAnyPage = e => {
        if (e.target.validity.valid) {
            this.setState({
                jumpPage: parseInt(e.target.value)
            });
            if (e.key === "Enter" && parseInt(e.target.value) !== this.state.current) {
                if (e.target.value !== "") {
                    this.props.getPoColorMapRequest({
                        ...this.state.tablePayload,
                        pageNo: e.target.value
                    });
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 5000);
                }
            }
        }
    }

    // sortData = (key) => {
    //     this.props.getPoColorMapRequest({
    //         ...this.state.tablePayload,
    //         sortedBy: key,
    //         sortedIn: this.state.tablePayload.sortedBy === key && this.state.tablePayload.sortedIn === "ASC" ? "DESC" : "ASC" 
    //     });
    //     this.setState((prevState) => ({
    //         tablePayload: {
    //             ...prevState.tablePayload,
    //             sortedBy: key,
    //             sortedIn: prevState.tablePayload.sortedBy === key && prevState.tablePayload.sortedIn === "ASC" ? "DESC" : "ASC"
    //         }
    //     }));
    // }

    selectData = (data) => {
        let selectedData = [...this.state.selectedData];
        let quantity = {...this.state.quantity};
        let errorQuantity = {...this.state.errorQuantity};
        let index = selectedData.indexOf(data);
        if (index === -1) {
            selectedData.push(data);
            quantity[data.itemcode] = "";
            errorQuantity[data.itemcode] = false;
        }
        else {
            selectedData.splice(index, 1);
            delete quantity[data.itemcode];
            delete errorQuantity[data.itemcode];
        }
        this.setState({
            selectedData,
            quantity,
            errorQuantity
        });
    }

    handleQuantity = (e, data) => {
        let quantity = {...this.state.quantity};
        quantity[data.itemcode] = e.target.value;
        this.setState({
            quantity
        });
        this.handleErrorQuantity(e.target.value, data)
    }

    handleErrorQuantity = (qty, data) => {
        let errorQuantity = {};
        if (qty === "" || !/^[0-9]*$/.test(qty) || parseInt(qty) <= 0) {// || parseInt(qty) > parseInt(data.qty)) {
            errorQuantity[data.itemcode] = true;
            this.setState((prevState) => ({
                errorQuantity: {...prevState.errorQuantity, ...errorQuantity}
            }));
            return true;
        }
        else {
            errorQuantity[data.itemcode] = false;
            this.setState((prevState) => ({
                errorQuantity: {...prevState.errorQuantity, ...errorQuantity}
            }));
            return false;
        }
    }

    handleDate = (e, data) => {
        let deliveryDate = {...this.state.deliveryDate};
        deliveryDate[data.itemcode] = e.target.value;
        this.setState({
            deliveryDate
        });
        this.handleErrorDate(e.target.value, data)
    }

    handleErrorDate = (date, data) => {
        let errorDate = {};
        date = new Date(date);
        let today = new Date();
        if (date === "" || date.getTime() < today.getTime()) {
            errorDate[data.itemcode] = true;
            this.setState((prevState) => ({
                errorDate: {...prevState.errorDate, ...errorDate}
            }));
            return true;
        }
        else {
            errorDate[data.itemcode] = false;
            this.setState((prevState) => ({
                errorDate: {...prevState.errorDate, ...errorDate}
            }));
            return false;
        }
    }

    setSelectedColor = () => {
        let valid = true;
        Object.keys(this.state.quantity).forEach((key, index) => {
            if (this.handleErrorQuantity(this.state.quantity[key], this.state.selectedData[index]) || this.handleErrorDate(this.state.deliveryDate[key], this.state.selectedData[index])) {
                valid = false;
            }
        });
        if (valid) {
            let stringValue = {
                color: "",
                itemcode: "",
                rate: "",
                // hsn: "",
                qty: "",
                totalQty: 0,
                deliveryDate: ""
            };
            this.state.selectedData.forEach((data, index) => {
                if (index === this.state.selectedData.length - 1) {
                    stringValue.color += data.colour;
                    stringValue.itemcode += data.itemcode;
                    stringValue.rate += data.cost_rate;
                    // stringValue.hsn += data.hsn;
                    stringValue.qty += this.state.quantity[data.itemcode];
                    stringValue.deliveryDate += this.state.deliveryDate[data.itemcode];
                }
                else {
                    stringValue.color += data.colour + ", ";
                    stringValue.itemcode += data.itemcode + ", ";
                    stringValue.rate += data.cost_rate + ", ";
                    // stringValue.hsn += data.hsn + ", ";
                    stringValue.qty += this.state.quantity[data.itemcode] + ", ";
                    stringValue.deliveryDate += this.state.deliveryDate[data.itemcode] + ", ";
                }
                stringValue.totalQty += parseInt(this.state.quantity[data.itemcode]);
            });
            this.props.setSelectedColor(stringValue, this.props.formIndex);
            this.props.closeColorModal();
        }
    }

    render() {console.log(this.state);
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div class="modal-content modalpoColor modalShow gen-color-modal">
                    <div class="gcm-head">
                        <div class="gcmh-top">
                            <div class="gcmht-left">
                                <h3>Select Colour</h3>
                                <p>You can select multiple colors from below list</p>
                            </div>
                            <div class="gcmht-right">
                                <button type="button" class="gcmht-close" id="closeButton" onClick={this.props.closeColorModal}>Close</button>
                                <button type="button" class="gcmht-done" id="doneButton" onClick={this.setSelectedColor}>Done</button>
                            </div>
                        </div>
                        <div class="gcmh-bottom">
                            <div class="gcmhb-left">
                                <div class="gcmhb-search">
                                    <input type="search" autocomplete="off" autocorrect="off" id="colorSearch" placeholder="Type to search" value={this.state.search} onChange={(e) => this.setState({search: e.target.value})} />
                                    <button class="search-image">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                            <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="gcmhb-right">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 pad-0">
                        <div class="gcm-body">
                            <div class="gcm-table">
                                <div class="gcmt-manage">
                                    <table class="table gcmt-inner">
                                        <thead>
                                            <tr>
                                                <th class="fix-action-btn"></th>
                                                <th><label>Color</label></th>
                                                <th><label>ICode</label></th>
                                                <th><label>Rate</label></th>
                                                <th><label>Current Stock</label></th>
                                                <th><label>Quantity</label></th>
                                                <th><label>Delivery Date</label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.tableData.length === 0 ?
                                            <tr><td align="center" colSpan="7"><label>No data found!</label></td></tr> :
                                            this.state.tableData.map((data, index) =>
                                                data.colour.toLowerCase().includes(this.state.search.toLowerCase()) || data.itemcode.toLowerCase().includes(this.state.search.toLowerCase()) ?
                                                <tr key={index}>
                                                    <td class="fix-action-btn">
                                                        <ul class="table-item-list">
                                                            <li class="til-inner">
                                                                <label class="checkBoxLabel0">
                                                                    <input type="checkBox" onChange={() => this.selectData(data)} checked={this.state.selectedData.includes(data)} />
                                                                    <span class="checkmark1"></span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td><label>{data.colour}</label></td>
                                                    <td><label>{data.itemcode}</label></td>
                                                    <td><label>{data.cost_rate}</label></td>
                                                    <td><label>{data.qty}</label></td>
                                                    <td>
                                                        <input type="number" className={this.state.errorQuantity[data.itemcode] ? "modal_tableBox errorBorder" : "modal_tableBox"} value={this.state.quantity[data.itemcode] === undefined ? "" : this.state.quantity[data.itemcode]} onClick={() => {this.state.selectedData.includes(data) ? "" : this.selectData(data)}} onChange={(e) => this.handleQuantity(e, data)} />
                                                        {this.state.errorQuantity[data.itemcode] ? <span className="error">Quantity must be between 1 and {data.qty}</span> : null}    
                                                    </td>
                                                    <td>
                                                        <input type="date" className={this.state.errorDate[data.itemcode] ? "pnl-purchase-date errorBorder" : "pnl-purchase-date onFocus"} placeholder={this.state.deliveryDate[data.itemcode]} value={this.state.deliveryDate[data.itemcode]} onChange={(e) => this.handleDate(e, data)} />
                                                        {this.state.errorDate[data.itemcode] ? <span className="error">Select valid date</span> : null}    
                                                    </td>
                                                </tr> :
                                                null
                                            )}
                                        </tbody>
                                    </table>
                                </div>
                                {/* <div className="col-md-12 pad-0" >
                                    <div className="new-gen-pagination">
                                        <div className="ngp-left">
                                            <div className="table-page-no">
                                                <span>Page:</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                                <span className="ngp-total-item">Total Items: </span> <span className="bold">{this.state.totalItems}</span>
                                            </div>
                                        </div>
                                        <div className="ngp-right">
                                            <div className="nt-btn">
                                                <Pagination {...this.state} {...this.props} page={this.page}
                                                    prev={this.state.previousPage} current={this.state.currentPage} maxPage={this.state.maxPage} next={this.state.nextPage} />
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}
export default VendorColorModal;