import React from "react";
class AddressModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addressSearch: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            addressState: [],
        }
    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }
    componentWillMount() {
        this.setState({
            addressSearch: this.props.isModalShow ? "" : this.props.addressSearch,
            type: this.props.isModalShow || this.props.addressSearch == "" ? 1 : 3
        })
        if (this.props.orders.getAddressData.isSuccess) {

            if (this.props.orders.getAddressData.data.resource != null) {

                this.setState({
                    addressState: this.props.orders.getAddressData.data.resource,
                    prev: this.props.orders.getAddressData.data.prePage,
                    current: this.props.orders.getAddressData.data.currPage,
                    next: this.props.orders.getAddressData.data.currPage + 1,
                    maxPage: this.props.orders.getAddressData.data.maxPage,
                })
            } else {
                this.setState({
                    addressState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.getAddressData.isSuccess) {
            if (nextProps.orders.getAddressData.data.resource != null) {
                this.setState({
                    addressState: nextProps.orders.getAddressData.data.resource,
                    prev: nextProps.orders.getAddressData.data.prePage,
                    current: nextProps.orders.getAddressData.data.currPage,
                    next: nextProps.orders.getAddressData.data.currPage + 1,
                    maxPage: nextProps.orders.getAddressData.data.maxPage,
                })
            } else {
                this.setState({
                    addressState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            if (window.screen.width > 1200) {
                if (!this.props.isModalShow) {
                    if (nextProps.orders.getAddressData.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.orders.getAddressData.data.resource[0].address,
                            addressSearch: this.props.isModalShow ? "" : this.props.addressSearch,
                            type: this.props.isModalShow || this.props.addressSearch == "" ? 1 : 3

                        })
                        document.getElementById(nextProps.orders.getAddressData.data.resource[0].address) != null ? document.getElementById(nextProps.orders.getAddressData.data.resource[0].address).focus() : null
                    }


                }
            }

        }
        
    }
    closeAddressModal(e) {
        this.props.onCloseAddressModal()
        const t = this

        t.setState({
            addressState: [],
            addressSearch: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })

        // document.getElementById("vendorBasedOn").value = ""

    }
    selectedData = (city, address) => {
        this.props.addressVal(city, address)
    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.addressState.length != 0 ?
                            document.getElementById(this.state.addressState[0].address).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.addressState.length != 0) {
                    document.getElementById(this.state.addressState[0].address).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.closeAddressModal(e)
        }

    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.orders.getAddressData.data.prePage,
                current: this.props.orders.getAddressData.data.currPage,
                next: this.props.orders.getAddressData.data.currPage + 1,
                maxPage: this.props.orders.getAddressData.data.maxPage,
            })
            if (this.props.orders.getAddressData.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    pageNo: this.props.orders.getAddressData.data.currPage - 1,
                    search: this.state.vendorSearch,
                    sortedBy: "",
                    sortedIn: ""
                }
                this.props.getAddressRequest(data)
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.orders.getAddressData.data.prePage,
                current: this.props.orders.getAddressData.data.currPage,
                next: this.props.orders.getAddressData.data.currPage + 1,
                maxPage: this.props.orders.getAddressData.data.maxPage,
            })
            if (this.props.orders.getAddressData.data.currPage != this.props.orders.getAddressData.data.maxPage) {
                let data = {
                    type: this.state.type,
                    pageNo: this.props.orders.getAddressData.data.currPage + 1,
                    search: this.state.vendorSearch,
                    sortedBy: "",
                    sortedIn: ""
                }
                this.props.getAddressRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.orders.getAddressData.data.prePage,
                current: this.props.orders.getAddressData.data.currPage,
                next: this.props.orders.getAddressData.data.currPage + 1,
                maxPage: this.props.orders.getAddressData.data.maxPage,
            })
            if (this.props.orders.getAddressData.data.currPage <= this.props.orders.getAddressData.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.vendorSearch,
                    sortedBy: "",
                    sortedIn: ""

                }
                this.props.getAddressRequest(data)
            }

        }
    }
    selectLi(e, code,city) {
        
        let addressState = this.state.addressState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < addressState.length; i++) {
                if (addressState[i].code == code) {
                    index = i
                }
            }
            if (index < addressState.length - 1 || index == 0) {
                document.getElementById(addressState[index + 1].code) != null ? document.getElementById(addressState[index + 1].code).focus() : null

                this.setState({
                    focusedLi: addressState[index + 1].code
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < addressState.length; i++) {
                if (addressState[i].code == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(addressState[index - 1].code) != null ? document.getElementById(addressState[index - 1].code).focus() : null

                this.setState({
                    focusedLi: addressState[index - 1].code
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(city,code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.closeSupplier(e)
        }

    }
    render(){
        return (
            <div className="gen-drop-search width800">
                <div className="gds-headers">
                    <span className="div-col-5">Short Name</span>
                    <span className="div-col-5">Address</span>
                    <span className="div-col-5">City</span>
                    <span className="div-col-5">State</span>
                </div>
                <ul className="gds-body-items">
                {this.state.addressState == undefined || this.state.addressState.length == 0 ? <li><span colSpan="4"> NO DATA FOUND </span></li> : this.state.addressState.length == 0 ? <li><span colSpan="4"> NO DATA FOUND </span></li> : this.state.addressState.map((data, key) => (
                    <li id={data.address} key={key} onClick={(e) => this.selectedData(data.city,`${data.address}`)} onKeyDown={(e) => this.selectLi(e, data.address,data.city)}>
                        <span className="gdsbi-text div-col-5" id='li'>{data.shortName}</span>
                        <span className="gdsbi-text div-col-5" id='li'>{data.address}</span>
                        <span className="gdsbi-text div-col-5" id='li'>{data.city}</span>
                        <span className="gdsbi-text div-col-5" id='li'>{data.state}</span>
                    </li>
                    ))}
                </ul>
                {/* <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" id="btn-close" onClick={(e) => this.closeAddressModal(e)}>Close</button>
                    </div>
                    <div className="page-next-prew-btn">
                        <button className="pnpb-prev" type="button" id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button>
                        <button className="pnpb-no" type="button" id='li'>0/0</button>
                        <button className="pnpb-next" type="button"  id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button>
                    </div>
                </div> */}
                <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" onClick={(e) => this.closeAddressModal(e)} id="btn-close">Close</button>
                    </div>
                    <div className="page-next-prew-btn">
                        {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg></button>}
                        <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button>
                            : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg>
                            </button> : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg></button>}
                    </div>
                </div>
            </div>
        )
    }
}
export default AddressModal;