import React, { Component } from 'react'
import Pagination from '../../../pagination';
import searchIcon from '../../../../assets/close-recently.svg';
import plusIcon from '../../../../assets/plus-blue.svg';
import exclaimIcon from '../../../../assets/exclain.svg';
import ColoumSetting from '../../../replenishment/coloumSetting';
import ConfirmationSummaryModal from '../../../replenishment/confirmationReset';
import moment from 'moment';
import SetLevelConfigHeader from '../../setLevelConfigHeader'
import SetLevelConfirmationModal from '../../setLevelConfirModal'
import ItemLevelDetails from '../../modals/itemLevelDetails';
import removeIcon from '../../../../assets/removeIcon.svg';
import { CONFIG } from "../../../../config/index";
import axios from 'axios';
import LocationModal from './locationModal';
import AddressModal from './addressModal';
import FilterLoader from '../../../loaders/filterLoader';
import clearSearch from '../../../../assets/clearSearch.svg';
import CreateAsnUpload from './createAsnUpload';

const userType = "entpo";

const todayDate = moment(new Date()).format("YYYY-MM-DD");

export default class VendorExpandDetailTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: "",
            no: 1,
            type: 1,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            actionExpand: false,
            expandedId: 0,
            dropOpen: true,
            setBaseExpandDetails: [],
            setRequestedQty: "",
            setBasedArray: [],
            setLvlGetHeaderConfig: [],
            setLvlFixedHeader: [],
            setLvlCustomHeaders: {},
            setLvlHeaderConfigState: {},
            setLvlHeaderConfigDataState: {},
            setLvlFixedHeaderData: [],
            setLvlCustomHeadersState: [],
            setLvlHeaderState: {},
            setLvlHeaderSummary: [],
            setLvlDefaultHeaderMap: [],
            setLvlConfirmModal: false,
            setLvlHeaderMsg: "",
            setLvlParaMsg: "",
            setLvlHeaderCondition: false,
            setLvlSaveState: [],
            setLvlColoumSetting: false,
            itemBaseArray: [],
            expandedLineItems: {},
            qcFromDate: "",
            qcFromDateErr: "",
            qcToDate: "",
            qcToDateErr: "",
            dateerr: "",
            shipmentRequestedDate: "",
            remarks: "",
            qcFromFormatted: "",
            formattedDate: "",
            prevId: "",
            shipmentDetails: "",
            emptyShow: true,
            saveShow: true,
            validToDateSelect: "",
            validFromDateSelect: "",
            validationData: "",
            poType: this.props.poType,
            mandateHeaderSet: [],
            maxQCToDate: todayDate,
            isQcOn: this.props.isQcOn,
            selectAddress: false,
            selectLocation: false,
            contactName:"",
            contactNameErr:false,
            contactNumber:"",
            contactNumberErr:false,
            location:"",
            locationErr:false,
            address:"",
            addressErr:false,
            ppsChecked:false,
            testReportChecked:false,
            addressModal: false,
            isModalShow:false,
            locationModal: false,
            AddressModalAnimation:false,
            PPSArray:[],
            ReportArray:[],
            addressSearch: "",
            locationEmail :"",
            reportFilesError:false,
            ppsFilesError:false,

            asnBufferDays: this.props.asnBufferDays,
            ppsUploadModal: false,
            uploadFileType:"",

            packingListChecked: false,
            packingArray:[],
            packingListError: false,
            noOfPercentFulfil: this.props.noOfPercentFulfil,
            createAsnFromToValidity: this.props.createAsnFromToValidity,
            calculatedPercent: 100,
            
            isAsnMask: this.props.isAsnMask,
            cancelLinePendingQty: this.props.cancelLinePendingQty,
            allowPackingList: this.props.allowPackingList,
            allowMultipleAsn: this.props.allowMultipleAsn,
            allowAsnBufferDays: this.props.allowAsnBufferDays,
            allowNoOfPercentFulfil: this.props.allowNoOfPercentFulfil,
        }
    }

    componentDidMount() {
        let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            //displayName: this.props.poType == "poicode" && this.props.inputValues == "true" ? this.props.icode_item : this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            displayName: this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,
            basedOn: this.props.poType == "poicode" ? "ITEM" : "SET"
            //basedOn: "SET"
        }
        // this.props.getSetHeaderConfigRequest(payload)
        if(this.props.poType == "poicode")
          this.props.getItemHeaderConfigRequest(payload)
        else  
          this.props.getSetHeaderConfigRequest(payload)

        this.props.onExpandRef(this);
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }

    componentWillUnmount(){
        this.props.onExpandRef(undefined);
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }

    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent"))){
          this.setState({ addressModal: false, locationModal: false, })
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.orders.VendorCreateShipmentDetails.isSuccess && prevState.actionExpand == false) {
            var poDetails = nextProps.orders.VendorCreateShipmentDetails.data.resource == null ? [] : nextProps.orders.VendorCreateShipmentDetails.data.resource
            poDetails.length == 0 ? [] : poDetails.poDetails.map((data) => {
                data.cancelledQty = 0, data.pendingQty = 0, data.requestedQty = poDetails.status == "PARTIAL" ? Number(data.totalNoOfSet == undefined ? data.orderQty : data.totalNoOfSet) - (Number(data.setRequestedQty == undefined ? data.totalRequestedQty : data.setRequestedQty) + Number(data.setCancelledQty == undefined ? data.totalCancelledQty : data.setCancelledQty)) : data.totalNoOfSet == undefined ? data.orderQty : data.totalNoOfSet, data.error = false
            })
            return {
                setBaseExpandDetails: nextProps.orders.VendorCreateShipmentDetails.data.resource == null ? [] : nextProps.orders.VendorCreateShipmentDetails.data.resource.poDetails,
                status: poDetails.status,
                setBasedArray: poDetails.poDetails,
                shipmentDetails: poDetails,
                prev: nextProps.orders.VendorCreateShipmentDetails.data.prePage,
                current: nextProps.orders.VendorCreateShipmentDetails.data.currPage,
                next: nextProps.orders.VendorCreateShipmentDetails.data.currPage + 1,
                maxPage: nextProps.orders.VendorCreateShipmentDetails.data.maxPage,
                validationData: nextProps.orders.VendorCreateShipmentDetails.data.resource,
                validFromDateSelect: nextProps.orders.VendorCreateShipmentDetails.data.resource.validFromSelectionDate,
                validToDateSelect: nextProps.orders.VendorCreateShipmentDetails.data.resource.validToSelectionDate,
                
            }
        }

        if (prevState.actionExpand && nextProps.orders.VendorCreateShipmentDetails.isSuccess) {
            let itemBaseArray = nextProps.orders.VendorCreateShipmentDetails.data.resource.poDetails
            itemBaseArray.map((data) => (data.totalPendingQty = 0))
            let itemDetails = nextProps.orders.VendorCreateShipmentDetails.data.resource.poDetails
            let id = prevState.expandedId;
            return {
                expandedLineItems: { ...prevState.expandedLineItems, [id]: itemDetails },
                itemBaseArray
            }
        }
        /* if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "SET") {
                let setLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let setLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let setLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                return {
                    setLvlCustomHeaders: prevState.setLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    setLvlGetHeaderConfig,
                    setLvlFixedHeader,
                    setLvlCustomHeadersState,
                    setLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    setLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    setLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    setLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    setLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                }
            }
        } */
        // if (nextProps.replenishment.inspectionOnOff.isSuccess) {
        //     if (nextProps.replenishment.inspectionOnOff.data.qc != undefined && nextProps.replenishment.inspectionOnOff.data.qc != "") {
        //          return {
        //             isQcOn: nextProps.replenishment.inspectionOnOff.data.qc,  
        //          }
        //     }
        // }
        // if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) {
        //     if( nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired != undefined ){
        //         return{
        //             isQcOn: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired,
        //             asnBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.asnBufferDays) : 0, 
        //             noOfPercentFulfil: nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment) : 0,
        //             createAsnFromToValidity: nextProps.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablility == "TRUE" ? true : false ,
        //             isAsnMask: nextProps.logistic.getButtonActiveConfig.data.resource.asnMask == "TRUE" ? true : false ,      
        //         }
        //     }
        // }
        
        return null;
    }
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "SET") {
            let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" && this.props.inputValues == "true" ? this.props.icode_item : this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            basedOn: "SET"
        }
        this.props.getSetHeaderConfigRequest(payload)
        }

        // if (this.props.replenishment.inspectionOnOff.isSuccess && this.props.replenishment.inspectionOnOff.data.resource != null) {
        //     if (this.props.replenishment.inspectionOnOff.data.qc != undefined ) {
        //          this.setState({
        //             isQcOn: this.props.replenishment.inspectionOnOff.data.qc,  
        //          })
        //     }
        //     this.props.getInspOnOffConfigClear()
        // }

        // if (this.props.logistic.getButtonActiveConfig.isSuccess && this.props.logistic.getButtonActiveConfig.data.resource != null) {
        //     if( this.props.logistic.getButtonActiveConfig.data.resource.inspectionRequired != undefined ){ 
        //         this.setState({ 
        //             isQcOn: this.props.logistic.getButtonActiveConfig.data.resource.inspectionRequired,
        //             asnBufferDays: this.props.logistic.getButtonActiveConfig.data.resource.asnBufferDays != undefined && this.props.logistic.getButtonActiveConfig.data.resource.asnBufferDays != null ? Number(this.props.logistic.getButtonActiveConfig.data.resource.asnBufferDays) : 0,
        //             noOfPercentFulfil: this.props.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != undefined && this.props.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment != null ? Number(this.props.logistic.getButtonActiveConfig.data.resource.asnCreationFulfilment) : 0,  
        //             createAsnFromToValidity: this.props.logistic.getButtonActiveConfig.data.resource.asnFromToApplicablility == "TRUE" ? true : false , 
        //             isAsnMask: this.props.logistic.getButtonActiveConfig.data.resource.asnMask == "TRUE" ? true : false ,       
        //         },()=>{
        //             if( this.state.isQcOn == "FALSE")
        //             this.props.getInspOnOffConfigRequest('asd');
        //         })    
        //     }
        //     this.props.getStatusButtonActiveClear()
        // }
            
    }
    


    expandColumn(id, e, data) {
        var img = e.target
        if (this.state.expandedLineItems.hasOwnProperty(id)) {
            let arr = [];
            arr = this.state.expandedLineItems[id]
            this.setState({
                itemBaseArray: arr,
                expandedId: id,
                actionExpand: false,
                dropOpen: !this.state.dropOpen
            })
        }
        else {
        if (!this.state.actionExpand || this.state.prevId !== id) {
            let payload = {
                id: this.props.expandedId,
                userType: "entpo",
                detailType: "item",
                setHeaderId: data.setHeaderId,
                poType: this.props.poType,
            }
            this.props.VendorCreateShipmentDetailsRequest(payload)
            //this.props.getItemHeaderConfigRequest(this.props.itemHeaderPayload)
            this.setState({ actionExpand: true, prevId: id, expandedId: id, dropOpen: true, order: { orderCode: data.orderCode, orderNumber: data.orderNumber } },
            )
        }
        else {
            this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
        }
        }
    }
    handleQuantityChange(e, data) {
        var setBasedArray = [...this.state.setBasedArray]
        var expandedLineItems = { ...this.state.expandedLineItems }
        var id = this.props.poType == "poicode" ? data.orderDetailId : data.setBarCode
        var indexId = this.props.poType == "poicode" ? "orderDetailId" : "setBarCode"
        let index = setBasedArray.findIndex((obj => obj[indexId] == data[indexId]));
        let numberPattern = /^[0-9]+$/;
        if(this.state.itemBaseArray.length == 0){
            let payload = {
                orderId: this.props.expandedId,
                userType: "entpo",
                detailType: "item",
                setHeaderId: data.setHeaderId !== null && data.setHeaderId !== undefined ? data.setHeaderId : "",
                poType: this.props.poType,
            }
            let headers = {
                'X-Auth-Token': sessionStorage.getItem('token'),
                'Content-Type': 'application/json'
            }
            axios.post(`${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorpo/get/poDetail`, payload, { headers: headers })
            .then(res => {
                this.setState({
                    itemBaseArray: res.data.data.resource.poDetails,
                    expandedLineItems: { ...this.state.expandedLineItems, [id]: [...res.data.data.resource.poDetails] },
                })
                
            }).catch((error) => {
                this.setState({
                    toastError: true
                })
            }) 
        }
        if (e.target.id == "requestedQty") {

            //For Checking Pending Qty is less than zero or not::
            let pendingQtyValue = (parseInt(setBasedArray[index].totalNoOfSet == undefined ? setBasedArray[index].orderQty : setBasedArray[index].totalNoOfSet)
                                   - (parseInt(numberPattern.test(e.target.value) ? e.target.value : 0)
                                   + parseInt(setBasedArray[index].cancelledQty == "" ? 0 : setBasedArray[index].cancelledQty))
                                   - (Number(setBasedArray[index].setRequestedQty == undefined ? setBasedArray[index].totalRequestedQty : setBasedArray[index].setRequestedQty)
                                   + Number(setBasedArray[index].setCancelledQty == undefined ? setBasedArray[index].totalCancelledQty : setBasedArray[index].setCancelledQty)));

            let totalQty = setBasedArray[index].totalNoOfSet == undefined ? setBasedArray[index].orderQty : setBasedArray[index].totalNoOfSet;
            let previouslyRequestedQty = setBasedArray[index].setRequestedQty == undefined ? setBasedArray[index].totalRequestedQty : setBasedArray[index].setRequestedQty;                       

            if( pendingQtyValue >= 0 ){

                //For set's item qty calculation::
                var arr = [...this.state.itemBaseArray]
                arr.map((item) => (
                    item.newRequestedQty = Number(item.nbset) * Number(numberPattern.test(e.target.value) ? e.target.value : 0),
                    item.setPendingQty = Number(item.orderQty) - (Number(item.newCancelledQty) + 
                            Number(item.newRequestedQty) + Number(item.totalRequestedQty) + Number(item.totalCancelledQty)),

                    item.requestQty = item.newRequestedQty,
                    item.totalPendingQty = item.setPendingQty
                ))
                this.setState({
                    expandedLineItems: { ...this.state.expandedLineItems, [id]: arr },
                    itemBaseArray: arr,
                })
                  
                //For set qty calculation::
                setBasedArray[index].requestedQty = numberPattern.test(e.target.value) ? e.target.value : "";
                setBasedArray[index].pendingQty = (parseInt(setBasedArray[index].totalNoOfSet == undefined ? setBasedArray[index].orderQty : setBasedArray[index].totalNoOfSet) - (parseInt(setBasedArray[index].requestedQty == "" ? 0 : setBasedArray[index].requestedQty) + parseInt(setBasedArray[index].cancelledQty == "" ? 0 : setBasedArray[index].cancelledQty)) - (Number(setBasedArray[index].setRequestedQty == undefined ? setBasedArray[index].totalRequestedQty : setBasedArray[index].setRequestedQty) + Number(setBasedArray[index].setCancelledQty == undefined ? setBasedArray[index].totalCancelledQty : setBasedArray[index].setCancelledQty)))
                // - (parseInt(event.target.value == "" ? 0 : event.target.value) + parseInt(setBasedArray[index].setCancelledQty == "" || undefined || NaN ? 0 : setBasedArray[index].setCancelledQty))
                if (Number(setBasedArray[index].requestedQty) > (Number(setBasedArray[index].totalNoOfSet == undefined ? setBasedArray[index].orderQty : setBasedArray[index].totalNoOfSet) - (Number(setBasedArray[index].setRequestedQty == undefined ? setBasedArray[index].totalRequestedQty : setBasedArray[index].setRequestedQty) + Number(setBasedArray[index].setCancelledQty == undefined ? setBasedArray[index].totalCancelledQty : setBasedArray[index].setCancelledQty)))) {
                    setBasedArray[index].error = true
                    // this.setState({saveShow : false})
                }

                //Percent Fulfilment logic::
                let finalReqestedQty = setBasedArray[index].requestedQty == "" ? 0 : Number(setBasedArray[index].requestedQty);
                let finalTotalQty = (Number(finalReqestedQty) + Number(setBasedArray[index].pendingQty == "" ? 0 : setBasedArray[index].pendingQty));
                if(finalTotalQty > 0 && numberPattern.test(setBasedArray[index].pendingQty)){
                    let calculatedPercent = parseInt(finalReqestedQty * 100 / finalTotalQty);
                    if(this.state.allowNoOfPercentFulfil && calculatedPercent < this.state.noOfPercentFulfil){
                        // If one PO has multiple set, then if requested qty is 0::
                        if( this.state.cancelLinePendingQty && setBasedArray.length > 1 && finalReqestedQty == 0){
                            setBasedArray[index].error = false;
                            setBasedArray[index].percentFulfilFlag = false;
                            this.props.toastMsgForPercentageFulfil(0)
                        }else{
                            this.props.toastMsgForPercentageFulfil(10000)
                            setBasedArray[index].error = true;
                            setBasedArray[index].percentFulfilFlag = true;
                        }
                    }else{
                        setBasedArray[index].percentFulfilFlag = false;
                        this.props.toastMsgForPercentageFulfil(0)
                    }
                    this.setState({ calculatedPercent: parseInt(finalReqestedQty * 100 / finalTotalQty)})
                }
                // else{
                //     if( this.state.noOfPercentFulfil !== 0){
                //         this.props.toastMsgForPercentageFulfil()
                //         setBasedArray[index].error = true;
                //         setBasedArray[index].percentFulfilFlag = true;
                //     }else{
                //         setBasedArray[index].percentFulfilFlag = false;
                //     }
                // }
            }
            if (Number(totalQty) === Number(previouslyRequestedQty)){
                this.props.toastMsgForShowingPendingQty(totalQty, previouslyRequestedQty); 
            }

            
        } else if (e.target.id == "cancelQty") {

            //For Checking Pending Qty is less than zero or not::
            let pendingQtyValue = (parseInt(setBasedArray[index].totalNoOfSet == undefined ? setBasedArray[index].orderQty : setBasedArray[index].totalNoOfSet) 
                                   - ((parseInt(numberPattern.test(e.target.value) ? e.target.value : 0) 
                                   + parseInt(setBasedArray[index].requestedQty == "" ? 0 : setBasedArray[index].requestedQty))) 
                                   - (Number(setBasedArray[index].setRequestedQty == undefined ? setBasedArray[index].totalRequestedQty : setBasedArray[index].setRequestedQty) 
                                   + Number(setBasedArray[index].setCancelledQty == undefined ? setBasedArray[index].totalCancelledQty : setBasedArray[index].setCancelledQty)))

            if( pendingQtyValue >= 0 ){ 

                //For set's item qty calculation::
                if (expandedLineItems[id] != undefined) {
                    var arr = [...expandedLineItems[id]]
                    arr.map((item) => (
                        item.newCancelledQty = Number(item.nbset) * Number(numberPattern.test(e.target.value) ? e.target.value : 0),
                        item.setPendingQty = Number(item.orderQty) - (Number(item.newCancelledQty) + Number(item.newRequestedQty) + Number(item.totalRequestedQty) + Number(item.totalCancelledQty)),

                        item.cancelQty = item.newCancelledQty,
                        item.totalPendingQty = item.setPendingQty
                    ))
                    this.setState({
                        expandedLineItems: { ...this.state.expandedLineItems, [id]: arr },
                        itemBaseArray: arr,
                    })
                }
                
                //For set qty calculation::
                setBasedArray[index].pendingQty = (parseInt(setBasedArray[index].totalNoOfSet == undefined ? setBasedArray[index].orderQty : setBasedArray[index].totalNoOfSet) - ((parseInt(e.target.value == "" ? 0 : e.target.value) + parseInt(setBasedArray[index].requestedQty == "" ? 0 : setBasedArray[index].requestedQty))) - (Number(setBasedArray[index].setRequestedQty == undefined ? setBasedArray[index].totalRequestedQty : setBasedArray[index].setRequestedQty) + Number(setBasedArray[index].setCancelledQty == undefined ? setBasedArray[index].totalCancelledQty : setBasedArray[index].setCancelledQty)))
                setBasedArray[index].cancelledQty = numberPattern.test(e.target.value) ? e.target.value : ""
                if (Number(setBasedArray[index].pendingQty < 0)) {
                    setBasedArray[index].error = true
                }
                if (Number(setBasedArray[index].cancelledQty) && Number(setBasedArray[index].cancelledQty) > (Number(setBasedArray[index].totalNoOfSet == undefined ? setBasedArray[index].orderQty : setBasedArray[index].totalNoOfSet) - Number(setBasedArray[index].requestedQty) - Number(setBasedArray[index].cancelledQty))) {
                    setBasedArray[index].error = true
                }

                //Percent Fulfilment logic::
                let finalReqestedQty = setBasedArray[index].requestedQty == "" ? 0 : Number(setBasedArray[index].requestedQty);
                let finalTotalQty = (Number(finalReqestedQty) + Number(setBasedArray[index].pendingQty == "" ? 0 : setBasedArray[index].pendingQty));
                if(finalTotalQty > 0 && numberPattern.test(setBasedArray[index].pendingQty)){
                    let calculatedPercent = parseInt(finalReqestedQty * 100 / finalTotalQty);
                    if(this.state.allowNoOfPercentFulfil && calculatedPercent < this.state.noOfPercentFulfil){
                        this.props.toastMsgForPercentageFulfil(10000)
                        setBasedArray[index].error = true;
                        setBasedArray[index].percentFulfilFlag = true;
                    }else{
                        setBasedArray[index].percentFulfilFlag = false;
                        this.props.toastMsgForPercentageFulfil(0)
                    }
                    this.setState({ calculatedPercent: parseInt(finalReqestedQty * 100 / finalTotalQty)})
                }
                // else{
                //     if( this.state.noOfPercentFulfil !== 0){
                //         this.props.toastMsgForPercentageFulfil()
                //         setBasedArray[index].error = true;
                //         setBasedArray[index].percentFulfilFlag = true;
                //     }else{
                //         setBasedArray[index].percentFulfilFlag = false;
                //     }
                // }
            }
        }
        else if (e.target.id == "newRequestedQty") {
            setBasedArray[index].totalPendingQty = e.target.value
        }

        if (Number(setBasedArray[index].pendingQty >= 0) && !setBasedArray[index].percentFulfilFlag) {
            setBasedArray[index].error = false
        }

        this.setState({
            setBasedArray,
            saveShow: (!setBasedArray.some((data) => data.error == true)) && (!setBasedArray.every((_) => _.requestedQty == 0 || _.requestedQty == "")),
            emptyShow: !setBasedArray.every((item) => ((Number(item.requestedQty) == 0 || "") && (Number(item.cancelledQty) == 0 || ""))),
            nullShow: setBasedArray.every((item) => ((Number(item.requestedQty) == "") || (Number(item.cancelledQty) == "")))
        })
    }

    date() {
        if (this.state.shipmentRequestedDate != "") {
            this.setState({
                dateerr: false
            })
        } else {
            this.setState({
                dateerr: true
            })
        }
    }
    fromDate() {
        if (this.state.qcFromDate != "") {
            this.setState({
                qcFromDateErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE" )
               this.setState({ qcFromDateErr: true })
            else
               this.setState({ qcFromDateErr: false })   
        }
    }
    toDate() {
        if (this.state.qcToDate != "") {
            this.setState({
                qcToDateErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE")
               this.setState({ qcToDateErr: true })
            else
               this.setState({ qcToDateErr: false }) 
        }
    }
    contactNameFun() {
        let contactNamePattern =  /^[A-Za-z0-9\.\-\_\s]+$/
        if (this.state.contactName != "" && contactNamePattern.test(this.state.contactName)) {
            this.setState({
                contactNameErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE" )
              this.setState({ contactNameErr: true })
            else
              this.setState({ contactNameErr: false })    
        }
    }
    contactNumberFun () {
        let numberPattern = /^[0-9]+$/;
        if (this.state.contactNumber != "" && Number(this.state.contactNumber.length) == 10 && numberPattern.test(this.state.contactNumber)) {
            this.setState({
                contactNumberErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE" )
               this.setState({ contactNumberErr: true })
            else
               this.setState({ contactNumberErr: false }) 
        }
    }
    locationFun =()=> {
        if (this.state.location != "" && this.state.baseLocation !== "") {
            this.setState({
                locationErr: false
            }) 
        } else {
            if( this.state.isQcOn === "TRUE" )
              this.setState({ locationErr: true })
            else
              this.setState({ locationErr: false })   
        }
    }
    addressFun () {
        if (this.state.address != "" && this.state.addressCity !== "") {
            this.setState({
                addressErr: false
            })
        } else {
            if( this.state.isQcOn === "TRUE" )
               this.setState({ addressErr: true })
            else
               this.setState({ addressErr: false }) 
        }
    }
    ppsFileErrorFun () {
        if (this.state.PPSArray.length != 0) {
            this.setState({
                ppsFilesError: false
            })
        } else {
            if(this.state.ppsChecked){
                this.setState({ ppsFilesError: true })
            }else{
                this.setState({ ppsFilesError: false })
            }
            
        }
    }
    reportFileErrorFun () {
        if (this.state.ReportArray.length != 0) {
            this.setState({
                reportFilesError: false
            })
        } else {
            if(this.state.testReportChecked){
                this.setState({ reportFilesError: true })
            }else{
                this.setState({ reportFilesError: false })
            }
        }
    }
    packingFileErrorFun =()=> {
        if (this.state.packingArray.length != 0) {
            this.setState({
                packingListError: false
            })
        } else {
            if(this.state.packingListChecked){
                this.setState({ packingListError: true })
            }else{
                this.setState({ packingListError: false })
            }
        }
    }

    createFinalShipment =()=> {
        if( this.state.shipmentDetails.isCreateASNAllowed !== undefined && this.state.shipmentDetails.isCreateASNAllowed == 0){
            this.props.toastMsgForFromToValidity();
        }else{
            this.date()
            this.fromDate()
            this.toDate()
            this.contactNameFun()
            this.contactNumberFun()
            this.addressFun()
            this.locationFun()
            this.ppsFileErrorFun()
            this.reportFileErrorFun()
            this.packingFileErrorFun()
            setTimeout(() => {
                if (!this.state.dateerr && !this.state.qcFromDateErr && !this.state.qcToDateErr
                    && !this.state.contactNameErr
                    && !this.state.contactNumberErr
                    && !this.state.addressErr
                    && !this.state.locationErr
                    && !this.state.ppsFilesError
                    && !this.state.reportFilesError
                    && !this.state.packingListError
                    ) {
                    let shipmentDetails = this.state.shipmentDetails
                    var changelineitems = [], totalRequestQty = 0, basicLineAmount = 0, basicAmount = 0, taxAmount = 0, setTotalOrderQty = 0, totalOrderQty = 0, totalCancelledQty = 0, totalPendingQty = 0;
                    changelineitems = this.state.setBasedArray.map((item) => {
                        totalRequestQty = totalRequestQty + parseInt(item.requestedQty == "" ? 0 : item.requestedQty)
                        totalCancelledQty = totalCancelledQty + parseInt(item.cancelledQty == "" ? 0 : item.cancelledQty)
                        totalOrderQty = totalOrderQty + parseInt(item.orderQty)
                        basicLineAmount = parseInt(item.rate) * parseInt(item.requestedQty == "" ? 0 : item.requestedQty)
                        totalPendingQty = totalPendingQty + parseInt(item.pendingQty),
                            basicAmount = basicAmount + basicLineAmount
                        taxAmount = taxAmount + basicLineAmount * (parseInt(item.gst) / 100)
                        return {
                            orderDetailId: item.orderDetailId == undefined ? 0 : item.orderDetailId,
                            setHeaderId: item.setHeaderId,
                            requestedQty: parseInt(item.requestedQty == "" ? 0 : item.requestedQty),
                            orderQty: item.totalNoOfSet == undefined ? item.orderQty : item.totalNoOfSet,
                            pendingQty: item.pendingQty,
                            cancelQty: parseInt(item.cancelledQty == "" ? 0 : item.cancelledQty)
                        }
                    })
                    let payload = {
                        basicAmount,
                        taxAmount,
                        // poId: shipmentDetails.id,
                        shipmentAdviceCode: shipmentDetails.shipmentAdviceCode,
                        // shipmentRequestOnDate: todayDate + "T00:00+05:30",
                        shipmentRequestDate: this.state.shipmentRequestedDate + "T00:00+05:30",
                        shipmentRequestOnDate: todayDate + "T00:00+05:30",
                        shipmentTotalRequestedQty: totalRequestQty,
                        totalPendingQty: totalPendingQty,
                        shipmentTotalCancelledQty: totalCancelledQty,
                        dueInDays: shipmentDetails.dueDays,
                        transporter: shipmentDetails.transporterName,
                        isShipConfirmed: "FALSE",
                        poDetails: changelineitems,
                        orderCode: this.props.order.orderCode,
                        orderNumber: this.props.expandedPo.orderNumber,
                        qcFromDate: this.state.qcFromDate == "" ? "" : this.state.qcFromDate + "T00:00+05:30",
                        qcToDate: this.state.qcToDate == "" ? "" : this.state.qcToDate + "T00:00+05:30",
                        remarks: this.state.remarks,
                        orderId: this.props.expandedPo.orderId,
                        documentNumber: this.props.expandedPo.documentNumber,
                        poType: this.state.poType,
                        enterpriseSite: this.props.siteDetails,
                        validFromDate: this.state.createAsnFromToValidity ? this.state.shipmentDetails.asnFromSelectionDate + "T00:00+05:30" : this.props.validFromDate + "T00:00+05:30",
                        validToDate: this.state.createAsnFromToValidity ? this.state.shipmentDetails.asnToSelectionDate + "T00:00+05:30" : this.props.validToDate + "T00:00+05:30",
                        contactPerson:this.state.contactName,
                        contactNo:this.state.contactNumber,
                        address:this.state.address,
                        location:this.state.location,
                        isPPSAvailable:(this.state.ppsChecked.toString()).toUpperCase(),
                        isTestReportAvailable:(this.state.testReportChecked.toString()).toUpperCase(),
                        locationEmail :this.state.locationEmail,
                        isPackagingUploaded: (this.state.packingListChecked.toString()).toUpperCase(),
                        vendorName: this.props.expandedPo.vendorName 
                    }
                    this.props.vendorCreateSrRequest(payload)
                    this.props.onFileUploadProps(
                        this.state.ppsChecked,
                        this.state.testReportChecked,
                        this.state.PPSArray,
                        this.state.ReportArray,
                        this.props.expandedPo.orderNumber,
                        this.props.expandedPo.orderId,
                        this.props.expandedPo.documentNumber,
                        this.state.packingListChecked,
                        this.state.packingArray,
                        shipmentDetails.shipmentAdviceCode,
                        shipmentDetails.vendorCode)
                    //this.onFileUpload()
                }
            }, 10);
        }
    }
    setLvlOpenColoumSetting(data) {
        if (this.state.setLvlCustomHeadersState.length == 0) {
            this.setState({
                setLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                setLvlColoumSetting: true
            })
        } else {
            this.setState({
                setLvlColoumSetting: false
            })
        }
    }
    setLvlpushColumnData(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlHeaderConfigDataState = this.state.setLvlHeaderConfigDataState
        let setLvlCustomHeaders = this.state.setLvlCustomHeaders
        let setLvlSaveState = this.state.setLvlSaveState
        let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
        let setLvlHeaderSummary = this.state.setLvlHeaderSummary
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (this.state.setLvlHeaderCondition) {

            if (!data.includes(setLvlGetHeaderConfig) || setLvlGetHeaderConfig.length == 0) {
                setLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(setLvlHeaderConfigDataState))) {
                    let invert = _.invert(setLvlFixedHeaderData)
                    let keyget = invert[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }
                if (!Object.keys(setLvlCustomHeaders).includes(setLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];
                    setLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(setLvlCustomHeadersState) || setLvlCustomHeadersState.length == 0) {
                setLvlCustomHeadersState.push(data)
                if (!setLvlCustomHeadersState.includes(setLvlHeaderConfigDataState)) {
                    let keyget = (_.invert(setLvlFixedHeaderData))[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }
                if (!Object.keys(setLvlCustomHeaders).includes(setLvlHeaderSummary)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];
                    setLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeadersState,
            setLvlCustomHeaders,
            setLvlSaveState,
            setLvlDefaultHeaderMap,
            setLvlHeaderSummary
        })
    }
    setLvlCloseColumn(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlHeaderConfigState = this.state.setLvlHeaderConfigState
        let setLvlCustomHeaders = []
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (!this.state.setLvlHeaderCondition) {
            for (let j = 0; j < setLvlCustomHeadersState.length; j++) {
                if (data == setLvlCustomHeadersState[j]) {
                    setLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlCustomHeadersState.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
            if (this.state.setLvlCustomHeadersState.length == 0) {
                this.setState({
                    setLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < setLvlGetHeaderConfig.length; i++) {
                if (data == setLvlGetHeaderConfig[i]) {
                    setLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlGetHeaderConfig.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
        }
        setLvlCustomHeaders.forEach(e => delete setLvlHeaderConfigState[e]);
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeaders: setLvlHeaderConfigState,
            setLvlCustomHeadersState,
        })
        setTimeout(() => {     // minValue,
            // maxValue
            let keygetvalue = (_.invert(this.state.setLvlFixedHeaderData))[data];     // minValue,
            // maxValue
            let setLvlSaveState = this.state.setLvlSaveState
            setLvlSaveState.push(keygetvalue)
            let setLvlHeaderSummary = this.state.setLvlHeaderSummary
            let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
            if (!this.state.setLvlHeaderCondition) {
                for (let j = 0; j < setLvlHeaderSummary.length; j++) {
                    if (keygetvalue == setLvlHeaderSummary[j]) {
                        setLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < setLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == setLvlDefaultHeaderMap[i]) {
                        setLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                setLvlHeaderSummary,
                setLvlDefaultHeaderMap,
                setLvlSaveState
            })
        }, 100);
    }
    setLvlsaveColumnSetting(e) {
        this.setState({
            setLvlColoumSetting: false,
            setLvlHeaderCondition: false,
            setLvlSaveState: []
        })
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    setLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // setLvlParaMsg: "Click confirm to continue.",
            setLvlConfirmModal: true,
        })
    }
    setLvlResetColumn() {
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            setLvlHeaderCondition: true,
            setLvlColoumSetting: false,
            setLvlSaveState: []
        })
    }
    handleRequestedDate = (e) => {
        var formattedDate = moment(e.target.value, "YYYY-MM-DD").format("DD-MM-YYYY")
        this.setState({ shipmentRequestedDate: e.target.value, qcFromFormatted: "", qcToFormatted: "",  formattedDate: formattedDate == "Invalid date" ? "" : formattedDate }, () => {
            this.date()
        }
        )
    }
    handleChange = (e) => {
        var formattedDate = moment(e.target.value, "YYYY-MM-DD").format("DD-MM-YYYY")
        var maxQCToDate = moment(e.target.value).add(2,'days').format("YYYY-MM-DD")
        if (e.target.id == "qcFromDate") {
            this.setState({ qcFromDate: e.target.value, qcToDate: "", qcToFormatted: "", qcFromFormatted: formattedDate == "Invalid date" ? "" : formattedDate,
                            maxQCToDate : maxQCToDate,
            }, () => { this.fromDate() })
        } else if (e.target.id == "qcToDate") {
            this.setState({ qcToDate: e.target.value, qcToFormatted: formattedDate == "Invalid date" ? "" : formattedDate }, () => { this.toDate() })
        } else if (e.target.id == "remarks") {
            this.setState({ remarks: e.target.value })
        } else if (e.target.id == "contactName") {
            this.setState({ contactName: e.target.value },() => { this.contactNameFun() })
        } else if (e.target.id == "contactNumber") {
            if( e.target.value.length <= 10 )
               this.setState({ contactNumber: e.target.value },() => { this.contactNumberFun() })
        } else if (e.target.id == "address") {
            this.setState({ address: e.target.value,addressSearch: this.state.isModalShow ? "" : e.target.value,
                            addressCity: ""})
        } else if (e.target.id == "location") {
            this.setState({ location: e.target.value, baseLocation: ""})
        } else if (e.target.id == "ppsCheckBox") {
            if(this.state.ppsChecked){
                this.setState({ ppsChecked: false,PPSArray:[],ppsFilesError:false })
            }else{
                this.setState({ ppsChecked: true,ppsFilesError:true })
            }
        } else if (e.target.id == "testReportCheckBox") {
            if(this.state.testReportChecked){
                this.setState({ testReportChecked: false,ReportArray:[],reportFilesError:false })
            }else{
                this.setState({ testReportChecked: true,reportFilesError:true })
            }
        } else if (e.target.id == "packingListChecked") {
            if(this.state.packingListChecked){
                this.setState({ packingListChecked: false, packingArray: [], packingListError: false })
            }else{
                this.setState({ packingListChecked: true, packingListError:true })
            }
        }
    }

    openAddressModal(e, id) {
        this.onEsc()
        let data = {
            type: this.state.address == "" || this.state.isModalShow ? 1 : 3,
            pageNo: 1,
            search: this.state.isModalShow ? "" : this.state.address,
            sortedBy: "",
            sortedIn: ""
        }
        this.props.getAddressRequest(data)
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
        this.setState({
            addressModal: true,
            AddressModalAnimation: !this.state.AddressModalAnimation
        }, ()=> document.addEventListener("click", this.onOutsideClickClose))
       
    }

    onOutsideClickClose =(e)=>{
       if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")){
           this.setState({ addressModal: false, locationModal: false})
       }
    }

    openLocationModal(e, id) {
        this.onEsc()
        let data = {
            type: this.state.location == "" || this.state.isModalShow ? 1 : 3,
            pageNo: 1,
            search: this.state.isModalShow ? "" : this.state.location,
            sortedBy: "",
            sortedIn: ""
        }
        this.props.getLocationRequest(data)

        this.setState({
            locationSearch: this.state.isModalShow ? "" : this.state.location,
            locationModal: true,
            LocationModalAnimation: !this.state.LocationModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    onEsc = () => {
        this.setState({
            addressModal: false,
            locationModal: false,
            ppsUploadModal: false,
        })//  this.escChild.current.childEsc();
    }
    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(id).click();
        } else if (e.key === "Enter") {
            if (id == "address") {
              this.openAddressModal() 
            }
            else if (id == "location") {
                this.openLocationModal()
            }
        } else {
            document.getElementById(id).focus()
        }
    }

    setLvlCloseConfirmModal(e) {
        this.setState({
            setLvlConfirmModal: !this.state.setLvlConfirmModal,
        })
    }
    onCloseAddressModal(e) {
        this.setState({
            addressModal: false,
            AddressModalAnimation: !this.state.AddressModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("address").focus()
    }
    addressVal = (city, data) => {
        this.setState({
            address:data,
            addressModal:false,
            addressCity: city,
        },
        ()=>{
            this.addressFun()
        })
    }
    onCloseLocationModal(e) {
        this.setState({
            locationModal: false,
            LocationModalAnimation: !this.state.LocationModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("address").focus()
    }
    locationVal = (baseLocation,data,email) => {
        this.setState({
            locationEmail :email,
            location:data,
            locationModal:false,
            baseLocation: baseLocation,
        },
        ()=>{
            this.locationFun()
        })
    }   
    
    // onFileUpload(subModule) {
    //     let files = this.state.PPSArray
    //     let date = new Date()
    //     const fd = new FormData();
    //     let uploadNode = {
    //         shipmentId: this.props.expandedPo.shipmentId,
    //         orderNumber: this.props.expandedPo.orderNumber,
    //         module: "SHIPMENT",
    //         subModule: subModule,
    //         isQCAvailable: "FALSE",
    //         isInvoiceAvailable: "FALSE",
    //         isBarcodeAvailable: "TRUE",
    //         orderId: this.props.expandedPo.orderId,
    //         documentNumber: this.props.expandedPo.documentNumber,
    //     }
    //     let headers = {
    //         'X-Auth-Token': sessionStorage.getItem('token'),
    //         'Content-Type': 'multipart/form-data'
    //     }
    //     for (var i = 0; i < files.length; i++) {
    //         fd.append("files", files[i]);
    //     }
    //     fd.append("uploadNode", JSON.stringify(uploadNode))
    //     // this.loaderUpload("true")
    //     axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
    //         .then(res => {
    //             this.setState({
    //                 loader: false
    //             })
    //             let result = res.data.data.resource
    //             }).catch((error) => {
    //             this.setState({
    //                 loader: false
    //             })
    //             this.props.openToastError()
    //         })
    // } 
    // onMmltipleFileUpload = (evt,subModule) =>{
    //     if(subModule=="PPS"){
    //         var files = evt.target.files; // FileList object
    //         var totalFiles = [];
    //         for (var i = 0, f; f = files[i]; i++) {
    //             const scope = this
    //             totalFiles.push(f)
    //             this.setState({
    //                 ppsFilesError:false,
    //                 PPSArray:this.state.PPSArray.concat(totalFiles).reverse(),
    //             },(e)=>{
    //             });
            
    //         var reader = new FileReader();
    //         reader.onload = (f,function(theFile) {
    //             return (e) => {
    //             var span = document.createElement('span');
    //             var img = document.createElement('img');
    //             img.className = "remove";
    //             img.src=clearSearch
    //             img.style.display="inherit"
    //             span.className = "caffbuf-file-name";
    //             span.innerHTML = 
    //             ['<span>'+theFile.name+'</span>',img.outerHTML]
    //             .join('')
    //             document.getElementById('ppsfileLabel').insertBefore(span, null);
    //             $(".remove").click((clickEvent)=>{
    //                 var index = Array.from(document.getElementById('ppsfileLabel').children).indexOf(clickEvent.target.parentNode)
    //                 var remove = document.querySelectorAll('#ppsfileLabel span')[index]
    //                 document.querySelector("#ppsfileLabel").removeChild(remove);
    //                 scope.state.PPSArray.splice(index, 1)
    //             })
    //             };
    //         })(f);                                                                                                                                                                                                                                                                
            
    //         reader.readAsDataURL(f);
    //         }
    //     }else if(subModule=="REPORT"){
    //         var files = evt.target.files; // FileList object
    //         var totalFiles = [];
    //         for (var i = 0, f; f = files[i]; i++) {
    //             const scope = this
    //             totalFiles.push(f)
    //             this.setState({
    //             reportFilesError:false,
    //             ReportArray:this.state.ReportArray.concat(totalFiles).reverse(),
    //             },(e)=>{
    //             });
            
    //         var reader = new FileReader();
    //         reader.onload = (f,function(theFile) {
    //             return (e) => {
    //             var span = document.createElement('span');
    //             var img = document.createElement('img');
    //             img.className = "remove";
    //             img.src=clearSearch
    //             img.style.display="inherit"
    //             span.className = "caffbuf-file-name";
    //             span.innerHTML = 
    //             ['<span>'+theFile.name+'</span>',img.outerHTML]
    //             .join('')
    //             document.getElementById('reportFileLabel').insertBefore(span, null);
    //             $(".remove").click((clickEvent)=>{
    //                 var index = Array.from(document.getElementById('reportFileLabel').children).indexOf(clickEvent.target.parentNode)
    //                 var remove = document.querySelectorAll('#reportFileLabel span')[index]
    //                 document.querySelector("#reportFileLabel").removeChild(remove);
    //                 scope.state.ReportArray.splice(index, 1)
    //             })
    //             };
    //         })(f);                                                                                                                                                                                                                                                                
            
    //         reader.readAsDataURL(f);
    //         }
    //     }
        
       
    // }
    onFileUploadSubmit = (fileList,moduleName) =>{
        if(moduleName == "PPS"){
            this.setState({
                PPSArray:fileList,
                ppsUploadModal: false,
                ppsFilesError:fileList.length != 0 ? false: true
            })
        }else if(moduleName == "REPORT"){
            this.setState({
                ReportArray:fileList,
                ppsUploadModal: false,
                reportFilesError:fileList.length != 0 ? false: true
            })
        }
        else if(moduleName == "PACKING"){
            this.setState({
                packingArray:fileList,
                ppsUploadModal: false,
                packingListError:fileList.length != 0 ? false: true
            })
        }
    }

    openPpsUploadModal(e,fileType) {
        e.preventDefault();
        this.setState({
            uploadFileType:fileType,
            ppsUploadModal: !this.state.ppsUploadModal
        });
    }
    CancelPpsUploadModal = (e) => {
        this.setState({
            ppsUploadModal: false,
        })
    }

    calculatedPercent =(e, data)=>{
        var setBasedArray = [...this.state.setBasedArray]
        var indexId = this.props.poType == "poicode" ? "orderDetailId" : "setBarCode"
        let index = setBasedArray.findIndex((obj => obj[indexId] == data[indexId]));
        let numberPattern = /^[0-9]+$/;

        //Percent Fulfilment logic::
        let finalReqestedQty = setBasedArray[index].requestedQty == "" ? 0 : Number(setBasedArray[index].requestedQty);
        let finalTotalQty = (Number(finalReqestedQty) + Number(setBasedArray[index].pendingQty == "" ? 0 : setBasedArray[index].pendingQty));
        if(finalTotalQty > 0 && numberPattern.test(setBasedArray[index].pendingQty)){
            this.setState({ calculatedPercent: parseInt(finalReqestedQty * 100 / finalTotalQty)})
        }
    }

    render() {
        const { qcFromDateErr, qcToDateErr, dateerr, emptyShow,
            contactName,
            contactNumber,
            contactNumberErr,contactNameErr,addressErr,locationErr,address,location,reportFilesError,ppsFilesError } = this.state  
        return (
            <div>
                <div className="gen-vend-sticky-table" id="expandTableMain">
                    <div className="gvst-expend">
                        <div className="col-md-12 pad-0" id="expandedTable" ref={node => this.expandTableref = node}>
                            {this.props.status == "APPROVED" && this.props.inputValues == "true" ? <div className="create-asn-form">
                                <form>
                                    <div className="col-md-3 pad-0 pr-15">
                                        <div className="caf-details">
                                            <div className="cafd-row">
                                                <label className="head-label">PO Number</label>
                                                <span className="cafd-number">{this.props.expandedPo.orderNumber}</span>
                                            </div>
                                            <div className="cafd-row">
                                                <label className="head-label">ASN/Inspection Number</label>
                                                <span className="cafd-number"> {this.state.isAsnMask ? this.state.shipmentDetails.asnMask : this.state.shipmentDetails.shipmentAdviceCode}</span>
                                            </div>
                                            <div className="cafd-row">
                                                <div className="cafdr-inner">
                                                    <label className="head-label">PO Valid From</label>
                                                    <span className="cafd-number">{this.props.expandedPo.validFromDate}</span>
                                                </div>
                                                <div className="cafdr-inner">
                                                    <label className="head-label">PO Valid To</label>
                                                    <span className="cafd-number"> {this.props.expandedPo.validToDate}</span>
                                                </div>
                                            </div>
                                            {this.state.createAsnFromToValidity && <div className="cafd-row">
                                                <div className="cafdr-inner">
                                                    <label className="head-label">ASN Valid From</label>
                                                    <span className="cafd-number">{this.state.shipmentDetails.asnFromDate}</span>
                                                </div>
                                                <div className="cafdr-inner">
                                                    <label className="head-label">ASN Valid To</label>
                                                    <span className="cafd-number"> {this.state.shipmentDetails.asnToDate}</span>
                                                </div>
                                            </div>}
                                        </div>
                                    </div>
                                    {/* <div className="m-top-5 verticalInlineFlex"> */}
                                    <div className="col-md-9 pad-0">
                                        <div className="caf-fields">
                                            <div className={this.state.isQcOn === "TRUE" ? "caff-top" :"caff-top caff-set-upload-packing"}>
                                                <div className="cafft-input dateFormat">
                                                    <label className="caffti-label">Expected Delivery Date<span className="mandatory">*</span></label>
                                                    <input type="date" className="inputDate" 
                                                    min={(this.state.createAsnFromToValidity && this.state.shipmentDetails.isCreateASNAllowed == 1) ? (todayDate > this.state.shipmentDetails.asnFromSelectionDate ? todayDate : this.state.shipmentDetails.asnFromSelectionDate ): (todayDate > this.state.validFromDateSelect ? todayDate : this.state.validFromDateSelect)} 
                                                    max={this.state.createAsnFromToValidity ? this.state.shipmentDetails.asnToSelectionDate : this.state.validToDateSelect} 
                                                    data-date={this.state.formattedDate == "" ? "Select Date" : this.state.formattedDate} 
                                                    onChange={this.handleRequestedDate} />
                                                    {/* <input type="date" className="inputDate" min={todayDate} max={this.state.maxValue} placeholder={this.state.shipmentRequestedDate == "" ? "Select Date" : this.state.shipmentRequestedDate} value={this.state.shipmentRequestedDate} onChange={this.handleRequestedDate} /> */}
                                                    {dateerr ? (
                                                        <span className="error">Select Date </span>
                                                    ) : null}
                                                </div>
                                                {this.state.isQcOn === "TRUE" ? null : this.state.allowPackingList && <div className="caff-check">
                                                    <div className="caffc-fields">
                                                        <label className="checkBoxLabel0">
                                                            <input type="checkBox" name="selectEach" checked={this.state.packingListChecked} id="packingListChecked" onChange={this.handleChange}/>
                                                            <span className="checkmark1"></span>
                                                            Packing List
                                                        </label>
                                                        { this.state.packingListChecked  ?
                                                        <React.Fragment>
                                                            <div className="caffb-upload-file">
                                                                <label onClick={(e) => this.openPpsUploadModal(e,"PACKING")}>
                                                                    {this.state.packingArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{this.state.packingArray.length} Packing Files</span>}
                                                                    <img src={require('../../../../assets/attach.svg')} />
                                                                </label>
                                                                <span id="ppsfileLabel"></span>
                                                                {this.state.packingListError ? (
                                                                    <span className="error">
                                                                        Select Files
                                                                </span>
                                                                ) : null}
                                                            </div>
                                                        </React.Fragment>
                                                        :null}
                                                    </div>
                                                </div>}
                                                {this.state.isQcOn === "TRUE" && <div className="cafft-input dateFormat">
                                                    <label className="caffti-label">Insp from date<span className="mandatory">*</span></label>
                                                    {this.state.shipmentRequestedDate != "" ? 
                                                    <input type="date" className="inputDate " id="qcFromDate" 
                                                    data-date={this.state.qcFromFormatted == "" ? "Select Date" : this.state.qcFromFormatted} 
                                                    min={(this.state.createAsnFromToValidity && this.state.shipmentDetails.isCreateASNAllowed == 1) ? (todayDate > this.state.shipmentDetails.asnFromSelectionDate ? todayDate : this.state.shipmentDetails.asnFromSelectionDate ) : (this.state.validFromDateSelect < todayDate ? todayDate : this.state.validFromDateSelect)} 
                                                    max={this.state.shipmentRequestedDate == "" ? this.state.validToDateSelect : this.state.shipmentRequestedDate} 
                                                    onChange={this.handleChange} />
                                                        : <input type="date" className="inputDate " id="qcFromDate" data-date="Select Date" disabled />}
                                                    {/* <input type="date" className="inputDate" id="qcFromDate" min={todayDate} max={this.state.maxValue} placeholder={this.state.qcFromDate == "" ? "Select Date" : this.state.qcFromDate} value={this.state.qcFromDate} onChange={this.handleChange} /> */}
                                                    {qcFromDateErr ? (
                                                        <span className="error">
                                                            Select Date
                                                    </span>
                                                    ) : null}
                                                </div>}
                                                {this.state.isQcOn === "TRUE" &&<div className="cafft-input dateFormat">
                                                    <label className="caffti-label">Insp to date<span className="mandatory">*</span></label>
                                                    {this.state.qcFromDate == "" ? <input type="date" className="inputDate" disabled value="" data-date="Select Date" /> : <input type="date" className="inputDate" id="qcToDate" min={this.state.maxQCToDate} max={this.state.shipmentRequestedDate == "" ? this.state.validToDateSelect : this.state.shipmentRequestedDate} data-date={this.state.qcToFormatted == "" ? "Select Date" : this.state.qcToFormatted} onChange={this.handleChange} />}
                                                    {/* {this.state.qcFromDate == "" ? <input type="date" className="inputDate" disabled value="" placeholder={this.state.qcToDate == "" ? "Select Date" : this.state.qcToDate} /> : <input type="date" className="inputDate" id="qcToDate" min={this.state.qcFromDate} max={this.state.maxValue} placeholder={this.state.qcToDate == "" ? "Select Date" : this.state.qcToDate} value={this.state.qcToDate} onChange={this.handleChange} />} */}
                                                    {qcToDateErr ? (
                                                        <span className="error">
                                                            Select Date
                                                    </span>
                                                    ) : null}
                                                </div>}
                                                {this.state.isQcOn === "TRUE" &&<div className="cafft-input">
                                                    <label className="caffti-label">Contact Person<span className="mandatory">*</span></label>
                                                    <input type="text" className="caffti-input" id="contactName" value={contactName} onChange={this.handleChange}/>
                                                    {contactNameErr ? (
                                                        <span className="error">
                                                            Enter Valid Contact Name
                                                    </span>
                                                    ) : null}
                                                </div>}
                                                {this.state.isQcOn === "TRUE" &&
                                                <div className="cafft-input">
                                                    <label className="caffti-label">Contact Number<span className="mandatory">*</span></label>
                                                    <input type="text" id="contactNumber" className="caffti-input"value={contactNumber} onChange={this.handleChange}/>
                                                    {contactNumberErr ? (
                                                        <span className="error">
                                                            Enter 10 Digit Contact Number
                                                    </span>
                                                    ) : null}
                                                </div>}
                                            </div>
                                            {this.state.isQcOn === "TRUE" &&
                                            <div className="caff-mid">
                                                <div className="caffm-search">
                                                    <label className="caffti-label">Address<span className="mandatory">*</span></label>
                                                    <input type="text" className="caffti-input" autoComplete="off" value={address} id="address" onChange={this.handleChange} placeholder="Type to Search" onKeyDown={(e) => this._handleKeyPress(e, "address")} />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick ={(e) => this.openAddressModal(e, "address")}>
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                    {addressErr ? (
                                                        <span className="error">
                                                            Select valid address
                                                    </span>
                                                    ) : null}
                                                    {!this.state.isModalShow ? this.state.addressModal ? <div className="backdrop-transparent"></div> : null : null}
                                                    {!this.state.isModalShow ? this.state.addressModal ? <AddressModal {...this.props} {...this.state} closeSupplier={(e) => this.openAddressModal(e, "address")} addressSearch={this.state.addressSearch} addressVal={this.addressVal} onCloseAddressModal={(e) => this.onCloseAddressModal(e)} /> : null : null}
                                                </div>
                                                <div className="caffm-search">
                                                    <label className="caffti-label">Location<span className="mandatory">*</span></label>
                                                    <input type="text" className="caffti-input" autoComplete="off" value={location} placeholder="Type to Search" onKeyDown={(e) => this._handleKeyPress(e, "location")} id="location" onChange={this.handleChange}/>
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick ={(e) => this.openLocationModal(e, "location")}>
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                    {locationErr ? (
                                                        <span className="error">
                                                            Select valid location
                                                    </span>
                                                    ) : null}
                                                    {!this.state.isModalShow ? this.state.locationModal ? <div className="backdrop-transparent"></div> : null : null}
                                                    {!this.state.isModalShow ? this.state.locationModal ? <LocationModal {...this.props} {...this.state} locationVal={this.locationVal} onCloseLocationModal={(e) => this.onCloseLocationModal(e)} /> : null : null}
                                                 </div>
                                            </div>
                                            }
                                             {this.state.isQcOn === "TRUE" &&
                                            <div className="caff-check">
                                                <div className="caffc-fields">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" checked={this.state.ppsChecked} id="ppsCheckBox" onChange={this.handleChange}/>
                                                        <span className="checkmark1"></span>
                                                        PPS Available
                                                    </label>
                                                    { this.state.ppsChecked  ?
                                                    <React.Fragment>
                                                        <div className="caffb-upload-file">
                                                            <label onClick={(e) => this.openPpsUploadModal(e,"PPS")}>
                                                                {/* <input type="file" id="ppsFileUpload"  onChange={(e) => this.onMmltipleFileUpload(e,"PPS")} multiple="multiple"  /> */}
                                                                 {this.state.PPSArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{this.state.PPSArray.length} PPS Files</span>}
                                                                <img src={require('../../../../assets/attach.svg')} />
                                                            </label>
                                                            <span id="ppsfileLabel"></span>
                                                            {ppsFilesError ? (
                                                                <span className="error">
                                                                    Select Files
                                                            </span>
                                                            ) : null}
                                                        </div>
                                                    </React.Fragment>
                                                    :null}
                                                </div>
                                                <div className="caffc-fields">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" checked={this.state.testReportChecked} id="testReportCheckBox" onChange={this.handleChange}/>
                                                        <span className="checkmark1"></span>
                                                        Test Report
                                                    </label>
                                                    { this.state.testReportChecked  ?
                                                    <React.Fragment>
                                                    <div className="caffb-upload-file">
                                                        <label onClick={(e) => this.openPpsUploadModal(e,"REPORT")}>
                                                            {/* <input type="file" id="reportFileUpload"  onChange={(e) => this.onMmltipleFileUpload(e, "REPORT")} multiple="multiple"  /> */}
                                                            {this.state.ReportArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{this.state.ReportArray.length} Test Reports</span>}
                                                            <img src={require('../../../../assets/attach.svg')} />
                                                        </label>
                                                        <span id="reportFileLabel"></span>
                                                        {reportFilesError ? (
                                                            <span className="error">
                                                                Select Files
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                    </React.Fragment>
                                                    :null}
                                                </div>
                                                {this.state.allowPackingList && <div className="caffc-fields">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" checked={this.state.packingListChecked} id="packingListChecked" onChange={this.handleChange}/>
                                                        <span className="checkmark1"></span>
                                                        Packing List
                                                    </label>
                                                    { this.state.packingListChecked  ?
                                                    <React.Fragment>
                                                        <div className="caffb-upload-file">
                                                            <label onClick={(e) => this.openPpsUploadModal(e,"PACKING")}>
                                                                 {this.state.packingArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{this.state.packingArray.length} Packing Files</span>}
                                                                <img src={require('../../../../assets/attach.svg')} />
                                                            </label>
                                                            <span id="ppsfileLabel"></span>
                                                            {this.state.packingListError ? (
                                                                <span className="error">
                                                                    Select Files
                                                            </span>
                                                            ) : null}
                                                        </div>
                                                    </React.Fragment>
                                                    :null}
                                                </div>}
                                            </div>}
                                             <div className="caff-bottom">
                                                {/* <div className="caffb-upload-file">
                                                    <label>
                                                        <input type="file" id="file" />
                                                        <span>Upload File</span>
                                                        <img src={require('../../../../assets/attach.svg')} />
                                                    </label>
                                                    <span className="caffbuf-file-name">Data_file.xls <img src={require('../../../../assets/clearSearch.svg')} /></span>
                                                </div> */}
                                                     <div className="gvst-form-action">
                                                    {this.state.saveShow ? <button type="button" className="gvstfa-create" onClick={() => this.createFinalShipment()}>{this.state.isQcOn == "TRUE" ? "Create Inspection" : "Create ASN"}</button>
                                                        : <button type="button" className="gvstfa-create btnDisabled">{this.state.isQcOn == "TRUE" ? "Create Inspection" : "Create ASN"}</button>}
                                                    </div>
                                            </div>
                                            {/* <div className="caff-check">
                                                <label className="checkBoxLabel0">
                                                    <input type="checkBox" name="selectEach" checked={this.state.ppsChecked} id="ppsCheckBox" onChange={this.handleChange}/>
                                                    <span className="checkmark1"></span>
                                                    PPS Available
                                                </label>
                                                <label className="checkBoxLabel0">
                                                    <input type="checkBox" name="selectEach" checked={this.state.testReportChecked} id="testReportCheckBox" onChange={this.handleChange}/>
                                                    <span className="checkmark1"></span>
                                                    Test Report
                                                </label>
                                            </div> */}
                                           {/*  {this.state.ppsChecked ?<div className="caff-bottom">
                                                 <div className="caffb-upload-file">
                                                    <label>
                                                        <input type="file" id="fileUpload"  onChange={(e) => this.onMmltipleFileUpload(e,"PPS")} multiple="multiple" />
                                                        <span>Upload File</span>
                                                        <img src={require('../../../../assets/attach.svg')} />
                                                    </label>
                                                    <span id="ppsfileLabel"></span>
                                                  {this.state.PPSArray.length != 0 ? this.state.PPSArray.map((hdata, key) => <span key={key} onClick={() => window.open(hdata.fileURL)} className="caffbuf-file-name">{hdata.fileName}<img src={require('../../../../assets/clearSearch.svg')} onClick={(e) => this.deleteUploads(hdata, "PPS")}/></span>)
                                                    : null} 
                                                    
                                                </div>
                                            </div>: null} */}
                                            {/* <div className="caff-bottom">
                                            {this.state.testReportChecked ?
                                                <div className="caffb-upload-file">
                                                    <label>
                                                        <input type="file" id="fileUpload"  onChange={(e) => this.onFileUpload(e, "REPORT")} multiple="multiple" />
                                                        <span>Upload File</span>
                                                        <img src={require('../../../../assets/attach.svg')} />
                                                    </label>
                                                    <span id="ppsfileLabel"></span>
                                                 </div>
                                                 : null}
                                               <div className="gvst-form-action">
                                                    {this.state.saveShow ? <button type="button" className="saveNew" onClick={() => this.createFinalShipment()}>Create ASN</button>
                                                    : <button type="button" className="saveNew btnDisabled">Create ASN</button>}
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>
                                    {/* <div className="col-md-12 pad-0 m-top-20">
                                        <div className="col-md-3">
                                            <div className="col-md-12 pad-0">
                                                <label className="head">Remarks</label>
                                            </div>
                                            <div className="col-md-12 pad-0">
                                                <textarea className="inputBox width80per borderRadius4 m-top-10" rows="2" id="remarks" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div> */}
                                </form>
                            </div> : null}
                            {/* <div className="col-md-12 m-top-10">
                            <div className="col-md-8"></div>
                            <div className="col-md-4 text-right newSearch clearIconPos">
                                <input type="search" className="searchWid" value={this.state.search} onChange={this.onSearch} onKeyDown={this.onSearch} placeholder="Search..." />
                                {search != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.searchClear} /></span> : null}
                            </div>
                            </div> */}
                            <div className="col-md-12 pad-0">
                                {/* <SetLevelConfigHeader {...this.props} {...this.state} setLvlColoumSetting={this.state.setLvlColoumSetting} setLvlGetHeaderConfig={this.state.setLvlGetHeaderConfig} setLvlResetColumnConfirmation={(e) => this.setLvlResetColumnConfirmation(e)} setLvlOpenColoumSetting={(e) => this.setLvlOpenColoumSetting(e)} setLvlCloseColumn={(e) => this.setLvlCloseColumn(e)} setLvlPushColumnData={(e) => this.setLvlpushColumnData(e)} setLvlsaveColumnSetting={(e) => this.setLvlsaveColumnSetting(e)} />
                                 */}
                                 <div className="gvst-inner" id="itemTable">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th className="fix-action-btn"><label></label></th>
                                                {this.props.poType == "poicode" ? 
                                                this.props.itemCustomHeadersState.length == 0 ? this.props.getItemHeaderConfig.map((data, key) => (
                                                        <th key={key}>
                                                            <label>{data}</label>
                                                        </th>
                                                    )) : this.props.itemCustomHeadersState.map((data, key) => (
                                                        <th key={key}>
                                                            <label>{data}</label>
                                                        </th>
                                                    ))
                                                :this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                                        <th key={key}>
                                                            <label>{data}</label>
                                                        </th>
                                                    )) : this.props.setCustomHeadersState.map((data, key) => (
                                                        <th key={key}>
                                                            <label>{data}</label>
                                                        </th>
                                                ))}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.setBaseExpandDetails.length != 0 ? this.state.setBaseExpandDetails.map((data, key) => (
                                                <React.Fragment key={key}>
                                                    <tr>
                                                        <td className="fix-action-btn">
                                                            <ul className="table-item-list">
                                                            {this.props.poType == "poicode" ? "" : <li className="til-inner" id={data.setBarCode} onClick={(e) => this.expandColumn(data.setBarCode, e, data)}>
                                                                        {this.state.dropOpen && this.state.expandedId == data.setBarCode ? 
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                        <path fill="#a4b9dd" fillRule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"/>
                                                                    </svg> 
                                                                    : 
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                        <path fill="#a4b9dd" fillRule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                                                    </svg> }
                                                                </li>}
                                                            </ul>
                                                            
                                                        </td>
                                                        {this.props.poType == "poicode" ? 
                                                            this.props.itemHeaderSummary.length == 0 ? this.props.itemDefaultHeaderMap.map((hdata, key) => (
                                                                <td key={key} >
                                                                {hdata == "newRequestedQty" ? ( this.props.status == "APPROVED" ? <span className="table-per-tooptip"><input type="text" value={data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} onMouseOver={(e) =>this.calculatedPercent(e,data)}  className={data["percentFulfilFlag"] ? "errorBorder" : ""} placeholder={data["requestedQty"] != undefined ? (data["requestedQty"] == "" ? 0 : data["requestedQty"]) : (data[hdata] == "" ? 0 : data[hdata])} autoComplete="off"/><span className="generic-tooltip">{this.state.calculatedPercent} %</span></span> : <label>{data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]}</label> ) :
                                                                    hdata == "setPendingQty" ? <label>{this.state.allowMultipleAsn ? data["pendingQty"] : 0 }</label> : 
                                                                        hdata == "newCancelledQty" ? ( this.props.status == "APPROVED" && this.state.allowMultipleAsn ? <input type="text" value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[hdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data["cancelledQty"] != undefined ? (data["cancelledQty"] == "" ? 0 : data["cancelledQty"]) : (data[hdata] == "" ? 0 : data[hdata])} autoComplete="off"/> : <label>{data["cancelledQty"] != undefined ? data["pendingQty"] : data[hdata]}</label> ) :
                                                                            <label>{data[hdata]}</label>}
                                                                </td>
                                                            )) : this.props.itemHeaderSummary.map((sdata, keyy) => (
                                                                <td key={keyy} >
                                                                    {sdata == "newRequestedQty" ? ( this.props.status == "APPROVED" ? <span className="table-per-tooptip"><input type="text" value={data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} onMouseOver={(e) =>this.calculatedPercent(e,data)} className={data["percentFulfilFlag"] ? "errorBorder" : ""} placeholder={data["requestedQty"] != undefined ? (data["requestedQty"] == "" ? 0 : data["requestedQty"]) : (data[sdata] == "" ? 0 : data[sdata])} autoComplete="off"/><span className="generic-tooltip">{this.state.calculatedPercent} %</span></span> : <label>{data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]}</label> ) :
                                                                        sdata == "setPendingQty" ? <label>{this.state.allowMultipleAsn ? data["pendingQty"] : 0 }</label> :
                                                                            sdata == "newCancelledQty" ? ( this.props.status == "APPROVED" && this.state.allowMultipleAsn ? <input type="text" value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[sdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data["cancelledQty"] != undefined ? (data["cancelledQty"] == "" ? 0 : data["cancelledQty"]) : (data[sdata] == "" ? 0 : data[sdata])} autoComplete="off"/> : <label>{data["cancelledQty"] != undefined ? data["pendingQty"] : data[sdata]}</label> ) :
                                                                                <label>{data[sdata]}</label>}
                                                                </td>
                                                            ))
                                                        : this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                                                <td key={key} >
                                                                {hdata == "newRequestedQty" ? ( this.props.status == "APPROVED" ? <span className="table-per-tooptip"><input type="text" value={data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} onMouseOver={(e) =>this.calculatedPercent(e,data)}  className={data["percentFulfilFlag"] ? "errorBorder" : ""} placeholder={data["requestedQty"] != undefined ? (data["requestedQty"] == "" ? 0 : data["requestedQty"]) : (data[hdata] == "" ? 0 : data[hdata])} autoComplete="off"/><span className="generic-tooltip">{this.state.calculatedPercent} %</span></span> : <label>{data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]}</label> ) :
                                                                    hdata == "setPendingQty" ? <label>{this.state.allowMultipleAsn ? data["pendingQty"] : 0 }</label> : 
                                                                        hdata == "newCancelledQty" ? ( this.props.status == "APPROVED" && this.state.allowMultipleAsn ? <input type="text" value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[hdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data["cancelledQty"] != undefined ? (data["cancelledQty"] == "" ? 0 : data["cancelledQty"]) : (data[hdata] == "" ? 0 : data[hdata])} autoComplete="off"/> : <label>{data["cancelledQty"] != undefined ? data["pendingQty"] : data[hdata]}</label> ) :
                                                                            <label>{data[hdata]}</label>}
                                                            </td>
                                                            )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                                                <td key={keyy} >
                                                                    {sdata == "newRequestedQty" ? ( this.props.status == "APPROVED" ? <span className="table-per-tooptip"><input type="text" value={data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} onMouseOver={(e) =>this.calculatedPercent(e,data)} className={data["percentFulfilFlag"] ? "errorBorder" : ""} placeholder={data["requestedQty"] != undefined ? (data["requestedQty"] == "" ? 0 : data["requestedQty"]) : (data[sdata] == "" ? 0 : data[sdata])} autoComplete="off"/><span className="generic-tooltip">{this.state.calculatedPercent} %</span></span> : <label>{data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]}</label> ) :
                                                                        sdata == "setPendingQty" ? <label>{this.state.allowMultipleAsn ? data["pendingQty"] : 0 }</label> :
                                                                            sdata == "newCancelledQty" ? ( this.props.status == "APPROVED" && this.state.allowMultipleAsn ? <input type="text" value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[sdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data["cancelledQty"] != undefined ? (data["cancelledQty"] == "" ? 0 : data["cancelledQty"]) : (data[sdata] == "" ? 0 : data[sdata])} autoComplete="off"/> : <label>{data["cancelledQty"] != undefined ? data["pendingQty"] : data[sdata]}</label> ) :
                                                                                <label>{data[sdata]}</label>}
                                                                </td>
                                                        ))}  

                                                    </tr>
                                                    {this.state.dropOpen && this.state.expandedId == data.setBarCode ? <tr><td colSpan="100%" className="pad-0"><ItemLevelDetails {...this.state}  {...this.props} set_Display="VENDOR_CANCELLED_SET" item_Display={this.props.item_Display} /> </td></tr> : null}

                                                </React.Fragment>)) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <div className="pagerDiv newPagination text-right justifyEnd alignMiddle displayFlex pad-top-35 expandPagination">
                            <Pagination {...this.state} page={this.page} />
                            </div> */}
                            {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                        </div>
                        {this.state.ppsUploadModal && <CreateAsnUpload CancelPpsUploadModal={this.CancelPpsUploadModal} onFileUploadSubmit={this.onFileUploadSubmit} newPPSArray ={this.state.PPSArray} newReportArray = {this.state.ReportArray} newPackingArray={this.state.packingArray} {...this.state} {...this.props}/>}
                    </div>
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.setLvlConfirmModal ? <SetLevelConfirmationModal {...this.state} {...this.props} setLvlCloseConfirmModal={(e) => this.setLvlCloseConfirmModal(e)} setLvlResetColumn={(e) => this.setLvlResetColumn(e)} /> : null}
                    
                </div>
            </div>
        )
    }
}
