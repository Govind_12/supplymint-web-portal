import React from "react";

class FilterDeliverAndIntransit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 1,
            no: 1,
            lgtNumber: this.props.lgtNumber,
            lgtDate: this.props.lgtDate,
            status: this.props.status,
            vendorName: this.props.vendorName,
            transporterName: this.props.transporterName,
            ownerSite: this.props.ownerSite,
            transportMode: this.props.transportMode,
            stationFrom: this.props.stationFrom,
            stationTo: this.props.stationTo,
            lgtReceivedQty: this.props.lgtReceivedQty,
            vehicleNo: this.props.vehicleNo,

            count: this.props.filterCount,
            userType: this.props.userType

        };
    }


    componentWillMount() {
        this.setState({
            lgtNumber: this.props.lgtNumber,
            lgtDate: this.props.lgtDate,
            status: this.props.status,
            vendorName: this.props.vendorName,
            transporterName: this.props.transporterName,
            ownerSite: this.props.ownerSite,
            transportMode: this.props.transportMode,
            stationFrom: this.props.stationFrom,
            stationTo: this.props.stationTo,
            lgtReceivedQty: this.props.lgtReceivedQty,
            vehicleNo: this.props.vehicleNo,
            count: this.props.filterCount,
        })
    }

    handleChange(e) {
        if (e.target.id == "lgtNumber") {
            this.setState({
                lgtNumber: e.target.value
            });
        } else if (e.target.id == "lgtDate") {
            this.setState({
                lgtDate: e.target.value
            });
        } else if (e.target.id == "vendorName") {
            this.setState({
                vendorName: e.target.value
            })

        } else if (e.target.id == "transporterName") {
            this.setState({
                transporterName: e.target.value
            });
        } else if (e.target.id == "ownerSite") {
            this.setState({
                ownerSite: e.target.value
            });
        } else if (e.target.id == "transportMode") {

            this.setState({
                transportMode: e.target.value
            });

        } else if (e.target.id == "stationFrom") {
            this.setState({
                stationFrom: e.target.value
            })

        } else if (e.target.id == "stationTo") {
            this.setState({
                stationTo: e.target.value
            });
        } else if (e.target.id == "lgtReceivedQty") {
            if (e.target.validity.valid) {
                this.setState({
                    lgtReceivedQty: e.target.value
                });
            }
        } else if (e.target.id == "vehicleNo") {

            this.setState({
                vehicleNo: e.target.value
            });

        }
    }

    clearFilter(count) {
        this.setState({
            lgtNumber: "",
            lgtDate: "",
            status: "",
            vendorName: "",
            transporterName: "",
            ownerSite: "",
            transportMode: "",
            stationFrom: "",
            stationTo: "",
            lgtReceivedQty: "",
            vehicleNo: "",
            count: 0
        })


        this.props.filterCount != 0 ? this.props.clearFilter() : null
    }

    onSubmit(count) {
        // e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            lgtNumber: this.state.lgtNumber,
            lgtDate: this.state.lgtDate,
            status: this.state.status,
            vendorName: this.state.vendorName,
            transporterName: this.state.transporterName,
            ownerSite: this.state.ownerSite,
            transportMode: this.state.transportMode,
            stationFrom: this.state.stationFrom,
            stationTo: this.state.stationTo,
            lgtReceivedQty: this.state.lgtReceivedQty,
            vehicleNo: this.state.vehicleNo,
            filterCount: count,
            search: "",
            userType: this.props.userType
        }
        this.props.getAllVendorTransitNDeliverRequest(data);
        this.props.closeFilter();
        this.props.updateFilter(data)
    }

    render() {

        let count = 0;
        if (this.state.lgtNumber != "") {
            count++;
        }
        if (this.state.lgtDate != "") {
            count++;
        }
        if (this.state.vendorName != "") {
            count++

        }
        if (this.state.transporterName != "") {
            count++

        } if (this.state.ownerSite != "") {
            count++

        }
        if (this.state.transportMode != "") {
            count++;
        }
        if (this.state.stationFrom != "") {
            count++;
        }
        if (this.state.stationTo != "") {
            count++

        }
        if (this.state.lgtReceivedQty != "") {
            count++

        }
        if (this.state.vehicleNo != "") {
            count++

        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(count)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="lgtNumber" value={this.state.lgtNumber} placeholder="LGT Number" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="lgtDate" value={this.state.lgtDate} placeholder={this.state.lgtDate != "" ? this.state.lgtDate : "LGT Date"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorName" value={this.state.vendorName} placeholder="Vendor Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="transporterName" value={this.state.transporterName} placeholder="Transporter Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="ownerSite" value={this.state.ownerSite} placeholder="Owner Site" className="organistionFilterModal" />


                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="transportMode" value={this.state.transportMode} placeholder="Transporter Mode" className="organistionFilterModal" />
                                    </li>
                                </ul></div></div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">


                                        <li>
                       
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="stationFrom" value={this.state.stationFrom} placeholder="Station From" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)}  id="stationTo" value={this.state.stationTo} placeholder="Station To" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} pattern="[0-9]*" id="lgtReceivedQty" value={this.state.lgtReceivedQty} placeholder="LGT received qty" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vehicleNo" value={this.state.vehicleNo} placeholder="Vehicle No." className="organistionFilterModal" />
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.state.lgtDate == "" && this.state.lgtNumber == "" && this.state.vendorName == "" && this.state.transporterName == "" && this.state.ownerSite == "" && this.state.transportMode == "" && this.state.stationFrom == "" && this.state.stationTo == "" && this.state.lgtReceivedQty == "" && this.state.vehicleNo == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button type="button" onClick={(e) => this.clearFilter(0)} className="modal_clear_btn">
                                                CLEAR FILTER
                                         </button>}
                                    </li>
                                    <li>
                                        {this.state.lgtDate != "" || this.state.lgtNumber != "" || this.state.vendorName != "" || this.state.transporterName != "" || this.state.ownerSite != "" || this.state.transportMode != "" || this.state.stationFrom != "" || this.state.stationTo != "" || this.state.lgtReceivedQty != "" || this.state.vehicleNo != ""? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default FilterDeliverAndIntransit;
