import React, { Component } from 'react'
import AddShipmentAdviceModal from './addShipmentAdviceModal';
import AddIcon from '../../../../assets/add.svg';
import downIcon from '../../../../assets/arrow-right.svg';
import DeleteIcon from '../../../../assets/deleteotb.svg';
import { handleChange } from '../../../../helper/index';
import { getDate, get_mmddyy } from '../../../../helper';
import ToastLoader from '../../../loaders/toastLoader';
import TransporterModal from './transportermodal';
import moment from 'moment'
import VendorTransporter from '../../modals/vendorTransporter';
import { eventChannel } from 'redux-saga';

const currentDate = moment(new Date()).format("YYYY-MM-DD")
export default class LrCreationModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toastLoader: false,
            toastMsg: "",
            actionExpand: false,
            sliderDisable: false,
            rangeVal: 1,
            logisticDate: getDate(),
            lrNo: "",
            lrDate: currentDate,
            consignorCode: this.props.checkedShipmentData[0].vendorCode,
            //transporter: this.props.checkedShipmentData[0].transporterId !== null ? this.props.checkedShipmentData[0].transporterName : "",
            //transporterCode: this.props.checkedShipmentData[0].transporterId,
            transporter: "",
            transporterCode: "",
            mode: "",
            vehicleNo: "",
            policyNo: "",
            declarationAmt: "",
            declarationAmterr: "",
            toPay: "",
            actualWeight: "",
            chargedRate: "",
            chargedRateerr: false,
            chargedFreighterr: false,
            chargedFreight: "",
            permitNo: "",
            remarks: "",
            lrDetailList: this.props.checkedShipmentData,
            //mandatory field
            lrNoerr: false,
            lrDateerr: false,
            modeerr: false,
            transportererr: false,
            vehicleNoerr: false,
            toPayerr: false,
            actualWeighterr: false,
            checkedLrList: [],
            addAdviceModal: false,
            lrList: [],
            transporterAnimation: false,
            transporterModal: false,
            totalLrQty: Number(this.props.checkedShipmentData[0].totalRequestedQty),
            lrBales: Number(this.props.checkedShipmentData[0].lrBales),
            lrFormattedDate: currentDate,
            invoiceFormattedDate: "",
            autoPopulateDate: [],
            consignee: "",
            vehicleType: "",
            vehicleTypeerr: "",
            consignmentNo: "",
            consignmentNoerr: false,
            courierName: "",
            courierNameerr: false,
            etaDate: "",
            transportarData: {},
            otherTransportFlag: false,
            otherTransporterValue: "",
            otherTransporterErr: false,
            showManageAsn: false,

        }
    }
    openManageAsn(e) {
        e.preventDefault();
        this.setState({
            showManageAsn: !this.state.showManageAsn
        });
    }
    closeManageAsn = (e) => {
        this.setState({
            showManageAsn: false,
        })
    }
    chargedRate() {
        let decimalPatternWithNumber = /^\d+(\.\d{1,2})?$/
        if( this.state.chargedRate == ""){
            this.setState({ chargedRateerr: false})
        }
        else if( decimalPatternWithNumber.test(this.state.chargedRate) && Number(this.state.chargedRate) > 0){
            this.setState({
                chargedRateerr: false
            })
        }else{
            this.setState({
                chargedRateerr: true
            })
            this.setState({
                toastMsg: "Enter Valid Charged rate",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        }
    }
    // etaDate() {
    //     if (this.state.etaDate == "") {
    //         this.setState({
    //             etaDateerr: true
    //         })
    //         this.setState({
    //             toastMsg: "Enter ETA ",
    //             toastLoader: true
    //         })
    //         setTimeout(() => {
    //             this.setState({
    //                 toastLoader: false
    //             })
    //         }, 1500);
    //     } else {
    //         this.setState({
    //             etaDateerr: false
    //         })
    //     }
    // }
    // chargedFreight() {
    //     if (this.state.chargedFreight == "") {
    //         this.setState({
    //             chargedFreighterr: true
    //         })
    //         this.setState({
    //             toastMsg: "Enter Valid Charged Freight",
    //             toastLoader: true
    //         })
    //         setTimeout(() => {
    //             this.setState({
    //                 toastLoader: false
    //             })
    //         }, 1500);
    //     } else {
    //         this.setState({
    //             chargedFreighterr: false
    //         })
    //     }
    // }
    // courierName() {
    //     if (this.state.courierName == "") {
    //         this.setState({
    //             courierNameerr: true
    //         })
    //         this.setState({
    //             toastMsg: "Enter Valid Courier Name ",
    //             toastLoader: true
    //         })
    //         setTimeout(() => {
    //             this.setState({
    //                 toastLoader: false
    //             })
    //         }, 1500);
    //     } else {
    //         this.setState({
    //             courierNameerr: false
    //         })
    //     }
    // }
    // consignmentNo() {
    //     if (this.state.consignmentNo == "") {
    //         this.setState({
    //             consignmentNoerr: true
    //         })
    //         this.setState({
    //             toastMsg: "Select Consignment No. ",
    //             toastLoader: true
    //         })
    //         setTimeout(() => {
    //             this.setState({
    //                 toastLoader: false
    //             })
    //         }, 1500);
    //     } else {
    //         this.setState({
    //             consignmentNoerr: false
    //         })
    //     }
    // }
    // vehicleType() {
    //     if (this.state.vehicleType == "") {
    //         this.setState({
    //             vehicleTypeerr: true
    //         })
    //         this.setState({
    //             toastMsg: "Select Vechile Type ",
    //             toastLoader: true
    //         })
    //         setTimeout(() => {
    //             this.setState({
    //                 toastLoader: false
    //             })
    //         }, 1500);
    //     } else {
    //         this.setState({
    //             vehicleTypeerr: false
    //         })
    //     }
    // }
    lrNo() {
        if (this.state.lrNo == "") {
            this.setState({
                lrNoerr: true
            })
            this.setState({
                toastMsg: "Enter LR No. ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        } else {
            this.setState({
                lrNoerr: false
            })
        }
    }
    lrDate() {
        if (this.state.lrDate == "") {
            this.setState({
                lrDateerr: true
            })
            this.setState({
                toastMsg: "Select LR Date ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        } else {
            this.setState({
                lrDateerr: false
            })
        }
    }

    // mode() {
    //     if (this.state.mode == "") {
    //         this.setState({
    //             modeerr: true
    //         })
    //         this.setState({
    //             toastMsg: "Select Mode ",
    //             toastLoader: true
    //         })
    //         setTimeout(() => {
    //             this.setState({
    //                 toastLoader: false
    //             })
    //         }, 1500);
    //     } else {
    //         this.setState({
    //             modeerr: false
    //         })
    //     }
    // }
    transporter =()=> {
        let flag = this.state.transporterCode == "" ? true : false;
        if (this.state.transporter == "" || flag) {
            this.setState({
                transportererr: true
            })
        } else {
            this.setState({
                transportererr: false
            })
        }
    }
    otherTransporter = () => {
        if( this.state.otherTransportFlag ){
            if (this.state.otherTransporterValue == "") {
                this.setState({
                    otherTransporterErr: true
                })
            } else {
                this.setState({
                    otherTransporterErr: false
                })
            }
        }else {
                this.setState({
                    otherTransporterErr: false
                })
        }
        
    }
    vehicleNo() {
        if (this.state.vehicleNo == "") {
            this.setState({
                vehicleNoerr: false
            })    
        }else if(this.state.vehicleNo.length >= 6 && this.state.vehicleNo.length <= 15){
            this.setState({
                vehicleNoerr: false
            }) 
        }else{
            this.setState({
                vehicleNoerr: true
            })   
            this.setState({
                toastMsg: "Enter Valid Vechile No. ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        }
    }
    toPay() {
        if (this.state.toPay == "") {
            this.setState({
                toPayerr: true
            })
            this.setState({
                toastMsg: "Select LR Type",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        } else {
            this.setState({
                toPayerr: false
            })
        }
    }
    actualWeight() {
        let decimalPatternWithNumber = /^\d+(\.\d{1,2})?$/
        if( this.state.actualWeight == ""){
            this.setState({ actualWeighterr: false})
        }
        else if( decimalPatternWithNumber.test(this.state.actualWeight) && Number(this.state.actualWeight) > 0){
            this.setState({
                actualWeighterr: false
            })
        }else {
            this.setState({
                actualWeighterr: true
            })
            this.setState({
                toastMsg: "Weight must be greater than zero..",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        }
    }
    declarationAmt() {
        if (this.state.declarationAmt == "") {
            this.setState({
                declarationAmterr: true
            })
            this.setState({
                toastMsg: "Enter Declaration Amt ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        } else {
            this.setState({
                declarationAmterr: false
            })
        }
    }
    handleChangeInput(e) {
        if (e.target.id == "lrNo") {
            this.setState({
                lrNo: e.target.value
            },
                () => {
                    this.lrNo();
                });

        } else if (e.target.id == "etaDate") {
            this.setState({
                etaDate: e.target.value
            }, () => {
                //this.etaDate()
            })
        } else if (e.target.id == "lrDate") {
            this.setState({
                lrDate: e.target.value,
                lrFormattedDate: e.target.value
            }, () => {
                this.lrDate()
            }
            )
        } else if (e.target.id == "mode") {
            this.setState({
                mode: e.target.value,
                vehicleNoerr: false,
                // vehicleTypeerr: false,
                // consignmentNoerr: false,
                // courierNameerr: false,
                vehicleNo: "",
                vehicleType: "",
                courierName: "",
                consignmentNo: ""
            }, () => {
                //this.mode();
            }
            )

        } else if (e.target.id == "vehicleNo") {
            let alphaNumPattern = /^[0-9a-zA-Z]+$/;
            if( e.target.value.length == 0 || (e.target.value.length <= 15 && alphaNumPattern.test(e.target.value))){
                this.setState({
                    vehicleNo: e.target.value
                }, () => {
                    this.vehicleNo();
                })
            }
        } else if (e.target.id == "policyNo") {
            this.setState({
                policyNo: e.target.value
            }
            )
        } else if (e.target.id == "declarationAmt") {
            if (e.target.validity.valid) {
                this.setState({
                    declarationAmt: e.target.value
                }, () => { this.declarationAmt() })
            }
        } else if (e.target.id == "toPay") {
            this.setState({
                toPay: e.target.value
            }, () => {
                this.toPay()
            }
            )
        } else if (e.target.id == "actualWeight") {
            this.setState({
                actualWeight: e.target.value
            }, () => {
                 this.actualWeight()
            }
            )
        } else if (e.target.id == "chargedRate") {
            if (e.target.validity.valid) {
                this.setState({
                    chargedRate: e.target.value
                }, () => this.chargedRate())
            }
        } else if (e.target.id == "chargedFreight") {
            if (e.target.validity.valid) {
                this.setState({
                    chargedFreight: e.target.value
                })
            }
        } else if (e.target.id == "permitNo") {
            this.setState({
                permitNo: e.target.value
            })
        } else if (e.target.id == "remarks") {
            this.setState({
                remarks: e.target.value
            })
        } else if (e.target.id == "vehicleType") {
            this.setState({ vehicleType: e.target.value })
        } else if (e.target.id == "consignmentNo") {
            this.setState({ consignmentNo: e.target.value })
        } else if (e.target.id == "courierName") {
            this.setState({ courierName: e.target.value })
        }

    }
    deleteLrList() {
        let lrDetailList = this.state.lrDetailList
        let checkedLrList = this.state.checkedLrList
        console.log(checkedLrList, this.props.checkedShipmentData[0].saId)
        if (checkedLrList.includes(this.props.checkedShipmentData[0].saId)) {
            this.setState({
                toastMsg: "You cannot Delete Primary ASN",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        } else {
            if (checkedLrList.length != 0) {
                if (lrDetailList.length != checkedLrList.length) {
                    for (let i = 0; i < lrDetailList.length;) {
                        if (checkedLrList.length == 0)
                            break
                        for (let j = 0; j < checkedLrList.length;) {
                            if (lrDetailList[i].saId == checkedLrList[j]) {

                                lrDetailList.splice(i, 1);
                                checkedLrList.splice(j, 1)
                                i = 0;
                                j = 0
                            }
                            else {
                                i++;
                                j++
                            }
                        }
                    }
                    this.updateLrDetails(lrDetailList)
                    this.setState({
                        lrDetailList,
                        checkedLrList
                    })
                } else {
                    this.setState({
                        toastMsg: "One row is required to create LR",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 10000);
                }
            } else {
                this.setState({
                    toastMsg: "Select atleast one to delete row",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 10000);
            }
        }
    }
    checkedLrList(saId) {
        let checkedLrList = this.state.checkedLrList
        if (checkedLrList.includes(saId)) {
            var index = checkedLrList.indexOf(saId);
            if (index > -1) {
                checkedLrList.splice(index, 1);
            }
        } else {
            checkedLrList.push(saId)
        }
        this.setState({
            checkedLrList
        })
    }

    addAdviceList() {
        let lrDetailList = this.state.lrDetailList
        let lrlist = []
        lrDetailList.map((item) => {
            return lrlist.push(item.saId)
        })
        this.setState({
            addAdviceModal: true,
            lrList: lrlist
        },()=>{
            let payload = {
                no: 1,
                type: 1,
                search: "",
                status: "SHIPMENT_SHIPPED",
                poNumber: "",
                vendorName: this.state.lrDetailList[0].vendorName,
                shipmentRequestDate: "",
                userType: "vendorlogi",
                isLRCreation: true,
                transporter: this.state.transporter, //this.state.lrDetailList[0].transporterName,
                siteDetail: this.state.lrDetailList[0].siteDetail
            }
            this.props.getAllVendorShipmentShippedRequest(payload)
        })
    }
    closeAdviceModal() {
        this.setState({
            addAdviceModal: false
        })
    }
    updateLrDetails(data) {
        console.log(data)
        var total = data.reduce((total, obj) => Number(obj.totalRequestedQty) + total, 0)
        var lrBales = data.reduce((total, obj) => Number(obj.lrBales) + total, 0)
        var declarationAmt = data.reduce((total, obj) => Number(obj.invoiceAmount) + total, 0)
        this.setState({
            lrDetailList: data,
            totalLrQty: total,
            lrBales,
            declarationAmt
        })
    }
    callHandleChange = (e) => {
        var scrollValue = handleChange(e)
        this.setState({ rangeVal: scrollValue.rangeVal, sliderDisable: scrollValue.total == 0 ? false : true })
    }

    componentDidMount() {
        let payload = { orderId: this.props.checkedShipmentData[0].orderId, shipmentId: this.props.checkedShipmentData[0].shipmentId }
        this.props.lrDetailsAutoRequest(payload)
        this.props.onRef(this)
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.logistic.lrDetailsAuto.isSuccess) {
            return {
                autoPopulateDate: nextProps.logistic.lrDetailsAuto.data.resource,
                declarationAmt: nextProps.logistic.lrDetailsAuto.data.resource.declarationAmount
            }
        }
        if (nextProps.logistic.getAllVendorTransporter.isSuccess) {
            if (nextProps.logistic.getAllVendorTransporter.data.resource != null) {
                return{
                    transportarData: nextProps.logistic.getAllVendorTransporter.data
                }
            }else{
                return{
                    transportarData : {}
                } 
            }
        }
        return null
    }

    componentDidUpdate(nextProps, prevState){
        if (nextProps.logistic.getAllVendorTransporter.isSuccess) {
            this.setState({ transporterModal : true })
        }
    }

    openTransporterModal = (e, keyCode, pageNo) => {
        if( e.which == '13' || keyCode == 'Search' || keyCode == 'page'){
            this.setState({
                 transporterModal: false,
                //transporterAnimation: true,
            }, () =>{
                let payload = {
                    no: pageNo == undefined || pageNo == null ? 1 : pageNo,
                    type: this.state.transporter == "" ? 1 : 3,
                    search: this.state.transporter,
                    slCode: this.state.consignorCode,
                    userType: "vendorlogi"
                }
                this.props.getAllVendorTransporterRequest(payload)
            })
        }     
    }

    onCloseTransporter =()=> {
        this.setState({
            transporterModal: false,
            //transporterAnimation: !this.state.transporterAnimation
        })
    }

    handleTransportarSearchText = (e) =>{
      let lrDetailList = [...this.state.lrDetailList]
      if( e.currentTarget.id == 'other')
         this.setState({ otherTransporterValue: e.target.value, },()=>this.otherTransporter())
      else   
         this.setState({ transporter: e.target.value, transporterCode : "", otherTransportFlag: false})

      lrDetailList[0].transporterName = this.state.otherTransportFlag ? e.target.value : ""    
    }

    onSelectTransportar = (e, data) =>{
        let lrDetailList = [...this.state.lrDetailList]
        if( data.transporterName == 'Other')
           this.setState({ otherTransportFlag: true, transporter: data.transporterName, transporterCode: data.transporterCode,transporterModal : false, },()=>this.transporter())
        else
           this.setState({ otherTransportFlag: false, transporterCode: data.transporterCode,
                            transporter: data.transporterName, transporterModal : false,
                            otherTransporterValue: ""},()=>this.transporter())  
                             
        lrDetailList[0].transporterName = this.state.otherTransportFlag ? "" : data.transporterName                        
    }
    // updateTransporterState = (data) => {
    //     this.setState({
    //         transporter: data.transporterName,
    //         transporterCode: data.transporterCode
    //     })
    // }

    onsubmit(e) {
        e.preventDefault();
        this.declarationAmt();
        this.transporter();
        this.otherTransporter();
        this.actualWeight();
        this.toPay();
        if (this.state.mode == "Air" || this.state.mode == "Courier" || this.state.mode == "Rail") {
            //this.consignmentNo()
            //this.courierName()
        } else if (this.state.mode == "Road") {
            this.vehicleNo()
            //this.vehicleType()
        }
        //this.chargedFreight()
        //this.etaDate()
        this.chargedRate()
        //this.mode();
        this.lrDate();
        this.lrNo();
        setTimeout(() => {
            const { lrBales, declarationAmterr, consignmentNo, logisticDate, courierName, lrDate, lrNo, transporter, transporterCode, mode,
                vehicleNo, policyNo, declarationAmt, totalLrQty, toPay, actualWeight, chargedFreight,
                chargedRate, permitNo, remarks, lrDetailList, autoPopulateDate,
                lrNoerr, lrDateerr, courierNameerr, consignmentNoerr, vehicleTypeerr, modeerr, vehicleType, transportererr, vehicleNoerr, toPayerr, actualWeighterr, otherTransporterErr, etaDateerr,chargedRateerr } = this.state
            if (!declarationAmterr && !lrNoerr && !lrDateerr && !transportererr && !toPayerr && !otherTransporterErr && !actualWeighterr && !chargedRateerr && !vehicleNoerr
                  ) {
                let lrDetailList = this.state.lrDetailList
                let lrDetailPayload = []
                for (let i = 0; i < lrDetailList.length; i++) {
                    let data = {
                        saId: lrDetailList[i].saId,
                        saTotalQty: lrDetailList[i].totalRequestedQty,
                        saTotalAmount: lrDetailList[i].basicAmount,
                        dueDays: lrDetailList[i].dueDays,
                        remarks: lrDetailList[i].remarks,
                        shipmentAdviceNo: lrDetailList[i].shipmentAdviceNo,
                        orderNo: lrDetailList[i].orderNo,
                        orderDate: lrDetailList[i].poSelectionDate + "T00:00+05:30",
                        validFromDate: lrDetailList[i].validFromSelectionDate + "T00:00+05:30",
                        validToDate: lrDetailList[i].validToSelectionDate + "T00:00+05:30",
                        shipmentAdviceDate: lrDetailList[i].shipmentSelectionDate + "T00:00+05:30",
                        orderCode: this.props.order.orderCode,
                        orderNo: this.props.order.orderNumber,
                        orderId: lrDetailList[i].orderId,
                        documentNumber: lrDetailList[i].documentNumber,
                        poType: lrDetailList[i].poType,
                    }
                    lrDetailPayload.push(data)
                }
                let payload = {
                    logisticNo: autoPopulateDate.logisticNo,
                    logisticDate: logisticDate,
                    lrNo: lrNo,
                    lrDate: lrDate + "T00:00:00+05:30",
                    transporterCode: transporterCode,
                    transporterName: this.state.otherTransportFlag ? this.state.otherTransporterValue : transporter,
                    lrPcs: totalLrQty,
                    lrBales: lrBales,
                    lrWeight: actualWeight,
                    lrType: toPay,
                    lrRate: chargedRate == "" ? 0 : chargedRate,
                    lrFreight: chargedFreight,
                    permitNo: permitNo,
                    policyNo: policyNo,
                    declarationAmount: declarationAmt,
                    consignorName: autoPopulateDate.consignor,
                    consigneeName: autoPopulateDate.consignee,
                    stationFrom: autoPopulateDate.fromLocation,
                    stationTo: autoPopulateDate.toLocation,
                    vehicleNo: vehicleNo,
                    vehicleType: vehicleType,
                    remarks: remarks,
                    transporterMode: mode,
                    lrDetailList: lrDetailPayload,
                    courier: courierName,
                    consignmentNo: consignmentNo,
                    ownerSite: autoPopulateDate.toLocation,
                    lrETADate: this.state.etaDate == "" ? "" : this.state.etaDate + "T00:00:00+05:30"
                }
                this.props.lrCreateRequest(payload)
                this.props.emptyArray();
            }
        }, 10);
    }
    handleAdviceScroll = () => {
        var table = document.getElementById("addAdviceMain")
        table.classList.toggle("scroll")
        table.classList.contains("scroll") ? document.getElementsByClassName('accordianLrCreation')[0].style.padding = "0 40px 97px 40px"
            : document.getElementsByClassName('accordianLrCreation')[0].style.padding = "0 40px 20px"
    }

    render() {
        
        const { etaDate, etaDateerr, lrBales, courierName, courierNameerr, logisticDate, lrDate, lrNo, transporter, mode, vehicleTypeerr, consignmentNo, consignmentNoerr,
            vehicleNo, policyNo, declarationAmt, toPay, actualWeight, chargedFreight, declarationAmterr,
            chargedRate, permitNo, lrDetailList, lrNoerr, lrDateerr, modeerr, vehicleType, remarks,
            transportererr, vehicleNoerr, toPayerr, actualWeighterr, autoPopulateDate, chargedFreighterr, chargedRateerr } = this.state
        return (
            <div>
                <div className="modal lrCreationModalMain" id="pocolorModel">
                    <div className="backdrop"></div>
                    <div className="modal-content modalShow create-lr-modal">
                        <div className="clrm-head">
                            <div className="clrmh-left">
                                <h3>New LR Entry</h3>
                                <div className="clrmhl-detail">
                                    <h5>Logistic Number</h5>
                                    <span className="clrmhld-info">{autoPopulateDate.logisticNo}</span>
                                </div>
                                <div className="clrmhl-detail">
                                    <h5>Today Date</h5>
                                    <span className="clrmhld-info">{logisticDate}</span>
                                </div>
                            </div>
                            <div className="clrmh-right">
                                <button type="button" className="clrmh-manageasn" onClick={(e) => this.openManageAsn(e)}>Manage ASN</button>
                                <button type="button" className="clrmh-createlr" onClick={(e) => this.onsubmit(e)}>Create LR</button>
                                <button type="button" className="clrmh-cancel" onClick={(e) => this.props.closeLrModal(e)}>Cancel</button>
                            </div>
                        </div>
                        <div className="clrm-body">
                            <div className="clrmb-oveflow">
                                <div className="clrmb-row">
                                    <h3>Logistics & Delivery Details</h3>
                                    <div className="clrmb-flex">
                                        <div className="clrmbr-inner">
                                            <label>LR/Consignment/Docket Number<span className="mandatory">*</span></label>
                                            <input type="text" className="onFocus" id="lrNo" value={lrNo} placeholder="LR No." onChange={(e) => this.handleChangeInput(e)} />
                                            {lrNoerr ? (<span className="error">Enter LR/Consignment/Docket No</span>) : null}
                                        </div>
                                        <div className="clrmbr-inner">
                                            <label>LR/Consignment/Docket Type<span className="mandatory">*</span></label>
                                            <select className="onFocus" value={toPay} id="toPay" onChange={(e) => this.handleChangeInput(e)}>
                                                <option value="">Select</option>
                                                <option value="topay"> To Pay</option>
                                                <option value="toBilled">To Be Billed</option>
                                                <option value="Paid">Paid</option>
                                            </select>
                                            {toPayerr ? (<span className="error">Select LR/Consignment/Docket Type</span>) : null}
                                        </div>
                                        <div className="clrmbr-inner">
                                            <label>Transporter/Courier Co. Name<span className="mandatory">*</span></label>
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus search-input" id="transporter" value={this.state.transporter} onChange={this.handleTransportarSearchText} onKeyPress={this.openTransporterModal}/>
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick={(e) => this.openTransporterModal(e, 'Search')}>
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                                {this.state.transporterModal ? <VendorTransporter {...this.props} {...this.state} onCloseTransporter={this.onCloseTransporter} openTransporterModal={this.openTransporterModal} transportarData={this.state.transportarData} onRef={ref => (this.child = ref)} onSelectTransportar={this.onSelectTransportar}/> : null}
                                            </div>
                                            {transportererr ? (<span className="error">Select Valid Transporter</span>) : null}
                                        </div>
                                        {this.state.otherTransportFlag && <div className="clrmbr-inner clrmbr-wdth16">
                                            <label></label>
                                            <input type="text" className="other-trans-input onFocus" id="other" value={this.state.otherTransporterValue} onChange={this.handleTransportarSearchText} placeholder={this.state.otherTransportFlag ? "Enter Other Transporter" : ""}/>
                                            {this.state.otherTransporterErr ? (<span className="error">Enter Transporter</span>) : null}
                                        </div>}
                                        <div className="clrmbr-inner dateFormat clrmbr-wdth16">
                                            <label>Date<span className="mandatory">*</span></label>
                                            <input type="date" className="onFocus" id="lrDate" value={lrDate} max={this.state.lrDetailList[0].shipmentConfirmSelectionDate} data-date={this.state.lrFormattedDate == "" ? "LR Date" : this.state.lrFormattedDate} onChange={(e) => this.handleChangeInput(e)} />
                                            {lrDateerr ? (<span className="error">Enter LR Date </span>) : null}
                                        </div>
                                        <div className="clrmbr-inner clrmbr-wdth16">
                                            <label>Estimated Time Arival(ETA)</label>
                                            <input type="date" className="onFocus" id="etaDate" min={currentDate} max={this.state.lrDetailList[0].validToSelectionDate} value={etaDate} placeholder={etaDate == "" ? "ETA Date" : etaDate} onChange={(e) => this.handleChangeInput(e)} />
                                            {/* {etaDateerr ? (<span className="error">Enter ETA</span>) : null} */}
                                        </div>
                                    </div>
                                </div>
                                <div className="clrmb-row">
                                    <h3>Item Details</h3>
                                    <div className="clrmbr-inner clrmbr-wdth16">
                                        <label>Weight (kg)</label>
                                        <input type="text" className="onFocus" value={actualWeight} placeholder="Approx Weight" id="actualWeight" onChange={(e) => this.handleChangeInput(e)} />
                                        {actualWeighterr ? (<span className="error">Enter Valid weight</span>) : null}
                                    </div>
                                    <div className="clrmbr-inner clrmbr-wdth16">
                                        <label>Rate</label>
                                        <input type="text" className="onFocus" value={chargedRate} placeholder="Charged Rate" id="chargedRate" onChange={(e) => this.handleChangeInput(e)} />
                                        {chargedRateerr ? (<span className="error"> Enter Valid Charged Rate</span>) : null}
                                    </div>
                                    <div className="clrmbr-inner clrmbr-wdth16">
                                        <label>Freight</label>
                                        <input type="text" className="onFocus" pattern="[1-9]*" value={chargedFreight} id="chargedFreight" placeholder="Charged Freight" onChange={(e) => this.handleChangeInput(e)} />
                                        {/* {chargedFreighterr ? (<span className="error"> Enter Valid Charged Freight</span>) : null} */}
                                    </div>
                                    <div className="clrmbr-inner clrmbr-wdth16">
                                        <label>Pcs</label>
                                        <input type="text" className="bgDisableColor" id="lrQty" pattern="[0-9.]*" placeholder="LR Qty" value={this.state.totalLrQty} disabled />
                                    </div>
                                    <div className="clrmbr-inner clrmbr-wdth16">
                                        <label>No. Of Cartons</label>
                                        <input type="text" className="bgDisableColor" value="" id="bales" pattern="[0-9.]*" value={lrBales} disabled />
                                    </div>
                                    <div className="clrmbr-inner clrmbr-wdth16">
                                        <label></label>
                                    </div>
                                </div>
                                <div className="clrmb-row">
                                    <h3>Policy, Courier & Transport Details</h3>
                                    <div className="clrmbr-inner">
                                        <label>Permit Number</label>
                                        <input type="text" className="onFocus" value={permitNo} placeholder="Permit No." id="permitNo" onChange={(e) => this.handleChangeInput(e)} />
                                    </div>
                                    <div className="clrmbr-inner">
                                        <label>Policy Number</label>
                                        <input type="text" className="onFocus" id="policyNo" placeholder="Policy No." value={policyNo} onChange={(e) => this.handleChangeInput(e)} />
                                    </div>
                                    <div className="clrmbr-inner clrmbr-wdth16">
                                        <label>Mode</label>
                                        <select className="onFocus" id="mode" value={mode} onChange={(e) => this.handleChangeInput(e)}>
                                            <option value="">Select Mode</option>
                                            <option value="Road">By Road</option>
                                            <option value="Air">By Air</option>
                                            <option value="Rail">By Rail</option>
                                            <option value="Courier">By Courier</option>
                                        </select>
                                        {/* {modeerr ? (<span className="error">Select Mode </span>) : null} */}
                                    </div>
                                    {mode != "" && mode == "Road" ? <div className="clrmbr-mode">
                                        <div className="clrmbr-inner">
                                            <label>Vehicle Number</label>
                                            <input type="text" className="onFocus" id="vehicleNo" placeholder="Vehicle No." value={vehicleNo !== null && vehicleNo !== "" ? vehicleNo.toUpperCase() : "" } onChange={(e) => this.handleChangeInput(e)} autoComplete="off"/>
                                            {vehicleNoerr ? (<span className="error">Min length: 6, Max length: 15</span>) : null}
                                        </div>
                                        <div className="clrmbr-inner">
                                            <label>Vehicle Type</label>
                                            <input type="text" className="onFocus" id="vehicleType" value={vehicleType} placeholder="Vehicle Type" onChange={(e) => this.handleChangeInput(e)} />
                                            {/* {vehicleTypeerr ? (<span className="error">Enter Vehicle Type</span>) : null} */}
                                        </div>
                                    </div> : mode != "" && mode != "Road" && 
                                    <div className="clrmbr-mode">
                                        <div className="clrmbr-inner">
                                            <label>Consignment No./ Docket No.</label>
                                            <input type="text" className="onFocus" id="consignmentNo" value={consignmentNo} placeholder="Consignment No./ Docket No" onChange={(e) => this.handleChangeInput(e)} />
                                            {/* {consignmentNoerr ? (<span className="error">Enter Consignment No.</span>) : null} */}
                                        </div>
                                        <div className="clrmbr-inner clrmbr-wdth16">
                                            <label>Courier Name</label>
                                            <input type="text" className="onFocus" id="courierName" value={courierName} placeholder="Courier Name" onChange={(e) => this.handleChangeInput(e)} />
                                            {/* {courierNameerr ? (<span className="error">Enter Valid Courier Name</span>) : null} */}
                                        </div>
                                    </div>}
                                </div>
                                <div className="clrmb-row">
                                    <div className="clrmbr-inner">
                                        <label>Declaration Amount<span className="mandatory">*</span></label>
                                        <input type="text" pattern="[0-9.]*" className="onFocus" value={declarationAmt} placeholder={declarationAmt == "" ? "Declaration Amount" : declarationAmt} onChange={(e) => this.handleChangeInput(e)} id="declarationAmt" />
                                        {declarationAmterr ? (<span className="error"> Enter Declaration Amount</span>) : null}
                                    </div>
                                    <div className="clrmbr-inner">
                                        <label>Consignee<span className="mandatory">*</span></label>
                                        <input type="text" className="bgDisableColor" value={autoPopulateDate.consignee} disabled />
                                    </div>
                                    <div className="clrmbr-inner">
                                        <label>Consignor<span className="mandatory">*</span></label>
                                        <input type="text" className="bgDisableColor" id="consignor" value={autoPopulateDate.consignor} disabled />
                                    </div>
                                    <div className="clrmbr-inner">
                                        <label>Station From<span className="mandatory">*</span></label>
                                        <input type="text" className="bgDisableColor" value={autoPopulateDate.fromLocation} readOnly />
                                    </div>
                                    <div className="clrmbr-inner">
                                        <label>Station To<span className="mandatory">*</span></label>
                                        <input type="text" className="bgDisableColor" id="stationTo" value={autoPopulateDate.toLocation} readOnly />
                                    </div>
                                </div>
                                <div className="clrmb-row">
                                    <div className="clrmbr-inner clrmbr-wdth50">
                                        <label>Remarks</label>
                                        <textarea className="onFocus" value={remarks} onChange={(e) => this.handleChangeInput(e)} id="remarks" placeholder={remarks == "" ? "Write here…" : remarks} rows="4" cols="65" />                                                   
                                        {/* <input type="text" className="inputBox" /> */}
                                    </div>
                                </div>
                            </div>
                            {this.state.showManageAsn && <div className="backdrop modal-backdrop-new"></div>}
                            <div className={this.state.showManageAsn === false ? "tableBottomFix" : "tableBottomFix scroll"} id="addAdviceMain">
                                <div className="tbf-head">
                                    <div className="tbfh-left">
                                        <button type="button" className="" onClick={(e) => this.deleteLrList(e)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                <path fill="#12203c" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                            </svg>
                                            <span className="generic-tooltip">Delete</span>
                                        </button>
                                        <button type="button" className="" onClick={(e) => this.addAdviceList(e)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="17.99" height="17.99" viewBox="0 0 17.99 17.99">
                                                <path fill="#8b77fa" d="M9 1.5A7.5 7.5 0 1 1 1.5 9 7.5 7.5 0 0 1 9 1.5zM9 0a9 9 0 1 0 9 9 9 9 0 0 0-9-9zm4.5 9.745H9.745v3.748h-1.5V9.745H4.5v-1.5h3.746V4.5h1.5v3.746h3.748z"/>
                                            </svg>
                                            <span className="generic-tooltip">Add ASN</span>
                                        </button>
                                    </div>
                                    <div className="tbfh-right">
                                        <button type="button" className="" onClick={(e) => this.closeManageAsn(e)}><img src={require('../../../../assets/clearSearch.svg')} /></button>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="vendor-gen-table">
                                        <div className="manage-table">
                                            <table className="table gen-main-table">
                                                <thead>
                                                    <tr>
                                                        <th className="fix-action-btn"><label> </label></th>
                                                        <th><label>ASN No.</label></th>
                                                        <th><label>ASN Date</label></th>
                                                        <th><label>PO Number</label></th>
                                                        <th><label>PO Date</label> </th>
                                                        <th><label>Shipment Confirm Date</label> </th>
                                                        <th><label>Ship By</label> </th>
                                                        <th><label>Vendor Name</label></th>
                                                        <th><label>Delivery Site</label></th>
                                                        <th><label>Valid From</label></th>
                                                        <th><label>Valid To</label> </th>
                                                        <th><label>Due Days</label></th>
                                                        <th> <label>Total Qty</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {lrDetailList.map((data, key) => (
                                                        <tr key={key}>
                                                            <td className="fix-action-btn">
                                                                <ul className="table-item-list">
                                                                    <li className="til-inner">
                                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.checkedLrList.includes(data.saId)} onChange={(e) => this.checkedLrList(data.saId)} /><span className="checkmark1"></span> </label>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                            <td><label className="width120">{data.shipmentAdviceNo}</label> </td>
                                                            <td><label>{data.shipmentAdviceDate}</label> </td>
                                                            <td> <label className="width120">{data.orderNumber}</label></td>
                                                            <td> <label>{data.orderDate}</label></td>
                                                            <td> <label>{data.shipmentConfirmDate}</label></td>
                                                            <td> <label className="minWid100">{data.transporterName}</label></td>
                                                            <td> <label>{data.vendorName}</label></td>
                                                            <td> <label>{data.siteDetail}</label></td>
                                                            <td><label >{data.validFromSelectionDate}</label> </td>
                                                            <td><label>{data.validToSelectionDate}</label> </td>
                                                            <td> <label>{data.dueDays}</label></td>
                                                            <td> <label>{data.totalRequestedQty}</label></td>
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*<div className="col-md-12 pad-0">
                        <div className="col-md-6">
                            <div className="slideContainer tableScroller m-top-30">
                                <input type="range" onInput={this.callHandleChange} onChange={this.callHandleChange} data-tabwid="tableWid" data-scrwid="scrollDiv" data-scrolldec="scrollDec" min="1" max="50" value={sliderDisable ? this.state.rangeVal : 0} className="sliderTable" id="myRange" />
                            </div>
                        </div>
                    </div>*/}
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {/* {this.state.transporterModal ? <TransporterModal {...this.props} {...this.state} slCode={this.state.consignorCode} transporter={this.state.transporterCode} onCloseTransporter={(e) => this.onCloseTransporter(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} /> : null} */}
                {this.state.addAdviceModal ? <AddShipmentAdviceModal {...this.props} {...this.state} updateLrDetails={(e) => this.updateLrDetails(e)} lrList={this.state.lrList} lrDetailList={this.state.lrDetailList} closeAdviceModal={(e) => this.closeAdviceModal(e)} /> : null}
            </div>
        )
    }
}
