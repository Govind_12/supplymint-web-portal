import React, { Component } from 'react'
import Pagination from '../../../pagination';
import refreshIcon from '../../../../assets/refresh-block.svg';
import { handleChangeModal } from '../../../../helper/index';
import pendingqc from '../../../../assets/pending-qc.svg';
import linkedIcon from '../../../../assets/linked-document.svg';

import ToastLoader from '../../../loaders/toastLoader';

export default class AddShipmentAdviceModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toastLoader: false,
            toastMsg: "",
            sliderDisable: false,
            rangeVal: 1,
            type: 1,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            no: 1,
            completeQcModal: false,
            adviceData: [],
            search: "",
            checkedAdvice: this.props.lrList,
            lrDetailList: this.props.lrDetailList
        }
    }
    componentWillMount() {
        this.setState({
            checkedAdvice: this.props.lrList,
            lrDetailList: this.props.lrDetailList
        })
    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.logistic.getAllVendorShipmentShipped.isSuccess) {
            if (nextProps.logistic.getAllVendorShipmentShipped.data.resource != null) {
                this.setState({
                    adviceData: nextProps.logistic.getAllVendorShipmentShipped.data.resource,
                    prev: nextProps.logistic.getAllVendorShipmentShipped.data.prePage,
                    current: nextProps.logistic.getAllVendorShipmentShipped.data.currPage,
                    next: nextProps.logistic.getAllVendorShipmentShipped.data.currPage + 1,
                    maxPage: nextProps.logistic.getAllVendorShipmentShipped.data.maxPage,
                })
            } else {
                this.setState({
                    adviceData: [],
                    prev: "",
                    current: 0,
                    next: "",
                    maxPage: 0,
                })
            }
        }
    }

    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {

                this.setState({
                    prev: this.props.logistic.getAllVendorShipmentShipped.data.prePage,
                    current: this.props.logistic.getAllVendorShipmentShipped.data.currPage,
                    next: this.props.logistic.getAllVendorShipmentShipped.data.currPage + 1,
                    maxPage: this.props.logistic.getAllVendorShipmentShipped.data.maxPage,
                })
                if (this.props.logistic.getAllVendorShipmentShipped.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.logistic.getAllVendorShipmentShipped.data.currPage - 1,
                        search: this.state.search,
                        status: "SHIPMENT_SHIPPED",
                        poNumber: "",
                        vendorName: this.state.lrDetailList[0].vendorName,
                        shipmentRequestDate: "",
                        userType: "vendorlogi",
                        isLRCreation: true,
                        transporter: this.state.lrDetailList[0].transporterName,
                        siteDetail: this.state.lrDetailList[0].siteDetail

                    }
                    this.props.getAllVendorShipmentShippedRequest(data)
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.logistic.getAllVendorShipmentShipped.data.prePage,
                current: this.props.logistic.getAllVendorShipmentShipped.data.currPage,
                next: this.props.logistic.getAllVendorShipmentShipped.data.currPage + 1,
                maxPage: this.props.logistic.getAllVendorShipmentShipped.data.maxPage,
            })
            if (this.props.logistic.getAllVendorShipmentShipped.data.currPage != this.props.logistic.getAllVendorShipmentShipped.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.logistic.getAllVendorShipmentShipped.data.currPage + 1,
                    search: this.state.search,
                    status: "SHIPMENT_SHIPPED",
                    poNumber: "",
                    vendorName: this.state.lrDetailList[0].vendorName,
                    shipmentRequestDate: "",
                    userType: "vendorlogi",
                    isLRCreation: true,
                    transporter: this.state.lrDetailList[0].transporterName,
                    siteDetail: this.state.lrDetailList[0].siteDetail
                }
                this.props.getAllVendorShipmentShippedRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            }
            else {
                this.setState({
                    prev: this.props.logistic.getAllVendorShipmentShipped.data.prePage,
                    current: this.props.logistic.getAllVendorShipmentShipped.data.currPage,
                    next: this.props.logistic.getAllVendorShipmentShipped.data.currPage + 1,
                    maxPage: this.props.logistic.getAllVendorShipmentShipped.data.maxPage,
                })
                if (this.props.logistic.getAllVendorShipmentShipped.data.currPage <= this.props.logistic.getAllVendorShipmentShipped.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        search: this.state.search,
                        status: "SHIPMENT_SHIPPED",
                        poNumber: "",
                        vendorName: this.state.lrDetailList[0].vendorName,
                        shipmentRequestDate: "",
                        userType: "vendorlogi",
                        isLRCreation: true,
                        transporter: this.state.lrDetailList[0].transporterName,
                        siteDetail: this.state.lrDetailList[0].siteDetail
                    }
                    this.props.getAllVendorShipmentShippedRequest(data)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.logistic.getAllVendorShipmentShipped.data.prePage,
                    current: this.props.logistic.getAllVendorShipmentShipped.data.currPage,
                    next: this.props.logistic.getAllVendorShipmentShipped.data.currPage + 1,
                    maxPage: this.props.logistic.getAllVendorShipmentShipped.data.maxPage,
                })
                if (this.props.logistic.getAllVendorShipmentShipped.data.currPage <= this.props.logistic.getAllVendorShipmentShipped.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.logistic.getAllVendorShipmentShipped.data.maxPage,
                        search: this.state.search,
                        status: "SHIPMENT_SHIPPED",
                        poNumber: "",
                        vendorName: this.state.lrDetailList[0].vendorName,
                        shipmentRequestDate: "",
                        userType: "vendorlogi",
                        isLRCreation: true,
                        transporter: this.state.lrDetailList[0].transporterName,
                        siteDetail: this.state.lrDetailList[0].siteDetail
                    }
                    this.props.getAllVendorShipmentShippedRequest(data)
                }
            }
        }
    }


    searchClear = () => {
        if (this.state.type === 3) {
            this.setState({ search: "", type: 1 })
            let payload = {
                no: 1,
                type: 1,
                search: "",
                status: "SHIPMENT_SHIPPED",
                poNumber: "",
                vendorName: this.state.lrDetailList[0].vendorName,
                shipmentRequestDate: "",
                userType: "vendorlogi",
                isLRCreation: true,
                transporter: this.state.lrDetailList[0].transporterName,
                siteDetail: this.state.lrDetailList[0].siteDetail
            }
            this.props.getAllVendorShipmentShippedRequest(payload)
        } else {
            this.setState({ search: "" })
        }
    }
    onSearch = (e) => {
        this.setState({
            search: e.target.value,
        })
        if (e.keyCode == 13) {
            this.setState({ type: 3 })
            let payload = {
                no: 1,
                type: 3,
                search: e.target.value,
                status: "SHIPMENT_SHIPPED",
                poNumber: "",
                vendorName: this.state.lrDetailList[0].vendorName,
                shipmentRequestDate: "",
                userType: "vendorlogi",
                isLRCreation: true,
                transporter: this.state.lrDetailList[0].transporterName,
                siteDetail: this.state.lrDetailList[0].siteDetail
            }
            this.props.getAllVendorShipmentShippedRequest(payload)
        }
    }
    onRefresh = () => {
        let payload = {
            no: 1,
            type: 1,
            search: "",
            status: "SHIPMENT_SHIPPED",
            poNumber: "",
            vendorName: this.state.lrDetailList[0].vendorName,
            shipmentRequestDate: "",
            userType: "vendorlogi",
            isLRCreation: true,
            transporter: this.state.lrDetailList[0].transporterName,
            siteDetail: this.state.lrDetailList[0].siteDetail
        }
        this.props.getAllVendorShipmentShippedRequest(payload)
    }
    callHandleChangeModal = (e) => {
        var scrollValue = handleChangeModal(e)
        this.setState({ rangeVal: scrollValue.rangeVal, sliderDisable: scrollValue.total == 0 ? false : true })
    }
    checkedShipmentFun(id) {
        let checkedAdvice = [...this.state.checkedAdvice]
        let lrDetailList = [...this.state.lrDetailList]
        if (checkedAdvice.includes(id)) {
            lrDetailList.forEach((item) => {
                if (item.saId == id) {
                    lrDetailList.splice(item, 1)
                }
            })
            var index = checkedAdvice.indexOf(id);
            if (index > -1) {
                checkedAdvice.splice(index, 1);
            }
        } else {
            checkedAdvice.push(id)
            let adviceData = [...this.state.adviceData]            
            adviceData.forEach((item) => {
                if (item.shipmentId == id) {              
                    let data = {
                        saId: id,
                        totalRequestedQty: item.totalRequestedQty,
                        dueDays: item.dueInDays,
                        remarks: "",
                        lrBales : item.unitCount,
                        shipmentAdviceNo: item.shipmentAdviceCode,
                        shipmentAdviceDate: item.shipmentDate,
                        shipmentConfirmDate: item.shipmentConfirmDate,
                        orderNumber: item.orderNumber,
                        orderDate: item.poDate,
                        validFrom: item.validFrom,
                        validTo: item.validTo,
                        siteDetail: item.siteDetails,
                        transporterId: item.transporterId,
                        transporterName: item.transporterName,
                        vendorCode: item.vendorCode,
                        vendorName: item.vendorName,
                        ownerSite:item.siteDetails,
                        saTotalAmount:item.saTotalAmount!= undefined?item.saTotalAmount:"",
                        poSelectionDate: item.poSelectionDate ,
                        validFromSelectionDate: item.validFromSelectionDate,
                        validToSelectionDate:item.validToSelectionDate,
                        shipmentSelectionDate: item.shipmentSelectionDate ,
                        poType : item.poType,
                        orderId :item.orderId,
                        documentNumber : item.documentNumber,
                        invoiceAmount : item.invoiceAmount
                    }    
                    console.log(data)            
                    lrDetailList.push(data)
                }
            })
        }     
        console.log(lrDetailList)
        this.setState({
            checkedAdvice,
            lrDetailList
        })
    }

    onDone() {
        if (this.state.lrDetailList.length != 0) {
          
            this.props.updateLrDetails(this.state.lrDetailList)
            this.props.closeAdviceModal()
        } else {
            this.setState({
                toastMsg: "You have to select atleast one",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        }
    }
    render() {  
        const { sliderDisable } = this.state
        return (
            <div>
                <div className="modal" id="pocolorModel">
                    <div className="backdrop modal-backdrop-new"></div>
                    <div className="modal-content modalShow createShipmentModal add-shipment-modal">
                        <div className="asm-head">
                            <div className="asmh-left">
                                <h3>Add Shipment ASN</h3>
                            </div>
                            <div className="asmh-right">
                                <button type="button" className="asmh-done" onClick={(e) => this.onDone(e)}>Done</button>
                                <button type="button" className="" onClick={(e) => this.props.closeAdviceModal(e)}>Cancel</button>
                            </div>
                        </div>
                        <div className="col-md-12 p-lr-30">
                            <div className="vendor-gen-table">
                                <div className="manage-table">
                                            {/* _________________________Enterprise Table ____________________________ */}
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                <th className="fix-action-btn">
                                                    <ul className="rab-refresh">
                                                        <li className="rab-rinner">
                                                            <span><img src={require('../../../../assets/refresh-block.svg')} onClick={this.onRefresh} /></span>
                                                        </li>
                                                    </ul>
                                                </th>
                                                {/* <th><label>ASN Id</label></th> */}
                                                <th><label>ASN No.</label></th>
                                                <th><label>ASN Date</label></th>
                                                <th><label className="min-width185 widthAuto">PO Number</label></th>
                                                <th><label>PO Date</label> </th>
                                                <th><label>Shipment Confirm Date</label> </th>
                                                <th><label>Ship By</label> </th>
                                                <th><label>Vendor Name</label></th>
                                                <th><label>Delivery Site</label></th>
                                                <th><label>Valid From</label></th>
                                                <th><label>Valid To</label> </th>
                                                <th><label>Due Days</label></th>
                                                <th> <label>Total Qty</label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.adviceData.length != 0 ? this.state.adviceData.map((data, key) => (
                                                <tr key={key}>
                                                    <td className="fix-action-btn">
                                                        <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.checkedAdvice.includes(data.shipmentId)} onChange={(e) => this.checkedShipmentFun(data.shipmentId)} /><span className="checkmark1"></span> </label>
                                                            </li>
                                                        </ul>
                                                        {/* <img src={linkedIcon} className="height16 m-lft-15 displayPointer" /> */}
                                                    </td>
                                                    {/* <td><label className="colorTableText fontBold">{data.shipmentId}</label></td> */}
                                                    <td> <label className="whiteUnset">{data.shipmentAdviceCode}</label></td>
                                                    <td><label className="colorTableText fontBold">{data.shipmentDate}</label></td>
                                                    <td><label className={data.poStatus == "PARTIAL" ? "colorTableText textWarning fontBold" : "colorTableText fontBold"}>{data.orderNumber}</label>{data.poStatus=="PARTIAL"?<span className="partialTable labelEvent">{data.poStatus}</span>:null}  </td>
                                                    <td> <label>{data.poDate}</label></td>
                                                    <td> <label>{data.shipmentConfirmDate}</label></td>
                                                    <td> <label> {data.transporterName}</label></td>
                                                    <td> <label className="whiteUnset">{data.vendorName}</label></td>
                                                    <td> <label>{data.siteDetails}</label></td>
                                                    <td> <label>{data.validFrom}</label></td>
                                                    <td> <label>{data.validTo}</label></td>
                                                    <td> <label> {data.dueInDays}</label></td>
                                                    <td><label className="colorTableText fontBold">{data.requestedQty}</label></td>
                                                </tr>
                                            )) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}

                                        </tbody>
                                    </table>
                                    {/* Enterprise Table End */}
                                </div>
                                <div className="col-md-12 pad-0">
                                    <div className="new-gen-pagination">
                                        <div className="ngp-left">
                                        {/* <div className="slideContainer tableScroller m-top-30">
                                                <input type="range" onInput={this.callHandleChangeModal} onChange={this.callHandleChangeModal} data-tabwid="tableWid" data-scrwid="scrollDiv" data-scrolldec="scrollDec" min="1" max="50" value={sliderDisable ? this.state.rangeVal : 0} className="sliderTable" id="myRange" />
                                            </div> */}
                                        </div>
                                        <div className="ngp-right">
                                            <div className="nt-btn">
                                                <Pagination {...this.state} {...this.props} page={this.page}
                                                  prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
