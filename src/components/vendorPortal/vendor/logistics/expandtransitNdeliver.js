import React, { Component } from 'react';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';
import plusIcon from '../../../../assets/plus-blue.svg';
import removeIcon from '../../../../assets/removeIcon.svg';
import searchIcon from '../../../../assets/close-recently.svg';
import AsnLevelConfigHeader from '../../asnLevelHeaderConfig';
import AsnLevelConfirmationModal from '../../asnLevelConfirmationModal';
import SetLevelProcessedExpand from '../setLevelProcessedExpand';
import ExpandModal from './expandModal';
import ItemLevelGoodsAndTransit from '../../modals/itemLevelGoodsAndTransit';
import ItemLevelDetails from '../../modals/itemLevelDetails';
export default class ExpandTransitNDeliver extends Component {
    constructor(props) {
        super(props);
        this.state = {
            poDetailsShipment: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            toastLoader: false,
            toastMsg: "",
            asnBaseExpandDetails: [],
            asnRequestedQty: "",
            asnBasedArray: [],
            asnLvlGetHeaderConfig: [],
            asnLvlFixedHeader: [],
            asnLvlCustomHeaders: {},
            asnLvlHeaderConfigState: {},
            asnLvlHeaderConfigDataState: {},
            asnLvlFixedHeaderData: [],
            asnLvlCustomHeadersState: [],
            asnLvlHeaderState: {},
            asnLvlHeaderSummary: [],
            asnLvlDefaultHeaderMap: [],
            asnLvlConfirmModal: false,
            asnLvlHeaderMsg: "",
            asnLvlParaMsg: "",
            asnLvlHeaderCondition: false,
            asnLvlSaveState: [],
            asnLvlColoumSetting: false,
            confirmModalHeader: false,
            mandateHeaderAsn: [],
            itemBaseArray:[]
        }
    }
    componentDidMount() {
        /* let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.asn_Display,
            basedOn: "ASN"
        }
        this.props.getHeaderConfigRequest(payload) */
        if (!this.props.replenishment.getSetHeaderConfig.isSuccess) {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.asn_Display,
                basedOn: "ASN"
            }
            this.props.getSetHeaderConfigRequest(payload)
        }
    }
    /* componentWillReceiveProps(nextProps) {
        if (nextProps.logistic.getAllVendorSiDetail.isSuccess) {
            if (nextProps.logistic.getAllVendorSiDetail.data.resource != null) {
                this.setState({
                    poDetailsShipment: nextProps.logistic.getAllVendorSiDetail.data.resource,
                    prev: nextProps.logistic.getAllVendorSiDetail.data.prePage,
                    current: nextProps.logistic.getAllVendorSiDetail.data.currPage,
                    next: nextProps.logistic.getAllVendorSiDetail.data.currPage + 1,
                    maxPage: nextProps.logistic.getAllVendorSiDetail.data.maxPage,
                })
            } else {
                this.setState({
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "ASN") {
                let asnLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let asnLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let asnLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                this.setState({
                    asnLvlCustomHeaders: this.state.asnLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    asnLvlGetHeaderConfig,
                    asnLvlFixedHeader,
                    asnLvlCustomHeadersState,
                    asnLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    asnLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    asnLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    asnLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    asnLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderAsn: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                })
            }
        }
    } */
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "ASN") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.asn_Display,
                basedOn: "ASN"
            }
            this.props.getSetHeaderConfigRequest(payload)
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.logistic.getAllVendorSiDetail.isSuccess) {
            if (nextProps.logistic.getAllVendorSiDetail.data.resource != null) {
                return {
                    poDetailsShipment: nextProps.logistic.getAllVendorSiDetail.data.resource,
                    prev: nextProps.logistic.getAllVendorSiDetail.data.prePage,
                    current: nextProps.logistic.getAllVendorSiDetail.data.currPage,
                    next: nextProps.logistic.getAllVendorSiDetail.data.currPage + 1,
                    maxPage: nextProps.logistic.getAllVendorSiDetail.data.maxPage,
                }
            } else {
                return {
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                }
            }
        }
        if (nextProps.logistic.getAllVendorSsdetail.data.resource != null && nextProps.logistic.getAllVendorSsdetail.data.basedOn == "Item") {
            return {
                itemBaseArray: nextProps.logistic.getAllVendorSsdetail.data.resource.poDetails
            }
        }
        return {}
    }
    searchShipment = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                if (this.state.search == "") {
                    this.setState({
                        toastMsg: "select data",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 10000)
                } else {
                    this.setState({ type: 3 })
                    let payload = {
                        no: 1,
                        type: 3,
                        userType: this.props.userType,
                        search: this.state.search,
                        lgtNumber: this.props.expandChecked[0].logisticNo,
                    }
                    this.props.getAllVendorSiDetailRequest(payload)
                }
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.type == 3 && this.state.search == "") {
                let data = {
                    no: 1,
                    type: 1,
                    search: "",
                    userType: this.props.userType,
                    lgtNumber: this.props.expandChecked[0].logisticNo,

                }
                this.props.getAllVendorSiDetailRequest(data)
                this.setState({ type: 1 })
            }
        })
    }
    searchClear = () => {
        if (this.state.type == 3) {
            let data = {
                no: 1,
                type: 1,
                search: "",
                userType: this.props.userType,
                lgtNumber: this.props.expandChecked[0].logisticNo,

            }
            this.props.getAllVendorSiDetailRequest(data)
            this.setState({ type: 1, search: "" })
        } else {
            this.setState({ search: "" })
        }
    }

    //DYNAMIC HEADER
    asnLvlOpenColoumSetting(data) {
        if (this.state.asnLvlCustomHeadersState.length == 0) {
            this.setState({
                asnLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                asnLvlColoumSetting: true
            })
        } else {
            this.setState({
                asnLvlColoumSetting: false
            })
        }
    }
    asnLvlpushColumnData(data) {
        let asnLvlGetHeaderConfig = this.state.asnLvlGetHeaderConfig
        let asnLvlCustomHeadersState = this.state.asnLvlCustomHeadersState
        let asnLvlHeaderConfigDataState = this.state.asnLvlHeaderConfigDataState
        let asnLvlCustomHeaders = this.state.asnLvlCustomHeaders
        let asnLvlSaveState = this.state.asnLvlSaveState
        let asnLvlDefaultHeaderMap = this.state.asnLvlDefaultHeaderMap
        let asnLvlHeaderSummary = this.state.asnLvlHeaderSummary
        let asnLvlFixedHeaderData = this.state.asnLvlFixedHeaderData
        if (this.state.asnLvlHeaderCondition) {
            if (!data.includes(asnLvlGetHeaderConfig) || asnLvlGetHeaderConfig.length == 0) {
                asnLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(asnLvlHeaderConfigDataState))) {
                    let invert = _.invert(asnLvlFixedHeaderData)
                    let keyget = invert[data];
                    Object.assign(asnLvlCustomHeaders, { [keyget]: data })
                    asnLvlSaveState.push(keyget)
                }
                if (!Object.keys(asnLvlCustomHeaders).includes(asnLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(asnLvlFixedHeaderData))[data];
                    asnLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(asnLvlCustomHeadersState) || asnLvlCustomHeadersState.length == 0) {
                asnLvlCustomHeadersState.push(data)
                if (!asnLvlCustomHeadersState.includes(asnLvlHeaderConfigDataState)) {
                    let keyget = (_.invert(asnLvlFixedHeaderData))[data];
                    Object.assign(asnLvlCustomHeaders, { [keyget]: data })
                    asnLvlSaveState.push(keyget)
                }
                if (!Object.keys(asnLvlCustomHeaders).includes(asnLvlHeaderSummary)) {
                    let keygetvalue = (_.invert(asnLvlFixedHeaderData))[data];
                    asnLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            asnLvlGetHeaderConfig,
            asnLvlCustomHeadersState,
            asnLvlCustomHeaders,
            asnLvlSaveState,
            asnLvlDefaultHeaderMap,
            asnLvlHeaderSummary
        })
    }
    asnLvlCloseColumn(data) {
        let asnLvlGetHeaderConfig = this.state.asnLvlGetHeaderConfig
        let asnLvlHeaderConfigState = this.state.asnLvlHeaderConfigState
        let asnLvlCustomHeaders = []
        let asnLvlCustomHeadersState = this.state.asnLvlCustomHeadersState
        let asnLvlFixedHeaderData = this.state.asnLvlFixedHeaderData
        if (!this.state.asnLvlHeaderCondition) {
            for (let j = 0; j < asnLvlCustomHeadersState.length; j++) {
                if (data == asnLvlCustomHeadersState[j]) {
                    asnLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in asnLvlFixedHeaderData) {
                if (!asnLvlCustomHeadersState.includes(asnLvlFixedHeaderData[key])) {
                    asnLvlCustomHeaders.push(key)
                }
            }
            if (this.state.asnLvlCustomHeadersState.length == 0) {
                this.setState({
                    asnLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < asnLvlGetHeaderConfig.length; i++) {
                if (data == asnLvlGetHeaderConfig[i]) {
                    asnLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in asnLvlFixedHeaderData) {
                if (!asnLvlGetHeaderConfig.includes(asnLvlFixedHeaderData[key])) {
                    asnLvlCustomHeaders.push(key)
                }
            }
        }
        asnLvlCustomHeaders.forEach(e => delete asnLvlHeaderConfigState[e]);
        this.setState({
            asnLvlGetHeaderConfig,
            asnLvlCustomHeaders: asnLvlHeaderConfigState,
            asnLvlCustomHeadersState,
        })
        setTimeout(() => {     // minValue,
            // maxValue
            let keygetvalue = (_.invert(this.state.asnLvlFixedHeaderData))[data];     // minValue,
            // maxValue
            let asnLvlSaveState = this.state.asnLvlSaveState
            asnLvlSaveState.push(keygetvalue)
            let asnLvlHeaderSummary = this.state.asnLvlHeaderSummary
            let asnLvlDefaultHeaderMap = this.state.asnLvlDefaultHeaderMap
            if (!this.state.asnLvlHeaderCondition) {
                for (let j = 0; j < asnLvlHeaderSummary.length; j++) {
                    if (keygetvalue == asnLvlHeaderSummary[j]) {
                        asnLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < asnLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == asnLvlDefaultHeaderMap[i]) {
                        asnLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                asnLvlHeaderSummary,
                asnLvlDefaultHeaderMap,
                asnLvlSaveState
            })
        }, 100);
    }
    asnLvlsaveColumnSetting(e) {
        this.setState({
            asnLvlColoumSetting: false,
            asnLvlHeaderCondition: false,
            asnLvlSaveState: []
        })
        let payload = {
            basedOn: "ASN",
            module: "SHIPMENT TRACKING",
            subModule: "LOGISTIC",
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.asn_Display,
            fixedHeaders: this.state.asnLvlFixedHeaderData,
            defaultHeaders: this.state.asnLvlHeaderConfigDataState,
            customHeaders: this.state.asnLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    asnLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            asnLvlConfirmModal: true,
        })
    }
    asnLvlResetColumn() {
        let payload = {
            basedOn: "ASN",
            module: "SHIPMENT TRACKING",
            subModule: "LOGISTIC",
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.asn_Display,
            fixedHeaders: this.state.asnLvlFixedHeaderData,
            defaultHeaders: this.state.asnLvlHeaderConfigDataState,
            customHeaders: this.state.asnLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            asnLvlHeaderCondition: true,
            asnLvlColoumSetting: false,
            asnLvlSaveState: []
        })
    }
    // ------------------

    asnLvlCloseConfirmModal(e) {
        this.setState({
            asnLvlConfirmModal: !this.state.asnLvlConfirmModal,
        })
    }

    expandColumn(id, e, data) {
        var img = e.target
        if (!this.state.actionExpand || this.state.prevId !== id) {
            let payload = {
                userType: this.props.userType,
                orderId: data.orderId,
                setHeaderId: data.setHeaderId,
                detailType: data.poType == "poicode" ? "item" : "set",
                poType: data.poType,
                shipmentId: data.id,
                basedOn: "set",
            }
            this.props.getAllVendorSsdetailRequest(payload);
            this.setState({ poType: data.poType, actionExpand: true, prevId: id, expandedId: id, dropOpen: true })
        } else {
            this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
        }
    }
    status = (data) => {
        return (
            <div className="poStatus"> {data.poStatus == "PARTIAL" ? <span className="partialTable partiaTag labelEvent">P</span> : ""}
                {data.poType == "poicode" ? <span className="partialTable setTag labelEvent">N</span>
                    : <span className="partialTable setTag labelEvent">S</span>}
                <div className="poStatusToolTip">
                    {data.poStatus == "PARTIAL" ? <div className="each-tag"><span className="partialTable partiaTag labelEvent">P</span><label>Partial</label></div> : ""}
                    {data.poType == "poicode" ? <div className="each-tag"><span className="partialTable setTag labelEvent">N</span><label>Non-set</label></div> : <div className="each-tag"><span className="partialTable setTag labelEvent">S</span><label>Set</label></div>}
                </div>
            </div>
        )
    }
    render() {
        return (
            <div className="gen-vend-sticky-table">
                <div className="gvst-expend" id="expandedTable">
                    {/* -----------------------------------------------------------expanded Table----------------------------- */}
                   {/*  <AsnLevelConfigHeader {...this.props} {...this.state} asnLvlColoumSetting={this.state.asnLvlColoumSetting} asnLvlGetHeaderConfig={this.state.asnLvlGetHeaderConfig} asnLvlResetColumnConfirmation={(e) => this.asnLvlResetColumnConfirmation(e)} asnLvlOpenColoumSetting={(e) => this.asnLvlOpenColoumSetting(e)} asnLvlCloseColumn={(e) => this.asnLvlCloseColumn(e)} asnLvlpushColumnData={(e) => this.asnLvlpushColumnData(e)} asnLvlsaveColumnSetting={(e) => this.asnLvlsaveColumnSetting(e)} />
                    */} 
                    <div className="gvst-inner">
                        <table className="table">
                            <thead>
                                <React.Fragment>
                                    <tr>
                                        <th className="fix-action-btn"><label></label></th>
                                        {/* {this.state.asnLvlCustomHeadersState.length == 0 ? this.state.asnLvlGetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.state.asnLvlCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))} */}
                                        {this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            )) : this.props.setCustomHeadersState.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            ))}
                                    </tr>
                                </React.Fragment>
                            </thead>
                            <tbody>
                                {this.state.poDetailsShipment.length != 0 ?
                                    this.state.poDetailsShipment.map((data, key) => (
                                        <React.Fragment key={key}>
                                            <tr>
                                                <td className="fix-action-btn">
                                                    <ul className="table-item-list">
                                                        <li className="til-inner" id={data.id} onClick={(e) => this.expandColumn(data.id, e, data)}>
                                                        {this.state.dropOpen && this.state.expandedId == data.id ? 
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                            <path fill="#a4b9dd" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"/>
                                                        </svg> : 
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                            <path fill="#a4b9dd" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                                        </svg>}
                                                        </li>
                                                    </ul>
                                                </td>
                                                {/* {this.state.asnLvlHeaderSummary.length == 0 ? this.state.asnLvlDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        {hdata == "poNumber" ? <React.Fragment><label className="fontWeig600  whiteUnset wordBreakInherit">{data["poNumber"]} </label>{this.status(data)}</React.Fragment>
                                                            : <label onClick={() => (hdata == "shipmentAdviceCode" && this.props.shipmentTrackingMethod(data))}>{data[hdata]}</label>}
                                                    </td>
                                                )) : this.state.asnLvlHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        {sdata == "poNumber" ? <React.Fragment><label className="table-td-text fontWeig600  whiteUnset wordBreakInherit">{data["poNumber"]} </label>{this.status(data)}</React.Fragment>
                                                            : <label onClick={() => (sdata == "shipmentAdviceCode" && this.props.shipmentTrackingMethod(data))}>{data[sdata]}</label>}
                                                    </td>
                                                ))} */}
                                                {this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                                        <td key={key} >
                                                        {hdata == "poNumber" ? <React.Fragment><label className="fontWeig600  whiteUnset wordBreakInherit">{data["poNumber"]} </label>{this.status(data)}</React.Fragment>
                                                            : <label onClick={() => (hdata == "shipmentAdviceCode" && this.props.shipmentTrackingMethod(data))}>{data[hdata]}</label>}
                                                    </td>
                                                    )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                                        <td key={keyy} >
                                                        {sdata == "poNumber" ? <React.Fragment><label className="table-td-text fontWeig600  whiteUnset wordBreakInherit">{data["poNumber"]} </label>{this.status(data)}</React.Fragment>
                                                            : <label onClick={() => (sdata == "shipmentAdviceCode" && this.props.shipmentTrackingMethod(data))}>{data[sdata]}</label>}
                                                    </td>
                                                    ))}  
                                            </tr>
                                            {/* {this.state.dropOpen && this.state.expandedId == data.logisticNo ? <tr><td colSpan="100%" className="pad-0"> <SetLevelProcessedExpand {...this.state}  {...this.props} /></td></tr> : null} */}
                                            {this.state.dropOpen && this.state.expandedId == data.id && this.state.actionExpand && <tr><td colSpan="100%" className="expandTd pad-0"><ItemLevelGoodsAndTransit {...this.state} {...this.props} set_Display="VENDOR_LR_INTRANSIT_SET" item_Display="VENDOR_LR_INTRANSIT_ITEM" /></td></tr>}
                                        </React.Fragment>
                                    ))
                                    : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                            </tbody>
                        </table>
                        {/* <div className="pagerDiv newPagination text-right justifyEnd alignMiddle displayFlex pad-top-20 expandPagination">
                            <Pagination {...this.props} {...this.state} page={this.page} />
                        </div> */}
                    </div>
                    {this.state.asnLvlConfirmModal ? <AsnLevelConfirmationModal {...this.state} {...this.props} asnLvlCloseConfirmModal={(e) => this.asnLvlCloseConfirmModal(e)} asnLvlResetColumn={(e) => this.asnLvlResetColumn(e)} /> : null}
                    {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
                    {/* -----------------------------------------------------------expanded Table End----------------------------- */}
                </div>
            </div>
        )
    }
}