import React from "react";

class FilterShipmentShipped extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 1,
            no: 1,
            shipmentRequestDate: this.props.shipmentRequestDate,
            poNumber: this.props.poNumber,
            status: this.props.status,
            vendorName: this.props.vendorName,
            transporter: this.props.transporter,
            siteDetail: this.props.siteDetail,
            adviceNo: this.props.adviceNo,
            adviceDate: this.props.adviceDate,
            poDate: this.props.poDate,
            shipmentConfirmDate: this.props.shipmentConfirmDate,
            totalQty: this.props.totalQty,
            dueDate: this.props.dueDate,
            count: this.props.filterCount,
            userType: this.props.userType,
            orderCode: this.props.orderCode,
            qcDate: this.props.qcDate
        };
    }


    componentWillMount() {
        this.setState({
            shipmentRequestDate: this.props.shipmentRequestDate,
            poNumber: this.props.poNumber,
            status: this.props.status,
            vendorName: this.props.vendorName,
            transporter: this.props.transporter,
            siteDetail: this.props.siteDetail,
            adviceNo: this.props.adviceNo,
            adviceDate: this.props.adviceDate,
            poDate: this.props.poDate,
            shipmentConfirmDate: this.props.shipmentConfirmDate,
            totalQty: this.props.totalQty,
            dueDate: this.props.dueDate,
            count: this.props.filterCount,
            userType: this.props.userType,
            orderCode: this.props.orderCode,
            qcDate: this.props.qcDate
        })
    }

    handleChange(e) {
        if (e.target.id == "shipmentRequestDate") {
            this.setState({
                shipmentRequestDate: e.target.value
            });
        } else if (e.target.id == "poNumber") {
            this.setState({
                poNumber: e.target.value
            });
        } else if (e.target.id == "vendorName") {
            this.setState({
                vendorName: e.target.value
            })

        } else if (e.target.id == "adviceNo") {
            this.setState({
                adviceNo: e.target.value
            });
        } else if (e.target.id == "adviceDate") {
            this.setState({
                adviceDate: e.target.value
            });
        } else if (e.target.id == "poDate") {

            this.setState({
                poDate: e.target.value
            });

        } else if (e.target.id == "shipmentConfirmDate") {
            this.setState({
                shipmentConfirmDate: e.target.value
            })

        } else if (e.target.id == "transporter") {
            this.setState({
                transporter: e.target.value
            });
        } else if (e.target.id == "siteDetail") {
            this.setState({
                siteDetail: e.target.value
            });
        } else if (e.target.id == "totalQty") {
            if (e.target.validity.valid) {
                this.setState({
                    totalQty: e.target.value
                });
            }
        } else if (e.target.id == "dueDate") {
            if (e.target.validity.valid) {
                this.setState({
                    dueDate: e.target.value
                })
            }
        }
        else if (e.target.id == "orderCode") {
            this.setState({ orderCode: e.target.value })
        }
        else if (e.target.id == "qcDate") {
            this.setState({ qcDate: e.target.value })
        }
    }

    clearFilter(count) {
        this.setState({
            shipmentRequestDate: "",
            poNumber: "",
            vendorName: "",
            transporter: "",
            siteDetail: "",
            adviceNo: "",
            adviceDate: "",
            poDate: "",
            shipmentConfirmDate: "",
            totalQty: "",
            dueDate: "",
            count: 0,
            orderCode: "",
            qcDate: ""
        })
        this.props.filterCount != 0 ? this.props.clearFilter() : null
    }

    onSubmit(count) {
        // e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            shipmentRequestDate: this.state.shipmentRequestDate,
            poNumber: this.state.poNumber,
            status: this.state.status,
            vendorName: this.state.vendorName,
            transporter: this.state.transporter,
            siteDetail: this.state.siteDetail,
            adviceNo: this.state.adviceNo,
            adviceDate: this.state.adviceDate,
            poDate: this.state.poDate,
            shipmentConfirmDate: this.state.shipmentConfirmDate,
            totalQty: this.state.totalQty,
            dueDate: this.state.dueDate,
            isLRCreation: false,
            filterCount: count,
            search: "",
            userType: this.props.userType,
            orderCode: this.state.orderCode,
            qcDate: this.state.qcDate
        }
        this.props.getAllVendorShipmentShippedRequest(data);
        this.props.closeFilter();
        this.props.updateFilter(data)
    }

    render() {
        let count = 0;
        if (this.state.shipmentRequestDate != "") {
            count++;
        }
        if (this.state.poNumber != "") {
            count++;
        }
        if (this.state.vendorName != "") {
            count++
        }
        if (this.state.adviceNo != "") {
            count++

        } if (this.state.adviceDate != "") {
            count++
        }
        if (this.state.poDate != "") {
            count++;
        }
        if (this.state.shipmentConfirmDate != "") {
            count++;
        }
        if (this.state.transporter != "") {
            count++
        }
        if (this.state.siteDetail != "") {
            count++
        }
        if (this.state.totalQty != "") {
            count++
        } if (this.state.dueDate != "") {
            count++
        }
        if (this.state.orderCode != "") {
            count++;
        }
        if (this.state.qcDate != "") {
            count++;
        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(count)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="shipmentRequestDate" value={this.state.shipmentRequestDate} placeholder={this.state.shipmentRequestDate != "" ? this.state.shipmentRequestDate : "Shipment Requested Date"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="poNumber" value={this.state.poNumber} placeholder="PO Number" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorName" value={this.state.vendorName} placeholder="Vendor Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="adviceNo" value={this.state.adviceNo} placeholder="ASN No" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="adviceDate" value={this.state.adviceDate} placeholder={this.state.adviceDate != "" ? this.state.adviceDate : "ASN date"} className="organistionFilterModal" />


                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="poDate" value={this.state.poDate} placeholder={this.state.poDate != "" ? this.state.poDate : "PO date"} className="organistionFilterModal" />
                                    </li>

                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="shipmentConfirmDate" value={this.state.shipmentConfirmDate} placeholder={this.state.shipmentConfirmDate != "" ? this.state.shipmentConfirmDate : "Shipment confirm date"} className="organistionFilterModal" />
                                    </li>
                                </ul></div></div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="transporter" value={this.state.transporter} placeholder="Transporter Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} pattern="[0-9]*" id="totalQty" value={this.state.totalQty} placeholder="PO Qty" className="organistionFilterModal" />
                                    </li>
                                    {/*<li>
                                        <input type="date" onChange={(e) => this.handleChange(e)}  id="dueDate" value={this.state.dueDate} placeholder={this.state.dueDate!=""?this.state.dueDate:"Due Date"} className="organistionFilterModal" />
                                    </li>*/}
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="siteDetail" value={this.state.siteDetail} placeholder="Site Details" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="orderCode" value={this.state.orderCode} placeholder="Order Code" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="qcDate" value={this.state.qcDate} placeholder={this.state.qcDate != "" ? this.state.qcDate : "QC Date"} className="organistionFilterModal" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.state.qcDate == "" && this.state.shipmentRequestDate == "" && this.state.orderCode == "" && this.state.poNumber == "" && this.state.vendorName == "" && this.state.adviceNo == "" && this.state.adviceDate == "" && this.state.poDate == "" && this.state.shipmentConfirmDate == "" && this.state.transporter == "" && this.state.siteDetail == "" && this.state.totalQty == "" && this.state.dueDate == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button type="button" onClick={(e) => this.clearFilter(0)} className="modal_clear_btn">
                                                CLEAR FILTER
                                         </button>}
                                    </li>
                                    <li>
                                        {this.state.qcDate != "" || this.state.shipmentRequestDate != "" || this.state.orderCode != "" || this.state.poNumber != "" || this.state.vendorName != "" || this.state.adviceNo != "" || this.state.adviceDate != "" || this.state.poDate != "" || this.state.shipmentConfirmDate != "" || this.state.transporter != "" || this.state.siteDetail != "" || this.state.totalQty != "" || this.state.dueDate != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default FilterShipmentShipped;
