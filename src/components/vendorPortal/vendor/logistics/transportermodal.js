import React from "react";

export default class TransporterModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            transporterState: [],
            selectedId: "",
            checked: false,
            search: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            transporterCode: "",
            transporterName: "",
            sectionSelection: "",
            toastLoader: false,
            toastMsg: "",
            selectedData: this.props.transporter,
            selectedTransporterCode: "",
            selectedTransporterName: ""
        }

    }
    componentDidMount() {
        this.textInput.current.focus();
    }
    componentWillMount() {

        if (this.props.logistic.getAllVendorTransporter.isSuccess) {
            if (this.props.logistic.getAllVendorTransporter.data.resource != null) {
                this.setState({
                    transporterState: this.props.logistic.getAllVendorTransporter.data.resource,
                    prev: this.props.logistic.getAllVendorTransporter.data.prePage,
                    current: this.props.logistic.getAllVendorTransporter.data.currPage,
                    next: this.props.logistic.getAllVendorTransporter.data.currPage + 1,
                    maxPage: this.props.logistic.getAllVendorTransporter.data.maxPage,
                    selectedData: this.props.transporter
                })
            } else {

                this.setState({
                    transporterState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }

    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.logistic.getAllVendorTransporter.isSuccess) {
            if (nextProps.logistic.getAllVendorTransporter.data.resource != null) {

                this.setState({
                    transporterState: nextProps.logistic.getAllVendorTransporter.data.resource,
                    prev: nextProps.logistic.getAllVendorTransporter.data.prePage,
                    current: nextProps.logistic.getAllVendorTransporter.data.currPage,
                    next: nextProps.logistic.getAllVendorTransporter.data.currPage + 1,
                    maxPage: nextProps.logistic.getAllVendorTransporter.data.maxPage,

                })
            } else {

                this.setState({
                    transporterState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            document.getElementById("search").focus()

        }

    }



    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.logistic.getAllVendorTransporter.data.prePage,
                current: this.props.logistic.getAllVendorTransporter.data.currPage,
                next: this.props.logistic.getAllVendorTransporter.data.currPage + 1,
                maxPage: this.props.logistic.getAllVendorTransporter.data.maxPage,
            })
            if (this.props.logistic.getAllVendorTransporter.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.logistic.getAllVendorTransporter.data.currPage - 1,
                    slCode: this.props.slCode,
                    userType: "vendorlogi",
                    search: this.state.search,

                }
                this.props.getAllVendorTransporterRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.logistic.getAllVendorTransporter.data.prePage,
                current: this.props.logistic.getAllVendorTransporter.data.currPage,
                next: this.props.logistic.getAllVendorTransporter.data.currPage + 1,
                maxPage: this.props.logistic.getAllVendorTransporter.data.maxPage,
            })
            if (this.props.logistic.getAllVendorTransporter.data.currPage != this.props.logistic.getAllVendorTransporter.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.logistic.getAllVendorTransporter.data.currPage + 1,
                    slCode: this.props.slCode,
                    search: this.state.search,
                    userType: "vendorlogi"

                }
                this.props.getAllVendorTransporterRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.logistic.getAllVendorTransporter.data.prePage,
                current: this.props.logistic.getAllVendorTransporter.data.currPage,
                next: this.props.logistic.getAllVendorTransporter.data.currPage + 1,
                maxPage: this.props.logistic.getAllVendorTransporter.data.maxPage,
            })
            if (this.props.logistic.getAllVendorTransporter.data.currPage <= this.props.logistic.getAllVendorTransporter.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    userType: "vendorlogi",
                    slCode: this.props.slCode,
                    search: this.state.search,

                }
                this.props.getAllVendorTransporterRequest(data)
            }

        }
    }

    selectedData(e) {

        for (let i = 0; i < this.state.transporterState.length; i++) {
            if (this.state.transporterState[i].transporterName == e) {
                this.setState({
                    selectedData: this.state.transporterState[i].transporterCode,
                    selectedTransporterCode: this.state.transporterState[i].transporterCode,
                    selectedTransporterName: this.state.transporterState[i].transporterName
                })
            }

        }

    }

    ondone() {
        var sCode = {};

        if (this.props.transporter != this.state.selectedData) {
            sCode = {
                transporterCode: this.state.selectedTransporterCode,
                transporterName: this.state.selectedTransporterName,
            }

            let t = this
            setTimeout(function () {
                t.props.updateTransporterState(sCode);
            }, 100)
        }

        if (this.state.selectedData != "") {

            this.setState(
                {
                    search: "",
                })


            this.closeTransporter()
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 10000)
        }

        document.onkeydown = function (t) {

            if (t.which == 9) {
                return true;
            }
        }
    }

    closeTransporter(e) {
        this.props.onCloseTransporter(e)
        const t = this
        t.setState({
            search: "",
            sectionSelection: "",
            transporterState: [],
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
        })

    }

    onClear(e) {
        if (this.state.type == 2 || this.state.type == 3) {
            var data = {
                type: 1,
                no: 1,
                slCode: this.props.slCode,
                search: "",
                userType: "vendorlogi"

            }
            this.props.getAllVendorTransporterRequest(data)
        }
        this.setState({
            search: "",
            sectionSelection: "",
            type: "",
            no: 1
        })
        document.getElementById("search").focus()
    }



    onSearch(e) {

        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 10000);
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,

                })
                let data = {
                    type: 3,
                    no: 1,

                    slCode: this.props.slCode,
                    search: this.state.search,
                    userType: "vendorlogi"

                }
                this.props.getAllVendorTransporterRequest(data)

            }
            else {
                this.setState({
                    type: 2,

                })
                let data = {
                    type: 2,
                    no: 1,
                    slCode: this.props.slCode,
                    search: "",
                    userType: "vendorlogi"

                }
                this.props.getAllVendorTransporterRequest(data)
            }
        }
        document.getElementById("search").focus()

    }

    handleChange(e) {
        if (e.target.id == "search") {

            this.setState(
                {
                    search: e.target.value
                },

            );

            //     if (document.getElementById("sectionBasedOn").value == "transporterCode") {
            //         this.setState(
            //             {
            //                 transporterCode: e.target.value,
            //                 transporterName: "",

            //             },

            //         );
            //     }
            //     else if (document.getElementById("sectionBasedOn").value == "transporterName") {
            //         this.setState(
            //             {
            //                 transporterCode: "",
            //                 transporterName: e.target.value,

            //             },

            //         );
            //     }
            //     else if (document.getElementById("sectionBasedOn").value == "") {
            //         this.setState(
            //             {
            //                 transporterCode: "",

            //                 transporterName: "",
            //                 search: e.target.value
            //             },

            //         );

            //     }
            // } else if (e.target.id == "sectionBasedOn") {
            //     this.setState(
            //         {
            //             search: "",
            //             sectionSelection: e.target.value
            //         },
            //     );


        }

    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }

    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == 1 || this.state.type == "")) {
                if (this.state.transporterState.length != 0) {
                    this.setState({
                        // selectedId: this.state.transporterState[0].transporterName,
                        selectedData: this.state.transporterState[0].transporterName
                    })
                    this.selectedData(this.state.transporterState[0].transporterName)
                    let da = this.state.transporterState[0].transporterName
                    window.setTimeout(() => {
                        document.getElementById("search").blur()

                        document.getElementById(da).focus()
                    }, 0)
                } else {
                    window.setTimeout(() => {
                        document.getElementById("search").blur()

                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }

            if (e.target.value != "") {
                window.setTimeout(() => {
                    document.getElementById("search").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById(this.state.transporterState[0].transporterName).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.ondone();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.closeTransporter();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("closeButton").blur()
                document.getElementById("search").focus()
            }, 0)
        }
    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onClear();
        }
        if (e.key == "Tab") {
            if (this.state.transporterState.length != 0) {
                this.setState({
                    // selectedId: this.state.transporterState[0].transporterName,
                    selectedData: this.state.transporterState[0].transporterName
                })
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById(this.state.transporterState[0].transporterName).focus()
                }, 0)
            } else {
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }


    render() {

        const { sectionSelection, search } = this.state;
        return (

            <div className={this.props.transporterAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.transporterAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.transporterAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.transporterAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT TRANSPORTER</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only single entry from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10">
                                        <div className="col-md-9 col-sm-9 pad-0 chooseDataModal" >
                                            <li>

                                                {/* <select id="sectionBasedOn" name="sectionSelection" value={sectionSelection} onChange={(e) => this.handleChange(e)}>
                                                    <option value="">Choose</option>
                                                    <option value="transporterCode">Transporter Code</option>
                                                    <option value="transporterName">Transporter Name</option>

                                                </select> */}
                                                <input type="search" id="search" ref={this.textInput} onKeyDown={this._handleKeyDown} value={search} onKeyPress={this._handleKeyPress} onChange={(e) => this.handleChange(e)} className="search-box" placeholder="Type to search" />
                                                <label className="m-lft-15">
                                                    <button className="findButton" type="button" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                            <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                        </svg>
                                                    </button>
                                                </label>
                                            </li>
                                        </div>
                                        <li className="float_right">

                                            <label>
                                                <button type="button" className={this.state.search == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onClear(e)}>CLEAR</button>
                                            </label>
                                        </li>

                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Transporter Code</th>
                                                    <th>Transporter Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.transporterState == undefined ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.transporterState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.transporterState.map((data, key) => (
                                                    <tr key={key}>
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input id={data.transporterName} type="radio" name="transporter" checked={this.state.selectedData == `${data.transporterCode}`} onKeyDown={(e) => this.focusDone(e)} onChange={() => this.selectedData(`${data.transporterName}`)} />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data.transporterCode}</td>
                                                        <td className="pad-lft-8">{data.transporterName}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.ondone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.closeTransporter(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}



                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}

