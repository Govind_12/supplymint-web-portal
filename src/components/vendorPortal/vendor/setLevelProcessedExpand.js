import React, { Component } from 'react'
import plusIcon from '../../../assets/plus-blue.svg';
import moment from 'moment';
import SetLevelConfigHeader from '../setLevelConfigHeader'
import removeIcon from '../../../assets/removeIcon.svg';
import SetLevelConfirmationModal from '../setLevelConfirModal';

const userType = "entpo";
const todayDate = moment(new Date()).format("YYYY-MM-DD");

export default class SetLevelProcessedExpand extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: "",
            no: 1,
            type: 1,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            actionExpand: false,
            expandedId: 0,
            dropOpen: true,
            setBaseExpandDetails: [],
            setRequestedQty: "",
            setBasedArray: [],
            setLvlGetHeaderConfig: [],
            setLvlFixedHeader: [],
            setLvlCustomHeaders: {},
            setLvlHeaderConfigState: {},
            setLvlHeaderConfigDataState: {},
            setLvlFixedHeaderData: [],
            setLvlCustomHeadersState: [],
            setLvlHeaderState: {},
            setLvlHeaderSummary: [],
            setLvlDefaultHeaderMap: [],
            setLvlConfirmModal: false,
            setLvlHeaderMsg: "",
            setLvlParaMsg: "",
            setLvlHeaderCondition: false,
            setLvlSaveState: [],
            setLvlColoumSetting: false,

            itemBaseArray: [],
            expandedLineItems: {},
            qcFromDate: "",
            qcFromDateErr: "",
            qcToDate: "",
            qcToDateErr: "",
            dateerr: "",
            shipmentRequestedDate: "",
            remarks: "",
            qcFromFormatted: "",
            formattedDate: "",
            prevId: "",
            shipmentDetails: "",
            emptyShow: true,
            saveShow: true,
            mandateHeaderSet: []
        }
    }
    componentDidMount() {
        /* let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            basedOn: "SET"
        }
        this.props.getHeaderConfigRequest(payload) */
        if(this.props.poType == "poicode"){
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.item_Display,
                basedOn: "SET"
            }
            this.props.getItemHeaderConfigRequest(payload)
        }else{
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.set_Display,
                basedOn: "SET"
            }
            this.props.getItemHeaderConfigRequest(payload)
        }
        var formattedDate = moment(this.props.expandPODate, "DD-MM-YYYY").format("YYYY-MM-DD")
        this.setState({
            shipmentRequestedDate: formattedDate
        })
    }
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "ASN") {
            if(this.props.poType == "poicode"){
                let payload = {
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                    attributeType: "TABLE HEADER",
                    displayName: this.props.item_Display,
                    basedOn: "SET"
                }
                this.props.getItemHeaderConfigRequest(payload)
            }else{
                let payload = {
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                    attributeType: "TABLE HEADER",
                    displayName: this.props.set_Display,
                    basedOn: "SET"
                }
                this.props.getItemHeaderConfigRequest(payload)
            }
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.shipment.getCompleteDetailShipment.isSuccess && prevState.actionExpand == false) {
            return {
                setBaseExpandDetails: nextProps.shipment.getCompleteDetailShipment.data.resource == null ? [] : nextProps.shipment.getCompleteDetailShipment.data.resource.poDetails,
            }
        }
        if (nextProps.shipment.getShipmentDetails.isSuccess && prevState.actionExpand == false) {
            return {
                setBaseExpandDetails: nextProps.shipment.getShipmentDetails.data.resource == null ? [] : nextProps.shipment.getShipmentDetails.data.resource.poDetails,
            }
        }
        if (prevState.actionExpand && nextProps.orders.VendorCreateShipmentDetails.isSuccess) {
            let itemDetails = nextProps.orders.VendorCreateShipmentDetails.data.resource.poDetails
            let id = prevState.expandedId;
            return {
                expandedLineItems: { ...prevState.expandedLineItems, [id]: itemDetails },
                itemBaseArray: nextProps.orders.VendorCreateShipmentDetails.data.resource.poDetails
            }
        }
        // console.log(nextProps.replenishment.getHeaderConfig.isSuccess)
        /* if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            // console.log(nextProps.replenishment.getHeaderConfig)
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "SET") {
                let setLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let setLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let setLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                return {
                    setLvlCustomHeaders: prevState.setLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    setLvlGetHeaderConfig,
                    setLvlFixedHeader,
                    setLvlCustomHeadersState,
                    setLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    setLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    setLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    setLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    setLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                }
            }
        } */
        return {}
    }

    /* componentDidUpdate(nextProps, prevState){
        if(nextProps.replenishment.getHeaderConfig.isSuccess ){
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "SET") {
                let setLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let setLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let setLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                this.setState({
                    setLvlCustomHeaders: prevState.setLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    setLvlGetHeaderConfig,
                    setLvlFixedHeader,
                    setLvlCustomHeadersState,
                    setLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    setLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    setLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    setLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    setLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                })
            }
        }
    } */

    setLvlOpenColoumSetting(data) {
        if (this.state.setLvlCustomHeadersState.length == 0) {
            this.setState({
                setLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                setLvlColoumSetting: true
            })
        } else {
            this.setState({
                setLvlColoumSetting: false
            })
        }
    }
    setLvlpushColumnData(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlHeaderConfigDataState = this.state.setLvlHeaderConfigDataState
        let setLvlCustomHeaders = this.state.setLvlCustomHeaders
        let setLvlSaveState = this.state.setLvlSaveState
        let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
        let setLvlHeaderSummary = this.state.setLvlHeaderSummary
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (this.state.setLvlHeaderCondition) {
            if (!data.includes(setLvlGetHeaderConfig) || setLvlGetHeaderConfig.length == 0) {
                setLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(setLvlHeaderConfigDataState))) {
                    let invert = _.invert(setLvlFixedHeaderData)
                    let keyget = invert[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }
                if (!Object.keys(setLvlCustomHeaders).includes(setLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];
                    setLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(setLvlCustomHeadersState) || setLvlCustomHeadersState.length == 0) {
                setLvlCustomHeadersState.push(data)
                if (!setLvlCustomHeadersState.includes(setLvlHeaderConfigDataState)) {
                    let keyget = (_.invert(setLvlFixedHeaderData))[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }
                if (!Object.keys(setLvlCustomHeaders).includes(setLvlHeaderSummary)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];
                    setLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeadersState,
            setLvlCustomHeaders,
            setLvlSaveState,
            setLvlDefaultHeaderMap,
            setLvlHeaderSummary
        })
    }
    setLvlCloseColumn(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlHeaderConfigState = this.state.setLvlHeaderConfigState
        let setLvlCustomHeaders = []
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (!this.state.setLvlHeaderCondition) {
            for (let j = 0; j < setLvlCustomHeadersState.length; j++) {
                if (data == setLvlCustomHeadersState[j]) {
                    setLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlCustomHeadersState.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
            if (this.state.setLvlCustomHeadersState.length == 0) {
                this.setState({
                    setLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < setLvlGetHeaderConfig.length; i++) {
                if (data == setLvlGetHeaderConfig[i]) {
                    setLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlGetHeaderConfig.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
        }
        setLvlCustomHeaders.forEach(e => delete setLvlHeaderConfigState[e]);
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeaders: setLvlHeaderConfigState,
            setLvlCustomHeadersState,
        })
        setTimeout(() => {     // minValue,
            // maxValue
            let keygetvalue = (_.invert(this.state.setLvlFixedHeaderData))[data];     // minValue,
            // maxValue
            let setLvlSaveState = this.state.setLvlSaveState
            setLvlSaveState.push(keygetvalue)
            let setLvlHeaderSummary = this.state.setLvlHeaderSummary
            let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
            if (!this.state.setLvlHeaderCondition) {
                for (let j = 0; j < setLvlHeaderSummary.length; j++) {
                    if (keygetvalue == setLvlHeaderSummary[j]) {
                        setLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < setLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == setLvlDefaultHeaderMap[i]) {
                        setLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                setLvlHeaderSummary,
                setLvlDefaultHeaderMap,
                setLvlSaveState
            })
        }, 100);
    }
    setLvlsaveColumnSetting(e) {
        this.setState({
            setLvlColoumSetting: false,
            setLvlHeaderCondition: false,
            setLvlSaveState: []
        })
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: "PROCESSED-ORDERS",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    setLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            setLvlConfirmModal: true,
        })
    }
    setLvlResetColumn() {
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: "PROCESSED-ORDERS",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            setLvlHeaderCondition: true,
            setLvlColoumSetting: false,
            setLvlSaveState: []
        })
    }
    handleRequestedDate = (e) => {
        var formattedDate = moment(e.target.value, "YYYY-MM-DD").format("DD-MM-YYYY")
        this.setState({ shipmentRequestedDate: e.target.value, formattedDate: formattedDate == "Invalid date" ? "" : formattedDate }, () => {
            this.date()
        }
        )
    }
    handleChange = (e) => {
        var formattedDate = moment(e.target.value, "YYYY-MM-DD").format("DD-MM-YYYY")
        if (e.target.id == "qcFromDate") {
            this.setState({ qcFromDate: e.target.value, qcToDate: "", qcToFormatted: "", qcFromFormatted: formattedDate == "Invalid date" ? "" : formattedDate }, () => { this.fromDate() })
        } else if (e.target.id == "qcToDate") {
            this.setState({ qcToDate: e.target.value, qcToFormatted: formattedDate == "Invalid date" ? "" : formattedDate }, () => { this.toDate() })
        } else if (e.target.id == "remarks") {
            this.setState({ remarks: e.target.value })
        }
    }

    setLvlCloseConfirmModal(e) {
        this.setState({
            setLvlConfirmModal: !this.state.setLvlConfirmModal,
        })
    }
    // componentDidUpdate(previousProps, previousState) {
    //     if (this.props.replenishment.createHeaderConfig.isSuccess && this.props.replenishment.createHeaderConfig.data.basedOn == "SET") {
    //         let payload = {
    //             enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
    //             attributeType: "TABLE HEADER",
    //             displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
    //             basedOn: "SET"
    //         }
    //         this.props.getHeaderConfigRequest(payload)
    //     }
    // }
    render() {
     console.log("in se", this.props.itemCustomHeadersState, this.props.getItemHeaderConfig)
        return (
            <div>
                <div className="item-detail-table-expend">
                    {/* <SetLevelConfigHeader {...this.props} {...this.state} setLvlColoumSetting={this.state.setLvlColoumSetting} setLvlGetHeaderConfig={this.state.setLvlGetHeaderConfig} setLvlResetColumnConfirmation={(e) => this.setLvlResetColumnConfirmation(e)} setLvlOpenColoumSetting={(e) => this.setLvlOpenColoumSetting(e)} setLvlCloseColumn={(e) => this.setLvlCloseColumn(e)} setLvlPushColumnData={(e) => this.setLvlpushColumnData(e)} setLvlsaveColumnSetting={(e) => this.setLvlsaveColumnSetting(e)} />
                     */}
                    <div className="idte-inner" id="expandTableMain">
                        <div id="expandedTable" ref={node => this.expandTableref = node}>
                            <table className="table">
                                <thead>
                                    <tr>
                                        {/* <th className="fixed-side-role fixed-side1"><label></label></th> */}
                                        {/*  {this.state.setLvlCustomHeadersState.length == 0 ? this.state.setLvlGetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.state.setLvlCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))} */}
                                        {this.props.itemCustomHeadersState.length == 0 ? this.props.getItemHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.props.itemCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.setBaseExpandDetails.length != 0 ? this.state.setBaseExpandDetails.map((data, key) => (
                                        <React.Fragment key={key}>
                                            <tr>
                                                {/* <td className="fixed-side-role fixed-side1 twi-inner-fix">
                                                    <img src={this.state.dropOpen && this.state.expandedId == data.setBarCode ? removeIcon : plusIcon} className="imgHeight displayPointer" id={data.setBarCode} onClick={(e) => this.expandColumn(data.setBarCode, e, data)} />
                                                </td> */}
                                                {/* {this.state.setLvlHeaderSummary.length == 0 ? this.state.setLvlDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        <label>{data[hdata]}</label>
                                                    </td>
                                                )) : this.state.setLvlHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        <label>{data[sdata]}</label>
                                                    </td>
                                                ))} */}
                                                    {this.props.itemHeaderSummary.length == 0 ? this.props.itemDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        <label>{data[hdata]}</label>
                                                    </td>
                                                )) : this.props.itemHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        <label>{data[sdata]}</label>
                                                    </td>
                                                ))} 
                                            </tr>
                                        </React.Fragment>)) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {this.state.setLvlConfirmModal ? <SetLevelConfirmationModal {...this.state} {...this.props} setLvlCloseConfirmModal={(e) => this.setLvlCloseConfirmModal(e)} setLvlResetColumn={(e) => this.setLvlResetColumn(e)} /> : null}

            </div>
        )
    }
}
