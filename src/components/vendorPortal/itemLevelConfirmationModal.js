import React from 'react';
import alertIcon from '../../assets/alert-2.svg';


export default class ItemLevelConfirmationModal extends React.Component {
    constructor(props) {
        super(props);

    }
    onSubmit() {

        this.props.ItemLvlResetColumn();
        
        this.props.ItemLvlCloseConfirmModal()
    }

    render() {

        return (
            <div>
                <div className="save-current-data modalPos set-modal-mid">
                    <div className="col-md-12 modal-dialog m0">
                        <div className="background-blur"></div>
                        <div className="modal-content">
                            <div className="modal-data ">
                                <div className="modal-header">
                                    <div className="icon">
                                        <img src={alertIcon} />
                                    </div>
                                    <h2>{this.props.ItemLvlHeaderMsg}</h2>
                                    <p>{this.props.ItemLvlParaMsg}</p>
                                    <div className="modal-footer">
                                        <button className="close-btn" type="button" onClick={(e) => this.props.ItemLvlCloseConfirmModal(e)}>No</button>
                                        <button className="confirm-btn" type="button" onClick={(e) => this.onSubmit(e)}>Yes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}

