import React, { useEffect, useState } from 'react';

export const EmailTranscript = (props) => {
    useEffect(() => (
        toEmailHandle()
    ), [])
    return (
        <div className="email-transcript-modal">
            <div className="email-transcript-inner">
                <h3>Email Transcript</h3>
                <form>
                    <div className="eti-form">
                        <div id="toEmail" className="eti-form-inner">
                            <label>To</label>
                            <input type="text" id="fun" name="custom-input-toEmail" placeholder="Type email" />
                        </div>
                        <div id="ccEmail" className="eti-form-inner">
                            <label>CC</label>
                            <input type="text" name="custom-input-ccEmail" />
                        </div>
                        <div className="eti-form-inner">
                            <label>Subject</label>
                            <input type="text" onChange={(e) => props.handleDesc(e, 'subject')} name="subject" value={props.subject} className="eti-form-inner-subject"></input>
                        </div>
                        <div className="eti-form-inner">
                            <label>Remark</label>
                            <textarea placeholder="Write your remark here…" onChange={(e) => props.handleDesc(e, 'remark')} name="remark" value={props.remark}></textarea>
                        </div>
                        <div className="eti-form-inner">
                            <button type="button" className="eti-form-send-btn" onClick={props.submitTranscript}>Send Transcript</button>
                            <button type="button" className="eti-form-close-btn" onClick={props.handleEmailTranscriptModal}>Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
const toEmailHandle = () => {
    var input = document.querySelector('input[name="custom-input-toEmail"]'),
        // init Tagify script on the above inputs
        tagify = new Tagify(input, {
            pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            maxTags: 10,
            dropdown: {
                maxItems: 20,           // <- mixumum allowed rendered suggestions
                classname: "tags-look", // <- custom classname for this dropdown, so it could be targeted
                enabled: 0,             // <- show suggestions on focus
                closeOnSelect: false    // <- do not hide the suggestions dropdown once an item has been selected
            }
        })
    tagify.on('add', function (e) {
    })
    var input = document.querySelector('input[name="custom-input-ccEmail"]'),
        // init Tagify script on the above inputs
        tagify2 = new Tagify(input, {
            pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\.[0-9]{1, 3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            maxTags: 10,
            dropdown: {
                maxItems: 20,           // <- mixumum allowed rendered suggestions
                classname: "tags-look2`", // <- custom classname for this dropdown, so it could be targeted
                enabled: 0,             // <- show suggestions on focus
                closeOnSelect: false    // <- do not hide the suggestions dropdown once an item has been selected
            }
        })
}