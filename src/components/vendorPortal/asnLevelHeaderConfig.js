import React from 'react';
import { Link } from 'react-router-dom';
import successIcon from '../../assets/greencheck.png'
import closeSearch from "../../assets/close-recently.svg"
import _ from 'lodash';
import cross from '../../assets/group-4.svg'
export default class AsnLevelConfigHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            asnLvlColoumSetting: this.props.asnLvlColoumSetting,
            search: ""
        }
    }

    asnLvlOpenColoumSetting(data) {
        this.props.asnLvlOpenColoumSetting(data)
    }

    asnLvlCloseColumn(data) {
        this.props.asnLvlCloseColumn(data)
    }
    handleChange(e) {
        this.setState({
            search: e.target.value
        })
    }
    clearSearch() {
        this.setState({
            search: ""
        })
    }
    render() {
        const { search } = this.state
        var result = _.filter(this.props.asnLvlFixedHeader, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        });
        return (
            <div>
                {this.props.asnLvlColoumSetting ? <div className="columnFilterGeneric" >
                    <span className="glyphicon glyphicon-menu-right" onClick={(e) => this.asnLvlOpenColoumSetting("false")}></span>
                    <div className="columnSetting" onClick={(e) => this.asnLvlOpenColoumSetting("false")}>Columns Setting</div>
                    <div className="columnFilterDropDown">
                        <div className="col-md-12 pad-0 filterHeader">
                            <div className="col-md-7 pad-0">
                                <input type="search" className="width100 inputSearch" value={this.state.search} onChange={(e) => this.handleChange(e)} placeholder="Search Columns…" />
                                {this.state.search != "" ? <div className="crossIcon"><img src={cross} onClick={(e) => this.clearSearch(e)} /></div> : null}
                            </div>
                            <div className="col-md-5 pad-right-0 columns">
                                <div className="col-md-7 pad-0">
                                    <h3>Visible Columns <span>{this.props.asnLvlHeaderCondition ? this.props.asnLvlGetHeaderConfig.length : this.props.asnLvlCustomHeadersState.length}</span></h3>
                                </div>
                                <div className="col-md-5 pad-0 textCenter">
                                    <button className="resetBtn asd" onClick={(e) => this.props.asnLvlResetColumnConfirmation(e)}>Reset to default</button>
                                    {!this.props.asnLvlHeaderCondition ? <button className={this.props.asnLvlCustomHeadersState.length == 0 || this.props.asnLvlSaveState.length == 0 ? "opacity saveBtn" : "saveBtn"} onClick={(e) => this.props.asnLvlCustomHeadersState.length == 0 || this.props.asnLvlSaveState.length == 0 ? null : this.props.asnLvlsaveColumnSetting(e)}>Save</button> :
                                        <button className={this.props.asnLvlGetHeaderConfig.length == 0 || this.props.asnLvlSaveState.length == 0 ? "opacity saveBtn" : "saveBtn"} onClick={(e) => this.props.asnLvlGetHeaderConfig.length == 0 || this.props.asnLvlSaveState.length == 0 ? null : this.props.asnLvlsaveColumnSetting(e)}>Save</button>}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-7 pad-0">
                            <div className="filterLeftHead">
                                <div className="col-md-12 identifier">
                                    <h2>Fixed</h2>
                                    <ul className="list-inline">
                                        {result.length != 0 ? result.map((data, key) => (<div key={key} className="inlineBlock col-md-4 pad-lft-0">
                                            {this.props.asnLvlHeaderCondition ? <li className={!this.props.asnLvlGetHeaderConfig.includes(data) ? "active" : "opacity active"} onClick={(e) => !this.props.asnLvlGetHeaderConfig.includes(data) ? this.props.asnLvlpushColumnData(data) : null}>{data}</li> :
                                                <li className={!this.props.asnLvlCustomHeadersState.includes(data) ? "active" : "opacity active"} onClick={(e) => !this.props.asnLvlCustomHeadersState.includes(data) ? this.props.asnLvlpushColumnData(data) : null}>{data}</li>}
                                        </div>
                                        )) : "No Data Found"}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-5 pad-0">
                            <div className="filterRightHead">
                                <div className="col-md-12 visibleColumn">
                                    <h3>All Visible Columns</h3>
                                    <div className="row">
                                        {this.props.asnLvlHeaderCondition ? this.props.asnLvlGetHeaderConfig.map((data, key) => (
                                            <div key={key} className="col-md-6 pad-0 m-top-3 displayFlex justifyCenter"><label className="alignMiddle displayFlex"><span className="eachVisibleColumn">{data}</span>
                                                {this.props.mandateHeaderAsn.length != 0  || !this.props.mandateHeaderAsn.includes(dataa) && <span onClick={(e) => this.asnLvlCloseColumn(data)}> <img src={closeSearch} /></span>}</label></div>
                                        )) : this.props.asnLvlCustomHeadersState.map((dataa, keyy) => (
                                            <div key={keyy} className="col-md-6 pad-0 m-top-3 displayFlex justifyCenter">
                                                <label className="alignMiddle displayFlex">
                                                    <span className="eachVisibleColumn">{dataa}</span>{this.props.mandateHeaderAsn.length != 0 ? !this.props.mandateHeaderAsn.includes(dataa) && <span onClick={(e) => this.asnLvlCloseColumn(dataa)}> <img className="float_Right displayPointer" src={closeSearch} /></span> : <span onClick={(e) => this.asnLvlCloseColumn(dataa)}> <img className="float_Right displayPointer" src={closeSearch} /></span>}
                                                </label>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : <div className="columnFilterGeneric" onClick={(e) => this.asnLvlOpenColoumSetting("true")}>
                        <span className="glyphicon glyphicon-menu-left"></span></div>}
            </div>
        )
    }
}

