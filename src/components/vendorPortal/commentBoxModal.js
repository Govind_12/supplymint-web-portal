import React from 'react';
// import abc from '../../assets/'
import Minimize from '../../assets/minimize.svg';
import CloseRed from '../../assets/close-red.svg';
import Attach from '../../assets/attach.svg';
import Send from '../../assets/send2.svg';
import Attachments from '../../assets/attachments.svg';
import userIcon from '../../assets/user.svg';
import refreshIcon from '../../assets/refresh.svg';
import DownloadPdf from '../../assets/downloadpdf.svg';
import CloseBlack from '../../assets/close-black.svg';
import SmallClose from '../../assets/smallClose.svg';
import moment from 'moment';
import axios from 'axios';
import { CONFIG } from "../../config/index";
import ToastLoader from '../loaders/toastLoader';
import { decodeToken } from "../../helper/index"
import { EmailsDrop } from './emailsDrop';
import FilterLoader from '../loaders/filterLoader';
import { EmailTranscript } from './emailTranscript';
import ConfirmationSummaryModal from "../replenishment/confirmationReset";


export default class CommentBoxModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            closeChatBox: "show",
            comment: "",
            current: 0,
            toastLoader: false,
            toastMsg: "",
            loader: false,
            sendComment: [],
            allComment: [],
            id: this.props.data.orderCode,
            isAttachment: false,
            attachments: [],
            allEmails: [],
            handleEmailDrop: false,
            searchComment: "",
            type: 1,
            emailAttach: [],
            cursor: 0,
            file: "",
            filteredEmails: [],
            selectedTags: [],
            message: '',
            errorToast: false,
            successToast: false,
            transcriptModal: false,
            confirmModal: false,
            toEmails: "",
            ccEmails: "",
            headerMsg: "",
            prefix: undefined,

        }
        this.myRef = React.createRef();
        this.subRef = React.createRef();

    }


    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        this.getEmails()
        let payload = {
            // shipmentId: this.props.data.shipmentId == undefined ? this.props.data.id : this.props.data.shipmentId,
            // module: this.props.module,
            // no: this.state.current + 1,
            // orderNumber: this.props.data.orderNumber,
            // // orderCode: this.props.data.orderCode,
            // orderId: this.props.orderId,
            // documentNumber: this.props.documentNumber,
            // subModule: "COMMENTS"

            module: this.props.module,
            pageNo: this.state.current + 1,
            subModule: "COMMENTS",
            isAllAttachment: "",
            type: 1,
            search: "",
            commentId: this.props.data.commentId,
            commentCode: this.props.data.commentCode
        }
        this.props.getAllCommentRequest(payload)
        //comment qc update  || new comment 
        let t = this
        const messaging = firebase.messaging()
        messaging.onMessage(function (payload) {
            let data = {
                // shipmentId: t.props.data.shipmentId == undefined ? t.props.data.id : t.props.data.shipmentId,
                // module: t.props.module,
                // no: t.state.current + 1,
                // orderNumber: t.props.data.orderNumber,
                // orderCode: t.props.data.orderCode,
                // subModule: "COMMENTS",
                // isAllAttachment: t.state.isAttachment ? "TRUE" : "",
                // refresh: "normal",
                // orderId: t.props.orderId,
                // documentNumber: t.props.documentNumber,

                module: t.props.module,
                pageNo: t.state.current + 1,
                subModule: "COMMENTS",
                isAllAttachment:  t.state.isAttachment ? "TRUE" : "",
                type: 1,
                search: "",
                commentId: t.props.data.commentId,
                commentCode: t.props.data.commentCode
            }
            t.props.getAllCommentRequest(data)
        });
        // -------------------------------------------------------------
        let commentPayload = {
            module: this.props.module,
            status: "",
            orderNumber: this.props.data.orderNumber,
            orderId: this.props.data.orderId,
            status: "SEEN",
            shipmentId: this.props.data.shipmentId == undefined ? this.props.data.id : this.props.data.shipmentId,
            commentId: this.props.data.commentId,

        }
        this.props.commentNotificationRequest(commentPayload)

        this.props.onRef(this);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
        clearInterval(this.newMessage)
        this.props.onRef(undefined);
        clearInterval(this.intervalId);
        document.removeEventListener('keyup', this.enterTextFunction);
    }
    handleClickOutside = (event) => {
        if (this.myRef.current && !this.myRef.current.contains(event.target)) {
            this.props.openChatBox('close')
        }
        if (event != undefined && event.target != null && event.target.className.baseVal == undefined && event.target.className.includes("backdrop")) {
            this.props.openChatBox('close')
        }
        if (event != undefined && event.target != null && event.target.className.baseVal == undefined && event.target.className.includes("save")) {
            this.setState({ confirmModal: false })
        }
    }
    convertToMsg = () => {
        var tag = document.getElementsByTagName('tags')[0].querySelector('span')
        var div = document.getElementsByTagName('tags')[0].querySelector('span').querySelectorAll('div')
        var allTags = tag.querySelectorAll('tag')
        for (let i = 0; i < allTags.length; i++) {
            allTags[i].outerHTML = '@' + allTags[i].getAttribute('value') + ' '
        }
        for (let j = 0; j < div.length; j++) {
            if (div[j].innerHTML.innerText == undefined) {
                div[j].outerHTML = div[j].innerHTML
            }
        }
        return tag.innerHTML.replace(/\&nbsp;/g, '').replace(/\<br>/g, '')
    }
    sendMessage = () => {
        let userWatcher = "", emailWatcher = ""
        this.state.selectedTags.map((_) => { userWatcher = [...userWatcher, _.useName], emailWatcher = [...emailWatcher, _.title] })
        let commentData = []
        let allComment = [...this.state.allComment]
        document.getElementsByTagName('tags')[0].querySelector('span').innerHTML = ''
        allComment.map((item) => {
            if (item.commentVersion == "NEW") {
                let data = {
                    commentedBy: item.commentedBy,
                    comments: item.comments,
                    commentDate: item.commentDate,
                    commentType: item.commentType,
                    folderDirectory: item.folderDirectory,
                    folderName: item.folderName,
                    filePath: item.filePath,
                    fileName: item.fileName,
                    fileURL: item.fileURL,
                    urlExpirationTime: item.urlExpirationTime,
                    isUploaded: item.isUploaded,
                    uploadedOn: item.uploadedOn,
                    subModule: "COMMENTS" 
                }
                commentData.push(data)
            }
        })
        let moduleName = this.props.module;
        let payload = {
            shipmentId: this.props.data.shipmentId == undefined ? (this.props.data.saId == undefined ? 0 : this.props.data.saId) : this.props.data.shipmentId,
            module: moduleName,
            isQCDone: "",
            QCStatus: "",
            isInvoiceUploaded: "",
            orderNumber: this.props.data.orderNumber || "",
            orderId: this.props.data.orderId || "",
            documentNumber: this.props.documentNumber || "",
            reason: "",
            lgtNumber: this.props.lgtNumber,
            shipmentAdviceCode: this.props.shipmentAdviceCode || "",
            userWatcher: userWatcher.length > 0 && userWatcher.join(','),
            emailWatcher: emailWatcher.length > 0 && emailWatcher.join(','),
            [moduleName]: {
                QC: [],
                COMMENTS: [this.state.sendComment],
                INVOICE: []
            },
            gateEntryNo: this.props.data.gateEntryNo,
            grcNumber: this.props.data.grcNumber,
            commentId: this.props.data.commentId,
            commentCode: this.props.data.commentCode,
            vendorCode: this.props.data.vendorCode,
        }
        this.setState({ sendComment: [], emailAttach: [] })
        this.props.qcAddCommentRequest(payload)
    }
    addComment =(data)=> {
        var parsedMsg = this.convertToMsg()
        if (data != "" && (parsedMsg != "" && this.state.file == "")) {
            let date = new Date()
            let allComment = this.state.allComment
            let payload = {
                subModule: this.props.subModule,
                commentedBy: sessionStorage.getItem("prn"),
                comments: parsedMsg,
                commentDate: moment(date).format("YYYY-MM-DDTHH:mm:ss") + "+05:30",
                commentType: "TEXT",
                folderDirectory: "",
                folderName: "",
                filePath: "",
                fileName: "",
                fileURL: "",
                urlExpirationTime: "",
                isUploaded: "FALSE",
                uploadedOn: "",
                // commentVersion: "NEW"
            }
            this.setState({
                sendComment: payload
            })
            allComment = [...allComment, payload]
            // this.props.setAddComment(allComment)
            this.setState(
                {
                    allComment: allComment,
                    callAddComments: true,
                    comment: ""
                }, () => {
                    this.sendMessage()
                    document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight
                })
        }
        if (this.state.file != "") {
            this.handleFile("", "", "send", parsedMsg)
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getAllComment.isSuccess) {
            if (nextProps.shipment.getAllComment.data.resource != null) {
                let commentData = []
                let allComment = []
                commentData = this.state.allComment.length == 0 ? nextProps.shipment.getAllComment.data.resource.COMMENTS : [...allComment.reverse(), ...nextProps.shipment.getAllComment.data.resource.COMMENTS]
                allComment = [...commentData.reverse()]
                // this.props.setAddComment(allComment)
                var attach = this.state.isAttachment ? nextProps.shipment.getAllComment.data.resource.COMMENTS.reverse() : []
                this.setState({
                    allComment,
                    attachments: attach
                }, () => {
                    document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight
                })
            }
            this.setState({ loader: false })
        }
        if (nextProps.shipment.getUserEmails.isSuccess) {
            var formatEmails = nextProps.shipment.getUserEmails.data.resource !== null ? nextProps.shipment.getUserEmails.data.resource.map((_) => { return { id: _.id, value: _.userName.concat('(' + _.email + ')'), title: _.email, useName: _.userName } }) : []
            this.setState({
                allEmails: formatEmails,
                filteredEmails: formatEmails
            }, () => {
                this.tagifyFun()
            })
            this.props.getUserEmailsClear()
        }
        if (nextProps.shipment.getUserEmails.isError) {
            this.tagifyFun()
            this.props.getUserEmailsClear()
        }

        // email transcript  
        if (nextProps.orders.emailTranscript.isSuccess) {
            this.setState({
                loader: false,
                successToast: true,
                transcriptModal: false
            })
            this.props.emailTranscriptClear()
        } else if (nextProps.orders.emailTranscript.isError) {
            this.setState({
                errorToast: true,
                errorMessage: nextProps.orders.emailTranscript.message.error == undefined ? undefined : nextProps.orders.emailTranscript.message.error.errorMessage,
                errorCode: nextProps.orders.emailTranscript.message.error == undefined ? undefined : nextProps.orders.emailTranscript.message.error.errorCode,
                code: nextProps.orders.emailTranscript.message.status,
                alert: true,
                loader: false
            })
            this.props.emailTranscriptClear()
        }

        if (nextProps.shipment.getAllComment.isLoading && nextProps.shipment.getAllComment.data.refresh == "load") {
            this.setState({ loader: true })
        }
        if (nextProps.orders.emailTranscript.isLoading || nextProps.shipment.qcAddComment.isLoading) {
            this.setState({ loader: true })
        }
        if (nextProps.shipment.qcAddComment.isSuccess || nextProps.shipment.qcAddComment.isError) {
            this.setState({ loader: false })
        }
    }

    handleFile = (e, subModule, type, parsedMsg) => {
        document.getElementById('fileName').innerHTML = type == "add" ? e.target.files[0].name : ""
        if (type == "add") {
            this.setState({ file: e.target.files })
        } else if (type == "delete") {
            this.setState({ file: "" })
            document.getElementById('commentUpload').value = ""
        } else if (type == "send") {
            this.onFileUpload(this.state.file, subModule, parsedMsg)
            document.getElementById('commentUpload').value = ""
        }
    }

    onFileUpload(e, subModule, parsedMsg) {
        let files = ""
        files = Object.values(this.state.file)
        let date = new Date()
        let id = "commentUpload";
        var allPdf = files.some((data) => (data.name.split('.').pop().toLowerCase() != "pdf" && data.name.split('.').pop().toLowerCase() != "png" && data.name.split('.').pop().toLowerCase() != "jpg" && data.name.split('.').pop().toLowerCase() != "svg" && data.name.split('.').pop().toLowerCase() != "bmp" && data.name.split('.').pop().toLowerCase() != "jpeg"))
        var fileSize = files.some((data) => (data.size > 5000000))
        if (!allPdf && id == "commentUpload") {
            if (!fileSize) {
                let uploadNode = {
                    orderId: this.props.data.orderId,
                    documentNumber: this.props.documentNumber || "",
                    shipmentId: this.props.data.shipmentId == undefined ? this.props.data.id : this.props.data.shipmentId,
                    orderNumber: this.props.data.orderNumber || "",
                    module: this.props.module,
                    subModule: "COMMENTS",
                    comments: parsedMsg,
                    commentId: this.props.data.commentId,
                    commentCode: this.props.data.commentCode,
                    vendorCode: this.props.data.vendorCode,
                }
                const fd = new FormData();
                this.setState({
                    loader: true
                })
                let headers = {
                    'X-Auth-Token': sessionStorage.getItem('token'),
                    'Content-Type': 'multipart/form-data'
                }
                for (var i = 0; i < files.length; i++) {
                    fd.append("files", files[i]);
                }
                fd.append("uploadNode", JSON.stringify(uploadNode))

                axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
                    .then(res => {
                        this.setState({
                            loader: false
                        })
                        let result = res.data.data.resource
                        document.getElementsByTagName('tags')[0].querySelector('span').innerHTML = ""
                        if (id == "fileUpload") {
                            let invoice = this.state.invoice
                            for (let i = 0; i < result.length; i++) {
                                let payload = {
                                    commentedBy: sessionStorage.getItem("prn"),
                                    comments: parsedMsg,
                                    commentDate: "",
                                    commentType: "URL",
                                    folderDirectory: result[i].folderDirectory,
                                    folderName: result[i].folderName,
                                    filePath: result[i].filePath,
                                    fileName: result[i].fileName,
                                    fileURL: result[i].fileURL,
                                    urlExpirationTime: result[i].urlExpirationTime,
                                    invoiceVersion: "NEW",
                                    uploadedOn: moment(date).format("YYYY-MM-DDTHH:mm:ss") + "+05:30",
                                    submodule: subModule

                                }
                                invoice.push(payload)
                            }
                            this.setState({
                                invoice,
                                loader: false,
                                fileUploadedNew: true
                            }, () =>
                                document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight
                            )
                        } else {
                            let allComment = this.state.allComment
                            for (let i = 0; i < result.length; i++) {
                                let payload = {
                                    commentedBy: sessionStorage.getItem("prn"),
                                    comments: parsedMsg,
                                    commentDate: "",
                                    commentType: "URL",
                                    folderDirectory: result[i].folderDirectory,
                                    folderName: result[i].folderName,
                                    filePath: result[i].filePath,
                                    fileName: result[i].fileName,
                                    fileURL: result[i].fileURL,
                                    urlExpirationTime: result[i].urlExpirationTime,
                                    isUploaded: "TRUE",
                                    uploadedBy: sessionStorage.getItem('prn'),
                                    commentVersion: "NEW",
                                    uploadedOn: moment(date).format("YYYY-MM-DDTHH:mm:ss") + "+05:30",
                                    submodule: subModule
                                }
                                allComment.push(payload)
                            }
                            this.setState({
                                loader: false,
                                allComment,
                                callAddComments: true,
                                file: ""
                            },
                                () => {
                                    document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight
                                })
                        }
                    }).catch((error) => {
                        this.setState({
                            loader: false
                        })
                    })
            } else {
                this.setState({
                    toastMsg: "File Size Not More then 5mb ",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 1500);
            }

        } else {
            this.setState({
                toastMsg: "Please Upload PDF and image file type only ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        }
    }
    handleSearch = (e) => {
        e.stopPropagation()
        let userName = e.target.dataset.username
        if (e.target.id == "email") {
            document.getElementById('searchComment').focus()
            this.setState((prevState) => ({ handleEmailDrop: false, searchComment: prevState.searchComment.concat(userName.replace(/\((.+?)\)/g, "")) }))
        } else {
            this.setState({ searchComment: e.target.value })
            if (e.which == 50) {
                this.setState({ handleEmailDrop: true })
                document.getElementById('drop-position').style.top = '28%'
            } else if (e.which == 27) {
                this.setState({ handleEmailDrop: false })
            }
            else if (e.which == 13) {
                let payload = {
                    // type: 3,
                    // search: this.state.searchComment,
                    // shipmentId: this.props.data.shipmentId == undefined ? this.props.data.id : this.props.data.shipmentId,
                    // module: this.props.module,
                    // no: 1,
                    // orderCode: this.props.data.orderCode,
                    // orderNumber: this.props.data.orderNumber,
                    // subModule: "COMMENTS",
                    // isAllAttachment: "FALSE",
                    // refresh: 'load',
                    // orderId: this.props.orderId,
                    // documentNumber: this.props.documentNumber,

                    module: this.props.module,
                    pageNo: 1,
                    subModule: "COMMENTS",
                    isAllAttachment:  "FALSE",
                    type: 3,
                    search: this.state.searchComment,
                    commentId: this.props.data.commentId,
                    commentCode: this.props.data.commentCode
                    
                }
                this.setState({ type: 3 })
                this.props.getAllCommentRequest(payload)
            }
        }
    }
    searchClear = () => {
        if (this.state.type == 3) {
            let payload = {
                // type: 1,
                // search: "",
                // shipmentId: this.props.data.shipmentId == undefined ? this.props.data.id : this.props.data.shipmentId,
                // module: this.props.module,
                // no: 1,
                // orderCode: this.props.data.orderCode,
                // orderNumber: this.props.data.orderNumber,
                // subModule: "COMMENTS",
                // isAllAttachment: "FALSE",
                // refresh: 'load',
                // orderId: this.props.orderId,
                // documentNumber: this.props.documentNumber,

                module: this.props.module,
                pageNo: 1,
                subModule: "COMMENTS",
                isAllAttachment:  "FALSE",
                type: 1,
                search: "",
                commentId: this.props.data.commentId,
                commentCode: this.props.data.commentCode
            }
            this.setState({ searchComment: "" })
            this.props.getAllCommentRequest(payload)
        }
        this.setState({ searchComment: "" })
    }
    getEmails = () => {
        const tokenDetails = decodeToken()
        const uType = sessionStorage.getItem('uType')
        var payload = {
            retailerEId: tokenDetails.schemaEntID,
            vendorEId: uType == "VENDOR" ? tokenDetails.eid : "",
            retailerOrgId: tokenDetails.schemaOrgID,
            vendorOrgId: uType == "VENDOR" ? tokenDetails.coreOrgID : "",
            dataRequired: "BOTH",
            userCallFrom: uType == "VENDOR" ? "VENDOR" : "RETAILER"
        }
        this.props.getUserEmailsRequest(payload)
    }
    // viewAllAttachment = () => {
    //     if (!this.state.isAttachment) {
    //         let payload = {
    //             shipmentId: this.props.data.shipmentId == undefined ? this.props.data.id : this.props.data.shipmentId,
    //             module: this.props.module,
    //             pageNo: 1,
    //             orderCode: this.props.data.orderCode,
    //             orderNumber: this.props.data.orderNumber,
    //             subModule: "COMMENTS",
    //             isAllAttachment: "TRUE",
    //             refresh: 'load',
    //             orderId: this.props.orderId,
    //             documentNumber: this.props.documentNumber,

    //         }
    //         this.props.getAllCommentRequest(payload)
    //     } else {
    //         let payload = {
    //             shipmentId: this.props.data.shipmentId == undefined ? this.props.data.id : this.props.data.shipmentId,
    //             module: this.props.module,
    //             no: this.state.current + 1,
    //             orderNumber: this.props.data.orderNumber,
    //             orderCode: this.props.data.orderCode,
    //             subModule: "COMMENTS",
    //             refresh: 'load',
    //             orderId: this.props.orderId,
    //             documentNumber: this.props.documentNumber,

    //         }
    //         this.props.getAllCommentRequest(payload)
    //     }
    //     this.setState({ isAttachment: !this.state.isAttachment })
    // }
    deleteUploads(data) {
        // let barcode = this.state.barcode.filter((check) => check.fileURL != data.fileURL)
        let files = []
        let a = {
            fileName: data.fileName,
            isActive: "FALSE"
        }
        files.push(a)
        let payload = {
            shipmentId: this.props.data.shipmentId == undefined ? 0 : this.props.data.shipmentId,
            module: this.props.module,
            subModule: "COMMENTS",
            files: files,
            commentId: this.props.data.commentId
        }
        this.setState({
            selectedFiles: files,
            // barcode
        })
        this.props.deleteUploadsRequest(payload)
    }
    tagifyFun = () => {
        const t = this
        var allEmails = this.state.allEmails
        var input = document.querySelector('[name=mix]'),
            tagify = new Tagify(input, {
                mode: 'mix',
                //pattern: /@|#/,
                pattern: /@/,
                transformTag: transformaTag,
                dropdown: {
                    enabled: 0,
                    position: "text",
                    classname: "customSuggestionsList",
                    searchKeys: ["value", "title"],
                    highlightFirst: true,  // automatically highlights first sugegstion item in the dropdown
                },
                whitelist: allEmails.map(function (item) { return typeof item == 'string' ? { value: item } : item }),
                callbacks: {
                    add: console.log,  // callback when adding a tag
                    remove: console.log   // callback when removing a tag
                }
            })
        function transformaTag(tagData) {
            tagData.value = tagData.value.replace(/\((.+?)\)/g, "")
        }
        tagify.on('input', function (e) {
            var prefix = e.detail.prefix;
            if (prefix) {
                if (prefix == '@')
                    tagify.settings.whitelist = allEmails;

                if (e.detail.value.length > 1) {
                    tagify.dropdown.show.call(tagify, e.detail.value);
                }
            }
            t.setState({ message: tagify.DOM.input.innerText, })
        })
        tagify.on('add', function (e) {
            t.setState({ selectedTags: tagify.value, prefix: '@' })
        })
        document.addEventListener('keyup', this.enterTextFunction);
    }
    
    enterTextFunction = (e) =>{
        if( e.key === 'Enter' && !e.altKey && !e.ctrlKey && !e.shiftKey && this.state.prefix == undefined){
            this.addComment();
        }
        if( e.key === 'Enter' && (e.altKey || e.ctrlKey || e.shiftKey) && this.state.prefix == undefined){
            document.getElementsByClassName('tagify__input')[0].innerText+ "\n";
        }
        else{
            this.setState({ prefix: undefined})
        }           
    }

    closeConfirmModal = (e) => {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }

    submitTranscript = () => {
        var toTags = [...document.getElementById("toEmail").querySelectorAll('tag')]
        var ccTags = [...document.getElementById("ccEmail").querySelectorAll('tag')]
        this.state.toEmails = toTags.map((_) => _.getAttribute('value'))
        this.state.ccEmails = ccTags.map((_) => _.getAttribute('value'))
        var subject = this.state.subject
        if (this.state.toEmails.length == 0) {
            // To cannot be empty
            this.setState({
                toastMsg: "Please enter a valid email address.",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);

        }
        if (this.state.toEmails.length != 0 && (this.state.subject === undefined || subject.length == 0)) {
            this.setState({
                headerMsg: "Are you sure want to send this message without subject ?",
                confirmModal: true,
                subject: ""
            })

        }

        if (this.state.toEmails.length > 0 && this.state.subject !== undefined && this.state.subject.length !== 0) {
            this.resetColumn();
        }

    }
    resetColumn = () => {
        let payload = {
            "shipmentId": this.props.data.shipmentId == undefined ? this.props.data.id : this.props.data.shipmentId,
            "orderId": this.props.data.orderId,
            "module": this.props.module,
            "subModule": "COMMENTS",
            "shipmentAdviceCode": this.props.shipmentAdviceCode || "",
            "documentNumber": this.props.documentNumber,
            "logisticNumber": this.props.lgtNumber || "",
            "to": this.state.toEmails.join(','),
            "cc": this.state.ccEmails.join(','),
            "subject": this.state.subject,
            "remarks": this.state.remark

        }
        // if (this.state.subject === undefined) {
        //     this.setState({
        //         toastMsg: "Subject is empty.",
        //         toastLoader: true
        //     })
        //     setTimeout(() => {
        //         this.setState({
        //             toastLoader: false
        //         })
        //         this.props.emailTranscriptRequest(payload)
        //     }, 1500);
        // }
        this.props.emailTranscriptRequest(payload)
        // api call
    }

    handleDesc = (event, name) => {
        this.setState({
            [name]: event.target.value
        })
    }
    handleEmailTranscriptModal = () => {
        this.setState({
            transcriptModal: !this.state.transcriptModal
        })
    }
    highlighTag = (comment) => {
        if (comment.match(`@\\w+`) === null) return comment.trim();
        else {
            var finalCommentStr = "";
            //if (comment.match(`@\\w+`) !== null) {
                var commentArray = comment.split(" ");
                commentArray.forEach( function(word){
                   if( word.match(`@\\w+`)){
                     word.split("@").forEach( function(char, index){
                         if( index == 0)
                            finalCommentStr += char;
                         else
                            finalCommentStr += char.replace(char, `<span class="mention-user"> @${char} </span> `);   
                     })  
                     
                   }else
                     finalCommentStr += word +" ";
                });
                // var replaceStr = comment.match(`@\\w+`)[0]
                // replaceStr = replaceStr.replace('@', '')
                // comment = comment.replace('@' + replaceStr, `<span class="mention-user"> @${replaceStr} </span> `)
                // if (comment.match(`@\\w+`) !== null) return this.highlighTag(comment)
                // else {
                //     return comment
                // }
                return finalCommentStr.trim();
            //}
        }
    }
    render() {
        const { searchComment, loader, file } = this.state
        return (
            // <div className={this.state.closeChatBox == "hide" ? "table-chat-box" : this.state.closeChatBox == "show" ? "table-chat-box visible-chatbox" : this.state.closeChatBox == "open" ? "table-chat-box visible-chatbox minimize-chatbox" : null}>
            <div>
                <div className="backdrop modal-backdrop-new"></div>
                {/* <div ref={this.myRef}> */}
                <div className="table-chat-box-inner" >
                    <div className="tcb-head" id="openMinimize" onClick={(event, data) => this.props.openChatBox(event, data)}>
                        <div className="tcb-head-top">
                            <div className="tcbht-inner">
                                <h3>Comments</h3>
                                {/* <span><img id="close" className="displayPointer" onClick={(event, data) => this.props.openChatBox(event, data)} id="minimize" src={Minimize}></img></span> */}
                                <span className="tcbh-close">
                                    <span className="tcbh-esc">Esc</span>
                                    <img src={require('../../assets/clearSearch.svg')} className="displayPointer" id={this.props.data.orderNumber == undefined ? this.props.openChats[0].id : this.props.data.orderNumber} data-id="closeChat" onClick={(event, data) => this.props.openChatBox(event, data)} ></img>
                                </span>
                            </div>
                        </div>
                        <div className="tcb-head-bottom">
                            <div className="tcb-name">
                                {this.props.module == "SHIPMENT" && <table>
                                    <tr><td><p>PO NUMBER</p></td><td style={{paddingLeft: "10px"}}><p>ASN NO.</p></td></tr>
                                    <tr>
                                        <td id="openMinimize"><span>{this.props.data.orderNumber}</span></td>
                                        <td id="openMinimize" style={{paddingLeft: "10px"}}><span>{this.props.data.adviceNo}</span></td>
                                    </tr>
                                </table> }
                                {this.props.module == "GOODS_IN_TRANSIT" && <table>
                                    <tr><td><p>LOGISTICS NO</p></td></tr>
                                    <tr><td id="openMinimize"><span>{this.props.data.lgtNo}</span></td></tr>
                                </table> }
                                {this.props.module == "GATE_ENTRY" && <table>
                                    <tr><td><p>GATE ENTRY NO.</p></td><td style={{paddingLeft: "10px"}}><p>ASN NO.</p></td></tr>
                                    <tr><td id="openMinimize"><span>{this.props.data.gateEntryNo}</span></td>
                                        <td id="openMinimize" style={{paddingLeft: "10px"}}><span>{this.props.data.adviceNo}</span></td>
                                    </tr>
                                </table> }
                                {this.props.module == "GRC_STATUS" && <table>
                                    <tr><td><p>GRC NO.</p></td><td style={{paddingLeft: "10px"}}><p>ASN NO.</p></td></tr>
                                    <tr><td id="openMinimize"><span>{this.props.data.grcNumber}</span></td>
                                        <td id="openMinimize" style={{paddingLeft: "10px"}}><span>{this.props.data.adviceNo}</span></td>
                                    </tr>
                                </table> }
                                {this.props.module == "ORDER" && <table>
                                     <tr><td><p>PO NUMBER</p></td></tr>
                                      <tr><td id="openMinimize"><span>{this.props.data.orderNumber}</span></td></tr>
                                </table> }
                                
                             {/* <p>{this.props.module == "SHIPMENT" ? "ASN NO." : this.props.module == "LOGISTICS" ? "LOGISTICS NO" : "PO NUMBER"}</p>
                                <a id="openMinimize">{this.props.module == "SHIPMENT" ? this.props.data.adviceNo : this.props.module == "LOGISTICS" ? this.props.data.lgtNo : this.props.data.orderNumber}</a> */}
                                <div className="tcb-hb-search-box">
                                    <input type="text" autoautoComplete="off" id="searchComment" placeholder="Search comments" onKeyDownCapture={(e) => this.handleSearch(e)} onChange={(e) => this.handleSearch(e)} value={searchComment} />
                                    {searchComment != "" ? <span className="closeSearch"><img src={require('../../assets/close-recently.svg')} onClick={this.searchClear} /></span> : null}
                                    <span className="tcb-hb-search-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17.094" height="17.231" viewBox="0 0 17.094 17.231">
                                            <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" />
                                        </svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="tcb-body chat-area" >
                        {/* <div className="tch-msg-attachment">
                            <label className="loadingCenter">Loading...</label>
                            {this.state.loader && <label className="loadingCenter">Loading...</label>}
                            <span><img src={Attachments} className="displayPointer" onClick={this.viewAllAttachment}></img></span>
                            <span><img src={refreshIcon} className="displayPointer" onClick={this.refreshChat} /></span>
                        </div> */}
                        {!this.state.isAttachment ? <div className="tcb-messages width100" id="scrollBot">
                            <div className="msg-sender">
                                <span>
                                    {/* <a className="ms-short-name">GS</a> */}
                                </span>
                                <span></span>
                            </div>
                            <div className="msg-reciver">
                                {this.state.allComment.length != 0 ? this.state.allComment.map((data, key) => (
                                    <div key={key} className={sessionStorage.getItem("prn") == data.commentedBy || sessionStorage.getItem("prn") == data.uploadedBy ? "messageSender " : "messageReceiver "}>
                                        <div className="each-comment m-top-15">
                                            <div className="msgBg">
                                                {/* <img className="msg-user-icon" src={userIcon} /> */}
                                                <span className="msg-user-icon-text">{data.commentedBy.charAt(0)}</span>
                                                {data.commentType == "TEXT" ?
                                                    <label>
                                                        <pre dangerouslySetInnerHTML={{
                                                            __html: this.highlighTag(data.comments)
                                                        }} />
                                                    </label>
                                                    // <label>{/*<span className="mention-user"></span>*/} {this.highlighTag(data.comments), console.log("asdads", this.highlighTag(data.comments))}</label>
                                                    : <div>
                                                        {data.comments != "" && <label><pre dangerouslySetInnerHTML={{ __html: this.highlighTag(data.comments) }} /></label>}
                                                        <div className="tcb-file">
                                                            <span className="msg-name-pdf">
                                                                {/* <img src={require('../../assets/pdf-file.svg')} /> */}
                                                                {/* <img src={require('../../assets/doc-file.svg')} /> */}
                                                            </span>
                                                            <a href={data.fileURL} className="fileNameQc" target="_blank" >{data.fileName}<img href={data.fileURL} className="msg-pdfdownload-link" src={require('../../assets/msg-download.svg')} /></a>{sessionStorage.getItem("prn") == data.uploadedBy && <span className="msg-clear-msg" onClick={(e) => this.deleteUploads(data)}><img src={require('../../assets/clearSearch.svg')} /></span>}</div>
                                                    </div>}
                                            </div>
                                            <div className="msg-user-detail">
                                                <span className="msg-user-name">{sessionStorage.getItem("prn") == data.commentedBy || sessionStorage.getItem("prn") == data.uploadedBy ? "You" : sessionStorage.getItem('uType') == "ENT" ? "Vendor" : "Retailer"}</span>
                                                {data.commentType == "TEXT" ? <span className="dtof-msg">{data.commentDate}</span> : <span className="dtof-msg">{data.uploadedOn}</span>}
                                            </div>
                                        </div>

                                    </div>
                                )) : null}
                            </div>
                        </div>
                            : <div className="tcb-messages" id="scrollBot"> {this.state.attachments.map((data) => (<div className="messageReceiver"><div className="each-comment m-top-15"><div className="msgBg" >
                                <span className="msg-user-icon-text"></span>
                                <div className="tcb-file">
                                    {/* <span className="msg-name-pdf">PDF</span> */}
                                    <a href={data.fileURL} className="fileNameQc" target="_blank" >{data.fileName} <img href={data.fileURL} className="msg-pdfdownload-link" src={DownloadPdf} /></a>
                                    <span className="displayPointer" onClick={(e) => this.deleteUploads(data)}>&#10005;</span></div>
                            </div>
                            </div>
                            </div>))}</div>}
                    </div>
                    <div className="tcb-footer">
                        <div className="tcb-footer-inner">
                            <div className="tcb-footer-inner-item">
                                <div className="tcb-footer-write-comment-field">
                                    <div id="all-email-container">
                                        {/* {emailAttach.length != 0 && emailAttach.map((_, k) => (
                                            <span className={_.userName} key={k}>{_.userName}</span>
                                        ))} */}
                                    </div>
                                    {/* <input type="text" name="message" autoautoComplete="off" value={comment} id="message" onKeyDownCapture={(e) => this.handleMsg(e)} onChange={(e) => this.handleMsg(e)} placeholder="Write your comment here…" /> */}
                                    <textarea name='mix' value="" placeholder="Write a message"> </textarea>

                                    {/* <span className="tcb-attach-img"><input type="file" /><img src={Attach}></img></span> */}
                                    <div className="tcb-footer-attach-file">
                                        <span className="tcbfaf-jumpnewline">
                                            <span className="tcbfafj-uper">Jump to new line</span>
                                            <span className="tcbfafj-lower">Shift + Enter</span>
                                        </span>
                                        <span className="tcbfaf-jumpnewline tcbfaf-border">
                                            <span className="tcbfafj-uper">Send Comment</span>
                                            <span className="tcbfafj-lower">Enter</span>
                                        </span>
                                        <div className="tcb-footer-attach-file-name">
                                            {file != "" && <span className="tcb-fafn-icon" onClick={(e) => this.handleFile("", "", "delete")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.826" height="15.391" viewBox="0 0 12.826 15.391">
                                                    <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M6.489 12.185a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm3.207-10.9v1.28H2V1.283h3.663c.577 0 1.046-.7 1.046-1.283h3.409c0 .578.468 1.283 1.046 1.283zM12.9 3.848v10.261H3.924V3.848H2.641v11.543h11.544V3.848z" transform="translate(-2)" />
                                                </svg>
                                            </span>}
                                            <p id="fileName"></p>
                                            {/* <span className="tcb-footer-attach-file-count">+2</span> */}
                                        </div>

                                        {/* <div className="tcb-footer-attach-file-mini-modal">
                                            <li>
                                                <span className="tcb-fafn-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.826" height="15.391" viewBox="0 0 12.826 15.391">
                                                        <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M6.489 12.185a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm3.207-10.9v1.28H2V1.283h3.663c.577 0 1.046-.7 1.046-1.283h3.409c0 .578.468 1.283 1.046 1.283zM12.9 3.848v10.261H3.924V3.848H2.641v11.543h11.544V3.848z" transform="translate(-2)" />
                                                    </svg>
                                                </span>
                                            <p>Documents.pdf</p>
                                            </li>
                                            <li>
                                            <span className="tcb-fafn-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.826" height="15.391" viewBox="0 0 12.826 15.391">
                                                    <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M6.489 12.185a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm3.207-10.9v1.28H2V1.283h3.663c.577 0 1.046-.7 1.046-1.283h3.409c0 .578.468 1.283 1.046 1.283zM12.9 3.848v10.261H3.924V3.848H2.641v11.543h11.544V3.848z" transform="translate(-2)" />
                                                </svg>
                                            </span>
                                            <p>Report.pdf</p>
                                            </li>
                                            <li>
                                                <span className="tcb-fafn-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.826" height="15.391" viewBox="0 0 12.826 15.391">
                                                        <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M6.489 12.185a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm3.207-10.9v1.28H2V1.283h3.663c.577 0 1.046-.7 1.046-1.283h3.409c0 .578.468 1.283 1.046 1.283zM12.9 3.848v10.261H3.924V3.848H2.641v11.543h11.544V3.848z" transform="translate(-2)" />
                                                    </svg>
                                                </span>
                                                <p>Invoice_vmart.pdf</p>
                                            </li>
                                        </div> */}

                                        <div className="writeComment">
                                            <label className="custom-file-Icon">
                                                <input type="file" id="commentUpload" onChange={(e) => this.handleFile(e, "COMMENTS", "add")} />
                                                <img src={Attach} />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="tcb-footer-addcomment">
                                    {/* <span className="tcb-send-img"><img src={Send} onClick={() => this.addComment(this.state.comment)}></img></span> */}
                                    <button type="button" className="tcb-footer-add-comment" onClick={() => this.addComment()}>Add Comment
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.444" height="14.491" viewBox="0 0 12.444 14.491">
                                            <g id="share_1_" data-name="share (1)" transform="rotate(180 24.3 7.246)">
                                                <g id="Group_2791" data-name="Group 2791" transform="translate(36.155)">
                                                    <path id="Path_825" fill="#fff" d="M48.419 4.229L44.376.18a.615.615 0 1 0-.87.869l3 3.006h-6.154a4.205 4.205 0 0 0-4.2 4.185v5.635a.614.614 0 1 0 1.229 0V8.243a2.974 2.974 0 0 1 2.968-2.958H46.5l-2.995 2.994a.615.615 0 0 0 .87.869L48.419 5.1a.614.614 0 0 0 0-.871z" data-name="Path 825" transform="translate(-36.155)"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </button>                                    {/* <button type="button" className="tcb-footer-add-comment" onClick={() => this.addComment(this.state.comment)}>Add Comment</button> */}

                                    <button type="button" className="tcb-footer-email-tcranscript" onClick={() => this.handleEmailTranscriptModal()}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17.028" height="17" viewBox="0 0 17.028 17">
                                            <path fill="#fff" id="prefix__email-transcript" d="M13.48 6.441S9.8 6.2 8.514 7.816A7.4 7.4 0 0 1 13.48 3.6V2.184l3.547 2.838-3.547 2.851V6.441zM.018 17.039h16.99L8.514 8.8l-8.5 8.235zm1.187-9.933l7.3-5.313 2.166 1.574a10.209 10.209 0 0 1 1.386-.748L8.507.039 0 6.229v8.852l5.216-5.057-4.011-2.918zm11.177 3.471l4.646 4.5V6.842l-4.646 3.735z" transform="translate(0 -.039)" />
                                        </svg>
                                        Email Transcript
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mention-person-in-comment" id="drop-position">
                        {this.state.handleEmailDrop && <EmailsDrop  {...this.state} handleKeyDown={this.handleKeyDown} allEmails={this.state.allEmails} handleMsg={this.handleSearch} />}
                    </div>
                </div>
                {this.state.transcriptModal && <EmailTranscript {...this.state} submitTranscript={this.submitTranscript} ref={this.subRef} handleDesc={this.handleDesc} handleEmailTranscriptModal={this.handleEmailTranscriptModal} />}
                {/* </div> */}
                {this.state.errorToast && <div className="email-transcript-alert-msg">
                    <div className="etam-inner">
                        <h3>Failed to Send</h3>
                        <span className="etam-status-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__cancel" width="18" height="18" viewBox="0 0 22 22">
                                <g id="prefix__Group_26" data-name="Group 26">
                                    <path fill="#eb0008" id="prefix__Path_11" d="M11 22a11 11 0 1 1 11-11 11.012 11.012 0 0 1-11 11zm0-20.167A9.167 9.167 0 1 0 20.167 11 9.177 9.177 0 0 0 11 1.833zm3.667 13.75a.914.914 0 0 1-.648-.269L6.685 7.981a.917.917 0 0 1 1.3-1.3l7.333 7.333a.917.917 0 0 1-.648 1.565zm0 0a.914.914 0 0 1-.648-.269L6.685 7.981a.917.917 0 0 1 1.3-1.3l7.333 7.333a.917.917 0 0 1-.648 1.565zm-7.333 0a.917.917 0 0 1-.648-1.565l7.333-7.333a.917.917 0 0 1 1.3 1.3l-7.338 7.33a.914.914 0 0 1-.648.269z" data-name="Path 11" />
                                </g>
                            </svg>
                        </span>
                        <p>Please try again.</p>
                        <sapn className="etam-close" onClick={() => this.setState({ errorToast: false })}>
                            <img src={SmallClose} />
                        </sapn>
                    </div>
                </div>}
                {this.state.successToast && <div className="email-transcript-alert-msg">
                    <div className="etam-inner">
                        <h3>Transcript Sent</h3>
                        <span className="etam-status-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">
                                <path fill="#3ee585" id="prefix__success" d="M10.5 0A10.5 10.5 0 1 0 21 10.5 10.5 10.5 0 0 0 10.5 0zM9.406 14.453l-3.937-3.779L6.69 9.418l2.693 2.57 5.342-5.441 1.243 1.233z" />
                            </svg>
                        </span>
                        <p>Please check Your Mailbox or spam.</p>
                        <sapn className="etam-close" onClick={() => this.setState({ successToast: false })}>
                            <img src={SmallClose} />
                        </sapn>
                    </div>
                </div>}
                {loader && <FilterLoader />}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
            </div>
        )
    }
}