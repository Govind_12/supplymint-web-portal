import React, { Component } from 'react';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';
import searchIcon from '../../../../assets/close-recently.svg';
import plusIcon from '../../../../assets/plus-blue.svg';
import removeIcon from '../../../../assets/removeIcon.svg';
import ItemLevelDetails from '../../modals/itemLevelDetails';
import SetLevelConfigHeader from '../../setLevelConfigHeader';
import ConfirmationSummaryModal from '../../../replenishment/confirmationReset';
import SetLevelConfirmationModal from '../../setLevelConfirModal';

export default class ExpandModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            poDetailsShipment: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            toastLoader: false,
            toastMsg: "",
            expandedId: "",
            dropOpen: true,
            itemBaseArray: [],
            expandedLineItems: {},

            // dynamic Header
            setLvlGetHeaderConfig: [],
            setLvlFixedHeader: [],
            setLvlCustomHeaders: {},
            setLvlHeaderConfigState: {},
            setLvlHeaderConfigDataState: {},
            setLvlFixedHeaderData: [],
            setLvlCustomHeadersState: [],
            setLvlHeaderState: {},
            setLvlHeaderSummary: [],
            setLvlDefaultHeaderMap: [],
            setLvlConfirmModal: false,
            setLvlHeaderMsg: "",
            setLvlParaMsg: "",
            setLvlHeaderCondition: false,
            setLvlSaveState: [],
            setLvlColoumSetting: false,
            mandateHeaderSet : [],

            editRequestQuantity: false,
            expandedItem:[],
        }
    }
    componentDidMount() {
        let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            basedOn: "SET"
        }
        this.props.getSetHeaderConfigRequest(payload)
        this.props.onRef(this);
    }
    componentWillUnmount(){
        this.props.onRef(undefined)
    }
    /* componentWillReceiveProps(nextProps) {
        if (nextProps.logistic.getAllVendorSsdetail.isSuccess) {
            if (nextProps.logistic.getAllVendorSsdetail.data.resource != null && nextProps.logistic.getAllVendorSsdetail.data.basedOn == "set") {
                this.setState({
                    poDetailsShipment: nextProps.logistic.getAllVendorSsdetail.data.resource,
                    prev: nextProps.logistic.getAllVendorSsdetail.data.prePage,
                    current: nextProps.logistic.getAllVendorSsdetail.data.currPage,
                    next: nextProps.logistic.getAllVendorSsdetail.data.currPage + 1,
                    maxPage: nextProps.logistic.getAllVendorSsdetail.data.maxPage,
                })
            } else {
                this.setState({
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "SET") {
                let setLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let setLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let setLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                this.setState({
                    setLvlCustomHeaders: this.state.setLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    setLvlGetHeaderConfig,
                    setLvlFixedHeader,
                    setLvlCustomHeadersState,
                    setLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    setLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    setLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    setLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    setLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                })
            }
        }
            if (nextProps.replenishment.createHeaderConfig.isSuccess && nextProps.replenishment.createHeaderConfig.data.basedOn == "SET") {
                let payload = {
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                    attributeType: "TABLE HEADER",
                    displayName: this.props.set_Display,
                    basedOn: "SET"
                }
                this.props.getHeaderConfigRequest(payload)
            }
        
        if (nextProps.logistic.getAllVendorSsdetail.data.resource != null && nextProps.logistic.getAllVendorSsdetail.data.basedOn == "Item") {
            this.setState({
                itemBaseArray: nextProps.logistic.getAllVendorSsdetail.data.resource.poDetails
            })
        }

    } */
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "SET") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
                basedOn: "SET"
            }
            this.props.getSetHeaderConfigRequest(payload)
        }
        if (this.props.shipment.updateVendorShipment.isSuccess) {
            let successObj = {};
            successObj.successMessage = this.props.shipment.updateVendorShipment.data.message
            successObj.success = true
            this.props.openLoader(false)
            this.props.openSuccess(successObj)
            this.props.updateVendorShipmentClear()
        } else if (this.props.shipment.updateVendorShipment.isError) {
            let errorObj = {};
            errorObj.errorMessage = this.props.shipment.updateVendorShipment.message.error.errorMessage
            successObj.alert = true
            this.props.openError(errorObj)
            this.props.openLoader(false)
            this.props.updateVendorShipmentClear()
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.logistic.getAllVendorSsdetail.isSuccess) {
            if (nextProps.logistic.getAllVendorSsdetail.data.resource != null && nextProps.logistic.getAllVendorSsdetail.data.basedOn == "set") {
                return {
                    poDetailsShipment: nextProps.logistic.getAllVendorSsdetail.data.resource,
                    prev: nextProps.logistic.getAllVendorSsdetail.data.prePage,
                    current: nextProps.logistic.getAllVendorSsdetail.data.currPage,
                    next: nextProps.logistic.getAllVendorSsdetail.data.currPage + 1,
                    maxPage: nextProps.logistic.getAllVendorSsdetail.data.maxPage,
                    expandedItem: nextProps.logistic.getAllVendorSsdetail.data.resource.poDetails
                }
            } else {
                return {
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                }
            }
        }
        if (nextProps.logistic.getAllVendorSsdetail.data.resource != null && nextProps.logistic.getAllVendorSsdetail.data.basedOn == "Item") {
            return {
                itemBaseArray: nextProps.logistic.getAllVendorSsdetail.data.resource.poDetails,
            }
        }
        
        return {}
    }
    searchShipment = (e) => {
        if (e.target.value.trim().length) {
            if (this.state.search == "" && e.keyCode == 13) {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 10000)
            } else {
                this.setState({ type: 3 })
                let payload = {
                    no: 1,
                    type: 3,
                    userType: "entlogi",
                    search: this.state.search,
                    poId: this.props.expandChecked[0].poId,
                    shipmentId: this.props.expandChecked[0].shipmentId,
                    orderCode: this.props.order.orderCode,
                    orderNumber: this.props.order.orderNumber
                }
                this.props.getAllVendorSsdetailRequest(payload)
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.type == 3 && this.state.search == "") {
                let data = {
                    no: 1,
                    type: 1,
                    search: "",
                    userType: "entlogi",
                    poId: this.props.expandChecked[0].poId,
                    shipmentId: this.props.expandChecked[0].shipmentId,
                    orderCode: this.props.order.orderCode,
                    orderNumber: this.props.order.orderNumber
                }
                this.props.getAllVendorSsdetailRequest(data)
                this.setState({ type: 1 })
            }
        })
    }
    searchClear = () => {
        if (this.state.type == 3) {
            let data = {
                no: 1,
                type: 1,
                search: "",
                userType: "entlogi",
                poId: this.props.expandChecked[0].poId,
                shipmentId: this.props.expandChecked[0].shipmentId,
                orderCode: this.props.order.orderCode,
                orderNumber: this.props.order.orderNumber
            }
            this.props.getAllVendorSsdetailRequest(data)
            this.setState({ type: 1, search: "" })
        } else {
            this.setState({ search: "" })
        }
    }

    expandColumn(id, e, data) {
        if (!this.state.actionExpand || this.state.prevId !== e.target.id) {
            let payload = {
                userType: "entlogi",
                shipmentId: this.props.shipmentId,
                orderId : this.props.orderId ,
                detailType: "Item",
                setHeaderId: data.setHeaderId,
                basedOn: "Item"
            }
            console.log(payload)
            this.props.getItemHeaderConfigRequest(this.props.itemHeaderPayload)
            this.props.getAllVendorSsdetailRequest(payload)
            this.setState({ actionExpand: true, prevId: e.target.id, expandedId: id, dropOpen: true, order: { orderCode: data.orderCode, orderNumber: data.orderNumber } },
            )
        }
        else {
            this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
        }
    }

    // Dynamic Header
    setLvlOpenColoumSetting(data) {
        if (this.state.setLvlCustomHeadersState.length == 0) {
            this.setState({
                setLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                setLvlColoumSetting: true
            })
        } else {
            this.setState({
                setLvlColoumSetting: false
            })
        }
    }
    setLvlPushColumnData(data) {
        console.log("in set level push")
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlHeaderConfigDataState = this.state.setLvlHeaderConfigDataState
        let setLvlCustomHeaders = this.state.setLvlCustomHeaders
        let setLvlSaveState = this.state.setLvlSaveState
        let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
        let setLvlHeaderSummary = this.state.setLvlHeaderSummary
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (this.state.setLvlHeaderCondition) {

            if (!data.includes(setLvlGetHeaderConfig) || setLvlGetHeaderConfig.length == 0) {
                setLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(setLvlHeaderConfigDataState))) {
                    let invert = _.invert(setLvlFixedHeaderData)
                    let keyget = invert[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }
                if (!Object.keys(setLvlCustomHeaders).includes(setLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];
                    setLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(setLvlCustomHeadersState) || setLvlCustomHeadersState.length == 0) {
                setLvlCustomHeadersState.push(data)
                if (!setLvlCustomHeadersState.includes(setLvlHeaderConfigDataState)) {
                    let keyget = (_.invert(setLvlFixedHeaderData))[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }
                if (!Object.keys(setLvlCustomHeaders).includes(setLvlHeaderSummary)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];
                    setLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeadersState,
            setLvlCustomHeaders,
            setLvlSaveState,
            setLvlDefaultHeaderMap,
            setLvlHeaderSummary
        })
    }
    setLvlCloseColumn(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlHeaderConfigState = this.state.setLvlHeaderConfigState
        let setLvlCustomHeaders = []
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (!this.state.setLvlHeaderCondition) {
            for (let j = 0; j < setLvlCustomHeadersState.length; j++) {
                if (data == setLvlCustomHeadersState[j]) {
                    setLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlCustomHeadersState.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
            if (this.state.setLvlCustomHeadersState.length == 0) {
                this.setState({
                    setLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < setLvlGetHeaderConfig.length; i++) {
                if (data == setLvlGetHeaderConfig[i]) {
                    setLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlGetHeaderConfig.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
        }
        setLvlCustomHeaders.forEach(e => delete setLvlHeaderConfigState[e]);
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeaders: setLvlHeaderConfigState,
            setLvlCustomHeadersState,
        })
        setTimeout(() => {     // minValue,
            // maxValue
            let keygetvalue = (_.invert(this.state.setLvlFixedHeaderData))[data];     // minValue,
            // maxValue
            let setLvlSaveState = this.state.setLvlSaveState
            setLvlSaveState.push(keygetvalue)
            let setLvlHeaderSummary = this.state.setLvlHeaderSummary
            let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
            if (!this.state.setLvlHeaderCondition) {
                for (let j = 0; j < setLvlHeaderSummary.length; j++) {
                    if (keygetvalue == setLvlHeaderSummary[j]) {
                        setLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < setLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == setLvlDefaultHeaderMap[i]) {
                        setLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                setLvlHeaderSummary,
                setLvlDefaultHeaderMap,
                setLvlSaveState
            })
        }, 100);
    }
    setLvlsaveColumnSetting(e) {
        this.setState({
            setLvlColoumSetting: false,
            setLvlHeaderCondition: false,
            setLvlSaveState: []
        })
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "LOGISTIC",
            section: "LR-PROCESSING",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    setLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            setLvlConfirmModal: true,
        })
    }
    setLvlResetColumn() {
        console.log("in set level reset")
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "LOGISTIC",
            section: "LR-PROCESSING",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            setLvlHeaderCondition: true,
            setLvlColoumSetting: false,
            setLvlSaveState: []
        })
    }
    setLvlCloseConfirmModal(e) {
        this.setState({
            setLvlConfirmModal: !this.state.setLvlConfirmModal,
        })
    }

    openEditRequestQuantity  = () =>  {
        this.setState({
            editRequestQuantity: !this.state.editRequestQuantity },()=>{
                this.props.showSaveChanges(this.state.editRequestQuantity);
            });
    }

    handleQuantityChange(e, data) {
        var expandedItem = [...this.state.expandedItem]
        var indexId = this.props.poType == "poicode" ? "orderDetailId" : "setBarCode"
        let index = expandedItem.findIndex((obj => obj[indexId] == data[indexId]));
        expandedItem[index].initialRequestedQty = this.props.poType == "poicode" ? expandedItem[index].requestQty : expandedItem[index].shipmentRequestedQty
        if (e.target.id == "requestedQty") {
            let value = e.target.value == "" ? 0 : Number(e.target.value);
            let cancelQty = Number(expandedItem[index].initialRequestedQty) - value;
            if( value >= 0 && cancelQty >= 0 ){
                expandedItem[index].requestedQty = value;
                expandedItem[index].cancelQty = cancelQty;
                this.setState({ expandedItem})
                this.props.showSaveChanges(true);
            }
            if(value == 0)
              this.props.showSaveChanges(false);
        }
    }

    finalEditRequestQty =()=> {
        var changelineitems = this.state.expandedItem.map((item) => {
            return {
                orderDetailId: item.orderDetailId == undefined ? 0 : item.orderDetailId,
                setHeaderId: item.setHeaderId == undefined ? 0 : item.setHeaderId,
                requestedQty: item.requestedQty == undefined ? (item.shipmentRequestedQty !== undefined ? item.shipmentRequestedQty : item.requestQty) : item.requestedQty,
                orderQty: item.totalNoOfSet == undefined ? item.orderQty : item.totalNoOfSet,
                pendingQty: item.shipmentPendingQty == undefined ? item.totalPendingQty : item.shipmentPendingQty,
                cancelQty: item.cancelQty == undefined ? (item.shipmentCancelledQty !== undefined ? item.shipmentCancelledQty : item.totalCancelledQty) : item.cancelQty,
            }
        })
        let payload = {
            shipmentId: this.props.shipmentId,
            poType: this.props.poType,
            vendorName: this.props.vendorName,
            poDetails: changelineitems, 
            editRequestQty: true,          
        }
        this.props.updateVendorShipmentRequest(payload)
        this.setState({ editRequestQuantity: false})
        this.props.openLoader(true)  
    }

    render() {
        return (
            <div className="gen-vend-sticky-table">
                {/* ---------------------------------------expanded Table----------------------------- */}
                <div className="gvst-expend" id="expandedTable" >
                    {/* <SetLevelConfigHeader {...this.props} {...this.state} setLvlColoumSetting={this.state.setLvlColoumSetting} setLvlGetHeaderConfig={this.state.setLvlGetHeaderConfig} setLvlResetColumnConfirmation={(e) => this.setLvlResetColumnConfirmation(e)} setLvlOpenColoumSetting={(e) => this.setLvlOpenColoumSetting(e)} setLvlCloseColumn={(e) => this.setLvlCloseColumn(e)} setLvlPushColumnData={(e) => this.setLvlPushColumnData(e)} setLvlsaveColumnSetting={(e) => this.setLvlsaveColumnSetting(e)} />
                     */}
                    <div className="gvst-inner">
                        <table className="table">
                            <thead>
                                <React.Fragment>
                                    <tr>
                                        <th className="fix-action-btn">
                                            <ul className="rab-refresh">
                                                { this.props.status === "SHIPMENT_INVOICE_REQUESTED" && <li className="til-inner til-edit-btn" onClick={this.openEditRequestQuantity}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>}
                                            </ul>
                                        </th>
                                        {/* {this.state.setLvlCustomHeadersState.length == 0 ? this.state.setLvlGetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.state.setLvlCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))} */}
                                        {this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            )) : this.props.setCustomHeadersState.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            ))}
                                    </tr>
                                </React.Fragment>
                            </thead>
                            <tbody>
                                {this.state.poDetailsShipment.length != 0 || this.state.poDetailsShipment.poDetails != undefined ?
                                    this.state.poDetailsShipment.poDetails.map((data, key) => (
                                        <React.Fragment key={key}>
                                            <tr>
                                                <td className="fix-action-btn">
                                                    {this.props.poType != "poicode" && <ul className="table-item-list">
                                                        <li className="til-inner til-add-btn" id={data.setBarCode} onClick={(e) => this.expandColumn(data.setBarCode, e, data)}>
                                                            {this.state.dropOpen && this.state.expandedId == data.setBarCode ? 
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                <path fill="#a4b9dd" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"/>
                                                            </svg> : 
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                <path fill="#a4b9dd" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                                            </svg>}
                                                        </li>
                                                    </ul>}
                                                </td>
                                                {/* {this.state.setLvlHeaderSummary.length == 0 ? this.state.setLvlDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        {<label>{data[hdata]}</label>}
                                                    </td>
                                                )) : this.state.setLvlHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        {<label>{data[sdata]}</label>}
                                                    </td>
                                                ))}  */}
                                                {this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                                        <td key={key} >
                                                            {((hdata == "shipmentRequestedQty" || hdata == "requestQty") && this.props.status === "SHIPMENT_INVOICE_REQUESTED") ? <input type="text" disabled={!this.state.editRequestQuantity} className={!this.state.editRequestQuantity ? "bgDisable" : ""} value={data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                hdata == "shipmentCancelledQty" || hdata == "cancelQty" ? <label>{data["cancelQty"] != undefined ? data["cancelQty"] : data[hdata]}</label> :
                                                                    <label>{data[hdata]}</label>}
                                                        </td>
                                                    )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                                        <td key={keyy} >
                                                            {((sdata == "shipmentRequestedQty" || sdata == "requestQty") && this.props.status === "SHIPMENT_INVOICE_REQUESTED") ? <input type="text" disabled={!this.state.editRequestQuantity} className={!this.state.editRequestQuantity ? "bgDisable" : ""} value={data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                sdata == "shipmentCancelledQty" || sdata == "cancelQty" ? <label>{data["cancelQty"] != undefined ? data["cancelQty"] : data[hdata]}</label> :
                                                                    <label>{data[sdata]}</label>}
                                                        </td>
                                                    ))}                                               </tr>
                                            {this.state.dropOpen && this.state.expandedId == data.setBarCode ? <tr><td colSpan="100%" className="pad-0"> <ItemLevelDetails {...this.state}  {...this.props} item_Display={this.props.item_Display} /> </td></tr> : null}
                                        </React.Fragment>
                                    ))
                                    : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                            </tbody>
                        </table>
                    </div>
                    {/* <div className="pagerDiv newPagination text-right justifyEnd alignMiddle displayFlex pad-top-20 expandPagination">
                        <Pagination {...this.state} {...this.props} page={this.page}
                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                    </div> */}
                    {this.state.setLvlConfirmModal ? <SetLevelConfirmationModal {...this.state} {...this.props} setLvlCloseConfirmModal={(e) => this.setLvlCloseConfirmModal(e)} setLvlResetColumn={(e) => this.setLvlResetColumn(e)} /> : null}
                    {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
                </div>
            </div>
        )
    }
}