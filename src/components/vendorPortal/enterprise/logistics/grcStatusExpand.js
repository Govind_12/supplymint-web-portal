import React, { Component } from 'react';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';
import searchIcon from '../../../../assets/close-recently.svg';
import plusIcon from '../../../../assets/plus-blue.svg';
import removeIcon from '../../../../assets/removeIcon.svg';
import ItemLevelDetails from '../../modals/itemLevelDetails';
import SetLevelConfigHeader from '../../setLevelConfigHeader';
import ConfirmationSummaryModal from '../../../replenishment/confirmationReset';
import SetLevelConfirmationModal from '../../setLevelConfirModal';

export default class GateEntryExpand extends Component {
    constructor(props) {
        super(props);
        this.state = {
            poDetailsShipment: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            toastLoader: false,
            toastMsg: "",
            expandedId: "",
            dropOpen: true,
            itemBaseArray: [],
            expandedLineItems: {},

            // dynamic Header
            setLvlGetHeaderConfig: [],
            setLvlFixedHeader: [],
            setLvlCustomHeaders: {},
            setLvlHeaderConfigState: {},
            setLvlHeaderConfigDataState: {},
            setLvlFixedHeaderData: [],
            setLvlCustomHeadersState: [],
            setLvlHeaderState: {},
            setLvlHeaderSummary: [],
            setLvlDefaultHeaderMap: [],
            setLvlConfirmModal: false,
            setLvlHeaderMsg: "",
            setLvlParaMsg: "",
            setLvlHeaderCondition: false,
            setLvlSaveState: [],
            setLvlColoumSetting: false,
            mandateHeaderSet: []
        }
    }
    componentDidMount() {
        let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            basedOn: "SET"
        }
        this.props.getSetHeaderConfigRequest(payload)
    }
    /* componentWillReceiveProps(nextProps) {
        if (nextProps.logistic.getGrcDetails.isSuccess) {
            if (nextProps.logistic.getGrcDetails.data.resource != null) {
                console.log(nextProps.logistic.getGrcDetails)
                this.setState({
                    poDetailsShipment: nextProps.logistic.getGrcDetails.data.resource,
                    prev: nextProps.logistic.getGrcDetails.data.prePage,
                    current: nextProps.logistic.getGrcDetails.data.currPage,
                    next: nextProps.logistic.getGrcDetails.data.currPage + 1,
                    maxPage: nextProps.logistic.getGrcDetails.data.maxPage,
                })
            } else {
                this.setState({
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "SET") {
                let setLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let setLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let setLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                this.setState({
                    setLvlCustomHeaders: this.state.setLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    setLvlGetHeaderConfig,
                    setLvlFixedHeader,
                    setLvlCustomHeadersState,
                    setLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    setLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    setLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    setLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    setLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                })
            }
        }
        if (nextProps.replenishment.createHeaderConfig.isSuccess && nextProps.replenishment.createHeaderConfig.data.basedOn == "SET") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.set_Display,
                basedOn: "SET"
            }
            this.props.getHeaderConfigRequest(payload)
        }

        if (nextProps.logistic.getGrcDetails.data.resource != null && nextProps.logistic.getGrcDetails.data.basedOn == "Item") {
            this.setState({
                itemBaseArray: nextProps.logistic.getGrcDetails.data.resource.poDetails
            })
        }

    } */
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "SET") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
                basedOn: "SET"
            }
            this.props.getSetHeaderConfigRequest(payload)
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.logistic.getGrcDetails.isSuccess) {
            if (nextProps.logistic.getGrcDetails.data.resource != null) {
                return {
                    poDetailsShipment: nextProps.logistic.getGrcDetails.data.resource,
                    prev: nextProps.logistic.getGrcDetails.data.prePage,
                    current: nextProps.logistic.getGrcDetails.data.currPage,
                    next: nextProps.logistic.getGrcDetails.data.currPage + 1,
                    maxPage: nextProps.logistic.getGrcDetails.data.maxPage,
                }
            } else {
                return {
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                }
            }
        }
        if (nextProps.logistic.getGrcDetails.data.resource != null && nextProps.logistic.getGrcDetails.data.basedOn == "Item") {
            return {
                itemBaseArray: nextProps.logistic.getGrcDetails.data.resource.poDetails
            }
        }
        return {}
    }
    searchShipment = (e) => {
        if (e.target.value.trim().length) {
            if (this.state.search == "" && e.keyCode == 13) {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 10000)
            } else {
                this.setState({ type: 3 })
                let payload = {
                    no: 1,
                    type: 3,
                    userType: "entlogi",
                    search: this.state.search,
                    poId: this.props.expandChecked[0].poId,
                    shipmentId: this.props.expandChecked[0].shipmentId,
                    orderCode: this.props.order.orderCode,
                    orderNumber: this.props.order.orderNumber
                }
                this.props.getGrcDetailsRequest(payload)
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.type == 3 && this.state.search == "") {
                let data = {
                    no: 1,
                    type: 1,
                    search: "",
                    userType: "entlogi",
                    poId: this.props.expandChecked[0].poId,
                    shipmentId: this.props.expandChecked[0].shipmentId,
                    orderCode: this.props.order.orderCode,
                    orderNumber: this.props.order.orderNumber
                }
                this.props.getGrcDetailsRequest(data)
                this.setState({ type: 1 })
            }
        })
    }
    searchClear = () => {
        if (this.state.type == 3) {
            let data = {
                no: 1,
                type: 1,
                search: "",
                userType: "entlogi",
                poId: this.props.expandChecked[0].poId,
                shipmentId: this.props.expandChecked[0].shipmentId,
                orderCode: this.props.order.orderCode,
                orderNumber: this.props.order.orderNumber
            }
            this.props.getGrcDetailsRequest(data)
            this.setState({ type: 1, search: "" })
        } else {
            this.setState({ search: "" })
        }
    }

    // Dynamic Header
    setLvlOpenColoumSetting(data) {
        if (this.state.setLvlCustomHeadersState.length == 0) {
            this.setState({
                setLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                setLvlColoumSetting: true
            })
        } else {
            this.setState({
                setLvlColoumSetting: false
            })
        }
    }
    setLvlPushColumnData(data) {
        console.log("in set level push")
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlHeaderConfigDataState = this.state.setLvlHeaderConfigDataState
        let setLvlCustomHeaders = this.state.setLvlCustomHeaders
        let setLvlSaveState = this.state.setLvlSaveState
        let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
        let setLvlHeaderSummary = this.state.setLvlHeaderSummary
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (this.state.setLvlHeaderCondition) {

            if (!data.includes(setLvlGetHeaderConfig) || setLvlGetHeaderConfig.length == 0) {
                setLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(setLvlHeaderConfigDataState))) {
                    let invert = _.invert(setLvlFixedHeaderData)
                    let keyget = invert[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }
                if (!Object.keys(setLvlCustomHeaders).includes(setLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];
                    setLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(setLvlCustomHeadersState) || setLvlCustomHeadersState.length == 0) {
                setLvlCustomHeadersState.push(data)
                if (!setLvlCustomHeadersState.includes(setLvlHeaderConfigDataState)) {
                    let keyget = (_.invert(setLvlFixedHeaderData))[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }
                if (!Object.keys(setLvlCustomHeaders).includes(setLvlHeaderSummary)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];
                    setLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeadersState,
            setLvlCustomHeaders,
            setLvlSaveState,
            setLvlDefaultHeaderMap,
            setLvlHeaderSummary
        })
    }
    setLvlCloseColumn(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlHeaderConfigState = this.state.setLvlHeaderConfigState
        let setLvlCustomHeaders = []
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (!this.state.setLvlHeaderCondition) {
            for (let j = 0; j < setLvlCustomHeadersState.length; j++) {
                if (data == setLvlCustomHeadersState[j]) {
                    setLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlCustomHeadersState.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
            if (this.state.setLvlCustomHeadersState.length == 0) {
                this.setState({
                    setLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < setLvlGetHeaderConfig.length; i++) {
                if (data == setLvlGetHeaderConfig[i]) {
                    setLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlGetHeaderConfig.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
        }
        setLvlCustomHeaders.forEach(e => delete setLvlHeaderConfigState[e]);
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeaders: setLvlHeaderConfigState,
            setLvlCustomHeadersState,
        })
        setTimeout(() => {     // minValue,
            // maxValue
            let keygetvalue = (_.invert(this.state.setLvlFixedHeaderData))[data];     // minValue,
            // maxValue
            let setLvlSaveState = this.state.setLvlSaveState
            setLvlSaveState.push(keygetvalue)
            let setLvlHeaderSummary = this.state.setLvlHeaderSummary
            let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
            if (!this.state.setLvlHeaderCondition) {
                for (let j = 0; j < setLvlHeaderSummary.length; j++) {
                    if (keygetvalue == setLvlHeaderSummary[j]) {
                        setLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < setLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == setLvlDefaultHeaderMap[i]) {
                        setLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                setLvlHeaderSummary,
                setLvlDefaultHeaderMap,
                setLvlSaveState
            })
        }, 100);
    }
    setLvlsaveColumnSetting(e) {
        this.setState({
            setLvlColoumSetting: false,
            setLvlHeaderCondition: false,
            setLvlSaveState: []
        })
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "LOGISTIC",
            section: "LR-PROCESSING",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    setLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            setLvlConfirmModal: true,
        })
    }
    setLvlResetColumn() {
        console.log("in set level reset")
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "LOGISTIC",
            section: "LR-PROCESSING",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            setLvlHeaderCondition: true,
            setLvlColoumSetting: false,
            setLvlSaveState: []
        })
    }
    setLvlCloseConfirmModal(e) {
        this.setState({
            setLvlConfirmModal: !this.state.setLvlConfirmModal,
        })
    }

    render() {
        return (
            <div className="gen-vend-sticky-table">
                <div className="gvst-expend">
                    {/* -----------------------------------------------------------expanded Table----------------------------- */}
                    {/* <SetLevelConfigHeader {...this.props} {...this.state} setLvlColoumSetting={this.state.setLvlColoumSetting} setLvlGetHeaderConfig={this.state.setLvlGetHeaderConfig} setLvlResetColumnConfirmation={(e) => this.setLvlResetColumnConfirmation(e)} setLvlOpenColoumSetting={(e) => this.setLvlOpenColoumSetting(e)} setLvlCloseColumn={(e) => this.setLvlCloseColumn(e)} setLvlPushColumnData={(e) => this.setLvlPushColumnData(e)} setLvlsaveColumnSetting={(e) => this.setLvlsaveColumnSetting(e)} />
                     */}
                    <div className="gvst-inner">
                        <table className="table">
                            <thead>
                                <React.Fragment>
                                    <tr>
                                        <th className="fix-action-btn"><label></label></th>
                                        {/* {this.state.setLvlCustomHeadersState.length == 0 ? this.state.setLvlGetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.state.setLvlCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))} */}
                                        
                                        {this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.props.setCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))
                                        }
                                    </tr>
                                </React.Fragment>
                            </thead>
                            <tbody>
                                {this.state.poDetailsShipment.length != 0 || this.state.poDetailsShipment != undefined ?
                                    this.state.poDetailsShipment.map((data, key) => (
                                        <React.Fragment key={key}>
                                            <tr>
                                                <td className="fix-action-btn">
                                                    {/* {this.props.poType != "poicode" && <img src={this.state.dropOpen && this.state.expandedId == data.setBarCode ? removeIcon : plusIcon} className="imgHeight displayPointer" id={data.setBarCode} onClick={(e) => this.expandColumn(data.setBarCode, e, data)} />} */}
                                                </td>
                                                {/* {this.state.setLvlHeaderSummary.length == 0 ? this.state.setLvlDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        {<label>{data[hdata]}</label>}
                                                    </td>
                                                )) : this.state.setLvlHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        {<label>{data[sdata]}</label>}
                                                    </td>
                                                ))} */}       
                                                {
                                                this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                    {<label>{data[hdata]}</label>}
                                                </td>
                                                )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                    {<label>{data[sdata]}</label>}
                                                </td>
                                                ))
                                                }                                        </tr>
                                            {/* {this.state.dropOpen && this.state.expandedId == data.setBarCode ? <tr><td colSpan="100%" className="pad-0"> <ItemLevelDetails {...this.state}  {...this.props} item_Display={this.props.item_Display} /> </td></tr> : null} */}
                                        </React.Fragment>
                                    ))
                                    : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                            </tbody>
                        </table>
                        {/* <div className="pagerDiv newPagination text-right justifyEnd alignMiddle displayFlex pad-top-20 expandPagination">
                            <Pagination {...this.state} {...this.props} page={this.page}
                                prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                        </div> */}
                        {this.state.setLvlConfirmModal ? <SetLevelConfirmationModal {...this.state} {...this.props} setLvlCloseConfirmModal={(e) => this.setLvlCloseConfirmModal(e)} setLvlResetColumn={(e) => this.setLvlResetColumn(e)} /> : null}

                    </div>
                    {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
                    {/* -----------------------------------------------------------expanded Table End----------------------------- */}
                </div>
            </div>
        )
    }
}