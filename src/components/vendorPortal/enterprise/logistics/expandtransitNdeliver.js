import React, { Component } from 'react';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';
import searchIcon from '../../../../assets/close-recently.svg';
export default class ExpandTransitNDeliver extends Component {
    constructor(props) {
        super(props);
        this.state = {
            poDetailsShipment: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            toastLoader: false,
            toastMsg: ""
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.logistic.getAllVendorSiDetail.isSuccess) {
            if (nextProps.logistic.getAllVendorSiDetail.data.resource != null) {
                this.setState({
                    poDetailsShipment: nextProps.logistic.getAllVendorSiDetail.data.resource,
                    prev: nextProps.logistic.getAllVendorSiDetail.data.prePage,
                    current: nextProps.logistic.getAllVendorSiDetail.data.currPage,
                    next: nextProps.logistic.getAllVendorSiDetail.data.currPage + 1,
                    maxPage: nextProps.logistic.getAllVendorSiDetail.data.maxPage,
                })
            } else {
                this.setState({
                    poDetailsShipment: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
    }

    // page = (e) => {
    //     if (e.target.id == "prev") {
    //         if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
    //         } else {

    //             this.setState({
    //                 prev: this.props.logistic.getAllVendorSiDetail.data.prePage,
    //                 current: this.props.logistic.getAllVendorSiDetail.data.currPage,
    //                 next: this.props.logistic.getAllVendorSiDetail.data.currPage + 1,
    //                 maxPage: this.props.logistic.getAllVendorSiDetail.data.maxPage,
    //             })
    //             if (this.props.logistic.getAllVendorSiDetail.data.currPage != 0) {
    //                 let data = {
    //                     no: this.props.logistic.getAllVendorSiDetail.data.currPage - 1,
    //                     type: this.state.type,
    //                     search: this.state.search,
    //                     userType: this.props.userType,
    //                     lgtNumber: this.props.expandChecked[0].lgtNumber,

    //                 }
    //                 this.props.getAllVendorSiDetailRequest(data)
    //             }
    //         }
    //     } else if (e.target.id == "next") {
    //         this.setState({
    //             prev: this.props.logistic.getAllVendorSiDetail.data.prePage,
    //             current: this.props.logistic.getAllVendorSiDetail.data.currPage,
    //             next: this.props.logistic.getAllVendorSiDetail.data.currPage + 1,
    //             maxPage: this.props.logistic.getAllVendorSiDetail.data.maxPage,
    //         })
    //         if (this.props.logistic.getAllVendorSiDetail.data.currPage != this.props.logistic.getAllVendorSiDetail.data.maxPage) {
    //             let data = {
    //                 no: this.props.logistic.getAllVendorSiDetail.data.currPage + 1,
    //                 type: this.state.type,
    //                 userType: this.props.userType,
    //                 search: this.state.search,
    //                 lgtNumber: this.props.expandChecked[0].lgtNumber,

    //             }
    //             this.props.getAllVendorSiDetailRequest(data)
    //         }
    //     }
    //     else if (e.target.id == "first") {
    //         if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

    //         }
    //         else {
    //             this.setState({
    //                 prev: this.props.logistic.getAllVendorSsdetail.data.prePage,
    //                 current: this.props.logistic.getAllVendorSsdetail.data.currPage,
    //                 next: this.props.logistic.getAllVendorSsdetail.data.currPage + 1,
    //                 maxPage: this.props.logistic.getAllVendorSsdetail.data.maxPage,
    //             })
    //             if (this.props.logistic.getAllVendorSsdetail.data.currPage <= this.props.logistic.getAllVendorSsdetail.data.maxPage) {
    //                 let data = {
    //                     no: 1,
    //                     type: this.state.type,
    //                     search: this.state.search,
    //                     userType: this.props.userType,
    //                     lgtNumber: this.props.expandChecked[0].lgtNumber,

    //                 }
    //                 this.props.getAllVendorSsdetailRequest(data)
    //             }
    //         }

    //     } else if (e.target.id == "last") {
    //         if (this.state.current == this.state.maxPage || this.state.current == undefined) {

    //         }
    //         else {
    //             this.setState({
    //                 prev: this.props.logistic.getAllVendorSsdetail.data.prePage,
    //                 current: this.props.logistic.getAllVendorSsdetail.data.currPage,
    //                 next: this.props.logistic.getAllVendorSsdetail.data.currPage + 1,
    //                 maxPage: this.props.logistic.getAllVendorSsdetail.data.maxPage,
    //             })
    //             if (this.props.logistic.getAllVendorSiDetail.data.currPage <= this.props.logistic.getAllVendorSiDetail.data.maxPage) {
    //                 let data = {
    //                     no: this.props.logistic.getAllVendorSiDetail.data.maxPage,
    //                     type: this.state.type,
    //                     userType: this.props.userType,
    //                     search: this.state.search,
    //                     lgtNumber: this.props.expandChecked[0].lgtNumber,

    //                 }
    //                 this.props.getAllVendorSiDetailRequest(data)
    //             }
    //         }
    //     }
    // }

    searchShipment = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                if (this.state.search == "") {
                    this.setState({
                        toastMsg: "select data",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 10000)
                } else {
                    this.setState({ type: 3 })
                    let payload = {
                        no: 1,
                        type: 3,
                        userType: this.props.userType,
                        search: this.state.search,
                        lgtNumber: this.props.expandChecked[0].lgtNumber,
                    }
                    this.props.getAllVendorSiDetailRequest(payload)
                }
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.type == 3 && this.state.search == "") {
                let data = {
                    no: 1,
                    type: 1,
                    search: "",
                    userType: this.props.userType,
                    lgtNumber: this.props.expandChecked[0].lgtNumber,    
                }
                this.props.getAllVendorSiDetailRequest(data)
                this.setState({ type: 1 })
            }
        })
    }
    searchClear = () => {
        if (this.state.type == 3) {
            let data = {
                no: 1,
                type: 1,
                search: "",
                userType: this.props.userType,
                lgtNumber: this.props.expandChecked[0].lgtNumber,
            }
            this.props.getAllVendorSiDetailRequest(data)
            this.setState({ type: 1, search: "" })
        } else {
            this.setState({ search: "" })
        }
    }

    render() {
        return (
            <div className="container-fluid">

                {/* -----------------------------------------------------------expanded Table----------------------------- */}

                <div className="col-md-12 col-sm-12 col-xs-12  tableGeneric bordere3e7f3 mainRoleTable bgWhite vendorTable cancelPoTable" id="expandedTable" >
                    <div className="col-md-12">
                        <div className="col-md-8"></div>
                        <div className="col-md-4 text-right newSearch">
                            <input type="search" className="searchWid" value={this.state.search} onChange={this.searchShipment} onKeyDown={this.searchShipment} placeholder="Search..." />
                            {this.state.search != "" ? <span className="closeSearch right4"><img src={searchIcon} onClick={this.searchClear} /></span> : null}
                        </div>
                    </div>
                    <div className="col-md-12 m-top-10">
                        <div className="zui-wrapper oflxAuto">
                            <table className="table zui-table roleTable border-bot-table">
                                <thead>
                                    <tr>
                                        <th><label>ASN No.</label></th>
                                        <th><label> ASN Date</label></th>
                                        <th><label>PO Number</label></th>
                                        <th><label>Order Code</label></th>
                                        <th><label>PO Date</label></th>
                                        <th><label>Due Date</label> </th>
                                        <th><label>Total Qty</label> </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.poDetailsShipment.length != 0 ?
                                        this.state.poDetailsShipment.map((data, key) => (
                                            <tr key={key}>
                                                <td><label>{data.shipmentAdviceNo}</label></td>
                                                <td><label >{data.shipmentAdviceDate} </label></td>
                                                <td><label >{data.poNumber} </label> </td>
                                                <td><label >{data.orderCode} </label> </td>
                                                <td> <label className="textBreak">{data.poDate}</label></td>
                                                <td><label>{data.dueDate}</label> </td>
                                                <td> <label> {data.totalQty} </label></td>

                                            </tr>
                                        ))
                                        : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {/* <div className="pagerDiv newPagination text-right justifyEnd alignMiddle displayFlex pad-top-20 expandPagination">
                        <Pagination {...this.state} {...this.props} page={this.page}
                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                    </div> */}
                </div>
                {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
                {/* -----------------------------------------------------------expanded Table End----------------------------- */}
            </div>
        )
    }
}