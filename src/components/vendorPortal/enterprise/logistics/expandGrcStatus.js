import React, { Component } from 'react';
import FilterIcon from '../../../../assets/headerFilter.svg';

export default class ExpandGrcStatus extends Component {
    render() {
        return (
            <div>
                <div className="expand-grc-status-sticky">
                    <div className="egss-inner">
                        <div className="egss-manage-table">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th><label>ICode</label><img src={FilterIcon} /></th>
                                        <th><label>Icode Qty</label><img src={FilterIcon} /></th>
                                        <th><label>Inwarded Qty</label><img src={FilterIcon} /></th>
                                        <th><label>Rejected Qty</label><img src={FilterIcon} /></th>
                                        <th><label>Pending Qty</label><img src={FilterIcon} /></th>
                                        <th><label>Remarks</label><img src={FilterIcon} /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label>DS829389</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                        <td><label>230</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>DS829389</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                        <td><label>230</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>DS829389</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                        <td><label>230</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>DS829389</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                        <td><label>230</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>DS829389</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                        <td><label>230</label></td>
                                        <td><label>230</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}