import React from "react";

class FilterShipmentShipped extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 1,
            no: 1,
            shipmentRequestDate: this.props.shipmentRequestDate,
            poNumber: this.props.poNumber,
            status: this.props.status,
            vendorName: this.props.vendorName,
            shipmentAdviceCode: this.props.shipmentAdviceCode,
            shipmentDate: this.props.shipmentDate,
            shipmentConfirmDate: this.props.shipmentConfirmDate,
            poDate: this.props.poDate,
            transporter: this.props.transporter,
            siteDetail: this.props.siteDetail,
            totalQty: this.props.totalQty,
            dueInDays: this.props.dueInDays,
            count: this.props.filterCount,
            userType: this.props.userType,
            orderCode: this.props.orderCode,
            qcDate : this.props.qcDate
        };
    }
    componentWillMount() {
        this.setState({
            shipmentRequestDate: this.props.shipmentRequestDate,
            poNumber: this.props.poNumber,
            status: this.props.status,
            vendorName: this.props.vendorName,
            shipmentAdviceCode: this.props.shipmentAdviceCode,
            shipmentDate: this.props.shipmentDate,
            shipmentConfirmDate: this.props.shipmentConfirmDate,
            poDate: this.props.poDate,
            transporter: this.props.transporter,
            siteDetail: this.props.siteDetail,
            totalQty: this.props.totalQty,
            dueInDays: this.props.dueInDays,
            count: this.props.filterCount,
            orderCode: this.props.orderCode,
            qcDate : this.props.qcDate
        })
    }
    handleChange(e) {
        if (e.target.id == "shipmentRequestDate") {
            this.setState({
                shipmentRequestDate: e.target.value
            });
        } else if (e.target.id == "poNumber") {
            this.setState({
                poNumber: e.target.value
            });
        } else if (e.target.id == "vendorName") {
            this.setState({
                vendorName: e.target.value
            })

        } else if (e.target.id == "shipmentAdviceCode") {
            this.setState({
                shipmentAdviceCode: e.target.value
            });
        } else if (e.target.id == "shipmentDate") {
            this.setState({
                shipmentDate: e.target.value
            });
        } else if (e.target.id == "poDate") {

            this.setState({
                poDate: e.target.value
            });

        } else if (e.target.id == "shipmentConfirmDate") {
            this.setState({
                shipmentConfirmDate: e.target.value
            })

        } else if (e.target.id == "transporter") {
            this.setState({
                transporter: e.target.value
            });
        } else if (e.target.id == "siteDetail") {
            this.setState({
                siteDetail: e.target.value
            });
        } else if (e.target.id == "totalQty") {
            if (e.target.validity.valid) {
                this.setState({
                    totalQty: e.target.value
                });
            }
        } else if (e.target.id == "dueInDays") {
            if (e.target.validity.valid) {
                this.setState({
                    dueInDays: e.target.value
                })
            }
        } else if (e.target.id == "orderCode") {
            this.setState({ orderCode: e.target.value })
        }else if (e.target.id == "qcDate") {
            this.setState({ qcDate: e.target.value })
        }
    }

    clearFilter(count) {
        this.setState({
            shipmentRequestDate: "",
            poNumber: "",
            status: "",
            vendorName: "",
            shipmentAdviceCode: "",
            shipmentDate: "",
            poDate: "",
            shipmentConfirmDate: "",
            transporter: "",
            siteDetail: "",
            totalQty: "",
            dueInDays: "",
            count: 0,
            orderCode: "",
            qcDate : ""
        })
        this.props.filterCount != 0 ? this.props.clearFilter() : null
    }

    onSubmit(count) {
        // e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            shipmentRequestDate: this.state.shipmentRequestDate,
            poNumber: this.state.poNumber,
            status: this.state.status,
            vendorName: this.state.vendorName,
            shipmentAdviceCode: this.state.shipmentAdviceCode,
            shipmentDate: this.state.shipmentDate,
            poDate: this.state.poDate,
            shipmentConfirmDate: this.state.shipmentConfirmDate,
            transporter: this.state.transporter,
            siteDetail: this.state.siteDetail,
            totalQty: this.state.totalQty,
            dueInDays: this.state.dueInDays,
            isLRCreation: false,
            filterCount: count,
            search: "",
            userType: this.props.userType,
            orderCode: this.state.orderCode,
            qcDate : this.state.qcDate
        }
        this.props.getAllVendorShipmentShippedRequest(data);
        this.props.closeFilter();
        this.props.updateFilter(data)
    }

    render() {

        let count = 0;
        if (this.state.shipmentRequestDate != "") {
            count++;
        }
        if (this.state.poNumber != "") {
            count++;
        }
        if (this.state.vendorName != "") {
            count++

        }
        if (this.state.shipmentAdviceCode != "") {
            count++

        } if (this.state.shipmentDate != "") {
            count++

        }
        if (this.state.poDate != "") {
            count++;
        }
        if (this.state.shipmentConfirmDate != "") {
            count++;
        }
        if (this.state.transporter != "") {
            count++

        }
        if (this.state.siteDetail != "") {
            count++

        }
        if (this.state.totalQty != "") {
            count++

        } if (this.state.dueInDays != "") {
            count++
        }
        if (this.state.orderCode != "") {
            count++;
        }
        if (this.state.qcDate != "") {
            count++;
        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(count)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="shipmentRequestDate" value={this.state.shipmentRequestDate} placeholder={this.state.shipmentRequestDate != "" ? this.state.shipmentRequestDate : "Shipment Requested Date"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="poNumber" value={this.state.poNumber} placeholder="PO Number" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorName" value={this.state.vendorName} placeholder="Vendor Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="shipmentAdviceCode" value={this.state.shipmentAdviceCode} placeholder="Shipment ASN code" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="shipmentDate" value={this.state.shipmentDate} placeholder={this.state.shipmentDate != "" ? this.state.shipmentDate : "Shipment date"} className="organistionFilterModal" />


                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="poDate" value={this.state.poDate} placeholder={this.state.poDate != "" ? this.state.poDate : "PO date"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="shipmentConfirmDate" value={this.state.shipmentConfirmDate} placeholder={this.state.shipmentConfirmDate != "" ? this.state.shipmentConfirmDate : "Shipment confirm date"} className="organistionFilterModal" />
                                    </li>
                                </ul></div></div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">
                                <ul className="list-inline m-top-20">                
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="transporter" value={this.state.transporter} placeholder="Transporter Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} pattern="[0-9]*" id="totalQty" value={this.state.totalQty} placeholder="PO Qty" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} pattern="[0-9]*" id="dueInDays" value={this.state.dueInDays} placeholder="Due In Days" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="siteDetail" value={this.state.siteDetail} placeholder="Site Details" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="orderCode" value={this.state.orderCode} placeholder="Order Code" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="qcDate" value={this.state.qcDate} placeholder={this.state.qcDate != "" ? this.state.qcDate : "QC Date"} className="organistionFilterModal" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.state.qcDate == "" && this.state.shipmentRequestDate == ""&&this.state.orderCode == "" && this.state.poNumber == "" && this.state.vendorName == "" && this.state.shipmentAdviceCode == "" && this.state.shipmentDate == "" && this.state.poDate == "" && this.state.shipmentConfirmDate == "" && this.state.transporter == "" && this.state.siteDetail == "" && this.state.totalQty == "" && this.state.dueInDays == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button type="button" onClick={(e) => this.clearFilter(0)} className="modal_clear_btn">
                                                CLEAR FILTER
                                         </button>}
                                    </li>
                                    <li>
                                        {this.state.qcDate != "" || this.state.shipmentRequestDate != "" ||this.state.orderCode != ""|| this.state.poNumber != "" || this.state.vendorName != "" || this.state.shipmentAdviceCode != "" || this.state.shipmentDate != "" || this.state.poDate != "" || this.state.shipmentConfirmDate != "" || this.state.transporter != "" || this.state.siteDetail != "" || this.state.totalQty != "" || this.state.dueInDays != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default FilterShipmentShipped;
