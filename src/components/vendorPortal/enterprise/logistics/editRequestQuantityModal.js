import React from 'react'; 

class EditRequestQuantity extends React.Component {
    render() {
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content edit-request-quantity">
                    <div className="erq-head">
                        <h3>Edit Request Quantity</h3>
                        <span className="erqh-remove" onClick={this.props.CloseEditRequestQuantity}>
                            <svg xmlns="http://www.w3.org/2000/svg" id="close_6_" width="19.771" height="19.77" data-name="close (6)" viewBox="0 0 19.771 19.77">
                                <g id="Group_3015" data-name="Group 3015">
                                    <path fill="#21314b" id="Path_926" d="M9.886 19.781A9.885 9.885 0 0 1 2.892 2.9a9.891 9.891 0 1 1 6.994 16.881zm-5.91-15.8a8.359 8.359 0 1 0 11.821 0 8.37 8.37 0 0 0-11.821 0z" class="cls-1" data-name="Path 926" transform="translate(0 -.011)"/>
                                </g>
                                <g id="Group_3016" data-name="Group 3016" transform="translate(6.459 6.361)">
                                    <path fill="#21314b" id="Path_927" d="M168.038 171.684a.766.766 0 0 1-.542-1.308l5.416-5.416a.766.766 0 0 1 1.088 1.083l-5.416 5.416a.765.765 0 0 1-.546.225z" class="cls-1" data-name="Path 927" transform="translate(-167.27 -164.735)"/>
                                    <path fill="#21314b" id="Path_928" d="M173.434 171.684a.764.764 0 0 1-.542-.224l-5.416-5.416a.766.766 0 0 1 1.083-1.083l5.416 5.416a.766.766 0 0 1-.542 1.308z" class="cls-1" data-name="Path 928" transform="translate(-167.251 -164.735)"/>
                                </g>
                            </svg>
                        </span>
                    </div>
                    <div className="erq-body">
                        <div className="erq-fields">
                            <label>Request Quantity</label>
                            <input type="text" />
                        </div>
                        <button type="button">Save Changes</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default EditRequestQuantity;