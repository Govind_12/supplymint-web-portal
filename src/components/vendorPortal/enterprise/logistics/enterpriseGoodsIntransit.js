import React, { Component } from 'react';
import orderIcon from '../../../../assets/order.svg';
import filterBtn from '../../../../assets/filterNew.svg';
import DropdownPortal from '../../dropdownPortal';
import chatIcon from '../../../../assets/chatIcon.svg';
import plusIcon from '../../../../assets/plus-blue.svg';
import removeIcon from '../../../../assets/removeIcon.svg';
import { handleChange } from '../../../../helper/index'
import Pagination from '../../../pagination';
import searchIcon from '../../../../assets/clearSearch.svg';
import DeliverConfirmationModal from './deliveryConfirmationModal';
import FilterDeliverAndIntransit from './filterDeliverAndIntransit';
import closeExpandIcon from '../../../../assets/closeDetail.svg';
import Reload from '../../../../assets/refresh-block.svg';
import { CONFIG } from "../../../../config/index";
import filterIcon from '../../../../assets/headerFilter.svg';
import ExportExcel from '../../../../assets/exportToExcel.svg';
import PdfDownload from '../../../../assets/pdfdownload.svg';
import SearchImage from '../../../../assets/searchicon.svg';
import axios from 'axios'
import VendorFilter from '../../vendorFilter';
import CommentBoxModal from '../../commentBoxModal';
import ColoumSetting from '../../../replenishment/coloumSetting';
import ConfirmationSummaryModal from '../../../replenishment/confirmationReset';
import ExpandTransitNDeliver from '../../vendor/logistics/expandtransitNdeliver';
import ShipmentTracking from '../../modals/shipmentTracking';
import ToastError from '../../../utils/toastError';
import ToastLoader from '../../../loaders/toastLoader';
import moment from 'moment';
import MultipleDownload from '../../multipleDownload';

export default class EnterpriseGoodsIntransit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deliveryModal: false,
            sliderDisable: false,
            rangeVal: 1,
            type: 1,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            no: 1,
            search: "",
            shipmentIntransit: [],
            prevId: "",
            expandedId: "",
            expandChecked: [],
            transitStatusCount: 0,
            userType: "entlogi",
            selectedlgtNum: "",
            filter: false,
            filterBar: false,
            lgtNumber: "",
            lgtDate: "",
            vendorName: "",
            transporterName: "",
            ownerSite: "",
            transportMode: "",
            stationFrom: "",
            stationTo: "",
            lgtReceivedQty: "",
            vehicleNo: "",
            filterCount: 0,
            status: "SHIPMENT_INTRANSIT",
            orderCode: "",
            dropOpen: true,
            // Header Filter
            filterKey: "",
            filterType: "",
            prevFilter: "",
            filterItems: {},
            checkedFilters: [],
            filteredValue:[],

            // Dyanmic Header
            coloumSetting: false,
            getHeaderConfig: [],
            fixedHeader: [],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],
            customHeadersState: [],
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            headerCondition: false,
            tableCustomHeader: [],
            tableGetHeaderConfig: [],
            saveState: [],
            notification: [],
            openChats: [],
            applyFilter: false,
            checkedData: [],
            mandateHeaderMain: [],
            reasons: {},
            jumpPage: 1,
            toastError: false,
            showDownloadDrop: false,
            exportToExcel: false,
            toastMsg: "",
            toastLoader: false,
            inputBoxEnable: false,
            shipmentModal: false,
            fromCreationDate: "start date",
            toCreationDate: "end date",
            filterNameForDate: "",
            tagState:false,
            showDownload: false,
            filterValueForTag: [],

            mainHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_LR_INTRANSIT_ALL",
                basedOn: "ALL"
            },
            setHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_LR_INTRANSIT_ASN",
                basedOn: "ASN"
            },
            itemHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "VENDOR_LR_INTRANSIT_ITEM",
                basedOn: "SET"
            },
            tabVal: "1",
            //main
            getMainHeaderConfig: [],
            mainFixedHeader: [],
            mainCustomHeadersState: [],
            mainHeaderSummary: [],
            mainDefaultHeaderMap: [],
            mainFixedHeaderData: [],
            mainHeaderConfigState: {},
            mainHeaderConfigDataState: {},
            mainCustomHeaders: {},
            mainAvailableHeaders: [],
            saveMainState: [],
            //set
            getSetHeaderConfig: [],
            setFixedHeader: [],
            setCustomHeadersState: [],
            setCustomHeaders: {},
            setHeaderSummary: [],
            setDefaultHeaderMap: [],
            setFixedHeaderData: [],
            setHeaderConfigState: {},
            setHeaderConfigDataState: {},
            setCustomHeaders: {},
            saveSetState: [],
            setHeaderCondition: false,
            setAvailableHeaders: [],
            //item
            itemHeaderSummary: [],
            itemDefaultHeaderMap: [],
            itemFixedHeaderData: [],
            itemHeaderConfigState: {},
            itemHeaderConfigDataState: {},
            itemCustomHeaders: {},
            saveItemState: [],
            itemHeaderCondition: false,
            getItemHeaderConfig: [],
            itemfixedHeader: [],
            itemCustomHeadersState: [],
            itemAvailableHeaders: [],
            changesInMainHeaders: false,
            changesInSetHeaders: false,
            changesInItemHeaders: false,
            dragOn:false,
            resultedDataCount: 0,

            selectAll: false,
            allowGateEntry: true,
            toastErrorMsg: "",
        }
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDragEnter = this.handleDragEnter.bind(this);
        this.pushColumnData = this.pushColumnData.bind(this);
    }
    openExportToExcel =(e)=> {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, ()=> document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () =>{
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }
    showDownloadDrop(event) {
        event.preventDefault();
        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });
    }

    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar,
            checkedData: [],
            selectAll: false,
            
        },() =>  document.addEventListener('click', this.closeFilterOnClickEvent));
    }

    closeFilterOnClickEvent =(e)=>{
        if (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
            this.setState({ filter: false, filterBar: false }, () =>
                document.removeEventListener('click', this.closeFilterOnClickEvent))
        }
    }

    closeFilter = (e) => {
        this.setState({ filter: false, filterBar: false }, () =>
            document.removeEventListener('click', this.closeFilterOnClickEvent))
    }
    
    componentDidMount() {
        let payload = {
            no: 1,
            type: 1,
            search: "",
            status: "SHIPMENT_INTRANSIT",
            userType: "entlogi",
            isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
        }
        this.props.getAllVendorTransitNDeliverRequest(payload)

        if (!this.props.replenishment.getMainHeaderConfig.isSuccess) {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
        }
        document.addEventListener("keydown", this.escFunction, false);
        this.props.onReff(this);
        //firebase notification
        let t = this
        const messaging = firebase.messaging()
        messaging.onMessage(function (payload) {
            t.newNotification()
        });
        sessionStorage.setItem('currentPage', "RDVLOGMAIN")
        sessionStorage.setItem('currentPageName', "Goods In-Transit")

        this.props.getStatusButtonActiveRequest();
    }
    newNotification() {
        let commentPayload = {
            module: "LOGISTICS",
            status: "",
            orderNumber: "",
            orderCode: ""
        }
        this.props.commentNotificationRequest(commentPayload)
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
        clearInterval(this.intervalID)
        this.props.onReff(undefined);
    }
  
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.logistic.getAllVendorTransitNDeliver.isSuccess) {
            return {
                shipmentIntransit: nextProps.logistic.getAllVendorTransitNDeliver.data.resource == null ? [] : nextProps.logistic.getAllVendorTransitNDeliver.data.resource,
                prev: nextProps.logistic.getAllVendorTransitNDeliver.data.prePage,
                current: nextProps.logistic.getAllVendorTransitNDeliver.data.currPage,
                next: nextProps.logistic.getAllVendorTransitNDeliver.data.currPage + 1,
                maxPage: nextProps.logistic.getAllVendorTransitNDeliver.data.maxPage,
                transitStatusCount: nextProps.logistic.getAllVendorTransitNDeliver.data.transitStatusCount,
                reasons: nextProps.logistic.getAllVendorTransitNDeliver.data.reasonDropdown,
                jumpPage: nextProps.logistic.getAllVendorTransitNDeliver.data.currPage,
                resultedDataCount: nextProps.logistic.getAllVendorTransitNDeliver.data.resultedDataCount == undefined || nextProps.logistic.getAllVendorTransitNDeliver.data.resultedDataCount == null
                                 ? 0 : nextProps.logistic.getAllVendorTransitNDeliver.data.resultedDataCount,
            }
        }
        if (nextProps.orders.commentNotification.isSuccess) {
            if (nextProps.orders.commentNotification.data.resource != null) {
                return { notification: nextProps.orders.commentNotification.data.resource }
            }
        }
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
                nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
                let mainFixedHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]) : []
                let mainCustomHeadersState = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : []
                let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ? 
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                return {
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    mainCustomHeaders: prevState.headerCondition ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] : {},
                    getMainHeaderConfig,
                    mainAvailableHeaders,
                    mainFixedHeader,
                    mainCustomHeadersState,
                    mainHeaderConfigState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : {},
                    mainFixedHeaderData: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
                    mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
                    mainHeaderSummary: nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : [],
                    mainDefaultHeaderMap: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderMain: Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Mandate Headers"])
                };
            }
        }
        if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getSetHeaderConfig.data.resource != null &&
                nextProps.replenishment.getSetHeaderConfig.data.basedOn == "ASN") {
                let getSetHeaderConfig = nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]) : []
                let setCustomHeadersState = nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]) : []
                let setAvailableHeaders = setCustomHeadersState.length !== 0 ? 
                Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                return {
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    setCustomHeaders: prevState.setHeaderCondition ? nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] : {},
                    getSetHeaderConfig,
                    setFixedHeader: nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]) : [],
                    setCustomHeadersState,
                    setAvailableHeaders,
                    setHeaderConfigState: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] : {},
                    setFixedHeaderData: nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] : {},
                    setHeaderConfigDataState: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] } : {},
                    setHeaderSummary: nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]) : [],
                    setDefaultHeaderMap: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet: Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Mandate Headers"])
                };
            }
        }
        if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getItemHeaderConfig.data.resource != null &&
                nextProps.replenishment.getItemHeaderConfig.data.basedOn == "SET") {
                let getItemHeaderConfig = nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]) : []
                let itemCustomHeadersState = nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]) : []
                let itemAvailableHeaders = itemCustomHeadersState.length !== 0 ?
                Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                return {
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    itemCustomHeaders: prevState.itemHeaderCondition ? nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] : {},
                    getItemHeaderConfig,
                    itemFixedHeader: nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"]) : [],
                    itemCustomHeadersState,
                    itemAvailableHeaders,
                    itemHeaderConfigState: nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] : {},
                    itemFixedHeaderData: nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"] : {},
                    itemHeaderConfigDataState: nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] } : {},
                    itemHeaderSummary: nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]) : [],
                    itemDefaultHeaderMap: nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderItem: Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Mandate Headers"])
                };
            }
        }
        if (nextProps.logistic.getAllVendorSsdetail.data.resource != null && nextProps.logistic.getAllVendorSsdetail.data.basedOn == "set") {
            return {
                itemBaseArray: nextProps.logistic.getAllVendorSsdetail.data.resource.poDetails
            }
        }
        if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) {
            return{
                allowGateEntry: nextProps.logistic.getButtonActiveConfig.data.resource.gateEntryVisible == "TRUE" ? true : false
            }
        }
        return null;
    }
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createMainHeaderConfig.isSuccess && this.props.replenishment.createMainHeaderConfig.data.basedOn == "ALL") {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)

        }
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "ASN") {
            this.props.getSetHeaderConfigRequest(this.state.setHeaderPayload)

        }
        if (this.props.replenishment.createItemHeaderConfig.isSuccess && this.props.replenishment.createItemHeaderConfig.data.basedOn == "SET") {
            this.props.getItemHeaderConfigRequest(this.state.itemHeaderPayload)
        }        
        if (this.props.logistic.updateDelivery.isSuccess) {
            let payload = {
                no: 1,
                type: 1,
                search: "",
                status: "SHIPMENT_INTRANSIT",
                userType: "entlogi",
                isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
            }
            this.setState({ selectedlgtNum: "" })
            this.handleDeliveryModal();
            this.props.getAllVendorTransitNDeliverRequest(payload)
        }
        if (this.props.replenishment.getSetHeaderConfig.isSuccess) {
            if (this.props.replenishment.getSetHeaderConfig.data.resource != null && this.props.replenishment.getSetHeaderConfig.data.basedOn == "SET") {
                setTimeout(() => {
                    this.setState({
                        setHeaderCondition: this.state.setCustomHeadersState.length == 0 ? true : false,
                    })
                }, 1000);

                // view email redirect logic here:::
                let queryParam = sessionStorage.getItem('login_redirect_queryParam') == null || sessionStorage.getItem('login_redirect_queryParam') == undefined ? null :
                    Object.keys(sessionStorage.getItem('login_redirect_queryParam')).length ? JSON.parse(sessionStorage.getItem('login_redirect_queryParam')) : null;
                let filterValue = "";
                let filterKeyValue = "";
                if (queryParam !== null && queryParam !== undefined && Object.keys(queryParam).length != 0) {
                    var allHeaders = this.props.replenishment.getMainHeaderConfig.data.resource["Default Headers"]
                    filterValue = Object.keys(queryParam).map((_) => { return allHeaders[_] });
                    let flag = false;
                    filterValue.map(data => { if (data == undefined) flag = true; });
                    filterKeyValue = Object.values(queryParam);
                    if (!flag && filterValue.length > 0 && filterKeyValue.length > 0 && filterValue.length == filterKeyValue.length) {
                        setTimeout(() => {
                             let payload = {
                                no: 1,
                                type: 2,
                                search: "",
                                status: "SHIPMENT_INTRANSIT",
                                userType: "entlogi",
                                filter: queryParam,
                                isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                            }
                            this.props.getAllVendorTransitNDeliverRequest(payload)
                        }, 500);

                        this.setState({
                            filteredValue: Object.keys(queryParam).length ? queryParam : {},
                            checkedFilters: Object.keys(queryParam).length ? filterValue : [],
                            applyFilter: Object.keys(queryParam).length ? true : false,
                            inputBoxEnable: Object.keys(queryParam).length ? true : false,
                            tagState: true,
                            //[filterValue] : queryParam.shipmentAdviceCode,
                            type: 2,
                            filterValueForTag: Object.keys(queryParam).length ? queryParam : {},

                        }, () => {
                            filterValue.map((data, index) => {
                                this.setState({ [data]: filterKeyValue[index] })
                            })
                        })
                    }
                }
            }
        }
        if (this.props.replenishment.getItemHeaderConfig.isSuccess) {
            if (this.props.replenishment.getItemHeaderConfig.data.resource != null && this.props.replenishment.getItemHeaderConfig.data.basedOn == "SET") {
                setTimeout(() => {
                    this.setState({
                        itemHeaderCondition: this.state.itemCustomHeadersState.length == 0 ? true : false,
                    })
                }, 1000);

                // view email redirect logic here:::
                let queryParam = sessionStorage.getItem('login_redirect_queryParam') == null || sessionStorage.getItem('login_redirect_queryParam') == undefined ? null :
                    Object.keys(sessionStorage.getItem('login_redirect_queryParam')).length ? JSON.parse(sessionStorage.getItem('login_redirect_queryParam')) : null;
                let filterValue = "";
                let filterKeyValue = "";
                if (queryParam !== null && queryParam !== undefined && Object.keys(queryParam).length != 0) {
                    var allHeaders = this.props.replenishment.getMainHeaderConfig.data.resource["Default Headers"]
                    filterValue = Object.keys(queryParam).map((_) => { return allHeaders[_] });
                    let flag = false;
                    filterValue.map(data => { if (data == undefined) flag = true; });
                    filterKeyValue = Object.values(queryParam);
                    if (!flag && filterValue.length > 0 && filterKeyValue.length > 0 && filterValue.length == filterKeyValue.length) {
                        setTimeout(() => {
                             let payload = {
                                no: 1,
                                type: 2,
                                search: "",
                                status: "SHIPMENT_INTRANSIT",
                                userType: "entlogi",
                                filter: queryParam,
                                isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                            }
                            this.props.getAllVendorTransitNDeliverRequest(payload)
                        }, 500);

                        this.setState({
                            filteredValue: Object.keys(queryParam).length ? queryParam : {},
                            checkedFilters: Object.keys(queryParam).length ? filterValue : [],
                            applyFilter: Object.keys(queryParam).length ? true : false,
                            inputBoxEnable: Object.keys(queryParam).length ? true : false,
                            tagState: true,
                            //[filterValue] : queryParam.shipmentAdviceCode,
                            type: 2,
                            filterValueForTag: Object.keys(queryParam).length ? queryParam : {},

                        }, () => {
                            filterValue.map((data, index) => {
                                this.setState({ [data]: filterKeyValue[index] })
                            })
                        })
                    }
                }
            }
        }
        if (this.props.replenishment.getMainHeaderConfig.isSuccess) {
            if (this.props.replenishment.getMainHeaderConfig.data.resource != null && this.props.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                setTimeout(() => {
                    this.setState({
                        headerCondition: this.state.mainCustomHeadersState.length == 0 ? true : false,
                    })
                }, 1000);

                // view email redirect logic here:::
                let queryParam = sessionStorage.getItem('login_redirect_queryParam') == null || sessionStorage.getItem('login_redirect_queryParam') == undefined ? null :
                    Object.keys(sessionStorage.getItem('login_redirect_queryParam')).length ? JSON.parse(sessionStorage.getItem('login_redirect_queryParam')) : null;
                let filterValue = "";
                let filterKeyValue = "";
                if (queryParam !== null && queryParam !== undefined && Object.keys(queryParam).length != 0) {
                    var allHeaders = this.props.replenishment.getMainHeaderConfig.data.resource["Default Headers"]
                    filterValue = Object.keys(queryParam).map((_) => { return allHeaders[_] });
                    let flag = false;
                    filterValue.map(data => { if (data == undefined) flag = true; });
                    filterKeyValue = Object.values(queryParam);
                    if (!flag && filterValue.length > 0 && filterKeyValue.length > 0 && filterValue.length == filterKeyValue.length) {
                        setTimeout(() => {
                             let payload = {
                                no: 1,
                                type: 2,
                                search: "",
                                status: "SHIPMENT_INTRANSIT",
                                userType: "entlogi",
                                filter: queryParam,
                                isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                            }
                            this.props.getAllVendorTransitNDeliverRequest(payload)
                        }, 500);

                        this.setState({
                            filteredValue: Object.keys(queryParam).length ? queryParam : {},
                            checkedFilters: Object.keys(queryParam).length ? filterValue : [],
                            applyFilter: Object.keys(queryParam).length ? true : false,
                            inputBoxEnable: Object.keys(queryParam).length ? true : false,
                            tagState: true,
                            //[filterValue] : queryParam.shipmentAdviceCode,
                            type: 2,
                            filterValueForTag: Object.keys(queryParam).length ? queryParam : {},

                        }, () => {
                            filterValue.map((data, index) => {
                                this.setState({ [data]: filterKeyValue[index] })
                            })
                        })
                    }
                }
            }
        }
        if (this.props.logistic.getAllVendorTransitNDeliver.isSuccess) {
            this.setState({ checkedData: [], selectAll: false})
            this.closingAllModal();
        }
        if (this.props.logistic.getButtonActiveConfig.isSuccess && this.props.logistic.getButtonActiveConfig.data.resource != null) {
            this.setState({
                allowGateEntry: this.props.logistic.getButtonActiveConfig.data.resource.gateEntryVisible == "TRUE" ? true : false
            })
            this.props.getStatusButtonActiveClear()
        }
    }
    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.logistic.getAllVendorTransitNDeliver.data.prePage,
                    current: this.props.logistic.getAllVendorTransitNDeliver.data.currPage,
                    next: this.props.logistic.getAllVendorTransitNDeliver.data.currPage + 1,
                    maxPage: this.props.logistic.getAllVendorTransitNDeliver.data.maxPage,
                })
                if (this.props.logistic.getAllVendorTransitNDeliver.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.logistic.getAllVendorTransitNDeliver.data.currPage - 1,
                        search: this.state.search,
                        status: "SHIPMENT_INTRANSIT",
                        userType: "entlogi",
                        lgtNumber: this.state.logisticNo,
                        lgtDate: this.state.lgtDate,
                        vendorName: this.state.vendorName,
                        transporterName: this.state.transporterName,
                        ownerSite: this.state.ownerSite,
                        transportMode: this.state.transporterMode,
                        stationFrom: this.state.stationFrom,
                        stationTo: this.state.stationTo,
                        lgtReceivedQty: this.state.lgtReceivedQty,
                        vehicleNo: this.state.vehicleNo,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        filter: this.state.filteredValue,
                        isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                    }
                    this.props.getAllVendorTransitNDeliverRequest(data)
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.logistic.getAllVendorTransitNDeliver.data.prePage,
                current: this.props.logistic.getAllVendorTransitNDeliver.data.currPage,
                next: this.props.logistic.getAllVendorTransitNDeliver.data.currPage + 1,
                maxPage: this.props.logistic.getAllVendorTransitNDeliver.data.maxPage,
            })
            if (this.props.logistic.getAllVendorTransitNDeliver.data.currPage != this.props.logistic.getAllVendorTransitNDeliver.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.logistic.getAllVendorTransitNDeliver.data.currPage + 1,
                    search: this.state.search,
                    status: "SHIPMENT_INTRANSIT",
                    userType: "entlogi",
                    lgtNumber: this.state.logisticNo,
                    lgtDate: this.state.lgtDate,
                    vendorName: this.state.vendorName,
                    transporterName: this.state.transporterName,
                    ownerSite: this.state.ownerSite,
                    transportMode: this.state.transporterMode,
                    stationFrom: this.state.stationFrom,
                    stationTo: this.state.stationTo,
                    lgtReceivedQty: this.state.lgtReceivedQty,
                    vehicleNo: this.state.vehicleNo,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    filter: this.state.filteredValue,
                    isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                }
                this.props.getAllVendorTransitNDeliverRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            }
            else {
                this.setState({
                    prev: this.props.logistic.getAllVendorTransitNDeliver.data.prePage,
                    current: this.props.logistic.getAllVendorTransitNDeliver.data.currPage,
                    next: this.props.logistic.getAllVendorTransitNDeliver.data.currPage + 1,
                    maxPage: this.props.logistic.getAllVendorTransitNDeliver.data.maxPage,
                })
                if (this.props.logistic.getAllVendorTransitNDeliver.data.currPage <= this.props.logistic.getAllVendorTransitNDeliver.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        search: this.state.search,
                        status: "SHIPMENT_INTRANSIT",
                        userType: "entlogi",
                        lgtNumber: this.state.logisticNo,
                        lgtDate: this.state.lgtDate,
                        vendorName: this.state.vendorName,
                        transporterName: this.state.transporterName,
                        ownerSite: this.state.ownerSite,
                        transportMode: this.state.transporterMode,
                        stationFrom: this.state.stationFrom,
                        stationTo: this.state.stationTo,
                        lgtReceivedQty: this.state.lgtReceivedQty,
                        vehicleNo: this.state.vehicleNo,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        filter: this.state.filteredValue,
                        isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                    }
                    this.props.getAllVendorTransitNDeliverRequest(data)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.logistic.getAllVendorTransitNDeliver.data.prePage,
                    current: this.props.logistic.getAllVendorTransitNDeliver.data.currPage,
                    next: this.props.logistic.getAllVendorTransitNDeliver.data.currPage + 1,
                    maxPage: this.props.logistic.getAllVendorTransitNDeliver.data.maxPage,
                })
                if (this.props.logistic.getAllVendorTransitNDeliver.data.currPage <= this.props.logistic.getAllVendorTransitNDeliver.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.logistic.getAllVendorTransitNDeliver.data.maxPage,
                        search: this.state.search,
                        status: "SHIPMENT_INTRANSIT",
                        userType: "entlogi",
                        lgtNumber: this.state.logisticNo,
                        lgtDate: this.state.lgtDate,
                        vendorName: this.state.vendorName,
                        transporterName: this.state.transporterName,
                        ownerSite: this.state.ownerSite,
                        transportMode: this.state.transporterMode,
                        stationFrom: this.state.stationFrom,
                        stationTo: this.state.stationTo,
                        lgtReceivedQty: this.state.lgtReceivedQty,
                        vehicleNo: this.state.vehicleNo,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        filter: this.state.filteredValue,
                        isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                    }
                    this.props.getAllVendorTransitNDeliverRequest(data)
                }
            }
        }
    }
    searchClear = () => {
        if (this.state.type == 3 || this.state.type == 4) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 2 : 1,
                search: "",
                status: "SHIPMENT_INTRANSIT",
                userType: "entlogi",
                filter: this.state.filteredValue,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
            }
            this.props.getAllVendorTransitNDeliverRequest(payload)
            this.setState({ search: "", type: this.state.type == 4 ? 2 : 1 })
        } else {
            this.setState({ search: "" })
        }
    }
    onSearch = (e) => {
        this.setState({ checkedData: [], selectAll: false})
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                let payload = {
                    no: 1,
                    type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                    search: e.target.value,
                    status: "SHIPMENT_INTRANSIT",
                    userType: "entlogi",
                    filter: this.state.filteredValue,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                }
                this.props.getAllVendorTransitNDeliverRequest(payload)
                this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3 })
            }
        }
        this.setState({
            search: e.target.value,
        }, () => {
            if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
                let payload = {
                    no: 1,
                    type: this.state.type == 4 ? 2 : 1,
                    search: "",
                    status: "SHIPMENT_INTRANSIT",
                    userType: "entlogi",
                    filter: this.state.filteredValue,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                }
                this.props.getAllVendorTransitNDeliverRequest(payload)
                this.setState({ search: "", type: this.state.type == 4 ? 2 : 1 })
            }
        })
    }
    onRefresh = () => {
        this.state.checkedFilters.map((data) => this.setState({ [data]: "" }))
        let payload = {
            no: 1,
            type: 1,
            search: "",
            status: "SHIPMENT_INTRANSIT",
            userType: "entlogi",
            isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
        }
        if (document.getElementsByClassName('rotate180')[0] != undefined) {
            document.getElementsByClassName('rotate180')[0].classList.remove('rotate180')
        }
        this.setState({ filteredValue: [], selectAll: false, checkedFilters: [], checkedData: [] })
        this.props.getAllVendorTransitNDeliverRequest(payload)
        this.resetField()
        this.closingAllModal();
        this.clearAllTag();
    }
    expandColumn(id, e, data) {
        var img = e.target
        if( !this.state.selectAll ){
            let activeData = { 'lgtNumber': data.logisticNo },
                expandChecked = []
            if (!this.state.actionExpand || this.state.prevId !== id) {
                expandChecked.push(activeData)
                let payload = {
                    no: 1,
                    type: 1,
                    search: "",
                    userType: "entlogi",
                    lgtNumber: activeData.lgtNumber,
                    status: "SHIPMENT_INTRANSIT"
                }
                this.props.getAllVendorSiDetailRequest(payload)
                this.setState({ actionExpand: true, prevId: id, expandedId: id, dropOpen: true })
            } else {
                this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
            }
            this.setState({
                expandChecked: expandChecked
            })
        }
    }


    callHandleChange = (e) => {
        var scrollValue = handleChange(e)
        this.setState({ rangeVal: scrollValue.rangeVal, sliderDisable: scrollValue.total == 0 ? false : true })
    }
    handleDeliveryModal = () => {
        this.setState({ deliveryModal: !this.state.deliveryModal }, ()=>{
            document.addEventListener('click', this.closeDeliveryModal)
            if( !this.state.deliveryModal )
             document.removeEventListener('click', this.closeDeliveryModal)
        })
    }
    closeDeliveryModal = (e) => {
        if( e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop")){
            this.setState({
                deliveryModal: false,
            }, () => document.removeEventListener('click', this.closeDeliveryModal))
        }
    }

    handleCheckBox(data) {
        let array = [...this.state.checkedData] 
        
        if (this.state.checkedData.some((item) => item.id == data.id)) {
            array = array.filter((item) => item.id != data.id)
        } else {
            array.push(data)                
        } 
        
        if( array.length == 1 )
           this.setState({ selectedlgtNum: array[0].id})
        else
            this.setState({ selectedlgtNum: ""}) 

        if( array.length === this.state.shipmentIntransit.length)
            this.setState({ checkedData: array, selectAll: true })
        else    
            this.setState({ checkedData: array, selectAll: false })
           
    }
    
    xlscsv() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let payload = {
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            status: "SHIPMENT_INTRANSIT",
            sortedBy: this.state.sortedBy || "",
            sortedIn: this.state.sortedIn || "",
        }
        let filter = { ...this.state.filteredValue }
        let final = { ...payload, filter}
        let selectAllFlag = this.state.selectAll ? true : false;
        let response = ""
        axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=ENT_LR_INTRANSIT_ALL&isAllData=false&isOnlyCurrentPage=${selectAllFlag}`, final, { headers: headers })
            .then(res => {
                response = res
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
                setTimeout(()=>{
                    this.setState({
                        toastError: false
                    })
                }, 5000)
            });

    }
    getAllData() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let payload = {
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            status: "SHIPMENT_INTRANSIT",
            sortedBy: this.state.sortedBy || "",
            sortedIn: this.state.sortedIn || "",
        }
        let filter = { ...this.state.filteredValue }
        let final = { ...payload, filter}
        let selectAllFlag = this.state.selectAll ? true : false;
        let response = ""
        axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=ENT_LR_INTRANSIT_ALL,ENT_LR_INTRANSIT_ASN,VENDOR_LR_INTRANSIT_ITEM,VENDOR_LR_INTRANSIT_ITEM&isAllData=true&isOnlyCurrentPage=${selectAllFlag}`, final, { headers: headers })
            .then(res => {
                response = res
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
                setTimeout(()=>{
                    this.setState({
                        toastError: false
                    })
                }, 5000)
            });

    }
    resetField() {
        this.setState({
            search: "",
            lgtNumber: "",
            lgtDate: "",
            vendorName: "",
            transporterName: "",
            ownerSite: "",
            transportMode: "",
            stationFrom: "",
            stationTo: "",
            lgtReceivedQty: "",
            vehicleNo: "",
            filterCount: 0,
            type: 1,
            checkedData: [],
            showDownload: false,
        })
        sessionStorage.setItem('login_redirect_queryParam', "")
    }

    // chat box
    openChatBox = (event, data) => {
        if (event == "close") {
            //this.setState({ openChats: [] })
            this.closingAllModal();
        } else {
            if( !this.state.selectAll ){
                event.preventDefault()
                let handleOpenClose = event.target.dataset.id
                let id = event.target.id
                var arr = [...this.state.openChats]
                if (handleOpenClose == "openChat") {
                    if (!this.state.openChats.some((item) => item.orderNumber == data.orderNumber)) {
                        if (arr.length < 1) {
                            let pushData = { id: data.id, orderNumber: data.orderNumber, lgtNo: data.logisticNo, orderCode: data.orderCode, shipmentId: data.id, orgID: data.orgId, commentId: data.id, commentCode: data.logisticNo, vendorCode: data.consignorCode }
                            this.setState(prevState => ({ lgtNumber: data.logisticNo, orderId: data.orderId, documentNumber: data.documentNumber, chatModal: "open", openChats: [...prevState.openChats, pushData] }))
                        }
                    }
                } else if (handleOpenClose == "closeChat") {
                    this.setState({ openChats: [] })
                } else if (id == "minimize") {
                    this.setState({ chatModal: "minimize" })
                } else if (id == "openMinimize") {
                    this.setState({ chatModal: "openMinimize" })
                }
            }
        }
    }
 
    closeConfirmModal(e) {
        if( e.target.localName === "button"  || e.target.className.includes("backdrop")){
            this.setState({
                confirmModal: !this.state.confirmModal,
            }, () => document.removeEventListener('click', this.closeConfirmModal))
        }
    }
    filterHeader = (event) => {
        var data = event.target.dataset.key
        if( event.target.closest("th").classList.contains("rotate180"))
            event.target.closest("th").classList.remove("rotate180")
        else
            event.target.closest("th").classList.add("rotate180")    
        //var def = { ...this.state.headerConfigDataState };
        var def = {...this.state.mainHeaderConfigDataState};
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ filterKey, filterType: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            let payload = {
                no: this.state.current,
                type: this.state.type,
                search: this.state.search,
                status: "SHIPMENT_INTRANSIT",
                userType: "entlogi",
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue,
                isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
            }
            this.props.getAllVendorTransitNDeliverRequest(payload)
        })
    }

    handleInput = (event,filterName) => {
        if( event != undefined && event.length != undefined ){
            this.setState({ fromCreationDate: moment(event[0]._d).format('YYYY-MM-DD'),
                            filterNameForDate: filterName}, ()=>this.handleFromAndToValue(event))
            this.setState({ toCreationDate: moment(event[1]._d).format('YYYY-MM-DD'),
                            filterNameForDate: filterName }, ()=>this.handleFromAndToValue(event))
            if(filterName == "Time"){
                this.setState({
                    fromCreationDate: moment(event[0]._d).format(),
                    filterNameForDate: filterName
                }, () => this.handleFromAndToValue(event))  
                this.setState({
                    toCreationDate: moment(event[1]._d).format(),
                    filterNameForDate: filterName
                }, () => this.handleFromAndToValue(event))
            } 
        }
        else if( event != null ){
           
            this.handleFromAndToValue(event);  
    }

    }
    handleFromAndToValue=()=>{
        var value = event.target.value;
        var name = event.target.dataset.value;
        if( name == undefined ){ 
            value = this.state.fromCreationDate+" | "+this.state.toCreationDate
            name = this.state.filterNameForDate;
         }
         else     
         value = event.target.value
         if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
          }
        
        this.setState({ [name]: value, applyFilter: true }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                //this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
                this.setState({ applyFilter: true })
            }
            this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
        })
    }

    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        let len = Object.values(this.state.filterValueForTag).length > 0;
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            delete this.state.filterValueForTag[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)]
            this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = "";
            this.setState({ [data]: ""})
            let flag = array.some(data => this.state[data] == "" || this.state[data] == undefined)
            if(!flag && len){
                this.state.checkedFilters.some( (item,index) => {
                    if( item == data){
                        this.clearTag(e, index)
                    }
                })
            }           
                
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
    }
    
    clearFilter = () => {
        this.state.checkedFilters.map((data) => this.setState({ [data]: "" }))
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                status: "SHIPMENT_INTRANSIT",
                userType: "entlogi",
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
            }
            this.props.getAllVendorTransitNDeliverRequest(payload)
        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            filterValueForTag: [],
            checkedFilters:[],
        })
        sessionStorage.setItem('login_redirect_queryParam', "")   
    }
   
    submitFilter = () => {
        let payload = {}
        let filtervalues =  {}

        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        // Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data] +"T00:00+05:30")))
        Object.keys(payload).map((data) => (( data.includes("logisticDate") || data.includes("lrDate")|| data.includes("validFromDate") || data.includes("validToDate")|| data.includes("qcFromDate") || data.includes("qcToDate")||data.includes("qcDate")||data.includes("Date"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim()+"T00:00+05:30", to: payload[data].split("|")[1].trim()+"T00:00+05:30" })))
        Object.keys(payload).map((data) => (data.includes("createdTime"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim()}))     
        
        //for handling to and from value on UI level::
         this.state.checkedFilters.map((data) => (filtervalues[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(filtervalues).map((data) => (data.includes("Qty") || data.includes("poAmount"))
         && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: filtervalues[data].split("|")[0].trim(), to: filtervalues[data].split("|")[1].trim()}))
        Object.keys(filtervalues).map((data) => (data.includes("logisticDate") || data.includes("lrDate") || data.includes("validFromDate") || data.includes("validToDate") || data.includes("qcFromDate") || data.includes("qcToDate") || data.includes("qcDate") || data.includes("Date"))
         && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: moment(filtervalues[data].split("|")[0].trim()).format("DD-MM-YYYY"), to: moment(filtervalues[data].split("|")[1].trim()).format('DD-MM-YYYY') }))
        Object.keys(filtervalues).map((data) => ((data.includes("createdTime"))
         && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: moment(filtervalues[data].split("|")[0].trim()).format("DD-MM-YYYY HH:mm"), to: moment(filtervalues[data].split("|")[1].trim()).format("DD-MM-YYYY HH:mm") }))) 
       
        let data = {
            no: 1,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            search: this.state.search,
            status: "SHIPMENT_INTRANSIT",
            userType: "entlogi",
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: payload,
            isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
        }
        this.props.getAllVendorTransitNDeliverRequest(data)

        this.setState({
            filter: false,
            filteredValue: payload,
            filterValueForTag: filtervalues,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            tagState:true
        })
        document.removeEventListener('click', this.closeFilterOnClickEvent)
    }

    handleInputBoxEnable = (e, data) => {
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }

    small = (str) => {
        if (str != null) {
            var str = str.toString()
            if (str.length <= 45) {
                return false;
            }
            return true;
        }
    }
    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        no: _.target.value,
                        type: this.state.type,
                        search: this.state.search,
                        status: "SHIPMENT_INTRANSIT",
                        userType: "entlogi",
                        filter: this.state.filteredValue,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        isDashboardComment: sessionStorage.getItem('isDashboardComment') != undefined ? sessionStorage.getItem('isDashboardComment') : 0,
                    }
                    this.props.getAllVendorTransitNDeliverRequest(payload)
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 10000);
                }
            }
        }
    }
    closeToastError = () => {
        this.setState({ toastError: false })
    }
    closingAllModal = () =>{
        if( this.commentBoxRef !== undefined && this.commentBoxRef.state !== undefined && this.commentBoxRef.state.confirmModal ) {
            this.commentBoxRef.closeConfirmModal();
        }
        else if( this.commentBoxRef !== undefined && this.commentBoxRef.state !== undefined && this.commentBoxRef.state.transcriptModal ) {
            this.commentBoxRef.handleEmailTranscriptModal();
        }
        else{
            this.setState({ openChats: [], cancelModal: false, filter: false, exportToExcel: false, 
                 showDownloadDrop: false, completeQCModal: false, handleModal: false,
                 actionExpand: false, dropOpen: false,headerCondition: false, coloumSetting: false, shipmentModal: false,
                 confirmModal: false, deliveryModal: false })
        }
        document.removeEventListener('click', this.closeFilterOnClickEvent)
        document.removeEventListener('click', this.closeExportToExcel)
        document.removeEventListener('click', this.closeDownloadDrop)
        document.removeEventListener('click', this.closeColumnSetting)
        document.removeEventListener('click', this.closeDeliveryModal)
    }
    escFunction = (event) => {
        if (event.keyCode === 27 ) {
            this.closingAllModal();
        } 
    }

    shipmentTrackModal = () => {
        this.closingAllModal();
        this.setState({ shipmentModal: !this.state.shipmentModal }, () =>
          document.addEventListener('click', this.closeShipmentTrackModal))
    }

    closeShipmentTrackModal = (e) =>{
        if( e.target.localName == "button"  || e.target.className.includes("backdrop") ){
            this.setState({ shipmentModal: false }, () =>
             document.removeEventListener('click', this.closeShipmentTrackModal))
        }
    }
   
    clearTag=(e,index)=>{
        let deleteItem = this.state.checkedFilters;
        let deletedItem = this.state.checkedFilters[index];
        deleteItem.splice(index,1)
        this.setState({
           checkedFilters:deleteItem,
           [deletedItem]: "",
        },()=>{
            if( this.state.checkedFilters.length == 0 )
                this.clearFilter();
            else
                this.submitFilter();
        })
    }
    clearAllTag=(e)=>{
        this.setState({
            checkedFilters:[],
      },()=>{
            this.clearFilter();
            this.clearFilterOutside();
        })
    }
 
   clearFilterOutside=()=>{
         this.setState({
             filteredValue:[],
             selectAll:false,
             checkedFilters:[]
         })
    }
    // dynamic Drag and drop header implementation
    handleDragStart(e, key, dragItem, dragNode) {
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', this.handleDragEnd(dragItem, dragNode))
        dragItem.current = key;
        this.setState({
            dragOn:true
        })
    }

    handleDragEnd(dragItem, dragNode) {
        //this.props.handleDragEnd(this.dragNode)
        dragNode.current.removeEventListener('dragend', this.HandleDragEnd)
        this.setState({
            dragOn:false
        })
        dragItem.current = null
        dragNode.current = null
    }
    handleDragEnter(e, key, dragItem, dragNode) {
        const currentItem = dragItem.current
        if (e.target !== dragItem.current) {
            if (this.state.tabVal == 1) {
                if (this.state.headerCondition) {
                    this.setState((prevState, props) => {
                        // let mainDefaultHeaderMapTemp = prevState.mainDefaultHeaderMap
                        // let getMainHeaderConfigTemp = prevState.getMainHeaderConfig
                        // let mainHeaderConfigDataStateTemp = prevState.mainHeaderConfigDataState
                        let newList = prevState.mainDefaultHeaderMap
                        let newMainHeaderConfigList = prevState.getMainHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newMainHeaderConfigList.splice(key, 0, newMainHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newMainHeaderConfigList))
                        let configheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { //mainHeaderConfigDataState: configheaderData ,
                             changesInMainHeaders: true, 
                             //mainDefaultHeaderMap: newList, 
                             mainCustomHeaders: configheaderData,
                             mainHeaderSummary: newList,
                              mainCustomHeadersState: newMainHeaderConfigList,
                            //  mainReOrderStatus:true,
                            // mainDefaultHeaderMapTemp:mainDefaultHeaderMapTemp ,
                            // getMainHeaderConfigTemp:getMainHeaderConfigTemp,
                            // mainHeaderConfigDataStateTemp :mainHeaderConfigDataStateTemp 
                        }
                    })
                } else {
                    this.setState((prevState, props) => {
                        // let mainCustomHeadersTemp = prevState.mainCustomHeaders 
                        // let mainHeaderSummaryTemp = prevState.mainHeaderSummary
                        // let mainCustomHeadersStateTemp = prevState.mainCustomHeadersState
                        let newList = prevState.mainHeaderSummary
                        let newCustomList = prevState.mainCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomList.splice(key, 0, newCustomList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newCustomList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true, 
                            mainCustomHeaders: customheaderData,
                             mainHeaderSummary: newList,
                              mainCustomHeadersState: newCustomList,
                            //   mainReOrderStatus:true,
                            //   mainCustomHeadersTemp:mainCustomHeadersTemp ,
                            //   mainHeaderSummaryTemp: mainHeaderSummaryTemp,
                            //    mainCustomHeadersStateTemp:mainCustomHeadersStateTemp
                            }
                    })

                }
            }
            if (this.state.tabVal == 2) {
                if (this.state.setHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.setDefaultHeaderMap
                        let newSetHeaderList = prevState.getSetHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newSetHeaderList.splice(key, 0, newSetHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newSetHeaderList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { //setHeaderConfigDataState:customheaderData,
                             changesInSetHeaders: true, 
                            //  setDefaultHeaderMap: newList, 
                            //  getSetHeaderConfig: newSetHeaderList,
                             setHeaderSummary: newList, 
                            setCustomHeadersState: newSetHeaderList,
                            setCustomHeaders:customheaderData,
                            }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.setHeaderSummary
                        let newCustomHeaderList = prevState.setCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setCustomHeaders:customheaderData, 
                            changesInSetHeaders: true, 
                            setHeaderSummary: newList, 
                            setCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            if (this.state.tabVal == 3) {
                if (this.state.itemHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.itemDefaultHeaderMap
                        let newItemHeaderConfigList = prevState.getItemHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newItemHeaderConfigList.splice(key, 0, newItemHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.itemHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newItemHeaderConfigList))
                        let mainheaderConfigData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { //itemHeaderConfigDataState:mainheaderConfigData,
                            changesInItemHeaders: true, 
                            //itemDefaultHeaderMap: newList, 
                            //getItemHeaderConfig: newItemHeaderConfigList,
                            itemCustomHeaders:mainheaderConfigData,
                            itemHeaderSummary: newList, 
                            itemCustomHeadersState: newItemHeaderConfigList
                         }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.itemHeaderSummary
                        let newItemCustomHeaderList = prevState.itemCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newItemCustomHeaderList.splice(key, 0, newItemCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.itemCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newItemCustomHeaderList))
                        let itemCustomHeaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { itemCustomHeaders:itemCustomHeaderData,
                            changesInItemHeaders: true, 
                            itemHeaderSummary: newList, 
                            itemCustomHeadersState: newItemCustomHeaderList }
                    })
                }
            }
        }

    }
    openColoumSetting(data) {
        this.setState({ checkedData: [], selectAll: false, actionExpand: false, dropOpen: false})
        if (this.state.tabVal == 1) {
            if (this.state.mainCustomHeadersState.length == 0) {
                this.setState({
                    headerCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }

        }
        else if (this.state.tabVal == 2) {
            if (this.state.setCustomHeadersState.length == 0) {
                this.setState({
                    setHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
        else if (this.state.tabVal == 3) {
            if (this.state.itemCustomHeadersState.length == 0) {
                this.setState({
                    itemHeaderCondition: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
    }
    closeColumnSetting = (e) => {
         if (e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
            this.setState({ coloumSetting: false }, () =>
                document.removeEventListener('click', this.closeColumnSetting))
        } 
    }
    pushColumnData(e,data) {
        if (this.state.tabVal == 1) {
            e.preventDefault();
            let getHeaderConfig = this.state.getMainHeaderConfig
            let customHeadersState = this.state.mainCustomHeadersState
            let headerConfigDataState = this.state.mainHeaderConfigDataState
            let customHeaders = this.state.mainCustomHeaders
            let saveState = this.state.saveMainState
            let defaultHeaderMap = this.state.mainDefaultHeaderMap
            let headerSummary = this.state.mainHeaderSummary
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (this.state.headerCondition) {
            if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig = getHeaderConfig
                    getHeaderConfig.push(data)
                    var even = (_.remove(mainAvailableHeaders), function (n) {
                            return n == data
                        });
                     if (!data.includes(Object.values(headerConfigDataState))) {

                        let invert = _.invert(fixedHeaderData)

                        let keyget = invert[data];

                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }

                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];


                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {

                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {

                    customHeadersState.push(data)
                    var even = _.remove(mainAvailableHeaders, function (n) {
                        return n == data;
                    });

                    if (!customHeadersState.includes(headerConfigDataState)) {

                        let keyget = (_.invert(fixedHeaderData))[data];


                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)

                    }

                    if (!Object.keys(customHeaders).includes(headerSummary)) {

                        let keygetvalue = (_.invert(fixedHeaderData))[data];

                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeadersState: customHeadersState,
                mainCustomHeaders: customHeaders,
                saveMainState: saveState,
                mainDefaultHeaderMap: defaultHeaderMap,
                mainHeaderSummary: headerSummary,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
        }
        if (this.state.tabVal == 2) {
            let getHeaderConfig = this.state.getSetHeaderConfig
            let customHeadersState = this.state.setCustomHeadersState
            let headerConfigDataState = this.state.setHeaderConfigDataState
            let customHeaders = this.state.setCustomHeaders
            let saveState = this.state.saveSetState
            let defaultHeaderMap = this.state.setDefaultHeaderMap
            let headerSummary = this.state.setHeaderSummary
            let fixedHeaderData = this.state.setFixedHeaderData
            let setAvailableHeaders = this.state.setAvailableHeaders

            if (this.state.setHeaderCondition) {

                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(setAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {

                        let invert = _.invert(fixedHeaderData)

                        let keyget = invert[data];

                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }

                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];


                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {

                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {

                    customHeadersState.push(data)
                    var even = _.remove(setAvailableHeaders, function (n) {
                        return n == data;
                    });

                    if (!customHeadersState.includes(headerConfigDataState)) {

                        let keyget = (_.invert(fixedHeaderData))[data];


                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)

                    }

                    if (!Object.keys(customHeaders).includes(headerSummary)) {

                        let keygetvalue = (_.invert(fixedHeaderData))[data];

                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getSetHeaderConfig: getHeaderConfig,
                setCustomHeadersState: customHeadersState,
                setCustomHeaders: customHeaders,
                saveSetState: saveState,
                setDefaultHeaderMap: defaultHeaderMap,
                setHeaderSummary: headerSummary,
                setAvailableHeaders,
                changesInSetHeaders: true,
            })
        }
        if (this.state.tabVal == 3) {
            let getHeaderConfig = this.state.getItemHeaderConfig
            let customHeadersState = this.state.itemCustomHeadersState
            let headerConfigDataState = this.state.itemHeaderConfigDataState
            let customHeaders = this.state.itemCustomHeaders
            let saveState = this.state.saveItemState
            let defaultHeaderMap = this.state.itemDefaultHeaderMap
            let headerSummary = this.state.itemHeaderSummary
            let fixedHeaderData = this.state.itemFixedHeaderData
            let itemAvailableHeaders = this.state.itemAvailableHeaders

            if (this.state.itemHeaderCondition) {

                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(itemAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {

                        let invert = _.invert(fixedHeaderData)

                        let keyget = invert[data];

                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }

                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];


                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {

                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {

                    customHeadersState.push(data)
                    var even = _.remove(itemAvailableHeaders, function (n) {
                        return n == data;
                    });

                    if (!customHeadersState.includes(headerConfigDataState)) {

                        let keyget = (_.invert(fixedHeaderData))[data];


                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)

                    }

                    if (!Object.keys(customHeaders).includes(headerSummary)) {

                        let keygetvalue = (_.invert(fixedHeaderData))[data];

                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getItemHeaderConfig: getHeaderConfig,
                itemCustomHeadersState: customHeadersState,
                itemCustomHeaders: customHeaders,
                saveItemState: saveState,
                itemDefaultHeaderMap: defaultHeaderMap,
                itemHeaderSummary: headerSummary,
                itemAvailableHeaders,
                changesInItemHeaders: true,
            })
        }

    }
    closeColumn(data) {
        if (this.state.tabVal == 1) {
            let getHeaderConfig = this.state.getMainHeaderConfig
            let headerConfigState = this.state.mainHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.mainCustomHeadersState
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.mainCustomHeadersState.length == 0) {
                    this.setState({
                        headerCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            mainAvailableHeaders.push(data)
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeaders: headerConfigState,
                mainCustomHeadersState: customHeadersState,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.mainFixedHeaderData))[data];
                //console.log((_.invert(this.state.mainFixedHeaderData))[data],"mainFixedHeaderData")
                let saveState = this.state.saveMainState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.mainHeaderSummary
                let defaultHeaderMap = this.state.mainDefaultHeaderMap
                if (!this.state.headerCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    mainHeaderSummary: headerSummary,
                    mainDefaultHeaderMap: defaultHeaderMap,
                    saveMainState: saveState
                })
            }, 100);
        }
        if (this.state.tabVal == 2) {
            let getHeaderConfig = this.state.getSetHeaderConfig
            let headerConfigState = this.state.setHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.setCustomHeadersState
            let fixedHeaderData = this.state.setFixedHeaderData
            let setAvailableHeaders = this.state.setAvailableHeaders

            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.setCustomHeadersState.length == 0) {
                    this.setState({
                        setHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            setAvailableHeaders.push(data)
            this.setState({
                getSetHeaderConfig: getHeaderConfig,
                setCustomHeaders: headerConfigState,
                setCustomHeadersState: customHeadersState,
                setAvailableHeaders,
                changesInSetHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.setFixedHeaderData))[data];
                let saveState = this.state.saveSetState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.setHeaderSummary
                let defaultHeaderMap = this.state.setDefaultHeaderMap
                if (!this.state.setHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    setHeaderSummary: headerSummary,
                    setDefaultHeaderMap: defaultHeaderMap,
                    saveSetState: saveState
                })
            }, 100);
        }
        if (this.state.tabVal == 3) {
            let getHeaderConfig = this.state.getItemHeaderConfig
            let headerConfigState = this.state.itemHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.itemCustomHeadersState
            let fixedHeaderData = this.state.itemFixedHeaderData
            let itemAvailableHeaders = this.state.itemAvailableHeaders

            if (!this.state.itemHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.itemCustomHeadersState.length == 0) {
                    this.setState({
                        itemHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            itemAvailableHeaders.push(data)
            this.setState({
                getItemHeaderConfig: getHeaderConfig,
                itemCustomHeaders: headerConfigState,
                itenCustomHeadersState: customHeadersState,
                itemAvailableHeaders,
                changesInItemHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.itemFixedHeaderData))[data];
                let saveState = this.state.saveItemState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.itemHeaderSummary
                let defaultHeaderMap = this.state.itemDefaultHeaderMap
                if (!this.state.itemHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    itemHeaderSummary: headerSummary,
                    itemDefaultHeaderMap: defaultHeaderMap,
                    saveItemState: saveState
                })
            }, 100);
        }

    }
    saveColumnSetting(e) {
        if (this.state.tabVal == 1) {
            this.setState({
                coloumSetting: false,
                headerCondition: false,
                saveMainState: [],
                changesInMainHeaders: false,
            })
            let payload = {
                basedOn: "ALL",
                module: "SHIPMENT TRACKING",
                subModule: "LOGISTIC",
                section: "LR-INTRANSIT",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "ENT_LR_INTRANSIT_ALL",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainCustomHeaders,
            }

            this.props.createMainHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 2) {
            this.setState({
                coloumSetting: false,
                setHeaderCondition: false,
                saveSetState: [],
                changesInSetHeaders: false,
            })
            let payload = {
                basedOn: "ASN",
                module: "SHIPMENT TRACKING",
                subModule: "LOGISTIC",
                section: "LR-INTRANSIT",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "ENT_LR_INTRANSIT_ASN",
                fixedHeaders: this.state.setFixedHeaderData,
                defaultHeaders: this.state.setHeaderConfigDataState,
                customHeaders: this.state.setCustomHeaders,
            }

            this.props.createSetHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 3) {
            this.setState({
                coloumSetting: false,
                itemHeaderCondition: false,
                saveSetState: [],
                changesInSetHeaders: false,
            })
            let payload = {
                basedOn: "SET",
                module: "SHIPMENT TRACKING",
                subModule: "LOGISTIC",
                section: "LR-INTRANSIT",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "VENDOR_LR_INTRANSIT_ITEM",
                fixedHeaders: this.state.itemFixedHeaderData,
                defaultHeaders: this.state.itemHeaderConfigDataState,
                customHeaders: this.state.itemCustomHeaders,
            }
            this.props.createItemHeaderConfigRequest(payload)
        }
    }
    resetColumnConfirmation() {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // paraMsg: "Click confirm to continue.",
            confirmModal: true,
        })
    }
    resetColumn() {
        const {getMainHeaderConfig,
            mainFixedHeader,
            setFixedHeader,
            getSetHeaderConfig,
            itemFixedHeader,
            getItemHeaderConfig} = this.state
        if (this.state.tabVal == 1) {
            let payload = {
                basedOn: "ALL",
                module: "SHIPMENT TRACKING",
                subModule: "LOGISTIC",
                section: "LR-INTRANSIT",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "ENT_LR_INTRANSIT_ALL",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: {},
            }
            this.props.createMainHeaderConfigRequest(payload)
            let availableHeaders= mainFixedHeader.filter(function (obj) { return getMainHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                    headerCondition: true,
                    coloumSetting: false,
                    saveMainState: [],
                    confirmModal: false,
                    mainAvailableHeaders:availableHeaders
                })
            
        }
        if (this.state.tabVal == 2) {
            let payload = {
                basedOn: "ASN",
                module: "SHIPMENT TRACKING",
                subModule: "LOGISTIC",
                section: "LR-INTRANSIT",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "ENT_LR_INTRANSIT_ASN",
                fixedHeaders: this.state.setFixedHeaderData,
                defaultHeaders: this.state.setHeaderConfigDataState,
                customHeaders: this.state.setHeaderConfigDataState,
            }
            this.props.createSetHeaderConfigRequest(payload)
            let availableHeaders= setFixedHeader.filter(function (obj) { return getSetHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                setHeaderCondition: true,
                coloumSetting: false,
                saveSetState: [],
                confirmModal: false,
                setAvailableHeaders:availableHeaders
            })
        }
        if (this.state.tabVal == 3) {
            let payload = {
                basedOn: "SET",
                module: "SHIPMENT TRACKING",
                subModule: "LOGISTIC",
                section: "LR-INTRANSIT",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "VENDOR_LR_INTRANSIT_ITEM",
                fixedHeaders: this.state.itemFixedHeaderData,
                defaultHeaders: this.state.itemHeaderConfigDataState,
                customHeaders: this.state.itemHeaderConfigDataState,
            }
            this.props.createItemHeaderConfigRequest(payload)
            let availableHeaders= itemFixedHeader.filter(function (obj) { return getItemHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                itemHeaderCondition: true,
                coloumSetting: false,
                saveItemState: [],
                confirmModal: false,
                itemAvailableHeaders:availableHeaders
            })
        }

    }
    onHeadersTabClick = (tabVal) => {
        this.setState({ tabVal: tabVal }, () => {
            if (tabVal === 1) {
                this.openColoumSetting("true")
                this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            } else if (tabVal === 2) {
                this.openColoumSetting("true")
                this.props.getSetHeaderConfigRequest(this.state.setHeaderPayload)
            } else if (tabVal === 3) {
                this.openColoumSetting("true")
                this.props.getItemHeaderConfigRequest(this.state.itemHeaderPayload)
            }
        })
    }

    selectAllAction =(e)=>{
        this.setState({ selectAll: !this.state.selectAll, checkedData: [] },()=>{
            if( this.state.selectAll ){
               let array = []; 
               this.state.shipmentIntransit.map( data =>{
                    array.push(data);
               })
               this.setState({ checkedData: array })
            }
        })
        this.closingAllModal();
    }

    toastMsgForInspection = (e)=>{
        this.setState({
            toastMsg: "Please complete your inspection.",
            toastLoader: true
        })
        setTimeout(() => {
            this.setState({
                toastLoader: false
            })
        }, 8000);
    }

    render() {
        // console.log( 'filtervalue',this.state.filteredValue,'checkFilter', this.state.checkedFilters,'filterItems',this.state.filterItems)
      
        return (
            <div className="container-fluid p-lr-0">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <input type="search" value={this.state.search} onChange={this.onSearch} onKeyDown={this.onSearch} placeholder="Type To Search" />
                                    <img className="search-image" src={SearchImage} />
                                    {this.state.search != "" && <span className="closeSearch"><img src={searchIcon} onClick={this.searchClear} /></span>}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                {this.state.allowGateEntry && (<button type="button" className={this.state.selectedlgtNum != "" && this.state.checkedData.length == 1 ? "gen-approved" : "btnDisabled"} onClick={this.state.selectedlgtNum != "" && this.state.checkedData.length == 1 ? this.handleDeliveryModal : null}>Gate Entry Delivery Confirmation</button>)}
                                <div className="gvpd-download-drop">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                        <span className="generic-tooltip">Export Excel</span>
                                    </button>
                                    {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button" onClick={() => this.xlscsv()}>
                                                    {/* <img src={ExportExcel} /> */}
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                            <li>
                                                <button className="export-excel" type="button" onClick={()=>this.getAllData()}>
                                                    <span className="pi-export-svg">
                                                        <img src={require('../../../../assets/downloadAll.svg')} />
                                                    </span>
                                                Download All</button>
                                            </li>
                                        </ul>}
                                </div>
                                <div className="gvpd-download-drop">
                                    {this.state.checkedData.length >= 1 ? <button className={this.state.showDownloadDrop === true && this.state.checkedData.length >= 1 ? "pi-download pi-download-focus" : "pi-download2"} type="button" onClick={this.showDownloadDrop}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>           
                                        <span className="generic-tooltip">Documents</span>                                                                                                                                                                                                   
                                    </button>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                        : <button className={this.state.showDownload === true && this.state.checkedData.length >= 1 ? "pi-download pi-download-focus" : "pi-download2"} type="button">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                                <path fill="#d8d3d3" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                            </svg>
                                            <span className="generic-tooltip">Documents</span>
                                        </button>}
                                    {this.state.showDownloadDrop ? (
                                        <MultipleDownload  {...this.props} drop="goodsInTrans" checkItems={this.state.checkedData} />
                                    ) : (null)}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Filter</span>
                                        {this.state.filterCount != 0 && <p className="noOfFilter">{this.state.filterCount}</p>}</button>
                                    {/* {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null} */}
                                    {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-lg-12 p-lr-47">
                    {this.state.tagState?this.state.checkedFilters.map((keys,index)=>(
                    <div className="show-applied-filter">
                            {(Object.values(this.state.filterValueForTag)[index]) !== undefined && index===0 ?<button type="button" className="saf-clear-all" onClick={(e)=>this.clearAllTag(e)}>Clear All</button>:null}
                            { (Object.values(this.state.filterValueForTag)[index]) !== undefined && <button type="button" className="saf-btn">{keys}
                            <img onClick={(e)=>this.clearTag(e,index)} src={require('../../../../assets/clearSearch.svg')} />
                    {/* <span className="generic-tooltip">{Object.values(this.state.filteredValue)[index]}</span> */}
                    <span className="generic-tooltip">{typeof (Object.values(this.state.filterValueForTag)[index]) == 'object' ? Object.values(Object.values(this.state.filterValueForTag)[index]).join(',') : Object.values(this.state.filterValueForTag)[index]}</span>
                        </button>}
                    </div>)):''}
                </div>

                <div className="col-md-12 p-lr-47">
                    <div className="vendor-gen-table">
                        <div className="manage-table">
                            {/* <ColoumSetting {...this.state} {...this.props} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} />
                             */}
                            <ColoumSetting {...this.props} {...this.state}
                                handleDragStart={this.handleDragStart}
                                handleDragEnter={this.handleDragEnter}
                                onHeadersTabClick={this.onHeadersTabClick}
                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                closeColumn={(e) => this.closeColumn(e)}
                                saveMainState={this.state.saveMainState}
                                saveSetState={this.state.saveSetState}
                                saveItemState={this.state.saveItemState}
                                tabVal={this.state.tabVal}
                                pushColumnData={this.pushColumnData}
                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                saveColumnSetting={(e) => this.saveColumnSetting(e)}
                            />
                            {/* _________________________Enterprise Table ____________________________ */}
                            <table className="table gen-main-table">
                                <thead>
                                    <React.Fragment>
                                        <tr>
                                            <th className="fix-action-btn">
                                                <ul className="rab-refresh">
                                                    <li className="rab-rinner">
                                                      {this.state.shipmentIntransit.length != 0 && <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.selectAll} name="selectAll" onChange={(e) => this.selectAllAction(e)} /><span className="checkmark1"></span></label>}
                                                      {this.state.shipmentIntransit.length != 0 && this.state.selectAll && <span className="select-all-text">{this.state.shipmentIntransit.length} line items selected</span>}
                                                    </li>
                                                    <li className="rab-rinner">
                                                        <span><img src={Reload} onClick={this.onRefresh}></img></span>
                                                    </li>
                                                </ul>
                                            </th>
                                            {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                                <th key={key} data-key={data} onClick={this.filterHeader}>
                                                  <label data-key={data}>{data}</label>
                                                   <img src={filterIcon} className="imgHead" data-key={data}/>
                                                </th>
                                            )) : this.state.mainCustomHeadersState.map((data, key) => (
                                                <th key={key} data-key={data} onClick={this.filterHeader}>
                                                   <label data-key={data}>{data}</label>
                                                   <img src={filterIcon} className="imgHead" data-key={data}/>
                                                </th>
                                            ))}
                                        </tr>
                                    </React.Fragment>
                                </thead>
                                <tbody>
                                    {this.state.shipmentIntransit.length != 0 ? this.state.shipmentIntransit.map((data, key) => (
                                        <React.Fragment key={key}>
                                            <tr className={this.state.checkedData.some((item) => item.id == data.id) ? "vgt-tr-bg" : ""}>
                                                <td className="fix-action-btn">
                                                    <ul className="table-item-list">
                                                        <li className="til-inner">
                                                            <label className="checkBoxLabel0">
                                                                <input type="checkBox" name="selectEach" checked={this.state.checkedData.some((item) => item.id == data.id)} id={data.id} onChange={(e) => this.handleCheckBox(data)} />
                                                                <span className="checkmark1"></span>
                                                            </label>
                                                        </li>
                                                        <li className="til-inner til-chat-icon">
                                                            <img src={chatIcon} className="height16 displayPointer" data-id="openChat" onClick={(event) => this.openChatBox(event, data)} />
                                                            {this.state.notification.length != 0 && this.state.notification.map((nData) => nData.shipmentId == data.id && <span className="badge chat-notification">{nData.totalNotification}</span>)}
                                                            {!this.state.selectAll && <span className="generic-tooltip">Comment</span>}
                                                        </li>
                                                        <li className="til-inner til-add-btn" id={data.logisticNo} onClick={(e) => this.expandColumn(data.id, e, data)}>
                                                            {this.state.dropOpen && this.state.expandedId == data.id ?
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                    <path fill="#a4b9dd" fillRule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z" />
                                                                </svg> :
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                    <path fill="#a4b9dd" fillRule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z" />
                                                                </svg>}
                                                                {!this.state.selectAll && <span className="generic-tooltip">Expand</span>}
                                                        </li>
                                                        {/* <span className="transit_status m-left-7">In Transit</span> */}
                                                        {/* <div className="checkSeperate checkBoxBorder width100" id={"img" + data.logisticNo} onClick={(e) => this.expandColumn("img" + data.logisticNo, e, data)}>
                                                        </div> */}
                                                    </ul>
                                                </td>
                                                {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                                        <td key={key} >
                                                        <label className="table-td-text">{data[hdata]}</label>
                                                        {this.small(data[hdata]) && <div className="table-tooltip"><label>{data[hdata]}</label></div>}
                                                    </td>
                                                )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        <label className="table-td-text">{data[sdata]}</label>
                                                        {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                                                    </td>
                                                    ))}
                                            </tr>
                                            {/* {this.state.dropOpen && this.state.expandedId == data.id && this.state.actionExpand && <tr><td colSpan="100%" className="expandTd"><ExpandModal {...this.state} {...this.props} set_Display="VENDOR_LR_INTRANSIT_ITEM" item_Display="ENT_LR_INTRANSIT_ITEM" /></td></tr>} */}
                                            {this.state.dropOpen && this.state.expandedId == data.id && this.state.actionExpand && <tr><td colSpan="100%" className="expandTd pad-0"><ExpandTransitNDeliver {...this.state} {...this.props} shipmentTrackingMethod={this.props.shipmentTrackingMethod} subModule="LOGISTIC" section="LR-INTRANSIT" asn_Display="ENT_LR_INTRANSIT_ASN" item_Display="ENT_LR_INTRANSIT_ITEM" /></td></tr>}
                                        </React.Fragment>
                                    )) : <tr className="tableNoData"><td> NO DATA FOUND </td></tr>}

                                </tbody>
                            </table>
                            {/* Enterprise Table End */}
                        </div>
                        <div className="col-md-12 pad-0">
                            <div className="new-gen-pagination">
                                <div className="col-md-6">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                            <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.resultedDataCount}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 p-lr-0">
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination {...this.state} {...this.props} page={this.page}
                                                prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="chatMain">
                    {this.state.openChats.map((data, key) => (
                        <div className={this.state.chatModal == "open" ? "table-chat-box visible-chatbox" : this.state.chatModal == "minimize" ? "table-chat-box visible-chatbox minimize-chatbox" : this.state.chatModal == "openMinimize" ? "table-chat-box visible-chatbox" : "table-chat-box visible-chatbox"} key={key}>
                            <CommentBoxModal {...this.state} {...this.props} module="GOODS_IN_TRANSIT" subModule="COMMENTS" openChatBox={(event, data) => this.openChatBox(event, data)} data={data} onRef={ref => (this.commentBoxRef = ref)} />
                        </div>
                    ))}
                </div>
                {this.state.toastError && <ToastError toastErrorMsg={this.state.toastErrorMsg}  closeToastError={this.closeToastError} />}
                {this.state.shipmentModal && <ShipmentTracking shipmentTrackModal={this.shipmentTrackModal} shipmentTrackingData={this.props.orders.shipmentTracking.data.resource || {}} />}
                {this.state.deliveryModal && <DeliverConfirmationModal handleDeliveryModal={this.handleDeliveryModal} selectedlgtNum={this.state.selectedlgtNum} {...this.state} {...this.props} toastMsgForInspection={this.toastMsgForInspection}/>}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div >
        )
    }
}
