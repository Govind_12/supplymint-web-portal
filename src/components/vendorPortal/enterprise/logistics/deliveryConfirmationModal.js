import React, { Component } from 'react';
import arrowIcon from '../../../../assets/prev-arrow.svg';
import fileIcon from '../../../../assets/fileIcon.svg';
import refreshIcon from '../../../../assets/refresh-block.svg';
import cloudIcon from '../../../../assets/cloud.svg';
import sendIcon from '../../../../assets/send.svg';
import userIcon from '../../../../assets/user.svg';
import moment from 'moment';

const currentDate = moment(new Date()).format("YYYY-MM-DD")
var todayDate = new Date()
todayDate.setDate(todayDate.getDate() - 10);
todayDate = moment(todayDate).format("YYYY-MM-DD")

export default class DeliverConfirmationModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.selectedlgtNum,
            deliveryDate: "",
            deliveryTime: "",
            deliveryStatus: "",
            reason: "",
            remark: "",
            dateerr: false,
            timeerr: false,
            statuserr: false,
            reasonerr: false,
            remarkerr: false,
            deliveryReason: "",
            deliveryReasonErr: "",
            previousDate: false,
            multipleASN: "",
            checkCancleQuantitiy:false
        }
    }
    
    componentDidMount(){
        let payload = {
            no: 1,
            type: 1,
            search: "",
            userType: "entlogi",
            lgtNumber: this.props.checkedData[0].logisticNo,
            status: "SHIPMENT_INTRANSIT"
        }
        this.props.getAllVendorSiDetailRequest(payload)
    }

    componentWillMount() {
        this.setState({
            id: this.props.selectedlgtNum,
        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.logistic.updateDelivery.isSuccess) {
            this.props.handleDeliveryModal
        }
        if( nextProps.logistic.getAllVendorSiDetail.isSuccess && nextProps.logistic.getAllVendorSiDetail.data.resource != null){
            this.setState({
                multipleASN: nextProps.logistic.getAllVendorSiDetail.data.resource
            },()=>{
                this.state.multipleASN.forEach( data => {
                      data.invoiceNumberErr = false,
                      data.noOfCartonErr = false,
                      data.invoiceAmountErr = false
               });
            });
        }
    }
    date() {
        if (this.state.deliveryDate != "") {
            this.setState({
                dateerr: false
            })

        } else {
            this.setState({
                dateerr: true
            })
        }
    }
    time() {
        if (this.state.deliveryTime != "") {
            this.setState({
                timeerr: false
            })
        } else {
            this.setState({
                timeerr: true
            })
        }
    }
    status() {
        if (this.state.deliveryStatus != "") {
            this.setState({
                statuserr: false
            })
        } else {
            this.setState({
                statuserr: true
            })
        }
    }
    reason() {
        if (this.state.reason == "") {
            this.setState({
                reasonerr: true
            })
        } else {
            if( this.state.reason == 'Others' && this.state.remark == "")
               this.setState({ remarkerr: true })
            else
               this.setState({ remarkerr: false }) 
            this.setState({
                reasonerr: false
            })
        }
    }
    remark() {
        if( this.state.reason == 'Others'){
            if (this.state.remark != "") {
                this.setState({
                    remarkerr: false
                })
            } else {
                this.setState({
                    remarkerr: true
                })
            }
        }
    }

    handleChange(e) {
        if (e.target.id == "deliveryTime") {
            this.setState({
                deliveryTime: e.target.value
            }, () => {
                this.time()
            })
        } else if (e.target.id == "deliveryDate") {
            this.setState({
                deliveryDate: e.target.value,
                //previousDate: moment(e.target.value, "YYYY/MM/DD").isBefore(currentDate)
            }, () => {
                this.date();
            })
        } else if (e.target.id == "deliveryStatus") {
            this.setState({
                deliveryStatus: e.target.value,
            }, () => {
                this.status()
            })
        } else if (e.target.id == "reason") {
            this.setState({
                reason: e.target.value
            }, () => {
                this.reason()
            })
        } else if (e.target.id == "remark") {
            this.setState({
                remark: e.target.value
            }
            , () => {
                this.remark()
            })
        }
    }
    handleChangeRadio(e) {
        this.setState({
            reason: e.target.value
        }, () => {
            this.reason();
        })
    }

    handleInvoiceNoValue =(id, e)=> {
        let multipleASN = this.state.multipleASN
        multipleASN.forEach( data => {
            if ( data.shipmentAdviceCode == id ) 
                  data.invoiceNumber = e.target.value
        })
        this.setState({ multipleASN }, () => {
            this.invoiceNumberErr()
        })
    }
    invoiceNumberErr =()=> {
        //let alphaNumPattern = /^[0-9a-zA-Z]+$/;
        let multipleASN = this.state.multipleASN
        multipleASN.forEach( data => {
            //if ( data.invoiceNumber == "" || !alphaNumPattern.test(data.invoiceNumber))
            if ( data.invoiceNumber == "")
                 data.invoiceNumberErr = true
            else 
                 data.invoiceNumberErr = false
        })
        this.setState({ multipleASN })
    }

    handleNoOfCartonValue =(id, e)=> {
        let multipleASN = this.state.multipleASN
        multipleASN.forEach( data => {
            if ( data.shipmentAdviceCode == id ) 
                  data.unitCount = e.target.value
        })
        this.setState({ multipleASN }, () => {
            this.noOfCartonErr()
        })
    }
    noOfCartonErr =()=> {
        let numberPattern = /^[0-9]+$/;
        let multipleASN = this.state.multipleASN;
        multipleASN.forEach( data => {
            if ( data.unitCount == "" || Number(data.unitCount) <= 0 || !numberPattern.test(data.unitCount)) 
                 data.noOfCartonErr = true
            else 
                 data.noOfCartonErr = false
        })
        this.setState({ multipleASN })
    }

    handleInvoiceAmountValue =(id, e)=> {
        let multipleASN = this.state.multipleASN
        multipleASN.forEach( data => {
            if ( data.shipmentAdviceCode == id ) 
                  data.invoiceAmount = e.target.value
        })
        this.setState({ multipleASN }, () => {
            this.invoiceAmountErr()
        })
    }
    invoiceAmountErr =()=> {
        let decimalPattern = /^\d+(\.\d{1,2})?$/
        let multipleASN = this.state.multipleASN
        multipleASN.forEach( data => {
            if ( data.invoiceAmount == "" || Number(data.invoiceAmount) <= 0 || !decimalPattern.test(data.invoiceAmount))
                 data.invoiceAmountErr = true
            else 
                 data.invoiceAmountErr = false
        })
        this.setState({ multipleASN })
    }

    selectStatus = (e, id) => {
        let multipleASN = this.state.multipleASN
        multipleASN.map((data) => (data.shipmentAdviceCode == id && (data.warehouseQcConfirmStatus = e.target.value)))
        this.setState({ multipleASN })
    }

    onSubmit =(e)=> {
        if (this.state.deliveryStatus == "recevied") {
            this.date();
            this.time();
            this.setState({ reasonerr: false, remarkerr: false, reason: "" })
        }else{
            this.setState({ dateerr: false, timeerr: false, date: "", time: ""})
        }
        this.status();
        (this.state.deliveryStatus != "recevied" || this.state.previousDate) && this.reason();
        this.state.reason == "Others" && this.remark();
        this.invoiceNumberErr();
        this.noOfCartonErr();
        this.invoiceAmountErr();
        let flag = false;
        setTimeout(() => {
            this.state.multipleASN.forEach((data) => {
                if (data.invoiceNumberErr == true || data.noOfCartonErr == true || data.invoiceAmountErr == true) {
                    flag = true
                    return;
                }
                if( data.qcStatus == "To Be Done At Warehouse" && (data.warehouseQcConfirmStatus == undefined || data.warehouseQcConfirmStatus == '')){
                    flag = true
                    this.props.toastMsgForInspection();
                }
            });
            const { deliveryStatus, deliveryDate, deliveryTime, remark, reason, statuserr, dateerr, remarkerr, timeerr, reasonerr } = this.state
            if (!statuserr && !dateerr && !remarkerr && !timeerr && !reasonerr && !flag) {
                // multiple ASN's Data updation::
                let multipleASNData = [];
                this.state.multipleASN.forEach( data =>{
                    let payload = {
                        orderId:data.orderId,
                        shipmentId: data.id,
                        invoiceNumber: data.invoiceNumber,
                        invoiceAmount: data.invoiceAmount,
                        noOfCartoon: data.unitCount,
                        qcStatus: data.warehouseQcConfirmStatus == undefined ? "" : data.warehouseQcConfirmStatus
                    }
                    multipleASNData.push(payload)
                });

                let updateDelivery = {
                    isShipmentCancelQty: this.state.deliveryStatus == "recevied" ? 0 : this.state.checkCancleQuantitiy ? 1 : 0,
                    id: this.props.selectedlgtNum,
                    status: this.state.deliveryStatus == "recevied" ? "SHIPMENT_DELIVERED" : "SHIPMENT_UNDELIVERED",
                    remark: this.state.remark,
                    reasonType: this.state.reason,
                    deliveryDate: this.state.deliveryDate != "" ? moment(this.state.deliveryDate).format('YYYY-MM-DD') + "T00:00+05:30" : "",
                    deliveryStatus: this.state.deliveryStatus,
                    deliveryTime: this.state.deliveryTime,
                    logisticsNo: this.props.checkedData[0].logisticNo,
                    vehicleNumber: this.props.checkedData[0].vehicleNo,
                    lrQty: this.props.checkedData[0].lrQty,
                    shipmentDetails: multipleASNData,
                }
                this.props.updateDeliveryRequest(updateDelivery)
            }
        }, 10)
    }

    render() {
        const { previousDate, deliveryDate, deliveryStatus, deliveryTime, reason, remark, statuserr, dateerr, timeerr, reasonerr, remarkerr } = this.state
        return (
            <div>
                <div className="modal">
                    <div className="backdrop modal-backdrop-new"></div>
                    <div className=" display_block">
                        <div className="modal-content deliveryConfimationModal delivery-confimation-modal">
                            <div className="col-md-12 pad-0">
                                <div className="modal_Color selectVendorPopUp">
                                    <div className="modalTop alignMiddle text-left">
                                        <div className="col-md-9 pad-0 alignMiddle">
                                            <img src={arrowIcon} onClick={this.props.handleDeliveryModal} className="displayPointer m-rgt-15" />
                                            <h2 className="displayInline m-rgt-30">Delivery Confirmation</h2>
                                        </div>
                                        <div className="col-md-3 pad-0">
                                            <ul className="dcm-list-item text-right">
                                                {/* <li className=""><button type="button" className="discardBtns " onClick={this.props.handleDeliveryModal}>Close</button> </li> */}
                                                <li><button type="button" className="confirmBtnBlue" onClick={(e) => this.onSubmit(e)}>Confirm</button> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-12 pad-0">
                                        <div className="dcm-asn-status ptb-15">
                                            <div className="dcmasns-number">
                                                <span>LR Number</span>
                                                <span className="dcmasnsn-num">{this.props.checkedData[0].logisticNo}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modalOpertions">
                                        <div className="col-md-12 pad-0">
                                            <div className="col-md-12 pad-0">
                                                <div className="col-md-4 pad-0">
                                                    <label>Received Status <span className="mandatory">*</span></label>
                                                    <select className="selectionGenric imgFocus" id="deliveryStatus" value={deliveryStatus} onChange={(e) => this.handleChange(e)}>
                                                        <option value="">Select Status</option>
                                                        <option value="recevied">Received</option>
                                                        <option value="notRecevied">Not Received</option>
                                                    </select>
                                                    {statuserr ? (
                                                        <span className="error">
                                                            Select Status
                                                        </span>
                                                    ) : null}
                                                </div>
                                                {deliveryStatus !== "notRecevied" && <React.Fragment>
                                                    <div className="col-md-4 pad-0">
                                                        <label>Received Date {deliveryStatus != "notRecevied" && <span className="mandatory">*</span>}</label>
                                                        <input type="date" id="deliveryDate" value={deliveryDate} placeholder={deliveryDate == "" ? "Select Received Date" : deliveryDate} min={todayDate} max={currentDate} className="selectionGenric" onChange={(e) => this.handleChange(e)} />
                                                        {dateerr ? (
                                                            <span className="error">
                                                                Select Date
                                                </span>
                                                        ) : null}
                                                    </div>
                                                    <div className="col-md-4 pad-0">
                                                        <label className="displayBlock">Received Time {deliveryStatus != "notRecevied" && <span className="mandatory">*</span>}</label>
                                                        <input type="time" id="deliveryTime" value={deliveryTime} placeholder={deliveryTime} className="selectionGenric" onChange={(e) => this.handleChange(e)} />
                                                        {timeerr ? (
                                                            <span className="error">
                                                                Select Time
                                                </span>
                                                        ) : null}
                                                    </div>
                                                </React.Fragment>}
                                                {(deliveryStatus == "notRecevied" || previousDate) && <div className="col-md-4 pad-0">
                                                    <label>Reason <span className="mandatory">*</span></label>
                                                    <select className="selectionGenric imgFocus" id="reason" value={reason} onChange={(e) => this.handleChange(e)}>
                                                        <option value="">Select Status</option>
                                                        {this.props.reasons != undefined && console.log(this.props.reasons, deliveryStatus), this.props.reasons[deliveryStatus].map((data) => <option value={data}>{data}</option>)}
                                                        {/* // <option value="SHIPMENT_UNDELIVERED">Not Received</option> */}
                                                    </select>
                                                    {reasonerr ? (
                                                        <span className="error">
                                                            Select Reason
                                                </span>
                                                    ) : null}
                                                </div>}
                                            </div>
                                            {deliveryStatus == "" || deliveryStatus == "recevied" ? "" :
                                            <div className="col-md-12 pad-0 m-top-20">
                                                <div className="cancel-asn-quantity-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" checked={this.state.checkCancleQuantitiy} onChange={()=>this.setState({checkCancleQuantitiy: !this.state.checkCancleQuantitiy})} name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                        Cancel ASN Quantity
                                                    </label>
                                                </div>
                                            </div>}
                                            {/* {deliveryStatus == "SHIPMENT_UNDELIVERED" ? <div className="col-md-12 pad-0 radioGeneric m-top-40">
                                                <label className="displayBlock">Reason <span className="mandatory">*</span></label>
                                                <div className="m-top-10 displayInline width100">
                                                    <label className="inputMain">Involved in accident
                                                    <input type="radio" id="accident" name="reason" value="Involved in accident" checked={this.state.reason == "Involved in accident"} onChange={(e) => this.handleChangeRadio(e)} />
                                                        <span className="spanBefore"></span>
                                                    </label>
                                                    <label className="inputMain">Damage Labels
                                                    <input type="radio" id="accident2" name="reason" value="Damage labels" checked={this.state.reason == "Damage labels"} onChange={(e) => this.handleChangeRadio(e)} />
                                                        <span className="spanBefore"></span>
                                                    </label>
                                                    <label className="inputMain">Others
                                                    <input type="radio" id="other" name="reason" value="Others" checked={this.state.reason == "Others"} onChange={(e) => this.handleChangeRadio(e)} />
                                                        <span className="spanBefore"></span>
                                                    </label>
                                                </div>
                                                {reasonerr ? (
                                                    <span className="error">
                                                        Select Reason
                                                </span>
                                                ) : null}
                                            </div> : null} */}
                                            <div className="col-md-12 pad-0 m-top-20">
                                                <label>Remarks
                                                    {/* <span className="mandatory">*</span> */}
                                                </label>
                                                <textarea placeholder="Write your comment here…" rows="5" columns="50" id="remark" value={remark} className="textareaGeneric" onChange={(e) => this.handleChange(e)} />
                                                {remarkerr ? (<span className="error"> Enter Remarks</span>) : null}
                                                <p className="note m-top-15">Note : These confirmation details will be shared with the respective vendor , please verify all details before confirming the details.</p>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-15">
                                                <div className="dcm-asn-details">
                                                    <h3>ASN Details</h3><span className="dcmad-items">{this.state.multipleASN !== "" && this.state.multipleASN.length } Items</span>
                                                </div>
                                            </div>
                                            {this.state.multipleASN !== "" && Object.values(this.state.multipleASN).map( data => <div>
                                                <div className="col-md-12 pad-0 m-top-25">
                                                    <div className="dcm-asn-status p-lr-0">
                                                        <div className="dcmasns-number">
                                                            <span>PO Number</span>
                                                            <span className="dcmasnsn-num">{data.orderNumber}</span>
                                                        </div>
                                                        <div className="dcmasns-number dcmasns-bdr-lft">
                                                            <span>ASN Number</span>
                                                            <span className="dcmasnsn-num">{data.shipmentAdviceCode}</span>
                                                        </div>
                                                        <div className="dcmasns-number dcmasns-bdr-lft">
                                                            <span>Inspection Status</span>
                                                            <span className="dcmasnsn-status">{data.qcStatus == null ? "NA" : data.qcStatus}</span>
                                                        </div>
                                                        {data.qcStatus === "To Be Done At Warehouse" && <div className="dcmasns-number dcmasns-bdr-lft">
                                                        <span>Status<span className="mandatory">*</span></span>
                                                            <span className="dcmasnsn-num">
                                                                <select id="status-selection" onChange={(e) => this.selectStatus(e, data.shipmentAdviceCode)}>
                                                                    <option value="">Select</option>
                                                                    <option value="Pass">Pass</option>
                                                                    <option value="Fail">Fail</option>
                                                                </select>
                                                            </span>
                                                        </div>}
                                                    </div>
                                                </div>
                                                <div className="col-md-12 pad-0 m-top-5">
                                                    <div className="col-md-4 pad-0">
                                                        <label>Invoice Number</label>
                                                        <input type="text" value={data.invoiceNumber} className="dcm-input" onChange={(e)=>this.handleInvoiceNoValue(data.shipmentAdviceCode, e)}/>
                                                        {data.invoiceNumberErr && <span className="error">Enter Valid Invoice Number.</span>}
                                                    </div>
                                                    <div className="col-md-4 pad-0">
                                                        <label>No. of Cartons</label>
                                                        <input type="text" value={data.unitCount} className="dcm-input" onChange={(e)=>this.handleNoOfCartonValue(data.shipmentAdviceCode, e)}/>
                                                        {data.noOfCartonErr && <span className="error">Enter Valid Number Of Cartons.</span>}
                                                    </div>
                                                    <div className="col-md-4 pad-0">
                                                        <label>Amount</label>
                                                        <input type="text" value={data.invoiceAmount} className="dcm-input-amount" onChange={(e)=>this.handleInvoiceAmountValue(data.shipmentAdviceCode, e)}/>
                                                        <span className="rupee-html-sign">&#8377;</span>
                                                        {data.invoiceAmountErr && <span className="error">Enter Valid Invoice Amount.</span>}
                                                    </div>
                                                </div> 
                                            </div> )}
                                            {/* <div className="col-md-12 pad-0 m-top-20">
                                                <div className="dcm-asn-status p-lr-0">
                                                    <div className="dcmasns-number">
                                                        <span>PO Number</span>
                                                        <span className="dcmasnsn-num">PO00052-0221BSP</span>
                                                    </div>
                                                    <div className="dcmasns-number dcmasns-bdr-lft">
                                                        <span>ASN Number</span>
                                                        <span className="dcmasnsn-num">VM/00052/20-21</span>
                                                    </div>
                                                    <div className="dcmasns-number dcmasns-bdr-lft">
                                                        <span>Inspection Status</span>
                                                        <span className="dcmasnsn-status">To be done later</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-20">
                                                <div className="col-md-4 pad-0">
                                                    <label>Invoice Number</label>
                                                    <input type="text" value="8728947983" className="dcm-input" />
                                                </div>
                                                <div className="col-md-4 pad-0">
                                                    <label>No. of Cartons</label>
                                                    <input type="text" value="12" className="dcm-input" />
                                                </div>
                                                <div className="col-md-4 pad-0">
                                                    <label>Amount</label>
                                                    <input type="text" value="1,206,839" className="dcm-input-amount" />
                                                    <span className="rupee-html-sign">&#8377;</span>
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
