import React, { Component } from 'react';
import plusIcon from '../../../../assets/plus-blue.svg';
import removeIcon from '../../../../assets/removeIcon.svg';
import Pagination from '../../../pagination';
import searchIcon from '../../../../assets/close-recently.svg';

import SetLevelConfigHeader from '../../setLevelConfigHeader'
import SetLevelConfirmationModal from '../../setLevelConfirModal'
import ItemLevelDetails from '../../modals/itemLevelDetails';
const userType = "entpo";
export default class EnterpriseExpandDetailTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: "",
            no: 1,
            type: 1,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            setLvlGetHeaderConfig: [],
            setLvlFixedHeader: [],
            setLvlCustomHeaders: {},
            setLvlHeaderConfigState: {},
            setLvlHeaderConfigDataState: {},
            setLvlFixedHeaderData: [],
            setLvlCustomHeadersState: [],
            setLvlHeaderState: {},
            setLvlHeaderSummary: [],
            setLvlDefaultHeaderMap: [],
            setLvlConfirmModal: false,
            setLvlHeaderMsg: "",
            setLvlParaMsg: "",
            setLvlHeaderCondition: false,
            setLvlSaveState: [],
            setLvlColoumSetting: false,
            actionExpand: false,
            expandedId: 0,
            dropOpen: true,
            setBaseExpandDetails: [],
            setRequestedQty: "",
            setBasedArray: [],
            itemBaseArray: [],
            expandedLineItems: {},
            mandateHeaderSet : []
        }
    }
    componentDidMount() {
        let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            // displayName: this.props.poType == "poicode" ? "ENT_PENDING_ICODE_ITEM" :  this.props.set_Display,
            displayName: this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,
            basedOn: this.props.poType == "poicode" ? "ITEM" : "SET"
        }
        if(this.props.poType == "poicode")
          this.props.getItemHeaderConfigRequest(payload)
        else  
          this.props.getSetHeaderConfigRequest(payload)
        // if (!this.props.replenishment.getHeaderConfig.isSuccess) {
        /* let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,
            basedOn: "SET"
        }
        this.props.getHeaderConfigRequest(payload) */
        // }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.orders.EnterpriseGetPoDetails.isSuccess && prevState.actionExpand == false) {
            return {
                setBaseExpandDetails: nextProps.orders.EnterpriseGetPoDetails.data.resource == null ? [] : nextProps.orders.EnterpriseGetPoDetails.data.resource,
                prev: nextProps.orders.EnterpriseGetPoDetails.data.prePage,
                current: nextProps.orders.EnterpriseGetPoDetails.data.currPage,
                next: nextProps.orders.EnterpriseGetPoDetails.data.currPage + 1,
                maxPage: nextProps.orders.EnterpriseGetPoDetails.data.maxPage,
            }
        }
        if (prevState.actionExpand && nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
            let itemDetails = nextProps.orders.EnterpriseGetPoDetails.data.resource
            let id = prevState.expandedId;
            return {
                expandedLineItems: { ...prevState.expandedLineItems, [id]: itemDetails },
                itemBaseArray: nextProps.orders.EnterpriseGetPoDetails.data.resource
            }

        }
        if (nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
            return {
                expandDetails: nextProps.orders.EnterpriseGetPoDetails.data.resource == null ? [] : nextProps.orders.EnterpriseGetPoDetails.data.resource,
            }
        }
        else if (nextProps.orders.EnterpriseGetPoDetails.isError) {
            return { expandDetails: [] }
        }
        
        return {}
    }
    /* componentWillReceiveProps(nextProps) {
        if (nextProps.orders.EnterpriseGetPoDetails.isSuccess && this.state.actionExpand == false) {
            this.setState({
                setBaseExpandDetails: nextProps.orders.EnterpriseGetPoDetails.data.resource == null ? [] : nextProps.orders.EnterpriseGetPoDetails.data.resource,
                prev: nextProps.orders.EnterpriseGetPoDetails.data.prePage,
                current: nextProps.orders.EnterpriseGetPoDetails.data.currPage,
                next: nextProps.orders.EnterpriseGetPoDetails.data.currPage + 1,
                maxPage: nextProps.orders.EnterpriseGetPoDetails.data.maxPage,
            })
        }


        if (nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
            this.setState({
                expandDetails: nextProps.orders.EnterpriseGetPoDetails.data.resource == null ? [] : nextProps.orders.EnterpriseGetPoDetails.data.resource,
            })
        }
        else if (nextProps.orders.EnterpriseGetPoDetails.isError) {
            this.setState({ expandDetails: [] })
        }
        console.log(nextProps.orders.EnterpriseGetPoDetails)
        if (this.state.actionExpand && nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
            console.log("asdsa")
            let itemDetails = nextProps.orders.EnterpriseGetPoDetails.data.resource
            let id = this.state.expandedId;
            this.setState({
                expandedLineItems: { ...this.state.expandedLineItems, [id]: itemDetails },
                itemBaseArray: nextProps.orders.EnterpriseGetPoDetails.data.resource
            })

        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {

            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "SET") {

                let setLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let setLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let setLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                this.setState({
                    setLvlCustomHeaders: this.state.setLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    setLvlGetHeaderConfig,
                    setLvlFixedHeader,
                    setLvlCustomHeadersState,

                    setLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    setLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    setLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    setLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    setLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet : Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                })
            }
        }
    } */
    //     static getDerivedStateFromProps(nextProps, prevState) {
    // console.log(prevState.actionExpand)
    //     if (nextProps.orders.EnterpriseGetPoDetails.isSuccess && prevState.actionExpand == false) {
    //         return {
    //             setBaseExpandDetails: nextProps.orders.EnterpriseGetPoDetails.data.resource == null ? [] : nextProps.orders.EnterpriseGetPoDetails.data.resource,
    //             prev: nextProps.orders.EnterpriseGetPoDetails.data.prePage,
    //             current: nextProps.orders.EnterpriseGetPoDetails.data.currPage,
    //             next: nextProps.orders.EnterpriseGetPoDetails.data.currPage + 1,
    //             maxPage: nextProps.orders.EnterpriseGetPoDetails.data.maxPage,
    //         }
    //     }


    //     if (nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
    //         return {
    //             expandDetails: nextProps.orders.EnterpriseGetPoDetails.data.resource == null ? [] : nextProps.orders.EnterpriseGetPoDetails.data.resource,
    //         }
    //     }
    //     else if (nextProps.orders.EnterpriseGetPoDetails.isError) {
    //         return { expandDetails: [] }
    //     }
    //     console.log(nextProps.orders.EnterpriseGetPoDetails)
    //     if (prevState.actionExpand && nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
    //         console.log("asdsa")
    //         let itemDetails = nextProps.orders.EnterpriseGetPoDetails.data.resource
    //         let id = prevState.expandedId;
    //        return{
    //             expandedLineItems: { ...prevState.expandedLineItems, [id]: itemDetails },
    //             itemBaseArray: nextProps.orders.EnterpriseGetPoDetails.data.resource
    //         }

    //     }
    //     if (nextProps.replenishment.getHeaderConfig.isSuccess) {

    //         if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "SET") {

    //             let setLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
    //             let setLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
    //             let setLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []


    //             return {

    //                 setLvlCustomHeaders: prevState.setLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
    //                 setLvlGetHeaderConfig,
    //                 setLvlFixedHeader,
    //                 setLvlCustomHeadersState,

    //                 setLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
    //                 setLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
    //                 setLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
    //                 setLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
    //                 setLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],

    //             }


    //         }
    //     }
    //     return {}
    // }
    expandColumn(id, e, data) {
        var img = e.target
        /* if (this.state.expandedLineItems.hasOwnProperty(id)) {
            let arr = [];
            arr = this.state.expandedLineItems[id]
            this.setState({
                itemBaseArray: arr,
                expandedId: id,
                actionExpand: false,
                dropOpen: !this.state.dropOpen
            })
            
        }
        else { */
            if (!this.state.actionExpand || this.state.prevId !== e.target.id) {
                let payload = {
                    id: this.props.expandedId,
                    userType: "entpo",
                    detailType: "item",
                    setHeaderId : data.setHeaderId,
                    poType : this.props.poType,
                }
                this.props.EnterpriseGetPoDetailsRequest(payload)
                this.setState({ actionExpand: true, prevId: e.target.id, expandedId: id, dropOpen: true, order: { orderCode: data.orderCode, orderNumber: data.orderNumber } },
                )
            }
            else {
                this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
            }
        //}
    }

    setLvlOpenColoumSetting(data) {
        if (this.state.setLvlCustomHeadersState.length == 0) {
            this.setState({
                setLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                setLvlColoumSetting: true
            })
        } else {
            this.setState({
                setLvlColoumSetting: false
            })
        }
    }
    setLvlpushColumnData(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlHeaderConfigDataState = this.state.setLvlHeaderConfigDataState
        let setLvlCustomHeaders = this.state.setLvlCustomHeaders
        let setLvlSaveState = this.state.setLvlSaveState
        let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
        let setLvlHeaderSummary = this.state.setLvlHeaderSummary
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (this.state.setLvlHeaderCondition) {

            if (!data.includes(setLvlGetHeaderConfig) || setLvlGetHeaderConfig.length == 0) {
                setLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(setLvlHeaderConfigDataState))) {

                    let invert = _.invert(setLvlFixedHeaderData)

                    let keyget = invert[data];

                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }

                if (!Object.keys(setLvlCustomHeaders).includes(setLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];


                    setLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {

            if (!data.includes(setLvlCustomHeadersState) || setLvlCustomHeadersState.length == 0) {

                setLvlCustomHeadersState.push(data)

                if (!setLvlCustomHeadersState.includes(setLvlHeaderConfigDataState)) {

                    let keyget = (_.invert(setLvlFixedHeaderData))[data];


                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)

                }

                if (!Object.keys(setLvlCustomHeaders).includes(setLvlHeaderSummary)) {

                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];

                    setLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeadersState,
            setLvlCustomHeaders,
            setLvlSaveState,
            setLvlDefaultHeaderMap,
            setLvlHeaderSummary
        })
    }
    setLvlCloseColumn(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlHeaderConfigState = this.state.setLvlHeaderConfigState
        let setLvlCustomHeaders = []
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (!this.state.setLvlHeaderCondition) {
            for (let j = 0; j < setLvlCustomHeadersState.length; j++) {
                if (data == setLvlCustomHeadersState[j]) {
                    setLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlCustomHeadersState.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
            if (this.state.setLvlCustomHeadersState.length == 0) {
                this.setState({
                    setLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < setLvlGetHeaderConfig.length; i++) {
                if (data == setLvlGetHeaderConfig[i]) {
                    setLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlGetHeaderConfig.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
        }
        setLvlCustomHeaders.forEach(e => delete setLvlHeaderConfigState[e]);
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeaders: setLvlHeaderConfigState,
            setLvlCustomHeadersState,
        })
        setTimeout(() => {
            let keygetvalue = (_.invert(this.state.setLvlFixedHeaderData))[data];
            let setLvlSaveState = this.state.setLvlSaveState
            setLvlSaveState.push(keygetvalue)
            let setLvlHeaderSummary = this.state.setLvlHeaderSummary
            let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
            if (!this.state.setLvlHeaderCondition) {
                for (let j = 0; j < setLvlHeaderSummary.length; j++) {
                    if (keygetvalue == setLvlHeaderSummary[j]) {
                        setLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < setLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == setLvlDefaultHeaderMap[i]) {
                        setLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                setLvlHeaderSummary,
                setLvlDefaultHeaderMap,
                setLvlSaveState
            })
        }, 100);
    }
    setLvlsaveColumnSetting(e) {
        this.setState({
            setLvlColoumSetting: false,
            setLvlHeaderCondition: false,
            setLvlSaveState: []
        })
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: "PENDING-ORDERS",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,

            fixedHeaders: this.state.setLvlFixedHeaderData,

            defaultHeaders: this.state.setLvlHeaderConfigDataState,

            customHeaders: this.state.setLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    setLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // setLvlParaMsg: "Click confirm to continue.",
            setLvlConfirmModal: true,
        })
    }
    setLvlResetColumn() {
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: "PENDING-ORDERS",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            setLvlHeaderCondition: true,
            setLvlColoumSetting: false,
            setLvlSaveState: []
        }) 
    }
    setLvlCloseConfirmModal(e) {
        this.setState({
            setLvlConfirmModal: !this.state.setLvlConfirmModal,
        })
    }
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "SET") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName:  this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,
                basedOn: "SET"
        }
        this.props.getSetHeaderConfigRequest(payload)
        }
            
        /* if (this.props.replenishment.createHeaderConfig.isSuccess && this.props.replenishment.createHeaderConfig.data.basedOn == "SET") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName:  this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,
                basedOn: "SET"
            }
            this.props.getHeaderConfigRequest(payload)
        } */
    }
    render() {
        const { search } = this.state
        return (
            <div>
                <div className="gen-vend-sticky-table " id="expandTableMain">
                    <div className="gvst-expend" id="expandedTable" ref={node => this.expandTableref = node}>
                        {/* <SetLevelConfigHeader {...this.props} {...this.state} setLvlColoumSetting={this.state.setLvlColoumSetting} setLvlGetHeaderConfig={this.state.setLvlGetHeaderConfig} setLvlResetColumnConfirmation={(e) => this.setLvlResetColumnConfirmation(e)} setLvlOpenColoumSetting={(e) => this.setLvlOpenColoumSetting(e)} setLvlCloseColumn={(e) => this.setLvlCloseColumn(e)} setLvlPushColumnData={(e) => this.setLvlpushColumnData(e)} setLvlsaveColumnSetting={(e) => this.setLvlsaveColumnSetting(e)} />
                         */}<div className="gvst-inner" >
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn"><label></label></th>
                                        {/* {this.state.setLvlCustomHeadersState.length == 0 ? this.state.setLvlGetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.state.setLvlCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))} */}
                                        {this.props.poType == "poicode" ? 
                                            this.props.itemCustomHeadersState.length == 0 ? this.props.getItemHeaderConfig.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            )) : this.props.itemCustomHeadersState.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            ))
                                        : this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            )) : this.props.setCustomHeadersState.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                        ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.setBaseExpandDetails.length != 0 ? this.state.setBaseExpandDetails.map((data, key) => (
                                        <React.Fragment key={key}>
                                            <tr>
                                                <td className="fix-action-btn">
                                                    <ul className="table-item-list">
                                                    {this.props.poType != "poicode" && <li className="til-inner" id={data.setBarCode} onClick={(e) => this.expandColumn(data.setBarCode, e, data)} ref={node => this.openExpand = node}>
                                                        {this.state.dropOpen && this.state.expandedId == data.setBarCode ? 
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                            <path fill="#a4b9dd" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"/>
                                                        </svg>  : 
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                            <path fill="#a4b9dd" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                                        </svg>}
                                                        </li>}
                                                    </ul>
                                                </td>
                                                {/* {this.state.setLvlHeaderSummary.length == 0 ? this.state.setLvlDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        <label>{data[hdata]}</label>
                                                    </td>
                                                )) : this.state.setLvlHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        <label>{data[sdata]}</label>
                                                    </td>
                                                ))} */}
                                                {this.props.poType == "poicode" ? 
                                                    this.props.itemHeaderSummary.length == 0 ? this.props.itemDefaultHeaderMap.map((hdata, key) => (
                                                        <td key={key} >
                                                            <label>{data[hdata]}</label>
                                                        </td>
                                                    )) : this.props.itemHeaderSummary.map((sdata, keyy) => (
                                                        <td key={keyy} >
                                                            <label>{data[sdata]}</label>
                                                        </td>
                                                    ))
                                                : this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                                        <td key={key} >
                                                            <label>{data[hdata]}</label>
                                                        </td>
                                                    )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                                        <td key={keyy} >
                                                            <label>{data[sdata]}</label>
                                                        </td>
                                                ))}  
                                            </tr>
                                            {this.state.dropOpen && this.state.expandedId == data.setBarCode ? <tr><td colSpan="100%" className="pad-0"> <ItemLevelDetails {...this.state}  {...this.props} item_Display={this.props.item_Display} /> </td></tr> : null}
                                        </React.Fragment>)) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {this.state.setLvlConfirmModal ? <SetLevelConfirmationModal {...this.state} {...this.props} setLvlCloseConfirmModal={(e) => this.setLvlCloseConfirmModal(e)} setLvlResetColumn={(e) => this.setLvlResetColumn(e)} /> : null}
                </div>
            </div>
        )
    }
}
