import React, { Component } from 'react';
import orderIcon from '../../../../assets/order.svg';
import filterBtn from '../../../../assets/filterNew.svg';
import filterIcon from '../../../../assets/headerFilter.svg';
import dropIcon from '../../../../assets/chevron.svg';
import refreshIcon from '../../../../assets/refresh-block.svg';
import searchIcon from '../../../../assets/close-recently.svg';
import DropdownPortal from '../../dropdownPortal';
import Pagination from '../../../pagination';
import EnterpriseExpandDetailTable from './enterpriseExpandDetailTable';
import plusIcon from '../../../../assets/plus-blue.svg';
import removeIcon from '../../../../assets/removeIcon.svg';
import FilterOrders from './filterOrders'
import { CONFIG } from "../../../../config/index";
import { handleChange } from '../../../../helper/index'
import axios from 'axios';
import ColoumSetting from "../../../replenishment/coloumSetting";
import ConfirmationSummaryModal from "../../../replenishment/confirmationReset";
import VendorFilter from '../../vendorFilter';
import chatIcon from '../../../../assets/chatIcon.svg';
import Reload from '../../../../assets/reload.svg';
import CommentBoxModal from '../../commentBoxModal';
import CancelModal from '../../modals/cancelModal';
import MultipleDownload from '../../multipleDownload';
import ToastError from '../../../utils/toastError';

const userType = "entpo"

export default class EnterpriseClosedPO extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: [],
            approvedPO: [],
            search: "",
            type: 1,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            no: 1,
            totalPendingPo: "",
            actionExpand: false,
            expandedId: "",
            expandDetails: [],
            filter: false,
            filterBar: false,
            vendorId: "",
            poNumber: "",
            vendorName: "",
            vendorCity: "",
            vendorState: "",
            poDate: "",
            validTo: "",
            siteDetail: "",
            filterCount: 0,
            status: "APPROVED",
            dropOpen: true,
            sliderDisable: false,
            orderCode: "",
            orderNumber: "",
            order: "",
            getHeaderConfig: [],
            fixedHeader: [],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],
            customHeadersState: [],
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            headerCondition: false,
            saveState: [],
            // Header Filter
            filterKey: "",
            filterType: "",
            prevFilter: "",
            filterItems: {},
            checkedFilters: [],
            //chat box
            openChats: [],
            chatModal: "close",
            notification: [],
            applyFilter: false,
            cancelModal: false,
            mandateHeaderMain: [],
            searchCheck: false,
            selectAll: false,
            selectedItems: 0,
            filteredValue: [],
            jumpPage: 1,
            toastError: false,
            inputBoxEnable: false,
            toastErrorMsg: "",
        }
        // this.cancelRef = React.createRef();
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        });
    }


    closeFilter(e) {
        // e.preventDefault();
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
        this.setState({
            filter: false,
            filterBar: false
        });
    }
    updateFilter(data) {
        this.setState(
            {
                vendorId: data.vendorId,
                poNumber: data.poNumber,
                vendorName: data.vendorName,
                vendorCity: data.vendorCity,
                vendorState: data.vendorState,
                poDate: data.poDate,
                validTo: data.validTo,
                siteDetail: data.siteDetail,
                filterCount: data.filterCount,
                orderCode: data.orderCode,
            })
    }
    componentDidMount() {
        let payload = {
            no: 1,
            type: 1,
            search: "",
            status: "APPROVED",
            poDate: "",
            createdOn: "",
            poNumber: "",
            vendorName: "",
            userType: userType,
        }
        this.props.EnterpriseApprovedPoRequest(payload)
        if (!this.props.replenishment.getHeaderConfig.isSuccess) {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_PENDING_ALL",
                basedOn: "ALL"
            }
            this.props.getHeaderConfigRequest(payload)
        }
        // Call comment notication api after 10 seconds
        this.intervalID = setInterval(() => {
            let commentPayload = {
                module: "ORDER",
                status: "",
                orderNumber: "",
                orderId: ""
            }
            this.props.commentNotificationRequest(commentPayload)
        }, 5000);
    }
    componentWillUnmount() {
        clearInterval(this.intervalID)
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.EnterpriseApprovedPo.isSuccess) {
            this.setState({
                approvedPO: nextProps.orders.EnterpriseApprovedPo.data.resource == null ? [] : nextProps.orders.EnterpriseApprovedPo.data.resource,
                prev: nextProps.orders.EnterpriseApprovedPo.data.prePage,
                current: nextProps.orders.EnterpriseApprovedPo.data.currPage,
                next: nextProps.orders.EnterpriseApprovedPo.data.currPage + 1,
                maxPage: nextProps.orders.EnterpriseApprovedPo.data.maxPage,
                totalPendingPo: nextProps.orders.EnterpriseApprovedPo.data.statusCount,
                selectedItems: nextProps.orders.EnterpriseApprovedPo.data.resultedDataCount,
                jumpPage: nextProps.orders.EnterpriseApprovedPo.data.currPage
            })
        }
        if (nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
            this.setState({
                expandDetails: nextProps.orders.EnterpriseGetPoDetails.data.resource == null ? [] : nextProps.orders.EnterpriseGetPoDetails.data.resource,
            })
        }
        else if (nextProps.orders.EnterpriseGetPoDetails.isError) {
            this.setState({ expandDetails: [] })
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "ALL") {
                let getHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let fixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let customHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                this.setState({
                    filterItems: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
                    customHeaders: this.state.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    getHeaderConfig,
                    fixedHeader,
                    customHeadersState,
                    headerConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    fixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    headerConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    headerSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    defaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderMain: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                })
            }
        }
        if (nextProps.orders.commentNotification.isSuccess) {
            if (nextProps.orders.commentNotification.data.resource != null) {
                this.setState({ notification: nextProps.orders.commentNotification.data.resource })
            }
        }
    }
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createHeaderConfig.isSuccess && this.props.replenishment.createHeaderConfig.data.basedOn == "ALL") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_PENDING_ALL",
                basedOn: "ALL"
            }
            this.props.getHeaderConfigRequest(payload)
        }
        if (this.props.orders.EnterpriseCancelPo.isSuccess) {
            let payload = {
                no: 1,
                type: this.state.type,
                search: this.state.search,
                status: "APPROVED",
                userType: userType,
                filter: this.state.filteredValue
            }
            this.setState({ active: [] })
            this.props.EnterpriseApprovedPoRequest(payload)
        }

    }
    // ____________________--CANCEL PO_____________________________

    cancelRequest = (event) => {
        if (event.target.value == "confirm") {
            //if (this.cancelRef.current.value !== "") {
                let cancelList = this.state.active.map((data) => ({
                    orderId: data.id, status: data.status == "PARTIAL" ? data.status : "CANCELLED",
                    orgId: data.orgId, oldStatus: data.oldStatus, orderNumber: data.orderNumber,
                    documentNumber: data.documentNumber, vendorCode: data.vendorCode,
                    vendorName: data.vendorName, orderQty: data.orderQty, enterpriseSite: data.enterpriseSite
                    , orderDate: data.orderDate, orderAmount: data.orderAmount,
                    cancelRemark: this.cancelRef !== undefined && this.cancelRef.state !== undefined && this.cancelRef.state.remark ? this.cancelRef.state.remark : "",
                        reason: this.cancelRef !== undefined && this.cancelRef.state !== undefined && this.cancelRef.state.reason ? this.cancelRef.state.reason : "",
                }))
                if (this.state.selectAll) {
                    let poCriteria = {
                        pageNo: 0,
                        type: this.state.type,
                        search: this.state.search,
                        status: "APPROVED",
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        ...this.state.filteredValue,
                        cancelRemark: this.cancelRef !== undefined && this.cancelRef.state !== undefined && this.cancelRef.state.remark ? this.cancelRef.state.remark : "",
                        reason: this.cancelRef !== undefined && this.cancelRef.state !== undefined && this.cancelRef.state.reason ? this.cancelRef.state.reason : "",
                    }
                    let payload = {
                        cancelList: [],
                        poCriteria,
                        userType: userType
                    }
                    this.props.EnterpriseCancelPoRequest(payload)
                } else {
                    let data = {
                        cancelList,
                        poCriteria: {},
                        userType: userType
                    }
                    this.props.EnterpriseCancelPoRequest(data)
                }
                this.setState({ cancelModal: false, selectAll: false })
            // } else {
            //     this.setState({
            //         toastMsg: "Remarks Cannot be empty",
            //         toastLoader: true,
            //         loader: false
            //     })
            //     setTimeout(() => {
            //         this.setState({
            //             toastLoader: false
            //         })
            //     }, 2000)
            // }
        } else if (event.target.value == "check") {
            this.setState({ cancelModal: !this.state.cancelModal })
        }
    }
    cancelActive(e, data) {
        let array = [...this.state.active]
        if (e.target.name == "selectEach" && !this.state.selectAll) {
            let deletedItem = {
                orderNumber: data.orderNumber, orderId: data.orderId, id: data.id, status: data.status == "PARTIAL" ? data.status : "CANCELLED", orgId: data.orgId, oldStatus: data.status, orderCode: data.orderCode,
                documentNumber: data.documentNumber, orderNumber: data.orderNumber,
                documentNumber: data.documentNumber, vendorCode: data.vendorCode,
                vendorName: data.vendorName, orderQty: data.poQty, enterpriseSite: data.siteDetails
                , orderDate: data.createdOnDate, orderAmount: data.poAmount
            }
            if (this.state.active.some((item) => item.id == data.id)) {
                array = array.filter((item) => item.id != data.id)
            } else {
                array.push(deletedItem)
            }
            this.setState({ active: array })
        } else if (e.target.name == "selectAll" && array == "") {
            this.setState({ selectAll: !this.state.selectAll })
        }
    }
    onSearch = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                let payload = {
                    no: 1,
                    type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                    search: e.target.value,
                    status: "APPROVED",
                    poDate: "",
                    createdOn: "",
                    poNumber: "",
                    vendorName: "",
                    userType: userType,
                    filter: this.state.filteredValue,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                }
                this.props.EnterpriseApprovedPoRequest(payload)
                this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3, searchCheck: true })
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
                let payload = {
                    no: 1,
                    type: this.state.type == 4 ? 2 : 1,
                    search: "",
                    status: "APPROVED",
                    userType: userType,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    filter: this.state.filteredValue,
                }
                this.props.EnterpriseApprovedPoRequest(payload)
                this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false })
            }
        })
    }
    searchClear = () => {
        if (this.state.type === 3 || this.state.type == 4) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 2 : 1,
                search: "",
                status: "APPROVED",
                poDate: "",
                createdOn: "",
                poNumber: "",
                vendorName: "",
                userType: userType,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue,
            }
            this.props.EnterpriseApprovedPoRequest(payload)
            this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false })
        } else { this.setState({ search: "" }) }
    }

    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.orders.EnterpriseApprovedPo.data.prePage,
                    current: this.props.orders.EnterpriseApprovedPo.data.currPage,
                    next: this.props.orders.EnterpriseApprovedPo.data.currPage + 1,
                    maxPage: this.props.orders.EnterpriseApprovedPo.data.maxPage,
                })
                if (this.props.orders.EnterpriseApprovedPo.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.orders.EnterpriseApprovedPo.data.currPage - 1,
                        search: this.state.search,
                        status: "APPROVED",
                        createdOn: "",
                        userType: userType,
                        vendorId: this.state.vendorId,
                        poNumber: this.state.poNumber,
                        vendorName: this.state.vendorName,
                        vendorCity: this.state.vendorCity,
                        vendorState: this.state.vendorState,
                        poDate: this.state.poDate,
                        validTo: this.state.validTo,
                        siteDetail: this.state.siteDetail,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        filter: this.state.filteredValue
                    }
                    this.props.EnterpriseApprovedPoRequest(data)
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.orders.EnterpriseApprovedPo.data.prePage,
                current: this.props.orders.EnterpriseApprovedPo.data.currPage,
                next: this.props.orders.EnterpriseApprovedPo.data.currPage + 1,
                maxPage: this.props.orders.EnterpriseApprovedPo.data.maxPage,
            })
            if (this.props.orders.EnterpriseApprovedPo.data.currPage != this.props.orders.EnterpriseApprovedPo.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.orders.EnterpriseApprovedPo.data.currPage + 1,
                    search: this.state.search,
                    status: "APPROVED",
                    createdOn: "",
                    userType: userType,
                    vendorId: this.state.vendorId,
                    poNumber: this.state.poNumber,
                    vendorName: this.state.vendorName,
                    vendorCity: this.state.vendorCity,
                    vendorState: this.state.vendorState,
                    poDate: this.state.poDate,
                    validTo: this.state.validTo,
                    siteDetail: this.state.siteDetail,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                    filter: this.state.filteredValue
                }
                this.props.EnterpriseApprovedPoRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.orders.EnterpriseApprovedPo.data.prePage,
                    current: this.props.orders.EnterpriseApprovedPo.data.currPage,
                    next: this.props.orders.EnterpriseApprovedPo.data.currPage + 1,
                    maxPage: this.props.orders.EnterpriseApprovedPo.data.maxPage,
                })
                if (this.props.orders.EnterpriseApprovedPo.data.currPage <= this.props.orders.EnterpriseApprovedPo.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        search: this.state.search,
                        status: "APPROVED",
                        createdOn: "",
                        userType: userType,
                        vendorId: this.state.vendorId,
                        poNumber: this.state.poNumber,

                        vendorName: this.state.vendorName,
                        vendorCity: this.state.vendorCity,
                        vendorState: this.state.vendorState,
                        poDate: this.state.poDate,
                        validTo: this.state.validTo,
                        siteDetail: this.state.siteDetail,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        filter: this.state.filteredValue
                    }
                    this.props.EnterpriseApprovedPoRequest(data)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.orders.EnterpriseApprovedPo.data.prePage,
                    current: this.props.orders.EnterpriseApprovedPo.data.currPage,
                    next: this.props.orders.EnterpriseApprovedPo.data.currPage + 1,
                    maxPage: this.props.orders.EnterpriseApprovedPo.data.maxPage,
                })
                if (this.props.orders.EnterpriseApprovedPo.data.currPage <= this.props.orders.EnterpriseApprovedPo.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.orders.EnterpriseApprovedPo.data.maxPage,
                        search: this.state.search,
                        status: "APPROVED",
                        createdOn: "",
                        userType: userType,
                        vendorId: this.state.vendorId,
                        poNumber: this.state.poNumber,

                        vendorName: this.state.vendorName,
                        vendorCity: this.state.vendorCity,
                        vendorState: this.state.vendorState,
                        poDate: this.state.poDate,
                        validTo: this.state.validTo,
                        siteDetail: this.state.siteDetail,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        filter: this.state.filteredValue
                    }
                    this.props.EnterpriseApprovedPoRequest(data)
                }
            }
        }
    }
    onRefresh = () => {
        let payload = {
            no: 1,
            type: 1,
            search: "",
            status: "APPROVED",
            poDate: "",
            createdOn: "",
            poNumber: "",
            vendorName: "",
            userType: userType
        }
        this.setState({ filteredValue: [], selectAll: false, checkedFilters: [] })
        this.props.EnterpriseApprovedPoRequest(payload)
        this.resetField()
        if (document.getElementsByClassName('rotate180')[0] != undefined) {
            document.getElementsByClassName('rotate180')[0].classList.remove('rotate180')
        }
    }

    expandColumn(id, e, data) {
        var img = e.target
        if (!this.state.actionExpand || this.state.prevId !== id) {
            let payload = {
                id: id,
                userType: "entpo",
                detailType: data.poType == "poicode" ? "item" : "set",
                setHeaderId: "",
                poType: data.poType
            }
            this.props.EnterpriseGetPoDetailsRequest(payload)
            this.setState({ poType: data.poType, actionExpand: true, prevId: id, expandedId: id, dropOpen: true, order: { orderCode: data.orderCode, orderNumber: data.orderNumber } })
        } else {
            this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
        }
    }
    xlscsv() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let payload = {
            pageNo: 1,
            type: this.state.type,
            search: this.state.search,
            status: "APPROVED",
            sortedBy: this.state.sortedBy || "",
            sortedIn: this.state.sortedIn || "",
            ...this.state.filteredValue
        }
        let response = ""
        axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=ENT_PENDING_ORDER_ALL`, payload, { headers: headers })
            .then(res => {
                response = res
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
                setTimeout(()=>{
                    this.setState({
                        toastError: false
                    })
                }, 5000)
            });

    }
    resetField() {
        this.setState({
            search: "",
            vendorId: "",
            poNumber: "",
            vendorName: "",
            vendorCity: "",
            vendorState: "",
            poDate: "",
            validTo: "",
            siteDetail: "",
            filterCount: 0,
            type: 1
        })
    }
    callHandleChange = (e) => {
        var scrollValue = handleChange(e)
        this.setState({ rangeVal: scrollValue.rangeVal, sliderDisable: scrollValue.total == 0 ? false : true })
    }

    openColoumSetting(data) {
        if (this.state.customHeadersState.length == 0) {
            this.setState({
                headerCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                coloumSetting: true
            })
        } else {
            this.setState({
                coloumSetting: false
            })
        }
    }
    pushColumnData(data) {
        let getHeaderConfig = this.state.getHeaderConfig
        let customHeadersState = this.state.customHeadersState
        let headerConfigDataState = this.state.headerConfigDataState
        let customHeaders = this.state.customHeaders
        let saveState = this.state.saveState
        let defaultHeaderMap = this.state.defaultHeaderMap
        let headerSummary = this.state.headerSummary
        let fixedHeaderData = this.state.fixedHeaderData
        if (this.state.headerCondition) {
            if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                getHeaderConfig.push(data)
                if (!data.includes(Object.values(headerConfigDataState))) {
                    let invert = _.invert(fixedHeaderData)
                    let keyget = invert[data];
                    Object.assign(customHeaders, { [keyget]: data })
                    saveState.push(keyget)
                }
                if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                    let keygetvalue = (_.invert(fixedHeaderData))[data];
                    defaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                customHeadersState.push(data)
                if (!customHeadersState.includes(headerConfigDataState)) {
                    let keyget = (_.invert(fixedHeaderData))[data];
                    Object.assign(customHeaders, { [keyget]: data })
                    saveState.push(keyget)
                }
                if (!Object.keys(customHeaders).includes(headerSummary)) {
                    let keygetvalue = (_.invert(fixedHeaderData))[data];
                    headerSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            getHeaderConfig,
            customHeadersState,
            customHeaders,
            saveState,
            defaultHeaderMap,
            headerSummary
        })
    }
    closeColumn(data) {
        let getHeaderConfig = this.state.getHeaderConfig
        let headerConfigState = this.state.headerConfigState
        let customHeaders = []
        let customHeadersState = this.state.customHeadersState
        let fixedHeaderData = this.state.fixedHeaderData
        if (!this.state.headerCondition) {
            for (let j = 0; j < customHeadersState.length; j++) {
                if (data == customHeadersState[j]) {
                    customHeadersState.splice(j, 1)
                }
            }
            for (var key in fixedHeaderData) {
                if (!customHeadersState.includes(fixedHeaderData[key])) {
                    customHeaders.push(key)
                }
            }
            if (this.state.customHeadersState.length == 0) {
                this.setState({
                    headerCondition: false
                })
            }
        } else {
            for (var i = 0; i < getHeaderConfig.length; i++) {
                if (data == getHeaderConfig[i]) {
                    getHeaderConfig.splice(i, 1)
                }
            }
            for (var key in fixedHeaderData) {
                if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                    customHeaders.push(key)
                }
            }
        }
        customHeaders.forEach(e => delete headerConfigState[e]);
        this.setState({
            getHeaderConfig,
            customHeaders: headerConfigState,
            customHeadersState,
        })
        setTimeout(() => {
            let keygetvalue = (_.invert(this.state.fixedHeaderData))[data];
            let saveState = this.state.saveState
            saveState.push(keygetvalue)
            let headerSummary = this.state.headerSummary
            let defaultHeaderMap = this.state.defaultHeaderMap
            if (!this.state.headerCondition) {
                for (let j = 0; j < headerSummary.length; j++) {
                    if (keygetvalue == headerSummary[j]) {
                        headerSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < defaultHeaderMap.length; i++) {
                    if (keygetvalue == defaultHeaderMap[i]) {
                        defaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                headerSummary,
                defaultHeaderMap,
                saveState
            })
        }, 100);
    }
    saveColumnSetting(e) {
        this.setState({
            coloumSetting: false,
            headerCondition: false,
            saveState: []
        })
        let payload = {
            basedOn: "ALL",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: "PENDING-ORDERS",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: "ENT_PENDING_ALL",
            fixedHeaders: this.state.fixedHeaderData,
            defaultHeaders: this.state.headerConfigDataState,
            customHeaders: this.state.customHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    resetColumnConfirmation() {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            confirmModal: true,
        })
    }
    resetColumn() {
        let payload = {
            basedOn: "ALL",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: "PENDING-ORDERS",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: "ENT_PENDING_ALL",
            fixedHeaders: this.state.fixedHeaderData,
            defaultHeaders: this.state.headerConfigDataState,
            customHeaders: this.state.headerConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            headerCondition: true,
            coloumSetting: false,
            saveState: []
        })
    }
    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }
    filterHeader = (event) => {
        // var data = event.target.dataset.key
        var data = event.target.textContent
        var oldActive = document.getElementsByClassName("imgHead");
        if (event.target.classList.contains('rotate180')) {
            event.target.classList.remove("rotate180");
        } else {
            for (var i = 0; i < oldActive.length; i++) {
                oldActive[i].classList.remove("rotate180");
            }
            event.target.classList.add("rotate180");
        } var def = { ...this.state.headerConfigDataState };
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ filterKey, filterType: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            let payload = {
                no: this.state.current,
                type: this.state.type,
                search: this.state.search,
                status: "APPROVED",
                poDate: "",
                createdOn: "",
                poNumber: "",
                vendorName: "",
                userType: userType,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue,
            }
            this.props.EnterpriseApprovedPoRequest(payload)
        })
    }
    submitFilter = () => {
        let payload = {}
        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data] + "T00:00+05:30")))
        let data = {
            no: 1,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            search: this.state.search,
            status: "APPROVED",
            poDate: "",
            createdOn: "",
            poNumber: "",
            vendorName: "",
            userType: userType,
            filter: payload,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
        }
        this.props.EnterpriseApprovedPoRequest(data)

        this.setState({
            filter: false,
            filteredValue: payload,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2
        })
    }
    handleInput = (event) => {
        var applyFilter = this.state.applyFilter
        var value = event.target.value
        var name = event.target.dataset.value
        if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
        }
        this.setState({ [name]: value, applyFilter }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                this.setState({ applyFilter: true })
            }
        })
    }
    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            this.setState({ [data]: "" })
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
    }

    handleInputBoxEnable = (e, data) =>{
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }

    clearFilter = () => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let data = {
                no: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                status: "APPROVED",
                poDate: "",
                createdOn: "",
                poNumber: "",
                vendorName: "",
                userType: userType,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.EnterpriseApprovedPoRequest(data)
        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            selectAll: false,
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
        this.child.closeRecentlyAdd()
        sessionStorage.setItem('login_redirect_queryParam', "") 
    }

    // chat box
    openChatBox = (event, data) => {
        if (event == "close") {
            this.setState({ openChats: [] })
        } else {
            event.preventDefault()
            let handleOpenClose = event.target.dataset.id
            let id = event.target.id
            var arr = [...this.state.openChats]
            if (handleOpenClose == "openChat") {
                if (!this.state.openChats.some((item) => item.orderNumber == data.orderNumber)) {
                    if (arr.length < 1) {
                        let pushData = { id: data.id, orderNumber: data.orderNumber, orderCode: data.orderCode, shipmentId: data.shipmentId, orgID: data.orgId }
                        this.setState(prevState => ({ orderId: data.orderId, documentNumber: data.documentNumber, chatModal: "open", openChats: [...prevState.openChats, pushData] }))
                    }
                }
            } else if (handleOpenClose == "closeChat") {
                if (this.state.openChats.some((item) => item.orderNumber == id)) {
                    let afterRemove = arr.filter((item) => item.orderNumber != id)
                    this.setState({ openChats: afterRemove })
                }
            } else if (id == "minimize") {
                this.setState({ chatModal: "minimize" })
            } else if (id == "openMinimize") {
                this.setState({ chatModal: "openMinimize" })
            }
        }
    }

    small = (str) => {
        if (str != null) {
            if (str.length <= 45) {
                return false;
            }
            return true;
        }
    }
    status = (data) => {
        return (
            <div className="poStatus"> {data.status == "PARTIAL" ? <span className="partialTable partiaTag labelEvent">P</span> : ""}
                {data.poType == "poicode" ? <span className="partialTable setTag labelEvent">N</span>
                    : <span className="partialTable setTag labelEvent">S</span>}
                <div className="poStatusToolTip">
                    {data.status == "PARTIAL" ? <div className="each-tag"><span className="partialTable partiaTag labelEvent">P</span><label>Partial</label></div> : ""}
                    {data.poType == "poicode" ? <div className="each-tag"><span className="partialTable setTag labelEvent">N</span><label>Non-set</label></div> : <div className="each-tag"><span className="partialTable setTag labelEvent">S</span><label>Set</label></div>}
                </div>
            </div>
        )
    }
    selectAllAction = () => {
        let poCriteria = {
            pageNo: 0,
            type: this.state.type,
            search: this.state.search,
            status: "APPROVED",
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
        }
        let data = {
            poCriteria
        }
        this.props.EnterpriseCancelPoRequest(data)
    }
    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                let payload = {
                    no: _.target.value,
                    type: this.state.type,
                    search: this.state.search,
                    status: "APPROVED",
                    userType: userType,
                    filter: this.state.filteredValue,
                    sortedBy: this.state.filterKey,
                    sortedIn: this.state.filterType,
                }
                this.props.EnterpriseApprovedPoRequest(payload)
            }
        }
    }
    closeToastError = () => {
        this.setState({ toastError: false })
    }
    render() {
        const { approvedPO, search, sliderDisable } = this.state
        return (
            <div className="container-fluid p-lr-0">
                <div className="col-md-12 pad-container displayInline pad-0">
                    <div className="col-md-12 pad-0 confirmPoTop alignMiddle border-btm p-lr-47">
                        <div className="col-md-6 pad-0 confirmPoLeft">
                            <div className="orderImg">
                                <img src={orderIcon} />
                            </div>
                            <div className="leftText orderDropDown">
                                <div className="heading-top alignMiddle">
                                    <h1 className="displayBlock">Closed PO</h1>
                                    <div id="dropOrder" className="dropdownPosition">
                                        <DropdownPortal />
                                    </div>
                                </div>
                                <label className="displayBlock m-top-5"><span>{this.state.totalPendingPo}</span> orders pending from vendor till now</label>
                            </div>
                        </div>
                        <div className="confirmPoRight alignMiddle newSearch clearIconPos pagerWidth65 p-lr-0 new-box-shadow">
                            {/* <div className="col-md-7">
                                <input type="search" className="searchWid" value={this.state.search} onChange={this.onSearch} onKeyDown={this.onSearch} placeholder="Search" />
                                {search != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.searchClear} /></span> : null}
                            </div> */}
                            <div className="col-md-12 p-lr-0">
                                <div className="npo-cancel-btn npo-btn">
                                    {this.state.active.length >= 1 || this.state.selectAll ? <button value="check" onClick={this.cancelRequest}>Cancel Order</button>
                                        : <button className="btnDisabled">Cancel Order</button>}
                                </div>
                                <div className="displayInline posRelative npo-btn">
                                    {approvedPO.length != 0 ? <button type="button" className="m-rgt-15" onClick={() => this.xlscsv()}>Export to Excel</button> :
                                        <button type="button" className="m-rgt-15 btnDisabled">Export to Excel</button>}
                                </div>
                                <div className="displayInline posRelative npo-btn">
                                    <button type="button" className="vendorFilterBtn m-rgt-15 posRelative" onClick={(e) => this.openFilter(e)}>Filter<img src={filterBtn} />{this.state.filterCount != 0 && <p className="noOfFilter">{this.state.filterCount}</p>}</button>
                                    {/* {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null} */}
                                    {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)}/>}
                                </div>
                                <div className="displayInline posRelative npo-btn">
                                    <MultipleDownload  {...this.props} drop="entPendingOrder" checkItems={this.state.active} />
                                </div>

                                {/* <div className="dropdown posRelative filterDropDown float_Right npo-btn">
                                    {this.state.approvedPO.length != 0 ? <button onClick={(e) => this.xlscsv(e)}>Export to Excel</button> : <button className="btnDisabled">Export to Excel</button>}
                                    <ul className="pad-0 dropdown-menu">
                                        {this.state.approvedPO.length != 0 ? <li onClick={(e) => this.xlscsv(e)}><label>Export to Excel</label></li> : <li><label className="textDisable">Export to Excel</label></li>}
                                    </ul>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 p-lr-47">
                        <div className="po-new-search">
                            <div className="pons-left">
                                <span><img src={Reload} onClick={this.onRefresh}></img></span>
                                {approvedPO.length != 0 ? (Object.keys(this.state.filteredValue).length != 0 || this.state.searchCheck) && <label className="check-container"><input type="checkBox" checked={this.state.selectAll} name="selectAll" onChange={(e) => this.cancelActive(e, "")} /><span className="checkmark"></span></label> : ""}
                                {approvedPO.length != 0 && this.state.selectAll && <span className="select-all-text">{this.state.selectedItems} line items selected</span>}
                            </div>
                            <div className="pons-right">
                                <input type="search" value={this.state.search} onChange={this.onSearch} onKeyDown={this.onSearch} placeholder="Type To Search" />
                                {search != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.searchClear} /></span> : null}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 pad-0 p-lr-47">
                        <div className="vendorBottom" >
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 vendorTableGeneric tableGeneric mainRoleTable vendorTable">
                                <ColoumSetting {...this.props} {...this.state} coloumSetting={this.state.coloumSetting} getHeaderConfig={this.state.getHeaderConfig} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} />
                                <div className="zui-wrapper ">
                                    <div className="scrollableOrgansation scrollableTableFixed vendorTableGeneric roleTableHeight cancelPoTable" id="scrollDiv">
                                        <table className="table newTrExpandTable zui-table roleTable border-bot-table tableWhiteSpaceInherit oveflowC">
                                            <thead>
                                                <tr>
                                                    <th className="fixed-side-role fixed-side1"><label></label></th>
                                                    {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                                                        <th key={key} onClick={this.filterHeader} data-key={data} className="imgHead" >
                                                            <label>{data} <img src={filterIcon} className="imgHead" /></label>
                                                        </th>
                                                    )) : this.state.customHeadersState.map((data, key) => (
                                                        <th key={key} onClick={this.filterHeader} data-key={data} className="imgHead" >
                                                            <label>{data} <img src={filterIcon} className="imgHead" /></label>
                                                        </th>
                                                    ))}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {approvedPO.length != 0 ? approvedPO.map((data, key) => (
                                                    <React.Fragment key={key}>
                                                        <tr>
                                                            <td className="fixed-side-role fixed-side1">
                                                                <div className="checkSeperate checkBoxBorder displayContent">
                                                                    <label className="checkBoxLabel0"><input type="checkBox" name="selectEach" checked={this.state.active.some((item) => item.id == data.id)} id={data.id} onChange={(e) => this.cancelActive(e, data)} /><span className="checkmark1"></span> </label>
                                                                    {this.state.notification.length != 0 && this.state.notification.map((nData) => nData.orderId == data.orderId && <span className="badge chat-notification">{nData.totalNotification}</span>)}
                                                                    <img src={chatIcon} className="height16 displayPointer" data-id="openChat" onClick={(event) => this.openChatBox(event, data)} />
                                                                    <div className="actionBtn displayPointer" onClick={(e) => this.expandColumn(data.id, e, data)}>
                                                                        {/* <label className="displayPointer">Action</label> */}
                                                                        <img src={this.state.dropOpen && this.state.expandedId == data.id ? removeIcon : plusIcon} className="height12 displayPointer" />
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            {this.state.headerSummary.length == 0 ? this.state.defaultHeaderMap.map((hdata, key) => (
                                                                <td key={key} >
                                                                    {hdata == "orderNumber" ? <React.Fragment><label className="fontWeig600 whiteUnset wordBreakInherit">{data["orderNumber"]} </label>{this.status(data)}</React.Fragment>
                                                                        : <label>{data[hdata]}</label>}
                                                                </td>
                                                            )) : this.state.headerSummary.map((sdata, keyy) => (
                                                                <td key={keyy} >
                                                                    {sdata == "orderNumber" ? <React.Fragment><label className="table-td-text fontWeig600 whiteUnset wordBreakInherit">{data["orderNumber"]} </label>{this.status(data)}</React.Fragment>
                                                                        : <label className="table-td-text">{data[sdata]}</label>}
                                                                    {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}

                                                                    {/* <label>{data[sdata]}</label> */}
                                                                </td>
                                                            ))}
                                                        </tr>
                                                        {this.state.dropOpen && this.state.expandedId == data.id && this.state.actionExpand ? <tr><td colSpan="100%" className="expandTd"> <EnterpriseExpandDetailTable {...this.state} {...this.props} subModule="ORDERS" section="PENDING-ORDERS" subModule="ORDERS" set_Display="ENT_PENDING_SET" item_Display="ENT_PENDING_ITEM" inputValue="true" /> </td></tr> : null}
                                                    </React.Fragment>

                                                )) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                {/* -----------------------------------------------------------expanded Table-----------------------------*/}
                                {/* -----------------------------------------------------------expanded Table End----------------------------- */}
                            </div>
                            <div className="col-md-12 pad-0 displayInline" >
                                <div className="col-md-6 p-lr-0">
                                    <div className="table-page-no m-top-20">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                    </div>
                                </div>
                                <div className="col-md-6 p-lr-0">
                                    <div className="pagerDiv newPagination text-right justifyEnd alignMiddle displayFlex nt-btn">
                                        {/* <img src={refreshIcon} className="bottomRefresh" onClick={this.onRefresh} /> */}
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="chatMain">
                    {this.state.openChats.map((data, key) => (
                        <div className={this.state.chatModal == "open" ? "table-chat-box visible-chatbox" : this.state.chatModal == "minimize" ? "table-chat-box visible-chatbox minimize-chatbox" : this.state.chatModal == "openMinimize" ? "table-chat-box visible-chatbox" : "table-chat-box visible-chatbox"} key={key}>
                            <CommentBoxModal {...this.state} {...this.props} module="ORDER" subModule="COMMENTS" openChatBox={(event, data) => this.openChatBox(event, data)} data={data} />
                        </div>
                    ))}
                </div>
                {this.state.toastError && <ToastError toastErrorMsg={this.state.toastErrorMsg} closeToastError={this.closeToastError} />}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                {this.state.cancelModal && <CancelModal onRef={ref => (this.cancelRef = ref)} cancelRequest={this.cancelRequest} {...this.props} {...this.state} type=""/>}
            </div>
        )
    }
}
