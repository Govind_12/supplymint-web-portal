import React, { Component } from 'react';
import Pagination from '../../../pagination';
import ToastLoader from '../../../loaders/toastLoader';
import searchIcon from '../../../../assets/close-recently.svg';
import removeIcon from '../../../../assets/removeIcon.svg';
import plusIcon from '../../../../assets/plus-blue.svg';
import ItemLevelDetails from '../../modals/itemLevelDetails';
import SetLevelConfigHeader from '../../setLevelConfigHeader';
import SetLevelConfirmationModal from '../../setLevelConfirModal';

export default class ExpandEnterpriseModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            poDetailsShipment: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            toastLoader: false,
            toastMsg: "",
            setLvlGetHeaderConfig: [],
            setLvlFixedHeader: [],
            setLvlCustomHeaders: {},
            setLvlHeaderConfigState: {},
            setLvlHeaderConfigDataState: {},
            setLvlFixedHeaderData: [],
            setLvlCustomHeadersState: [],
            setLvlHeaderState: {},
            setLvlHeaderSummary: [],
            setLvlDefaultHeaderMap: [],
            setLvlConfirmModal: false,
            setLvlHeaderMsg: "",
            setLvlParaMsg: "",
            setLvlHeaderCondition: false,
            setLvlSaveState: [],
            setLvlColoumSetting: false,
            actionExpand: false,
            expandedId: 0,
            dropOpen: true,
            setBaseExpandDetails: [],
            setRequestedQty: "",
            setBasedArray: [],
            itemBaseArray: [],
            expandedLineItems: {},
            poType : this.props.poType,
            mandateHeaderSet : [],
        }
    }
    componentDidMount() {
         let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
            basedOn: "SET"
        }
        this.props.getSetHeaderConfigRequest(payload)
        /* this.props.getHeaderConfigRequest(payload)  
        if (!this.props.replenishment.getSetHeaderConfig.isSuccess) {
            this.props.getSetHeaderConfigRequest(this.props.setHeaderPayload)
        } */
    }
    /* componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getCompleteDetailShipment.isSuccess && this.state.actionExpand == false) {
            if (nextProps.shipment.getCompleteDetailShipment.data.resource != null) {
                this.setState({
                    poDetailsShipment: nextProps.shipment.getCompleteDetailShipment.data.resource,
                    prev: nextProps.shipment.getCompleteDetailShipment.data.prePage,
                    current: nextProps.shipment.getCompleteDetailShipment.data.currPage,
                    next: nextProps.shipment.getCompleteDetailShipment.data.currPage + 1,
                    maxPage: nextProps.shipment.getCompleteDetailShipment.data.maxPage,
                })
            } else {
                this.setState({
                    poDetailsShipment: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.shipment.getCompleteDetailShipment.isSuccess && this.state.actionExpand) {
            this.setState(prevState => ({
                itemBaseArray: nextProps.shipment.getCompleteDetailShipment.data.resource.poDetails
            }))
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "SET") {
                let setLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let setLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let setLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                this.setState({
                    setLvlCustomHeaders: this.state.setLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    setLvlGetHeaderConfig,
                    setLvlFixedHeader,
                    setLvlCustomHeadersState,

                    setLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    setLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    setLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    setLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    setLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderSet : Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                })
            }
        }
    } */
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "SET") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: this.props.poType == "poicode" ? this.props.item_Display : this.props.set_Display,
                basedOn: "SET"
            }
            this.props.getSetHeaderConfigRequest(payload)
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.shipment.getCompleteDetailShipment.isSuccess && prevState.actionExpand == false) {
            if (nextProps.shipment.getCompleteDetailShipment.data.resource != null) {
                return {
                    poDetailsShipment: nextProps.shipment.getCompleteDetailShipment.data.resource,
                    prev: nextProps.shipment.getCompleteDetailShipment.data.prePage,
                    current: nextProps.shipment.getCompleteDetailShipment.data.currPage,
                    next: nextProps.shipment.getCompleteDetailShipment.data.currPage + 1,
                    maxPage: nextProps.shipment.getCompleteDetailShipment.data.maxPage,
                }
            } else {
                return {
                    poDetailsShipment: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                }
            }
        }
        if (nextProps.shipment.getCompleteDetailShipment.isSuccess && prevState.actionExpand) {
            return {
                itemBaseArray: nextProps.shipment.getCompleteDetailShipment.data.resource.poDetails
            }
        }
        
        return {}
    }
    searchShipment = (e) => {
        if (e.keyCode == 13) {
            if (this.state.search == "") {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 1000)
            } else {
                this.setState({ type: 3 })
                let payload = {
                    no: 1,
                    type: 3,
                    search: this.state.search,
                    sa_code: this.props.advicedCode,
                    status: this.props.status
                }
                this.props.getCompleteDetailShipmentRequest(payload)
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.type == 3 && this.state.search == "") {
                let data = {
                    no: 1,
                    type: 1,
                    search: "",
                    sa_code: this.props.advicedCode,
                    status: this.props.status
                }
                this.props.getCompleteDetailShipmentRequest(data)
                this.setState({ type: 1 })
            }
        })

    }
    searchClear = () => {
        if (this.state.type == 3) {
            let data = {
                no: 1,
                type: 1,
                search: "",
                sa_code: this.props.advicedCode,
                status: this.props.status
            }
            this.props.getCompleteDetailShipmentRequest(data)
            this.setState({ type: 1, search: "" })
        } else {
            this.setState({ search: "" })
        }
    }

    expandColumn(id, e, data) {
        var img = e.target
        if (this.state.expandedLineItems.hasOwnProperty(id)) {
            let arr = [];
            arr = this.state.expandedLineItems[id]
            this.setState({
                itemBaseArray: arr,
                expandedId: id,
                dropOpen: !this.state.dropOpen
            })
        }
        else {
            if (!this.state.actionExpand || this.state.prevId !== e.target.id) {
                let payload = {
                    orderId: this.props.orderId,
                    setHeaderId: data.setHeaderId,
                    detailType: "item",
                    poType: this.props.poType,
                    shipmentStatus: this.props.moduleStatus,
                    shipmentId: this.props.expandedId
                }
                this.props.getItemHeaderConfigRequest(this.props.itemHeaderPayload)
                this.props.getCompleteDetailShipmentRequest(payload)
                this.setState({ actionExpand: true, prevId: e.target.id, expandedId: id, dropOpen: true, order: { orderCode: data.orderCode, orderNumber: data.orderNumber } },
                )
            }
            else {
                this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
            }
        }
    }

    //Dynaic Header
    setLvlOpenColoumSetting(data) {
        if (this.state.setLvlCustomHeadersState.length == 0) {
            this.setState({
                setLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                setLvlColoumSetting: true
            })
        } else {
            this.setState({
                setLvlColoumSetting: false
            })
        }
    }
    setLvlpushColumnData(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlHeaderConfigDataState = this.state.setLvlHeaderConfigDataState
        let setLvlCustomHeaders = this.state.setLvlCustomHeaders
        let setLvlSaveState = this.state.setLvlSaveState
        let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
        let setLvlHeaderSummary = this.state.setLvlHeaderSummary
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (this.state.setLvlHeaderCondition) {

            if (!data.includes(setLvlGetHeaderConfig) || setLvlGetHeaderConfig.length == 0) {
                setLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(setLvlHeaderConfigDataState))) {
                    let invert = _.invert(setLvlFixedHeaderData)
                    let keyget = invert[data];
                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)
                }

                if (!Object.keys(setLvlCustomHeaders).includes(setLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];


                    setLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {

            if (!data.includes(setLvlCustomHeadersState) || setLvlCustomHeadersState.length == 0) {

                setLvlCustomHeadersState.push(data)

                if (!setLvlCustomHeadersState.includes(setLvlHeaderConfigDataState)) {

                    let keyget = (_.invert(setLvlFixedHeaderData))[data];


                    Object.assign(setLvlCustomHeaders, { [keyget]: data })
                    setLvlSaveState.push(keyget)

                }

                if (!Object.keys(setLvlCustomHeaders).includes(setLvlHeaderSummary)) {

                    let keygetvalue = (_.invert(setLvlFixedHeaderData))[data];

                    setLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeadersState,
            setLvlCustomHeaders,
            setLvlSaveState,
            setLvlDefaultHeaderMap,
            setLvlHeaderSummary
        })
    }
    setLvlCloseColumn(data) {
        let setLvlGetHeaderConfig = this.state.setLvlGetHeaderConfig
        let setLvlHeaderConfigState = this.state.setLvlHeaderConfigState
        let setLvlCustomHeaders = []
        let setLvlCustomHeadersState = this.state.setLvlCustomHeadersState
        let setLvlFixedHeaderData = this.state.setLvlFixedHeaderData
        if (!this.state.setLvlHeaderCondition) {
            for (let j = 0; j < setLvlCustomHeadersState.length; j++) {
                if (data == setLvlCustomHeadersState[j]) {
                    setLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlCustomHeadersState.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
            if (this.state.setLvlCustomHeadersState.length == 0) {
                this.setState({
                    setLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < setLvlGetHeaderConfig.length; i++) {
                if (data == setLvlGetHeaderConfig[i]) {
                    setLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in setLvlFixedHeaderData) {
                if (!setLvlGetHeaderConfig.includes(setLvlFixedHeaderData[key])) {
                    setLvlCustomHeaders.push(key)
                }
            }
        }
        setLvlCustomHeaders.forEach(e => delete setLvlHeaderConfigState[e]);
        this.setState({
            setLvlGetHeaderConfig,
            setLvlCustomHeaders: setLvlHeaderConfigState,
            setLvlCustomHeadersState,
        })
        setTimeout(() => {
            let keygetvalue = (_.invert(this.state.setLvlFixedHeaderData))[data];
            let setLvlSaveState = this.state.setLvlSaveState
            setLvlSaveState.push(keygetvalue)
            let setLvlHeaderSummary = this.state.setLvlHeaderSummary
            let setLvlDefaultHeaderMap = this.state.setLvlDefaultHeaderMap
            if (!this.state.setLvlHeaderCondition) {
                for (let j = 0; j < setLvlHeaderSummary.length; j++) {
                    if (keygetvalue == setLvlHeaderSummary[j]) {
                        setLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < setLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == setLvlDefaultHeaderMap[i]) {
                        setLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                setLvlHeaderSummary,
                setLvlDefaultHeaderMap,
                setLvlSaveState
            })
        }, 100);
    }
    setLvlsaveColumnSetting(e) {
        this.setState({
            setLvlColoumSetting: false,
            setLvlHeaderCondition: false,
            setLvlSaveState: []
        })
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: "SHIPMENT",
            section: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    setLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // setLvlParaMsg: "Click confirm to continue.",
            setLvlConfirmModal: true,
        })
    }
    setLvlResetColumn() {
        let payload = {
            basedOn: "SET",
            module: "SHIPMENT TRACKING",
            subModule: this.props.section,
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.poType == "poicode" ? this.props.item_Display :  this.props.set_Display,
            fixedHeaders: this.state.setLvlFixedHeaderData,
            defaultHeaders: this.state.setLvlHeaderConfigDataState,
            customHeaders: this.state.setLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            setLvlHeaderCondition: true,
            setLvlColoumSetting: false,
            setLvlSaveState: []
        })
    }
    setLvlCloseConfirmModal(e) {
        this.setState({
            setLvlConfirmModal: !this.state.setLvlConfirmModal,
        })
    }
    render() {
        console.log(this.props.poType)
        return (
            <div className="gen-vend-sticky-table">
                <div className="gvst-expend" id="expandTableMain">
                    {/* <SetLevelConfigHeader {...this.props} {...this.state} setLvlColoumSetting={this.state.setLvlColoumSetting} setLvlGetHeaderConfig={this.state.setLvlGetHeaderConfig} setLvlResetColumnConfirmation={(e) => this.setLvlResetColumnConfirmation(e)} setLvlOpenColoumSetting={(e) => this.setLvlOpenColoumSetting(e)} setLvlCloseColumn={(e) => this.setLvlCloseColumn(e)} setLvlPushColumnData={(e) => this.setLvlpushColumnData(e)} setLvlsaveColumnSetting={(e) => this.setLvlsaveColumnSetting(e)} />
                     */}
                    <div className="gvst-inner">
                        <table className="table">
                            <thead>
                                <tr>
                                    <React.Fragment>
                                        <th className="fix-action-btn"><label></label></th>
                                        {/* {this.state.setLvlCustomHeadersState.length == 0 ? this.state.setLvlGetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.state.setLvlCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))} */}
                                        {this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            )) : this.props.setCustomHeadersState.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            ))}
                                    </React.Fragment>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.poDetailsShipment.length != 0 ? this.state.poDetailsShipment.poDetails != undefined && this.state.poDetailsShipment.poDetails != "" ?
                                    this.state.poDetailsShipment.poDetails.map((data, key) => (
                                        <React.Fragment key={key}>
                                            <tr>
                                                <td className="fix-action-btn">
                                                    <ul className="table-item-list">
                                                        {this.state.poType != "poicode" && <li className="til-inner" id={data.setBarCode} onClick={(e) => this.expandColumn(data.setBarCode, e, data)} ref={node => this.openExpand = node}>
                                                            {this.state.dropOpen && this.state.expandedId == data.setBarCode ? 
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                <path fill="#a4b9dd" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"/>
                                                            </svg>  : 
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                <path fill="#a4b9dd" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                                            </svg>}
                                                        </li>}
                                                    </ul>
                                                </td>
                                                {/* {this.state.setLvlHeaderSummary.length == 0 ? this.state.setLvlDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        {hdata == "newRequestedQty" ? <input type="text" value={data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                            hdata == "setPendingQty" ? <label>{data["pendingQty"]}</label> :
                                                                hdata == "newCancelledQty" ? <input type="text" value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[hdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                    <label>{data[hdata]}</label>}
                                                    </td>
                                                )) : this.state.setLvlHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        {sdata == "newRequestedQty" ? <input type="text" value={data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                            sdata == "setPendingQty" ? <label>{data["pendingQty"]}</label> :
                                                                sdata == "newCancelledQty" ? <input type="text" value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[sdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                    <label>{data[sdata]}</label>}
                                                    </td>
                                                ))} */}
                                                {this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                                        <td key={key} >
                                                        {hdata == "newRequestedQty" ? <input type="text" value={data["requestedQty"] != undefined ? data["requestedQty"] : data[hdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                            hdata == "setPendingQty" ? <label>{data["pendingQty"]}</label> :
                                                                hdata == "newCancelledQty" ? <input type="text" value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[hdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                    <label>{data[hdata]}</label>}
                                                    </td>
                                                    )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                                        <td key={keyy} >
                                                        {sdata == "newRequestedQty" ? <input type="text" value={data["requestedQty"] != undefined ? data["requestedQty"] : data[sdata]} id="requestedQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                            sdata == "setPendingQty" ? <label>{data["pendingQty"]}</label> :
                                                                sdata == "newCancelledQty" ? <input type="text" value={data["cancelledQty"] != undefined ? data["cancelledQty"] : data[sdata]} id="cancelQty" onChange={(e) => this.handleQuantityChange(e, data)} /> :
                                                                    <label>{data[sdata]}</label>}
                                                    </td>
                                                    ))}
                                            </tr>
                                            {this.state.dropOpen && this.state.expandedId == data.setBarCode ? <tr><td colSpan="100%" className="pad-0"> <ItemLevelDetails {...this.state}  {...this.props} item_Display={this.props.item_Display} /> </td></tr> : null}
                                        </React.Fragment>
                                    ))
                                    : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr> : ""}
                            </tbody>
                        </table>
                    </div>
                    {this.state.setLvlConfirmModal ? <SetLevelConfirmationModal {...this.state} {...this.props} setLvlCloseConfirmModal={(e) => this.setLvlCloseConfirmModal(e)} setLvlResetColumn={(e) => this.setLvlResetColumn(e)} /> : null}
                    {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
                    {/* -----------------------------------------------------------expanded Table End----------------------------- */}
                </div>
            </div>
        )
    }
}