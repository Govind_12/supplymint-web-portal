import React from 'react'
import arrowIcon from '../../../../assets/prev-arrow.svg';
import fileIcon from '../../../../assets/fileIcon.svg';
import refreshIcon from '../../../../assets/refresh-block.svg';
import cloudIcon from '../../../../assets/cloud.svg';
import sendIcon from '../../../../assets/send.svg';
import userIcon from '../../../../assets/user.svg';
import checkIcon from '../../../../assets/success-summary.svg';
import { CONFIG } from "../../../../config/index";
import moment from 'moment';
import FilterLoader from "../../../loaders/filterLoader";
import ToastLoader from "../../../loaders/toastLoader";
import axios, { post } from "axios";

export default class QualityCheckModalEnterprise extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            poDetailsShipment: this.props.poDetailsShipment,
            allComment: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            comment: "",
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
            callAddComments: false,
            qualitySatisfy: "",
            reason: "",
            fileUploadQc: "",
            QC: [],
            invoice: [],
            isQCDone: "",
            selectedFiles: [],
            isInvoiceUploaded: "",
        }
    }
    handleChange(e) {
        this.setState({
            comment: e.target.value
        })

    }
    componentWillMount() {
        this.setState({
            poDetailsShipment: this.props.poDetailsShipment
        })

        let payload = {
            shipmentId: this.props.shipmentId,
            poId: this.props.poId,
            module: "SHIPMENT_CONFIRMED",
            no: this.state.current + 1,
            orderCode: this.props.order.orderCode,
            orderNumber: this.props.order.orderNumber
        }
        this.props.getAllCommentRequest(payload)
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getAllComment.isSuccess) {

            if (nextProps.shipment.getAllComment.data.resource != null) {

                let allComment = this.state.allComment
                let commentData = this.state.allComment.length == 0 ? nextProps.shipment.getAllComment.data.resource.COMMENTS : { ...allComment.reverse(), ...nextProps.shipment.getAllComment.data.resource.COMMENTS }
                let QCData = nextProps.shipment.getAllComment.data.resource.QC
                QCData.map((data) => data.qcVersion = "OLD")

                let invoice = nextProps.shipment.getAllComment.data.resource.INVOICE
                invoice.map((data) => data.invoiceVersion = "OLD")

                let reverseData = commentData.reverse()
                for (let i = 0; i < reverseData.length; i++) {
                    reverseData[i].commentVersion = "OLD"
                }
                this.setState({
                    allComment: reverseData,
                    isQCDone: nextProps.shipment.getAllComment.data.resource.isQCDone,
                    isInvoiceUploaded: nextProps.shipment.getAllComment.data.resource.isInvoiceUploaded,
                    // reason : nextProps.shipment.getAllComment.data.resource.reason,
                    qualitySatisfy: nextProps.shipment.getAllComment.data.resource.QCStatus,
                    QC: QCData,
                    invoice,
                    current: nextProps.shipment.getAllComment.data.currPage,
                    maxPage: nextProps.shipment.getAllComment.data.maxPage,
                }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
            } else {
                this.setState({
                    allComment: [],

                })
            }
        }

    }

    loadComments() {
        let payload = {
            shipmentId: this.props.shipmentId,
            poId: this.props.poId,
            module: "SHIPMENT_CONFIRMED",
            orderCode: this.props.order.orderCode,
            orderNumber: this.props.order.orderNumber
        }
        this.props.getAllCommentRequest(payload)
    }
    addKey(e) {
        if (e.key == "Enter") {
            this.addComment(e.target.value)
        }
    }
    addComment(data) {
        if (data != "") {
            let date = new Date()
            let allComment = this.state.allComment
            let payload = {
                commentedBy: sessionStorage.getItem("prn"),
                comments: data,
                commentDate: moment(date).format("DD MMM YYYY HH:MM"),
                commentType: "TEXT",
                folderDirectory: "",
                folderName: "",
                filePath: "",
                fileName: "",
                fileURL: "",
                urlExpirationTime: "",
                isUploaded: "FALSE",
                uploadedOn: "",
                commentVersion: "NEW"
            }

            allComment.push(payload)
            this.setState(
                {
                    allComment: allComment,
                    callAddComments: true,
                    comment: ""
                }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
        }

    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    loaderUpload = (status) => {
        this.setState({ loader: status })
    }
    onFileUpload(e, subModule) {
        let files = Object.values(e.target.files)
        let date = new Date()
        let id = e.target.id
        const fd = new FormData();
        let uploadNode = {
            orderCode: this.props.order.orderCode,
            shipmentId: this.props.shipmentId,
            orderNumber: this.props.order.orderNumber,
            module: "SHIPMENT_CONFIRMED",
            subModule: subModule,
        }
        console.log(uploadNode)
        this.setState({
            loader: true
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        for (var i = 0; i < files.length; i++) {
            fd.append("files", files[i]);
        }
        fd.append("uploadNode", JSON.stringify(uploadNode))


        this.loaderUpload("true")
        axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
            .then(res => {
                this.setState({
                    loader: false
                })
                let result = res.data.data.resource
                if (id == "fileUpload") {
                    let QC = this.state.QC
                    for (let i = 0; i < result.length; i++) {
                        let payload = {
                            commentedBy: sessionStorage.getItem("prn"),
                            comments: "",
                            commentDate: "",
                            commentType: "URL",
                            folderDirectory: result[i].folderDirectory,
                            folderName: result[i].folderName,
                            filePath: result[i].filePath,
                            fileName: result[i].fileName,
                            fileURL: result[i].fileURL,
                            urlExpirationTime: result[i].urlExpirationTime,
                            qcVersion: "NEW",
                            uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                            submodule: subModule

                        }
                        QC.push(payload)
                    }
                    this.setState({
                        QC,
                        loader: false
                    })
                } else {
                    let allComment = this.state.allComment

                    for (let i = 0; i < result.length; i++) {
                        let payload = {
                            commentedBy: sessionStorage.getItem("prn"),
                            comments: "",
                            commentDate: "",
                            commentType: "URL",
                            folderDirectory: result[i].folderDirectory,
                            folderName: result[i].folderName,
                            filePath: result[i].filePath,
                            fileName: result[i].fileName,
                            fileURL: result[i].fileURL,
                            urlExpirationTime: result[i].urlExpirationTime,
                            uploadedBy: sessionStorage.getItem('prn'),
                            isUploaded: "TRUE",
                            commentVersion: "NEW",
                            uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                            submodule: subModule
                        }
                        allComment.push(payload)
                    }

                    this.setState({
                        allComment,
                        callAddComments: true,
                        loader: false
                    }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
                }

            }).catch((error) => {
                this.setState({
                    loader: false
                })
            })
    }

    onCloseModal() {
        if (this.state.callAddComments || this.state.qualitySatisfy != null) {
            let date = new Date()
            let commentData = []
            let allComment = this.state.allComment
            allComment.forEach((item) => {
                if (item.commentVersion == "NEW") {
                    let data = {
                        commentedBy: item.commentedBy,
                        comments: item.comments,
                        commentDate: item.commentDate,
                        commentType: item.commentType,
                        folderDirectory: item.folderDirectory,
                        folderName: item.folderName,
                        filePath: item.filePath,
                        fileName: item.fileName,
                        fileURL: item.fileURL,
                        urlExpirationTime: item.urlExpirationTime,
                        isUploaded: item.isUploaded,
                        uploadedOn: item.uploadedOn,
                        subModule: "COMMENTS"
                    }
                    commentData.push(data)
                }
            })
            let payload = {
                poId: this.props.poId,
                shipmentId: this.props.shipmentId,
                module: "SHIPMENT_CONFIRMED",
                isQCDone: this.state.qualitySatisfy != null ? "TRUE" :"FALSE" ,
                QCStatus: this.state.qualitySatisfy,
                isInvoiceUploaded: this.state.isInvoiceUploaded,
                reason: this.state.reason,
                orderCode: this.props.order.orderCode,
                orderNumber: this.props.order.orderNumber,

                SHIPMENT_CONFIRMED: {
                    QC: [],
                    COMMENTS: commentData,
                    INVOICE: []
                }
            }
            this.props.qcAddCommentRequest(payload)
        }

        this.props.handleModal()
    }
handleQuality = (e) => {
    if (e.target.name == "quality") {
        this.setState({ qualitySatisfy: e.target.value })
    } else if (e.target.id == "reason") {
        this.setState({ reason: e.target.value })
    }
}
deleteUploads(data) {

    let QC = this.state.QC.filter((check) => check.fileName != data.fileName)

    let files = []
    let a = {
        fileName: data.fileName,
        isActive: "FALSE"
    }
    files.push(a)
    let payload = {
        poId: this.props.poId,
        shipmentId: this.props.shipmentId,
        module: "SHIPMENT_CONFIRMED",
        subModule: "QC",
        files: files
    }
    this.setState({
        selectedFiles: files
    })
    this.props.deleteUploadsRequest(payload)
}
render() {    
    return (
        <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
            <div className=" display_block">
                <div className="modal-content modalRight createShipmentModal qualityCheckRight text-initial chatModalHeight">
                    <div className="col-md-12 pad-0">
                        <div className="modal_Color selectVendorPopUp displayInline">
                            <div className="modalTop alignMiddle text-left">
                                <div className="col-md-8 pad-0 alignMiddle">
                                    <img src={arrowIcon} onClick={(e) => this.onCloseModal(e)} className="displayPointer m-rgt-15" />
                                    <h2 className="displayInline m-rgt-30">Comments</h2>
                                    {/* <p className="displayInline"><img src={checkIcon} />Confirmed by {sessionStorage.getItem("prn")} for final Shipment</p> */}
                                </div>
                                <div className="col-md-4 pad-0">
                                    <div className="float_Right">
                                        <button type="button" className="discardBtns  m-rgt-10" onClick={(e) => this.onCloseModal(e)}>Close</button>
                                        <button type="button" className="saveBtnBlue" onClick={(e) => this.onCloseModal(e)}>Confirm</button>
                                    </div>
                                </div>
                            </div>
                            {this.props.displayDetails && <div className="modalOpertions">
                                <div className="col-md-12 pad-0">
                                    <div className="col-md-6 pad-0">
                                        <div className="col-md-12 m-top-5">
                                            <div className="col-md-5 pad-0">
                                                <label>Vendor ID</label>
                                            </div>
                                            <div className="col-md-7 pad-0">
                                                <h5 className="displayInline">{this.state.poDetailsShipment.vendorId}</h5>
                                            </div>
                                        </div>
                                        <div className="col-md-12 m-top-5">
                                            <div className="col-md-5 pad-0">
                                                <label>PO Date</label>
                                            </div>
                                            <div className="col-md-7 pad-0">
                                                <h5 className="displayInline">{this.state.poDetailsShipment.poDate.split("T")[0]}</h5>
                                            </div>
                                        </div>
                                        <div className="col-md-12 m-top-5">
                                            <div className="col-md-5 pad-0">
                                                <label>Valid To</label>
                                            </div>
                                            <div className="col-md-7 pad-0">
                                                <h5 className="displayInline">{this.state.poDetailsShipment.validTo.split("T")[0]}</h5>
                                            </div>
                                        </div>
                                        <div className="col-md-12 pad-right-0 m-top-5">
                                            <div className="displayInline pad-0">
                                                <label>Shipment Requested Date</label>
                                            </div>
                                            <div className="displayInline pad-0">
                                                <h5 className="displayInline">{this.state.poDetailsShipment.requestedOn.split("T")[0]}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 pad-0">
                                        <div className="col-md-12 m-top-5 pad-right-0">
                                            <div className="col-md-5 pad-0">
                                                <label>PO Number</label>
                                            </div>
                                            <div className="col-md-7 pad-0">
                                                <h5 className="displayInline">{this.state.poDetailsShipment.poNumber}</h5>
                                            </div>
                                        </div>
                                        <div className="col-md-12 m-top-5 pad-right-0">
                                            <div className="col-md-5 pad-0">
                                                <label>Valid From</label>
                                            </div>
                                            <div className="col-md-7 pad-0">
                                                <h5 className="displayInline">{this.state.poDetailsShipment.validFrom.split("T")[0]}</h5>
                                            </div>
                                        </div>
                                        <div className="col-md-12 m-top-5 pad-right-0">
                                            <div className="col-md-5 pad-0">
                                                <label>Transporter</label>
                                            </div>
                                            <div className="col-md-7 pad-0">
                                                <h5 className="displayInline">{this.state.poDetailsShipment.tranporterName}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>}
                            {this.props.qcUpload ? <div className="col-md-12 qualityCheckVendorSave chatModalMid">
                                <h3>How satisfied were you with the quality of material on the basis of the below parameters? *</h3>
                                <div className="col-md-12 pad-0 radioGeneric m-top-10">
                                    <label className="inputMain">Pass
                                                    <input type="radio" id="pass" name="quality" onChange={this.handleQuality} checked={this.state.qualitySatisfy == "Pass"} value="Pass" />
                                        <span className="spanBefore"></span>
                                    </label>
                                    <label className="inputMain">Fail
                                                    <input type="radio" id="fail" name="quality" onChange={this.handleQuality} checked={this.state.qualitySatisfy == "Fail"} value="Fail" />
                                        <span className="spanBefore"></span>
                                    </label>
                                    {/* <label className="inputMain">Not Required
                                                    <input type="radio" id="notRequire" name="quality" onChange={this.handleQuality} checked={this.state.qualitySatisfy == "Not Required"} value="Not Required" />
                                                    <span className="spanBefore"></span>
                                                </label> */}
                                </div>

                                <div className="uploadInvoiceLeft pad-0 heightAuto">
                                    <h3>Upload QC</h3>
                                    <div className="bottomToolTip displayInline">
                                        <label className="file customInput m-top-5 hoverMe" >
                                            <input type="file" id="fileUpload" multiple="multiple" onChange={(e) => this.onFileUpload(e, "QC")} />
                                            <span >Choose file</span>
                                            <img src={fileIcon} />
                                        </label>
                                        <div className="fileUploads">
                                            {this.state.QC.map((data, key) => <p key={key}>{data.fileName}<span onClick={(e) => this.deleteUploads(data)}>&#10005;</span></p>)}
                                        </div>
                                        <p className="displayMe"> <span></span></p>
                                    </div>
                                    {/* <button type="button" className="uploadBtn">Upload</button> */}
                                </div>
                                <div className="uploadInvoiceRight padRightNone verticalTop heightAuto">
                                    {this.state.invoice.length == 0 ? <h4 className="textWarning" >Invoice Not Available</h4> : <h4>Invoice is Available</h4>}
                                    <div className="posRelative">
                                        {this.state.invoice.length == 0 ? <button type="button" className="downloadQcBtn btnDisabled removeAfter" ><img src={cloudIcon} className="m-rgt-10" />Download Invoice</button>
                                            : <button type="button" className="downloadQcBtn dropdown-toggle removeAfter" data-toggle="dropdown" ><img src={cloudIcon} className="m-rgt-10" />Download Invoice</button>}
                                        <div className="filesDownloadDrop dropdown-menu">
                                            <div className="fileUploads">
                                                {this.state.invoice.map((data, key) => (<p key={key} onClick={() => window.open(data.fileURL)}>{data.fileName}</p>))}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3 className="m-top-5">Reason</h3>
                                <textarea placeholder="Write here..." rows="5" columns="50" id="remark" id="reason" onChange={this.handleQuality} className="textareaGeneric width100 m-top-10" />
                            </div> : null}
                            {/* {this.props.qcUpload ? <div className="col-md-12 pad-0 invoiceMain">
                                    <div className="uploadInvoiceLeft">
                                        <h3>Upload Invoice</h3>
                                        <div className="bottomToolTip displayInline">
                                            <label className="file customInput m-top-5 hoverMe" >
                                                <input type="file" />
                                                <span >Choose file</span>
                                                <img src={fileIcon} />
                                            </label>
                                            <p className="displayMe"> <span></span></p>
                                        </div>
                                        <button type="button" className="uploadBtn">Upload</button>
                                    </div>
                                    <div className="uploadInvoiceRight">
                                        <h4>QC File Available </h4>
                                        <button type="button" className="downloadQcBtn"><img src={cloudIcon} />Download QC file </button>
                                    </div>
                                </div> : null} */}
                            <div className={!this.props.displayDetails ? "col-md-12 pad-0 commentSection" : "col-md-12 pad-0 commentSection height71per"}>
                                <div className="commentHeader">
                                    <h2>Comment Section</h2>
                                    {this.state.current + 1 <= this.state.maxPage ? <button type="button" className="oldCommentBtn" onClick={(e) => this.loadComments(e)}><img src={refreshIcon} />Load old Comments</button> : null}
                                </div>
                                <div className={!this.props.displayDetails ? "comments comment_overflow  " : "comments comment_overflow height71per"} id="scrollBot">
                                    <div className="messages displayInline width100">
                                        {this.state.allComment.length != 0 ? this.state.allComment.map((data, key) => (

                                            data.commentType == "TEXT" ?
                                                <div key={key} className={sessionStorage.getItem("prn") == data.commentedBy ? "messageReceiver displayInline width100" : "messageSender displayInline width100"}>
                                                    <div className="each-comment m-top-15">
                                                        <div className="msgBg">
                                                            <img src={userIcon} />
                                                            {data.commentType == "TEXT" ? <label>{data.comments}</label> : <div><a href={data.fileURL} className="fileNameQc" target="_blank" >{data.fileName}</a></div>}
                                                        </div>
                                                        {data.commentType == "TEXT" ? <span>{data.commentDate}</span> : <span>{data.uploadedOn}</span>}
                                                    </div>

                                                </div>
                                                : <div key={key} className={sessionStorage.getItem("prn") == data.uploadedBy ? "messageReceiver displayInline width100" : "messageSender displayInline width100"}>
                                                    <div className="each-comment m-top-15">
                                                        <div className="msgBg">
                                                            <img src={userIcon} />
                                                            {data.commentType == "TEXT" ? <label>{data.comments}</label> : <div><a href={data.fileURL} className="fileNameQc" target="_blank" >{data.fileName}</a></div>}
                                                        </div>
                                                        {data.commentType == "TEXT" ? <span>{data.commentDate}</span> : <span>{data.uploadedOn}</span>}
                                                    </div>

                                                </div>


                                        )) : null}


                                        {/*<div className="messageReceiver">
                                                <div className="each-comment">
                                                    <div className="msgBg">
                                                        <label>We are proceeding with final
                                                    confirmation</label>
                                                        <img src={userIcon} />
                                                    </div>
                                                    <span>06:30pm 24 Sep 2019</span>
                                                </div>
                                            </div>*/}
                                    </div>
                                </div>
                                <div className="writeComment">
                                    <div className="fileInput">
                                        <input type="text" placeholder="Write your comment..." value={this.state.comment} onKeyDown={(e) => this.addKey(e)} onChange={(e) => this.handleChange(e)} />
                                        <label className="custom-file-Icon">
                                            <input type="file" id="commentUpload" multiple="multiple" onChange={(e) => this.onFileUpload(e, "COMMENTS")} />
                                            <img src={fileIcon} />
                                        </label>
                                        <img src={sendIcon} className="sendIcon" onClick={(e) => this.addComment(this.state.comment)} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {this.state.loader ? <FilterLoader /> : null}
            {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
            {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

        </div>

    )
}
}