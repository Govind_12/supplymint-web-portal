import React from "react";

class FilterShipment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 1,
            no: 1,
            shipmentRequestDate: this.props.shipmentRequestDate,
            poNumber: this.props.poNumber,
            status: this.props.status,
            vendorName: this.props.vendorName,
            shipmentAdviceCode: this.props.shipmentAdviceCode,
            requestedOn: this.props.requestedOn,
            poDate: this.props.poDate,
            validFrom: this.props.validFrom,
            validTo: this.props.validTo,
            transporterName: this.props.transporterName,
            siteDetail: this.props.siteDetail,
            poQty: this.props.poQty,
            dueInDays: this.props.dueInDays,
            count: this.props.filterCount,
            orderCode: this.props.orderCode,
            qcFromDate: this.props.qcFromDate,
            qcToDate: this.props.qcToDate,
            qcDate: this.props.qcDate,
        };
    }
    componentWillMount() {
        this.setState({
            count: this.props.filterCount,
            shipmentRequestDate: this.props.shipmentRequestDate,
            poNumber: this.props.poNumber,
            status: this.props.status,
            vendorName: this.props.vendorName,
            shipmentAdviceCode: this.props.shipmentAdviceCode,
            requestedOn: this.props.requestedOn,
            poDate: this.props.poDate,
            validFrom: this.props.validFrom,
            validTo: this.props.validTo,
            transporterName: this.props.transporterName,
            siteDetail: this.props.siteDetail,
            poQty: this.props.poQty,
            dueInDays: this.props.dueInDays,
            orderCode: this.props.orderCode,
            qcFromDate: this.props.qcFromDate,
            qcToDate: this.props.qcToDate,
            qcDate: this.props.qcDate,
        })
    }
    handleChange(e) {
        if (e.target.id == "shipmentRequestDate") {
            this.setState({
                shipmentRequestDate: e.target.value
            });
        } else if (e.target.id == "poNumber") {
            this.setState({
                poNumber: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        } else if (e.target.id == "vendorName") {
            this.setState({
                vendorName: e.target.value
            })

        } else if (e.target.id == "shipmentAdviceCode") {
            this.setState({
                shipmentAdviceCode: e.target.value
            });
        } else if (e.target.id == "requestedOn") {
            this.setState({
                requestedOn: e.target.value
            });
        } else if (e.target.id == "poDate") {

            this.setState({
                poDate: e.target.value
            });

        } else if (e.target.id == "validFrom") {
            this.setState({
                validFrom: e.target.value
            })

        } else if (e.target.id == "validTo") {
            this.setState({
                validTo: e.target.value
            })

        } else if (e.target.id == "transporterName") {
            this.setState({
                transporterName: e.target.value
            });
        } else if (e.target.id == "siteDetail") {
            this.setState({
                siteDetail: e.target.value
            });
        } else if (e.target.id == "poQty") {
            if (e.target.validity.valid) {
                this.setState({
                    poQty: e.target.value
                });
            }
        } else if (e.target.id == "dueInDays") {
            if (e.target.validity.valid) {
                this.setState({
                    dueInDays: e.target.value
                })
            }
        } else if (e.target.id == "orderCode") {
            this.setState({ orderCode: e.target.value })
        } else if (e.target.id == "qcFromDate") {
            this.setState({ qcFromDate: e.target.value })
        } else if (e.target.id == "qcToDate") {
            this.setState({ qcToDate: e.target.value })
        } else if (e.target.id == "qcDate") {
            this.setState({ qcDate: e.target.value })
        }
    }

    clearFilter(count) {
        this.setState({
            shipmentRequestDate: "",
            poNumber: "",
            status: "",
            vendorName: "",
            shipmentAdviceCode: "",
            requestedOn: "",
            poDate: "",
            validFrom: "",
            validTo: "",
            transporterName: "",
            siteDetail: "",
            poQty: "",
            dueInDays: "",
            count: 0,
            orderCode: "",
            qcFromDate: "",
            qcToDate: ""
        })
        this.props.filterCount != 0 ? this.props.clearFilter() : null
    }

    onSubmit(count) {
        // e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            shipmentRequestDate: this.state.shipmentRequestDate,
            poNumber: this.state.poNumber,
            status: this.state.status,
            vendorName: this.state.vendorName,
            shipmentAdviceCode: this.state.shipmentAdviceCode,
            requestedOn: this.state.requestedOn,
            poDate: this.state.poDate,
            validFrom: this.state.validFrom,
            validTo: this.state.validTo,
            transporterName: this.state.transporterName,
            siteDetail: this.state.siteDetail,
            poQty: this.state.poQty,
            dueInDays: this.state.dueInDays,
            filterCount: count,
            orderCode: this.state.orderCode,
            qcFromDate: this.state.qcFromDate,
            qcToDate: this.state.qcToDate,
            qcDate: this.state.qcDate,
        }
        this.props.getShipmentRequest(data);
        this.props.closeFilter();
        this.props.updateFilter(data)
    }

    render() {
        let count = 0;
        if (this.state.shipmentRequestDate != "") {
            count++;
        }
        if (this.state.poNumber != "") {
            count++;
        }
        if (this.state.vendorName != "") {
            count++

        }
        if (this.state.shipmentAdviceCode != "") {
            count++

        } if (this.state.requestedOn != "") {
            count++

        }
        if (this.state.poDate != "") {
            count++;
        }
        if (this.state.validFrom != "") {
            count++;
        } if (this.state.validTo != "") {
            count++;
        }
        if (this.state.transporterName != "") {
            count++

        }
        if (this.state.poQty != "") {
            count++

        } if (this.state.dueInDays != "") {
            count++
        }
        if (this.state.orderCode != "") {
            count++;
        }
        if (this.state.qcFromDate != "") {
            count++
        }
        if (this.state.qcToDate != "") {
            count++;
        }
        if (this.state.qcDate != "") {
            count++;
        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(count)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="shipmentRequestDate" value={this.state.shipmentRequestDate} placeholder={this.state.shipmentRequestDate != "" ? this.state.shipmentRequestDate : "Shipment Requested Date"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="poNumber" value={this.state.poNumber} placeholder="PO Number" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorName" value={this.state.vendorName} placeholder="Vendor Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="shipmentAdviceCode" value={this.state.shipmentAdviceCode} placeholder="Shipment ASN code" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="requestedOn" value={this.state.requestedOn} placeholder={this.state.requestedOn != "" ? this.state.requestedOn : "Requested On"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="poDate" value={this.state.poDate} placeholder={this.state.poDate != "" ? this.state.poDate : "PO Date"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="validFrom" value={this.state.validFrom} placeholder={this.state.validFrom != "" ? this.state.validFrom : "Valid From"} className="organistionFilterModal" />
                                    </li>
                                </ul></div></div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">
                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="validTo" value={this.state.validTo} placeholder={this.state.validTo != "" ? this.state.validTo : "Valid To"} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="transporterName" value={this.state.transporterName} placeholder="Transporter Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} pattern="[0-9]*" id="poQty" value={this.state.poQty} placeholder="PO Qty" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} pattern="[0-9]*" id="dueInDays" value={this.state.dueInDays} placeholder="Due In Days" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="orderCode" value={this.state.orderCode} placeholder="Order Code" className="organistionFilterModal" />
                                    </li>
                                    {this.props.qcDateFilter == "False" ? <div className="displayInline verticalBot"> <li className="m-lr-5 float_Left">
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="qcFromDate" value={this.state.qcFromDate} placeholder={this.state.qcFromDate != "" ? this.state.qcFromDate : "QC From Date"} className="organistionFilterModal" />
                                    </li>
                                        <li className="m-lr-5 float_Left">
                                            <input type="date" onChange={(e) => this.handleChange(e)} id="qcToDate" value={this.state.qcToDate} placeholder={this.state.qcToDate != "" ? this.state.qcToDate : "QC To Date"} className="organistionFilterModal" />
                                        </li></div>
                                        : <li>
                                            <input type="date" onChange={(e) => this.handleChange(e)} id="qcDate" value={this.state.qcDate} placeholder={this.state.qcDate != "" ? this.state.qcDate : "QC Date"} className="organistionFilterModal" />
                                        </li>
                                    }

                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.state.shipmentRequestDate == "" && this.state.qcFromDate == "" && this.state.qcDate == "" && this.state.qcToDate == "" && this.state.orderCode == "" && this.state.poNumber == "" && this.state.vendorName == "" && this.state.shipmentAdviceCode == "" && this.state.requestedOn == "" && this.state.poDate == "" && this.state.validFrom == "" && this.state.validTo == "" && this.state.transporterName == "" && this.state.poQty == "" && this.state.dueInDays == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button type="button" onClick={(e) => this.clearFilter(0)} className="modal_clear_btn">
                                                CLEAR FILTER
                                         </button>}
                                    </li>
                                    <li>
                                        {this.state.shipmentRequestDate != "" || this.state.qcDate != "" || this.state.qcFromDate != "" || this.state.qcToDate != "" || this.state.orderCode != "" || this.state.poNumber != "" || this.state.vendorName != "" || this.state.shipmentAdviceCode != "" || this.state.requestedOn != "" || this.state.poDate != "" || this.state.validFrom != "" || this.state.validTo != "" || this.state.transporterName != "" || this.state.poQty != "" || this.state.dueInDays != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default FilterShipment;
