import React, { Component } from 'react'
import exclaimIcon from '../../../../assets/exclain.svg';
import moment from 'moment';
import { handleChange } from '../../../../helper/index'
import Pagination from '../../../pagination';
import searchIcon from '../../../../assets/close-recently.svg';
import QcDateConfirmModal from '../../modals/qcDateConfirmModal';
import fileIcon from '../../../../assets/fileIcon.svg';
import QualityCheckModalEnterprise from './qualityCheckModalEnterprise';
import Close from '../../../../assets/close-red.svg';
import axios, { post } from "axios";
import { CONFIG } from "../../../../config/index";
import ToastLoader from '../../../loaders/toastLoader';
import cloudIcon from '../../../../assets/iconmonstr-note.svg';
import Eye from '../../../../assets/eye-icon.svg';
import FilterLoader from '../../../loaders/filterLoader';
import RequestError from  '../../../loaders/requestError'; 
import CreateAsnUpload from '../../vendor/orders/createAsnUpload';

export default class ConfirmShipmentModalEnterprise extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rangeVal: 1,
            sliderDisable: false,
            valueDate: "",
            poDetailsShipment: [],
            qcModalConfirm: false,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            search: "",
            type: 1,
            qualityModal: false,
            qcUpload: true,
            isQCDone: "",
            isInvoiceUploaded: "",
            toastLoader: false,
            toastMsg: "",

            allQC: "",
            invoice: [],
            QC: [],
            multipleQc: [...this.props.checkedShipConfirmed],
            showDrop: false,
            prevId: "",
            showInvoice: false,
            prevInvoiceID: "",
            fileNames: [],

            noOfReInspection: 0,
            inspectionBufferDays: 0,
            errorMessage: "",
            errorCode: "",
            code: "",
            alert: false,

            uploadFileType:"",
            ppsUploadModal: false,
            currentAsnId: 0,
            allowNoOfReInspection: false,
            allowInspBufferDays: false,
        }
    }
    componentDidMount() {
        var data = this.state.multipleQc
        data.map((data) => (data.reQcDate = "", data.QCStatus = "", data.select = "", data.selectErr = "", data.reQcDateErr = "" , data.qcArray = []))
        this.setState({ multipleQc: data })
        this.props.getStatusButtonActiveRequest();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getAllComment.isSuccess) {
            if (nextProps.shipment.getAllComment.data.resource != null) {
                this.setState({
                    QC: nextProps.shipment.getAllComment.data.resource.QC,
                    allQC: nextProps.shipment.getAllComment.data.resource,
                    invoice: nextProps.shipment.getAllComment.data.resource.INVOICE
                })
            }
        }
        if (nextProps.shipment.deleteUploads.isSuccess) {
            var multipleQc = [...this.state.multipleQc]
            multipleQc.map((data) => {
                if (data.id == this.state.deletedId) {
                    data.isQCUploaded = nextProps.shipment.deleteUploads.data.isQCAvailable
                    if (nextProps.shipment.deleteUploads.data.isQCAvailable == "FALSE") {
                        this.setState({ showDrop: false })
                    }
                }
            })
            this.setState({ multipleQc })
        }
        if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) {
            this.setState({
                noOfReInspection: nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.reqcCount) : 0,
                inspectionBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays != undefined && nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays != null ? Number(nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferdays) : 0,
                allowNoOfReInspection: nextProps.logistic.getButtonActiveConfig.data.resource.reqcRequired === 'TRUE' ? true : false,
                allowInspBufferDays: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionBufferDaysRequired === 'TRUE' ? true : false  
            })
            this.props.getStatusButtonActiveClear()
            this.setState({ loader: false})
        }
        else if (nextProps.logistic.getButtonActiveConfig.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getButtonActiveConfig.message.error == undefined ? undefined : nextProps.logistic.getButtonActiveConfig.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.logistic.getButtonActiveConfig.message.status,
                errorCode: nextProps.logistic.getButtonActiveConfig.message.error == undefined ? undefined : nextProps.logistic.getButtonActiveConfig.message.error.errorCode
            })
            this.props.getStatusButtonActiveClear()
        }
    }
    viewAllAttachment = (data) => {
        if (!this.state.showDrop || this.state.prevId !== data.id) {
            let payload = {
                // shipmentId: data.id,
                // module: "SHIPMENT",
                // no: 1,
                // orderCode: data.orderCode,
                // orderNumber: data.orderNumber,
                // isAllAttachment: "TRUE",
                // subModule: "QC"

                module: "SHIPMENT",
                pageNo: 1,
                subModule: "QC",
                isAllAttachment: "TRUE",
                type: 1,
                search: "",
                commentId: data.id,
                commentCode: data.shipmentAdviceCode
            }
            this.props.getAllCommentRequest(payload)
            this.setState({ showDrop: true, showInvoice: false, prevId: data.id, expandedID: data.id, fileNames: [] })
        } else {
            this.setState({ showDrop: false })
        }
    }

    onFileUpload(e, subModule, data) {
        // document.getElementById('fileName').innerHTML = Object.values(e.target.files)[0].name
        let files = Object.values(e.target.files)
        let multipleQc = [...this.state.multipleQc]
        let date = new Date()
        let id = e.target.id
        const fd = new FormData();
        var fileNames = files.map((item, index) => Object.values(e.target.files)[index].name)
        let uploadNode = {
            orderCode: data.orderCode,
            shipmentId: data.id,
            orderNumber: data.orderNumber,
            module: "SHIPMENT",
            subModule: subModule,
            orderId: data.orderId,
            documentNumber: data.documentNumber,
            commentId: data.id,
            commentCode: data.shipmentAdviceCode,
            vendorCode: data.vendorCode,
        }
        this.setState({
            loader: true,
            fileNames,
            expandedID: data.id
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        for (var i = 0; i < files.length; i++) {
            fd.append("files", files[i]);
        }
        fd.append("uploadNode", JSON.stringify(uploadNode))
        // this.loaderUpload("true")
        axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
            .then(res => {
                this.setState({
                    loader: false
                })
                let result = res.data.data.resource
                if (res.data.status == "2000") {
                    multipleQc.map((hdata) => {
                        if (hdata.id == data.id) {
                            hdata.isQCUploaded = res.data.data.resource[0].isQCAvailable
                        }
                    })
                }
                if (id == "fileUpload") {
                    let QC = this.state.QC
                    for (let i = 0; i < result.length; i++) {
                        let payload = {
                            commentedBy: sessionStorage.getItem("prn"),
                            comments: "",
                            commentDate: "",
                            commentType: "URL",
                            folderDirectory: result[i].folderDirectory,
                            folderName: result[i].folderName,
                            filePath: result[i].filePath,
                            fileName: result[i].fileName,
                            fileURL: result[i].fileURL,
                            urlExpirationTime: result[i].urlExpirationTime,
                            qcVersion: "NEW",
                            uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                            submodule: subModule
                        }
                        QC.push(payload)
                    }
                    this.setState({
                        QC,
                        loader: false
                    })
                } else {
                    let allComment = this.state.allComment

                    for (let i = 0; i < result.length; i++) {
                        let payload = {
                            commentedBy: sessionStorage.getItem("prn"),
                            comments: "",
                            commentDate: "",
                            commentType: "URL",
                            folderDirectory: result[i].folderDirectory,
                            folderName: result[i].folderName,
                            filePath: result[i].filePath,
                            fileName: result[i].fileName,
                            fileURL: result[i].fileURL,
                            urlExpirationTime: result[i].urlExpirationTime,
                            uploadedBy: sessionStorage.getItem('prn'),
                            isUploaded: "TRUE",
                            commentVersion: "NEW",
                            uploadedOn: moment(date).format("DD MMM YYYY HH:MM"),
                            submodule: subModule
                        }
                        allComment.push(payload)
                    }
                    this.setState({
                        allComment,
                        multipleQc,
                        callAddComments: true,
                        loader: false
                    }, () => { document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight })
                }
            }).catch((error) => {
                this.setState({
                    loader: false
                })
                this.props.openToastError()
            })
        e.target.value = ''
    }
    deleteUploads(data, multipleAsn) {
        let QC = this.state.QC.filter((check) => check.fileURL != data.fileURL)
        let files = []
        let a = {
            fileName: data.fileName,
            isActive: "FALSE"
        }
        files.push(a)
        let payload = {
            poId: this.props.poId,
            shipmentId: this.state.expandedID,
            module: "SHIPMENT",
            subModule: "QC",
            isQCAvailable: multipleAsn.isQCUploaded,
            isInvoiceAvailable: multipleAsn.isInvoiceUploaded,
            isBarcodeAvailable: multipleAsn.isBarcodeUploaded,
            files: files,
            commentId: multipleAsn.id
        }
        this.setState({
            selectedFiles: files,
            QC,
            deletedId: multipleAsn.id
        })
        this.props.deleteUploadsRequest(payload)
    }
    onDelete = (e, id) => {
        let multipleQc = [...this.state.multipleQc]
        if (multipleQc.length > 1) {
            multipleQc = multipleQc.filter((data) => data.id != id)
        }
        this.setState({ multipleQc })
    }
    handleFields = (e, id, type) => {
        let multipleQc = [...this.state.multipleQc]
        if (type == "remarks") {
            multipleQc.map((data) => (data.id == id && (data.remarks = e.target.value)))
            this.setState({ multipleQc })
        } else if (type == "select") {
            multipleQc.map((data) => { if(data.id === id && e.target.value === "reQC" && (this.state.allowNoOfReInspection && data.reqcCount === this.state.noOfReInspection)){
                                            this.setState({
                                                toastMsg: "You have exceeded your Re-QC limit.",
                                                toastLoader: true,
                                                loader: false
                                            })
                                            setTimeout(() => {
                                                this.setState({
                                                    toastLoader: false
                                                })
                                            }, 5000)
                                            data.QCStatus = "";
                                            document.getElementById("status-selection"+data.id).options[0].selected = true;
                                    }else{
                                        data.id == id && (data.QCStatus = e.target.value)
                                    }
                                })
            this.setState({ multipleQc }, () => (this.select(id)))
        } else if (type == "reQcDate") {
            multipleQc.map((data) => (data.id == id && (data.reQcDate = e.target.value)))
            this.setState({ multipleQc }, () => (this.dateErr(id)))
        }
    }
    select = (id) => {
        let multipleQc = [...this.state.multipleQc]
        if (id != undefined) {
            multipleQc.map((data) => id == data.id && data.QCStatus == "" ? (data.selectErr = true) : (data.selectErr = false))
        } else {
            multipleQc.map((data) => data.QCStatus == "" ? (data.selectErr = true) : (data.selectErr = false))
        }
        this.setState({ multipleQc })
    }
    dateErr = () => {
        let multipleQc = [...this.state.multipleQc]
        multipleQc.map((data) => data.QCStatus == "reQC" && data.reQcDate == "" ? (data.reQcDateErr = true) : (data.reQcDateErr = false))
        this.setState({ multipleQc })
    }
    onConfirm = () => {
        this.select()
        this.dateErr()
        let flag = false
        setTimeout(() => {
            let multipleQc = this.state.multipleQc
            multipleQc.forEach((data) => {
                if (data.selectErr == true || data.reQcDateErr == true) {
                    flag = true
                    return;
                }
            })
            if (!flag) {
                let push = []
                multipleQc.map((hdata) => {
                    let payload = {
                        "shipmentId": hdata.id,
                        "module": "SHIPMENT",
                        "isQCDone": hdata.QCStatus == "Pass" ? "TRUE" : "FALSE",
                        "QCStatus": hdata.QCStatus,
                        "isInvoiceUploaded": "",
                        "reason": hdata.remarks,
                        "orderCode": hdata.orderCode,
                        "orderNumber": hdata.orderNumber,
                        "reqcDate": hdata.reQcDate !== "" ? moment(hdata.reQcDate).format("YYYY-MM-DD") + "T00:00+05:30" : "",
                        "shipmentAdviceCode": hdata.shipmentAdviceCode,
                        "orderId": hdata.orderId,
                        "documentNumber": hdata.documentNumber,
                        "qcSelectionDate": hdata.qcSelectionDate !== null ? moment(hdata.qcSelectionDate).format("YYYY-MM-DD") + "T00:00+05:30" : "",
                        "shipmentStatus": hdata.shipmentStatus,
                        "SHIPMENT": {
                            "QC": [
                            ],
                            "COMMENTS": [
                            ],
                            "INVOICE": [
                            ]
                        }
                    }
                    push.push(payload)
                })
                let finalPayLoad = {
                    qcList: push
                }
                // this.props.addMultipleCommentQcRequest(finalPayLoad)
                 //Calling File upload after submission::
                //  this.props.onFileUploadProps(multipleQc)
            }
        }, 10)
    }
    dowloadInvoice(data) {
        if (!this.state.showInvoice || this.state.prevInvoiceID !== data.id) {
            let payload = {
                // shipmentId: data.id,
                // module: "SHIPMENT",
                // no: 1,
                // orderCode: data.orderCode,
                // orderNumber: data.orderNumber,
                // isAllAttachment: "TRUE",
                // subModule: "INVOICE"

                module: "SHIPMENT",
                pageNo: 1,
                subModule: "INVOICE",
                isAllAttachment: "TRUE",
                type: 1,
                search: "",
                commentId: data.id,
                commentCode: data.shipmentAdviceCode
            }
            this.props.getAllCommentRequest(payload)
            this.setState({ showDrop: false, showInvoice: true, prevInvoiceID: data.id, expandedID: data.id, fileNames: [] })
        } else {
            this.setState({ showInvoice: false })
        }
    }

    //---------------------------------------- New File Upload ---------------------------// 
    openPpsUploadModal =(e, data, fileType)=> {
        e.preventDefault();
        this.setState({
            uploadFileType: fileType,
            ppsUploadModal: !this.state.ppsUploadModal,
            currentAsnId: data.id,
        },()=> document.addEventListener('click', this.clickClosePpsModal));
    }

    onFileUploadSubmit = (fileList, moduleName) =>{
        if(moduleName == "QC"){
            let multipleQc = this.state.multipleQc
            multipleQc.map( data => {
                if(data.id == this.state.currentAsnId){
                    data.qcArray = fileList
                    data.fileType = "QC"
                }  
            })
            this.setState({
                multipleQc,
                ppsUploadModal: false,
            },()=> document.removeEventListener('click', this.clickClosePpsModal))
        }
    }

    CancelPpsUploadModal = (e) => {
        this.setState({
            ppsUploadModal: false,
        },()=> document.removeEventListener('click', this.clickClosePpsModal))
    }

    clickClosePpsModal =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop")){
            this.setState({ ppsUploadModal: false})
        }
    }

    render() {

        return (
            <div>
                <div className="modal" id="pocolorModel">
                    <div className="backdrop modal-backdrop-new"></div>
                    <div className="display_block">
                        <div className="modal-content modalShow chatModalHeight shipment-aprv-asn-mdl">
                            <div className="shipment-approvedasn-modal-head">
                                <div className="samh-left">
                                    <h3>Complete Insp</h3>
                                </div>
                                <div className="samh-right">
                                    <button type="button" onClick={() => this.props.CancelShipment()}>Cancel</button>
                                    <button type="button" className="confirmBtnBlue" onClick={this.onConfirm}>Confirm</button>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="shipment-approvedasn-modal-body">
                                    <div className="responsive-table">
                                        <table className="table modal-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">PO NUMBER</label>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">ASN No.</label>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">Insp Status</label>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">Upload Insp </label>
                                                        </div>
                                                    </th>
                                                    {/* <th>
                                                        <div className="samb-asnno">
                                                            <label className="samb-bold">Download Invoice</label>
                                                        </div>
                                                    </th> */}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.multipleQc.map((data, key) => (
                                                    <tr key={key}>
                                                        <td><label className="displayBlock">{data.orderNumber}</label></td>
                                                        <td><label className="displayBlock">{data.shipmentAdviceCode}</label></td>
                                                        <td>
                                                            <div className="samb-asnno">
                                                                <div className="samb-dropdown">
                                                                    <select id={"status-selection"+data.id} value={data.QCStatus} onChange={(e) => this.handleFields(e, data.id, "select")}>
                                                                        <option value="">Select</option>
                                                                        <option value="Pass">Pass</option>
                                                                        <option value="Fail">Fail</option>
                                                                        <option value="reQC">Re Insp</option>
                                                                    </select>
                                                                    {data.selectErr && <span className="error">Please Select</span>}
                                                                    <div className="posRelative displayInline">
                                                                        {data.QCStatus == "Fail" && <input type="text" value={data.remarks} onChange={(e) => this.handleFields(e, data.id, "remarks")} placeholder="Remark" />}
                                                                        {data.QCStatus == "Pass" && <input type="text" value={data.remarks} onChange={(e) => this.handleFields(e, data.id, "remarks")} placeholder="Remark" />}
                                                                        {data.QCStatus == "reQC" && <input type="date" min={data.qcFromDateSelection} max={this.state.allowInspBufferDays ?  moment(data.qcToDateSelection).add(this.state.inspectionBufferDays,'days').format("YYYY-MM-DD") : moment(data.qcToDateSelection).format("YYYY-MM-DD")} value={data.reQcDate} placeholder={data.reQcDate == "" ? "Select Date" : data.reQcDate} onChange={(e) => this.handleFields(e, data.id, "reQcDate")} />}
                                                                        {data.reQcDateErr && data.QCStatus == "reQC" && <span className="error">Please Select Re Insp Date</span>}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="caffc-fields">
                                                                <React.Fragment>
                                                                    <div className="caffb-upload-file">
                                                                        <label onClick={(e) => this.openPpsUploadModal(e, data, "QC")}>
                                                                            {data.qcArray !== undefined && (data.qcArray.length == 0 ? <span>Upload File</span>:<span className="caffbuf-name">+{data.qcArray.length} Files</span>)}
                                                                            <img src={require('../../../../assets/attach.svg')} />
                                                                        </label>
                                                                        <span id="ppsfileLabel"></span>
                                                                    </div>
                                                                </React.Fragment>
                                                                {this.state.ppsUploadModal && this.state.multipleQc.map( item => item.id == this.state.currentAsnId && <CreateAsnUpload CancelPpsUploadModal={this.CancelPpsUploadModal} onFileUploadSubmit={this.onFileUploadSubmit} newQcArray ={item.qcArray} {...this.state} {...this.props}/>)}
                                                            </div>
                                                            {/* <div className="samb-asnno">
                                                                <div className="uploadInvoiceLeft pad-0 heightAuto">
                                                                    <div className="bottomToolTip displayInline">
                                                                        <label className="file width115px customInput m-top-5 hoverMe" >
                                                                            <input type="file" id="fileUpload" onChange={(e) => this.onFileUpload(e, "QC", data)} multiple="multiple" />
                                                                            <span >Choose file</span>
                                                                            <img src={fileIcon} />
                                                                        </label>
                                                                        <div className="fileName" id="fileName"></div>
                                                                        <div className="fileUploads">
                                                                            {this.state.fileNames.length != 0 && data.id == this.state.expandedID && <label>{this.state.fileNames.length}-Files Uploaded</label>}
                                                                        </div>
                                                                        <p className="displayMe"> <span></span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="uploadInvoiceRight">
                                                                {data.isQCUploaded == "TRUE" ? <button type="button" className="removeAfter eye-icon-new" onClick={(e) => { this.viewAllAttachment(data) }}><span className="tooltip-shipment-asn">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                        <g id="prefix__eye" transform="translate(-4.736 -19.47)">
                                                                            <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                            <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#000" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                        </g>
                                                                    </svg>
                                                                    <span className="tooltip-content-shipment">View Attachment</span></span></button>
                                                                    : <button type="button" className="removeAfter eye-icon-new pointerNone" ><span className="tooltip-shipment-asn">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.317" height="8.308" viewBox="0 0 12.317 8.308">
                                                                            <g id="prefix__eye" fill="#ccd8ed" transform="translate(-4.736 -19.47)">
                                                                                <path id="prefix__Path_416" d="M16.674 22.628c-1.057-1.181-3.211-3.158-5.779-3.158s-4.723 1.977-5.779 3.158a1.5 1.5 0 000 1.993c1.057 1.18 3.211 3.157 5.779 3.157s4.722-1.977 5.779-3.158a1.5 1.5 0 000-1.992zm-.816 1.267C13.6 26.418 11.636 26.69 10.9 26.69s-2.712-.272-4.973-2.79a.408.408 0 010-.544c2.26-2.521 4.226-2.793 4.968-2.793s2.708.272 4.968 2.795a.408.408 0 010 .543z" className="prefix__cls-1" data-name="Path 416" />
                                                                                <path id="prefix__Path_417" d="M31.754 28.24a2.962 2.962 0 102.874 2.96 2.921 2.921 0 00-2.874-2.96zm0 4.835a1.875 1.875 0 111.785-1.875 1.832 1.832 0 01-1.785 1.874z" className="prefix__cls-1" fill="#ccd8eds" data-name="Path 417" transform="translate(-20.858 -7.577)" />
                                                                            </g>
                                                                        </svg>
                                                                        <span className="tooltip-content-shipment">View Attachment</span></span></button>}
                                                                {this.state.showDrop && data.id == this.state.expandedID && <div className="filesDownloadDrop ">
                                                                    <div className="fileUploads">
                                                                        {this.state.QC.length != 0 ? this.state.QC.map((hdata, key) => <div className="asn-uploaded-file"><div className="asn-up-file-name"><p key={key} onClick={() => window.open(hdata.fileURL)}>{hdata.fileName}</p></div><span onClick={(e) => this.deleteUploads(hdata, data)}>&#10005;</span></div>)
                                                                            : <label>No Data Found</label>}
                                                                    </div>
                                                                </div>}
                                                            </div> */}
                                                        </td>
                                                        <td>
                                                            <div className="close-flex">
                                                                <span className="close-btn" onClick={(e) => this.onDelete(e, data.id)}><img className="displayPointer" src={Close}></img></span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {/* {this.state.qualityModal ? <QualityCheckModalEnterprise displayDetails={false} qcUpload={this.state.qcUpload} {...this.state} {...this.props} poId={this.props.poId} handleModal={(e) => this.handleModal(e)} shipmentId={this.props.shipmentId} poDetailsShipment={this.state.poDetailsShipment} /> : null} */}
                {this.state.qcModalConfirm ? <QcDateConfirmModal {...this.state} {...this.props} closeQc={(e) => this.closeQc(e)} CancelShipment={this.props.CancelShipment} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div >
        )
    }
}
