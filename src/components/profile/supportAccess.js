import React from 'react';

class SupportAccess extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentPassword: "",
            currentPassworderr: false,
            newPassword: "",
            newPassworderr: false,
            reenterPassword: "",
            reenterPassworderr: false,
            btnDisable: true,
            type: 'password',
            type1: 'password',
            type2: 'password',
        };
        this.showHideCurrentPassword = this.showHideCurrentPassword.bind(this);
        this.showHideNewPassword = this.showHideNewPassword.bind(this);
        this.showHideReNewPassword = this.showHideReNewPassword.bind(this);
    }
    
    showHideCurrentPassword(e){
        this.setState({
          type: this.state.type === 'password' ? 'text' : 'password'
        })
    }
    showHideNewPassword(e){
        this.setState({
            type1: this.state.type1 === 'password' ? 'text' : 'password'
        })
    }
    showHideReNewPassword(e){
        this.setState({
            type2: this.state.type2 === 'password' ? 'text' : 'password'
        })
    }


    currentPassword =()=> {
        if (this.state.currentPassword == "" || !this.state.currentPassword.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/)) {
            this.setState({
                currentPassworderr: true
            });
        } else {
            this.setState({

                currentPassworderr: false
            });
        }
    }

    newPassword =()=> {
        if (this.state.newPassword == "" || !this.state.newPassword.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/)) {
            this.setState({
                newPassworderr: true
            });
        } else {
            this.setState({
                newPassworderr: false
            });
        }
    }

    reenterPassword =()=> {
        if (this.state.reenterPassword == "" || !this.state.reenterPassword.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/) || (document.getElementById("newPassword").value != document.getElementById("reenterPassword").value)) {
            this.setState({
                reenterPassworderr: true
            });
        } else {
            this.setState({
                reenterPassworderr: false
            });
        }
    }
    handleChange =(e)=> {
        this.setState({
            btnDisable: false
        })
        if (e.target.id == "currentPassword") {
            this.setState(
                {
                    currentPassword: e.target.value
                },
                () => {
                    this.currentPassword();
                }
            );
        }
        else if (e.target.id == "newPassword") {
            this.setState(
                {
                    newPassword: e.target.value
                },
                () => {
                    this.newPassword();
                }
            );
        }
        else if (e.target.id == "reenterPassword") {
            this.setState(
                {
                    reenterPassword: e.target.value
                },
                () => {
                    this.reenterPassword();
                }
            );

        }
    }

    contactSubmit =(e)=> {
        e.preventDefault();
        this.currentPassword();
        // this.newPassword();
        // this.reenterPassword();
        setTimeout(()=> {
            const { currentPassworderr, currentPassword } = this.state;
            if (!currentPassworderr) {
                this.props.createSupportAccessRequest(currentPassword);
                this.onClear();
            }
        }, 100)
    }


    onClear =()=> {
        // e.preventDefault();
        this.setState({
            currentPassword: "",

            newPassword: "",
            reenterPassword: ""

        });
    }

    render() {console.log(this.props.createSupportAccess.is_success);
        const { currentPassword, currentPassworderr
            , newPassword, newPassworderr, reenterPassword, reenterPassworderr } = this.state;
            
        return (
            <div className="user-info-tab-details">{
                this.props.supportAccessDetails.is_user == 1 || this.props.createSupportAccess.is_success == 1 ?
                <form className="organizationForm">
                    <div className="uitd-body">
                        {/* <div className="uitdb-row">
                            <div className="uitdbr-inner">
                                <label>Already have access</label>
                            </div>
                        </div> */}
                        <div className="uitdb-support-access">
                            <div className="uitdbsa-left">
                                <img src={require('../../assets/fingerprint.svg')} />
                                <span className="uitdbsa-lock">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18.773" height="18.773" viewBox="0 0 18.773 18.773">
                                        <defs>
                                            <linearGradient id="ouz6btuk3a" x1="1" x2=".184" y2="1" gradientUnits="objectBoundingBox">
                                                <stop offset="0" stopColor="#7a65ef"/>
                                                <stop offset="1" stopColor="#23e3eb"/>
                                            </linearGradient>
                                        </defs>
                                        <g>
                                            <circle cx="8.66" cy="8.66" r="8.66" fill="url(#ouz6btuk3a)" transform="translate(.727 .727)"/>
                                            <path fill="none" d="M9.387 18.773a9.387 9.387 0 1 1 9.387-9.387 9.4 9.4 0 0 1-9.387 9.387zm0-17.319a7.933 7.933 0 1 0 7.933 7.933 7.941 7.941 0 0 0-7.933-7.933z"/>
                                        </g>
                                        <path fill="#fff" d="M134.786 167.333a.579.579 0 0 1-.413-.173l-2.139-2.174a.58.58 0 1 1 .827-.813l1.709 1.737 3.536-3.895a.58.58 0 1 1 .858.78l-3.948 4.349a.58.58 0 0 1-.417.19z" transform="translate(-126.474 -155.242)"/>
                                    </svg>
                                </span>
                            </div>
                            <div className="uitdbsa-right">
                                <h3>You already have an access to help & support portal</h3>
                                <p>Once enabled you can access help and support request portal</p>
                            </div>
                        </div>
                        <div className="uitdbsa-bottom m-top-30">
                            <h3>You can use your Supplymint's Credentials to login into Support Portal</h3>
                            <div className="uitdbsab-inner m-top-20">
                                <p>Login Supplymint Portal with valid credentials</p>
                                <div className="uitdbsabi-row">
                                    <p>Please click on below link to access support portal</p>
                                    <a href="http://support.supplymint.com" target="_blank">http://support.supplymint.com</a>
                                </div>
                                <div className="uitdbsabi-row">
                                    <p>If you have any concerns regarding the application, you can Submit the ticket on the below link</p>
                                    <a href="http://support.supplymint.com" target="_blank">Submit a ticket</a>
                                </div>
                                <div className="uitdbsabi-row">
                                    <span>You can also write us at <span className="bold">support@supplymint.com</span> to raise your concern.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form> :
                <form name="changePasswordForm" className="organizationForm" onSubmit={(e) => this.contactSubmit((e))} autoComplete="off">
                    <div className="uitd-body">
                        <div className="uitdb-support-access">
                            <div className="uitdbsa-left">
                                <img src={require('../../assets/fingerprint.svg')} />
                                <span className="uitdbsa-lock">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.828" height="16.837" viewBox="0 0 12.828 16.837">
                                        <defs>
                                            <linearGradient id="xnlcja5dra" x1=".148" x2=".766" y1=".196" y2="1" gradientUnits="objectBoundingBox">
                                                <stop offset="0" stopColor="#7a65ef"/>
                                                <stop offset="1" stopColor="#00ced6"/>
                                            </linearGradient>
                                        </defs>
                                        <g>
                                            <g>
                                                <path fill="url(#xnlcja5dra)" d="M74.975 5.612h-.8v-1.6a4.009 4.009 0 1 0-8.018 0v1.6h-.8a1.608 1.608 0 0 0-1.6 1.6v8.018a1.608 1.608 0 0 0 1.6 1.6h9.621a1.608 1.608 0 0 0 1.6-1.6V7.216a1.608 1.608 0 0 0-1.603-1.604zm-4.811 7.216a1.6 1.6 0 1 1 1.6-1.6 1.608 1.608 0 0 1-1.6 1.6zm2.486-7.216h-4.971v-1.6a2.485 2.485 0 1 1 4.971 0z" transform="translate(-63.75) translate(63.75) translate(-63.75)"/>
                                            </g>
                                        </g>
                                    </svg>
                                </span>
                            </div>
                            <div className="uitdbsa-right">
                                <h3>Please enter valid password to enable support access.</h3>
                                <p>Once enabled you can access help and support portal anytime from the supplymint portal.</p>
                            </div>
                        </div>
                        <div className="uitdb-row m-top-30">
                            <div className="uitdbr-inner">
                                <label>Current Password</label>
                                <input type={this.state.type} autoComplete="off" className={currentPassworderr ? "errorBorder ":"onFocus"} name="currentPassword" value={currentPassword} id="currentPassword" onChange={e => this.handleChange(e)} />
                                <span className="password__show" onClick={this.showHideCurrentPassword}>{this.state.type === 'password' ? <img src={require('../../assets/eye-open.svg')} /> : <img src={require('../../assets/hide.svg')} />}</span>
                                {/* {currentPassworderr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                {currentPassworderr ? (
                                    <span className="error">
                                        Enter Current Password
                                </span>
                                ) : null}
                            </div>
                        </div>
                        <div className="uitdb-row">
                            {this.state.btnDisable ? <button type="button" className="gen-save btnDisabled">Enable Support Portal</button>:<button type="submit" className="gen-save">Enable Support Portal</button>}
                            <button type="reset" className="gen-clear" onClick={() => this.onClear()}>Cancel</button>
                        </div>
                    </div>
                </form>
            }</div>
        )
    }
}

export default SupportAccess;