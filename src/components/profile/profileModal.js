import React from 'react';
// import errorIcon from "../../assets/error_icon.svg";
// import profileImg from "../../assets/profile.png";
import uploadFailed from "../../assets/alert-2.svg";
import greenCheck from "../../assets/greencheck.png";
import fileUploadIcon from "../../assets/fileiconwhite.svg";
import axios ,{post} from "axios";
import {CONFIG} from "../../config/index";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import ToastLoader from "../loaders/toastLoader";


class ProfileModal extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        fileData:"",
        fname:"",
        image:"",
        imageSize:"",
        success:false,
        alert:false,
        loader:false,
        successMessage:"",
        toastMsg:"",
        toastLoader:false,
        btndisable:false,
        imagePath:"",
        errorMessage:""
        
        
        }}
        onRequest(e) {
            e.preventDefault();
            this.setState({
              success: false
            });
          }

          



 readURL(e) {
        let zero = e.target.files[0].name
        let filesZero = e.target.files[0]

        const t= this;

            if (e.target.files && e.target.files[0]) {
                if(e.target.files[0].size<5242880 ){
         
                    if( e.target.files[0].type=="image/jpeg" || e.target.files[0].type=="image/png" || e.target.files[0].type=="image/svg+xml" ){
              var reader = new FileReader();
          
              reader.onload = function(e) {
                t.setState({
                    fileData: e.target.result,
                    fileName:zero,
                    files:e.target.files,
                    filesZero:filesZero,
                    btndisable:false,
                    image:"",
                    imageSize:""
                
                })
              }
          
              reader.readAsDataURL(e.target.files[0]);
            
              document.getElementById("file").value=""

            }else{
                this.setState({
                    image:true,
                    fileData:"",
                    imageSize:false,
                    btndisable:true
                })
              
            }
          }else{
            this.setState({
                imageSize:true,
                fileData:"",
                image:false,
                btndisable:true

            })
           
          }
        }}
          fileSelectedHandler(e){
            sessionStorage.removeItem("profile");
      
         
       if(this.state.fileData!=""){
        this.setState({
            loader:true
        })
           const reader = new FileReader();
       
           reader.readAsDataURL(this.state.filesZero);
           reader.onload=(e)=>{
          
               var data={
                   file: this.state.fileData,
                   userName:sessionStorage.getItem("userName"),
                   fileName:this.state.fileName
               }
      
   
               var headers={
                   'X-Auth-Token': sessionStorage.getItem('token'),
                   'Content-Type': 'application/json',
                 }
          return post(`${CONFIG.BASE_URL}/admin/user/update/profile/image`,data,{headers: headers})
          .then(res =>{
      

            const t = this
      setTimeout(function(){
       t.setState({
           imageRemove:"",
           imagePath:res.data.data!=null?res.data.data.resource.path:"",
           loader:false,
           success:true,
           errorMessage: res.data.error!=null?res.data.error.errorMessage:"" ,
           successMessage: res.data.data!=null?res.data.data.message:"",
           
       })
      
     
      
      },10)
   
      sessionStorage.setItem("profile",res.data.data.resource.path)
    //   this.props.getUserNameRequest(sessionStorage.getItem('userName'));
    //   window.location.href = "#/profile";
     
      this.props.history.push("/home");
   
         
       }).catch(() => {
         
               this.setState({
                   loader:false,
                //    alert:true,
                //    errorMessage: nextProps.purchaseIndent.brand.message.error.errorMessage,
                //    errorCode: nextProps.purchaseIndent.brand.message.error == undefined ? undefined : nextProps.purchaseIndent.brand.message.error.errorCode,
                //    code: nextProps.purchaseIndent.brand.message.status,
              
                //    success: false,
                // errorMessage: res.data.error.errorMessage  ,
                  
               })
                   
               })
           }}else{

            this.setState({
                toastMsg:"Select Image ",
                toastLoader: true
            })
            const t=this
            setTimeout(function(){
             t.setState({
                 toastLoader:false
             })
            },1000)

           }
           
       }


    render(){
      

        return(
            <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
     
            <div className=" display_block">
            <div className="modal-content vendorEditModalContent modalShow adHocModal profileModal">
                
            
                <div className="col-md-12 col-sm-12 pad-0">
                    <div className=" mainAdHoc"> 
                        
                            <label className="contribution_mart">Profile Update</label>
                           <div className="uploadPic m-top-20">
                                <div className="chooseFileProfile uploadFile m-top-20">
                                 
                                        <label className="custom-file-upload-budget">
                                            <input type="file" autoComplete="off" id="file"  onChange={(e)=>this.readURL(e)}/>
                                            Choose File<img src={fileUploadIcon} />
                                        </label>
                                </div>
                                <div className="showPc">
                            {this.state.fileData==""?<img src={sessionStorage.getItem("profile")}/>:<img src={this.state.fileData} />}   
                                    <label className="pad-top-20">Profile Preview</label>
                                </div>
                           </div>       
                    </div>
                    <div className="imageUpload">
                        {/* <div className="uploadStatus">
                            <img src={uploadFailed} />
                            <label>Image upload Failed !</label>
                            <p>please try again </p>
                        </div> */}

                        
                            {/* <p>{this.state.successMessage}</p> */}
             
                        {this.state.successMessage!="" ?<div className="uploadStatus">
                            <img src={greenCheck} />
                            <label>Image upload Successful !</label>
                            <p>Everything looks good </p>
                        </div>:""}
                        {this.state.errorMessage!="" ?<div className="uploadStatus">
                            <img src={uploadFailed} />
                            <label>Image upload Failed !</label>
                            <p>please try again </p>
                        </div>:""}
                        {this.state.image?  <div className="imgUploadNote m-top-45">
                            <p>Note - Image should be  png ,jpeg, jpg or svg file</p>
                        </div>:""}
                        {this.state.imageSize ?<div className="imgUploadNote m-top-45">
                            <p>Note - Image size can't be greater than 5 MB</p>
                        </div>:""}
                    
                    </div>
                    <div className="profileModalBot m-top-45">
                       {this.state.btndisable?<button type="button" className="saveBtnBorder btnDisabled" id="saveBtn" disabled>UPLOAD & SAVE CHANGES</button>:<button type="button" className="saveBtnBorder" id="saveBtn" onClick={(e)=>this.fileSelectedHandler(e)}>UPLOAD & SAVE CHANGES</button>} 
                        <button type="button" className="discardBtn" onClick={(e)=>this.props.editProfile(e)}>Discard & Exit</button>
                    </div>
                    
                </div>
               
               
                
            </div>
            
      
        </div>
        {this.state.loader ? <FilterLoader /> : null}
         {/* {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null} */}
        {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} />:null}
        </div>
        )
    }
}

export default ProfileModal;