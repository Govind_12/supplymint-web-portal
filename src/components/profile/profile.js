import React from 'react';
import openRack from '../../assets/open-rack.svg'
import errorIcon from "../../assets/error_icon.svg"
import profile from "../../assets/avatar.svg";
import { Link } from "react-router-dom";
import BraedCrumps from '../breadCrumps';
import FilterLoader from '../loaders/filterLoader';
import ChangePassword from './changePassword';
import SupportAccess from './supportAccess';


class ProfileUser extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: "",
            lastName: "",
            mobileNo: "",
            email: "",
            updatedOn: "",
            loader: false
        };
    }
    componentDidMount() {
        this.props.getProfileDetailsRequest();
        this.props.getSupportAccessRequest();
    }
    // componentWillMount() {
    //     if (!this.props.auth.getUserName.isSuccess) {
    //         this.props.getUserNameRequest(sessionStorage.getItem('userName'));
    //         this.setState({
    //             // loader: false
    //         })
    //     } else {
    //         this.props.getUserNameRequest(sessionStorage.getItem('userName'));
    //     }
    //     if (this.props.auth.getUserName.isSuccess) {
    //         this.setState({
    //             updatedOn: this.props.auth.getUserName.data.resource.updatedOn,
    //             firstName: this.props.auth.getUserName.data.resource.userDetails.firstName,
    //             lastName: this.props.auth.getUserName.data.resource.userDetails.lastName,
    //             mobileNo: this.props.auth.getUserName.data.resource.userDetails.mobileNumber,
    //             email: this.props.auth.getUserName.data.resource.userDetails.email
    //         })
    //     }
    // }

    // componentWillReceiveProps(nextprops) {
    //     if (nextprops.auth.getUserName.isSuccess) {
    //         this.setState({
    //             updatedOn: nextprops.auth.getUserName.data.resource.updatedOn,
    //             firstName: nextprops.auth.getUserName.data.resource.userDetails.firstName,
    //             lastName: nextprops.auth.getUserName.data.resource.userDetails.lastName,
    //             mobileNo: nextprops.auth.getUserName.data.resource.userDetails.mobileNumber,
    //             email: nextprops.auth.getUserName.data.resource.userDetails.email,
    //             loader: false,
    //         })
    //         sessionStorage.setItem("firstName", nextprops.auth.getUserName.data.resource.userDetails.firstName)
    //         sessionStorage.setItem("lastName", nextprops.auth.getUserName.data.resource.userDetails.lastName)
    //         sessionStorage.setItem("email", nextprops.auth.getUserName.data.resource.userDetails.email)
    //     } else if (!nextprops.auth.getUserName.isLoading) {
    //         this.setState({
    //             loader: false
    //         })
    //     }
    //     if (nextprops.auth.getUserName.isLoading) {
    //         this.setState({
    //             loader: true
    //         })
    //     }
    // }

    render() {
        return (
            <div className="container-fluid pad-0" id="home">
                <div className="col-lg-12 pad-0">
                    <div className="col-lg-2 col-md-2 col-sm-12 pad-0">
                        <div className="user-profile-page">
                            <div className="upp-head">
                                <h3>User Profile</h3>
                            </div>
                            <div className="upp-body">
                                <ul className="nav nav-tabs" role="tablist">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="#userinfo" role="tab" data-toggle="tab">
                                            <span className="ipp-nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 21 25.171">
                                                    <path d="M93.094 12.125a5.866 5.866 0 0 0 4.287-1.776 5.867 5.867 0 0 0 1.776-4.286 5.867 5.867 0 0 0-1.777-4.287 6.061 6.061 0 0 0-8.573 0 5.866 5.866 0 0 0-1.776 4.286 5.866 5.866 0 0 0 1.776 4.287 5.868 5.868 0 0 0 4.287 1.776zm-3.243-9.306a4.586 4.586 0 0 1 6.487 0 4.383 4.383 0 0 1 1.344 3.243 4.383 4.383 0 0 1-1.344 3.244 4.585 4.585 0 0 1-6.487 0 4.382 4.382 0 0 1-1.344-3.244 4.382 4.382 0 0 1 1.344-3.243zm0 0" transform="translate(-82.753)"/>
                                                    <path d="M20.949 254.513a14.974 14.974 0 0 0-.2-1.59 12.531 12.531 0 0 0-.391-1.6 7.9 7.9 0 0 0-.658-1.491 5.622 5.622 0 0 0-.991-1.292 4.371 4.371 0 0 0-1.424-.895 4.921 4.921 0 0 0-1.818-.329 1.845 1.845 0 0 0-.985.418c-.3.193-.641.415-1.026.662a5.881 5.881 0 0 1-1.328.585 5.157 5.157 0 0 1-3.249 0 5.864 5.864 0 0 1-1.327-.585c-.382-.244-.728-.467-1.027-.662a1.843 1.843 0 0 0-.985-.418 4.914 4.914 0 0 0-1.818.329 4.367 4.367 0 0 0-1.424.895 5.623 5.623 0 0 0-.991 1.292 7.913 7.913 0 0 0-.657 1.492 12.559 12.559 0 0 0-.391 1.6 14.871 14.871 0 0 0-.2 1.591c-.033.481-.05.981-.05 1.486a4.18 4.18 0 0 0 1.241 3.162 4.468 4.468 0 0 0 3.2 1.167h12.11a4.469 4.469 0 0 0 3.2-1.167A4.178 4.178 0 0 0 21 256c0-.507-.017-1.008-.051-1.487zm-2.208 3.58a3.009 3.009 0 0 1-2.182.76H4.44a3.009 3.009 0 0 1-2.182-.76A2.729 2.729 0 0 1 1.475 256c0-.472.016-.937.047-1.384a13.413 13.413 0 0 1 .184-1.432 11.078 11.078 0 0 1 .344-1.41 6.443 6.443 0 0 1 .535-1.213 4.167 4.167 0 0 1 .728-.955 2.9 2.9 0 0 1 .947-.589 3.394 3.394 0 0 1 1.162-.224c.052.027.144.08.293.177.3.2.653.423 1.039.67a7.309 7.309 0 0 0 1.667.745 6.631 6.631 0 0 0 4.156 0 7.316 7.316 0 0 0 1.668-.745c.4-.253.735-.472 1.038-.669.149-.1.241-.15.293-.177a3.4 3.4 0 0 1 1.162.224 2.9 2.9 0 0 1 .947.589 4.157 4.157 0 0 1 .728.955 6.421 6.421 0 0 1 .535 1.212 11.051 11.051 0 0 1 .344 1.41 13.528 13.528 0 0 1 .184 1.433c.031.445.047.911.047 1.384a2.729 2.729 0 0 1-.782 2.092zm0 0" transform="translate(0 -235.158)"/>
                                                </svg>
                                            </span>
                                            User Info</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#changepassword" role="tab" data-toggle="tab">
                                            <span className="ipp-nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 18.707 25.622">
                                                    <path d="M17.957 8.233H14.4V4.577A4.837 4.837 0 0 0 9.354 0 4.837 4.837 0 0 0 4.3 4.577v3.656H.751A.751.751 0 0 0 0 8.984v9.6a7.046 7.046 0 0 0 7.038 7.038h4.632a7.046 7.046 0 0 0 7.038-7.038v-9.6a.751.751 0 0 0-.751-.751zM5.8 4.577A3.342 3.342 0 0 1 9.354 1.5 3.342 3.342 0 0 1 12.9 4.577v3.656H5.8zm11.4 14.007a5.543 5.543 0 0 1-5.536 5.536H7.038A5.543 5.543 0 0 1 1.5 18.584V9.735h15.7zm0 0"/>
                                                    <g transform="translate(3.805 15.29)">
                                                        <circle cx="1" cy="1" r="1" transform="translate(-.122 .332)"/>
                                                        <circle cx="1" cy="1" r="1" transform="translate(2.878 .332)"/>
                                                        <circle cx="1" cy="1" r="1" transform="translate(5.878 .332)"/>
                                                        <circle cx="1" cy="1" r="1" transform="translate(8.878 .332)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            Change Password</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#supportaccess" role="tab" data-toggle="tab">
                                            <span className="ipp-nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="help" width="16.501" height="16.501" viewBox="0 0 14.501 14.501">
                                                    <path fill="#000" id="Path_1359" d="M14.5 9.814A4.7 4.7 0 0 0 11.068 5.3a5.537 5.537 0 1 0-10.3 3.051l-.747 2.7 2.7-.747a5.516 5.516 0 0 0 2.574.762 4.689 4.689 0 0 0 6.9 2.781l2.282.631-.628-2.278a4.671 4.671 0 0 0 .651-2.386zM2.857 9.387l-1.618.448.448-1.618-.1-.16a4.686 4.686 0 1 1 1.429 1.432zm10.405 3.875l-1.2-.332-.16.1a3.839 3.839 0 0 1-5.726-2 5.547 5.547 0 0 0 4.862-4.862 3.839 3.839 0 0 1 2 5.726l-.1.16zm0 0" class="cls-1"/>
                                                    <path fill="#000" id="Path_1360" d="M180.5 271h.85v.85h-.85zm0 0" class="cls-1" transform="translate(-175.388 -263.325)"/>
                                                    <path fill="#000" id="Path_1361" d="M138.049 91.7a.841.841 0 0 1-.277.627l-1 .913v1.037h.85v-.663l.722-.661A1.7 1.7 0 1 0 135.5 91.7h.85a.85.85 0 1 1 1.7 0zm0 0" class="cls-1" transform="translate(-131.662 -87.451)"/>
                                                </svg>
                                            </span>
                                            Support Access</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-10 col-md-10 col-sm-12 pad-0">
                        <div className="tab-content">
                            <div className="tab-pane fade in active" id="userinfo" role="tabpanel">
                                <div className="user-info-tab-details">
                                    <div className="uitd-head">
                                        <div className="uitdh-name">
                                            {/* {sessionStorage.getItem("profile")!=null   ?<img src={sessionStorage.getItem("profile")} className="img_profile profileIcon thumbnail editProfileImg" alt=" " />:   <img src={profile} className="img_profile profileIcon thumbnail editProfileImg" alt="not found" />}*/}
                                            <span>{sessionStorage.getItem('firstName') == null ? "" : sessionStorage.getItem('firstName').charAt(0)}</span>
                                        </div>
                                        <div className="uitdh-info">
                                            <h3>{this.props.profileDetails.firstName} {this.props.profileDetails.lastName}</h3>
                                            <p>{this.props.profileDetails.email}</p>
                                            <Link to="/profile/edit"><button type="button" className="gen-save">Edit Profile</button></Link>
                                        </div>
                                    </div>
                                    <div className="uitd-body">
                                        <div className="uitdb-row">
                                            <div className="uitdbr-inner">
                                                <label>First Name</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.firstName} disabled />
                                            </div>
                                            <div className="uitdbr-inner">
                                                <label>Last Name</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.lastName} disabled />
                                            </div>
                                        </div>
                                        <div className="uitdb-row">
                                            <div className="uitdbr-inner">
                                                <label>Email</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.email} disabled />
                                            </div>
                                            <div className="uitdbr-inner">
                                                <label>Mobile No</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.phone} disabled />
                                            </div>
                                        </div>
                                        <div className="uitdb-row">
                                            <div className="uitdbr-inner">
                                                <label>Enterprise Id</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.enterpriseId} disabled />
                                            </div>
                                            <div className="uitdbr-inner">
                                                <label>Organisation Id</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.organisationId} disabled />
                                            </div>
                                        </div>
                                        <div className="uitdb-row">
                                            <div className="uitdbr-inner">
                                                <label>Username</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.username} disabled />
                                            </div>
                                            <div className="uitdbr-inner">
                                                <label>User Type</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.uType} disabled />
                                            </div>
                                        </div>
                                        <div className="uitdb-row">
                                            <div className="uitdbr-inner">
                                                <label>User Code</label>
                                                <input className="profil-dis-input" type="text" value={this.props.profileDetails.uCode} disabled />
                                            </div>
                                            <div className="uitdbr-inner">
                                                <label>Language</label>
                                                <input className="profil-dis-input" type="text" value="English" disabled />
                                            </div>
                                        </div>
                                        {/* <div className="uitdb-row">
                                            <div className="uitdbr-inner">
                                                <label>Timezone</label>
                                                <input className="profil-dis-input" type="text" value="+5:30 GMT" />
                                            </div>
                                        </div> */}
                                    </div>
                                    <div className="uitd-footer m-top-70">
                                        <p>Last Profile Update <span>{this.props.profileDetails.updatedOn}</span></p>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="changepassword" role="tabpanel">
                                <ChangePassword  {...this.props}/>
                            </div>
                            <div className="tab-pane fade" id="supportaccess" role="tabpanel">
                                <SupportAccess {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
            </div>
        )
    }
}

export default ProfileUser;