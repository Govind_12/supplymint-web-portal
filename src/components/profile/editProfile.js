import React from 'react';
import openRack from '../../assets/open-rack.svg'
// import errorIcon from "../../assets/error_icon.svg"
import profile from "../../assets/avatar.svg";
import { Link } from "react-router-dom";
import BraedCrumps from '../breadCrumps';
import imageChoose from '../../assets/edit-icon.svg';

import FilterLoader from "../loaders/filterLoader";
import ProfileModal from "./profileModal";
import ChangePassword from './changePassword';

class EditProfile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            updatedOn: "",
            fname: "",
            lname: "",
            email: "",
            mobile: "",
            // locale: "",
            timezone: "",
            enterpriseId: "",
            organisationId: "",
            username: "",
            userType: "",
            userCode: "",
            fnameerr: false,
            lnameerr: false,
            emailerr: false,
            mobileerr: false,
            // localeerr: false,
            timezoneerr: false,
            imageRemove: "",
            image: "",
            loader: false,
            btnDisable: true,
            profileModal: false

        };
    }
    componentDidMount() {
        this.props.getProfileDetailsRequest()
    }
    // componentWillMount() {
    //     if (!this.props.auth.getUserName.isSuccess) {

    //         this.props.getUserNameRequest(sessionStorage.getItem('userName'));
    //         this.setState({

    //             loader: false
    //         })
    //     }
    //     if (this.props.auth.getUserName.isSuccess) {
    //         this.setState({
    //             updatedOn: this.props.auth.getUserName.data.resource.updatedOn,
    //             fname: this.props.auth.getUserName.data.resource.userDetails.firstName,
    //             lname: this.props.auth.getUserName.data.resource.userDetails.lastName,
    //             mobile: this.props.auth.getUserName.data.resource.userDetails.mobileNumber == null ? "" : this.props.auth.getUserName.data.resource.userDetails.mobileNumber,
    //             email: this.props.auth.getUserName.data.resource.userDetails.email
    //         })
    //     }
    // }
    componentWillReceiveProps(nextprops, prevrops) {
        if (nextprops.auth.updateProfile.isLoading) {
            sessionStorage.removeItem("firstName");
            sessionStorage.removeItem("lastName");
            sessionStorage.removeItem("email");
        }

        if (nextprops.auth.getUserName.isSuccess) {
            this.setState({
                updatedOn: nextprops.auth.getUserName.data.resource.updatedOn,
                fname: nextprops.auth.getUserName.data.resource.userDetails.firstName,
                lname: nextprops.auth.getUserName.data.resource.userDetails.lastName,
                mobile: nextprops.auth.getUserName.data.resource.userDetails.mobileNumber == null ? "" : nextprops.auth.getUserName.data.resource.userDetails.mobileNumber,
                email: nextprops.auth.getUserName.data.resource.userDetails.email,
                enterpriseId: nextprops.auth.getUserName.data.resource.enterpriseId == null ? "" : nextprops.auth.getUserName.data.resource.enterpriseId,
                organisationId: nextprops.auth.getUserName.data.resource.organisationId == null ? "" : nextprops.auth.getUserName.data.resource.organisationId,
                username: nextprops.auth.getUserName.data.resource.username == null ? "" : nextprops.auth.getUserName.data.resource.username,
                userType: nextprops.auth.getUserName.data.resource.uType == null ? "" : nextprops.auth.getUserName.data.resource.uType,
                userCode: nextprops.auth.getUserName.data.resource.uCode == null ? "" : nextprops.auth.getUserName.data.resource.uCode

            })
        }
        if (nextprops.auth.getProfileDetails.isSuccess) {
            this.setState({
                updatedOn: nextprops.auth.getProfileDetails.data.resource.updatedOn,
                fname: nextprops.auth.getProfileDetails.data.resource.firstName,
                lname: nextprops.auth.getProfileDetails.data.resource.lastName,
                email: nextprops.auth.getProfileDetails.data.resource.email,
                mobile: nextprops.auth.getProfileDetails.data.resource.phone,
                enterpriseId: nextprops.auth.getProfileDetails.data.resource.enterpriseId == null ? "" : nextprops.auth.getProfileDetails.data.resource.enterpriseId,
                organisationId: nextprops.auth.getProfileDetails.data.resource.organisationId == null ? "" : nextprops.auth.getProfileDetails.data.resource.organisationId,
                username: nextprops.auth.getProfileDetails.data.resource.username == null ? "" : nextprops.auth.getProfileDetails.data.resource.username,
                userType: nextprops.auth.getProfileDetails.data.resource.uType == null ? "" : nextprops.auth.getProfileDetails.data.resource.uType,
                userCode: nextprops.auth.getProfileDetails.data.resource.uCode == null ? "" : nextprops.auth.getProfileDetails.data.resource.uCode
            })
        }

    }
    onSubmit(e) {
        e.preventDefault();
        this.fname();
        this.lname();
        this.email();

        // this.locale();
        // this.timezone();
        const t = this;
        setTimeout(function () {
            const { fnameerr, lnameerr, emailerr, mobileerr } = t.state;
            if (!fnameerr && !lnameerr && !emailerr && !mobileerr) {
                let updateProfile = {
                    // userName: sessionStorage.getItem('userName'),
                    firstName: t.state.fname,
                    lastName: t.state.lname,
                    // mobileNumber: t.state.mobile,
                    // eid: sessionStorage.getItem('partnerEnterpriseId')
                }
                // t.props.updateProfileRequest(updateProfile);
                t.props.updateUserDetailsRequest(updateProfile);
            }
        }, 100)
    }

    fname() {
        if (
            this.state.fname == "" || this.state.fname.trim() == "" || !this.state.fname.match(/^[a-zA-Z ]+$/)
        ) {
            this.setState({
                fnameerr: true
            });
        } else {
            this.setState({
                fnameerr: false
            });
        }
    }
    lname() {
        if (
            this.state.lname == "" || this.state.lname.trim() == "" || !this.state.lname.match(/^[a-zA-Z ]+$/)
        ) {
            this.setState({
                lnameerr: true
            });
        } else {
            this.setState({
                lnameerr: false
            });
        }
    }
    email() {
        if (
            this.state.email == "" ||
            !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                emailerr: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }
    mobile() {
        if (
            this.state.mobile == "" ||
            !this.state.mobile.match(/^\d{10}$/) ||
            !this.state.mobile.match(/^[1-9]\d+$/) ||
            !this.state.mobile.match(/^.{10}$/)) {
            this.setState({
                mobileerr: true
            });
        } else {
            this.setState({
                mobileerr: false
            });
        }
    }
    locale() {
        if (
            this.state.locale == "" ||
            !this.state.locale.match(/^[a-zA-Z]+$/)
        ) {
            this.setState({
                localeerr: true
            });
        } else {
            this.setState({
                localeerr: false
            });
        }
    }
    timezone() {
        if (
            this.state.timezone == ""
        ) {
            this.setState({
                timezoneerr: true
            });
        } else {
            this.setState({
                timezoneerr: false
            });
        }
    }
    onClear(e) {
        e.preventDefault();
        this.setState({
            fname: "",
            lname: "",
            // email: "",
            mobile: "",
            // locale: "",
            timezone: "",
        })
    }



    handleChange(e) {
        this.setState({
            btnDisable: false
        })
        if (e.target.id == "fname") {
            this.setState(
                {
                    fname: e.target.value
                },
                () => {
                    this.fname();
                }
            );
            sessionStorage.setItem('firstName', e.target.value)
        } else if (e.target.id == "lname") {
            this.setState(
                {
                    lname: e.target.value
                },
                () => {
                    this.lname();
                }
            );
            sessionStorage.setItem('lastName', e.target.value)
        } else if (e.target.id == "email") {
            this.setState(
                {
                    email: e.target.value
                },
                () => {
                    this.email();
                }
            );
        } else if (e.target.id == "mobile") {
            this.setState({
                mobile: e.target.value
            },
                () => {
                    this.mobile();
                });
        }
        else if (e.target.id == "locale") {
            this.setState({
                locale: e.target.value
            }, () => {
                this.locale();
            });
        }
        else if (e.target.id == "timezone") {
            e.target.placeholder = e.target.value;
            this.setState({
                timezone: e.target.value
            }, () => {
                this.timezone();
            });
        }
        // else   if (e.target.id=="chooseImage"){
        //     this.setState({
        //         imageRemove:e.target.value
        //     })
        //     this.fileSelectedHandler(e)
        // }
    }
    editProfile() {
        this.setState({
            profileModal: !this.state.profileModal
        })

    }

    render() {
        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        const { fname, fnameerr, lname, lnameerr, email, emailerr, mobile, mobileerr, locale, localeerr, timezone, timezoneerr, imageRemove, image, enterpriseId, organisationId, username, userType, userCode } = this.state;

        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="col-lg-2 col-md-2 col-sm-12 pad-0">
                        <div className="user-profile-page">
                            <div className="upp-head">
                                <h3>User Profile</h3>
                            </div>
                            <div className="upp-body">
                                <ul className="nav nav-tabs" role="tablist">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="#userinfo" role="tab" data-toggle="tab">
                                            <span className="ipp-nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 21 25.171">
                                                    <path d="M93.094 12.125a5.866 5.866 0 0 0 4.287-1.776 5.867 5.867 0 0 0 1.776-4.286 5.867 5.867 0 0 0-1.777-4.287 6.061 6.061 0 0 0-8.573 0 5.866 5.866 0 0 0-1.776 4.286 5.866 5.866 0 0 0 1.776 4.287 5.868 5.868 0 0 0 4.287 1.776zm-3.243-9.306a4.586 4.586 0 0 1 6.487 0 4.383 4.383 0 0 1 1.344 3.243 4.383 4.383 0 0 1-1.344 3.244 4.585 4.585 0 0 1-6.487 0 4.382 4.382 0 0 1-1.344-3.244 4.382 4.382 0 0 1 1.344-3.243zm0 0" transform="translate(-82.753)"/>
                                                    <path d="M20.949 254.513a14.974 14.974 0 0 0-.2-1.59 12.531 12.531 0 0 0-.391-1.6 7.9 7.9 0 0 0-.658-1.491 5.622 5.622 0 0 0-.991-1.292 4.371 4.371 0 0 0-1.424-.895 4.921 4.921 0 0 0-1.818-.329 1.845 1.845 0 0 0-.985.418c-.3.193-.641.415-1.026.662a5.881 5.881 0 0 1-1.328.585 5.157 5.157 0 0 1-3.249 0 5.864 5.864 0 0 1-1.327-.585c-.382-.244-.728-.467-1.027-.662a1.843 1.843 0 0 0-.985-.418 4.914 4.914 0 0 0-1.818.329 4.367 4.367 0 0 0-1.424.895 5.623 5.623 0 0 0-.991 1.292 7.913 7.913 0 0 0-.657 1.492 12.559 12.559 0 0 0-.391 1.6 14.871 14.871 0 0 0-.2 1.591c-.033.481-.05.981-.05 1.486a4.18 4.18 0 0 0 1.241 3.162 4.468 4.468 0 0 0 3.2 1.167h12.11a4.469 4.469 0 0 0 3.2-1.167A4.178 4.178 0 0 0 21 256c0-.507-.017-1.008-.051-1.487zm-2.208 3.58a3.009 3.009 0 0 1-2.182.76H4.44a3.009 3.009 0 0 1-2.182-.76A2.729 2.729 0 0 1 1.475 256c0-.472.016-.937.047-1.384a13.413 13.413 0 0 1 .184-1.432 11.078 11.078 0 0 1 .344-1.41 6.443 6.443 0 0 1 .535-1.213 4.167 4.167 0 0 1 .728-.955 2.9 2.9 0 0 1 .947-.589 3.394 3.394 0 0 1 1.162-.224c.052.027.144.08.293.177.3.2.653.423 1.039.67a7.309 7.309 0 0 0 1.667.745 6.631 6.631 0 0 0 4.156 0 7.316 7.316 0 0 0 1.668-.745c.4-.253.735-.472 1.038-.669.149-.1.241-.15.293-.177a3.4 3.4 0 0 1 1.162.224 2.9 2.9 0 0 1 .947.589 4.157 4.157 0 0 1 .728.955 6.421 6.421 0 0 1 .535 1.212 11.051 11.051 0 0 1 .344 1.41 13.528 13.528 0 0 1 .184 1.433c.031.445.047.911.047 1.384a2.729 2.729 0 0 1-.782 2.092zm0 0" transform="translate(0 -235.158)"/>
                                                </svg>
                                            </span>
                                            User Info</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#changepassword" role="tab" data-toggle="tab">
                                            <span className="ipp-nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 18.707 25.622">
                                                    <path d="M17.957 8.233H14.4V4.577A4.837 4.837 0 0 0 9.354 0 4.837 4.837 0 0 0 4.3 4.577v3.656H.751A.751.751 0 0 0 0 8.984v9.6a7.046 7.046 0 0 0 7.038 7.038h4.632a7.046 7.046 0 0 0 7.038-7.038v-9.6a.751.751 0 0 0-.751-.751zM5.8 4.577A3.342 3.342 0 0 1 9.354 1.5 3.342 3.342 0 0 1 12.9 4.577v3.656H5.8zm11.4 14.007a5.543 5.543 0 0 1-5.536 5.536H7.038A5.543 5.543 0 0 1 1.5 18.584V9.735h15.7zm0 0"/>
                                                    <g transform="translate(3.805 15.29)">
                                                        <circle cx="1" cy="1" r="1" transform="translate(-.122 .332)"/>
                                                        <circle cx="1" cy="1" r="1" transform="translate(2.878 .332)"/>
                                                        <circle cx="1" cy="1" r="1" transform="translate(5.878 .332)"/>
                                                        <circle cx="1" cy="1" r="1" transform="translate(8.878 .332)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            Change Password</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-10 col-md-10 col-sm-12 pad-0">
                        <div className="tab-content">
                            <div className="tab-pane fade in active" id="userinfo" role="tabpanel">
                                <div className="user-info-tab-details">
                                    <div className="uitd-head">
                                        <div className="uitdh-name">
                                            {/* {sessionStorage.getItem("profile")==null?this.state.image!=""?<img src={image} className="img_profile profileIcon thumbnail editProfileImg" alt="not found" />:<img src={profile} className="img_profile profileIcon thumbnail editProfileImg" alt="not found" />:<img src={sessionStorage.getItem("profile")} className="img_profile profileIcon thumbnail editProfileImg" alt="not found" />}      
                                            <label className="chooseimg"> */}
                                            {/* <input type="file" value={imageRemove} id ="chooseImage" onChange={(e)=>this.handleChange(e)}/> */}
                                            {/* <img src={imageChoose} onClick={(e)=>this.editProfile(e)} /> */}
                                            {/* </label> */}
                                            <span>{sessionStorage.getItem('firstName') == null ? "" : sessionStorage.getItem('firstName').charAt(0)}</span>
                                            {/* <span className="uitdh-edit">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12.297 12.35">
                                                    <g id="edit_3_" transform="translate(-.757)">
                                                        <path fill="#fff" id="Path_980" d="M102.8.416L102.784.4a1.539 1.539 0 0 0-2.174.1l-5.5 6.021a.531.531 0 0 0-.112.19l-.646 1.939a.737.737 0 0 0 .7.97.734.734 0 0 0 .3-.062l1.872-.819a.53.53 0 0 0 .179-.128l5.5-6.021A1.541 1.541 0 0 0 102.8.416zm-7.209 7.876l.379-1.138.03-.034.719.657-.032.035zm6.522-6.418l-4.675 5.118-.719-.657 4.675-5.118a.477.477 0 0 1 .674-.03l.015.014a.477.477 0 0 1 .03.673z" class="cls-1" transform="translate(-90.245)"/>
                                                        <path fill="#fff" id="Path_981" d="M11.486 36.023a.531.531 0 0 0-.531.531v4.508a1.352 1.352 0 0 1-1.35 1.35H3.169a1.352 1.352 0 0 1-1.35-1.35v-6.384a1.352 1.352 0 0 1 1.35-1.35h4.658a.531.531 0 0 0 0-1.062H3.169a2.415 2.415 0 0 0-2.412 2.412v6.383a2.415 2.415 0 0 0 2.412 2.412H9.6a2.415 2.415 0 0 0 2.412-2.412v-4.507a.531.531 0 0 0-.526-.531z" class="cls-1" transform="translate(0 -31.124)"/>
                                                    </g>
                                                </svg>
                                            </span> */}
                                        </div>
                                        <div className="uitdh-info uitdh-info60">
                                            <h3>{fname} {lname}</h3>
                                            <p>{email}</p>
                                        </div>
                                    </div>
                                    <div className="uitd-body">
                                        <form onSubmit={(e) => this.onSubmit(e)} autoComplete="off">
                                            <div className="uitdb-row">
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="firstname">First Name</label>
                                                    <input type="text" autoComplete="off" value={fname} onChange={(e) => this.handleChange(e)} className={fnameerr ? "errorBorder" : "onFocus"} id="fname" placeholder="First Name" />
                                                    {/* {fnameerr ? <img src={errorIcon} className="profile_error_icon" /> : null} */}
                                                    {fnameerr ? (
                                                        <span className="error">
                                                            Enter  Valid First Name
                                                        </span>
                                                    ) : null}
                                                </div>
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="lastname">Last Name</label>
                                                    <input type="text" autoComplete="off" value={lname} onChange={(e) => this.handleChange(e)} className={lnameerr ? "errorBorder" : "onFocus"} id="lname" placeholder="Last name" />
                                                    {/* {lnameerr ? <img src={errorIcon} className="profile_error_icon" /> : null} */}
                                                    {lnameerr ? (
                                                        <span className="error">
                                                            Enter  valid last name
                                                        </span>
                                                    ) : null}
                                                </div>
                                            </div>
                                            <div className="uitdb-row">
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="email">Email</label>
                                                    <input type="email" autoComplete="off" value={email} onChange={(e) => this.handleChange(e)} className={emailerr ? "profil-dis-input errorBorder" : "profil-dis-input"} id="email" disabled placeholder="Email" />
                                                    {/* {emailerr ? <img src={errorIcon} className="profile_error_icon" /> : null} */}
                                                    {emailerr ? (
                                                        <span className="error">
                                                            Enter valid email
                                                        </span>
                                                    ) : null}
                                                </div>
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="mobile">Mobile No</label>
                                                    <input type="text" autoComplete="off" maxLength="10" disabled value={mobile} onChange={(e) => this.handleChange(e)} className={mobileerr ? "errorBorder profil-dis-input" : "profil-dis-input"} id="mobile" placeholder="Mobile Number" />
                                                    {/* {mobileerr ? <img src={errorIcon} className="profile_error_icon" /> : null} */}
                                                    {mobileerr ? (
                                                        <span className="error">
                                                            Enter valid mobile number
                                                        </span>
                                                    ) : null}
                                                </div>
                                            </div>
                                            <div className="uitdb-row">
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="enterpriseId">Enterprise Id</label>
                                                    <input className="profil-dis-input" type="text" id="enterpriseId" value={enterpriseId} disabled />
                                                </div>
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="organisationId">Organisation Id</label>
                                                    <input className="profil-dis-input" type="text" id="organisationId" value={organisationId} disabled />
                                                </div>
                                            </div>
                                            <div className="uitdb-row">
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="username">Username</label>
                                                    <input className="profil-dis-input" type="text" id="username" value={username} disabled />
                                                </div>
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="userType">User Type</label>
                                                    <input className="profil-dis-input" type="text" id="userType" value={userType} disabled />
                                                </div>
                                            </div>
                                            <div className="uitdb-row">
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="userCode">User Code</label>
                                                    <input className="profil-dis-input" type="text" id="userCode" value={userCode} disabled />
                                                </div>
                                                <div className="uitdbr-inner">
                                                    <label htmlFor="locale">Language</label>
                                                    <input className="profil-dis-input" type="text" id="locale" value="English" disabled />
                                                    {localeerr ? (
                                                        <span className="error">
                                                            Enter valid language
                                                        </span>
                                                    ) : null}
                                                </div>
                                                {/* <div className="uitdbr-inner">
                                                <label htmlFor="time">Timezone</label>
                                                <select className={timezoneerr ? "form-box errorBorder":"form-box"} placeholder="Timezone" onChange={(e) => this.handleChange(e)} value={timezone} id="timezone" title="Timezone" name="DropDownTimezone" >
                                                <option value="">Timezone</option>
                                                    <option value="(GMT +5:30) Bombay, Calcutta, Madras, New Delhi">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                                                        </select>
                                                        {timezoneerr ? (
                                                            <span className="error">
                                                                Enter valid timezone
                                                            </span>
                                                        ) : null}
                                                </div> */}
                                            </div>
                                            <div className="uitdb-row m-top-50">
                                               {this.state.btnDisable ? <button type="button" className="gen-save btnDisabled">Save</button> : <button type="submit" className="gen-save">Save</button>}
                                                <button type="button" onClick={(e) => this.props.history.push("/profile")} className="gen-clear">Cancel</button>
                                                {/* <button type="reset" onClick={(e) => this.onClear(e)} className="gen-clear">Clear</button> */}
                                            </div>
                                        </form>
                                    </div>
                                    <div className="uitd-footer">
                                        <p>Last Profile Update <span>{this.state.updatedOn}</span></p>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="changepassword" role="tabpanel">
                                <ChangePassword  {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <div className="col-md-12 col-sm-12 col-xs-12 pad-0 ">
                    <div className="m-top-20">
                        <p className="profile_title main_profile">Want to Change Your Password ?<Link to="/profile/changePassword"><span className="profile_name pad-lft-10" >Click here</span></Link></p>
                    </div>
                </div> */}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.profileModal ? <ProfileModal editProfile={(e) => this.editProfile(e)} /> : null}
            </div>
        )
    }
}

export default EditProfile;