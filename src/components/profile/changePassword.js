import React from 'react';

class ChangePassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentPassword: "",
            currentPassworderr: false,
            newPassword: "",
            newPassworderr: false,
            reenterPassword: "",
            reenterPassworderr: false,
            btnDisable: true,
            type: 'password',
            type1: 'password',
            type2: 'password',
        };
        this.showHideCurrentPassword = this.showHideCurrentPassword.bind(this);
        this.showHideNewPassword = this.showHideNewPassword.bind(this);
        this.showHideReNewPassword = this.showHideReNewPassword.bind(this);
    }
    
    showHideCurrentPassword(e){
        this.setState({
          type: this.state.type === 'password' ? 'text' : 'password'
        })
    }
    showHideNewPassword(e){
        this.setState({
            type1: this.state.type1 === 'password' ? 'text' : 'password'
        })
    }
    showHideReNewPassword(e){
        this.setState({
            type2: this.state.type2 === 'password' ? 'text' : 'password'
        })
    }


    currentPassword =()=> {
        if (this.state.currentPassword == "" || !this.state.currentPassword.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/)) {
            this.setState({
                currentPassworderr: true
            });
        } else {
            this.setState({

                currentPassworderr: false
            });
        }
    }

    newPassword =()=> {
        if (this.state.newPassword == "" || !this.state.newPassword.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/)) {
            this.setState({
                newPassworderr: true
            });
        } else {
            this.setState({
                newPassworderr: false
            });
        }
    }

    reenterPassword =()=> {
        if (this.state.reenterPassword == "" || !this.state.reenterPassword.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/) || (document.getElementById("newPassword").value != document.getElementById("reenterPassword").value)) {
            this.setState({
                reenterPassworderr: true
            });
        } else {
            this.setState({
                reenterPassworderr: false
            });
        }
    }
    handleChange =(e)=> {
        this.setState({
            btnDisable: false
        })
        if (e.target.id == "currentPassword") {
            this.setState(
                {
                    currentPassword: e.target.value
                },
                () => {
                    this.currentPassword();
                }
            );
        }
        else if (e.target.id == "newPassword") {
            this.setState(
                {
                    newPassword: e.target.value
                },
                () => {
                    this.newPassword();
                }
            );
        }
        else if (e.target.id == "reenterPassword") {
            this.setState(
                {
                    reenterPassword: e.target.value
                },
                () => {
                    this.reenterPassword();
                }
            );

        }
    }

    contactSubmit =(e)=> {
        e.preventDefault();
        this.currentPassword();
        this.newPassword();
        this.reenterPassword();
        setTimeout(()=> {
            const { currentPassworderr, currentPassword, newPassword, newPassworderr, reenterPassworderr } = this.state;
            if (!currentPassworderr && !newPassworderr && !reenterPassworderr) {
                let data = {
                    oldPassword: currentPassword,
                    password: newPassword
                }
                this.props.changePasswordRequest(data);
                this.onClear();
            }
        }, 100)
    }


    onClear =()=> {
        // e.preventDefault();
        this.setState({
            currentPassword: "",

            newPassword: "",
            reenterPassword: ""

        });
    }


    render() {
        const { currentPassword, currentPassworderr
            , newPassword, newPassworderr, reenterPassword, reenterPassworderr } = this.state;
            
        return (
            <div className="user-info-tab-details">
                <form name="changePasswordForm" className="organizationForm" onSubmit={(e) => this.contactSubmit((e))} autoComplete="off">
                    <div className="uitd-body">
                        <div className="uitdb-row">
                            <div className="uitdbr-inner">
                                <label>Current Password</label>
                                <input type={this.state.type} autoComplete="off" className={currentPassworderr ? "errorBorder ":"onFocus"} name="currentPassword" value={currentPassword} id="currentPassword" onChange={e => this.handleChange(e)} />
                                <span className="password__show" onClick={this.showHideCurrentPassword}>{this.state.type === 'password' ? <img src={require('../../assets/eye-open.svg')} /> : <img src={require('../../assets/hide.svg')} />}</span>
                                {/* {currentPassworderr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                {currentPassworderr ? (
                                    <span className="error">
                                        Enter Current Password
                                </span>
                                ) : null}
                            </div>
                        </div>
                        <div className="uitdb-row">
                            <div className="uitdbr-inner">
                                <label>New Password</label>
                                <input type={this.state.type1} autoComplete="off" className={newPassworderr ? "form-box errorBorder":"form-box"} name="newPassword" value={newPassword} id="newPassword" onChange={e => this.handleChange(e)} />
                                <span className="password__show" onClick={this.showHideNewPassword}>{this.state.type1 === 'password' ? <img src={require('../../assets/eye-open.svg')} /> : <img src={require('../../assets/hide.svg')} /> }</span>
                                {/* {newPassworderr ? <img src={errorIcon} className="error_icon" /> : null} */}
                            </div>
                            {newPassworderr ? (
                                <span className="error">
                                    Enter New Password<br />
                                    (must contain at least 1 lowercase alphabet, at least 1 uppercase alphabet, at least 1 numeric character ,at least one special character and must be eight characters or longer)
                                </span>
                            ) : null}
                        </div>
                        <div className="uitdb-row">
                            <div className="uitdbr-inner">
                                <label>Re-enter New Password</label>
                                <input type={this.state.type2} autoComplete="off" className={reenterPassworderr ? "errorBorder":"onFocus"} name="reenterPassword" value={reenterPassword} id="reenterPassword" onChange={e => this.handleChange(e)} />
                                <span className="password__show" onClick={this.showHideReNewPassword}>{this.state.type2 === 'password' ? <img src={require('../../assets/eye-open.svg')} /> : <img src={require('../../assets/hide.svg')} />}</span>
                                {/* {reenterPassworderr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                {reenterPassworderr ? (
                                    <span className="error">
                                        Enter Re-enter New Password
                                </span>
                                ) : null}
                            </div>
                        </div>
                        <div className="uitdb-row m-top-70">
                            {this.state.btnDisable ? <button type="button" className="gen-save btnDisabled">Save</button>:<button type="submit" className="gen-save">Save</button>}
                            <button type="reset" className="gen-clear" onClick={() => this.onClear()}>Clear</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default ChangePassword;