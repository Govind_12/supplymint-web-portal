import React from 'react';
import { Link } from 'react-router-dom';
import { openMyDrop, closeMyDrop } from '../helper/index'

class ShipmentDrop extends React.Component {

    render() {

        var Shipment = JSON.parse(sessionStorage.getItem('Shipment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Shipment')).crud)
        var Asn_Under_Approval = JSON.parse(sessionStorage.getItem('ASN Under Approval')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('ASN Under Approval')).crud)
        var Approved_Asn = JSON.parse(sessionStorage.getItem('Approved ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Approved ASN')).crud)
        var Cancelled_Asn = JSON.parse(sessionStorage.getItem('Cancelled ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled ASN')).crud)

        return (
            <label>
                <div className="dropdown" onMouseOver={(e) => openMyDrop(e)} id="myFav">
                    <button className="dropbtn home_link">Shipment
                    <i className="fa fa-chevron-down"></i>
                    </button>
                    {/* {sessionStorage.getItem("uType") == "ENT" && Shipment > 0 ? <div className="displayNone adminBreacrumbsDropdown anchorErrorSubMenu" id="dropMenu">
                        {Asn_Under_Approval > 0 ? <Link to="/enterprise/shipment/asnUnderApproval" onClick={(e) => closeMyDrop(e)}>ASN Under Approval</Link> : null}
                        {Approved_Asn > 0 ? <Link to="/enterprise/shipment/approvedAsn" onClick={(e) => closeMyDrop(e)}>Approved ASN</Link> : null}
                        {Cancelled_Asn > 0 ? <Link to="/enterprise/shipment/cancelledAsn" onClick={(e) => closeMyDrop(e)}>Rejected ASN</Link> : null}
                    </div> : null}
                    {sessionStorage.getItem("uType") == "VENDOR" && Shipment > 0 ? <div className="displayNone adminBreacrumbsDropdown anchorErrorSubMenu" id="dropMenu">
                        {Asn_Under_Approval > 0 ? <Link to="/vendor/shipment/asnUnderApproval" onClick={(e) => closeMyDrop(e)}>ASN Under Approval</Link> : null}
                        {Approved_Asn > 0 ? <Link to="/vendor/shipment/approvedAsn" onClick={(e) => closeMyDrop(e)}>Approved ASN</Link> : null}
                        {Cancelled_Asn > 0 ? <Link to="/vendor/shipment/cancelledAsn" onClick={(e) => closeMyDrop(e)}>Cancelled ASN</Link> : null}
                    </div> : null} */}
                </div>
            </label>
        );
    }
}

export default ShipmentDrop;