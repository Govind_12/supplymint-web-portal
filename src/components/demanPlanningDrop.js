import React from 'react';
import { Link } from 'react-router-dom';
import { openMyDrop, closeMyDrop } from '../helper/index';

class DemandPlanningDrop extends React.Component {

  render() {
    var Weekly = JSON.parse(sessionStorage.getItem('Weekly')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Weekly')).crud);
    var Monthly = JSON.parse(sessionStorage.getItem('Monthly')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Monthly')).crud);
    var Forecast_On_Demand = JSON.parse(sessionStorage.getItem('Forecast On Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Forecast On Demand')).crud);
    var Budgeted_Sales = JSON.parse(sessionStorage.getItem('Budgeted Sales')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Budgeted Sales')).crud);
    var Budgeted_Sales_History = JSON.parse(sessionStorage.getItem('Budgeted Sales History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Budgeted Sales History')).crud);
    var Weekly_History = JSON.parse(sessionStorage.getItem('Weekly History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Weekly History')).crud);
    var Monthly_History = JSON.parse(sessionStorage.getItem('Monthly History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Monthly History')).crud);
    var Forecast_History = JSON.parse(sessionStorage.getItem('Forecast History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Forecast History')).crud);

    return (
      <label>
        <div className="dropdown" onMouseOver={(e) => openMyDrop(e)} id="myFav">
          <button className="dropbtn home_link">Demand Planning
              <i className="fa fa-chevron-down"></i>
          </button>
          {/* <div className="displayNone adminBreacrumbsDropdown anchorError" id="dropMenu">
            {Forecast_On_Demand > 0 ? <Link to="/demandPlanning/forecastOnDemand" onClick={(e)=> closeMyDrop(e)} className="menuFavItem"> Forecast On Demand</Link> : null}
            <span className="scheduler">Forecast <span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>

              <div className="hovershowDropdown subDropPos">
                <Link to="/demandPlanning/forecastAutoConfiguration" onClick={(e)=> closeMyDrop(e)} className="menuFavItem">Auto Configuration</Link>
                {Forecast_On_Demand > 0 ? <Link to="/demandPlanning/forecastOnDemand" onClick={(e)=> closeMyDrop(e)} className="menuFavItem"> Forecast On Demand</Link> : null}
                {Forecast_History > 0 ? <Link to="/demandPlanning/forecastHistory" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Forecast History</Link> : null}

              </div>
            </span> */}



            {/* <span className="scheduler">Weekly <span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>
              <div className="hovershowDropdown subDropPos">
                {Weekly > 0 ? <Link to="/demandPlanning/weekly" onClick={(e) => closeMyDrop(e)} className="menuFavItem"> Insight</Link> : null}
                {Weekly_History > 0 ? <Link to="/demandPlanning/weeklyHistory" onClick={(e) => closeMyDrop(e)} className="menuFavItem">History</Link> : null}
              </div>
            </span> */}
            {/* <span className="scheduler">Monthly<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>

              <div className="hovershowDropdown subDropPos">
                {Monthly > 0 ? <Link to="/demandPlanning/monthly" onClick={(e) => closeMyDrop(e)} className="menuFavItem"> Insight</Link> : null}
                {Monthly_History > 0 ? <Link to="/demandPlanning/monthlyHistory" onClick={(e) => closeMyDrop(e)} className="menuFavItem">History</Link> : null}
              </div>
            </span> */}
            {/* <span className="scheduler">Budgeted Sales<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>

              <div className="hovershowDropdown subDropPos">
                {Budgeted_Sales > 0 ? <Link to="/demandPlanning/budgetedSales" onClick={(e) => closeMyDrop(e)} className="menuFavItem"> Upload Data</Link> : null}
                {Budgeted_Sales_History > 0 ? <Link to="/demandPlanning/budgetedSalesHistory" onClick={(e) => closeMyDrop(e)} className="menuFavItem">History</Link> : null}
              </div>
            </span> */}
            {/* <Link to="/demandPlanning/openToBuy" onClick={(e) => closeMyDrop(e)} className="menuFavItem"> Open To Buy</Link> */}
            {/* {Weekly > 0 ? <Link to="/demandPlanning/weekly" onClick={(e)=> closeMyDrop(e)} className="menuFavItem"> Weekly</Link> : null} */}
            {/* {Monthly > 0 ? <Link to="/demandPlanning/monthly" onClick={(e)=> closeMyDrop(e)} className="menuFavItem"> Monthly</Link> : null} */}
            {/* {Budgeted_Sales > 0 ?<Link to="/demandPlanning/budgetedSales" onClick={(e)=> closeMyDrop(e)} className="menuFavItem"> Budgeted Sales</Link> :null} */}
            {/* {Weekly_History > 0 ? <Link to="/demandPlanning/weeklyHistory" onClick={(e)=> closeMyDrop(e)} className="menuFavItem"> Weekly History</Link> : null} */}
            {/* {Monthly_History > 0 ? <Link to="/demandPlanning/monthlyHistory" onClick={(e)=> closeMyDrop(e)} className="menuFavItem"> Monthly History</Link> : null} */}
            {/* {Budgeted_Sales_History > 0 ? <Link to="/demandPlanning/budgetedSalesHistory" onClick={(e)=> closeMyDrop(e)} className="menuFavItem"> Budgeted Sales History</Link> : null} */}
          {/* </div> */}
        </div>
      </label>

    );
  }
}

export default DemandPlanningDrop;