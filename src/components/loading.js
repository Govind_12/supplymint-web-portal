import React from 'react';
import loaderIcon from "../assets/hourglass-2.svg";

class Loading extends React.Component{
    render(){
        return(
            
                <div className="circle-loader">
                    <div className="oval-shape">
                    </div>
                    <div className="loader-icon">
                        <img src={loaderIcon} /> 
                    </div>
                    <div className="loader-txt">
                        <p>Hold on</p>
                        <h2>Processing Your Request...</h2>
                    </div>
                </div>
            
            );
    }
}

export default Loading;