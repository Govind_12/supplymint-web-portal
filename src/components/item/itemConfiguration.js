import React from "react";
import openRack from "../../assets/open-rack.svg";


class ItemConfiguration extends React.Component {
  render() {
    return (
        <div className="container_div m-top-100" id="vendor_manage">
        <div className="col-md-12 col-sm-12 pad-0">
            <div className="menu_path">
                <ul className="list-inline width_100">
                    {/* <li>
                        <label className="home_link">Home > Administrator > Item Configuration </label>
                    </li> */}
                    <li className="float_right">
                        <img src={openRack} className="right_sidemenu" />
                    </li>
                </ul>
            </div>
        </div>

        <div className="col-md-12 col-sm-12 col-xs-12 m-top-10 pad-0">
            <div className="item_container container_content_my">
                <div className="my_border">

                </div>
                <div className="col-md-12 col-sm-12 pad-0">
                    <ul className="list_style">
                        <li>
                            <label className="contribution_mart">
                                ITEM CONFIGURATION
                            </label>
                        </li>
                        <li>
                            <p className="master_para">Manage application configuration and setting of items</p>
                        </li>
                    </ul>
                </div>



                <div className="col-md-12 col-sm-12 pad-0 m-top-50">
                    <ul className="list_style">
                        <li>
                            <label className="detail_label">
                                CONFIGURATION 1
                            </label>
                        </li>
                        <li>
                            <p className="master_para">Change Hierarchy labels for your application</p>
                        </li>
                    </ul>
                </div>

                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-10">
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="H1 Level Name" className="conf_input_box" />
                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="H2 Level Name" className="conf_input_box" />
                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="H3 Level Name" className="conf_input_box" />
                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="H4 Level Name" className="conf_input_box" />
                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="H5 Level Name" className="conf_input_box" />
                    </div>
                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="H6 Level Name" className="conf_input_box" />
                    </div>
                </div>



                <div className="col-md-12 col-sm-12 pad-0 m-top-50">
                    <ul className="list_style">
                        <li>
                            <label className="detail_label">
                                CONFIGURATION 2
                            </label>
                        </li>
                        <li>
                            <p className="master_para">Change Hierarchy labels for your application</p>
                        </li>
                    </ul>
                </div>

                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">


                    <div className="col-md-2 col-sm-2 pad-0">
                        <select className="conf2_input_box">

                            <option>Applicable Seasons</option>

                        </select>
                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <select className="conf2_input_box myspacing">
                            <option>Applicable Events</option>
                        </select>
                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <select className="conf2_input_box myspacing">
                            <option>Other Factors</option>
                        </select>
                    </div>

                </div>

              




                <div className="col-md-12 col-sm-12 pad-0 m-top-50">
                    <ul className="list_style">
                        <li>
                            <label className="detail_label">
                                CONFIGURATION 3
                            </label>
                        </li>
                        <li>
                            <p className="master_para">Change Configuration labels and options</p>
                        </li>
                    </ul>
                </div>



                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">


                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 1" className="conf_input_box" />

                    </div>

                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 2" className="conf_input_box" />

                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 3" className="conf_input_box" />

                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 4" className="conf_input_box" />

                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 5" className="conf_input_box" />

                    </div>

                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">

                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 6" className="conf_input_box" />

                    </div>

                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 7" className="conf_input_box" />

                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 8" className="conf_input_box" />

                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 9" className="conf_input_box" />

                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <input type="text" placeholder="UDF 10" className="conf_input_box" />

                    </div>




                </div>

                <div className="col-md-12 col-sm-12 m-bot-15 m-top-50">
                        <ul className="list-inline m-lft-0">
                          <li>
                            <button className="clearbtnOrganisation" type="reset">
                              Clear
                            </button>
                          </li>
                          <li>
                            <button className="savebtnOrganisation">Save</button>
                          </li>
                        </ul>
                      </div>

               
            </div>
        </div></div>

        )
    }
}

export default ItemConfiguration;