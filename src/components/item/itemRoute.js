import React from "react";
import openRack from "../../assets/open-rack.svg";

class itemRoute extends React.Component {
  render() {
    return (
        <div className="container_div m-top-100" id="vendor_manage">
        <div className="col-md-12 col-sm-12 pad-0">
            <div className="menu_path">
                <ul className="list-inline width_100">
                    {/* <li>
                        <label className="home_link">Home > Administrator > Item Configuration </label>
                    </li> */}
                    <li className="float_right">
                        <img src={openRack} className="right_sidemenu" />
                    </li>
                </ul>
            </div>
        </div>

        <div className="col-md-12 col-sm-12 col-xs-12 m-top-10 pad-0">
            <div className="organisation_container container_content_my">
                <div className="my_border">

                </div>
                <div className="col-md-12 col-sm-12 pad-0">
                    <ul className="list_style">
                        <li>
                            <label className="contribution_mart">
                                ITEM ROUTE
                            </label>
                        </li>
                        <li>
                            <p className="master_para">Manage Item Route for Enterprise</p>
                        </li>
                    </ul>
                </div>


                <div className="col-md-12 col-sm-12 pad-0 m-top-50">
                    <ul className="list_style">
                        <li>
                            <label className="contribution_mart">
                                AVAILABLE MAPPINGS <span className="head17">17</span>
                            </label>
                        </li>
                        <li>
                            <p className="master_para">Total Number of Available Mapping for selected item</p>
                        </li>
                    </ul>
                </div>

                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-10">


                    <div className="col-md-2 col-sm-2 pad-0">
                        <select className="conf2_input_box">
                            <option>Applicable Seasons</option>
                        </select>
                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <select className="conf2_input_box">
                            <option>Applicable Events</option>
                        </select>
                    </div>
                    <div className="col-md-2 col-sm-2 pad-0">
                        <select className="conf2_input_box">
                            <option>Other Factors</option>
                        </select>
                    </div>

                </div>

               


                <div className="col-md-12 col-sm-12 m-bot-15 m-top-250"> 
                        <ul className="list-inline m-lft-0">
                          <li>
                            <button className="clearbtnOrganisation" type="reset">
                              Clear
                            </button>
                          </li>
                          <li>
                            <button className="savebtnOrganisation">Save</button>
                          </li>
                        </ul>
                      </div>

                
            </div>
        </div>
        </div>

    )
  }
}

export default itemRoute;