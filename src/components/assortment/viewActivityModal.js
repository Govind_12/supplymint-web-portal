import React from 'react';
import ToastLoader from '../loaders/toastLoader';

class ViewActivityModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            filter: false,
            dataFilter: false,
            type: 1,
            no: 1,
            search: "",
            loader: true,
            status: "",
            itemCount: "",
            createdOn: "",
            alert: false,
            code: "",
            errorMessage: "",
            errorCode: "",
            toastMsg: "Enter text on search input",
            toastLoader: false
        };
    }

    componentWillMount() {
        let data = {
            type: 1,
            search: "",
            no: 1
        }
        this.props.getActivityRequest(data);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.inventoryManagement.getActivity.isSuccess) {
            this.setState({
                loader: false,
                prev: nextProps.inventoryManagement.getActivity.data.prePage,
                current: nextProps.inventoryManagement.getActivity.data.currPage,
                next: nextProps.inventoryManagement.getActivity.data.currPage + 1,
                maxPage: nextProps.inventoryManagement.getActivity.data.maxPage,
                tableData: nextProps.inventoryManagement.getActivity.data.resource == null ? [] : nextProps.inventoryManagement.getActivity.data.resource
            })

        }
        // else if (nextProps.inventoryManagement.getActivity.isError) {
        //     this.setState({
        //         loader: false,
        //         alert: true,
        //         code: nextProps.inventoryManagement.getActivity.message.status,
        //         errorCode: nextProps.inventoryManagement.getActivity.message.error == undefined ? undefined : nextProps.inventoryManagement.getActivity.message.error.errorCode,
        //         errorMessage: nextProps.inventoryManagement.getActivity.message.error == undefined ? undefined : nextProps.inventoryManagement.getActivity.message.error.errorMessage
        //     })
        // }

        // if (nextProps.inventoryManagement.getActivity.isLoading) {
        //     this.setState({
        //         loader: true
        //     })
        // }
    }


    handleChange(e) {
        if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            })
        } else if (e.target.id == "itemCount") {
            this.setState({
                itemCount: e.target.value
            })
        } else if (e.target.id == "createdOn") {
            e.target.placeholder = e.target.value;
            this.setState({
                createdOn: e.target.value
            })
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.inventoryManagement.getActivity.data.prePage,
                current: this.props.inventoryManagement.getActivity.data.currPage,
                next: this.props.inventoryManagement.getActivity.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getActivity.data.maxPage,
            })
            if (this.props.inventoryManagement.getActivity.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    search: this.state.search,
                    no: this.props.inventoryManagement.getActivity.data.currPage - 1,
                    itemCount: this.state.itemCount,
                    status: this.state.status,
                    createdOn: this.state.createdOn
                }
                this.props.getActivityRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.inventoryManagement.getActivity.data.prePage,
                current: this.props.inventoryManagement.getActivity.data.currPage,
                next: this.props.inventoryManagement.getActivity.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getActivity.data.maxPage,
            })
            if (this.props.inventoryManagement.getActivity.data.currPage != this.props.inventoryManagement.getActivity.data.maxPage) {
                let data = {
                    type: this.state.type,
                    search: this.state.search,
                    no: this.props.inventoryManagement.getActivity.data.currPage + 1,
                    itemCount: this.state.itemCount,
                    status: this.state.status,
                    createdOn: this.state.createdOn,
                }
                this.props.getActivityRequest(data)
            }
        } else if (e.target.id == "first") {
            if (this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.inventoryManagement.getActivity.data.prePage,
                    current: this.props.inventoryManagement.getActivity.data.currPage,
                    next: this.props.inventoryManagement.getActivity.data.currPage + 1,
                    maxPage: this.props.inventoryManagement.getActivity.data.maxPage,
                })
                if (this.props.inventoryManagement.getActivity.data.currPage <= this.props.inventoryManagement.getActivity.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        search: this.state.search,
                        no: 1,
                        itemCount: this.state.itemCount,
                        status: this.state.status,
                        createdOn: this.state.createdOn,
                    }
                    this.props.getActivityRequest(data);
                }

            }
        }
    }

    onClearSearch(e) {
        this.setState({
            search: "",
            type: 1,
            no: 1
        })
        let data = {
            type: 1,
            search: "",
            no: 1
        }
        this.props.getActivityRequest(data);
    }

    clearFilter(e) {
        // e.preventDefault();
        this.setState({
            status: "",
            itemCount: "",
            createdOn: ""
        })
        document.getElementById('createdOn').placeholder = "Created On";
    }

    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            let data = {
                search: this.state.search,
                type: 3,
                no: 1
            }
            this.props.getActivityRequest(data);
            this.setState({
                type: 3,
                no: 1,
                status: "",
                itemCount: "",
                createdOn: "",
            })
        }
    }


    applyFilter(e) {
        e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            serach: "",
            status: this.state.status,
            itemCount: this.state.itemCount,
            createdOn: this.state.createdOn
        }
        this.setState({
            no: 1,
            type: 2,
            search: ""
        })
        this.props.getActivityRequest(data);
    }
    truncate = (str, no_words) => {
        if (str.length <= 70) {
            return str;
        } else {
            let semi = str.split(" ").splice(4, no_words).join(" ");
            return `${semi}...`
        }
    }

    small = (str) => {
        if (str.length <= 70) {
            return false;
        }
        return true;
    }

    truncateAssort = (str, no_words) => {
        if (str.length < 30) {
            return str;
        } else {
            let semi = str.split(" ").splice(0, no_words).join("");
            return `${semi}...`
        }
    }
    smallAssort = (str) => {
        if (str.length < 30) {
            return false;
        }
        return true;
    }

    onClearFilter(e) {
        this.setState({
            type: 1,
            no: 1,
            search: "",
            status: "",
            itemCount: "",
            createdOn: "",
        })
        document.getElementById('createdOn').placeholder = "Created On";
        let data = {
            type: 1,
            no: 1,
            search: "",
        }
        this.props.getActivityRequest(data);
    }

    render() {
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>

                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal viewActivityModal">

                        <div className="col-md-12 col-sm-12 pad-0">

                            <ul className="list_style m0">
                                <li><label className="contribution_mart">View Activity</label>

                                </li>

                            </ul>
                        </div>
                        <div className="col-md-12 pad-0">
                            <form onSubmit={(e) => this.applyFilter(e)}>
                                <ul className="pad-0">
                                    <li className="displayInline">


                                        <ul className="list-inline">
                                            <label className="filter_modal displayBlock pad-lft-5 textLeft">FILTERS</label>
                                            <li>
                                                <input type="text" onChange={(e) => this.handleChange(e)} id="itemCount" value={this.state.itemCount} placeholder="Item count" className="organistionFilterModal" />                                    </li>

                                            <li>
                                                <input type="date" placeholder="Created On" id="createdOn" onChange={(e) => this.handleChange(e)} value={this.state.createdOn} className="organistionFilterModal" />
                                            </li>


                                            <li>
                                                <select id="status" onChange={(e) => this.handleChange(e)} value={this.state.status} className="organistionFilterModal">
                                                    <option value="">
                                                        Status
                                                    </option>
                                                    <option value="SUCCEEDED">SUCCEEDED</option>
                                                    <option value="FAILED">FAILED</option>
                                                </select></li>
                                        </ul>

                                    </li>
                                    <li className="displayInline posRelative verticalBot">
                                        {this.state.status != "" || this.state.itemCount != "" || this.state.createdOn != "" ? <button type="submit" className="modal_Apply_btn m-top-40">
                                            APPLY
                                </button> : <button type="submit" className="modal_Apply_btn m-top-40 btnDisabled" disabled>
                                                APPLY
                                </button>}
                                        {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}

                                    </li>
                                </ul>
                            </form>
                        </div>

                        <div className="col-md-12 pad-0">
                            <div className="col-md-6"></div>
                            <div className="col-md-6 pad-0">
                                <ul className="list-inline manageSearch float_Right">
                                    <li>
                                        <form onSubmit={(e) => this.onSearch(e)}>
                                            <input type="text" placeholder="Type to Search..." onChange={(e) => this.setState({ search: e.target.value })} value={this.state.search} className="search_bar" />
                                            {/* <img src="../imgs/search.svg" className="search_img" /> */}
                                            <button type="submit" className="searchWithBar">Search
                                        <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                    <path fill="#ffffff" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                </svg>
                                            </button>
                                        </form>
                                        {this.state.type != 3 ? null : <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span>}

                                    </li>

                                </ul>

                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="adHocModalTable viewActivityTable m-top-10">
                                <table>
                                    <thead>

                                        <th>
                                            <label>Assortment Hierarchy</label>
                                        </th>
                                        <th>
                                            <label>Pattern</label>
                                        </th>
                                        <th>
                                            <label>Item Count</label>
                                        </th>
                                        <th>
                                            <label>Created On</label>
                                        </th>
                                        <th>
                                            <label>Status</label>
                                        </th>

                                    </thead>

                                    <tbody>
                                        {/* {this.state.adhocModalData.map((data,key) => (  */}
                                        {this.state.tableData.length == 0 ? <tr className="modalTableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.tableData.map((data, key) => (<tr key={key}>
                                            <td>
                                                <div className="topToolTip toolTipSupplier limitTextToolTip toolTipZindex">

                                                    <label >
                                                        {/* {this.truncate(`${data.hl1Name == "NA" ? "" : ` Division : ${data.hl1Name} `}${data.hl2Name == "NA" ? "" : `, Department : ${data.hl2Name}`}${data.hl3Name == "NA" ? "" : `, Section : ${data.hl3Name} `}${data.hl4Name == "NA" ? "" : `, Article : ${data.hl4Name}`}`, 5)}
                                                        {this.small(`${data.hl1Name == "NA" ? "" : ` Division : ${data.hl1Name}, `}${data.hl2Name == "NA" ? "" : ` Department : ${data.hl2Name}, `}${data.hl3Name == "NA" ? "" : ` Section : ${data.hl3Name}, `}${data.hl4Name == "NA" ? "" : ` Article : ${data.hl4Name}, `}`) ? <span className="topToolTipText" id="hideToolTip">{data.hl1Name == "NA" ? "" : ` Division : ${data.hl1Name}, `}{data.hl2Name == "NA" ? "" : ` Department : ${data.hl2Name}, `}{data.hl3Name == "NA" ? "" : ` Section : ${data.hl3Name}, `}{data.hl4Name == "NA" ? "" : ` Article : ${data.hl4Name}, `}</span> : null} */}
                                                        {this.truncateAssort(`${data.hierarchyPattern}`, 1)}
                                                        {this.smallAssort(`${data.hierarchyPattern}`) ? <span className="topToolTipText" id="hideToolTip">{data.hierarchyPattern}</span> : null}
                                                        {/* {data.hierarchyPattern} */}
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="topToolTip toolTipSupplier limitTextToolTip toolTipZindex">
                                                    <label>
                                                        {/* {this.truncate(`${data.pattern}`, 4)} */}
                                                        {this.truncateAssort(`${data.pattern}`, 3)}
                                                    </label>
                                                    {this.smallAssort(`${data.pattern}`) ? <span className="topToolTipText" id="hideToolTip">{data.pattern}</span> : null}

                                                </div>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.itemCount}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.createdOn}
                                                </label>
                                            </td>
                                            <td>
                                                <label>{data.status}</label>
                                            </td>

                                        </tr>))}
                                        {/* ))} */}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="viewActivityFoooter posRelative displayInline width100">
                            <div className="pagerDiv ">
                                <ul className="list-inline pagination">
                                    {this.state.current == 1 ? <li >
                                        <button className="PageFirstBtn pointerNone" >
                                            First
                                        </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="first" >
                                                First
                                             </button>
                                        </li>}
                                    {this.state.prev != 0 ? <li >
                                        <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                            Prev
                                        </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn" disabled>
                                                Prev
                                            </button>
                                        </li>}
                                    <li>
                                        <button className="PageFirstBtn pointerNone">
                                            <span>{this.state.current}/{this.state.maxPage}</span>
                                        </button>
                                    </li>
                                    {this.state.next - 1 != this.state.maxPage ? <li >
                                        <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                            Next
                                         </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn borderNone" disabled>
                                                Next
                                            </button>
                                        </li>}
                                </ul>
                            </div>


                            <div className="adHocBottom">
                                <button className="m0" type="button" onClick={(e) => this.props.activitySh(e)}>CLOSE</button>
                            </div>
                        </div>

                    </div>


                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                </div>
            </div>
        )
    }
}

export default ViewActivityModal;