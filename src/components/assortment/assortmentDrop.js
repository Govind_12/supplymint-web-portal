import React from 'react';

export const AssortmentDrop = (props) => {
    return <div className="dropdown searchStoreProfileMain m-top-5">
        <input type="search" placeholder="Search..." />
        <ul className="pad-0 m-top-10">
            {props.data.length == 0 ? null : props.data.map((data, key) => (<li key={key} value={data} id={props.id} onClick={(e) => props.onChange(e,data)}>{data}</li>))}
        </ul>
    </div>
}