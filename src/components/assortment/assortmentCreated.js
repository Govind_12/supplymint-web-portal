import React from 'react';
class AssortmentCreated extends React.Component {
    
    render() {
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>

                <div className=" display_block assortmentCreatedModal">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain createAssortmentModal ">

                        <div className="col-md-12 col-sm-12 pad-0">

                            <ul className="list_style m0">
                                <li><label className="contribution_mart">New Assortment Created !</label>

                                </li>
                                <li>
                                    {/* <label className="m-top-5">You need to wait for 5 minustes before you run your forecast !</label> */}
                                </li>

                            </ul>
                        </div>
                        {/* <div className="emptyDiv"></div> */}
                        <div className="col-md-12 proceedOrNot pad-lft-0">
                            <p>You need to wait for 5 minustes before you run your forecast !</p>
                            <div className="saveCloseAssortBtn m-top-30">
                                <button onClick={(e) => this.props.createModal(e)} type="buttton">OK</button>
                                
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        )
    }
}

export default AssortmentCreated;