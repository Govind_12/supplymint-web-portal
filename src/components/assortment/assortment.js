
import React, { forwardRef } from 'react';
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ViewActivityModal from './viewActivityModal';
import CreateAssortConfirmModal from './createAssortConfirmModal';
import AssortmentCreated from './assortmentCreated';
import RejectIcon from '../../assets/reject.svg';
import CloseStatus from '../../assets/group-4.svg';
import SuccessIcon from '../../assets/success-check.svg';
import AssortRunning from '../../assets/live-job.svg';
import { AssortmentDrop } from './assortmentDrop';
import ToastLoader from '../loaders/toastLoader';
import { DefaultAssortment, PriorityModal } from './prioritySetModal';
import AssortmentFilterModal from './assortmentFilterModal';
import Hl2Modal from './hl2Modal';
import Hl3Modal from './hl3Modal';
import Hl4Modal from './hl4Modal';
import _ from 'lodash';

class Assortment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            hlName:"",
            prioritySetData: {},
            hlField: "",
            openRightBar: false,
            rightbar: false,
            success: false,
            alert: false,
            loader: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            successMessage: "",
            h1: "",
            h2: "",
            h3: "",
            h4: "",
            h5: "",
            h6: "",
            hl1: [],
            hl2: [],
            hl3: [],
            hl4: [],
            itemName: false,
            brandName: false,
            color: false,
            brandSize: false,
            brandPatter: false,
            brandMaterial: false,
            brandDesign: false,
            barCode: false,
            mrp: false,
            // mrp: false,
            h1Checked: false,
            h2Checked: false,
            h3Checked: false,
            h4Checked: false,
            h5Checked: false,
            h6Checked: false,
            count: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            search: "",
            type: 1,
            no: 1,
            assortmentData: [],
            colors: false,
            materials: false,
            sizes: false,
            brands: false,
            designs: false,
            patterns: false,
            activity: false,
            popup: false,
            defaultassortment: "",
            assortmentCreated: false,
            status: "",
            statusMessage: "",
            hl1drop: false,
            hl2drop: false,
            hl3drop: false,
            hl4drop: false,
            toastLoader: false,
            toastMsg: "",
            prioritySet: false,
            assortmentDropdown: {},
            hl1Modal: false,
            hl4Modal: false,
            hl2Modal: false,
            hl3Modal: false,
            hl1ModalAnimation: false,
            hl2ModalAnimation: false,
            hl3ModalAnimation: false,
            hl4ModalAnimation: false,
            hl1Name: "",
            hl2Name: "",
            hl3Name: "",
            dropdownObject: {},
            setCount: [],
            assortmentcode: [],
        
        }
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }
    componentWillMount() {
    
        // this.props.hl1Request();
        let data = {
            no: 1,
            type: 1,
            search: ""
        }

        this.props.getAssortmentStatusRequest();
        this.props.getAssortmentRequest(data);
        this.props.getAssortmentDropdownRequest()
    }
    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);

    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
        this.props.assetCodeRequest();

        clearInterval(this.statusInterval);

    }
    componentWillReceiveProps(nextProps) {
    
        if (nextProps.inventoryManagement.getAssortmentStatus.isSuccess) {
            
            if (nextProps.inventoryManagement.getAssortmentStatus.data.resource.status == 'INPROGRESS') {
                this.setState({
                    status: nextProps.inventoryManagement.getAssortmentStatus.data.resource.status,
                    statusMessage: nextProps.inventoryManagement.getAssortmentStatus.data.resource.message
                })
                setTimeout(() => {
                    this.props.getAssortmentStatusRequest()
                }, 10000)

            } else {
                this.setState({
                    status: nextProps.inventoryManagement.getAssortmentStatus.data.resource.status,
                    statusMessage: nextProps.inventoryManagement.getAssortmentStatus.data.resource.message
                })
            }
        }
        if (nextProps.inventoryManagement.getAssortmentDropdown.isSuccess) {
            
            let assortmentDropdown = nextProps.inventoryManagement.getAssortmentDropdown.data.resource.hlevel
            this.setState({
                assortmentDropdown: nextProps.inventoryManagement.getAssortmentDropdown.data.resource.hlevel
            })
            let dropdownObject = this.state.dropdownObject
            dropdownObject.HL1NAME = Object.values(assortmentDropdown.HL1NAME).join()
            dropdownObject.HL2NAME = Object.values(assortmentDropdown.HL2NAME).join()
            dropdownObject.HL3NAME = Object.values(assortmentDropdown.HL3NAME).join()
            dropdownObject.HL4NAME = Object.values(assortmentDropdown.HL4NAME).join()
            dropdownObject.HL5NAME = Object.values(assortmentDropdown.HL5NAME).join()
            dropdownObject.HL6NAME = Object.values(assortmentDropdown.HL6NAME).join()

            this.setState({
                dropdownObject: dropdownObject

            })
            this.props.getAssortmentDropdownClear();
        } else if (nextProps.inventoryManagement.getAssortmentDropdown.isError) {
         
            
            this.setState({
                alert: true,
                loader: false,
                success: false,
                code: nextProps.inventoryManagement.getAssortmentDropdown.message.status,
                errorCode: nextProps.inventoryManagement.getAssortmentDropdown.message.error == undefined ? undefined : nextProps.inventoryManagement.getAssortmentDropdown.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.getAssortmentDropdown.message.error == undefined ? undefined : nextProps.inventoryManagement.getAssortmentDropdown.message.error.errorMessage
            })
        }


        if (nextProps.inventoryManagement.getAssortment.isSuccess) {
          
            this.setState({
                loader: false,
                assortmentData: nextProps.inventoryManagement.getAssortment.data.resource,
                prev: nextProps.inventoryManagement.getAssortment.data.prePage,
                current: nextProps.inventoryManagement.getAssortment.data.currPage,
                next: nextProps.inventoryManagement.getAssortment.data.currPage + 1,
                maxPage: nextProps.inventoryManagement.getAssortment.data.maxPage,
            })
        }else if(nextProps.inventoryManagement.getAssortment.isError){
         
            this.setState({
                alert: true,
                loader: false,
                success: false,
                code: nextProps.inventoryManagement.getAssortment.message.status,
                errorCode: nextProps.inventoryManagement.getAssortment.message.error == undefined ? undefined : nextProps.inventoryManagement.getAssortment.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.getAssortment.message.error == undefined ? undefined : nextProps.inventoryManagement.getAssortment.message.error.errorMessage
            })   
        }

        if (nextProps.inventoryManagement.hl1.isSuccess) {
            this.setState({
                loader: false,
                hl1: nextProps.inventoryManagement.hl1.data.resource == null ? [] : nextProps.inventoryManagement.hl1.data.resource

            })
        } else if (nextProps.inventoryManagement.hl1.isError) {
            this.setState({
                alert: true,
                loader: false,
                success: false,
                code: nextProps.inventoryManagement.hl1.message.status,
                errorCode: nextProps.inventoryManagement.hl1.message.error == undefined ? undefined : nextProps.inventoryManagement.hl1.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.hl1.message.error == undefined ? undefined : nextProps.inventoryManagement.hl1.message.error.errorMessage
            })
        }

        if (nextProps.inventoryManagement.hl2.isSuccess) {
            this.setState({
                loader: false,
                hl2: nextProps.inventoryManagement.hl2.data.resource == null ? [] : nextProps.inventoryManagement.hl2.data.resource
            })
        } else if (nextProps.inventoryManagement.hl2.isError) {
            this.setState({
                alert: true,
                loader: false,
                success: false,
                code: nextProps.inventoryManagement.hl2.message.status,
                errorCode: nextProps.inventoryManagement.hl2.message.error == undefined ? undefined : nextProps.inventoryManagement.hl2.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.hl2.message.error == undefined ? undefined : nextProps.inventoryManagement.hl2.message.error.errorMessage
            })
        }

        if (nextProps.inventoryManagement.hl3.isSuccess) {
            this.setState({
                loader: false,
                hl3: nextProps.inventoryManagement.hl3.data.resource == null ? [] : nextProps.inventoryManagement.hl3.data.resource
            })
        } else if (nextProps.inventoryManagement.hl3.isError) {
            this.setState({
                alert: true,
                loader: false,
                success: false,
                code: nextProps.inventoryManagement.hl3.message.status,
                errorCode: nextProps.inventoryManagement.hl3.message.error == undefined ? undefined : nextProps.inventoryManagement.hl3.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.hl3.message.error == undefined ? undefined : nextProps.inventoryManagement.hl3.message.error.errorMessage
            })
        }

        if (nextProps.inventoryManagement.hl4.isSuccess) {

            this.setState({
                loader: false,
                hl4: nextProps.inventoryManagement.hl4.data.resource == null ? [] : nextProps.inventoryManagement.hl4.data.resource
            })
        } else if (nextProps.inventoryManagement.hl4.isError) {
            this.setState({
                alert: true,
                loader: false,
                success: false,
                code: nextProps.inventoryManagement.hl4.message.status,
                errorCode: nextProps.inventoryManagement.hl4.message.error == undefined ? undefined : nextProps.inventoryManagement.hl4.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.hl4.message.error == undefined ? undefined : nextProps.inventoryManagement.hl4.message.error.errorMessage
            })
        }
        if (nextProps.inventoryManagement.assetCode.isSuccess) {
            this.setState({
                loader: false,
                count: nextProps.inventoryManagement.assetCode.data.resource == null ? "" : nextProps.inventoryManagement.assetCode.data.resource.itemCount
            });
            this.props.assetCodeRequest();
        } else if (nextProps.inventoryManagement.assetCode.isError) {
            this.setState({
                loader: false,
                code: nextProps.inventoryManagement.assetCode.message.status,
                errorCode: nextProps.inventoryManagement.assetCode.message.error == undefined ? undefined : nextProps.inventoryManagement.assetCode.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.assetCode.message.error == undefined ? undefined : nextProps.inventoryManagement.assetCode.message.error.errorMessage
            });
            this.props.assetCodeRequest();
        }
        if (nextProps.inventoryManagement.createAssortment.isSuccess) {
            if (nextProps.inventoryManagement.createAssortment.data.resource.popup == "true") {
                this.setState({
                    popup: true,
                    defaultassortment: nextProps.inventoryManagement.createAssortment.data.resource.defaultAssortment
                })
                this.props.createAssortmentRequest();
            } else {
                this.setState({
                    assortmentCreated: true,
                    // success: true,
                    loader: false,
                    // successMessage: nextProps.inventoryManagement.createAssortment.data.message,
                    h1: "",
                    h2: "",
                    h3: "",
                    h4: "",
                    h5: "",
                    h6: "",
                    hl2: [],
                    hl3: [],
                    hl4: [],


                    itemName: false,
                    brandName: false,
                    color: false,
                    brandSize: false,
                    brandPatter: false,
                    brandMaterial: false,
                    brandDesign: false,
                    barCode: false,
                    mrp: false,
                    count: "",
                    // mrp: false,
                    h1Checked: false,
                    h2Checked: false,
                    h3Checked: false,
                    h4Checked: false,
                    h5Checked: false,
                    h6Checked: false,


                })
                this.props.createAssortmentRequest();
            }
            this.props.getAssortmentStatusRequest()
        } else if (nextProps.inventoryManagement.createAssortment.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.inventoryManagement.createAssortment.message.status,
                errorCode: nextProps.inventoryManagement.createAssortment.message.error == undefined ? undefined : nextProps.inventoryManagement.createAssortment.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.createAssortment.message.error == undefined ? undefined : nextProps.inventoryManagement.createAssortment.message.error.errorMessage
            })
            this.props.createAssortmentRequest();
        }

        if (nextProps.inventoryManagement.getActivity.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.inventoryManagement.getActivity.message.status,
                errorCode: nextProps.inventoryManagement.getActivity.message.error == undefined ? undefined : nextProps.inventoryManagement.getActivity.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.getActivity.message.error == undefined ? undefined : nextProps.inventoryManagement.getActivity.message.error.errorMessage
            })
        } else if (nextProps.inventoryManagement.getActivity.isSuccess) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.inventoryManagement.getActivity.isLoading || nextProps.inventoryManagement.getAssortmentDropdown.isLoading || nextProps.inventoryManagement.getAssortment.isLoading
            || nextProps.inventoryManagement.hl1.isLoading || nextProps.inventoryManagement.hl2.isLoading
            || nextProps.inventoryManagement.hl3.isLoading || nextProps.inventoryManagement.hl4.isLoading
            || nextProps.inventoryManagement.assetCode.isLoading || nextProps.inventoryManagement.createAssortment.isLoading) {
            this.setState({
                loader: true
            })
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }

    createModal(e) {
        this.setState({
            assortmentCreated: !this.state.assortmentCreated
        })
    }

    onChange(e, dataa) {
        let value = dataa
        this.setState({
            hl1drop: false,
            hl2drop: false,
            hl3drop: false,
            hl4drop: false,
        })
        let t = e.target;
        if (t.id == "hl1") {
            this.setState({
                h1: value,
                h2: "",
                h3: "",
                h4: "",
                hl2: [],
                hl3: [],
                hl4: [],
                h2Checked: false,
                h3Checked: false,
                h4Checked: false
            })
            this.props.hl2Request(value);
            this.props.hl3Request();
            this.props.hl4Request();
        } else if (t.id == "hl2") {
            this.setState({
                h2: value,
                h3: "",
                h4: "",
                hl3: [],
                hl4: [],
                h3Checked: false,
                h4Checked: false
            })
            let data = {
                hl1: this.state.h1,
                hl2: value
            }
            this.props.hl3Request(data);
            this.props.hl4Request();
        } else if (t.id == "hl3") {
            this.setState({
                h3: value,
                h4: "",
                hl4: [],
            })
            let data = {
                hl1: this.state.h1,
                hl2: this.state.h2,
                hl3: value,
                h4Checked: false
            }
            this.props.hl4Request(data);
        } else if (t.id == "hl4") {
            this.setState({
                h4: value,
            })
        }
    }

    onCheck(e) {
        let t = e.target;
        if (t.id == "color") {
            this.setState({
                color: !this.state.color
            })
        }
        else if (t.id == "barCode") {
            this.setState({
                barCode: !this.state.barCode
            })
        }
        else if (t.id == "mrp") {
            this.setState({
                mrp: !this.state.mrp
            })
        }
        else if (t.id == "brandDesign") {
            this.setState({
                brandDesign: !this.state.brandDesign
            })
        } else if (t.id == "brandMaterial") {
            this.setState({
                brandMaterial: !this.state.brandMaterial
            })
        } else if (t.id == "brandName") {
            this.setState({
                brandName: !this.state.brandName
            })
        } else if (t.id == "brandPatter") {
            this.setState({
                brandPatter: !this.state.brandPatter
            })
        } else if (t.id == "brandSize") {
            this.setState({
                brandSize: !this.state.brandSize
            })
        } else if (t.id == "itemName") {
            this.setState({
                itemName: !this.state.itemName
            })
        } else if (t.id == "h1Checked") {
            this.setState({
                h1Checked: !this.state.h1Checked
            })
        } else if (t.id == "h2Checked") {
            this.setState({
                h2Checked: !this.state.h2Checked
            })
        } else if (t.id == "h3Checked") {
            this.setState({
                h3Checked: !this.state.h3Checked
            })
        } else if (t.id == "h4Checked") {
            this.setState({
                h4Checked: !this.state.h4Checked
            })
        } else if (t.id == "brands") {
            this.setState({
                brands: !this.state.brands
            })
        } else if (t.id == "colors") {
            this.setState({
                colors: !this.state.colors
            })
        } else if (t.id == "sizes") {
            this.setState({
                sizes: !this.state.sizes
            })
        } else if (t.id == "materials") {
            this.setState({
                materials: !this.state.materials
            })
        } else if (t.id == "patterns") {
            this.setState({
                patterns: !this.state.patterns
            })
        } else if (t.id == "designs") {
            this.setState({
                designs: !this.state.designs
            })
        }
    }

    onGetDetail(e) {

        if (this.state.dropdownObject == {}) {


            const { h1, h2, h3, h4, itemName, brandDesign, brandMaterial, brandName, brandPatter, brandSize, color, h1Checked, h2Checked, h3Checked, h4Checked, barCode, mrp } = this.state;
            let data = {
                hl1Name: "",
                hl2Name: "",
                hl3Name: "",
                hl4Name: "",
                hl5Name: "",
                hl6Name: "",
                itemName: itemName,
                brand: brandName,
                color: color,
                size: brandSize,
                pattern: brandPatter,
                material: brandMaterial,
                design: brandDesign,
                barCode: barCode,
                sell_Price: mrp
            }
            this.props.assetCodeRequest(data)
        } else {
            this.createAssortmentPayload('getDetails')




        }
    }

    createAssortmentPayload(data) {
        let dropdownObject = this.state.dropdownObject
        let hl1Name = ""
        let hl2Name = ""
        let hl3Name = ""
        let hl4Name = ""
        let hl5Name = ""
        let hl6Name = ""
        let h1CheckedVar = false
        let h2CheckedVar = false
        let h3CheckedVar = false
        let h4CheckedVar = false
        let h5CheckedVar = false
        let h6CheckedVar = false

        if (dropdownObject.HL1NAME != "") {

            if (dropdownObject.HL1NAME == "HL1NAME") {
                hl1Name = this.state.h1,
                    h1CheckedVar = this.state.h1Checked

            } else if (dropdownObject.HL1NAME == "HL2NAME") {
                hl2Name = this.state.h1
                h2CheckedVar = this.state.h1Checked

            } else if (dropdownObject.HL1NAME == "HL3NAME") {
                hl3Name = this.state.h1
                h3CheckedVar = this.state.h1Checked

            } else if (dropdownObject.HL1NAME == "HL4NAME") {
                hl4Name = this.state.h1
                h4CheckedVar = this.state.h1Checked

            } else if (dropdownObject.HL1NAME == "HL5NAME") {
                hl5Name = this.state.h1
                h5CheckedVar = this.state.h1Checked

            } else if (dropdownObject.HL1NAME == "HL6NAME") {
                hl6Name = this.state.h1
                h6CheckedVar = this.state.h1Checked

            }
        }
        if (dropdownObject.HL2NAME != "") {
            if (dropdownObject.HL2NAME == "HL1NAME") {
                hl1Name = this.state.h2
                h1CheckedVar = this.state.h2Checked
            } else if (dropdownObject.HL2NAME == "HL2NAME") {
                hl2Name = this.state.h2
                h2CheckedVar = this.state.h2Checked
            } else if (dropdownObject.HL2NAME == "HL3NAME") {
                hl3Name = this.state.h2
                h3CheckedVar = this.state.h2Checked
            } else if (dropdownObject.HL2NAME == "HL4NAME") {
                hl4Name = this.state.h2
                h4CheckedVar = this.state.h2Checked
            } else if (dropdownObject.HL2NAME == "HL5NAME") {
                hl5Name = this.state.h2
                h5CheckedVar = this.state.h2Checked
            } else if (dropdownObject.HL2NAME == "HL6NAME") {
                hl6Name = this.state.h2
                h6CheckedVar = this.state.h2Checked
            }
        }
        if (dropdownObject.HL3NAME != "") {
            if (dropdownObject.HL3NAME == "HL1NAME") {
                hl1Name = this.state.h3
                h1CheckedVar = this.state.h3Checked
            } else if (dropdownObject.HL3NAME == "HL2NAME") {
                hl2Name = this.state.h3
                h2CheckedVar = this.state.h3Checked
            } else if (dropdownObject.HL3NAME == "HL3NAME") {
                hl3Name = this.state.h3
                h3CheckedVar = this.state.h3Checked
            } else if (dropdownObject.HL3NAME == "HL4NAME") {
                hl4Name = this.state.h3
                h4CheckedVar = this.state.h3Checked
            } else if (dropdownObject.HL3NAME == "HL5NAME") {
                hl5Name = this.state.h3
                h5CheckedVar = this.state.h3Checked
            } else if (dropdownObject.HL3NAME == "HL6NAME") {
                hl6Name = this.state.h3
                h6CheckedVar = this.state.h3Checked
            }
        }
        if (dropdownObject.HL4NAME != "") {
            if (dropdownObject.HL4NAME == "HL1NAME") {
                hl1Name = this.state.h4
                h1CheckedVar = this.state.h4Checked
            } else if (dropdownObject.HL4NAME == "HL2NAME") {
                hl2Name = this.state.h4
                h2CheckedVar = this.state.h4Checked
            } else if (dropdownObject.HL4NAME == "HL3NAME") {
                hl3Name = this.state.h4
                h3CheckedVar = this.state.h4Checked
            } else if (dropdownObject.HL4NAME == "HL4NAME") {
                hl4Name = this.state.h4
                h4CheckedVar = this.state.h4Checked
            } else if (dropdownObject.HL4NAME == "HL5NAME") {
                hl5Name = this.state.h4
                h5CheckedVar = this.state.h4Checked
            } else if (dropdownObject.HL4NAME == "HL6NAME") {
                hl6Name = this.state.h4
                h6CheckedVar = this.state.h4Checked
            }
        }
        if (dropdownObject.HL5NAME != "") {
            if (dropdownObject.HL5NAME == "HL1NAME") {
                hl1Name = this.state.h5
                h1CheckedVar = this.state.h5Checked
            } else if (dropdownObject.HL5NAME == "HL2NAME") {
                hl2Name = this.state.h5
                h2CheckedVar = this.state.h5Checked
            } else if (dropdownObject.HL5NAME == "HL3NAME") {
                hl3Name = this.state.h5
                h3CheckedVar = this.state.h5Checked
            } else if (dropdownObject.HL5NAME == "HL4NAME") {
                hl4Name = this.state.h5
                h4CheckedVar = this.state.h5Checked
            } else if (dropdownObject.HL5NAME == "HL5NAME") {
                hl5Name = this.state.h5
                h5CheckedVar = this.state.h5Checked
            } else if (dropdownObject.HL5NAME == "HL6NAME") {
                hl6Name = this.state.h5
                h6CheckedVar = this.state.h5Checked
            }
        }
        if (dropdownObject.HL6NAME != "") {
            if (dropdownObject.HL6NAME == "HL1NAME") {
                hl1Name = this.state.h6
                h1CheckedVar = this.state.h6Checked
            } else if (dropdownObject.HL6NAME == "HL2NAME") {
                hl2Name = this.state.h6
                h2CheckedVar = this.state.h6Checked
            } else if (dropdownObject.HL6NAME == "HL3NAME") {
                hl3Name = this.state.h6
                h3CheckedVar = this.state.h6Checked
            } else if (dropdownObject.HL6NAME == "HL4NAME") {
                hl4Name = this.state.h6
                h4CheckedVar = this.state.h6Checked
            } else if (dropdownObject.HL6NAME == "HL5NAME") {
                hl5Name = this.state.h6
                h5CheckedVar = this.state.h6Checked
            } else if (dropdownObject.HL6NAME == "HL6NAME") {
                hl6Name = this.state.h6
                h6CheckedVar = this.state.h6Checked
            }
        }

        setTimeout(() => {
            if (data == "getDetails") {
                const { h1, h2, h3, h4, itemName, brandDesign, brandMaterial, brandName, brandPatter, brandSize, color, h1Checked, h2Checked, h3Checked, h4Checked, barCode, mrp } = this.state;

                let payload = {
                    hl1Name: hl1Name,
                    hl2Name: hl2Name,
                    hl3Name: hl3Name,
                    hl4Name: hl4Name,
                    hl5Name: hl5Name,
                    hl6Name: hl6Name,
                    itemName: itemName,
                    brand: brandName,
                    color: color,
                    size: brandSize,
                    pattern: brandPatter,
                    material: brandMaterial,
                    design: brandDesign,
                    barCode: barCode,
                    sell_Price: mrp
                }
                this.props.assetCodeRequest(payload)
            } else if (data == "openClosePopup") {
                const { itemName, brandDesign, brandMaterial, brandName, brandPatter, brandSize, color, barCode, mrp } = this.state;

                let payload = {
                    hl1Name: h1CheckedVar ? hl1Name : "NA",
                    hl2Name: h2CheckedVar ? hl2Name : "NA",
                    hl3Name: h3CheckedVar ? hl3Name : "NA",
                    hl4Name: h4CheckedVar ? hl4Name : "NA",
                    hl5Name: h5CheckedVar ? hl5Name : "NA",
                    hl6Name: h6CheckedVar ? hl6Name : "NA",
                    itemName: itemName,
                    brand: brandName,
                    color: color,
                    size: brandSize,
                    pattern: brandPatter,
                    material: brandMaterial,
                    design: brandDesign,
                    barCode: barCode,
                    sell_Price: mrp,
                    configureAssortment: false,
                    assortmentcode: []

                }
                this.props.createAssortmentRequest(payload);
            } else if (data == "PrioritySet") {
                const { h1, h2, h3, h4, itemName, brandDesign, brandMaterial, brandName, brandPatter, brandSize, color, h1Checked, h2Checked, h3Checked, h4Checked, h5Checked, h6Checked, barCode, mrp } = this.state;
                if (h1Checked || h2Checked || h3Checked || h4Checked || itemName || brandName || color || brandSize || brandPatter || brandMaterial || brandDesign || barCode || mrp) {
                    let assortmentDropdown = this.state.assortmentDropdown
                    let hl1NameShow = ""
                    let hl2NameShow = ""
                    let hl3NameShow = ""
                    let hl4NameShow = ""
                    let hl5NameShow = ""
                    let hl6NameShow = ""

                    Object.keys(assortmentDropdown).forEach((data) => {

                        if (Object.values(assortmentDropdown[data]) == "HL1NAME") {
                            hl1NameShow = Object.keys(assortmentDropdown[data]).join()

                        } else if (Object.values(assortmentDropdown[data]) == "HL2NAME") {
                            hl2NameShow = Object.keys(assortmentDropdown[data]).join()
                        } else if (Object.values(assortmentDropdown[data]) == "HL3NAME") {
                            hl3NameShow = Object.keys(assortmentDropdown[data]).join()
                        }
                        else if (Object.values(assortmentDropdown[data]) == "HL4NAME") {
                            hl4NameShow = Object.keys(assortmentDropdown[data]).join()
                        }
                        else if (Object.values(assortmentDropdown[data]) == "HL5NAME") {
                            hl5NameShow = Object.keys(assortmentDropdown[data]).join()
                        }
                        else if (Object.values(assortmentDropdown[data]) == "HL6NAME") {
                            hl6NameShow = Object.keys(assortmentDropdown[data]).join()
                        }
                    })



                    let sdata = {
                        hl1Name: h1CheckedVar ? hl1NameShow + "-" + hl1Name : false,
                        hl2Name: h2CheckedVar ? hl2NameShow + "-" + hl2Name : false,
                        hl3Name: h3CheckedVar ? hl3NameShow + "-" + hl3Name : false,
                        hl4Name: h4CheckedVar ? hl4NameShow + "-" + hl4Name : false,
                        hl5Name: h5CheckedVar ? hl5NameShow + "-" + hl5Name : false,
                        hl6Name: h6CheckedVar ? hl6NameShow + "-" + hl6Name : false,
                        itemName: itemName,
                        brand: brandName,
                        color: color,
                        size: brandSize,
                        pattern: brandPatter,
                        material: brandMaterial,
                        design: brandDesign,
                        barCode: barCode,
                        sell_Price: mrp,

                    }
                    let priority = Object.values(sdata)
                    let countt = 0
                    let countArr = []
                    let assValue = {}
                    let assortment = []

                    for (var z = 0; z < priority.length; z++) {
                        if (priority[z] != false) {
                            countt++
                            countArr.push(countt)
                        }
                    }

                    let setkeys = Object.keys(sdata)

                    setkeys.forEach(d => {
                        if (sdata.data != false) {

                            assValue = {
                                type: d,
                                value: sdata[d],
                                priority: ""

                            }
                        }
                        assortment.push(assValue)
                    })
                    this.setState({
                        prioritySet: true,
                        prioritySetData: assortment,
                        setCount: countArr,

                    })

                } else {
                    let payload = {
                        hl1Name: h1CheckedVar ? hl1Name : "NA",
                        hl2Name: h2CheckedVar ? hl2Name : "NA",
                        hl3Name: h3CheckedVar ? hl3Name : "NA",
                        hl4Name: h4CheckedVar ? hl4Name : "NA",
                        hl5Name: h5CheckedVar ? hl5Name : "NA",
                        hl6Name: h6CheckedVar ? hl6Name : "NA",
                        itemName: itemName,
                        brand: brandName,
                        color: color,
                        size: brandSize,
                        pattern: brandPatter,
                        material: brandMaterial,
                        design: brandDesign,
                        barCode: barCode,
                        sell_Price: mrp,
                        configureAssortment: true,
                        assortmentcode: []
                    }
                    this.props.createAssortmentRequest(payload);
                }


            } else if (data == "prioritySetTrue") {
                const { h1, h2, h3, h4, itemName, brandDesign, brandMaterial, brandName, brandPatter, brandSize, color, h1Checked, h2Checked, h3Checked, h4Checked, h5Checked, h6Checked, barCode, mrp } = this.state;
                let payload = {
                    hl1Name: h1CheckedVar ? hl1Name : "NA",
                    hl2Name: h2CheckedVar ? hl2Name : "NA",
                    hl3Name: h3CheckedVar ? hl3Name : "NA",
                    hl4Name: h4CheckedVar ? hl4Name : "NA",
                    hl5Name: h5CheckedVar ? hl5Name : "NA",
                    hl6Name: h6CheckedVar ? hl6Name : "NA",
                    itemName: itemName,
                    brand: brandName,
                    color: color,
                    size: brandSize,
                    pattern: brandPatter,
                    material: brandMaterial,
                    design: brandDesign,
                    barCode: barCode,
                    sell_Price: mrp,
                    configureAssortment: false,
                    assortmentcode: this.state.assortmentcode
                }

                this.props.createAssortmentRequest(payload);


            }

        }, 10)

    }

    prioritySetTrue(setDataa) {

        let setData = setDataa

        let assormentArray = []
        setData.forEach(s => {

            let assortmentCode = {}
            assortmentCode[s.type] = typeof (s.value) == 'string' ? s.value.split('-')[1] : s.value

            assormentArray.push(assortmentCode)
        })

        this.setState({
            assortmentcode: assormentArray
        })
        setTimeout(() => {
            this.createAssortmentPayload('prioritySetTrue')
        }, 10)
    }



    openClosePopup(e) {
        this.setState({
            popup: false,
            prioritySet: false
        })
        if (e == 'yes') {
            this.createAssortmentPayload('openClosePopup')
        }
    }


    closePriority() {
        this.setState({
            prioritySet: false
        })
    }

    createAssortment(e) {
        this.createAssortmentPayload('PrioritySet')
    }

    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.inventoryManagement.getAssortment.data.prePage,
                    current: this.props.inventoryManagement.getAssortment.data.currPage,
                    next: this.props.inventoryManagement.getAssortment.data.currPage + 1,
                    maxPage: this.props.inventoryManagement.getAssortment.data.maxPage,
                })
                if (this.props.inventoryManagement.getAssortment.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        search: this.state.search,
                        no: this.props.inventoryManagement.getAssortment.data.currPage - 1
                    }
                    this.props.getAssortmentRequest(data);
                }

            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.inventoryManagement.getAssortment.data.prePage,
                current: this.props.inventoryManagement.getAssortment.data.currPage,
                next: this.props.inventoryManagement.getAssortment.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getAssortment.data.maxPage,
            })
            if (this.props.inventoryManagement.getAssortment.data.currPage != this.props.inventoryManagement.getAssortment.data.maxPage) {
                let data = {
                    type: this.state.type,
                    search: this.state.search,
                    no: this.props.inventoryManagement.getAssortment.data.currPage + 1
                }
                this.props.getAssortmentRequest(data);
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1) {

            }
            else {

                this.setState({
                    prev: this.props.inventoryManagement.getAssortment.data.prePage,
                    current: this.props.inventoryManagement.getAssortment.data.currPage,
                    next: this.props.inventoryManagement.getAssortment.data.currPage + 1,
                    maxPage: this.props.inventoryManagement.getAssortment.data.maxPage,
                })
                if (this.props.inventoryManagement.getAssortment.data.currPage <= this.props.inventoryManagement.getAssortment.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        search: this.state.search,
                        no: 1
                    }
                    this.props.getAssortmentRequest(data);
                }
            }

        }
    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }

    activitySh(e) {
        this.setState({
            activity: !this.state.activity
        })
    }

    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
                no: 1
            })
            let data = {
                search: this.state.search,
                type: 3,
                no: 1
            }
            this.props.getAssortmentRequest(data);
        }
    }

    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: ""
        })
        let data = {
            search: "",
            type: 1,
            no: 1
        }
        this.props.getAssortmentRequest(data);
    }
    manageDrop(e) {
        var id = e.target.id;
        this.setState({
            hl1drop: id == "hl1" ? !this.state.hl1drop : false,
            hl2drop: id == "hl2" ? !this.state.hl2drop : false,
            hl3drop: id == "hl3" ? !this.state.hl3drop : false,
            hl4drop: id == "hl4" ? !this.state.hl4drop : false
        })
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }
    closeH1Modal() {
        this.setState({
            hl1Modal: false,
            hl1ModalAnimation: !this.state.hl1ModalAnimation
        })

    }

    manageDropdown(type, checkValue,name) {
        if (checkValue == "HL1NAME") {
            let hlField = ""
            if (type == "HL1NAME") {
                hlField = "h1"
            } else if (type == "HL2NAME") {
                hlField = "h2"
            }
            else if (type == "HL3NAME") {
                hlField = "h3"
            }
            else if (type == "HL4NAME") {
                hlField = "h4"
            }
            else if (type == "HL5NAME") {
                hlField = "h5"
            } else if (type == "HL6NAME") {
                hlField = "h6"
            }

            let data = {
                no: 1,
                type: 1,
                search: "",
                totalCount: 0
            }
            this.props.hl1Request(data)
            this.setState({
                hlField: hlField,
                hlName:name,
                hl1Modal: true,
                hl1ModalAnimation: !this.state.hl1ModalAnimation
            })


        }
        else if (checkValue == "HL2NAME") {
            let hlField = ""
            if (type == "HL1NAME") {
                hlField = "h1"
            } else if (type == "HL2NAME") {
                hlField = "h2"
            }
            else if (type == "HL3NAME") {
                hlField = "h3"
            }
            else if (type == "HL4NAME") {
                hlField = "h4"
            }
            else if (type == "HL5NAME") {
                hlField = "h5"
            } else if (type == "HL6NAME") {
                hlField = "h6"
            }
            let dropdownObject = this.state.dropdownObject
            let hl1Name = ""
            if ((_.invert(dropdownObject))["HL1NAME"] == "HL1NAME") {
                hl1Name = this.state.h1
            } else if ((_.invert(dropdownObject))["HL1NAME"] == "HL2NAME") {
                hl1Name = this.state.h2
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL3NAME") {
                hl1Name = this.state.h3
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL4NAME") {
                hl1Name = this.state.h4
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL5NAME") {
                hl1Name = this.state.h5
            }
            let data = {
                hl1Name: hl1Name,
                no: 1,
                type: 1,
                search: "",
                totalCount: 0
            }
            this.props.hl2Request(data)

            this.setState({
                hl1Name: hl1Name,
                hlName:name,
                hlField: hlField,
                hl2Modal: true,
                hl2ModalAnimation: !this.state.hl2ModalAnimation
            })
        } else if (checkValue == "HL3NAME") {
            let dropdownObject = this.state.dropdownObject
            let hl1Name = ""
            let hl2Name = ""
            if ((_.invert(dropdownObject))["HL1NAME"] == "HL1NAME") {
                hl1Name = this.state.h1
            } else if ((_.invert(dropdownObject))["HL1NAME"] == "HL2NAME") {
                hl1Name = this.state.h2
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL3NAME") {
                hl1Name = this.state.h3
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL4NAME") {
                hl1Name = this.state.h4
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL5NAME") {
                hl1Name = this.state.h5
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL6NAME") {
                hl1Name = this.state.h6
            }

            if ((_.invert(dropdownObject))["HL2NAME"] == "HL1NAME") {
                hl2Name = this.state.h1
            } else if ((_.invert(dropdownObject))["HL2NAME"] == "HL2NAME") {
                hl2Name = this.state.h2
            }

            else if ((_.invert(dropdownObject))["HL2NAME"] == "HL3NAME") {
                hl2Name = this.state.h3
            }
            else if ((_.invert(dropdownObject))["HL2NAME"] == "HL4NAME") {
                hl2Name = this.state.h4
            }
            else if ((_.invert(dropdownObject))["HL2NAME"] == "HL5NAME") {
                hl2Name = this.state.h5
            }
            else if ((_.invert(dropdownObject))["HL2NAME"] == "HL6NAME") {
                hl2Name = this.state.h6
            }
            let hlField = ""
            if (type == "HL1NAME") {
                hlField = "h1"
            } else if (type == "HL2NAME") {
                hlField = "h2"
            }
            else if (type == "HL3NAME") {
                hlField = "h3"
            }
            else if (type == "HL4NAME") {
                hlField = "h4"
            }
            else if (type == "HL5NAME") {
                hlField = "h5"
            } else if (type == "HL6NAME") {
                hlField = "h6"
            }
            let data = {
                hl1Name: hl1Name,
                hl2Name: hl2Name,
                no: 1,
                type: 1,
                search: "",
                totalCount: 0
            }
            this.props.hl3Request(data)

            this.setState({
                hlName:name,
                hl1Name: hl1Name,
                hl2Name: hl2Name,
                hlField: hlField,
                hl3Modal: true,
                hl3ModalAnimation: !this.state.hl3ModalAnimation
            })
        } else if (checkValue == "HL4NAME") {
            let dropdownObject = this.state.dropdownObject
            let hl1Name = ""
            let hl2Name = ""
            if ((_.invert(dropdownObject))["HL1NAME"] == "HL1NAME") {
                hl1Name = this.state.h1
            } else if ((_.invert(dropdownObject))["HL1NAME"] == "HL2NAME") {
                hl1Name = this.state.h2
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL3NAME") {
                hl1Name = this.state.h3
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL4NAME") {
                hl1Name = this.state.h4
            }
            else if ((_.invert(dropdownObject))["HL1NAME"] == "HL5NAME") {
                hl1Name = this.state.h5
            }

            if ((_.invert(dropdownObject))["HL2NAME"] == "HL1NAME") {
                hl2Name = this.state.h1
            } else if ((_.invert(dropdownObject))["HL2NAME"] == "HL2NAME") {
                hl2Name = this.state.h2
            }

            else if ((_.invert(dropdownObject))["HL2NAME"] == "HL3NAME") {
                hl2Name = this.state.h3
            }
            else if ((_.invert(dropdownObject))["HL2NAME"] == "HL4NAME") {
                hl2Name = this.state.h4
            }
            else if ((_.invert(dropdownObject))["HL2NAME"] == "HL5NAME") {
                hl2Name = this.state.h5
            }
            else if ((_.invert(dropdownObject))["HL2NAME"] == "HL6NAME") {
                hl2Name = this.state.h5
            }
            let hl3Name = ""
            if ((_.invert(dropdownObject))["HL3NAME"] == "HL1NAME") {
                hl3Name = this.state.h1
            } else if ((_.invert(dropdownObject))["HL3NAME"] == "HL2NAME") {
                hl3Name = this.state.h2
            }

            else if ((_.invert(dropdownObject))["HL3NAME"] == "HL3NAME") {
                hl3Name = this.state.h3
            }
            else if ((_.invert(dropdownObject))["HL3NAME"] == "HL4NAME") {
                hl3Name = this.state.h4
            }
            else if ((_.invert(dropdownObject))["HL3NAME"] == "HL5NAME") {
                hl3Name = this.state.h5
            }
            else if ((_.invert(dropdownObject))["HL3NAME"] == "HL6NAME") {
                hl3Name = this.state.h6
            }


            let hlField = ""
            if (type == "HL1NAME") {
                hlField = "h1"
            } else if (type == "HL2NAME") {
                hlField = "h2"
            }
            else if (type == "HL3NAME") {
                hlField = "h3"
            }
            else if (type == "HL4NAME") {
                hlField = "h4"
            }
            else if (type == "HL5NAME") {
                hlField = "h5"
            } else if (type == "HL6NAME") {
                hlField = "h6"
            }
            let data = {
                hl1Name: hl1Name,
                hl2Name: hl2Name,
                hl3Name: hl3Name,
                no: 1,
                type: 1,
                search: "",
                totalCount: 0
            }
            this.props.hl4Request(data)

            this.setState({
                hlName:name,
                hl1Name: hl1Name,
                hl2Name: hl2Name,
                hl3Name: hl3Name,
                hlField: hlField,
                hl4Modal: true,
                hl4ModalAnimation: !this.state.hl4ModalAnimation
            })
        } else if (checkValue == "HL5NAME") {

        } else if (checkValue == "HL6NAME") {

        }

    }
    closeAssorment() {
        this.setState({
            hl1ModalAnimation: false
        })
    }


    handleClickOutside(event) {
        var id = event.target.id
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            if (id == "hl1" || id == "hl2" || id == "hl3" || id == "hl4") {
            } else {
                this.setState({
                    hl1drop: false,
                    hl2drop: false,
                    hl3drop: false,
                    hl4drop: false
                })
            }
        }
    }

    updateHl(data) {
        if (data.field == "h1") {
            this.setState({
                h1: data.value,
                h2: "",
                h3: "",
                h4: "",
                h5: "",
                h6: "",
               
            })
        } else if (data.field == "h2") {
            this.setState({
                h2: data.value,
                h3: "",
                h4: "",
                h5: "",
                h6: "",
            
            })
        }
        else if (data.field == "h3") {
            this.setState({
                h3: data.value,
                h4: "",
                h5: "",
                h6: "",
              
            })
        }
        else if (data.field == "h4") {
            this.setState({
                h4: data.value,
                h5: "",
                h6: "",
               
            })
        }
        else if (data.field == "h5") {
            this.setState({
                h5: data.value,
                h6: "",
               
            })
        }
        else if (data.field == "h6") {
            this.setState({
                h6: data.value
            })
        }
    }

    closeH2Modal() {
        this.setState({
            hl2Modal: false,
            hl2ModalAnimation: !this.state.hl2ModalAnimation
        })

    }
    closeH3Modal() {
        this.setState({
            hl3Modal: false,
            hl3ModalAnimation: !this.state.hl3ModalAnimation
        })

    }



    closeH4Modal() {
        this.setState({
            hl4Modal: false,
            hl4ModalAnimation: !this.state.hl4ModalAnimation
        })

    }


    render() {


        const { assortmentDropdown, hl1, hl2, hl3, hl4, hl5, hl6, h1, h2, h3, h4, h5, h6, itemName, brandDesign, brandMaterial, brandName, brandPatter, brandSize, mrp, color, h1Checked, h2Checked, h3Checked, h4Checked, h5Checked, h6Checked, materials, patterns, designs, colors, sizes, brands, barCode } = this.state;

        return (
            <div className="container-fluid" >
                <div className="container-fluid">
                    <div className="container_div" id="">


                        <div className="container-fluid pad-0">
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                <div className="replenishment_container">
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <ul className="list_style">
                                            <li>
                                                <label className="contribution_mart">
                                                    ASSORTMENT

                                            </label>
                                            </li>
                                            <li>
                                                <p className="master_para">Create and manage assortments</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="container-fluid assortment">
                                        <div className="col-md-12 col-sm-12 pad-0">
                                            <h5>Manage Assortment Filters</h5>
                                            <p>Filter your results by choosing below options</p>
                                        </div>
                                        {/* {Object.keys(assortmentDropdown).length == 0 ?  
                                        <div className="col-md-12 col-sm-12 pad-0 assortmentSelect">
                                            <ul className="pad-0 m-top-5">
                                                <li><h6>Choose Division</h6>
                                                    <div className="form-group assortSelectDiv">
                                                        <div id="hl1"
                                                            onClick={(e) => this.manageDrop(e)}
                                                            value={h1} title={h1} className="form-control dropDiv alignMiddle">
                                                            {this.state.h1 == "" ? "Select Division" : this.state.h1.length > 15 ? this.state.h1.slice(0, 20).concat("...") : this.state.h1}
                                                            <div className="checkSeperate">
                                                                <label className="checkBoxLabel0"><input checked={h1Checked} onChange={(e) => this.onCheck(e)} id="h1Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                            </div>
                                                        </div>
                                                        {this.state.hl1drop ? <div ref={(e) => this.setWrapperRef(e)} className="dropdown searchStoreProfileMain m-top-5">
                                                            <input type="search" placeholder="Search..." />
                                                            <ul className="pad-0 m-top-10">
                                                                {this.state.hl1.length == 0 ? null : this.state.hl1.map((data, key) => (<li key={key} className={this.state.h1 == data ? "activeDropDown" : null} value={data} id="hl1" onClick={(e) => this.onChange(e, data)}>{data}</li>))}
                                                            </ul>
                                                        </div> : null}
                                                    </div>
                                                </li>
                                                <li><h6>Choose Section</h6>
                                                    <div className="form-group assortSelectDiv">
                                                        <div className="dropDiv form-control alignMiddle" title={h2} id="hl2" onClick={(e) => this.manageDrop(e)} value={h2}>
                                                            {this.state.h2 == "" ? "Select Section" : this.state.h2.length > 15 ? this.state.h2.slice(0, 20).concat("...") : this.state.h2}
                                                            {hl2.length == 0 ? null : hl2.map((data, key) => (<option key={key} value={data}>{data}</option>))} */}
                                        {/* <div className="checkSeperate">
                                                                <label className="checkBoxLabel0"><input checked={h2Checked} onChange={(e) => this.onCheck(e)} id="h2Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                            </div> 
                                         </div>
                                                        {this.state.hl2drop ? <div ref={(e) => this.setWrapperRef(e)} className="dropdown searchStoreProfileMain m-top-5">
                                                            <input type="search" placeholder="Search..." />
                                                            <ul className="pad-0 m-top-10">
                                                                {this.state.hl2.length == 0 ? null : this.state.hl2.map((data, key) => (<li key={key} className={this.state.h2 == data ? "activeDropDown" : null} value={data} id="hl2" onClick={(e) => this.onChange(e, data)}>{data}</li>))}
                                                            </ul>
                                                        </div> : null}
                                                    </div>
                                                </li>  */}
                                        {/* <li><h6>Choose Department</h6>
                                                    <div className="form-group assortSelectDiv">
                                                        <div id="hl3" onClick={(e) => this.manageDrop(e)} title={h3} value={h3} className="form-control dropDiv alignMiddle">
                                                            {this.state.h3 == "" ? "Select Department" : this.state.h3.length > 15 ? this.state.h3.slice(0, 20).concat("...") : this.state.h3}
                                                            <div className="checkSeperate">
                                                                <label className="checkBoxLabel0"><input checked={h3Checked} onChange={(e) => this.onCheck(e)} id="h3Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                            </div>
                                                        </div>
                                                        {this.state.hl3drop ? <div ref={(e) => this.setWrapperRef(e)} className="dropdown searchStoreProfileMain m-top-5">
                                                            <input type="search" placeholder="Search..." />
                                                            <ul className="pad-0 m-top-10">
                                                                {this.state.hl3.length == 0 ? null : this.state.hl3.map((data, key) => (<li key={key} className={this.state.h3 == data ? "activeDropDown" : null} value={data} id="hl3" onClick={(e) => this.onChange(e, data)}>{data}</li>))}
                                                            </ul>
                                                        </div> : null}
                                                    </div>
                                                </li>  */}
                                        {/* <li><h6>Choose Article</h6>
                                                    <div className="form-group assortSelectDiv">
                                                        <div id="hl4" onClick={(e) => this.manageDrop(e)} title={h4} value={h4} className="form-control dropDiv alignMiddle">
                                                            {this.state.h4 == "" ? "Select Article" : this.state.h4.length > 15 ? this.state.h4.slice(0, 20).concat("...") : this.state.h4}
                                                            {hl4.length == 0 ? null : hl4.map((data, key) => (<option key={key} value={data}>{data}</option>))} */}
                                        {/* <div className="checkSeperate">
                                                                <label className="checkBoxLabel0"><input checked={h4Checked} onChange={(e) => this.onCheck(e)} id="h4Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                            </div>
                                                        </div>  */}
                                        {/* {this.state.hl4drop ? <div ref={(e) => this.setWrapperRef(e)} className="dropdown searchStoreProfileMain m-top-5">
                                                            <input type="search" placeholder="Search..." />
                                                            <ul className="pad-0 m-top-10">
                                                                {this.state.hl4.length == 0 ? null : this.state.hl4.map((data, key) => (<li className={this.state.h4 == data ? "activeDropDown" : null} key={key} value={data} id="hl4" onClick={(e) => this.onChange(e, data)}>{data}</li>))}
                                                            </ul>
                                                        </div> : null}
                                                    </div>
                                                </li>
                                            </ul> */}
                                        {/* </div>  
                                     : null} */}
                                        {Object.keys(this.state.assortmentDropdown).length != 0 ?
                                            <div className="col-md-12 col-sm-12 pad-0 assortmentSelect">
                                                <ul className="pad-0 m-top-5">
                                                    {Object.keys(this.state.assortmentDropdown).map((data, key) => (

                                                        Object.values(this.state.assortmentDropdown[data]) != "" ?
                                                            <li key={key}><h6>Choose {Object.keys(this.state.assortmentDropdown[data])}</h6>
                                                                <div className="form-group assortSelectDiv">
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL1NAME" ? <div id="hl1"
                                                                        onClick={(e) => this.manageDropdown("HL1NAME", Object.values(this.state.assortmentDropdown[data]).join(),Object.keys(this.state.assortmentDropdown[data]).join())}
                                                                        value={h1} title={h1} className="form-control dropDiv alignMiddle">
                                                                        {this.state.h1 == "" ? "Select " + Object.keys(this.state.assortmentDropdown[data]).join() : this.state.h1.length > 15 ? this.state.h1.slice(0, 20).concat("...") : this.state.h1}


                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL1NAME" ? <div className="checkSeperate">
                                                                        <label className="checkBoxLabel0"><input checked={h1Checked} onChange={(e) => this.onCheck(e)} id="h1Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL2NAME" ? <div id="hl2"
                                                                        onClick={(e) =>this.state.h1 == "" ? null : this.manageDropdown("HL2NAME", Object.values(this.state.assortmentDropdown[data]).join(),Object.keys(this.state.assortmentDropdown[data]).join())}
                                                                        value={h2} title={h2} className="form-control dropDiv alignMiddle">
                                                                        {this.state.h2 == "" ? "Select " + Object.keys(this.state.assortmentDropdown[data]).join() : this.state.h2.length > 15 ? this.state.h2.slice(0, 20).concat("...") : this.state.h2}

                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL2NAME" ?
                                                                        <div className="checkSeperate">
                                                                            <label className="checkBoxLabel0"><input checked={h2Checked} onChange={(e) =>this.onCheck(e)} id="h2Checked" type="checkBox" /><span className= "checkmark1"></span> </label>
                                                                        </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL3NAME" ? <div id="hl3"
                                                                        onClick={(e) =>this.state.h2 == "" ? null : this.manageDropdown("HL3NAME", Object.values(this.state.assortmentDropdown[data]).join(),Object.keys(this.state.assortmentDropdown[data]).join())}
                                                                        value={h3} title={h3} className="form-control dropDiv alignMiddle">
                                                                        {this.state.h3 == "" ? "Select " + Object.keys(this.state.assortmentDropdown[data]).join() : this.state.h3.length > 15 ? this.state.h3.slice(0, 20).concat("...") : this.state.h3}

                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL3NAME" ? <div className="checkSeperate">
                                                                        <label className="checkBoxLabel0"><input checked={h3Checked} onChange={(e) =>this.onCheck(e)} id="h3Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL4NAME" ? <div id="hl4"
                                                                        onClick={(e) =>this.state.h3 == "" ? null : this.manageDropdown("HL4NAME", Object.values(this.state.assortmentDropdown[data]).join(),Object.keys(this.state.assortmentDropdown[data]).join())}
                                                                        value={h4} title={h4} className="form-control dropDiv alignMiddle">
                                                                        {this.state.h4 == "" ? "Select " + Object.keys(this.state.assortmentDropdown[data]).join() : this.state.h4.length > 15 ? this.state.h4.slice(0, 20).concat("...") : this.state.h4}

                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL4NAME" ? <div className="checkSeperate">
                                                                        <label className="checkBoxLabel0"><input checked={h4Checked} onChange={(e) =>this.onCheck(e)} id="h4Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL5NAME" ? <div id="hl5"
                                                                        onClick={(e) =>this.state.h4 == ""? null : this.manageDropdown("HL5NAME", Object.values(this.state.assortmentDropdown[data]).join(),Object.keys(this.state.assortmentDropdown[data]).join())}
                                                                        value={h5} title={h5} className="form-control dropDiv alignMiddle">
                                                                        {this.state.h1 == "" ? "Select " + Object.keys(this.state.assortmentDropdown[data]).join() : this.state.h5.length > 15 ? this.state.h5.slice(0, 20).concat("...") : this.state.h5}

                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL5NAME" ? <div className="checkSeperate">
                                                                        <label className="checkBoxLabel0"><input checked={h5Checked} onChange={(e) =>this.onCheck(e)} id="h5Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL6NAME" ? <div id="hl6"
                                                                        onClick={(e) =>this.state.h5==""? null : this.manageDropdown("HL6NAME", Object.values(this.state.assortmentDropdown[data]).join(),Object.keys(this.state.assortmentDropdown[data]).join())}
                                                                        value={h6} title={h6} className="form-control dropDiv alignMiddle">
                                                                        {this.state.h6 == "" ? "Select " + Object.keys(this.state.assortmentDropdown[data]).join() : this.state.h6.length > 15 ? this.state.h6.slice(0, 20).concat("...") : this.state.h6}

                                                                    </div> : null}
                                                                    {Object.values(this.state.assortmentDropdown[data]) != "" && data == "HL6NAME" ? <div className=" checkSeperate">
                                                                        <label className="checkBoxLabel0"><input checked={h6Checked} onChange={(e) =>this.onCheck(e)} id="h6Checked" type="checkBox" /><span className="checkmark1"></span> </label>
                                                                    </div> : null}
                                                                </div>
                                                                <div onClick={() => this.manageDrop()} ></div>
                                                            </li> : null))}
                                                </ul>
                                            </div> : null}


                                        <div className="col-md-12 pad-0 m-top-10">
                                            <div className="assortmentCheck">
                                                <ul>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="itemName" onChange={(e) => this.onCheck(e)} checked={itemName} />Item Name <span className="checkmark1"></span> </label>
                                                    </li>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="color" onChange={(e) => this.onCheck(e)} checked={color} />Color <span className="checkmark1"></span> </label>
                                                    </li>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="brandSize" onChange={(e) => this.onCheck(e)} checked={brandSize} />Size <span className="checkmark1"></span> </label>
                                                    </li>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="brandPatter" onChange={(e) => this.onCheck(e)} checked={brandPatter} />Pattern <span className="checkmark1"></span> </label>
                                                    </li>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="brandMaterial" onChange={(e) => this.onCheck(e)} checked={brandMaterial} />Material <span className="checkmark1"></span> </label>
                                                    </li>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="brandDesign" onChange={(e) => this.onCheck(e)} checked={brandDesign} />Design <span className="checkmark1"></span> </label>
                                                    </li>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="brandName" onChange={(e) => this.onCheck(e)} checked={brandName} />Brand <span className="checkmark1"></span> </label>
                                                    </li>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="barCode" onChange={(e) => this.onCheck(e)} checked={barCode} />Barcode  <span className="checkmark1"></span> </label>
                                                    </li>
                                                    <li>
                                                        <label className="checkBoxLabel0"><input type="checkBox" id="mrp" onChange={(e) => this.onCheck(e)} checked={mrp} />MRP  <span className="checkmark1"></span> </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 pad-0">
                                            <div className="assortGetDetail m-top-20">
                                                <button type="button" onClick={(e) => this.onGetDetail(e)} >GET DETAILS</button>
                                            </div>
                                        </div>

                                    </div>
                                    {this.state.count == "" ? null : <div>
                                        <div className="container-fluid m-top-50">
                                            <div className="row">
                                                <div className="totalItem">
                                                    <h3>{this.state.count}</h3>
                                                    <p>Total Items Available For Selected Assortment</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="container-fluid m-top-30">
                                            <div className="row">
                                                <div className="createAssortment">
                                                    <p>Click on below to create assortment for available items</p>
                                                    {this.state.status == "INPROGRESS" ? <button type="button" className="btnDisabled pointerNone" >CREATE ASSORTMENT</button>
                                                        : <button type="button" onClick={(e) => this.createAssortment(e)}>CREATE ASSORTMENT</button>}                                                </div>
                                                {this.state.status != "SUCCEEDED_VIEWED" ?
                                                    <div className="assortmentStatus m-top-20">
                                                        <div className="assortFailed">
                                                            {this.state.status == "SUCCEEDED" ? <img src={SuccessIcon} />
                                                                : this.state.status == "INPROGRESS" ? <img src={AssortRunning} /> : null}
                                                            <label>{this.state.statusMessage}</label>
                                                            {this.state.status == "SUCCEEDED" ? <img className="displayPointer" onClick={(e) => this.props.viewMessageRequest('data')} src={CloseStatus} /> : null}
                                                        </div>
                                                    </div> : null}
                                            </div>
                                        </div>
                                    </div>}
                                    <div className="container-fluid">
                                        <div className="row m-top-100 ">
                                            <div className="col-md-6 pad-0">
                                                <button onClick={(e) => this.activitySh(e)} className="assortViewBtn"><i className="fa fa-bell-o" aria-hidden="true"></i>VIEW ACTIVITIES</button>
                                            </div>
                                            <div className="col-md-12 pad-0">
                                                <div className="col-md-6"></div>
                                                <div className="col-md-6">
                                                    <ul className="list-inline search_list manageSearch m0">
                                                        <li>
                                                            <form onSubmit={(e) => this.onSearch(e)}>
                                                                <input onChange={(e) => this.setState({ search: e.target.value })} value={this.state.search} type="search" placeholder="Type to Search..." className="search_bar" />
                                                                <button type="submit" className="searchWithBar">Search
                                                                <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                                        <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                                                        </path></svg>
                                                                </button>
                                                            </form>
                                                        </li>

                                                    </ul>

                                                    {this.state.type == 3 ? <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span> : null}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid">

                                        <div className="row m-top-30 ">
                                            <span className="assortAvailable">Available Assortments</span>
                                            <table className="availabeAssortment tableOddColor">
                                                <thead>
                                                    <tr>
                                                        <th>Select Columns</th>
                                                        <th>Item Code</th>
                                                        <th>Assortment</th>
                                                        {sizes ? <th>Size</th> : null}
                                                        {patterns ? <th>Pattern</th> : null}
                                                        {materials ? <th>Material</th> : null}
                                                        {brands ? <th>Brand</th> : null}
                                                        {designs ? <th>Design</th> : null}
                                                        {colors ? <th>Color</th> : null}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td rowSpan="11" className="fixRow">
                                                        <p> Customize Assortment using
                                                        <span>filters</span></p>
                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={colors} id="colors" onChange={(e) => this.onCheck(e)} />Color <span className="checkmark1"></span> </label>
                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={brands} id="brands" onChange={(e) => this.onCheck(e)} />Brand <span className="checkmark1"></span></label>
                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={sizes} id="sizes" onChange={(e) => this.onCheck(e)} />Size <span className="checkmark1"></span></label>
                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={patterns} id="patterns" onChange={(e) => this.onCheck(e)} />Pattern <span className="checkmark1"></span></label>
                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={designs} id="designs" onChange={(e) => this.onCheck(e)} />Design <span className="checkmark1"></span></label>
                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={materials} id="materials" onChange={(e) => this.onCheck(e)} />Material <span className="checkmark1"></span></label> </td>
                                                    </tr>
                                                    {this.state.assortmentData == null ? <tr className="tableNoData assortmentNoDate"><td colspan="8"> NO DATA FOUND </td></tr>
                                                        :
                                                        this.state.assortmentData.length == 0 ? <tr className="tableNoData assortmentNoDate"><td colSpan="8" rowSpan="1"> NO DATA FOUND </td></tr>
                                                            : this.state.assortmentData.map((data, key) => (<tr key={key} className="tableDetails">
                                                                <td className="countCol4">{data.iCode}</td>
                                                                <td className="countCol4">{data.assortmentCode}</td>
                                                                {sizes ? <td className="countCol4">{data.size}</td> : null}
                                                                {patterns ? <td className="countCol4">{data.pattern}</td> : null}
                                                                {materials ? <td className="countCol4">{data.material}</td> : null}
                                                                {brands ? <td className="countCol4">{data.brand}</td> : null}
                                                                {designs ? <td className="countCol4">{data.design}</td> : null}
                                                                {colors ? <td className="countCol4">{data.color}</td> : null}
                                                            </tr>))}
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                    <div className="pagerDiv">
                                        <ul className="list-inline pagination">
                                            <li >
                                                <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                                    First
                                                     </button>
                                            </li>
                                            <li>
                                                <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li>
                                            {/* {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" disabled>
                                                        Prev
                  </button>
                                                </li>} */}
                                            <li>
                                                <button className="PageFirstBtn pointerNone">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                                <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" disabled>
                                                        Next
                  </button>
                                                </li>}


                                            {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.assortmentCreated ? <AssortmentCreated createModal={(e) => this.createModal(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.activity ? <ViewActivityModal {...this.props} activitySh={(e) => this.activitySh(e)} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {/* {this.state.popup ? <CreateAssortConfirmModal defaultassortment={this.state.defaultassortment} openClosePopup={(e) => this.openClosePopup(e)} /> : null} */}
                {this.state.popup ? <DefaultAssortment defaultassortment={this.state.defaultassortment} openClosePopup={(e) => this.openClosePopup(e)} />
                    : this.state.prioritySet ? <PriorityModal openClosePopup={(e) => this.openClosePopup(e)} prioritySetTrue={(e) => this.prioritySetTrue(e)} prioritySetData={this.state.prioritySetData} setCount={this.state.setCount} />
                        : null}
                {this.state.hl1Modal ? <AssortmentFilterModal hlName={this.state.hlName} hlField={this.state.hlField} {...this.props} updateHl={(e) => this.updateHl(e)} closeH1Modal={(e) => this.closeH1Modal(e)} hl1ModalAnimation={this.state.hl1ModalAnimation} /> : null}
                {this.state.hl2Modal ? <Hl2Modal {...this.props} hlName={this.state.hlName} hl1Name={this.state.hl1Name} hlField={this.state.hlField} closeH2Modal={(e) => this.closeH2Modal(e)} updateHl={(e) => this.updateHl(e)} hl2ModalAnimation={this.state.hl2ModalAnimation} /> : null}

                {this.state.hl3Modal ? <Hl3Modal {...this.props} hlName={this.state.hlName} hl1Name={this.state.hl1Name} hl2Name={this.state.hl2Name} hlField={this.state.hlField} closeH3Modal={(e) => this.closeH3Modal(e)} updateHl={(e) => this.updateHl(e)} hl3ModalAnimation={this.state.hl3ModalAnimation} /> : null}

                {this.state.hl4Modal ? <Hl4Modal {...this.props}hlName={this.state.hlName} hlField={this.state.hlField} closeH4Modal={(e) => this.closeH4Modal(e)} hl1Name={this.state.hl1Name} hl2Name={this.state.hl2Name} hl3Name={this.state.hl3Name} updateHl={(e) => this.updateHl(e)} hl4ModalAnimation={this.state.hl4ModalAnimation} /> : null}

            </div>


        );
    }
}

export default Assortment;  