import React from "react";

class ViewActivityFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: this.props.search,
            type: this.props.type,
            no: this.props.no,
            status: this.props.status,
            itemCount: this.props.itemCount,
            createdOn: this.props.createdOn,
        };
    }

    componentWillMount(){
        this.setState({
            search: this.props.search,
            type: this.props.type,
            no: this.props.no,
            status: this.props.status,
            itemCount: this.props.itemCount,
            createdOn: this.props.createdOn,
        })
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            search: nextProps.search,
            type: nextProps.type,
            no: nextProps.no,
            status: nextProps.status,
            itemCount: nextProps.itemCount,
            createdOn: nextProps.createdOn,
        })
    }

    applyFilter(e){
        e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            serach: "",
            status: this.state.status,
            itemCount: this.state.itemCount,
            createdOn: this.state.createdOn
        }
        this.props.getActivityRequest(data);
        this.props.openFilter(e);
        this.props.updateFilter(data)
    }



    handleChange(e){
        if(e.target.id == "status"){
            this.setState({
                status: e.target.value
            })
        }else if(e.target.id == "itemCount"){
            this.setState({
                itemCount: e.target.value
            })
        }else if(e.target.id == "createdOn"){
            e.target.placeholder = e.target.value;
            this.setState({
                createdOn: e.target.value
            })
        }
    }

    clearFilter(e){
        // e.preventDefault();
        this.setState({
            status: "",
            itemCount: "",
            createdOn: ""
        })
        document.getElementById('createdOn').placeholder = "Created On";
    } 

    render() {
        let count = 0;
        if(this.state.status != ""){
            count ++;
        }
        if(this.state.createdOn != ""){
            count ++;
        }if(this.state.itemCount != ""){
            count ++;
        }
        return (

            <div className={this.props.dataFilter ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.dataFilter ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.dataFilter ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.applyFilter(e)}>
                        <button type="button" onClick={(e) => this.props.openFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0 dataSyncFilterItem">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="itemCount" value={this.state.itemCount} placeholder="Item count" className="organistionFilterModal" />                                    </li>
                                    
                                    <li>
                                        <input type="date" placeholder="Created On" id="createdOn" onChange={(e) => this.handleChange(e)} value={this.state.createdOn} className="organistionFilterModal" />
                                    </li>
                                    

                                    <li>
                                        <select id="status" onChange={(e) => this.handleChange(e)} value={this.state.status} className="organistionFilterModal">
                                            <option value="">
                                                Status
                                                </option>
                                            <option value="PROCESSED">SUCCEEDED</option>
                                            <option value="FAILED">FAILED</option>
                                        </select></li>
                                       


                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        <button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                            CLEAR FILTER
                     </button>
                                    </li>
                                    <li>
                                    {this.state.status != "" || this.state.createdOn != "" || this.state.itemCount != "" ? <button type="submit" className="modal_Apply_btn">
                                                APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                    APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                 </div>
             </div>


        );
    }
}

export default ViewActivityFilter;
