import React,  { useState , useEffect} from 'react';
import { get } from 'https';
import _ from 'lodash';
import ToastLoader from '../loaders/toastLoader';

export const DefaultAssortment = (props) => {

    return (
        <div>
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>

                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain createAssortmentModal">

                        <div className="col-md-12 col-sm-12 pad-0">

                            <ul className="list_style m0">
                                <li><label className="contribution_mart">Default Assortment Pattern :</label>

                                </li>
                                <li>
                                    <label className="m-top-5">{props.defaultassortment}</label>
                                </li>

                            </ul>
                        </div>
                        <div className="emptyDiv"></div>
                        <div className="col-md-12 proceedOrNot pad-lft-0">
                            <p>If you want to create assortment manually you need to check atleast one or more from the above
                            Do you want to proceed with deafult assortment pattern?</p>
                            <div className="saveCloseAssortBtn m-top-30">
                                <button onClick={() => props.openClosePopup('yes')} type="buttton">Yes</button>
                                <button onClick={() => props.openClosePopup('no')} type="button">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
// export const PriorityModal = (props) => {/
export  function PriorityModal(props) {

   
    const [prioritySetData, setprioritySetData] = useState(props.prioritySetData)
    const [toastLoader,setToastLoader]= useState(false)
    const [toastMsg,setToastMsg]= useState("")
    const [focused,setFocused]= useState("")
 
  function createAssortment(){



    let assortment = prioritySetData
    let flag =false
    for(var j=0;j<assortment.length;j++){
        if(assortment[j].value!=false){
            if(assortment[j].priority==""){
            flag =true
            break
        }
    }

    }

    setTimeout(()=>{
        if(flag){
            setToastMsg("Assorment order is mandatory to fill!!"),
            setToastLoader(true)
        setTimeout(() => {
            setToastLoader(false)
        }, 1500);
    
        }else{
        let finalData =[]   
       assortment.forEach(a=>{
              
                     if(a.priority!=""){
                     
                         finalData.push(a)
                         
                     }
                })
         
                var sortedObjs = _.sortBy( finalData, 'priority' );
     
        props.prioritySetTrue(sortedObjs)
        props.openClosePopup('no')
            }
    },10)

  }  
 function handleChange(e){
  let prioritySetDataa=  props.prioritySetData
  let flag =false
  for(var i=0;i<prioritySetDataa.length;i++){
 if(e.target.value!=""){
  if( prioritySetDataa[i].priority == e.target.value){
      flag= true
     
      e.target.value=focused
      break
  }
}
  }
  if(flag){

    setToastMsg("Duplicate assortment priority found. Select unique assortment priority!!"),
        setToastLoader(true)
    setTimeout(() => {
        setToastLoader(false)
    }, 1500);

  }
  else{
  
  for(var j=0;j<prioritySetDataa.length;j++){
      if(prioritySetDataa[j].type == e.target.id){
        prioritySetDataa[j].priority = e.target.value
      }
  }
  setprioritySetData(prioritySetDataa)
}
 
  
  }

 function focusDataConfirmation(e){

       setFocused(e.target.value)
  }
 
    return (
    
        <div>
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block "></div>

                <div className=" display_block prioritySetModal">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal createAssortmentModal">

                        <div className="col-md-12 col-sm-12 pad-0">

                            <ul className="list_style m0">
                                <li><h2 className="contribution_mart">Confirm Assortment Order</h2 >
                                </li>
                                <li>
                                    <label className="m-top-5 fontWeightNormal">Select assortment priority for selected parameters</label>
                                </li>
                            </ul>
                        </div>
                        <div className="middleContent">
                            {prioritySetData.map((data, key) => (
                          data.value !=false?
                          data.type=="hl1Name"||data.type=="hl2Name"||data.type=="hl3Name" ||data.type=="hl4Name"||data.type=="hl5Name"||data.type=="hl6Name"?
                                <div className="col-md-12 m-top-15" key={key}>
                                    <div className="col-md-6 pad-lft-0">
                                        <div className="orders ">
                                            <div className="col-md-6">
                                                <label>{data.value.split('-')[0]}</label>
                                            </div>
                                            <div className="col-md-6">
                                                <select id={data.type}  onChange={(e)=>handleChange(e)}  onFocus={(e) => focusDataConfirmation(e)} >
                                                <option value="">Select priority</option>
                                                {props.setCount.map((dataa, key) => (
                                                             <option key={key} value={dataa}>{dataa}</option>
                                                             ))}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>:<div className="col-md-12" key={key}>
                                    <div className="col-md-6 pad-lft-0">
                                        <div className="orders ">
                                            <div className="col-md-6">
                                                <label>{data.type}</label>
                                            </div>
                                            <div className="col-md-6">
                                            <select id={data.type} onChange={(e)=>handleChange(e)} onFocus={(e) => focusDataConfirmation(e)}>
                                            <option value="">Select priority</option>
                                                {props.setCount.map((dataa, key) => (
                                                            <option key={key} value={dataa}>{dataa}</option>
                                                             ))}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>:null
                              

                            ))}



                        </div>
                        <div className="col-md-12 proceedOrNot pad-lft-0">
                            {/* <label>Indicates highest priority assortment </label> */}
                            <div className="saveCloseAssortBtn">
                                <button  type="buttton" onClick={()=>createAssortment()}>Confirm</button>
                                <button onClick={() => props.openClosePopup('no')} type="button">Close</button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            {toastLoader ? <ToastLoader toastMsg={toastMsg} /> : null}
        </div>
    )
}
// export default PriorityModal