import React from "react";
import ViewActivityFilter from "./viewActivityFilter";
import SideBar from "../sidebar";
import RightSideBar from "../rightSideBar";
import openRack from "../../assets/open-rack.svg";
import BraedCrumps from "../breadCrumps";
import RequestError from "../loaders/requestError";
import FilterLoader from "../loaders/filterLoader";
import ToastLoader from "../loaders/toastLoader";

class ViewActivity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            filter: false,
            dataFilter: false,
            type: 1,
            no: 1,
            search: "",
            loader: true,
            status: "",
            itemCount: "",
            createdOn: "",
            alert: false,
            code: "",
            errorMessage: "",
            errorCode: "",
            toastLoader:false,
            toastMsg:""
        };
    }

    componentWillMount() {
        let data = {
            type: 1,
            search: "",
            no: 1
        }
        this.props.getActivityRequest(data);
    }
    
    componentWillReceiveProps(nextProps) {
        if(nextProps.inventoryManagement.getActivity.isSuccess) {
            this.setState({
                loader: false,
                prev: nextProps.inventoryManagement.getActivity.data.prePage,
                current: nextProps.inventoryManagement.getActivity.data.currPage,
                next: nextProps.inventoryManagement.getActivity.data.currPage + 1,
                maxPage: nextProps.inventoryManagement.getActivity.data.maxPage,
                tableData: nextProps.inventoryManagement.getActivity.data.resource == null ? [] : nextProps.inventoryManagement.getActivity.data.resource
            })

        } else if (nextProps.inventoryManagement.getActivity.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.inventoryManagement.getActivity.message.status,
                errorCode: nextProps.inventoryManagement.getActivity.message.error == undefined ? undefined : nextProps.inventoryManagement.getActivity.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.getActivity.message.error == undefined ? undefined : nextProps.inventoryManagement.getActivity.message.error.errorMessage
            })
        }

        if (nextProps.inventoryManagement.getActivity.isLoading) {
            this.setState({
                loader: true
            })
        }
    }

    updateFilter(data){
        this.setState({
            status: data.status,
            itemCount: data.itemCount,
            createdOn: data.createdOn,
            type: 2,
            search: "",
            no: 1
        })
    }

    openFilter(e) {
        this.setState({
            filter: !this.state.filter,
            dataFilter: !this.state.dataFilter
        })
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.inventoryManagement.getActivity.data.prePage,
                current: this.props.inventoryManagement.getActivity.data.currPage,
                next: this.props.inventoryManagement.getActivity.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getActivity.data.maxPage,
            })
            if (this.props.inventoryManagement.getActivity.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    search: this.state.state.search,
                    no: this.props.inventoryManagement.getActivity.data.currPage - 1,
                    itemCount: this.state.itemCount,
                    status: this.state.status,
                    createdOn: this.state.createdOn
                }
                this.props.getActivityRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.inventoryManagement.getActivity.data.prePage,
                current: this.props.inventoryManagement.getActivity.data.currPage,
                next: this.props.inventoryManagement.getActivity.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getActivity.data.maxPage,
            })
            if (this.props.inventoryManagement.getActivity.data.currPage != this.props.inventoryManagement.getActivity.data.maxPage) {
                let data = {
                    type: this.state.type,
                    search: this.state.state.search,
                    no: this.props.inventoryManagement.getActivity.data.currPage + 1,
                    itemCount: this.state.itemCount,
                    status: this.state.status,
                    createdOn: this.state.createdOn,
                }
                this.props.getActivityRequest(data)
            }
        } else if (e.target.id == "first") {
            this.setState({
                prev: this.props.inventoryManagement.getActivity.data.prePage,
                current: this.props.inventoryManagement.getActivity.data.currPage,
                next: this.props.inventoryManagement.getActivity.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getActivity.data.maxPage,
            })
            if (this.props.inventoryManagement.getActivity.data.currPage <= this.props.inventoryManagement.getActivity.data.maxPage) {
                let data = {
                    type: this.state.type,
                    search: this.state.state.search,
                    no: 1,
                    itemCount: this.state.itemCount,
                    status: this.state.status,
                    createdOn: this.state.createdOn,
                }
                this.props.getActivityRequest(data);
            }

        }
    }

    onClearSearch(e){
        this.setState({
            search: "",
            type: 1,
            no: 1
        })
        let data = {
            type: 1,
            search: "",
            no: 1
        }
        this.props.getActivityRequest(data);
    }

    onSearch(e){
        e.preventDefault();
        if(this.state.search == ""){
            this.setState({
                toastMsg:"Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
               this.setState({
                toastLoader:false
               })
            }, 1500);
        }else{
        let data = {
            search: this.state.search,
            type: 3,
            no: 1
        }
        this.props.getActivityRequest(data);
        this.setState({
            type: 3,
            no: 1,
            status: "",
            itemCount: "",
            createdOn: "",
        })
    }
}

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="col-md-12 col-sm-12 col-xs-12">
                    <div className="container_content heightAuto">
                        <div className="col-md-6 col-sm-12 pad-0">
                            <ul className="list_style">
                                <li>
                                    <label className="contribution_mart">
                                        View Activity
                            </label>
                                </li>

                                <li className="m-top-30">

                                    <ul className="list-inline m-top-10">

                                        <li>
                                            <button className="filter_button" onClick={e => this.openFilter(e)} data-toggle="modal" data-target="#myModal">
                                                FILTER

                                            <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                                                    <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                                                </svg>
                                            </button>
                                        </li>
                                        {/* <li>
                                        <button className="deleteMulti">
                                            <span>
                                                <img src={multideleteIcon} />
                                            </span>
                                            <span>
                                                Delete Selected
                                                </span>
                                        </button>
                                    </li> */}
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-6 col-sm-12 pad-0">
                            {/* <ul className="list-inline circle_list">
                                <li>
                                    <div className="tooltip" onClick={() => this.props.history.push('/administration/organisation/addOrganisation')}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 35 35">
                                            <g fill="none" fillRule="evenodd">
                                                <circle cx="17.5" cy="17.5" r="17.5" fill="#6D6DC9" />
                                                <path fill="#FFF" d="M25.699 18.64v-1.78H18.64V9.8h-1.78v7.059H9.8v1.78h7.059V25.7h1.78V18.64H25.7" />
                                            </g>
                                        </svg>

                                        <p className="tooltiptext tooltip-left">
                                            Add Organisations <p>Please click on add icon to add new organisations data
                                        </p>
                                        </p>
                                    </div>

                                </li>
                            </ul> */}
                            <ul className="list-inline search_list manageSearch">
                                <li>
                                    <form onSubmit={(e) => this.onSearch(e)}>
                                        <input onChange={(e) => this.setState({search: e.target.value})} value={this.state.search} type="search" placeholder="Type to Search..." className="search_bar" />
                                        <button type="submit" className="searchWithBar">Search
                                            <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                                </path></svg>
                                        </button>
                                    </form>
                                </li>

                            </ul>
                            {this.state.type == 3 ? <span onClick={(e) => this.onClearSearch(e)} className="clearSearchFilter">
                                Clear Search Filter
                                </span> : null}
                        </div>

                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 bordere3e7f3 tableGeneric">
                            <div className="zui-wrapper">
                                <div className="scrollableTableFixed table-scroll zui-scroller dataSyncTable scrollableOrgansation orgScroll" id="table-scroll">

                                    <table className="table scrollTable main-table zui-table">
                                        <thead>
                                            <tr>

                                                {/* <th>
                                                <label>ORGANISATION NAME</label>
                                            </th> */}
                                                <th>
                                                    <label>ASSORTMENT HIERARCHY</label>
                                                </th>
                                                <th>
                                                    <label>PATTERN</label>
                                                </th>
                                                <th>
                                                    <label>ITEM COUNT</label>
                                                </th>
                                                <th>
                                                    <label>CREATED ON</label>
                                                </th>
                                                <th>
                                                    <label>STATUS</label>
                                                </th>


                                            </tr>
                                        </thead>

                                        <tbody>
                                            {this.state.tableData.length == 0 ? <tr className="tableNoData assortmentNoDate"><td> NO DATA FOUND </td></tr> : this.state.tableData.map((data, key) => (<tr key={key}>
                                                <td>
                                                    <label>
                                                        {data.hl1Name == "NA" ? "" : ` Division : ${data.hl1Name} `} {data.hl2Name == "NA" ? "" : ` Department : ${data.hl2Name} `} {data.hl3Name == "NA" ? "" : ` Section : ${data.hl3Name} `}{data.hl4Name == "NA" ? "" : ` Article : ${data.hl4Name} `}
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        {data.pattern}
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        {data.itemCount}
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        {data.createdOn}
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>{data.status}</label>
                                                </td>

                                            </tr>))}


                                        </tbody>

                                    </table>


                                </div>
                            </div>
                        </div>
                        <div className="pagerDiv">
                            <ul className="list-inline pagination">
                                {this.state.current == 1 ? <li >
                                    <button className="PageFirstBtn" >
                                        First
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="first" >
                                            First
                  </button>
                                    </li>}
                                {this.state.prev != 0 ? <li >
                                    <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                        Prev
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn" disabled>
                                            Prev
                  </button>
                                    </li>}
                                <li>
                                    <button className="PageFirstBtn pointerNone">
                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                    </button>
                                </li>
                                {this.state.next - 1 != this.state.maxPage ? <li >
                                    <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                        Next
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn borderNone" disabled>
                                            Next
                  </button>
                                    </li>}


                                {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                            </ul>
                        </div>

                        {this.state.filter ? <ViewActivityFilter {...this.state} updateFilter={(e) => this.updateFilter(e)} {...this.props} openFilter={(e) => this.openFilter(e)} /> : null}

                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} />:null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>
        );
    }
}

export default ViewActivity;
