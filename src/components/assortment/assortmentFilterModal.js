import React from "react";
import ToastLoader from "../loaders/toastLoader";

export default class AssortmentFilterModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search:"",
            hl1: [],
            hl1Value: this.props.h1,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            toastLoader: false,
            toastMsg: "",
        }
    }
    UNSAFE_componentWillMount() {
        this.setState({
            hl1Value: this.props.h1
        })
        if (this.props.inventoryManagement.hl1.isSuccess) {
            if (this.props.inventoryManagement.hl1.data.resource != null) {
                this.setState({

                    hl1: this.props.inventoryManagement.hl1.data.resource,
                    prev: this.props.inventoryManagement.hl1.data.prePage,
                    current: this.props.inventoryManagement.hl1.data.currPage,
                    next: this.props.inventoryManagement.hl1.data.currPage + 1,
                    maxPage: this.props.inventoryManagement.hl1.data.maxPage
                })
            } else {
                this.setState({
                    hl1: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })
            }
        }
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.inventoryManagement.hl1.isSuccess) {
            if (nextProps.inventoryManagement.hl1.data.resource != null) {
                this.setState({

                    hl1: nextProps.inventoryManagement.hl1.data.resource,
                    prev: nextProps.inventoryManagement.hl1.data.prePage,
                    current: nextProps.inventoryManagement.hl1.data.currPage,
                    next: nextProps.inventoryManagement.hl1.data.currPage + 1,
                    maxPage: nextProps.inventoryManagement.hl1.data.maxPage
                })
            } else {
                this.setState({
                    hl1: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })

            }
        }

    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.inventoryManagement.hl1.data.prePage,
                current: this.props.inventoryManagement.hl1.data.currPage,
                next: this.props.inventoryManagement.hl1.data.currPage + 1,
                maxPage: this.props.inventoryManagement.hl1.data.maxPage,
            })
            if (this.props.inventoryManagement.hl1.data.currPage != 0) {
                let data = {
                    no: this.props.inventoryManagement.hl1.data.currPage - 1,
                    type: this.state.type,
                    search: this.state.search,
                    totalCount: this.props.inventoryManagement.hl1.data.maxPage
                }
                this.props.hl1Request(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.inventoryManagement.hl1.data.prePage,
                current: this.props.inventoryManagement.hl1.data.currPage,
                next: this.props.inventoryManagement.hl1.data.currPage + 1,
                maxPage: this.props.inventoryManagement.hl1.data.maxPage,
            })
            if (this.props.inventoryManagement.hl1.data.currPage != this.props.inventoryManagement.hl1.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.inventoryManagement.hl1.data.currPage + 1,

                    search: this.state.search,
                    totalCount: this.props.inventoryManagement.hl1.data.maxPage
                }
                this.props.hl1Request(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.inventoryManagement.hl1.data.prePage,
                current: this.props.inventoryManagement.hl1.data.currPage,
                next: this.props.inventoryManagement.hl1.data.currPage + 1,
                maxPage: this.props.inventoryManagement.hl1.data.maxPage,
            })
            if (this.props.inventoryManagement.hl1.data.currPage <= this.props.inventoryManagement.hl1.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.search,
                    totalCount: this.props.inventoryManagement.hl1.data.maxPage
                }
                this.props.hl1Request(data)
            }

        }
    }
    onClose() {
        this.props.closeH1Modal();
    }

    ondone() {
        let hl1Value = this.state.hl1Value
        if (hl1Value != "" || hl1Value != undefined) {
            let data={
                value:hl1Value,
                field:this.props.hlField
            }
            this.props.updateHl(data)
            this.onClose()
        } else {

            this.setState({
              
                  toastMsg: "Select Data",
            toastLoader:true,
         })
      
  
        setTimeout(()=>{
            this.setState({
                toastLoader:false
            })
        },2000)
        }

    }

    selectedData(e) {
        let hl1 = this.state.hl1
        for (var i = 0; i < hl1.length; i++) {
            if (hl1[i] == e) {
                this.setState({
                    hl1Value: e
                })
            }
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
       this.onSearch();
        }
       
                     
      }
  onSearch(){
    if (this.state.search == "") {
        this.setState({
            toastMsg:"Enter text on search input ",
            toastLoader: true
        })
        setTimeout(() => {
           this.setState({
            toastLoader:false
           })
        }, 1500);
      } else {
          this.setState({
              type:3
          })
        let data = {
            type: 3,
            no: 1,
            search: this.state.search,
            totalCount: this.state.maxPage
        }
        this.props.hl1Request(data)
      }
  }

  onSearchClear(){
      this.setState({
          search:""
      })
    if(this.state.type ==3){

        let data = {
            type: 1,
            no: 1,
            search: "",
            totalCount: this.state.maxPage
        }
        this.props.hl1Request(data)
    }

  }
  handleChange(e){
      this.setState({
          search:e.target.value
      })

  }
    render() {
        return (
            <div className={this.props.hl1ModalAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.hl1ModalAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.hl1ModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.hl1ModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color selectVendorPopUp supplierSelectModal">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT {this.props.hlName}</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only single entry from below records</p>
                                        </li>
                                    </ul>
                                    <ul className="list-inline width_100 m-top-10">
                                        <form >
                                            <div className="col-md-7 col-sm-7 pad-0">
                                                <div className="mrpSelectCode">
                                                    <li>
                                                        <input type="text" value={this.state.search} onKeyPress={this._handleKeyPress} onChange={(e)=>this.handleChange(e)} className="search-box" placeholder="Type to search" />


                                                        <label className="m-lft-15">
                                                            <button id="find" type ="button" className="findButton" onClick={(e)=> this.onSearch(e)}>FIND
                                <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                    <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                                </svg>
                                                            </button>
                                                        </label>
                                                    </li>
                                                </div>
                                            </div>
                                            <li className="float_right">

                                                <label>
                                                    <button type="button" className="clearbutton" onClick ={(e)=>this.onSearchClear(e)}>CLEAR</button>
                                                </label> 
                                            </li>
                                        </form>
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>{this.props.hlName}</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.hl1.length != 0 ? this.state.hl1.map((data, key) => (
                                                    <tr key ={key} >
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input type="radio" name="supplier" checked={data == this.state.hl1Value} onChange={() => this.selectedData(`${data}`)} />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data}</td>
                                                    </tr>)) : <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> }

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" onClick={(e)=>this.ondone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" onClick={(e) => this.onClose(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg}/>:null}
            </div>


        );
    }
}

