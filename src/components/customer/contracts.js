import React from "react";
import AddCustomer from "./Managecustomer/addCustomer";
import EmptyBox from "./Managecustomer/emptyBox";
import ViewCustomer from "./Managecustomer/viewCustomer";
import CustomerData from "../../json/customerData.json";

class Contracts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customerState: [],
      // addCustomer: false
    };
  }

  onAddCustomer() {
    // this.setState({
    //   addCustomer: true
    // });
    this.props.history.push('/vendor/contracts/addVendor')

  }

  onSaveCustomer() {
    // this.setState({
      // addCustomer: false,
      // customerState: CustomerData
    // });
    this.props.history.push('/vendor/contracts/viewVendor')
  }

  render() {
    
    const hash = window.location.hash.split("/")[3];
    
    return (
      <div className="container-fluid">
         { hash == "emptyBox" ?
             <EmptyBox
             {...this.props}
             customerState = {this.state.customerState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addCustomer={() => this.onAddCustomer()}
            />
           : hash == "viewCustomer" ? 
            <ViewCustomer
              CustomerData={CustomerData}
              {...this.props}
              clickRightSideBar={() => this.props.rightSideBar()}
            />
            
         : hash == "addCustomer" ?
          <AddCustomer
            {...this.props}
            saveCustomer={() => this.onSaveCustomer()}
            clickRightSideBar={() => this.props.rightSideBar()}
          />: null} 
      </div>
    );
  }
}

export default Contracts;
