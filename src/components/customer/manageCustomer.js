import React from "react";
import AddCustomer from "./Managecustomer/addCustomer";
import EmptyBox from "./Managecustomer/emptyBox";
import ViewCustomer from "./Managecustomer/viewCustomer";
import CustomerData from "../../json/customerData.json";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

class ManageCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customerState: [],
      // addVendor: false
      page: 1,
      success: false,
      alert: false,
      loader: false,
      errorMessage: "",
      successMessage: "",
      emptyBox: false,
      viewCustomer: false,

      errorCode: "",
      code: "",
    };
  }


  // componentWillMount() {
  //   if (!this.props.vendor.getCustomer.isSuccess) {
  //     // this.props.vendorRequest(this.state.page);
  //   } else {
  //     this.setState({
  //       loader: false
  //     })
  //     if (this.props.vendor.getCustomer.data.resource == null) {
  //       this.setState({
  //         emptyBox: true,
  //         viewCustomer: false
  //       })
  //     } else {
  //       this.setState({
  //         customerState: this.props.vendor.getCustomer.data.resource,
  //         emptyBox: false,
  //         viewCustomer: true
  //       })
  //     }
  //   }
  // }
  componentWillReceiveProps(nextProps) {
    // if (!nextProps.vendor.editCustomer.isLoading && !nextProps.vendor.editCustomer.isSuccess && !nextProps.vendor.editCustomer.isError) {
    //   this.setState({
    //     loader: false,

    //   })
    // }
    if (nextProps.vendor.getAllManageCustomer.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getAllManageCustomerClear()
      if (nextProps.vendor.getAllManageCustomer.data.resource == null) {
        this.setState({
          emptyBox: true
        })
      } else {
        this.setState({
          customerState: nextProps.vendor.getAllManageCustomer.data.resource,
          viewCustomer: true
        })
      }
    } else if (nextProps.vendor.getAllManageCustomer.isError) {
      this.setState({
        loader: false,
        alert: true,
        success: false,
        errorCode: nextProps.vendor.getAllManageCustomer.message.error == undefined ? undefined : nextProps.vendor.getAllManageCustomer.message.error.errorCode,
        errorMessage: nextProps.vendor.getAllManageCustomer.message.error == undefined ? undefined : nextProps.vendor.getAllManageCustomer.message.error.errorMessage,

        code: nextProps.vendor.getAllManageCustomer.message.status,
      })
      this.props.getAllManageCustomerClear()
    } else if (nextProps.vendor.getAllManageCustomer.isLoading) {
      this.setState({
        loader: true
      })
    }

    // if (nextProps.vendor.deleteCustomer.isLoading) {
    //   this.setState({
    //     loader: true
    //   })
    // }
    // else if (nextProps.vendor.deleteCustomer.isSuccess) {
    //   this.setState({
    //     success: true,
    //     loader: false,
    //     successMessage: nextProps.vendor.deleteCustomer.data.message,
    //     alert: false,
    //   })
    //   this.props.deleteCustomerRequest();
    // }
    // else if (nextProps.vendor.deleteCustomer.isError) {
    //   this.setState({
    //     alert: true,
    //     errorMessage: nextProps.vendor.deleteCustomer.error.errorMessage,
    //     success: false,
    //     loader: false
    //   })
    //   this.props.deleteCustomerRequest();
    // }

    //edit  organization modal
    if (nextProps.vendor.editCustomer.isLoading) {
      this.setState({
        loader: true
      });
    }
    else if (nextProps.vendor.editCustomer.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        successMessage: nextProps.vendor.editCustomer.data.message,
        alert: false,
      })
      this.props.editCustomerClear();
    }
    else if (nextProps.vendor.editCustomer.isError) {
      this.setState({
        loader: false,
        alert: true,
        success: false,
        errorCode: nextProps.vendor.editCustomer.message.error == undefined ? undefined : nextProps.vendor.editCustomer.message.error.errorCode,
        errorMessage: nextProps.vendor.editCustomer.message.error == undefined ? undefined : nextProps.vendor.editCustomer.message.error.errorMessage,

        code: nextProps.vendor.editCustomer.message.status,
      })
      this.props.editCustomerClear();
    }

    // dynamic header
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getHeaderConfigClear()
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
      })
      this.props.getHeaderConfigClear()
    }

    // if (nextProps.vendor.accessCustomerPortal.isSuccess) {
    //   this.setState({
    //     loader: false
    //   })
    //   // this.props.accessCustomerPortalClear()
    // } else if (nextProps.vendor.accessCustomerPortal.isError) {
    //   this.setState({
    //     loader: false,
    //     alert: true,
    //     code: nextProps.vendor.accessCustomerPortal.message.status,
    //     errorCode: nextProps.vendor.accessCustomerPortal.message.error == undefined ? "NA" : nextProps.vendor.accessCustomerPortal.message.error.code,
    //     errorMessage: nextProps.vendor.accessCustomerPortal.message.error == undefined ? "NA" : nextProps.vendor.accessCustomerPortal.message.error.message
    //   })
    //   //this.props.accessCustomerPortalClear()
    // }

    if (//nextProps.vendor.createCustomer.isLoading || 
      nextProps.replenishment.getHeaderConfig.isLoading
        //|| nextProps.vendor.accessCustomerPortal.isLoading
        ) {
      this.setState({
        loader: true
      })
    }
  }
  onAddCustomer() {
    // this.setState({
    //   addVendor: true
    // });
    this.props.history.push('/mdm/manageCustomers/addCustomer')
  }

  onSaveCustomer() {
    this.setState({
      // addVendor: false,
      customerState: CustomerData
    });
    // this.props.history.push('/vendor/manageVendors/viewVendor')
  }

  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }

  render() {
    const hash = window.location.hash.split("/")[3];
    return (
      <div className="container-fluid pad-0">
        {/*{ this.state.emptyBox  ?
             <EmptyBox
             {...this.props}
             vendorState = {this.state.vendorState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addVendor={() => this.onAddVendor()}
            />
           : null}*/}

        {/*{ this.state.viewVendor ? */}
        <ViewCustomer
          CustomerData={this.state.customerState}
          {...this.props}
          clickRightSideBar={() => this.props.rightSideBar()}
        />
        {/*: null}*/}

        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export default ManageCustomer;
