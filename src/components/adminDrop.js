import React from 'react';
import { Link } from 'react-router-dom';
import { openMyDrop, closeMyDrop } from '../helper/index'

class AdminDrop extends React.Component {


  render() {
    var Organization = JSON.parse(sessionStorage.getItem('Organization')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Organization')).crud);
    var Manage_Users = JSON.parse(sessionStorage.getItem('Manage Users')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Users')).crud);
    var Roles = JSON.parse(sessionStorage.getItem('Roles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Roles')).crud);
    var Site = JSON.parse(sessionStorage.getItem('Site')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Site')).crud);
    var Site_Mapping = JSON.parse(sessionStorage.getItem('Site Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Site Mapping')).crud);
    var Custom_Masters = JSON.parse(sessionStorage.getItem('Custom Masters')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Custom Masters')).crud);
    var Data_Sync = JSON.parse(sessionStorage.getItem('Data Sync')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Data Sync')).crud);
    var Retail = JSON.parse(sessionStorage.getItem('Retail Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Retail Master')).crud);
    var MBO = JSON.parse(sessionStorage.getItem('MBO Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('MBO Master')).crud);
    // ______________________________MANAGE ROLE CRUD______________________________________________

    var Role_Master = JSON.parse(sessionStorage.getItem('Role Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Role Master')).crud)
    var Add_Role = JSON.parse(sessionStorage.getItem('Add Role')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Add Role')).crud)
    var Manage_Role = JSON.parse(sessionStorage.getItem('Manage Role')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Role')).crud)
    var Change_Setting = JSON.parse(sessionStorage.getItem('Change Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Change Setting')).crud);

    var FMCG_Master = JSON.parse(sessionStorage.getItem('FMCG Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('FMCG Master')).crud)
    var Event_Master = JSON.parse(sessionStorage.getItem('Event Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Event Master')).crud)
    var UPLOAD_MASTER = JSON.parse(sessionStorage.getItem('Upload Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Upload Master')).crud)
    console.log(Role_Master)
    return (
      <label>
        <div className="dropdown" onMouseOver={(e) => openMyDrop(e)} id="myFav">
          <button className="dropbtn home_link">Administration
              <i className="fa fa-chevron-down"></i>
          </button>
          {/* <div className="displayNone adminBreacrumbsDropdown anchorError" id="dropMenu">
            {Change_Setting > 0 && <a className="scheduler">Change Setting <span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>
              <div className="hovershowDropdown subDropPos">
                <Link to="/administration/festivalSetting" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Festival Setting</Link>
                <Link to="/administration/promotionalEvents" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Promotional Event</Link>
              </div>
            </a>}
            {Organization > 0 ? <Link to="/administration/organisation" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Organisation</Link> : null}
            {Manage_Users > 0 ? <Link to="/administration/users" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Users</Link> : null} */}
            {/* <a className="scheduler">Role Master <span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>

              <div className="hovershowDropdown subDropPos">
                <Link to="/administration/rolesMaster/addRoles" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Add Roles</Link>
                <Link to="/administration/rolesMaster/manageRoles" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Manage Roles</Link>
              </div>
            </a> */}
            {/* {Roles > 0 ? <Link to="/administration/roles" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Roles</Link> : null}
            {Site > 0 ? <Link to="/administration/site" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Site</Link> : null}
            {Site_Mapping > 0 ? <Link to="/administration/siteMapping" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Site Mapping</Link> : null}
            {Data_Sync > 0 ? <Link to="/administration/dataSync" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Data Sync</Link> : null}

            {Custom_Masters > 0 ? <span className="scheduler">Custom<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>

              <div className="hovershowDropdown customDropShow">
                <ul>
                  {sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ?<div>
                  {Retail > 0 ? <div> <li >
                    <label>
                      <Link to="/administration/custom/masterSkechersRetail" onClick={(e) => closeMyDrop(e)} className="menuFavItem">
                        Retail Master
                            </Link>
                    </label>
                  </li> </div> : null}
                  {MBO > 0 ? <div> <li >
                    <label>
                      <Link to="/administration/custom/masterSkechersMBO" onClick={(e) => closeMyDrop(e)} className="menuFavItem">
                        MBO Master
                          </Link>
                    </label>
                  </li></div> : null}
                  {Event_Master > 0 ? <div> <li >
                    <label>
                      <Link to="/administration/custom/masterEvents" onClick={(e) => closeMyDrop(e)} className="customActive">
                        Master Events
                                </Link>
                    </label>
                  </li></div> : null}

                  {FMCG_Master > 0 ?
                    <div>
                      <li >
                        <label>
                          <Link to="/administration/custom/uploadCustomMaster">
                            FMCG MASTER
                                  </Link>
                        </label>
                      </li>
                    </div> : null}
                  {UPLOAD_MASTER > 0 ? <li >
                    <label>
                      <Link to="/administration/custom/uploadCustomMaster" >
                        UPLOAD MASTER
                            </Link>
                    </label>
                  </li> : null}
                </ul>
              </div>
            </span> : null}

            {Role_Master > 0 ? <span className="scheduler">Role Master<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>
              <div className="hovershowDropdown customDropShow">
                <ul>
                  {Add_Role > 0 ? <li >
                    <label>
                      <Link to="/administration/rolesMaster/addRoles" onClick={(e) => closeMyDrop(e)} className="menuFavItem">
                        Add Role
                            </Link>
                    </label>
                  </li> : null}
                  {Manage_Role > 0 ? <li >
                    <label>
                      <Link to="/administration/rolesMaster/manageRoles" onClick={(e) => closeMyDrop(e)} className="menuFavItem">
                        Manager Role
                            </Link>
                    </label>
                  </li> : null}
                </ul>
              </div>
            </span> : null}
          </div> */}
        </div>
      </label>
    );
  }
}

export default AdminDrop;