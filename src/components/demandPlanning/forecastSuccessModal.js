import React from 'react';
import SuccessIcon from '../../assets/success-check.svg';
import RejectIcon from '../../assets/reject.svg';

class ForecastSuccessModal extends React.Component {
  
    render() {
        
        return (
            <div className="save-current-data foreCastSuccessModal">
			<div className="col-md-12 modal-dialog">
				<div className="background-blur"></div>
				<div className="modal-content">
					<div className="modal-data ">
						<div className="modal-header">
                            <div className="icon">
                                { this.props.runStatus != "Failed" ? <img src={SuccessIcon}/> :
                                <img src={RejectIcon}/>}
                            </div>
                            {/* <h2>Processed Successfully !</h2> */}
                            <h2>{this.props.runStatus == "Failed" ? "FAILED" : "SUCCEEDED"}</h2>
                            {/* <p>Last process Successful without any errors</p> */}
                            <p>{this.props.runStatus == "Failed" ? "Last forecast failed due to some error. Please run forecast again." : <p>Forecast generated successfully!<br/> Click Forecast History to download Forecast Report</p>}</p>
                            <div className="modal-footer pad-0">
                                <div className="col-md-6 pad-0"><button type ="button" onClick={(e) => this.props.closeSfModal(e)} className="float_Left confirmBtn">OK</button></div>
                                <div className="col-md-6 pad-0"><button type="button" onClick={(e) => this.props.history.push('/demandPlanning/forecastHistory')} className="dowonload-report-btn">Forecast History</button></div>
                            </div>
					    </div>
					</div>
				</div>
			</div>
	</div>

            )
        }
    }
    
export default ForecastSuccessModal;