import React from "react";

class DemandPlanningFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            status: "",
            type: "",
            no: "" 
        };
    }

    clearFilter(e) {
        this.setState({
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            status: "",
            type: "",
            no: "" 
        })
        document.getElementById('startMonth').placeholder = "Started On";
    }

    onSubmit(e) {
        e.preventDefault();
        let data = {
            demandForecast: this.state.demandForecast,
            frequency: this.state.frequency,
            predictPeriod: this.state.predictPeriod,
            chooseYear: this.state.chooseYear,
            startMonth: this.state.startMonth,
            status: this.state.status,
            search: "",
            type: 2,
            no: 1
        }
        this.props.getForcastRequest(data);
        this.props.closeFilter(e);
        this.props.updateFilter(data)
    }

    handleChange(e){
        if (e.target.id == "frequency") {
            this.setState({
                frequency: e.target.value
            });
        } else if (e.target.id == "predictPeriod") {
            this.setState({
                predictPeriod: e.target.value
            });
        } else if (e.target.id == "startMonth") {
            e.target.placeholder = e.target.value
            this.setState({
                startMonth: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        }
    }

    render() {
        let count = 0;
        if(this.state.demandForecast != ""){
            count ++;
        }
        if(this.state.frequency != ""){
            count ++;
        }if(this.state.predictPeriod != ""){
            count ++;
        }
        if(this.state.chooseYear != ""){
            count ++;
        }
        if(this.state.startMonth != ""){
            count ++;
        }if(this.state.status != ""){
            count ++;
        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS
               
                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    {/* <li>
                                        <input type="text" onChange={(e) => this.setState({demandForecast: e.target.value})} id="demandForecast" value={this.state.demandForecast} placeholder="Demand Forecast" className="organistionFilterModal" />
                                    </li> */}
                                    <li>
                                        <select  onChange={(e) => this.handleChange(e)} id="frequency" value={this.state.frequency} placeholder="Frequency" className="organistionFilterModal" >
                                            <option value="">Select Frequency</option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Monthly">Monthly</option>
                                        </select>
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="predictPeriod" value={this.state.predictPeriod} placeholder="Forecast Period" className="organistionFilterModal" />
                                    </li>
                                    {/* <li>
                                        <input type="text" onChange={(e) => this.setState({ chooseYear : e.target.value})} id="chooseYear" value={this.state.chooseYear} placeholder="Choose Year" className="organistionFilterModal" />
                                    </li> */}
                                    {/* <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} id="startMonth" value={this.state.startMonth} placeholder="Started On" className="organistionFilterModal" />
                                    </li> */}
                                    <li>
                                        <select onChange={(e) => this.handleChange(e)} id="status" value={this.state.status} placeholder="Status" className="organistionFilterModal">
                                            <option value="">Select Status</option>
                                            <option value="FAILED">FAILED</option>
                                            <option value="SUCCEEDED">SUCCEEDED</option>
                                        </select>
                                    </li>
                                    {/* <li>
                                        <select id="status" onChange={(e) => this.setState({status: e.target.value})} value={this.state.status} className="organistionFilterModal">
                                            <option value="">
                                                Status
                                                </option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                        <input type="text" placeholder="s" className="organistionFilterModal" />
                                    </li> */}


                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        <button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                            CLEAR FILTER
                     </button>
                                    </li>
                                    <li>
                                    {this.state.demandForecast != "" || this.state.frequency != "" || this.state.predictPeriod != "" || this.state.chooseYear != "" || this.state.startMonth !="" || this.state.status !="" ? <button type="submit" className="modal_Apply_btn">
                                                APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                    APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default DemandPlanningFilter;
