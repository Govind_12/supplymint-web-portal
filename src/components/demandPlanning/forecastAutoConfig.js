import React, { Component } from 'react';
import runningjobIcon from '../../assets/live-job.svg';

export default class ForecastAutoConfig extends Component {
    render() {

        return (
            <div>
                <div className="container-fluid">
                    <div className="container_div" id="home">

                        <div className="container-fluid">
                            <div className="container_div" id="">

                                <div className="container-fluid pad-0">
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                        <div className="replenishment_container weekelyPlanning monthlyPlanning">
                                            <div className="col-md-12 pad-0">
                                                <div className="col-md-8 col-sm-8 pad-lft-0">
                                                    <ul className="list_style">
                                                        <li>
                                                            <label className="contribution_mart">
                                                                AUTO CONFIGURATION
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <p className="master_para">For Demand Forecasting</p>
                                                        </li>
                                                    </ul>
                                                    <div className="configurationDetails m-top-50">
                                                        <div className="configContainer">
                                                            <div className="top-config">
                                                                <div className="col-md-12">
                                                                    <label className="displayBlock labelGlobal font13 fontWeightNormal">Frequency</label>
                                                                    <ul className="list-inline forecastRadioBtn m-top-5">
                                                                        <li><label className="checkBoxLabel0"> Days<input type="radio" name="days" value="45 days" /><span className="checkmark1"><p>45</p></span></label></li>
                                                                        <li><label className="checkBoxLabel0"> Days<input type="radio" name="days" value="60 days" /><span className="checkmark1"><p>60</p></span></label></li>
                                                                        <li><label className="checkBoxLabel0"> Days<input type="radio" name="days" value="90 days" /><span className="checkmark1"><p>90</p></span></label></li>
                                                                    </ul>
                                                                </div>
                                                                <div className="col-md-12 pad-0 m-top-20 predectionTime">
                                                                    <div className="chooseYear">
                                                                        {/* <div className="col-md-3">
                                                                            <h5>Choose Year</h5>
                                                                            <ul className="pad-0">
                                                                                <li className="m-rgt-0">
                                                                                    <select className="displayPointer width100">
                                                                                        <option>Select Year</option>
                                                                                        <option value="2019">2019</option>
                                                                                        <option value="2020">2020</option>
                                                                                    </select>
                                                                                </li>
                                                                            </ul>
                                                                        </div> */}
                                                                        <div className="col-md-3">
                                                                            <h5>Start Date</h5>
                                                                            <input type="date" className="purchaseOrderTextbox " placeholder="Start Date" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="bottom-config m-top-20">
                                                                <h3>Last Configured Details</h3>
                                                                <ul className="list-inline m-top-15">
                                                                    <li><label>Frequency <span>45Days</span></label></li>
                                                                    <li><label>Year<span>2019</span></label></li>
                                                                    <li><label>Start Date<span>16-04-2019</span></label></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4 col-sm-4 pad-right-0">
                                                    <div className="jobStatusForecast noJobRun">
                                                        <img src={runningjobIcon} /><label>1 Job Running</label>
                                                    </div>
                                                    <div className="configJobDetails">
                                                        <div className="forecastTimer">
                                                            <div className="rightReplenishment autoConfigTimer">
                                                                <div className="replenishmentAnimateDiv">
                                                                    <div className="middleReplensh">
                                                                     <div className="single-chart">
                                                                            <svg viewBox="0 0 36 36" className="circular-chart green">
                                                                            <path className="circle-bg" fill="#fff"
                                                                                d="M18 2.0845
                                                                                a 15.9155 15.9155 0 0 1 0 31.831
                                                                                a 15.9155 15.9155 0 0 1 0 -31.831"
                                                                            />
                                                                            <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
                                                                            <stop offset="0%" stopColor="#4c83ff" />
                                                                            <stop offset="100%" stopColor="#2afadf" />
                                                                            </linearGradient>
                                                                            <path className="circle" stroke="url(#gradient)"
                                                                                strokeDasharray="60, 100"
                                                                                d="M36 20.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
                                                                            />
                                                                            </svg>
                                                                        </div>
                                                                        <div className="autoConfigMid">
                                                                            <div className="autoComfigTime onHoverSvg">
                                                                                <h3>00:00</h3>
                                                                                <p>Mins elapsed</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="bottomReplesh">
                                                                        <label className="updateEngin displayBlock marginAuto">
                                                                            Last Engine Run  
                                                                        </label>
                                                                        <span className="time">37 Mins Ago</span>
                                                                        <label className="schduleTime">
                                                                            <span>Next Trigger scheduled</span>
                                                                        </label>
                                                                        <span className="timeSchedule">
                                                                            Na
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                                <div className="footerDivForm height4per">
                                                    <ul className="list-inline m-lft-0 m-top-10">
                                                        <li><button className="clear_button_vendor" type="reset">Clear</button>
                                                        </li>
                                                        <li><button type="button" className="save_button_vendor" id="saveButton">Save</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
