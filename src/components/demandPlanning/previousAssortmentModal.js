import React, { Component } from 'react'

export default class PreviousAssortmentModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assortmentdata: {},
            financialYear: "CURRENT_FISCAL_YEAR",
            choosePeriod: "6_MONTHS",
            search: "",
            curYear: "",
            prevYear: "",
            type: 1,
            no: 1,
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            totalCount:"",
            startDate : "",
            endDate : "",
            selectAssortId : "",
            checkedData : this.props.checkedData,
            lastRunId:""
        }
    }

    componentDidMount() {
        this.currentPreviousYear()
    }
    componentWillReceiveProps(nextProps) {
        var assortmentdata = {}
        if (nextProps.demandPlanning.getAssortmentHistory.isSuccess == true) {
            if (nextProps.demandPlanning.getAssortmentHistory.data.resource != null) {
                let response = nextProps.demandPlanning.getAssortmentHistory.data.resource
                for (let i = 0; i < response.length; i++) {
                    assortmentdata[Object.keys(response[i])] = Object.values(response[i])
                }
                this.setState({
                    assortmentdata:assortmentdata,
                    maxPage:nextProps.demandPlanning.getAssortmentHistory.data.maxPage,
                    current: nextProps.demandPlanning.getAssortmentHistory.data.currPage,
                    lastRunId:nextProps.demandPlanning.getAssortmentHistory.data.lastRunId
                })
            } else {
                this.setState({
                    assortmentdata: {},
                    loader : false
                })
            }
        }
    }
    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }
    onSearch(e) {
        e.preventDefault();
        if (this.state.search != "") {
            let data = {
                type: 3,
                search: this.state.search,
                no: 1,
                totalCount: 0,
                year: this.state.financialYear,
                frequency: this.props.weeklyMonthly,
                startDate: "",
                endDate: "",
                duration: this.state.choosePeriod
            }
            this.setState({
                type: 3
            })
            this.props.getAssortmentHistoryRequest(data)
        }
    }
    currentPreviousYear() {
        var date = new Date();
        var curYear = date.getFullYear();
        var curr = curYear + '-' + (curYear + 1)
        var prev = (curYear - 1) + '-' + curYear
        this.setState({
            curYear: curr,
            prevYear: prev
        })
    }
    FinancialYear(e) {
     
        
        if (e.target.id == "previousYear") {
            this.setState({
                financialYear: "PREVIOUS_FISCAL_YEAR",
                type: 1
            })
            let data = {
                type: 1,
                search: "",
                no: 1,
                totalCount: 0,
                year: "PREVIOUS_FISCAL_YEAR",
                frequency: this.props.weeklyMonthly,
                startDate: "",
                endDate: "",
                duration: this.state.choosePeriod
            }
            this.props.getAssortmentHistoryRequest(data)
        } else if (e.target.id == "currentYear") {
            this.setState({
                financialYear: "CURRENT_FISCAL_YEAR",
                type: 1
            })
            let data = {
                type: 1,
                search: "",
                no: 1,
                totalCount: 0,
                year: "CURRENT_FISCAL_YEAR",
                frequency: this.props.weeklyMonthly,
                startDate: "",
                endDate: "",
                duration:this.state.choosePeriod
            }
            this.props.getAssortmentHistoryRequest(data)
        }
    }
    ChoosePeriod(months) {

        let data = {
            type: 1,
            search: "",
            no: 1,
            totalCount: 0,
            year: "CURRENT_FISCAL_YEAR",
            frequency: this.props.weeklyMonthly,
            startDate: "",
            endDate: "",
            duration : months
        }
        this.props.getAssortmentHistoryRequest(data)
        if (months == "3_MONTHS") {
            this.setState({
                choosePeriod: "3_MONTHS"
            })
        } else if (months == "6_MONTHS") {
            this.setState({
                choosePeriod: "6_MONTHS"
            })
        } else if (months == "12_MONTHS") {
            this.setState({
                choosePeriod: "12_MONTHS"
            })
        }
    }
    onClearSearch(e) {


        e.preventDefault();
        this.setState({
            type: 1,
            search: ""
        })
        let data = {
            type: 1,
            search: "",
            no: 1,
            totalCount: 0,
            year: "CURRENT_FISCAL_YEAR",
            frequency: this.props.weeklyMonthly,
            startDate: "",
            endDate: "",
            duration : "6_MONTHS"
        }
        this.props.getAssortmentHistoryRequest(data);
    }
    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.demandPlanning.getAssortmentHistory.data.prePage,
                    current: this.props.demandPlanning.getAssortmentHistory.data.currPage,
                    next: this.props.demandPlanning.getAssortmentHistory.data.currPage + 1,
                    maxPage: this.props.demandPlanning.getAssortmentHistory.data.maxPage,
                })
                if (this.props.demandPlanning.getAssortmentHistory.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.demandPlanning.getAssortmentHistory.data.currPage - 1,
                        startDate: this.state.startDate,
                        endDate: this.state.endDate,
                        search: this.state.search,
                        totalCount: this.state.maxPage,
                        frequency: this.props.weeklyMonthly,
                        year: this.state.financialYear,
                        duration:this.state.choosePeriod
                    }
                    this.props.getAssortmentHistoryRequest(data);
                }

            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.demandPlanning.getAssortmentHistory.data.prePage,
                current: this.props.demandPlanning.getAssortmentHistory.data.currPage,
                next: this.props.demandPlanning.getAssortmentHistory.data.currPage + 1,
                maxPage: this.props.demandPlanning.getAssortmentHistory.data.maxPage,
            })
            if (this.props.demandPlanning.getAssortmentHistory.data.currPage != this.props.demandPlanning.getAssortmentHistory.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.demandPlanning.getAssortmentHistory.data.currPage + 1,
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    search: this.state.search,
                    totalCount: this.state.maxPage,
                    frequency: this.props.weeklyMonthly,
                    year: this.state.financialYear,
                    duration: this.state.choosePeriod
                }
                this.props.getAssortmentHistoryRequest(data)
            }
        }else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            }
            else {
                this.setState({
                    prev: this.props.demandPlanning.getAssortmentHistory.data.prePage,
                    current: this.props.demandPlanning.getAssortmentHistory.data.currPage,
                    next: this.props.demandPlanning.getAssortmentHistory.data.currPage + 1,
                    maxPage: this.props.demandPlanning.getAssortmentHistory.data.maxPage,
                })
                if (this.props.demandPlanning.getAssortmentHistory.data.currPage <= this.props.demandPlanning.getAssortmentHistory.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        startDate: this.state.startDate,
                        endDate: this.state.endDate,
                        search : this.state.search,
                        totalCount: this.state.maxPage,
                        frequency: this.props.weeklyMonthly,
                        year : this.state.financialYear,
                        duration : this.state.choosePeriod
                    }
                    this.props.getAssortmentHistoryRequest(data)
                }
            }
        }
    }
    onSelect(data){
        this.setState({
            selectAssortId : data,
            checkedData : ""
        })
    }
    onDone(){
        let data= {
            id:this.state.selectAssortId,
            code : this.state.assortmentdata[this.state.selectAssortId][0].assortmentCode,
            startDate :  this.state.assortmentdata[this.state.selectAssortId][0].startDate,
            endDate : this.state.assortmentdata[this.state.selectAssortId][0].endDate,
            lastRunId:this.state.lastRunId,
            timeAgo: this.state.assortmentdata[this.state.selectAssortId][0].timeAgo
        }
        this.props.selectedAssortment(data)
    }
    render() {
        
        
        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content modalShow pad-0">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle">
                                    <div className="col-md-6 pad-0">
                                        <h2>View Previous Assortment</h2>
                                    </div>
                                    <div className="col-md-6 pad-0">
                                        <div className="modalHandlers">
                                        {this.state.selectAssortId == "" ? <button type="button" className="btnDisabled">Done</button>
                                             :<button type="button" onClick={() => this.onDone()}>Done</button>}
                                            <button type="button" onClick={() => this.props.onClose()}>Close</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="modalOpertions">
                                    <div className="col-md-6 pad-0">
                                        <h4>Choose Financial Year</h4>
                                        <div className="financialYearRadio">
                                            <button type="button" id="previousYear" className={this.state.financialYear == "PREVIOUS_FISCAL_YEAR" ? "unCheck active" : "unCheck"} onClick={(e) => this.FinancialYear(e)}>{this.state.prevYear}</button>
                                            <button type="button" id="currentYear" className={this.state.financialYear == "CURRENT_FISCAL_YEAR" ? "unCheck active" : "unCheck"} onClick={(e) => this.FinancialYear(e)}>{this.state.curYear}</button>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="choosePeriod">
                                            <h4>Choose Period</h4>
                                            <div className="settingDrop dropdown yearDropDown ">
                                                <button className="btn dropdown-toggle userModalSelect" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    {this.state.choosePeriod == "3_MONTHS" ? "Last 3 Month" : this.state.choosePeriod == "6_MONTHS" ? "Last 6 Month" : this.state.choosePeriod == "12_MONTHS" ? "Last 12 Month" : ""}
                                                    <i className="fa fa-chevron-down"></i>
                                                </button>
                                                <ul className="dropdown-menu" aria-labelledby="dropdownMenu1" id="frequency">
                                                    <li onClick={() => this.ChoosePeriod("3_MONTHS")}> <a>Last 3 Month</a></li>
                                                    <li onClick={() => this.ChoosePeriod("6_MONTHS")}> <a>Last 6 Month</a></li>
                                                    <li onClick={() => this.ChoosePeriod("12_MONTHS")}> <a>Last 12 Month</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modalSearch">
                                    <div className="col-md-6"></div>
                                    <div className="col-md-6">
                                        <div className="dropdown searchStoreProfileMain">
                                            <form onSubmit={(e) => { this.onSearch(e) }}>
                                                <input type="search" placeholder="Search..." value={this.state.search} onChange={(e) => this.handleSearch(e)} />
                                                {this.state.type == 3 ? <span onClick={(e) => this.onClearSearch(e)} className="clearSearchFilter">
                                                    Clear Search Filter
                                                </span> : null}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="modalContentMid">
                                    <div className="col-md-12 col-sm-12 pad-0 modalTableNew">
                                        <div className="modal_table">
                                            <table className="table tableModal table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>ACTION</th>
                                                        <th>ASSORTMENT PATTERN</th>
                                                        <th>DATE</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {Object.keys(this.state.assortmentdata).length != 0 ? Object.keys(this.state.assortmentdata).map((data, key) => (

                                                        <tr key={key}>
                                                            <td>
                                                                <label className={this.state.checkedData == data ? "selectedRadio select_modalRadio radioModalCheckBox" : "select_modalRadio radioModalCheckBox"}>
                                                                    <input type="radio" name="divisionRadio" id={data}  onChange={(e)=>this.onSelect(data)}/>
                                                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                                                </label>
                                                            </td>
                                                            <td>{this.state.assortmentdata[data][0].assortmentCode}</td>
                                                            <td>{this.state.assortmentdata[data][0].createdOn}</td>
                                                        </tr>
                                                    )) : <tr className="modalTableNoData" ><td colSpan="4">No Data Found</td></tr>}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div className="modalBottom">
                                    {/* <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            <li>
                                                <button className="PageFirstBtn" type="button">First</button>
                                            </li>
                                            <li >
                                                <button className="PageFirstBtn" type="button" id="prev">Prev</button>
                                            </li>
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button"><span>01</span></button>
                                            </li>
                                            <li>
                                                <button className="PageFirstBtn borderNone" type="button" id="next">Next</button>
                                            </li>
                                        </ul>
                                    </div> */}
                                    <div className="pagerDiv">
                                        <ul className="list-inline pagination">
                                            <li >
                                                <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                                    First
                                     </button>
                                            </li>

                                            <li>
                                                <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li>
                                            {/* {this.state.prev != 0 ? <li >
                                <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                    Prev
                  </button>
                            </li> : <li >
                                    <button className="PageFirstBtn" disabled>
                                        Prev
                  </button>
                                </li>} */}
                                            <li>
                                                <button className="PageFirstBtn pointerNone">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                                <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" disabled>
                                                        Next
                  </button>
                                                </li>}


                                            {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}
