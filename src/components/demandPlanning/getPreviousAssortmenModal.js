import React from "react";
class GetPreviousAssortmentModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
            assortmentData: [],
            selected: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: 1,
            search: "",
            no: 1,
        }

    }

    componentWillMount() {
        this.setState({
            selected: this.props.assortment
        })
        // if(this.props.demandPlanning.getPreviousAssortment.isSuccess){
        //     this.setState({
        //         assortmentData: this.props.demandPlanning.getPreviousAssortment.data.resource,
        //         prev: this.props.demandPlanning.getPreviousAssortment.data.prePage,
        //         current: this.props.demandPlanning.getPreviousAssortment.data.currPage,
        //         next: this.props.demandPlanning.getPreviousAssortment.data.currPage + 1,
        //         maxPage: this.props.demandPlanning.getPreviousAssortment.data.maxPage,
        //         selected: this.props.assortment
        //     })
        // }  
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.demandPlanning.getPreviousAssortment.isSuccess) {
            this.setState({
                assortmentData: nextProps.demandPlanning.getPreviousAssortment.data.resource,
                prev: nextProps.demandPlanning.getPreviousAssortment.data.prePage,
                current: nextProps.demandPlanning.getPreviousAssortment.data.currPage,
                next: nextProps.demandPlanning.getPreviousAssortment.data.currPage + 1,
                maxPage: nextProps.demandPlanning.getPreviousAssortment.data.maxPage
            })
        }
    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.demandPlanning.getPreviousAssortment.data.prePage,
                current: this.props.demandPlanning.getPreviousAssortment.data.currPage,
                next: this.props.demandPlanning.getPreviousAssortment.data.currPage + 1,
                maxPage: this.props.demandPlanning.getPreviousAssortment.data.maxPage,
            })
            if (this.props.demandPlanning.getPreviousAssortment.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    search: this.state.search,
                    no: this.props.demandPlanning.getPreviousAssortment.data.currPage - 1,
                    totalCount: this.state.maxPage,
                    runId: this.props.selectedAssortment,
                    assortmentCode: "NA",
                    startDate: "",
                    endDate: "",
                }
                this.props.getPreviousAssortmentRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.demandPlanning.getPreviousAssortment.data.prePage,
                current: this.props.demandPlanning.getPreviousAssortment.data.currPage,
                next: this.props.demandPlanning.getPreviousAssortment.data.currPage + 1,
                maxPage: this.props.demandPlanning.getPreviousAssortment.data.maxPage,
            })
            if (this.props.demandPlanning.getPreviousAssortment.data.currPage != this.props.demandPlanning.getPreviousAssortment.data.maxPage) {
                let data = {
                    type: this.state.type,
                    search: this.state.search,
                    no: this.props.demandPlanning.getPreviousAssortment.data.currPage + 1,
                    totalCount: this.state.maxPage,
                    runId: this.props.selectedAssortment,
                    assortmentCode: "NA",
                    startDate: "",
                    endDate: "",
                }
                this.props.getPreviousAssortmentRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.demandPlanning.getPreviousAssortment.data.prePage,
                current: this.props.demandPlanning.getPreviousAssortment.data.currPage,
                next: this.props.demandPlanning.getPreviousAssortment.data.currPage + 1,
                maxPage: this.props.demandPlanning.getPreviousAssortment.data.maxPage,
            })
            if (this.props.demandPlanning.getPreviousAssortment.data.currPage <= this.props.demandPlanning.getPreviousAssortment.data.maxPage) {
                let data = {
                    type: this.state.type,
                    search: this.state.search,
                    no: 1,
                    totalCount: this.state.maxPage,
                    runId: this.props.selectedAssortment,
                    assortmentCode: "NA",
                    startDate: "",
                    endDate: "",
                }
                this.props.getPreviousAssortmentRequest(data)
            }

        }
    }

    selectedData(data) {
        this.setState({ selected: data })
    }
    ondone(e) {
        this.props.openAssortment();
        this.props.updateAssortment(this.state.selected)
    }


    onSearch(e) {
        e.preventDefault();
        this.setState({
            type: 3
        })
        let data = {
            runId: this.props.selectedAssortment,
            totalCount: 0,
            assortmentCode: "NA",
            startDate: "",
            endDate: "",
            type: 3,
            search: this.state.search,
            no: 1
        }
        this.props.getPreviousAssortmentRequest(data)
    }

    onClear() {
        this.setState({
            search: "",
            type: 1,
            no: 1
        })
        let data = {
            type: 1,
            search: "",
            no: 1,
            totalCount: 0,
            runId: this.props.selectedAssortment,
            assortmentCode: "NA",
            startDate: "",
            endDate: "",
        }
        this.props.getPreviousAssortmentRequest(data);
    }

    render() {

        return (
            <div className="modal display_block" id="pocolorModel">
                <div className="backdrop display_block"></div>
                <div className="modal_Indent display_block">
                    <div className="modal-content modalpoColor modalShow">
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color selectVendorPopUp supplierSelectModal">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT ASSORTMENT</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only single entry from below records</p>
                                        </li>
                                    </ul>
                                    <ul className="list-inline width_100 m-top-10">
                                        <form onSubmit={(e) => this.onSearch(e)}>
                                            <div className="col-md-7 col-sm-7 pad-0">
                                                <div className="mrpSelectCode">
                                                    <li>
                                                        <input type="text" className="search-box" onChange={(e) => this.setState({ search: e.target.value })} value={this.state.search} placeholder="Type to search" />


                                                        <label className="m-lft-15">
                                                            <button type="submit" className="findButton">FIND
                                <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                    <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                                </svg>
                                                            </button>
                                                        </label>
                                                    </li>
                                                </div>
                                            </div>
                                            <li className="float_right">

                                                <label>
                                                    <button type="button" className="clearbutton" onClick={(e) => this.onClear()}>CLEAR</button>
                                                </label>
                                            </li>
                                        </form>
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {this.state.assortmentData == undefined || this.state.assortmentData.length == 0 ? <tr className="modalTableNoData"><td colSpan="3"> NO DATA FOUND </td></tr> : this.state.assortmentData.length == 0 ? <tr className="modalTableNoData"><td colSpan="3"> NO DATA FOUND </td></tr> : this.state.assortmentData.map((data, key) => (
                                                    <tr key={key}>
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input id={data} type="radio" name="supplier" checked={this.state.selected == data} onChange={() => this.selectedData(`${data}`)} />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" onClick={(e) => this.ondone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" onClick={(e) => this.props.openAssortment(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 ? <li >
                                                <button className="PageFirstBtn" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.next - 1 != this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}

export default GetPreviousAssortmentModal;
