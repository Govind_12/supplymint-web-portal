import React from 'react';

class UpdatePlanModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            filter: false,
            dataFilter: false,
            type: 1,
            no: 1,
            search: "",
            loader: true,
            status: "",
            itemCount: "",
            createdOn: "",
            alert: false,
            code: "",
            errorMessage: "",
            errorCode: "",
            activePlanTrue:[],
            percent:"",
            percenterr:false,
            
        };
    }

    componentWillMount(){
        let activePlanTrue =[]
        let activePlan = this.props.activePlan
        for(var i=0 ;i<activePlan.length;i++){
            if(activePlan[i].status=="TRUE"){
                activePlanTrue.push(activePlan[i])
                this.setState({
                    activePlanTrue:activePlanTrue
                })
            }
           
        } 

    }
    percent()
{
    if (
        this.state.percent == "")
       {
        this.setState({
            percenterr: true
        });
      } else {
        this.setState({
            percenterr: false
        });
      }  
}
    handleChange(e){
      if (e.target.id == "percent") {
           if(e.target.validity.valid){
        this.setState(
       {
         percent: e.target.value
       },
       () => {
         this.percent();
       }
     );
    }
      }}


      updatePlan(){
this.percent();
const t= this
setTimeout(function(){
    if(!t.state.percenterr){
     let payload={
        planId: t.state.activePlanTrue[0].planId,
        planName:t.state.activePlanTrue[0].planName,
        isUfAsd:t.props.updateData=="isUfAsd"?"TRUE":"FALSE",
        isUfDfd:t.props.updateData=="isUfDfd"?"TRUE":"FALSE",
        increasePercentageAsd:t.props.updateData=="isUfAsd"?t.state.percent:"",
        increasePercentageDfd:t.props.updateData=="isUfDfd"?t.state.percent:"",
        activeDataOption:t.state.activePlanTrue[0].activeDataOption,
        isUserOTBPlan:"TRUE"
     }
t.props.updatePlanRequest(payload)
t.props.closeUpdatePlan()
    }

},10)



      }
    render() {
        const {percent,percenterr}=this.state
        return (
            <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
     
            <div className=" display_block">
                <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain updatePlanModalMain pad-0">
                    <div className="col-md-12 pad-0">   
                        <div className="col-md-9 modalLeft">
                            
                            <h4>Update Plan <span className="planName">{this.state.activePlanTrue.length!=0?this.state.activePlanTrue[0].planName:null}</span></h4>
                            <div className="changeData m-top-30">
                            {/* First */}
                                {/* <input type="text" placeholder="Enter Plan Name"/> */}
                            {/* first End */}
                            {/* Second */}
                            {/* <label>Chooose Update option</label>
                            <button type="button">Update From Last year Data</button>
                            <button type="button">Update from Sales forecasted Data</button> */}

                            {/* Second End */}
                            {/* Third */}
                                <label>The percentage increase from the Past year (e.g. 30)</label>
                                <input type="text" value={percent} pattern="[0-9]+([\.][0-9]{0,2})?" maxLength="5" onChange={(e) => this.handleChange(e)}  id="percent"/>
                                {percenterr ? (
                          <span className="error">
                            Enter  percentage
                          </span>
                        ) : null}
                            {/* Third End */}
                            </div>
                            {/* <div className="emptyDiv"></div> */}
                            {/* <span className="glyphicon glyphicon-chevron-right"></span> */}
                            <div className="col-md-12 modalFooter pad-lft-0">
                                <button className="nxtBtn" type="button" onClick={(e)=>this.updatePlan(e)}>Update </button>
                                <button className="clear_button_vendor" type="button" onClick={(e) => this.props.closeUpdatePlan(e)}>Cancel</button>
                            </div>
                        </div>
                
                    </div>  
                           
                    
                    
                    

                
                    
                </div>
            
      
        </div>
        </div>

            )
        }
    }
    
export default UpdatePlanModal;