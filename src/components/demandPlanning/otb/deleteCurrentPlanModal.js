import React from 'react';
import deleteIcon from '../../../assets/deleteCurrentPlan.svg';

class DeleteCurrentPlanModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activePlanTrue :[],
            status: "",
            delete:false
        }
    }

componentWillMount(){
    let activePlanTrue =[]
    let activePlan = this.props.activePlan
   
    for(var i=0 ;i<activePlan.length;i++){
        if(activePlan[i].status=="TRUE"){
            activePlanTrue.push(activePlan[i])
            this.setState({
                activePlanTrue:activePlanTrue
            })
        }
           
    }
}

    removePlan(){
        let payload={
            planId: this.state.activePlanTrue[0].planId,
            planName:this.state.activePlanTrue[0].planName,
        }
    
        this.props.removePlanRequest(payload)
        this.props.closeDeletePlan()
       
    }


    render() {
        return (
            <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
     
            <div className=" display_block">
                <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain sessionExpireModalMain deleteCurrentPlanModal pad-0">
                    <div className="col-md-12 pad-0 m-top-30">                                                
                         <div className="save-current-data modalPos">
                                <div className="col-md-12 m0">
                                    {/* <div className="background-blur"></div> */}
                                            <div className="modal-header">
                                                <div className="icon">
                                                <img src={deleteIcon}/>
                                                </div>
                                                <h2>Are you sure you want to Delete current Plan ?</h2>
                                                <p>Confirming this action will delete your current plan. <br/ >
                                                This process cannot be undone.</p>
                                                </div>
                                            <div className="modal-footer">
                                                <button className="close-btn" type ="button" onClick={(e)=>this.props.closeDeletePlan(e)}>Cancel</button>
                                                <button className="confirm-btn" type="button" onClick={(e)=>this.removePlan(e)}>Confirm</button>
                                            </div>
                                            </div> 
                        </div>
                    </div>  
                </div>  
      
            </div>
        </div>

            )
        }
    }
    
export default DeleteCurrentPlanModal;