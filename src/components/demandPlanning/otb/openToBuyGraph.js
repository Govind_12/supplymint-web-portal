import React from 'react';
import {BarChart,ResponsiveContainer, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';


class OpenToBuyGraph extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openToBuyData: this.props.openToBuyData,
    };    
  }
  openCreatePlan(e){
    this.setState({
        createPlan: !this.state.createPlan,
    })
}
   
   render(){


     
   //  let data = "26px"
        return (
          <div className="otbGraph" onMouseOver={() => { this.props.toolTipLabel() }}>
             {/* <div className="upRowGraph">
                <span style={{width: `${data}`}}></span>
                <span style={{width: "56px"}}></span>
                <span style={{width: "52px"}}></span>
             </div>
             <div className="downRowGraph">
             <span style={{width: "41px"}}></span>
                <span style={{width: "41px"}}></span>
                <span style={{width: "52px"}}></span>
             </div> */}
             <ResponsiveContainer>
             <BarChart
                  width={1500}
                  height={150}  
                  
                  data={this.props.openToBuyData}
                  barGap={6}
                  barCategoryGap={6}
                  margin={{
                     top: 20, right: 30, left: 20, bottom: 5,
                  }}
                 
                  >
                  {/* <CartesianGrid strokeDasharray="3 3" /> */}
                  <defs>
   
          <linearGradient id="closingInventory" x1="0" y1="0" x2="0" y2="1">
      				<stop offset="5%" stopColor="#81fbb8" stopOpacity={1}/>
      				<stop offset="95%" stopColor="#28c76f" stopOpacity={0.8}/>
    			</linearGradient>
    			<linearGradient id="openingStock" x1="0" y1="0" x2="0" y2="1">
      				<stop offset="5%" stopColor="#fff6b7" stopOpacity={1}/>
      				<stop offset="95%" stopColor="#f6416c" stopOpacity={0.8}/>
    			</linearGradient>
          <linearGradient id="openPurchaseOrder" x1="0" y1="0" x2="0" y2="1">
      				<stop offset="5%" stopColor="#ee9ae5" stopOpacity={1}/>
      				<stop offset="95%" stopColor="#5961f9" stopOpacity={0.8}/>
    			</linearGradient>
    			<linearGradient id="otbValue" x1="0" y1="0" x2="0" y2="1">
      				<stop offset="5%" stopColor="#90f7ec" stopOpacity={1}/>
      				<stop offset="95%" stopColor="#32ccbc" stopOpacity={0.8}/>
    			</linearGradient>
          <linearGradient id="planSales" x1="0" y1="0" x2="0" y2="1">
      				<stop offset="5%" stopColor="#f761a1" stopOpacity={1}/>
      				<stop offset="95%" stopColor="#8c1bab" stopOpacity={0.8}/>
    			</linearGradient>
    			<linearGradient id="markDown" x1="0" y1="0" x2="0" y2="1">
      				<stop offset="5%" stopColor="#eead92" stopOpacity={1}/>
      				<stop offset="95%" stopColor="#6018dc" stopOpacity={0.8}/>
    			</linearGradient>
 			  </defs>
                  <XAxis dataKey={this.props.frequency =="Quarterly"?"quarter":"billDate"} />
                  <YAxis  />
            
                  <Tooltip cursor={{fill: 'transparent'}}/>
                  {/* <Legend /> */}
                 <Bar  dataKey="planSales" fill="url(#planSales)" barSize={4} />
                  <Bar  dataKey="markDown" fill="url(#markDown)"  barSize={4} /> 
                  <Bar  dataKey="closingInventory" fill="url(#closingInventory)"  barSize={4} />
                  <Bar  dataKey="openingStock" fill="url(#openingStock)"  barSize={4} />
                  <Bar  dataKey="openPurchaseOrder" fill="url(#openPurchaseOrder)"  barSize={4}/>
                  <Bar  dataKey="otbValue" fill="url(#otbValue)" barSize={4} />
            </BarChart>
            </ResponsiveContainer>
          </div>

        );
    }
}

export default OpenToBuyGraph;