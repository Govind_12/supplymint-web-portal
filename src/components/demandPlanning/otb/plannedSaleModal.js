import React from 'react'

class ConfirmPlannedSale extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            plannedData: {}

        }
    }
    componentDidMount() {
        let data = {
            billDateState: this.props.billDateState,
            assortmentCodeState: this.props.assortmentCodeState,
            focusValue: this.props.focusValue,
            valueState: this.props.valueState,
            markDown: this.props.markDown,
            closingInventory: this.props.closingInventory,
            openingStock: this.props.openingStock,
            openPurchaseOrder: this.props.openPurchaseOrder,
            otbValue: this.props.otbValue,
        }
        this.setState({
            plannedData: data
        })
    }

    render() {
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>

                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal deleteCurrentPlanModal plannedSaleConfirmation pad-0">
                        <div className="col-md-12 pad-0">
                            <div className="save-current-data modalPos">
                                <div className="col-md-12 m0">
                                    <div className="modal-header">
                                        <div className="icon">
                                        </div>
                                        <h2>Are you sure you want to update the planned Sales?</h2>
                                    </div>
                                    <div className="modal-footer">
                                        <button className="close-btn" type="button" onClick={(e) => this.props.saleConfirmation(this.state.plannedData)}>Cancel</button>
                                        <button className="confirm-btn" type="button" onClick={(e) => this.props.onConfirmModal(this.state.plannedData)}>Confirm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }

}
export default ConfirmPlannedSale; 