import React from 'react';
import { getDate } from '../../../helper';
// import moment from 'moment';
class CustomDateModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startMonth:"",
            startMontherr:false,
            endMonth:"",
            endMontherr:false,
            startDate: "",
            minDate:"",
            maxDate:"",
            activePlanTrue:[],
            allMonths:[],
            rMonths:[]
        }}
        handleChange(e){
            if(e.target.id=="startMonth"){
                this.setState({
                    startMonth:e.target.value,
                    endMonth:""
                },
                () => {
                    this.startMonth();
                }
            );

            if(e.target.value!=""){
                this.remainingValues(e.target.value)
            }             
            
            }else if(e.target.id=="endMonth"){

                this.setState({
                    endMonth:e.target.value
                },
                () => {
                    this.endMonth();
                });
            }
        }

        startMonth() {
            if (this.state.startMonth == "") {
                this.setState({
                    startMontherr: true
                });
            } else {
                this.setState({
                    startMontherr: false
                });
            }
        }

        endMonth() {
            if (this.state.endMonth == "" ) {
                this.setState({
                    endMontherr: true
                });
            } else {
                this.setState({
                    endMontherr: false
                });
            }
        }

        onSubmit(){
         this.startMonth();
         this.endMonth();
         const t = this
         setTimeout(function(){
         if(!t.state.startMontherr && !t.state.endMontherr){
             let date={
                 startDate :t.state.startMonth,
                 endDate: t.state.endMonth
             }
            t.props.updateFrequency(date)
            t.props.closeCustomDate()
         }
        },10)
        }

        componentDidMount(){
        //     var activePlan=this.props.activePlan
        //     var activePlanTrue=""
        //     if(activePlan!=null){
        //     for(var i=0 ;i<activePlan.length;i++){
        //       if(activePlan[i].status=="TRUE"){
        //           activePlanTrue=moment(activePlan[i].createdOn).format("YYYY-MM-DD")
                
        //           this.setState({
        //               minDate : activePlanTrue,
        //           })
        //       }
        //   }  
        
        //     var d = new Date(activePlanTrue);
        //     var year = d.getFullYear();
        //     var month = d.getMonth();
        //     var day = d.getDate();
        //     var c = moment(new Date(year + 1, month, day)).format("YYYY-MM-DD")
        //     this.setState({
        //         maxDate :c
        //     })
        //   }
          this.addvalues()
         
        }
 

        addvalues(){
            var monthNames = new Array();
        
            monthNames[0] = "January";
            monthNames[1] = "February";
            monthNames[2] = "March";
            monthNames[3] = "April";
            monthNames[4] = "May";
            monthNames[5] = "June";
            monthNames[6] = "July";
            monthNames[7] = "August";
            monthNames[8] = "September";
            monthNames[9] = "October";
            monthNames[10] = "November";
            monthNames[11] = "December";
            var months = new Array();
            var today = new Date(Date());
    
            var loopDate = new Date();
            loopDate.setTime(today.valueOf());
    
            var todayPlus12Months = new Date(today.setMonth(today.getMonth() + 12));
    
            while (loopDate.valueOf() < todayPlus12Months.valueOf()) {
      
                var month = monthNames[loopDate.getMonth()];
    
    
                months.push(month + ' - ' + loopDate.getFullYear());
                loopDate.setMonth(loopDate.getMonth() + 1);
            }
          
              this.setState({
                  allMonths:months
              })
        }

        remainingValues(value){
            let allMonths = this.state.allMonths
            let index = 0
            let remainingValues =[]
            for(let i=0;i<allMonths.length;i++){
                if(allMonths[i]==value){
                    index = i 
                }
            }
            for(let j=0;j<allMonths.length;j++){
                if(j>=index){
                    remainingValues.push(allMonths[j])
                }
            }
        
            this.setState({
               rMonths:remainingValues
            })

        }

    render() {
    
        const {startMonth,startMontherr,endMonth,endMontherr}= this.state
        return (
        <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
            <div className=" display_block">
                <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain deleteCurrentPlanModal customDateMain pad-0">
                    <div className="col-md-12 pad-0 m-top-20">                                                
                        <div className="save-current-data modalPos">
                            <div className="col-md-12 m0">
                                <div className="modal-header">                                            
                                    <h2>Custom Month</h2>
                                </div>
                                <div className="col-md-12 customDate m-top-10">
                                    <div className="col-md-5 pad-lft-0">
                                        <label className="purchaseLabel">
                                            From
                                        </label>
                                        {/* <input type="date" className= "purchaseOrderTextbox" min={this.state.minDate} max={this.state.maxDate} value={startDate} id ="startDate" onChange={(e)=>this.handleChange(e)} placeholder={startDate==""?"YYYY-MM-DD":startDate}/> */}
                                        <select
                                                        name="startMonth"
                                                        id="startMonth"
                                                        className={startMontherr ? "errorBorder orgnisationTextbox displayPointer" : "orgnisationTextbox displayPointer"}
                                                        onChange={e => this.handleChange(e)} 
                                                        value={startMonth}
                                                    >
                                                    <option value="">Select start month</option>
                                                    {this.state.allMonths.map((data, key) => (
                                                           <option value={data} key={key}>{data}</option>
                                                     ) )}
                                                      
                                                        
                                                    </select>
                                        {startMontherr ? (
                                            <span className="error">
                                                Enter Start Month
                                            </span>
                                        ) : null}
                                    </div>
                                    <div className="col-md-5 pad-lft-0">
                                        <label className="purchaseLabel">
                                            To
                                        </label>
                                       {this.state.startMonth!=""? <select name="endMonth"
                                                        id="endMonth"
                                                        className={endMontherr ? "errorBorder orgnisationTextbox displayPointer" : "orgnisationTextbox displayPointer"}
                                                        onChange={e => this.handleChange(e)}
                                                        value={endMonth}
                                                    >
                                                          <option value="">Select end month</option>
                                                    {this.state.rMonths.map((data, akey) => (
                                                           <option value={data} key ={akey}>{data}</option>
                                                     ) )}
                                                      
                                                        
                                                    </select>:<select name="endMonth"
                                                        id="endMonth"
                                                        className= "orgnisationTextbox displayPointer btnDisabled"
                                                        
                                                        value={endMonth}
                                                    >
                                                          <option value="">Select end month</option>
                                                   
                                                      
                                                        
                                                    </select>}
                                        {endMontherr ? (
                                            <span className="error">
                                                Enter End Month
                                            </span>
                                        ) : null}
                                    </div>      
                                </div>
                                <div className="modal-footer">
                                    <button className="close-btn"type ="button" onClick={(e)=>this.props.closeCustomDate(e)}>Cancel</button>
                                    <button className="confirm-btn" type="button" onClick={(e)=>this.onSubmit(e)}>Submit</button>
                                </div>
                            </div> 
                        </div>
                    </div>  
                </div>
            </div>
        </div>

            )
        }
    }
    
export default CustomDateModal;