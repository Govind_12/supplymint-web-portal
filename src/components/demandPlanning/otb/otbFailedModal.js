import React from 'react';
import failedIcon from '../../../assets/failed-circle.svg';
import successIcon from '../../../assets/ok.svg';
import circleIcon from "../../../assets/circleLoader.svg";
class OtbFailedModal extends React.Component {
    render() {
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>
                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal otbFailedMain displayFlex">
                        {this.props.statusFailed ? <div className="col-md-12 pad-0 text-center midContent">
                            <img src={failedIcon} />
                            <h3>Your OTB Plan Creation Failed due to some Error</h3>
                            <label>Please Try Again</label>
                            <button type="button" className="closeBtn" onClick={(e)=> this.props.closeOtbFailed(e)}>Close</button>
                        </div>: null}
                        {!this.props.noPlanLoader ? <div className="col-md-12 pad-0 text-center midContent">
                            <img src={successIcon} />
                            <h3>Your OTB Plan Created successfully !</h3>
                            <button type="button" className="closeBtn m-top-35" onClick={(e)=> this.props.closeOtbFailed(e)}>Done</button>
                        </div>:null}
                        {this.props.noPlanLoader ? <div className="col-md-12 pad-0 text-center midContent">
                            <h3>Your OTB Plan Creation is in under process !</h3>
                            <div className="col-md-4 text-center circleLoader m-top-25">
                                <p>{this.props.planPercentage} Completed</p>
                                <img className="rotateAnimation width-17 m-top-15" src={circleIcon} />
                            </div>
                        </div>:null}
                    </div>
                </div>
            </div>
        )
    }
}

export default OtbFailedModal;