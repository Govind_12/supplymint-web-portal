import React from "react";
import NoPlanIcon from '../../../assets/planning.svg'
import CreateNewPlanModal from "./createNewPlanModal";
class NoPlanExist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      createPlan: false,
    };    
  }
  openCreatePlan(e){
    this.setState({
        createPlan: !this.state.createPlan,
    })
}

  render() {
    return (
      <div className="container_div" id="">
        <div className="col-md-12 col-sm-12 col-xs-12">
          <div className="container_content height-70vh noPlanExist">
          <label className="contribution_mart"> OPEN TO BUY </label>
            <div className="middleContent">
                <img src={NoPlanIcon} />
                    <h2>No existing Plan Found</h2>
                    <button className="record_btn" onClick={(e) => this.openCreatePlan(e)}>
                      Create New Plan
                    </button>
            </div>
          </div>
        </div>
        {this.state.createPlan ? <CreateNewPlanModal {...this.props} closePlanModal={(e)=>this.openCreatePlan(e)}/> :null}
      </div>
    );
  }
}

export default NoPlanExist;
