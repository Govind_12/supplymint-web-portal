import React from 'react';
import FilterLoader from '../../loaders/filterLoader';
import RequestSuccess from "../../loaders/requestSuccess";
import NoPlanIcon from '../../../assets/planning.svg'
import RequestError from "../../loaders/requestError";
import _ from 'lodash';
import { getForecastStart, getForecastEnd } from '../../../helper/index';
import OpenToBuyGraph from '../../demandPlanning/otb/openToBuyGraph';
import exportBtn from '../../../assets/export.svg';
import planIcon from '../../../assets/event.svg';
import openArrow from '../../../assets/open-arrow.svg';
import CreateNewPlanModal from '../../demandPlanning/otb/createNewPlanModal';
import newPlanIcon from '../../../assets/newPlan.svg';
import copyIcon from '../../../assets/copy.svg';
import deleteIcon from '../../../assets/deleteotb.svg';
import settingIcon from '../../../assets/settings.svg';
import DeleteCurrentPlanModal from '../../demandPlanning/otb/deleteCurrentPlanModal';
import CustomDateModal from '../../demandPlanning/otb/customDateOtbModal';
import UpdatePlanModal from '../../demandPlanning/otb/updatePlanModal';
import ToastLoader from '../../loaders/toastLoader';
import moment from 'moment';
import axios from 'axios';
import { CONFIG } from "../../../config/index";
import TotalOtbGraph from "./totalOtbGraph";
import ConfirmPlannedSale from './plannedSaleModal';
import loader from '../../../assets/Loader.svg';
import leftScroll from '../../../assets/scroll-left.svg';
import rightScroll from '../../../assets/scroll-right.svg';
import ProcessLoader from '../../loaders/processLoader';
import OtbFailedModal from './failedOtbModal';

class Otb extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: JSON.parse(sessionStorage.getItem('weeklyGraph')),
            blue: true,
            green: true,
            purple: true,
            storeData: [],
            assortmentData: [],
            weeklyData: [],
            alert: false,
            loader: false,
            success: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            store: "",
            assortment: "",
            weekly: "",
            storeSearch: "",
            assortmentSearch: "",
            assortmentModal: false,
            assortmentModalAnimation: false,
            weeklyerr: false,
            assortmenterr: false,
            storeshow: false,
            startDate: "",
            endDate: "",
            startmin: getForecastStart(),
            endmax: getForecastEnd(),
            startDateerr: false,
            endDateerr: false,
            weeklyshow: false,
            chooseStoreerr: false,
            active: "graph",
            tableData: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            no: 1,
            type: 1,
            search: "",
            durationst: "",
            durationend: "",
            assCode: "",
            dev: true,
            downloadUrl: "",
            createPlan: false,
            deletePlan: false,
            customDate: false,
            updatePlan: false,
            otbState: "Retail",
            activePlan: this.props.activePlan,
            updateData: "",
            activePlanTrue: [],
            toastLoader: false,
            toastMsg: "",
            makeCopy: "",
            frequency: "",
            frequencyData: [{ frequency: "Next 3 Months" }, { frequency: "Next 6 Months" }, { frequency: "Next 12 Months" }, { frequency: "Quarterly" }, { frequency: "Custom Month Range" }],
            otbPlanData: [],
            assortmentCode: [],
            totalOtb: [],
            currPageno: "",
            disableActivePlan: false,
            activeAssormentCode: "",
            maxPageno: "",
            totalOtbCondition: true,
            noMoreData: false,
            scrollLoader: false,
            focusValue: "",
            planSaleModal: false,
            billDateState: "",
            assortmentCodeState: "",
            valueState: "",
            noPlanLoader: this.props.noPlanLoader,
            THREE_MONTHS: false,
            QUARTERLY: false,
            TWELVE_MONTHS: false,
            SIX_MONTHS: false,
            CUSTOM: false
        }
    }
    onFrequencyChange(frequency) {
        if (frequency != this.state.frequency) {
            if (frequency == "Custom Month Range") {
                this.setState({
                    customDate: !this.state.customDate,
                })
            }
            let freq = []
            let value = ""
            if (frequency == "Quarterly") {
                value = "quarterly"
            } else {
                freq = frequency.split(" ");
                value = freq[1] + "_" + freq[2]
            }
            const t = this
            if (frequency == "Next 6 Months" || frequency == "Next 3 Months" || frequency == "Next 12 Months" || frequency == "Quarterly") {
                this.setState({
                    frequency: frequency,
                    totalOtb: [],
                    otbPlanData: [],
                    assortmentCode: [],
                    currPageno: 0,
                    noMoreData: false
                })
                setTimeout(function () {
                    let activePlan = t.props.activePlan
                    var activePlanTrue = []
                    if (activePlan != null) {
                        for (var i = 0; i < activePlan.length; i++) {
                            if (activePlan[i].status == "TRUE") {
                                activePlanTrue.push(activePlan[i])
                                t.setState({
                                    activePlanTrue: activePlanTrue
                                })
                                let data = {
                                    no: 1,
                                    activePlan: activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD",
                                    frequency: value,
                                    startDate: "",
                                    endDate: "",
                                    planId: activePlanTrue[0].planId,
                                }
                                t.props.getOtbPlanRequest(data)
                                t.props.totalOtbRequest(data)
                            }
                        }
                    }
                }, 10)
            }
        }
    }
    numDifferentiation(val) {
        if (val >= 10000000) val = (val / 10000000).toFixed(2) + ' Cr';
        else if (val >= 100000) val = (val / 100000).toFixed(2) + ' Lac';
        else if (val >= 1000) val = (val / 1000).toFixed(2) + ' K';
        return val;
    }
    commaSeparator(val) {
        var nf = new Intl.NumberFormat();
        return nf.format(val)
    }
    updateFrequency(date) {
        this.setState({
            startDate: date.startDate,
            endDate: date.endDate,
            frequency: date.startDate + "-" + date.endDate,
            totalOtb: [],
            otbPlanData: [],
            assortmentCode: [],
            currPageno: 0,
            noMoreData: false
        })
        let activePlan = this.props.activePlan
        var activePlanTrue = []
        if (activePlan != null) {
            for (var i = 0; i < activePlan.length; i++) {
                if (activePlan[i].status == "TRUE") {
                    activePlanTrue.push(activePlan[i])
                    this.setState({
                        activePlanTrue: activePlanTrue
                    })
                    let data = {
                        no: 1,
                        activePlan: activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD",
                        frequency: "NA",
                        startDate: date.startDate,
                        endDate: date.endDate,
                        planId: activePlanTrue[0].planId,
                    }
                    this.props.getOtbPlanRequest(data)
                    this.props.totalOtbRequest(data)
                }
            }
        }
    }
    openCreatePlan(plan) {
        this.setState({
            makeCopy: plan,
            createPlan: !this.state.createPlan,
        })
    }
    openDeletePlan(e) {
        this.setState({
            deletePlan: !this.state.deletePlan,
        })
    }
    openCustomDate(e) {
        this.setState({
            customDate: !this.state.customDate,
        })
    }
    openUpdatePlan(plan) {
        this.setState({
            updateData: plan,
            updatePlan: !this.state.updatePlan,
        })
    }
    hidePanel(id) {
        this.tableWidth();
        if (this.state.activeAssormentCode != id) {
            this.setState({
                activeAssormentCode: id,
                totalOtbCondition: false
            })
        } else {
            this.setState({
                activeAssormentCode: ""
            })
        }
        this.showBtn();
    }
    otbToggle() {
        this.setState({
            otbState: this.state.otbState == "Retail" ? "Cost" : "Retail"
        })
    }
    // ________________________________________UPDATE ACTIVE PLAN______________________________________
    updateActivePlan(name, id) {
        const t = this
        setTimeout(function () {
            let payload = {
                planId: id,
                planName: name,
            }
            t.props.updateActivePlanRequest(payload)
        })
    }
    componentWillMount() {
        setTimeout(() => {
            this.setState({
                noPlanLoader: this.props.noPlanLoader
            })
        })
        this.tableWidth();
        if (this.props.demandPlanning.getActivePlan.isSuccess) {
            this.setState({
                loader: false
            })
            if (this.props.demandPlanning.getActivePlan.data.resource == null) {
                this.setState({
                    activePlan: this.props.demandPlanning.getActivePlan.data.resource,
                    planExist: true,
                    openToBuy: false
                })
            } else {
                this.setState({
                    activePlan: this.props.demandPlanning.getActivePlan.data.resource,
                    planExist: false,
                    openToBuy: true
                })
            }
            this.setState({
                SIX_MONTHS: this.props.demandPlanning.getActivePlan.data.otbFilter.SIX_MONTHS,
                TWELVE_MONTHS: this.props.demandPlanning.getActivePlan.data.otbFilter.TWELVE_MONTHS,
                THREE_MONTHS: this.props.demandPlanning.getActivePlan.data.otbFilter.THREE_MONTHS,
                QUARTERLY: this.props.demandPlanning.getActivePlan.data.otbFilter.QUARTERLY,
                CUSTOM: this.props.demandPlanning.getActivePlan.data.otbFilter.CUSTOM,

            })
            if (this.props.demandPlanning.getActivePlan.data.otbFilter.THREE_MONTHS) {
                this.setState({
                    frequency: "Next 3 Months"
                })
            } else if (this.props.demandPlanning.getActivePlan.data.otbFilter.SIX_MONTHS) {
                this.setState({
                    frequency: "Next 6 Months"
                })
            } else if (this.props.demandPlanning.getActivePlan.data.otbFilter.TWELVE_MONTHS) {
                this.setState({
                    frequency: "Next 12 Months"
                })
            } else if (this.props.demandPlanning.getActivePlan.data.otbFilter.QUARTERLY) {
                this.setState({
                    frequency: "Quarterly"
                })
            } else if (this.props.demandPlanning.getActivePlan.data.otbFilter.CUSTOM) {
                this.setState({
                    frequency: "Custom Month Range"
                })
            }
            this.props.getActivePlanClear();
        }
        // let activePlan = this.props.activePlan
        // let frequency = this.state.frequency.split(" ");
        // let value = frequency[1] + "_" + frequency[2]
        // var activePlanTrue = []
        // let flag = false
        // if (activePlan != null) {
        //     for (var i = 0; i < activePlan.length; i++) {

        //         if (activePlan[i].status == "TRUE") {
        //             flag = true

        //             break;
        //         } else {
        //             flag = false
        //         }
        //     }
        // }
        // if (flag) {
        //     for (var i = 0; i < activePlan.length; i++) {

        //         if (activePlan[i].status == "TRUE") {
        //             activePlanTrue.push(activePlan[i])
        //             this.setState({
        //                 activePlanTrue: activePlanTrue,

        //             })
        //             let data = {
        //                 no: 1,
        //                 activePlan: activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD",
        //                 frequency: value,
        //                 startDate: "",
        //                 endDate: "",
        //                 planId: activePlanTrue[0].planId,
        //             }
        //             this.props.getOtbPlanRequest(data)
        //             this.props.totalOtbRequest(data)
        //     
        //         }
        //     }
        // } else {
        //     this.setState({
        //         activePlanTrue: [],
        //     })
        // }

    }
    componentWillReceiveProps(nextProps) {
        this.tableWidth();
        const t = this
        setTimeout(() => {
            this.setState({
                activePlan: this.props.activePlan,
            })
        }, 10)
        if (nextProps.demandPlanning.getActivePlan.isSuccess) {
            this.setState({
                SIX_MONTHS: nextProps.demandPlanning.getActivePlan.data.otbFilter.SIX_MONTHS,
                TWELVE_MONTHS: nextProps.demandPlanning.getActivePlan.data.otbFilter.TWELVE_MONTHS,
                THREE_MONTHS: nextProps.demandPlanning.getActivePlan.data.otbFilter.THREE_MONTHS,
                QUARTERLY: nextProps.demandPlanning.getActivePlan.data.otbFilter.QUARTERLY,
                CUSTOM: nextProps.demandPlanning.getActivePlan.data.otbFilter.CUSTOM,

            })
            if (nextProps.demandPlanning.getActivePlan.data.otbFilter.THREE_MONTHS) {
                this.setState({
                    frequency: "Next 3 Months"
                })
            } else if (nextProps.demandPlanning.getActivePlan.data.otbFilter.SIX_MONTHS) {
                this.setState({
                    frequency: "Next 6 Months"
                })
            } else if (nextProps.demandPlanning.getActivePlan.data.otbFilter.TWELVE_MONTHS) {
                this.setState({
                    frequency: "Next 12 Months"
                })
            } else if (nextProps.demandPlanning.getActivePlan.data.otbFilter.QUARTERLY) {

                this.setState({
                    frequency: "Quarterly"
                })
            } else if (nextProps.demandPlanning.getActivePlan.data.otbFilter.CUSTOM) {
                this.setState({
                    frequency: "Custom Month Range"
                })
            }
            this.props.getActivePlanClear();
            this.props.getActivePlanClear();
            // let activePlan = this.props.activePlan
            // let frequency = this.state.frequency.split(" ");
            // let value = frequency[1] + "_" + frequency[2]
            // var activePlanTrue = []
            // let flag = false
            // if (activePlan != null) {
            //     for (var i = 0; i < activePlan.length; i++) {

            //         if (activePlan[i].status == "TRUE") {
            //             flag = true

            //             break;
            //         } else {
            //             flag = false
            //         }
            //     }
            // }
            // if (flag) {
            //     for (var i = 0; i < activePlan.length; i++) {

            //         if (activePlan[i].status == "TRUE") {
            //             activePlanTrue.push(activePlan[i])
            //             this.setState({
            //                 activePlanTrue: activePlanTrue,

            //             })
            //             let data = {
            //                 no: 1,
            //                 activePlan: activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD",
            //                 frequency: value,
            //                 startDate: "",
            //                 endDate: "",
            //                 planId: activePlanTrue[0].planId,
            //             }
            //          
            //             // this.props.getOtbPlanRequest(data)
            //             // this.props.totalOtbRequest(data)

            //         }
            //     }
            // } else {
            //     this.setState({
            //         activePlanTrue: [],
            //     })
            // }
        }
        if (nextProps.demandPlanning.getOtbStatus.isSuccess) {
            if (nextProps.demandPlanning.getOtbStatus.data.resource.status == "SUCCEEDED") {
                this.setState({
                    noPlanLoader: false,
                    statusFailed: false
                })
                // setTimeout(() => {
                let activePlan = this.props.activePlan
                let frequency = []


                let value = ""
                if (this.state.frequency == "Quarterly") {
                    value = "quarterly"

                } else {

                    frequency = this.state.frequency.split(" ");
                    value = frequency[1] + "_" + frequency[2]
                }
                var activePlanTrue = []
                let flag = false
                if (activePlan != null) {
                    for (var i = 0; i < activePlan.length; i++) {

                        if (activePlan[i].status == "TRUE") {
                            flag = true

                            break;
                        } else {
                            flag = false
                        }
                    }
                }
                if (flag) {
                    for (var i = 0; i < activePlan.length; i++) {

                        if (activePlan[i].status == "TRUE") {
                            activePlanTrue.push(activePlan[i])
                            this.setState({
                                activePlanTrue: activePlanTrue,

                            })
                            let data = {
                                no: 1,
                                activePlan: activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD",
                                frequency: value,
                                startDate: "",
                                endDate: "",
                                planId: activePlanTrue[0].planId,
                            }
                            this.props.getOtbPlanRequest(data)
                            this.props.totalOtbRequest(data)
                            this.props.getOtbStatusClear();
                            // this.props.getActivePlanRequest()
                        }
                    }
                } else {
                    this.setState({
                        activePlanTrue: [],
                    })
                }
            } else if (nextProps.demandPlanning.getOtbStatus.data.resource.status == "INPROGRESS") {
                this.setState({
                    noPlanLoader: true
                })
            }
        }
        if (nextProps.demandPlanning.getOtbPlan.isSuccess) {
            this.setState({
                scrollLoader: false
            })
            // document.body.classList.remove('scrollHidden')
            if (nextProps.demandPlanning.getOtbPlan.data.resource != null) {
                this.setState({
                    otbPlanData: this.state.otbPlanData.length == 0 ? nextProps.demandPlanning.getOtbPlan.data.resource : { ...this.state.otbPlanData, ...nextProps.demandPlanning.getOtbPlan.data.resource },
                    currPageno: nextProps.demandPlanning.getOtbPlan.data.currPage,
                    maxPageno: nextProps.demandPlanning.getOtbPlan.data.maxPage,
                    assortmentCode: this.state.otbPlanData.length == 0 ? Object.keys(nextProps.demandPlanning.getOtbPlan.data.resource) : Object.keys({ ...this.state.otbPlanData, ...nextProps.demandPlanning.getOtbPlan.data.resource })
                })
                this.showBtn()
            } else {
                this.setState({
                    otbPlanData: [],
                    assortmentCode: [],
                    currPageno: 0,
                    noMoreData: false
                })
            }
            this.props.getOtbPlanClear();
        }
        if (nextProps.demandPlanning.totalOtb.isSuccess) {
            if (nextProps.demandPlanning.totalOtb.data.resource != null) {
                this.setState({
                    totalOtb: nextProps.demandPlanning.totalOtb.data.resource,
                })
            } else {
                this.setState({
                    totalOtb: [],
                })
            }
            this.props.totalOtbRequest();
        }
        if (nextProps.demandPlanning.getOtbPlan.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getOtbPlanRequest();
        }
        else if (nextProps.demandPlanning.getOtbPlan.isError) {
            this.setState({
                errorMessage: nextProps.demandPlanning.getOtbPlan.message.error == undefined ? undefined : nextProps.demandPlanning.getOtbPlan.message.error.errorMessage,
                errorCode: nextProps.demandPlanning.getOtbPlan.message.error == undefined ? undefined : nextProps.demandPlanning.getOtbPlan.message.error.errorCode,
                code: nextProps.demandPlanning.getOtbPlan.message.status,
                alert: true,
                loader: false
            })
            this.props.getOtbPlanRequest();
        } else if (!nextProps.demandPlanning.getOtbPlan.isLoading) {
            this.setState({
                scrollLoader: false
            })
        }
        if (nextProps.demandPlanning.createPlan.isSuccess) {
            this.props.getActivePlanRequest()
            this.setState({
                totalOtb: [],
                otbPlanData: [],
                assortmentCode: [],
                currPageno: 0,
                noMoreData: false
            });
        }
        if (nextProps.demandPlanning.updatePlan.isSuccess) {
            this.setState({
                totalOtb: [],
                otbPlanData: [],
                assortmentCode: [],
                currPageno: 0,
                noMoreData: false

            });
        }
        if (nextProps.demandPlanning.planSaleUpdate.isSuccess) {
            let activePlanstate = this.props.activePlan
            let frequency = this.state.frequency
            frequency = frequency.split(" ");
            let value = frequency[1] + "_" + frequency[2];
            var activePlanTrue = []
            if (activePlanstate != null) {
                for (var i = 0; i < activePlanstate.length; i++) {
                    if (activePlanstate[i].status == "TRUE") {
                        activePlanTrue.push(activePlanstate[i])
                        this.setState({
                            activePlanTrue: activePlanTrue
                        })
                        let data = {
                            no: 1,
                            activePlan: activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD",
                            frequency: this.state.frequency == "Next 6 Months" || this.state.frequency == "Next 3 Months" || this.state.frequency == "Next 12 Months" ? value : this.state.frequency == "Quarterly" ? "quarterly" : "NA",
                            startDate: this.state.startDate,
                            endDate: this.state.endDate,
                            planId: activePlanTrue[0].planId,
                        }
                        this.props.totalOtbRequest(data)
                    }
                }
            }
        }
        if (nextProps.demandPlanning.updateActivePlan.isSuccess) {
            this.setState({
                totalOtb: [],
                otbPlanData: [],
                assortmentCode: [],
                currPageno: 0,
                noMoreData: false
            });
        }
        if (nextProps.demandPlanning.removePlan.isSuccess) {
            this.setState({
                totalOtb: [],
                otbPlanData: [],
                assortmentCode: [],
                currPageno: 0,
                noMoreData: false
            });
        }
        if (nextProps.demandPlanning.planSaleUpdate.isSuccess) {
            let planSaleData = nextProps.demandPlanning.planSaleUpdate.data.resource
            let otbPlanData = this.state.otbPlanData
            for (var i = 0; i < otbPlanData[planSaleData.assortmentCode].length; i++) {
                if (otbPlanData[planSaleData.assortmentCode][i].billDate == planSaleData.billDate) {
                    otbPlanData[planSaleData.assortmentCode][i].planSales = planSaleData.planSales
                    otbPlanData[planSaleData.assortmentCode][i].otbValue = planSaleData.otbValue
                }
            }
            this.setState({
                otbPlanData: otbPlanData
            })
            this.props.planSaleUpdateClear();
        }
        if (nextProps.demandPlanning.getOtbPlan.isLoading) {
            this.setState({
                scrollLoader: true
            })
        }
    }
    exportData() {
        this.setState({
            loader: true
        })
        let planId = this.state.activePlanTrue[0].planId
        let activePlan = this.state.activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD"
        let frequency = []
        let value = ""
        if (this.state.frequency == "Quarterly") {
            value = "quarterly"

        } else {
            frequency = this.state.frequency.split(" ");
            value = frequency[1] + "_" + frequency[2]
        }
        let startDate = this.state.startDate
        let endDate = this.state.endDate
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}${CONFIG.OTB}/export/otbplan?planId=${planId}&activePlan=${activePlan}&frequency=${value}&startDate=${startDate}&endDate=${endDate}`, { headers: headers })
            .then(res => {
                this.setState({
                    loader: false
                })
                window.open(res.data.data.resource.url, '_blank')

            }).catch((error) => {
                this.setState({
                    loader: false,
                    errorMessage: res.data.error.errorMessage,
                    errorCode: res.data.error.errorCode,
                    code: res.data.status,
                    alert: true
                })
            });
    }

    totalhidePanel(e) {
        this.tableWidth();
        if (this.state.totalOtbCondition) {
            this.setState({
                totalOtbCondition: false

            })

        } else {
            this.setState({
                totalOtbCondition: true,
                activeAssormentCode: ""

            })
            this.showBtn();
        }

        // var isMobileVersion = document.getElementsByClassName('collapsed');
        //     if (document.getElementById('hide').style.display=="block"){
        //     document.getElementById('hide').style.display="none"

        //     }else{
        //     document.getElementById('hide').style.display="block";

        //     }
        //     if ( document.getElementById('rotate').style.transform=="rotate(180deg)"){
        //         document.getElementById('rotate').style.transform="rotate(0deg)"
        //     }else{
        //         document.getElementById('rotate').style.transform="rotate(180deg)"
        //     }

    }
    tableWidth() {
        setTimeout(() => {
            if (document.getElementById('tbodyOtb') != undefined || document.getElementById('tbodyOtb') != null) {
                var wid = document.getElementById('tbodyOtb').offsetWidth;
                var totalScrollLeft = document.getElementById("tbodyOtb").scrollLeft;
                document.getElementById("scrollTotalLeft").scrollLeft = totalScrollLeft
                if (document.getElementsByClassName('tabDivision') != undefined) {
                    for (let i = 0; i < document.getElementsByClassName('tabDivision').length; i++) {
                        document.getElementsByClassName('tabDivision')[i].style.width = wid
                        if (document.getElementById("scrollTotalLeft").style.width != undefined) {
                            document.getElementById("scrollTotalLeft").style.width = wid - 10
                        }
                    }

                }
            }
            if (document.getElementById('getAllOtb') != undefined || document.getElementById('getAllOtb') != null) {
                var wid = document.getElementById('getAllOtb').offsetWidth;
                var tbodyScrollLeft = document.getElementById("getAllOtb").scrollLeft;
                document.getElementById("scrollMeLeft").scrollLeft = tbodyScrollLeft
                if (document.getElementsByClassName('tabDivision') != undefined) {
                    for (let i = 0; i < document.getElementsByClassName('tabDivision').length; i++) {
                        document.getElementsByClassName('tabDivision')[i].style.width = wid
                        if (document.getElementById("scrollMeLeft").style.width != undefined) {
                            document.getElementById("scrollMeLeft").style.width = wid - 10
                        }
                    }
                }
            }
        }, 10)
    }
    // _________________________SALE PLAN CODE START_______________________________

    onHandleChangetotalOtb(billDate, assortmentCode, e) {
        let otbPlanData = this.state.otbPlanData
        if (e.target.validity.valid) {
            for (var j = 0; j < otbPlanData[assortmentCode].length; j++) {
                if (otbPlanData[assortmentCode][j].billDate == billDate) {
                    otbPlanData[assortmentCode][j].planSales = e.target.value
                }
            }
        }
        this.setState({
            otbPlanData: otbPlanData
        })
    }
    focusSaleConfirmation(e) {
        this.setState({
            focusValue: e.target.value,
        })
    }
    saleConfirmation(plannedData) {
        let otbPlanData = this.state.otbPlanData
        for (var j = 0; j < otbPlanData[plannedData.assortmentCodeState].length; j++) {
            if (otbPlanData[plannedData.assortmentCodeState][j].billDate == plannedData.billDateState) {
                otbPlanData[plannedData.assortmentCodeState][j].planSales = plannedData.focusValue
            }
        }
        this.setState({
            otbPlanData: otbPlanData,
            planSaleModal: !this.state.planSaleModal,
        })
    }

    blurTotalOtbPlanSales(billDate, assortmentCode, e, markDown, closingInventory, openingStock, openPurchaseOrder, otbValue) {
        if (this.state.focusValue != e.target.value) {
            this.setState({
                planSaleModal: true,
                billDateState: billDate,
                assortmentCodeState: assortmentCode,
                valueState: e.target.value,
                markDown: markDown,
                closingInventory: closingInventory,
                openingStock: openingStock,
                openPurchaseOrder: openPurchaseOrder,
                otbValue: otbValue,
            })
        }
    }
    onConfirmModal(plannedData) {
        let planId = this.state.activePlanTrue[0].planId
        let activePlan = this.state.activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD"
        const t = this
        setTimeout(() => {
            let payload = {
                activePlan: activePlan,
                billDate: plannedData.billDateState,
                assortmentCode: plannedData.assortmentCodeState,
                newPlanSales: plannedData.valueState,
                planId: planId,
                isUserCreatePlan: "TRUE",
                markDown: plannedData.markDown,
                closingInventory: plannedData.closingInventory,
                openingStock: plannedData.openingStock,
                openPurchaseOrder: plannedData.openPurchaseOrder,
                otbValue: plannedData.otbValue,
            }
            t.props.planSaleUpdateRequest(payload)
        }, 10)
        this.setState({
            planSaleModal: !this.state.planSaleModal
        })
    }
    componentDidMount() {
        document.body.addEventListener('scroll', this.handleScrollToElement);
    }
    componentWillUnmount() {
        document.body.removeEventListener('scroll', this.handleScrollToElement);
    }
    handleScrollToElement = () => {
        let activePlanTrue = this.state.activePlanTrue
        let frequency = []
        let value = ""
        if (this.state.frequency == "Quarterly") {
            value = "quarterly"
        } else {
            frequency = this.state.frequency.split(" ");
            value = frequency[1] + "_" + frequency[2]
        }
        if (document.body.scrollHeight - document.body.scrollTop >= 620 && document.body.scrollHeight - document.body.scrollTop <= 680 && this.state.scrollLoader == false) {
            if (this.state.currPageno > 0) {
                if (this.state.currPageno < this.state.maxPageno) {
                    let data = {
                        no: this.state.currPageno + 1,
                        activePlan: activePlanTrue[0].isUfDfd == "YES" ? "DFD" : "ASD",
                        frequency: this.state.frequency == "Next 6 Months" || this.state.frequency == "Next 3 Months" || this.state.frequency == "Next 12 Months" ? value : this.state.frequency == "Quarterly" ? "quarterly" : "NA",
                        startDate: this.state.startDate,
                        endDate: this.state.endDate,
                        planId: activePlanTrue[0].planId,
                    }
                    this.props.getOtbPlanRequest(data)
                } else if (this.state.currPageno == this.state.maxPageno) {
                    this.setState({
                        noMoreData: true,
                    })
                }
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    otbInitScroll(e) {
        var id = e.target.parentNode.parentNode.id;
        if (id == "wid") {
            if (e.target.id == "scrollLeft") {
                var element = document.getElementById('wid');
                var positionInfo = element.getBoundingClientRect();
                var width = positionInfo.width;
                var elmnt = document.getElementById("wid");
                elmnt.scrollLeft = elmnt.scrollLeft - width;
            } else {
                var element = document.getElementById('wid');
                var positionInfo = element.getBoundingClientRect();
                var width = positionInfo.width;
                var elmnt = document.getElementById("wid");
                elmnt.scrollLeft = elmnt.scrollLeft + width;
                elmnt.style.transition = "5s";
            }
        } else {
            if (e.target.id == "scrollLeftAll") {
                var element = document.getElementById(id);
                var positionInfo = element.getBoundingClientRect();
                var width = positionInfo.width;
                var elmnt = document.getElementById(id);
                elmnt.scrollLeft = elmnt.scrollLeft - width;
            } else {
                var element = document.getElementById(id);
                var positionInfo = element.getBoundingClientRect();
                var width = positionInfo.width;
                var elmnt = document.getElementById(id);
                elmnt.scrollLeft = elmnt.scrollLeft + width;
                elmnt.style.transition = "5s";
            }
        }
    }
    showBtn() {
        setTimeout(() => {
            for (let i = 0; i < this.state.assortmentCode.length; i++) {
                var r = this.state.assortmentCode[i].replace(/ /g, '').replace(/[\[\]']/g, '').replace(/-/g, '')
                var td2 = document.getElementById(r) != null ? document.getElementById(r).querySelectorAll("td") : null
                if (td2 != null) {
                    if (td2.length > 10) {
                        if (!document.getElementById(r).classList.contains("scroll")) {
                            document.getElementById(r).classList.add("scroll")
                        }
                    }
                }
            }
            var element = document.getElementById('wid');
            if (element != null || element != undefined) {
                var allTd = element.querySelectorAll("td");
                var positionInfo = element.getBoundingClientRect();
                var width = positionInfo.width;
                if (allTd.length > 10) {
                    document.getElementById("wid").classList.add("scroll")
                }
                else { document.getElementById("wid").classList.remove("scroll") }
            }
        }, .1)
        // if(element2 != null || element2 != undefined){
        //     var alltd2 = element2.querySelectorAll("td");
        //     var width2 = positionInfo2.width;
        //     if (alltd2.length > 10) { document.getElementById("wid2").classList.add("scroll") }
        //     else { document.getElementById("wid2").classList.remove("scroll") }

        // }
    }
    toolTipLabel() {
        setTimeout(() => {
            let li = document.querySelector('.otbGraph').childNodes[0].childNodes[0].childNodes[1].childNodes[0].childNodes[1].getElementsByTagName("li")
            li[0].childNodes[0].innerHTML = "OTB Value"
            for (let i = 1; i < li.length - 1; i++) {
                var str = li[i].childNodes[0].innerHTML.match(/[A-Z]+[^A-Z]*|[^A-Z]+/g).join(" ")
                li[i].childNodes[0].innerHTML = str[0].toUpperCase() + str.slice(1)
            }
            li[li.length - 1].childNodes[0].innerHTML = "Planned Sales"
        }, 100)
    }
    handleKeyPress(billDate, data, e, markDown, closingInventory, openingStock, openPurchaseOrder, otbValue) {
        if (e.key == "Enter") {
            this.blurTotalOtbPlanSales(billDate, data, e, markDown, closingInventory, openingStock, openPurchaseOrder, otbValue)
        }
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="container_div" id="home">
                    <div className="container_div" id="">
                        <div className="container-fluid pad-0">
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                <div className="replenishment_container weekelyPlanning openToBuyMain">
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <ul className="list_style">
                                            <li>
                                                <label className="contribution_mart">
                                                    OPEN TO BUY
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-12 col-sm-12 pad-0 ">
                                        <div className="col-md-6 pad-0 alignMiddle">
                                            <div className="col-md-3 pad-0">
                                                <h4>Plan  </h4><h3 className="pad-lft-7">{this.state.activePlanTrue.length != 0 ? this.state.activePlanTrue[0].planName : null}</h3>
                                            </div>
                                            <div className="col-md-6">
                                                {/* <div className="switch6 switchToggleText">
                                                        <label className="switch6-light">
                                                            <input type="checkbox" />
                                                            <span onClick={() => this.otbToggle()}>
                                                                <span className={this.state.otbState == "Retail" ? "textWhite" : "otbToggleSwitch"}>OTB at Retail</span>
                                                                <span className={this.state.otbState == "Cost" ? "textWhite" : "otbToggleSwitch"}>OTB at Cost</span>
                                                            </span>
                                                            <a className="bgBtn btn btn-primary"></a>
                                                        </label>
                                                    </div> */}
                                                <span className="otbButtonStatic">OTB at Retail</span>

                                            </div>
                                        </div>
                                        <div className="col-md-6 pad-0 mainHeadRight">
                                            <div className="col-md-3">
                                            </div>
                                            <div className="col-md-9 pad-0">
                                                <ul className="list_style float_Right">
                                                    <li className="m-rgt-20">
                                                        {this.state.activePlanTrue.length == 0 || this.state.totalOtb.length == 0 ? <button type="button" className="btnDisabled pointerNone"><img src={exportBtn} />Export</button> : <button type="button" onClick={(e) => this.exportData()}><img src={exportBtn} />Export</button>}
                                                    </li>
                                                    <li>
                                                        <div className="settingDrop dropdown planDropDown">
                                                            <button className="btn btn-default dropdown-toggle userModalSelect" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                <img src={planIcon} />  Plan
                                                                </button>
                                                            <div className="dropDownContent dropdown-menu">
                                                                <h5>Manage Plans</h5>
                                                                <ul>
                                                                    <li><a className="active" onClick={e => this.openCreatePlan("")}><img src={newPlanIcon} />New Plan</a></li>
                                                                    {this.state.activePlanTrue.length == 0 ? <li><a disabled className="pointerNone disableText "><img src={copyIcon} />Make a Copy</a></li> :
                                                                        <li><a onClick={e => this.openCreatePlan("Make a copy")}><img src={copyIcon} />Make a Copy</a></li>}
                                                                    {this.state.activePlanTrue.length == 0 ? <li><a className="pointerNone disableText " disabled><img src={deleteIcon} />Remove</a></li> :
                                                                        <li><a onClick={(e) => this.openDeletePlan(e)}><img src={deleteIcon} />Remove</a></li>}

                                                                </ul>

                                                                <h5 className="pad-top-10">Available Plans</h5>
                                                                <ul className="availablePlans">
                                                                    {this.props.activePlan == null ? <li> NO DATA FOUND </li> : this.props.activePlan.length == 0 ? <li> NO DATA FOUND</li> : this.props.activePlan.map((data, akey) => (
                                                                        <li key={akey} onClick={() => this.updateActivePlan(`${data.planName}`, `${data.planId}`)}> <label className="select_modalRadio displayPointer">
                                                                            <input type="radio" checked={data.status == "TRUE"} name="plan" /><span className="pad-lft-15">{data.planName}</span>
                                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                                        </label>
                                                                            {data.status == "TRUE" ? <span className="planStatus">Active</span> : null}
                                                                        </li>))}

                                                                </ul>
                                                                <h5 className="pad-top-10">Update Plans</h5>
                                                                <ul className="updatePlan">
                                                                    <li>
                                                                        {this.state.activePlanTrue.length == 0 ? <a className="disablePlan disableText pointerNone"><img src={settingIcon} />Update with Past Year Data</a> :
                                                                            <a onClick={(e) => this.openUpdatePlan("isUfAsd")}><img src={settingIcon} />Update with Past Year Data</a>}
                                                                    </li>
                                                                    <li>
                                                                        {this.state.activePlanTrue.length == 0 ? <a className="disablePlan disableText pointerNone"><img src={settingIcon} />Update with Sales Forecast Data</a> :
                                                                            <a onClick={(e) => this.openUpdatePlan("isUfDfd")}><img src={settingIcon} />Update with Sales Forecast Data</a>}
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="settingDrop dropdown yearDropDown">
                                                            {this.state.activePlanTrue.length == 0 ? <button className="btn btn-default dropdown-toggle userModalSelect btnDisabled pointerNone" type="button" id="dropdownMenu1" aria-haspopup="true" aria-expanded="true">
                                                                {this.state.frequency}
                                                                <i className="fa fa-chevron-down"></i>
                                                            </button> : <button className="btn btn-default dropdown-toggle userModalSelect" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                    {this.state.frequency}
                                                                    <i className="fa fa-chevron-down"></i>
                                                                </button>}
                                                            <ul className="dropdown-menu" aria-labelledby="dropdownMenu1" id="frequency">
                                                                {/*{this.state.frequencyData.map((data, keuy) => (
                                                                   <li key={keuy} onClick={(e) => this.onFrequencyChange(`${data.frequency}`)}> <a>{data.frequency}</a>     </li>))}*/}
                                                                {this.state.THREE_MONTHS ? <li onClick={(e) => this.onFrequencyChange("Next 3 Months")}><a>Next 3 Months</a></li> : null}
                                                                {this.state.SIX_MONTHS ? <li onClick={(e) => this.onFrequencyChange("Next 6 Months")}><a>Next 6 Months</a></li> : null}
                                                                {this.state.TWELVE_MONTHS ? <li onClick={(e) => this.onFrequencyChange("Next 12 Months")}><a>Next 12 Months</a></li> : null}
                                                                {this.state.QUARTERLY ? <li onClick={(e) => this.onFrequencyChange("Quarterly")}><a>Quarterly</a></li> : null}
                                                                {this.state.CUSTOM ? <li onClick={(e) => this.onFrequencyChange("Custom Month Range")}><a>Custom Month Range</a></li> : null}

                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    {/* total otb graph start */}
                                    {/* -------------------------------------------------No Current Plan Div------------------------ */}
                                    {this.props.statusFailed || this.state.activePlanTrue.length == 0 ? <div className="noCurrentPlanExist">
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 noExistCurrentPlan">

                                            <div className="container_content height-70vh noPlanExist">
                                                {/* <label className="contribution_mart"> OPEN TO BUY </label> */}
                                                <div className="middleContent">
                                                    <img src={NoPlanIcon} />
                                                    <h2>No existing current active Plan Found</h2>
                                                    <button className="record_bt " type="button" onClick={e => this.openCreatePlan("")}>
                                                        Create New Plan
                                                                </button>
                                                </div>
                                            </div>

                                        </div>
                                        {this.state.noPlanLoader == true ? <ProcessLoader /> : null}
                                    </div> : null}

                                    {this.state.activePlanTrue.length != 0 && this.state.totalOtb.length == 0 ? <div className="noCurrentPlanExist">
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">

                                            <div className="container_content height-70vh noPlanExist">
                                                {/* <label className="contribution_mart"> OPEN TO BUY </label> */}
                                                <div className="middleContent">
                                                    <img src={NoPlanIcon} />
                                                    <h2>No Data in current Plan Found</h2>
                                                    <button className="record_bt " type="button" onClick={(e) => this.openUpdatePlan("isUfAsd")}>
                                                        Update with Past Year Data
                                                                </button>
                                                    <button className="record_bt " type="button" onClick={(e) => this.openUpdatePlan("isUfDfd")}>
                                                        Update with Sales Forecast Data
                                                                </button>
                                                </div>
                                            </div>
                                        </div>
                                        {this.state.noPlanLoader == true ? <ProcessLoader /> : null}
                                    </div> : null}
                                    {/* -------------------------------------------------No Current Plan Div End------------------------ */}
                                    {this.state.totalOtb.length != 0 ? <div className="otbAccordian m-top-10">

                                        <div className="panel-group" id="accordion">
                                            <div className="panel panel-default">
                                                <div className="panel-heading">
                                                    <h4 className="panel-title">
                                                        <a>
                                                            Total Otb
                                                                                                            </a>
                                                    </h4>
                                                    <button className="accordion-toggle float_Right" data-toggle="collapse" data-parent="#accordion" href="#totalOtb" type="button" onClick={(e) => this.totalhidePanel(e)} >
                                                        <img src={openArrow} className={this.state.totalOtbCondition == false ? "img_transform-180" : "img_transform-0"} id="rotate" /></button>
                                                </div>
                                                {/* -------------------------------------------------hide Div---------------------------------------- */}

                                                {this.state.totalOtbCondition == false ? <div className="initialShowHead display_blockk" id="hide">
                                                    <div className="col-md-12 pad-0">
                                                        <div className="col-md-1 pad-0">
                                                            <h4>{this.state.frequency == "Quarterly" ? "Quarter" : "Month"}</h4>
                                                            <h4 className="m-top-10">OTB</h4>
                                                        </div>
                                                        <div className="col-md-11 pad-0">
                                                            <div className="otbHeadTable " id="wid">
                                                                <button type="button" onClick={(e) => this.otbInitScroll(e)}> <img id="scrollLeft" src={leftScroll} /></button>
                                                                <button type="button" onClick={(e) => this.otbInitScroll(e)}><img id="scrollRight" src={rightScroll} /></button>
                                                                <table>
                                                                    <thead>
                                                                        <tr>
                                                                            {/* <th>Month</th> */}
                                                                            {this.state.totalOtb != null ? this.state.totalOtb.map((otb, otbidd) => (
                                                                                <th id={otbidd} key={otbidd} >
                                                                                    <label>
                                                                                        {this.state.frequency == "Quarterly" ? otb.quarter : otb.billDate}
                                                                                    </label>
                                                                                </th>)) : null}
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            {/* <td>OTB</td> */}

                                                                            {this.state.totalOtb != null ? this.state.totalOtb.map((otbb, otbiidd) => (
                                                                                <td id={otbiidd} key={otbiidd} >
                                                                                    <label>
                                                                                        {this.commaSeparator(otbb.otbValue)}
                                                                                    </label>
                                                                                </td>)) : null}
                                                                        </tr>
                                                                    </tbody>
                                                                </table></div></div></div>
                                                </div> : null}
                                                {/* -------------------------------------------------------hide Div End---------------------------------- */}
                                                {this.state.totalOtbCondition ? <div id="totalOtb" className="panel-collapse collapse in">
                                                    <div className="panel-body pad-lft-0 padRightNone otbTotalTable">
                                                        <div className="col-md-12 m-top-10 pad-0">
                                                            <div className="panelBodyLeft">
                                                                <div className="col-md-12 pad-0 graphLeft">

                                                                    <ul className="graphItems list-inline">
                                                                        <li><span></span>Planned Sales</li>
                                                                        <li><span></span>Planned Markdown</li>
                                                                        <li><span></span>Planned Closing Inventory</li>
                                                                        <li><span></span>Planned Opening Stock</li>
                                                                        <li><span></span>Open Purchase Order</li>
                                                                        <li><span></span>Open-to-Buy at Retail</li>
                                                                    </ul>
                                                                    <div className="col-md-12 pad-0">
                                                                        <div className="col-md-12 pad-0 m-top-6"> {this.state.totalOtbCondition ? <TotalOtbGraph {...this.props} {...this.state} totalOtb={this.state.totalOtb} toolTipLabel={() => { this.toolTipLabel() }} /> : null}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="panelBodyRight">
                                                                <div className="col-md-2 metrices pad-0">
                                                                    <h4>Metric</h4>
                                                                    <ul className="pad-0">
                                                                        <li>Planned Sales</li>
                                                                        <li>Planned Markdown</li>
                                                                        <li>Planned Closing Inventory</li>
                                                                        <li>Planned Opening Stock</li>
                                                                        <li>Open Purchase Order</li>
                                                                        <li>Open-to-Buy at Retail</li>

                                                                    </ul>
                                                                </div>
                                                                <div className="panelTableMain col-md-10">

                                                                    <div className="tableHeadWidth tabDivision" id="scrollTotalLeft">
                                                                        <ul className="otbTable"> {this.state.totalOtb != null ? this.state.totalOtb.map((monthOtb, monthOtbid) => (
                                                                            <li id={monthOtbid} key={monthOtbid} >
                                                                                <label>
                                                                                    {this.state.frequency == "Quarterly" ? monthOtb.quarter : monthOtb.billDate}
                                                                                </label>
                                                                            </li>)) : null}</ul>
                                                                    </div>
                                                                    <div className="tableBody tabDivision" id="tbodyOtb" onScroll={() => this.tableWidth()}>
                                                                        {this.state.totalOtb != null ? this.state.totalOtb.map((dataOtb, dataOtbid) => (
                                                                            <ul className="tabDivision divvalue otbTotalTable" id={dataOtbid} key={dataOtbid}>
                                                                                <li>

                                                                                    <label>
                                                                                        {this.commaSeparator(dataOtb.planSales)}
                                                                                    </label>
                                                                                </li>
                                                                                <li >
                                                                                    <label>
                                                                                        {dataOtb.markDown}
                                                                                    </label>
                                                                                </li>
                                                                                <li  >
                                                                                    <label>
                                                                                        {dataOtb.closingInventory}
                                                                                    </label>
                                                                                </li>
                                                                                <li  >
                                                                                    <label>
                                                                                        {dataOtb.openingStock}
                                                                                    </label>
                                                                                </li>
                                                                                <li  >
                                                                                    <label>
                                                                                        {dataOtb.openPurchaseOrder}
                                                                                    </label>
                                                                                </li>
                                                                                <li  >
                                                                                    <label>
                                                                                        {this.commaSeparator(dataOtb.otbValue)}
                                                                                    </label>
                                                                                </li>
                                                                            </ul>
                                                                        )) : null}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> : null}
                                            </div>
                                        </div>
                                    </div> : null}
                                    {/* total otb graph ends */}
                                    {this.state.otbPlanData.length == 0 && this.state.assortmentCode.length == 0 ? null :
                                        this.state.assortmentCode.map((data, key1) => (
                                            <div className="otbAccordian m-top-10" key={key1} >
                                                <div className="panel-group" id={"accordion" + data.replace(/ /g, '').replace(/[\[\]']/g, '').replace(/-/g, '')}>
                                                    <div className="panel panel-default">
                                                        <div className="panel-heading">
                                                            <h4 className="panel-title">
                                                                <a>
                                                                    {data}
                                                                </a>
                                                            </h4>
                                                            <button className="accordion-toggle float_Right" data-toggle="collapse" data-parent={"#accordion" + data.replace(/ /g, '').replace(/[\[\]']/g, '').replace(/-/g, '')} type="button" onClick={(e) => this.hidePanel(data)}><img src={openArrow} className={this.state.activeAssormentCode != data ? "img_transform-180" : "img_transform-0"} id={"rotate" + data.replace(/ /g, '').replace(/[\[\]']/g, '').replace(/-/g, '')} /></button>
                                                        </div>
                                                        {/* -------------------------------------------------hide Div---------------------------------------- */}

                                                        {this.state.activeAssormentCode != data ? <div className="initialShowHead display_blockk" id={"hide" + data.replace(/ /g, '').replace(/[\[\]']/g, '').replace(/-/g, '')}>
                                                            <div className="col-md-1 pad-0">
                                                                <h4>{this.state.frequency == "Quarterly" ? "Quarter" : "Month"}</h4>
                                                                <h4 className="m-top-10">OTB</h4>
                                                            </div>
                                                            <div className="col-md-11 pad-0">
                                                                <div className="otbHeadTable " id={data.replace(/ /g, '').replace(/[\[\]']/g, '').replace(/-/g, '')}>
                                                                    <button type="button" onClick={(e) => this.otbInitScroll(e)}> <img id="scrollLeftAll" src={leftScroll} /></button>
                                                                    <button type="button" onClick={(e) => this.otbInitScroll(e)}><img id="scrollRightAll" src={rightScroll} /></button>
                                                                    <table>
                                                                        <thead>
                                                                            <tr>
                                                                                {/* <th>Month</th> */}

                                                                                {this.state.otbPlanData[data].map((month, idd) => (
                                                                                    <th id={idd} key={idd} >
                                                                                        <label>
                                                                                            {this.state.frequency == "Quarterly" ? month.quarter : month.billDate}
                                                                                        </label>
                                                                                    </th>))}
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                {this.state.otbPlanData[data].map((otb, otbidd) => (
                                                                                    <td id={otbidd} key={otbidd} >
                                                                                        <label>
                                                                                            {this.commaSeparator(otb.otbValue)}
                                                                                        </label>
                                                                                    </td>))}
                                                                            </tr>
                                                                        </tbody>
                                                                    </table></div></div>
                                                        </div> : null}
                                                        {/* -------------------------------------------------------hide Div End---------------------------------- */}
                                                        {this.state.activeAssormentCode == data ? <div className="panel-collapse collapse in">
                                                            <div className="panel-body pad-lft-0 padRightNone">
                                                                <div className="col-md-12 m-top-10 pad-0">
                                                                    <div className="panelBodyLeft">
                                                                        <div className="col-md-12 pad-0 graphLeft">
                                                                            <ul className="graphItems list-inline">
                                                                                <li><span></span>Planned Sales</li>
                                                                                <li><span></span>Planned Markdown</li>
                                                                                <li><span></span>Planned Closing Inventory</li>
                                                                                <li><span></span>Planned Opening Stock</li>
                                                                                <li><span></span>Open Purchase Order</li>
                                                                                <li><span></span>Open-to-Buy at Retail</li>
                                                                            </ul>
                                                                            <div className="col-md-12 pad-0">
                                                                                <div className="col-md-12 padhidePanel m-top-6">{this.state.activeAssormentCode == data ? <OpenToBuyGraph {...this.props} {...this.state} openToBuyData={this.state.otbPlanData[data]} toolTipLabel={() => this.toolTipLabel()} /> : null}</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="panelBodyRight">
                                                                        <div className="col-md-2 metrices padhidePanel">
                                                                            <h4>Metric</h4>
                                                                            <ul className="pad-0">
                                                                                <li>Planned Sales</li>
                                                                                <li>Planned Markdown</li>
                                                                                <li>Planned Closing Inventory</li>
                                                                                <li>Planned Opening Stock</li>
                                                                                <li>Open Purchase Order</li>
                                                                                <li>Open-to-Buy at Retail</li>

                                                                            </ul>
                                                                        </div>
                                                                        <div className="panelTableMain col-md-10">

                                                                            <div className="tableHeadWidth" id="scrollMeLeft">
                                                                                <ul className="otbTable">{this.state.otbPlanData[data].map((months, idsd) => (
                                                                                    <li id={idsd} key={idsd} >
                                                                                        <label>
                                                                                            {this.state.frequency == "Quarterly" ? months.quarter : months.billDate}
                                                                                        </label>
                                                                                    </li>))}</ul>
                                                                            </div>
                                                                            <div className="tableBody tabDivision" id="getAllOtb" onScroll={() => this.tableWidth()}>
                                                                                {this.state.otbPlanData[data].map((sale, saleid) => (

                                                                                    <ul className="divvalue tabDivision" id={saleid} key={saleid}>
                                                                                        <li>   <input type="text" value={sale.planSales} onFocus={(e) => this.focusSaleConfirmation(e)}
                                                                                            onChange={(e) => this.onHandleChangetotalOtb(`${sale.billDate}`, `${data}`, e)} pattern="[0-9]+([\.][0-9]{0,2})?" onKeyDown={(e) => this.handleKeyPress(`${sale.billDate}`, `${data}`, e, `${sale.markDown}`, `${sale.closingInventory}`, `${sale.openingStock}`, `${sale.openPurchaseOrder}`, `${sale.otbValue}`)}
                                                                                            onBlur={(e) => this.blurTotalOtbPlanSales(`${sale.billDate}`, `${data}`, e, `${sale.markDown}`, `${sale.closingInventory}`, `${sale.openingStock}`, `${sale.openPurchaseOrder}`, `${sale.otbValue}`)} /></li>

                                                                                        <li >
                                                                                            <label>
                                                                                                {sale.markDown}
                                                                                            </label>
                                                                                        </li>
                                                                                        <li  >
                                                                                            <label>
                                                                                                {sale.closingInventory}
                                                                                            </label>
                                                                                        </li>
                                                                                        <li  >
                                                                                            <label>
                                                                                                {sale.openingStock}
                                                                                            </label>
                                                                                        </li>
                                                                                        <li  >
                                                                                            <label>
                                                                                                {sale.openPurchaseOrder}
                                                                                            </label>
                                                                                        </li>
                                                                                        <li  >
                                                                                            <label>
                                                                                                {this.commaSeparator(sale.otbValue)}
                                                                                            </label>
                                                                                        </li>
                                                                                    </ul>

                                                                                ))}</div>

                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div> : null}
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    <div className="OtbAccordianEnd">
                                        {/* <ConfirmPlannedSale /> */}
                                        {this.state.noMoreData ? <div className="OtbAccordianEnd">
                                            <p>No More Data</p>
                                        </div> : null}
                                    </div>
                                    {this.state.scrollLoader ? <div className="otbLoader textCenter">
                                        <div className="loaderBgBLur"></div>
                                        <img src={loader} />
                                    </div> : null}
                                </div>
                            </div>
                        </div>
                    </div></div>
                {/* <Footer /> */}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.createPlan ? <CreateNewPlanModal {...this.props} activePlan={this.props.activePlan} makeCopy={this.state.makeCopy} closePlanModal={(e) => this.openCreatePlan(e)} /> : null}
                {this.state.deletePlan ? <DeleteCurrentPlanModal {...this.props} activePlan={this.props.activePlan} closeDeletePlan={e => this.openDeletePlan(e)} /> : null}
                {this.state.customDate ? <CustomDateModal updateFrequency={(e) => this.updateFrequency(e)} activePlan={this.props.activePlan} closeCustomDate={(e) => this.openCustomDate(e)} /> : null}
                {this.state.updatePlan ? <UpdatePlanModal activePlan={this.props.activePlan} updateData={this.state.updateData} {...this.props} closeUpdatePlan={(e) => this.openUpdatePlan(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.planSaleModal ? <ConfirmPlannedSale {...this.state} {...this.props} focusValue={this.state.focusValue} markDown={this.state.markDown} closingInventory={this.state.closingInventory} openingStock={this.state.openingStock} openPurchaseOrder={this.state.openPurchaseOrder} otbValue={this.state.otbValue} blurTotalOtbPlanSales={(e) => this.blurTotalOtbPlanSales(e)} focusSaleConfirmation={(e) => this.focusSaleConfirmation(e)} onConfirmModal={(e) => this.onConfirmModal(e)} saleConfirmation={(e) => this.saleConfirmation(e)} /> : null}
            </div>
        );
    }
}
export default Otb;