import React from 'react';
import { Link } from 'react-router-dom';

class ReplenishmentDrop extends React.Component {

  render() {
    var Schedulers = JSON.parse(sessionStorage.getItem('Schedulers')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Schedulers')).crud);
    var History = JSON.parse(sessionStorage.getItem('History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('History')).crud);
    var Summary = JSON.parse(sessionStorage.getItem('Summary')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Summary')).crud);
    var Auto_Configuration = JSON.parse(sessionStorage.getItem('Auto Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Auto Configuration')).crud);
    var Run_on_Demand = JSON.parse(sessionStorage.getItem('Run on Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Run on Demand')).crud);
    
    return (
      <label>
        <div className="dropdown" id="myFav">
          <button className="dropbtn home_link">Replenishment
              <i className="fa fa-chevron-down"></i>
          </button>
          {/* <div className="dropdown-content adminBreacrumbsDropdown subDropPos">
            {Schedulers>0 ?<a className="scheduler">Scheduler
            <div className="hovershowDropdown subDropPos">
                {Auto_Configuration > 0 ?<Link to="/inventoryPlanning/inventoryAutoConfig" id="menuFavItem">Auto Configuration</Link>: null}
                {Run_on_Demand > 0 ?<Link to="/inventoryPlanning/runOnDemand" id="menuFavItem">Run-On-Demand</Link>: null}
              </div>
            </a>: null}
            <Link to="/inventoryPlanning/configuration" id="menuFavItem">Configuration</Link>
            <Link to="/inventoryPlanning/manageRuleEngine" id="menuFavItem">Manage Rule Engine</Link>
            
            {Summary> 0 ?<Link to="/inventoryPlanning/summary" id="menuFavItem">Summary</Link>: null}
            {History> 0 ?<Link to="/inventoryPlanning/historyReplenishment" id="menuFavItem">History</Link>: null}

          </div> */}
        </div>
      </label>

    );
  }
}

export default ReplenishmentDrop;