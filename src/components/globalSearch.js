import React from "react";
import NewSideBar from "../components/newSidebar";
import SideBar from "./sidebar";
import UserActivity from "./globalSearch/userActivity";
import SearchPages from "./globalSearch/searchPages";
import SearchData from "./globalSearch/searchData";
import FilterLoader from "./loaders/filterLoader";
import RequestError from "./loaders/requestError";
import RequestSuccess from "./loaders/requestSuccess";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import Header from "./header";

class GlobalSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",

            setNoFormate: false,
            setActivityType: false,
            downloadItem: false,
            search: "",
            jsonData: [],
            searchPages: [],
            userActivitiesAll: [],
            userActivities: [],
            userActivityType: "dataValue",
            searchData: [],
            searchDataType: "ASN",
            filter: false,
            tab: "searchPages"
        };
    }
    showFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter
        })
    }
    componentDidMount() {
        let json = JSON.parse(sessionStorage.getItem('modules'))
        json = Object.values(json)[0]
        let jsonObj = [];
        json.map(obj => {
            var path = obj.name;
            if (obj.isPage == 0 && obj.hasOwnProperty("subModules") && obj.subModules != null) {
                this.parseSubModules(obj.subModules, path, jsonObj);
            }
        });
        this.setState({
            jsonData: jsonObj,
            searchPages: jsonObj
        });
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.home.userActivity.isSuccess) {
            this.setState({
                loading: false,
                userActivities: nextProps.home.userActivity.data.resource === null ? [] : nextProps.home.userActivity.data.resource,
                userActivitiesAll: nextProps.home.userActivity.data.resource === null ? [] : nextProps.home.userActivity.data.resource
            });
        }
        else if (nextProps.home.userActivity.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.home.userActivity.message.status,
                errorCode: nextProps.home.userActivity.message.error == undefined ? undefined : nextProps.home.userActivity.message.error.errorCode,
                errorMessage: nextProps.home.userActivity.message.error == undefined ? undefined : nextProps.home.userActivity.message.error.errorMessage,
                userActivities: [],
                userActivitiesAll: []
            });
        }

        if (nextProps.home.searchData.isSuccess) {
            this.setState({
                loading: false,
                searchData: nextProps.home.searchData.data.resource === null ? [] : nextProps.home.searchData.data.resource,
                // userActivitiesAll: nextProps.home.searchData.data.resource === null ? [] : nextProps.home.searchData.data.resource
            });
        }
        else if (nextProps.home.searchData.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.home.searchData.message.status,
                errorCode: nextProps.home.searchData.message.error == undefined ? undefined : nextProps.home.searchData.message.error.errorCode,
                errorMessage: nextProps.home.searchData.message.error == undefined ? undefined : nextProps.home.searchData.message.error.errorMessage,
                searchData: [],
                // userActivitiesAll: []
            });
        }

        if (nextProps.home.userActivity.isLoading || nextProps.home.searchData.isLoading) {
            this.setState({
                loading: true,
                // userActivities: [],
                // userActivitiesAll: []
            });
        }
    }
    find_in_object(my_criteria) {
        return this.state.jsonData.filter(function (obj) {
            return Object.keys(my_criteria).every(function (c) {
                var s = obj[c] + "";
                return s.indexOf(my_criteria[c]) != -1;
            });
        });
    }
    parseSubModules(json, path, jsonObj) {
        const t = this
        $.each(json, function (idx, obj) {
            if (obj.isPage == 0 && obj.hasOwnProperty("subModules") && obj.subModules != null) {
                path += " > " + obj.name;
                t.parseSubModules(obj.subModules, path, jsonObj)
            } else {
                var page = path + " > " + obj.name;
                // console.log(page + " ------- " + obj.name);
                var item = {};
                item["key"] = obj.name.toLowerCase();
                item["name"] = obj.name;
                item["path"] = page;
                item["url"] = obj.pageUrl.replace('#', '');
                jsonObj.push(item);
            }
        });
    }
    openSetNoFormate(e) {
        e.preventDefault();
        this.setState({
            setNoFormate: !this.state.setNoFormate
        }, () => document.addEventListener('click', this.closeSetNoFormate));
    }
    closeSetNoFormate = () => {
        this.setState({ setNoFormate: false }, () => {
            document.removeEventListener('click', this.closeSetNoFormate);
        });
    }

    openSetActivityType(e) {
        e.preventDefault();
        this.setState({
            setActivityType: !this.state.setActivityType
        }, () => document.addEventListener('click', this.closeSetActivityType));
    }
    closeSetActivityType = () => {
        this.setState({ setActivityType: false }, () => {
            document.removeEventListener('click', this.closeSetActivityType);
        });
    }
    
    onInputChange = (e) => {
        this.setState({ search: e.target.value.toLowerCase(), searchPages: this.find_in_object({ key: e.target.value.toLowerCase() })})
    }
    handleSearchChange = (e) => {
        this.setState({
            search: e.target.value
        });
        if (this.state.tab === "searchPages") {
            this.setState({
                searchPages: this.find_in_object({key: e.target.value.toLowerCase()})
            });
        }
        else if (this.state.tab === "userActivity") {
            this.setState({
                userActivities: this.state.userActivityType === "name" ? this.state.userActivitiesAll.filter(item => item.firstName.toLowerCase().includes(e.target.value.toLowerCase()) || item.lastName.toLowerCase().includes(e.target.value.toLowerCase())) : this.state.userActivitiesAll.filter(item => item[this.state.userActivityType].toLowerCase().includes(e.target.value.toLowerCase()))
            });
            // this.setState(prevState => ({
            //     userActivities: prevState.userActivities.filter(item => item.dataValue.toLowerCase().includes(e.target.value.toLowerCase()) || item.email.toLowerCase().includes(e.target.value.toLowerCase()) || item.firstName.toLowerCase().includes(e.target.value.toLowerCase()) || item.lastName.toLowerCase().includes(e.target.value.toLowerCase()) || item.message.toLowerCase().includes(e.target.value.toLowerCase()) || item.module.toLowerCase().includes(e.target.value.toLowerCase()) || item.userName.toLowerCase().includes(e.target.value.toLowerCase()))
            // }));
        }
    }
    changeTabCS = (e) => {
        switch (e.target.hash) {
            case "#searchPages": {
                this.setState({
                    tab: "searchPages",
                    search: ""
                });
                break;
            }
            case "#userActivity": {
                this.setState({
                    tab: "userActivity",
                    search: ""
                });
                this.props.userActivityRequest({
                    pageNo: 1,
                    type: 1,
                    search: "",
                    dataValue: "",
                    message: "",
                    userId: "",
                    userName: ""
                });
                break;
            }
            case "#searchData": {
                this.setState({
                    tab: "searchData",
                    search: ""
                });
                break;
            }
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    getSearchData = () => {
        this.props.searchDataRequest({
            type: this.state.searchDataType,
            search: this.state.search
        });
    }

    render() {console.log(this.state);
        const searchDataNames = {
            ASN: "ASN",
            PO: "PO Number",
            LR: "LR Number",
            GATE: "Gate Entry Number",
            GRC: "GRC Number",
            PR_PO: "Purchase Order",
            PR_PI: "Purchase Indent"
        };
        const userActivityNames = {
            dataValue: "Data Value",
            email: "Email",
            name: "User Name",
            message: "Message",
            module: "Module"
        }
        return (
            <React.Fragment>
            <div className="">
                {/* <div className="backdrop modal-backdrop-new"></div> */}
                <div className="global-search-modal">
                    {/* <Header {...this.props} /> */}
                    {/* <SideBar {...this.props} /> */}
                    <div className="gsm-inner">
                        <div className="gsmi-head">
                            <div className="gsmih-search">
                                {this.state.tab == "searchData" && <button type="button" className="gsmih-dropd-btn" onClick={(e) => this.openSetNoFormate(e)}>{searchDataNames[this.state.searchDataType]} <img src={require('../assets/downArrowNew.svg')} /></button>}
                                {this.state.setNoFormate &&
                                <div className="gsmihs-dropdown">
                                    <ul className="gsmihsdd-inner">
                                        <li onClick={() => this.setState({searchDataType: "ASN"})}>ASN</li>
                                        <li onClick={() => this.setState({searchDataType: "PO"})}>PO Number</li>
                                        <li onClick={() => this.setState({searchDataType: "LR"})}>LR Number</li>
                                        <li onClick={() => this.setState({searchDataType: "GATE"})}>Gate Entry Number</li>
                                        <li onClick={() => this.setState({searchDataType: "GRC"})}>GRC Number</li>
                                        <li onClick={() => this.setState({searchDataType: "PR_PO"})}>Purchase Order</li>
                                        <li onClick={() => this.setState({searchDataType: "PR_PI"})}>Purchase Indent</li>
                                    </ul>
                                </div>}
                                {this.state.tab == "userActivity" && <button type="button" className="gsmih-dropd-btn" onClick={(e) => this.openSetActivityType(e)}>{userActivityNames[this.state.userActivityType]} <img src={require('../assets/downArrowNew.svg')} /></button>}
                                {this.state.setActivityType &&
                                <div className="gsmihs-dropdown">
                                    <ul className="gsmihsdd-inner">
                                        <li onClick={() => this.setState({userActivityType: "dataValue"})}>Data Value</li>
                                        <li onClick={() => this.setState({userActivityType: "email"})}>Email</li>
                                        <li onClick={() => this.setState({userActivityType: "name"})}>User Name</li>
                                        <li onClick={() => this.setState({userActivityType: "message"})}>Message</li>
                                        <li onClick={() => this.setState({userActivityType: "module"})}>Module</li>
                                    </ul>
                                </div>}
                                <span className="gsmihs-search"><img src={require('../assets/searchicon.svg')} /></span>
                                <input type="search" value={this.state.search} onChange={this.handleSearchChange} placeholder="Type to Search" />
                                {/* <span className="gsmihs-enter">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16.266" height="18.941" viewBox="0 0 16.266 18.941">
                                        <g data-name="share (1)">
                                            <g data-name="Group 2791">
                                                <path d="M52.186 5.528L46.9.236a.804.804 0 1 0-1.137 1.136L49.688 5.3h-8.047a5.5 5.5 0 0 0-5.486 5.471v7.366a.803.803 0 1 0 1.606 0v-7.363a3.887 3.887 0 0 1 3.88-3.867h8.031l-3.909 3.914a.804.804 0 0 0 1.137 1.136l5.285-5.293a.8.8 0 0 0 .001-1.136z" data-name="Path 825" transform="rotate(180 26.21 9.47)"/>
                                            </g>
                                        </g>
                                    </svg>
                                </span> */}
                                {this.state.tab === "searchData" ? <button type="button" className="gsmihs-search-btn" onClick={this.getSearchData} onKeyDown={(e) => e.key == "Enter" ? this.getSearchData : ""}>Search</button> : null}
                            </div>
                            <button type="button" className="gsmihs-clear" onClick={this.props.CloseGlobalSearch}><img src={require('../assets/clearSearch.svg')} /></button>
                        </div>
                        <div className="gsmi-body">
                            <div className="global-search-tab">
                                <ul className="nav nav-tabs gst-inner" role="tablist">
                                    <li className="nav-item active" >
                                        <a className="nav-link gsti-btn" href="#searchPages" role="tab" data-toggle="tab" onClick={this.changeTabCS}><img src={require('../assets/page.svg')} /> Search Pages</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link gsti-btn" href="#userActivity" role="tab" data-toggle="tab" onClick={this.changeTabCS}><span className="gstib-img"><img src={require('../assets/userNew.svg')} /><img className="gstibi-flash" src={require('../assets/flash.svg')} /></span> User Activities</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link gsti-btn" href="#searchData" role="tab" data-toggle="tab" onClick={this.changeTabCS}><img src={require('../assets/folder.svg')} />Search Data</a>
                                    </li>
                                    {/* <li className="nav-item">
                                        <a className="nav-link gsti-btn" href="#comments" role="tab" data-toggle="tab"><img src={require('../assets/commentTab.svg')} />Comments</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link gsti-btn" href="#comments2" role="tab" data-toggle="tab"><img src={require('../assets/commentTab.svg')} />Comments</a>
                                    </li> */}
                                    {this.state.tab == "searchData" && <li className="gsmi-filters-button">
                                        {this.state.filter === false ? <button type="button" onClick={(e) => this.showFilter(e)}>Add Filter <img src={require('../assets/filter-icon.svg')} /></button> :
                                        <button type="button" onClick={(e) => this.showFilter(e)}>Close Filter <img src={require('../assets/clearSearch.svg')} /></button>}
                                    </li>}
                                </ul>
                            </div>
                            {this.state.filter === false ?  
                            <div className="gsmi-search-result">
                                <div className="gsmisr-inner">
                                    <span className="gsmisri-searchresult">Results: </span><span>{this.state.tab === "searchPages" ? this.state.searchPages.length : this.state.tab === "userActivity" ? this.state.userActivities.length : this.state.searchData.length} items found</span>
                                </div>
                            </div> : null}
                            <div className="tab-content">
                                <div className="tab-pane fade in active" id="searchPages" role="tabpanel">
                                    <SearchPages search={this.state.search} searchPages={this.state.searchPages} />
                                </div>
                                <div className="tab-pane fade in" id="userActivity" role="tabpanel">
                                    <UserActivity search={this.state.search} userActivities={this.state.userActivities} />
                                </div>
                                <div className="tab-pane fade in" id="searchData" role="tabpanel">
                                    <SearchData search={this.state.search} searchData={this.state.searchData} showFilters={this.state.filter} />
                                </div>
                                {/* <div className="tab-pane fade" id="comments" role="tabpanel">
                                    <div className="gsmi-comments">
                                        <div className="gsmic-search-comment">
                                            <img src={require('../assets/chat.svg')} />
                                            <div className="gsmics-text">
                                                <h3>Search Comments</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
                                            </div>
                                        </div>
                                        <ul className="gsmic-search-result">
                                            <li>
                                                <div className="gsmicsr-details">
                                                    <div className="gsmicsrd-left">
                                                        <span className="gsmicsrdl-icon"><img src={require('../assets/user.svg')} /></span>
                                                        <span className="gsmicsrd-user-name">Sameer Singh</span>
                                                        <p>Please Deliver at the earliest</p>
                                                        <button className="view-full-thread-btn">View full Thread</button>
                                                    </div>
                                                    <div className="gsmicsrd-right">
                                                        <span className="gsmicsrdr-num">#PO000609-0620BSP</span>
                                                        <span className="gsmicsrdr-time">
                                                            <span className="gsmicsrdr-date">23 Jun 2020</span>
                                                            <span className="gsmicsrdrt-time">2 hour ago</span>
                                                        </span>
                                                        <div className="gsmiuair-button">
                                                            <button type="button" className="gsmiuair-mod-btn">Digivend</button>
                                                            <button type="button" className="gsmiuair-btn">Open PO</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="gsmicsr-details">
                                                    <div className="gsmicsrd-left">
                                                        <span className="gsmicsrdl-icon"><img src={require('../assets/user.svg')} /></span>
                                                        <span className="gsmicsrd-user-name">Rachiel Drift</span>
                                                        <p>Please Deliver the order before 10 aug 2020 if possible.</p>
                                                        <button className="view-full-thread-btn">View full Thread</button>
                                                    </div>
                                                    <div className="gsmicsrd-right">
                                                        <span className="gsmicsrdr-num">#PO000609-0620BSP</span>
                                                        <span className="gsmicsrdr-time">
                                                            <span className="gsmicsrdr-date">23 Jun 2020</span>
                                                            <span className="gsmicsrdrt-time">2 hour ago</span>
                                                        </span>
                                                        <div className="gsmiuair-button">
                                                            <button type="button" className="gsmiuair-mod-btn">Digivend</button>
                                                            <button type="button" className="gsmiuair-btn">Open PO</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="gsmicsr-details">
                                                    <div className="gsmicsrd-left">
                                                        <span className="gsmicsrdl-icon"><img src={require('../assets/user.svg')} /></span>
                                                        <span className="gsmicsrd-user-name">Akash Latwal</span>
                                                        <p>Hope You are doing well . Can you please deliver the order before or on 05 April 2020</p>
                                                        <button className="view-full-thread-btn">View full Thread</button>
                                                    </div>
                                                    <div className="gsmicsrd-right">
                                                        <span className="gsmicsrdr-num">#PO000609-0620BSP</span>
                                                        <span className="gsmicsrdr-time">
                                                            <span className="gsmicsrdr-date">23 Jun 2020</span>
                                                            <span className="gsmicsrdrt-time">2 hour ago</span>
                                                        </span>
                                                        <div className="gsmiuair-button">
                                                            <button type="button" className="gsmiuair-mod-btn">Digivend</button>
                                                            <button type="button" className="gsmiuair-btn">Open PO</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="comments2" role="tabpanel">
                                    <div className="gsmi-comments">
                                        <div className="gsmic-search-comment">
                                            <img src={require('../assets/chat.svg')} />
                                            <div className="gsmics-text">
                                                <h3>Search Comments</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                        {/* <div className="gsmi-bottom">
                            <div className="gsmib-lft">
                                <span className="gsmibl-searchresult">Results</span>
                                <span>{this.state.searchPages.length} items found</span>
                            </div>
                            <div className="gsmib-right">
                                <span>Press <span className="bold">ESC</span> to exit</span>
                                <button type="button" onClick={this.props.CloseGlobalSearch}>Close</button>
                            </div>
                        </div> */}
                    </div>
                </div>
            </div>
            {this.state.loading ? <FilterLoader /> : null}
            {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
            {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </React.Fragment>
        )
    }
}
export function mapStateToProps(state) {
    return {
        home: state.home
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GlobalSearch);