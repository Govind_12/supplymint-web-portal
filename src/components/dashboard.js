import React from "react";
import AllStoresOnMap from "./dashboard/allStoresOnMap";
import ProductConsistency from "./dashboard/productConsistency";
import RightSideBar from "../components/rightSideBar";
import openRack from "../assets/open-rack.svg"
import Footer from '../components/footer'
import SideBar from "../components/sidebar";
import TopStock from './dashboard/topStocks'
import BraedCrumps from "../components/breadCrumps";
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rightbar: false,
      openRightBar: false,
      sideBar: true
    };
  }
  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }

  render() {
    return (
      <div>

        <div className="col-md-12 col-sm-12 col-xs-6 pad-0 m-top-35">
          <TopStock />
          {/* <AllStoresOnMap /> */}
          <ProductConsistency />
        </div>
        {/* <TopStock /> */}
      </div>
    );
  }
}

export default Dashboard;
