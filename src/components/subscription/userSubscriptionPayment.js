import React from "react";
import Rupee from "../../assets/rupeeNew.svg";
import Coupon from "../../assets/coupon.svg";


class UserSubscriptionPayment extends React.Component {
    render () {
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new2"></div>
                <div className="modal-content user-subscription-payment-modal">
                    <div className="col-lg-12 pad-0">
                        <div className="uspm-head">
                            <h4>User Subscription Payment</h4>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 9.155 9.155">
                                    <g id="prefix__not_inlcude" data-name="not inlcude" transform="translate(-3.2 -64.15)">
                                        <path fill="#ccd8ed" id="prefix__Path_137" d="M69.653 68.728l3.46-3.461a.654.654 0 0 0-.925-.925l-3.46 3.458-3.461-3.46a.654.654 0 0 0-.925.925l3.461 3.46-3.46 3.461a.654.654 0 0 0 .924.925l3.461-3.461 3.461 3.46a.654.654 0 0 0 .925-.924z" data-name="Path 137" transform="translate(-60.95)"/>
                                    </g>
                                </svg>
                            </span>
                        </div>
                        <div className="uspm-body">
                            <div className="uspmb-select-n-license">
                                <span>Select No. of Licence</span>
                                <div className="uspm-snl-btn">
                                    <button className="decrease">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                            <path fill="#853ee5" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"/>
                                        </svg>
                                    </button>
                                    <span className="uspm-snl-no">01</span>
                                    <button className="increase">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                            <path fill="#853ee5" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div className="uspm-price">
                                <div className="uspm-price-top">
                                    <span className="uspm-pt-item">Base price per License</span>
                                    <span className="uspm-pt-date">( x12 months )</span>
                                    <span className="uspm-pt-amount"><img src={Rupee} /> 14,988</span>
                                    <span className="uspm-pt-item">GST</span>
                                    <span className="uspm-pt-date">( 18% )</span>
                                    <span className="uspm-pt-amount"><img src={Rupee} /> 2,700</span>
                                </div>
                                <div className="uspm-price-bottom">
                                    <span className="uspm-pb-item">Grand total</span>
                                    <span className="uspm-pb-amount"><img src={Rupee} /> 17,786</span>
                                </div>
                            </div>
                            <div className="uspm-b-input">
                                <input type="text" placeholder="DISC15" /><button type="button">Apply</button>
                                <span className="uspm-b-close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.155" height="9.155" viewBox="0 0 9.155 9.155">
                                        <g id="prefix__not_inlcude" data-name="not inlcude" transform="translate(-3.2 -64.15)">
                                            <path fill="#2d3748" id="prefix__Path_137" d="M69.653 68.728l3.46-3.461a.654.654 0 0 0-.925-.925l-3.46 3.458-3.461-3.46a.654.654 0 0 0-.925.925l3.461 3.46-3.46 3.461a.654.654 0 0 0 .924.925l3.461-3.461 3.461 3.46a.654.654 0 0 0 .925-.924z" data-name="Path 137" transform="translate(-60.95)"/>
                                        </g>
                                    </svg>
                                </span>
                                <div className="uspm-coupon">
                                    <p>Coupon Applied</p>
                                    <span className="coupon-code">DISC15 <img src={Coupon} /></span> <span className="remove-coupon">Remove</span>
                                </div>
                            </div>
                            <div className="uspm-process-btn">
                                <button type="button">Proceed to Payment</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserSubscriptionPayment;