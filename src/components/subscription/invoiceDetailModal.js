import React from "react";
import Rupee from "../../assets/rupeeNew.svg";

class InvoiceDetails extends React.Component {
    render () {
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new2"></div>
                <div className="modal-content invoice-detail-modal">
                    <div className="col-lg-12 pad-0">
                        <div className="col-lg-8">
                            <div className="idm-company-details">
                                <span className="idm-cd-name">
                                    <h6>Company Name</h6>
                                    <p>XYZ Private Limited</p>
                                </span>
                                <span className="idm-cd-address">
                                    <h6>Billing Address</h6>
                                    <p>C-401-403, Unitech Business Zone</p>
                                    <p>Gurugram sector-50, Haryana</p>
                                    <p>Pincode - 100891</p>
                                </span>
                                <span className="idm-cd-contact">
                                    <span className="idm-cd-contact-inner">
                                        <h6>GSTIN</h6>
                                        <p>7851652145214121</p>
                                    </span>
                                    <span className="idm-cd-contact-inner">
                                        <h6>Contact Number</h6>
                                        <p>+91525623505</p>
                                    </span>
                                    <span className="idm-cd-contact-inner">
                                        <h6>Email</h6>
                                        <p>xyz@gmail.com</p>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="idm-company-details">
                                <span className="idm-cd-invoice-no">Invoice No- <span>#0936235</span></span>
                                <span className="idm-cd-time">
                                    <h6>Date</h6>
                                    <p>29 April 2020</p>
                                </span>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="idm-table">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th><label>S.no</label></th>
                                            <th><label>Product / Service Name</label></th>
                                            <th><label>IGST</label></th>
                                            <th><label>CGST</label></th>
                                            <th><label>CESS</label></th>
                                            <th><label>Discount</label></th>
                                            <th><label>Amount</label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><label>01</label></td>
                                            <td><label className="bold">1 year Standard Plan</label></td>
                                            <td><label><img src={Rupee} /> 340.00</label>
                                                <p>@18%</p>
                                            </td>
                                            <td><label><img src={Rupee} /> 340.00</label>
                                                <p>@18%</p>
                                            </td>
                                            <td><label>0</label></td>
                                            <td><label><img src={Rupee} /> 560</label>
                                                <p className="clr-green">- Discount 5%</p>
                                            </td>
                                            <td><label><img src={Rupee} /> 8009</label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" className="idm-tgrand-total">
                                                <label>Grand Total</label>
                                                <p><img src={Rupee} /> 8009</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default InvoiceDetails;