import React from "react";
import PurchaseOrderForm from "./purchaseOrder/purchaseOrderForm";
import GenericPurchaseOrder from "./purchaseOrder/genericPurchaseOrder"
import FilterLoader from './loaders/filterLoader';
import RequestSuccess from "./loaders/requestSuccess";
import RequestError from "./loaders/requestError";
import ItemFound from "./loaders/itemFoundModal";
import PoWithUpload from "./purchaseOrder/poWithUpload";
class PO extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contactSubmit: false,
      supplierState: [],
      loader: false,
      errorCode: "",
      code: "",
      successMessage: "",
      success: false,
      alert: false,
      errorMessage: "",
      moduleError: "procurement"
    };
  }

  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false,
      errorMessage: ""
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }

  closeItemNotFound() {
    this.setState({
      itemNotFound: false
    })
  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
    if(this.state.contactSubmit){
      window.location.reload('false')
    }
  }
  closeConfirmModal(e) {
    this.setState({
      confirmModal: !this.state.confirmModal,
    })
  }

  resetData() {
    this.refs.child != null ? this.refs.child.resetData() : null
  }

  deleteRows(id) {
    this.refs.child != null ? this.refs.child.deleteRows(id) : null
  }
  confirmModal(idx) {
    this.setState({
      confirmModal: true,
      headerMsg: "Are you sure you want to delete row?",
      paraMsg: "Click confirm to continue.",
      rowDelete: "row Delete",
      rowsId: idx
    })
  }

  onResetData() {
    this.setState({
      confirmModal: true,
      headerMsg: "Are you sure to reset the form?",
      paraMsg: "Click confirm to continue.",
      pi: "pi reset"
    })
  }

  componentDidMount() {
    this.props.poSaveDraftClear()
  }

  componentWillMount() {


    // if(!this.props.purchaseIndent.getPurchaseTerm.isSuccess){
    //   this.props.getPurchaseTermRequest();
    // }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.purchaseIndent.getAgentName.isSuccess) {
      this.setState({
        loader: false
      })
    } else if (nextProps.purchaseIndent.getAgentName.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getAgentName.message.error == undefined ? undefined : nextProps.purchaseIndent.getAgentName.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.getAgentName.message.error == undefined ? undefined : nextProps.purchaseIndent.getAgentName.message.error.errorCode,
        code: nextProps.purchaseIndent.getAgentName.message.status,
        alert: true,
        loader: false
      })
    }

    if (nextProps.purchaseIndent.getTransporter.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getTransporterRequest()
    } else if (nextProps.purchaseIndent.getTransporter.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getTransporter.message.error == undefined ? undefined : nextProps.purchaseIndent.getTransporter.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getTransporter.message.error == undefined ? undefined : nextProps.purchaseIndent.getTransporter.message.error.errorCode,
        code: nextProps.purchaseIndent.getTransporter.message.status,
        alert: true,
        loader: false

      })
      this.props.getTransporterRequest()
    }
    else if (!nextProps.purchaseIndent.getTransporter.isLoading) {
      this.setState({
        loader: false,

      })
    }





    if (nextProps.purchaseIndent.supplier.isSuccess) {
      this.setState({
        supplierState: nextProps.purchaseIndent.supplier.data.resource,


        loader: false,
        alert: false
      })

      this.props.supplierClear();
    } else if (nextProps.purchaseIndent.supplier.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.supplier.message.error == undefined ? undefined : nextProps.purchaseIndent.supplier.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.supplier.message.error == undefined ? undefined : nextProps.purchaseIndent.supplier.message.error.errorCode,
        code: nextProps.purchaseIndent.supplier.message.status,
        alert: true,

        loader: false

      })
      this.props.supplierClear();
    }
    else if (!nextProps.purchaseIndent.supplier.isLoading) {
      this.setState({
        loader: false
      })
    }


    if (nextProps.purchaseIndent.divisionData.isSuccess) {
      this.setState({

        divisionState: this.props.purchaseIndent.divisionData.data.resource,

        loader: false
      })
      this.props.divisionSectionDepartmentRequest();


    } else if (nextProps.purchaseIndent.divisionData.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorCode,
        code: nextProps.purchaseIndent.divisionData.message.status,
        alert: true,

        loader: false

      })
      this.props.divisionSectionDepartmentRequest();
    }

    if (nextProps.purchaseIndent.leadTime.isError) {

      this.setState({


        errorMessage: nextProps.purchaseIndent.leadTime.message.error == undefined ? undefined : nextProps.purchaseIndent.leadTime.message.error.errorMessage,


        errorCode: nextProps.purchaseIndent.leadTime.message.error == undefined ? undefined : nextProps.purchaseIndent.leadTime.message.error.errorCode,
        code: nextProps.purchaseIndent.leadTime.message.status,



        alert: true,

        loader: false
      })
      this.props.leadTimeClear();



    }
    else if (nextProps.purchaseIndent.leadTime.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.leadTimeClear();
    }




    if (nextProps.purchaseIndent.purchaseTerm.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.purchaseTerm.message.error == undefined ? undefined : nextProps.purchaseIndent.purchaseTerm.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.purchaseTerm.message.error == undefined ? undefined : nextProps.purchaseIndent.purchaseTerm.message.error.errorCode,
        code: nextProps.purchaseIndent.purchaseTerm.message.status,
        alert: true,

        loader: false
      })
      this.props.purchaseTermClear()
    } else if (nextProps.purchaseIndent.purchaseTerm.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.purchaseTermClear()
    }





    if (nextProps.purchaseIndent.color.isError) {
      this.setState({

        errorMessage: nextProps.purchaseIndent.color.message.error == undefined ? undefined : nextProps.purchaseIndent.color.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.color.message.error == undefined ? undefined : nextProps.purchaseIndent.color.message.error.errorCode,
        code: nextProps.purchaseIndent.color.message.status,
        alert: true,
        loader: false
      })

      this.props.colorRequest();
    } else if (nextProps.purchaseIndent.color.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.colorRequest();
    }
    else if (!nextProps.purchaseIndent.color.isLoading) {
      this.setState({
        loader: false,

      })
    }


    if (nextProps.purchaseIndent.lineItem.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.lineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.lineItem.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.lineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.lineItem.message.error.errorCode,
        code: nextProps.purchaseIndent.lineItem.message.status,
        alert: true,

        loader: false
      })
      this.props.lineItemRequest();
    } else if (nextProps.purchaseIndent.lineItem.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.lineItemRequest();
    }


    if (nextProps.purchaseIndent.udfType.isSuccess) {
      this.setState({

        loader: false

      })
      this.props.udfTypeRequest();
    }

    else if (nextProps.purchaseIndent.udfType.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.udfType.message != "" ? nextProps.purchaseIndent.udfType.message.error == undefined ? undefined : nextProps.purchaseIndent.udfType.message.error.errorMessage : "",
        errorCode: nextProps.purchaseIndent.udfType.message != "" ? nextProps.purchaseIndent.udfType.message.error == undefined ? undefined : nextProps.purchaseIndent.udfType.message.error.errorCode : "",
        code: nextProps.purchaseIndent.udfType.message != "" ? nextProps.purchaseIndent.udfType.message.status : undefined,
        alert: true,
        success: false,
        loader: false
      })
      this.props.udfTypeRequest();
    }
    // else if (!nextProps.purchaseIndent.udfType.isLoading && !nextProps.purchaseIndent.udfType.isError && !nextProps.purchaseIndent.udfType.isSuccess){
    //   this.setState({
    //     loader:false,
    //     success:false,
    //     alert:false
    //   })
    // } 


    if (nextProps.purchaseIndent.articlePo.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.articlePo.message.error == undefined ? undefined : nextProps.purchaseIndent.articlePo.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.articlePo.message.error == undefined ? undefined : nextProps.purchaseIndent.articlePo.message.error.errorCode,
        code: nextProps.purchaseIndent.articlePo.message.status,
        alert: true,
        success: false,
        loader: false
      })
      this.props.articlePoClear();
    }
    else if (nextProps.purchaseIndent.articlePo.isSuccess) {
      this.setState({
        alert: false,

        loader: false
      })
      this.props.articlePoClear();
    }

    if (nextProps.purchaseIndent.otb.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.otb.message.error == undefined ? undefined : nextProps.purchaseIndent.otb.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.otb.message.error == undefined ? undefined : nextProps.purchaseIndent.otb.message.error.errorCode,
        code: nextProps.purchaseIndent.otb.message.status,
        alert: true,
        success: false,
        loader: false
      })
      this.props.otbClear();
    }
    else if (nextProps.purchaseIndent.otb.isSuccess) {
      this.setState({
        alert: false,

        loader: false
      })
      // this.props.otbClear();
    }


    if (nextProps.purchaseIndent.markUp.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.markUpClear();
    }
    else if (nextProps.purchaseIndent.markUp.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.markUp.message.error == undefined ? undefined : nextProps.purchaseIndent.markUp.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.markUp.message.error == undefined ? undefined : nextProps.purchaseIndent.markUp.message.error.errorCode,
        code: nextProps.purchaseIndent.markUp.message.status,
        alert: true,

        loader: false
      })
      this.props.markUpClear();
    }
    else if (!nextProps.purchaseIndent.markUp.isLoading) {
      this.setState({
        loader: false,

      })
    }



    if (nextProps.purchaseIndent.poItemcode.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.poItemcode.message.error == undefined ? undefined : nextProps.purchaseIndent.poItemcode.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.poItemcode.message.error == undefined ? undefined : nextProps.purchaseIndent.poItemcode.message.error.errorCode,
        code: nextProps.purchaseIndent.poItemcode.message.status,
        alert: true,

        loader: false
      })
      this.props.poItemcodeClear();
    } else if (nextProps.purchaseIndent.poItemcode.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.poItemcodeClear();
    }



    if (nextProps.purchaseIndent.getUdfMapping.isSuccess) {
      this.setState({


        loader: false
      })
      this.props.getUdfMappingClear();

    } else if (nextProps.purchaseIndent.getUdfMapping.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorCode,
        code: nextProps.purchaseIndent.getUdfMapping.message.status,
        alert: true,

        loader: false

      })
      this.props.getUdfMappingClear();

    } else if (!nextProps.purchaseIndent.getUdfMapping.isLoading) {
      this.setState({
        loader: false
      })

    }



    if (nextProps.purchaseIndent.multipleLineItem.isSuccess) {
      this.setState({


        // loader: false
      })
      this.props.multipleLineItemClear();

    } else if (nextProps.purchaseIndent.multipleLineItem.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.multipleLineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.multipleLineItem.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.multipleLineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.multipleLineItem.message.error.errorCode,
        code: nextProps.purchaseIndent.multipleLineItem.message.status,
        alert: true,

        loader: false

      })
      this.props.multipleLineItemClear();

    } else if (!nextProps.purchaseIndent.multipleLineItem.isLoading) {
      this.setState({
        loader: false
      })

    }
    if (nextProps.purchaseIndent.getMultipleMargin.isSuccess) {
      this.setState({


        loader: false
      })
      this.props.getMultipleMarginClear();

    } else if (nextProps.purchaseIndent.getMultipleMargin.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getMultipleMargin.message.error == undefined ? undefined : nextProps.purchaseIndent.getMultipleMargin.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getMultipleMargin.message.error == undefined ? undefined : nextProps.purchaseIndent.getMultipleMargin.message.error.errorCode,
        code: nextProps.purchaseIndent.getMultipleMargin.message.status,
        alert: true,

        loader: false

      })
      this.props.getMultipleMarginClear();

    } else if (!nextProps.purchaseIndent.getMultipleMargin.isLoading) {
      this.setState({
        loader: false
      })

    }


    if (nextProps.purchaseIndent.getCatDescUdf.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.getCatDescUdfRequest();

    } else if (nextProps.purchaseIndent.getCatDescUdf.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getCatDescUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.getCatDescUdf.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getCatDescUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.getCatDescUdf.message.error.errorCode,
        code: nextProps.purchaseIndent.getCatDescUdf.message.status,
        alert: true,

        loader: false

      })
      this.props.getCatDescUdfRequest();
    } else if (!nextProps.purchaseIndent.getCatDescUdf.isLoading) {
      this.setState({
        loader: false
      })

    }


    if (nextProps.purchaseIndent.poSize.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.poSizeRequest();

    } else if (nextProps.purchaseIndent.poSize.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.poSize.message.error == undefined ? undefined : nextProps.purchaseIndent.poSize.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.poSize.message.error == undefined ? undefined : nextProps.purchaseIndent.poSize.message.error.errorCode,
        code: nextProps.purchaseIndent.poSize.message.status,
        alert: true,

        loader: false

      })
      this.props.poSizeRequest();
    } else if (!nextProps.purchaseIndent.poSize.isLoading) {
      this.setState({
        loader: false
      })

    }



    if (nextProps.purchaseIndent.itemUdfMapping.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.itemUdfMappingRequest();

    } else if (nextProps.purchaseIndent.itemUdfMapping.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.itemUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.itemUdfMapping.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.itemUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.itemUdfMapping.message.error.errorCode,
        code: nextProps.purchaseIndent.itemUdfMapping.message.status,
        alert: true,

        loader: false

      })
      this.props.itemUdfMappingRequest();
    } else if (!nextProps.purchaseIndent.itemUdfMapping.isLoading) {
      this.setState({
        loader: false
      })

    }




    if (nextProps.purchaseIndent.poCreate.isSuccess) {

      this.setState({

        success: true,
        loader: false,


        successMessage: nextProps.purchaseIndent.poCreate.data.message,
      })
      this.props.poCreateRequest();

    } else if (nextProps.purchaseIndent.poCreate.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.poCreate.message.error == undefined ? undefined : nextProps.purchaseIndent.poCreate.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.poCreate.message.error == undefined ? undefined : nextProps.purchaseIndent.poCreate.message.error.errorCode,
        code: nextProps.purchaseIndent.poCreate.message.status,
        alert: true,

        loader: false

      })
      this.props.poCreateRequest();
    }


    if (nextProps.purchaseIndent.loadIndent.isSuccess) {
      this.setState({
        loader: false

      })
      this.props.loadIndentClear()
    }

    else if (nextProps.purchaseIndent.loadIndent.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.loadIndent.message.error == undefined ? undefined : nextProps.purchaseIndent.loadIndent.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.loadIndent.message.error == undefined ? undefined : nextProps.purchaseIndent.loadIndent.message.error.errorCode,
        code: nextProps.purchaseIndent.loadIndent.message.status,
        alert: true,
        loader: false,
        //  success: false

      })
      this.props.loadIndentClear()
    } else if (!nextProps.purchaseIndent.loadIndent.isLoading) {
      this.setState({
        loader: false,

      })
    }



    if (nextProps.purchaseIndent.getItemDetailsValue.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.getItemDetailsValueRequest();

    } else if (nextProps.purchaseIndent.getItemDetailsValue.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getItemDetailsValue.message.error == undefined ? undefined : nextProps.purchaseIndent.getItemDetailsValue.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getItemDetailsValue.message.error == undefined ? undefined : nextProps.purchaseIndent.getItemDetailsValue.message.error.errorCode,
        code: nextProps.purchaseIndent.getItemDetailsValue.message.status,
        alert: true,

        loader: false

      })
      this.props.getItemDetailsValueRequest();
    } else if (!nextProps.purchaseIndent.getItemDetailsValue.isLoading) {
      this.setState({
        loader: false,

      })
    }


    if (nextProps.purchaseIndent.getItemDetails.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.getItemDetailsRequest();

    } else if (nextProps.purchaseIndent.getItemDetails.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getItemDetails.message.error == undefined ? undefined : nextProps.purchaseIndent.getItemDetails.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getItemDetails.message.error == undefined ? undefined : nextProps.purchaseIndent.getItemDetails.message.error.errorCode,
        code: nextProps.purchaseIndent.getItemDetails.message.status,
        alert: true,

        loader: false

      })
      this.props.getItemDetailsRequest();
    } else if (!nextProps.purchaseIndent.getItemDetails.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.getPoData.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.getPoDataClear();

    } else if (nextProps.purchaseIndent.getPoData.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getPoData.message.error == undefined ? undefined : nextProps.purchaseIndent.getPoData.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getPoData.message.error == undefined ? undefined : nextProps.purchaseIndent.getPoData.message.error.errorCode,
        code: nextProps.purchaseIndent.getPoData.message.status,
        alert: true,

        loader: false

      })
      this.props.getPoDataClear();
    } else if (!nextProps.purchaseIndent.getPoData.isLoading) {
      this.setState({
        loader: false,

      })
    }


    if (nextProps.purchaseIndent.selectVendor.isSuccess) {

      this.setState({
        loader: false
      })
      this.props.selectVendorRequest();

    } else if (nextProps.purchaseIndent.selectVendor.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.selectVendor.message.error == undefined ? undefined : nextProps.purchaseIndent.selectVendor.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.selectVendor.message.error == undefined ? undefined : nextProps.purchaseIndent.selectVendor.message.error.errorCode,
        code: nextProps.purchaseIndent.selectVendor.message.status,
        alert: true,

        loader: false

      })
      this.props.selectVendorClear();
    } else if (!nextProps.purchaseIndent.selectVendor.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.selectOrderNumber.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.selectOrderNumberRequest();

    } else if (nextProps.purchaseIndent.selectOrderNumber.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.selectOrderNumber.message.error == undefined ? undefined : nextProps.purchaseIndent.selectOrderNumber.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.selectOrderNumber.message.error == undefined ? undefined : nextProps.purchaseIndent.selectOrderNumber.message.error.errorCode,
        code: nextProps.purchaseIndent.selectOrderNumber.message.status,
        alert: true,

        loader: false

      })

      this.props.selectOrderNumberRequest();
    } else if (!nextProps.purchaseIndent.selectOrderNumber.isLoading) {
      this.setState({
        loader: false,

      })
    }



    if (nextProps.purchaseIndent.discount.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.discountClear();

    } else if (nextProps.purchaseIndent.discount.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.discount.message.error == undefined ? undefined : nextProps.purchaseIndent.discount.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.discount.message.error == undefined ? undefined : nextProps.purchaseIndent.discount.message.error.errorCode,
        code: nextProps.purchaseIndent.discount.message.status,
        alert: true,

        loader: false

      })

      this.props.discountClear();
    } else if (!nextProps.purchaseIndent.discount.isLoading) {
      this.setState({
        loader: false,

      })
    }


    if (nextProps.purchaseIndent.departmentSetBased.isSuccess) {

      this.setState({
        loader: false
      })
      this.props.departmentSetBasedRequest();

    } else if (nextProps.purchaseIndent.departmentSetBased.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.departmentSetBased.message.error == undefined ? undefined : nextProps.purchaseIndent.departmentSetBased.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.departmentSetBased.message.error == undefined ? undefined : nextProps.purchaseIndent.departmentSetBased.message.error.errorCode,
        code: nextProps.purchaseIndent.departmentSetBased.message.status,
        alert: true,

        loader: false

      })

      this.props.departmentSetBasedRequest();
    }
    else if (!nextProps.purchaseIndent.departmentSetBased.isLoading) {
      this.setState({
        loader: false,

      })
    }
    if (nextProps.purchaseIndent.setBased.isSuccess) {

      this.setState({
        loader: false
      })
      this.props.setBasedRequest();

    } else if (nextProps.purchaseIndent.setBased.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.setBased.message.error == undefined ? undefined : nextProps.purchaseIndent.setBased.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.setBased.message.error == undefined ? undefined : nextProps.purchaseIndent.setBased.message.error.errorCode,
        code: nextProps.purchaseIndent.setBased.message.status,
        alert: true,

        loader: false

      })

      this.props.setBasedRequest();
    } else if (!nextProps.purchaseIndent.setBased.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.procurementSite.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.procurementSiteClear();
    } else if (nextProps.purchaseIndent.procurementSite.isError) {
      this.setState(
        {
          loader: false,
          errorMessage: nextProps.purchaseIndent.procurementSite.message.error == undefined ? undefined : nextProps.purchaseIndent.procurementSite.message.error.errorMessage,
          errorCode: nextProps.purchaseIndent.procurementSite.message.error == undefined ? undefined : nextProps.purchaseIndent.procurementSite.message.error.errorCode,
          code: nextProps.purchaseIndent.procurementSite.message.status,
          alert: true
        }
      )
      this.props.procurementSiteClear();
    } else if (!nextProps.purchaseIndent.procurementSite.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.hsnCode.isSuccess) {

      this.setState({
        loader: false
      })
      this.props.hsnCodeRequest();

    } else if (nextProps.purchaseIndent.hsnCode.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.hsnCode.message.error == undefined ? undefined : nextProps.purchaseIndent.hsnCode.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.hsnCode.message.error == undefined ? undefined : nextProps.purchaseIndent.hsnCode.message.error.errorCode,
        code: nextProps.purchaseIndent.hsnCode.message.status,
        alert: true,

        loader: false

      })

      this.props.hsnCodeRequest();
    } else if (!nextProps.purchaseIndent.hsnCode.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.marginRule.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.marginRuleClear();
    }

    else if (nextProps.purchaseIndent.marginRule.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.marginRule.message.error == undefined ? undefined : nextProps.purchaseIndent.marginRule.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.marginRule.message.error == undefined ? undefined : nextProps.purchaseIndent.marginRule.message.error.errorCode,
        code: nextProps.purchaseIndent.marginRule.message.status,
        alert: true,
        loader: false
      })
      this.props.marginRuleClear();



    } else if (!nextProps.purchaseIndent.marginRule.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.getPoItemCode.isSuccess) {
      this.setState({

        loader: false
      })
      // this.props.getPoItemCodeClear();
    }

    else if (nextProps.purchaseIndent.getPoItemCode.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.getPoItemCode.message.error == undefined ? undefined : nextProps.purchaseIndent.getPoItemCode.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.getPoItemCode.message.error == undefined ? undefined : nextProps.purchaseIndent.getPoItemCode.message.error.errorCode,
        code: nextProps.purchaseIndent.getPoItemCode.message.status,
        alert: true,
        loader: false
      })
      this.props.getPoItemCodeClear();



    } else if (!nextProps.purchaseIndent.getPoItemCode.isLoading) {
      this.setState({
        loader: false,

      })
    }
    if (nextProps.purchaseIndent.poItemBarcode.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.poItemBarcodeClear();
    }

    else if (nextProps.purchaseIndent.poItemBarcode.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.poItemBarcode.message.error == undefined ? undefined : nextProps.purchaseIndent.poItemBarcode.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.poItemBarcode.message.error == undefined ? undefined : nextProps.purchaseIndent.poItemBarcode.message.error.errorCode,
        code: nextProps.purchaseIndent.poItemBarcode.message.status,
        alert: true,
        loader: false
      })
      this.props.poItemBarcodeClear();



    } else if (!nextProps.purchaseIndent.poItemBarcode.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.getAllCity.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.getAllCityClear();
    }

    else if (nextProps.purchaseIndent.getAllCity.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.getAllCity.message.error == undefined ? undefined : nextProps.purchaseIndent.getAllCity.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.getAllCity.message.error == undefined ? undefined : nextProps.purchaseIndent.getAllCity.message.error.errorCode,
        code: nextProps.purchaseIndent.getAllCity.message.status,
        alert: true,
        loader: false
      })
      this.props.getAllCityClear();



    } else if (!nextProps.purchaseIndent.getAllCity.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.poArticle.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.poArticleClear();
    }

    else if (nextProps.purchaseIndent.poArticle.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.poArticle.message.error == undefined ? undefined : nextProps.purchaseIndent.poArticle.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.poArticle.message.error == undefined ? undefined : nextProps.purchaseIndent.poArticle.message.error.errorCode,
        code: nextProps.purchaseIndent.poArticle.message.status,
        alert: true,
        loader: false
      })
      this.props.poArticleClear();



    } else if (!nextProps.purchaseIndent.poArticle.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.size.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.size.message.error == undefined ? undefined : nextProps.purchaseIndent.size.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.size.message.error == undefined ? undefined : nextProps.purchaseIndent.size.message.error.errorCode,
        code: nextProps.purchaseIndent.size.message.status,
        alert: true,
        loader: false
      })
      this.props.sizeRequest();

    } else if (nextProps.purchaseIndent.size.isSuccess) {
      this.setState({
        loader: false,
      })
      this.props.sizeRequest();

    }
    else if (!nextProps.purchaseIndent.size.isLoading) {
      this.setState({
        loader: false,
      })
    }




    if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.poRadioValidationClear();
    }

    else if (nextProps.purchaseIndent.poRadioValidation.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.poRadioValidation.message.error == undefined ? undefined : nextProps.purchaseIndent.poRadioValidation.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.poRadioValidation.message.error == undefined ? undefined : nextProps.purchaseIndent.poRadioValidation.message.error.errorCode,
        code: nextProps.purchaseIndent.poRadioValidation.message.status,
        alert: true,
        loader: false
      })
      this.props.poRadioValidationClear();



    } else if (!nextProps.purchaseIndent.poRadioValidation.isLoading) {

      this.setState({
        loader: false


      })
    }

    if (nextProps.purchaseIndent.gen_IndentBased.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.gen_IndentBasedClear();
    } else if (nextProps.purchaseIndent.gen_IndentBased.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.gen_IndentBased.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_IndentBased.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.gen_IndentBased.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_IndentBased.message.error.errorCode,
        code: nextProps.purchaseIndent.gen_IndentBased.message.status,
        alert: true,
        loader: false
      })
      this.props.gen_IndentBasedClear();



    } else if (!nextProps.purchaseIndent.gen_IndentBased.isLoading) {

      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.save_edited_po.isSuccess) {
      this.setState({
        contactSubmit: true,
        success: true,
        loader: false,
        successMessage: nextProps.purchaseIndent.save_edited_po.data.message,
      })
    }

    else if (nextProps.purchaseIndent.save_edited_po.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.save_edited_po.message.error == undefined ? undefined : nextProps.purchaseIndent.save_edited_po.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.save_edited_po.message.error == undefined ? undefined : nextProps.purchaseIndent.save_edited_po.message.error.errorCode,
        code: nextProps.purchaseIndent.save_edited_po.message.status,
        alert: true,
        loader: false
      })



    } else if (!nextProps.purchaseIndent.save_edited_po.isLoading) {

      this.setState({
        loader: false,

      })
    }



    if (nextProps.purchaseIndent.gen_SetBased.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.gen_SetBasedClear();
    }

    else if (nextProps.purchaseIndent.gen_SetBased.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.gen_SetBased.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_SetBased.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.gen_SetBased.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_SetBased.message.error.errorCode,
        code: nextProps.purchaseIndent.gen_SetBased.message.status,
        alert: true,
        loader: false
      })
      this.props.gen_SetBasedClear();



    } else if (!nextProps.purchaseIndent.gen_SetBased.isLoading) {

      this.setState({
        loader: false,

      })
    }



    if (nextProps.purchaseIndent.gen_PurchaseOrder.isSuccess) {
      this.setState({
        contactSubmit: true,
        success: true,
        loader: false,


        successMessage: nextProps.purchaseIndent.gen_PurchaseOrder.data.message,
      })
    }

    else if (nextProps.purchaseIndent.gen_PurchaseOrder.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.gen_PurchaseOrder.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_PurchaseOrder.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.gen_PurchaseOrder.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_PurchaseOrder.message.error.errorCode,
        code: nextProps.purchaseIndent.gen_PurchaseOrder.message.status,
        alert: true,
        loader: false
      })
      



    } else if (!nextProps.purchaseIndent.gen_PurchaseOrder.isLoading) {

      this.setState({
        loader: false,

      })
    }

    if (!nextProps.purchaseIndent.po_edit_data.isLoading) {
      this.setState({

        loader: false
      })
      // this.props.poEditClear()
    }
    if (nextProps.purchaseIndent.po_edit_data.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.po_edit_data.message.error == undefined ? undefined : nextProps.purchaseIndent.po_edit_data.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.po_edit_data.message.error == undefined ? undefined : nextProps.purchaseIndent.po_edit_data.message.error.errorCode,
        code: nextProps.purchaseIndent.po_edit_data.message.status,
        alert: true,
        loader: false
      })
      this.props.poEditClear()
    }

    if (!nextProps.purchaseIndent.get_draft_data.isLoading) {
      this.setState({

        loader: false
      })
      // this.props.getDraftClear()
    }

    if (nextProps.purchaseIndent.get_draft_data.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.get_draft_data.message.error == undefined ? undefined : nextProps.purchaseIndent.get_draft_data.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.get_draft_data.message.error == undefined ? undefined : nextProps.purchaseIndent.get_draft_data.message.error.errorCode,
        code: nextProps.purchaseIndent.get_draft_data.message.status,
        alert: true,
        loader: false
      })
      // this.props.getDraftClear()
    }

    if (nextProps.purchaseIndent.save_draft_po.isSuccess) {
      console.log(nextProps.purchaseIndent.save_draft_po)
      if (nextProps.purchaseIndent.save_draft_po.data.modal == "modal") {
        this.setState({
          successMessage: `Your Draft No.  ${nextProps.purchaseIndent.save_draft_po.data.resource.orderNo} has been saved`,
          loader: false,
          success: true,
        })
      } else {
        this.setState({ loader: false })
      }
      this.props.poSaveDraftClear()
    } else if (nextProps.purchaseIndent.save_draft_po.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.purchaseIndent.save_draft_po.message.status,
        errorCode: nextProps.purchaseIndent.save_draft_po.message.error == undefined ? undefined : nextProps.purchaseIndent.save_draft_po.message.error.errorCode,
        errorMessage: nextProps.purchaseIndent.save_draft_po.message.error == undefined ? undefined : nextProps.purchaseIndent.save_draft_po.message.error.errorMessage
      })
      this.props.poSaveDraftClear()
    }

    if (nextProps.purchaseIndent.discardPoPiData.isSuccess) {

      this.setState({
        successMessage: nextProps.purchaseIndent.discardPoPiData.data.message,
        loader: false,
        success: true,
      })
      this.props.discardPoPiDataClear()
    } else if (nextProps.purchaseIndent.discardPoPiData.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.purchaseIndent.discardPoPiData.message.status,
        errorCode: nextProps.purchaseIndent.discardPoPiData.message.error == undefined ? undefined : nextProps.purchaseIndent.discardPoPiData.message.error.errorCode,
        errorMessage: nextProps.purchaseIndent.discardPoPiData.message.error == undefined ? undefined : nextProps.purchaseIndent.discardPoPiData.message.error.errorMessage
      })
      this.props.discardPoPiDataClear()
    }

    if (nextProps.purchaseIndent.save_edited_po.isLoading || nextProps.purchaseIndent.size.isLoading || nextProps.purchaseIndent.poArticle.isLoading || nextProps.purchaseIndent.purchaseTerm.isLoading || nextProps.purchaseIndent.getTransporter.isLoading || nextProps.purchaseIndent.procurementSite.isLoading ||
      nextProps.purchaseIndent.color.isLoading || nextProps.purchaseIndent.udfType.isLoading || nextProps.purchaseIndent.setBased.isLoading ||
      nextProps.purchaseIndent.supplier.isLoading || nextProps.purchaseIndent.multipleLineItem.isLoading || nextProps.purchaseIndent.getMultipleMargin.isLoading ||
      nextProps.purchaseIndent.leadTime.isLoading || nextProps.purchaseIndent.divisionData.isLoading || nextProps.purchaseIndent.discount.isLoading || nextProps.purchaseIndent.poRadioValidation.isLoading
      || nextProps.purchaseIndent.articlePo.isLoading || nextProps.purchaseIndent.poItemcode.isLoading || nextProps.purchaseIndent.poCreate.isLoading
      || nextProps.purchaseIndent.loadIndent.isLoading || nextProps.purchaseIndent.getCatDescUdf.isLoading || nextProps.purchaseIndent.gen_PurchaseOrder.isLoading ||
      nextProps.purchaseIndent.gen_SetBased.isLoading || nextProps.purchaseIndent.gen_IndentBased.isLoading ||
      nextProps.purchaseIndent.itemUdfMapping.isLoading || nextProps.purchaseIndent.getUdfMapping.isLoading
      || nextProps.purchaseIndent.getItemDetailsValue.isLoading || nextProps.purchaseIndent.hsnCode.isLoading || nextProps.purchaseIndent.getAllCity.isLoading || nextProps.purchaseIndent.getAgentName.isLoading
      || nextProps.purchaseIndent.getItemDetails.isLoading || nextProps.purchaseIndent.getPoData.isLoading || nextProps.purchaseIndent.otb.isLoading
      || nextProps.purchaseIndent.selectVendor.isLoading || nextProps.purchaseIndent.selectOrderNumber.isLoading || nextProps.purchaseIndent.departmentSetBased.isLoading || nextProps.purchaseIndent.getPoItemCode.isLoading || nextProps.purchaseIndent.poItemBarcode.isLoading || nextProps.purchaseIndent.po_edit_data.isLoading || nextProps.purchaseIndent.get_draft_data.isLoading) {

      this.setState({
        loader: true

      })
    }

  }
  render() {
    console.log("success", this.state.success)
    const hash = window.location.hash;


    return (
      <div className="container-fluid pad-l50">
        {hash == "#/purchase/purchaseOrders" ? (

          <GenericPurchaseOrder ref="child" statusProp = {this.state.status} errorMessage={this.state.errorMessage} {...this.props} confirmModal={(e) => this.confirmModal(e)} onResetData={(e) => this.onResetData(e)} />
        ) : null}
        {hash == "#/purchase/purchaseOrder" ? (

          <PurchaseOrderForm ref="child" {...this.props} confirmModal={(e) => this.confirmModal(e)} onResetData={(e) => this.onResetData(e)} />
        ) : null}

        {this.state.loader ? <FilterLoader /> : null}
        {this.state.confirmModal ? <ConfirmModal {...this.props} {...this.state} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} pi={this.state.pi} rowsId={this.state.rowsId} rowDelete={this.state.rowDelete} deleteRows={(e) => this.deleteRows(e)} resetData={(e) => this.resetData(e)} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} moduleError={this.state.moduleError} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
        {/* {this.state.itemNotFound ?<ItemFound {...this.props} savePayload={this.state.savePayload}  piResource={this.state.piResource} closeItemNotFound={(e)=>this.closeItemNotFound(e)} itemNotFound ={this.state.itemNotFound}/>:null} */}

      </div>
    );
  }
}

export default PO;
