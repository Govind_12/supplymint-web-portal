import React from "react";
// import ConfigWithoutScheduler from "../replenishment/configWithoutScheduler";
import Footer from '../footer'
import errorIcon from "../../assets/error_icon.svg";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import { getDate } from '../../helper';
// import { getDefaultSettings } from "http2";
class ReplenishmentAutoConfig extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lastCreated: false,
            lastFreq: "",
            lastTime: "",
            lastDate: "",
            frequency: "",
            saveconfig: true,
            reschedule: false,
            frequencyerr: false,
            time: "",
            timeerr: false,
            lastRun: "0 Hours ago",
            date: "",
            dateerr: false,
            status: "No Job in progress",
            nextScheduled: "NA",
            progress: "",
            timeTaken: "00",
            monthserr: false,
            dayserr: false,
            months: [
                { id: 1, name: "JAN", value: "JAN", checked: false },
                { id: 2, name: "FEB", value: "FEB", checked: false },
                { id: 3, name: "MAR", value: "MAR", checked: false },
                { id: 4, name: "APR", value: "APR", checked: false },
                { id: 5, name: "MAY", value: "MAY", checked: false },
                { id: 6, name: "JUN", value: "JUN", checked: false },
                { id: 7, name: "JULY", value: "JUL", checked: false },
                { id: 8, name: "AUG", value: "AUG", checked: false },
                { id: 9, name: "SEP", value: "SEP", checked: false },
                { id: 10, name: "OCT", value: "OCT", checked: false },
                { id: 11, name: "NOV", value: "NOV", checked: false },
                { id: 12, name: "DEC", value: "DEC", checked: false },
            ],
            days: [
                { id: 1, name: "SUN", value: "SUN", checked: false },
                { id: 2, name: "MON", value: "MON", checked: false },
                { id: 3, name: "TUE", value: "TUE", checked: false },
                { id: 4, name: "WED", value: "WED", checked: false },
                { id: 5, name: "THU", value: "THU", checked: false },
                { id: 6, name: "FRI", value: "FRI", checked: false },
                { id: 7, name: "SAT", value: "SAT", checked: false }
            ],
            month: false,
            week: false,
            monthSelected: [],
            daysSelected: [],
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",
            percentage: 0,
            min: getDate()
        };
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    componentWillMount() {

        var mode = "";
      

        if (process.env.NODE_ENV == "develop") {
            mode = "-DEV-JOB"

        } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality" ) {
            mode = "-PROD-JOB"
        }
     
        // this.props.createJobRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        this.props.getJobByNameRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        this.props.nscheduleRequest(sessionStorage.getItem('partnerEnterpriseCode') + '-TRIGGER');
        this.props.jobRunDetailRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        // this.statusInterval = setInterval(() => {            
        //     this.props.createJobRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        // }, 8000)
       

        // this.props.getJobByNameRequest('SMGLUE-'+sessionStorage.getItem('partnerEnterpriseName').toUpperCase()+mode);
        // this.props.jobRunDetailRequest('SMGLUE-'+sessionStorage.getItem('partnerEnterpriseName').toUpperCase()+mode);

        // REFERENCE FOR set interval for api call
        // this.statusInterval = setInterval(() => {
        //     if (user.isAuthenticated != null) {
        //       this.getStatus();
        //     } else{
        //       clearInterval(this.statusInterval);
        //     }
        //   }, pollingInterval.fetchStatusInterval);            
    }
    componentWillUnmount() {
        clearInterval(this.statusInterval);
        clearInterval(this.statusIntervall);
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.replenishment.createTrigger.isSuccess && !nextProps.replenishment.createTrigger.isError && !nextProps.replenishment.createTrigger.isLoading) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.replenishment.createTrigger.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createTrigger.data.message,
                loader: false,
                success: true,
                frequency:[],
                date:"",
                time: "",
                monthSelected: [],
                daysSelected: []
            })
            this.props.createTriggerClear();
        } else if (nextProps.replenishment.createTrigger.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.createTrigger.message.error == undefined ? undefined : nextProps.replenishment.createTrigger.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.createTrigger.message.status,
                errorCode: nextProps.replenishment.createTrigger.message.error == undefined ? undefined : nextProps.replenishment.createTrigger.message.error.errorCode
            })
            this.props.createTriggerClear();
        } else if (nextProps.replenishment.createTrigger.isLoading) {
            this.setState({
                loader: true
            })
        }
        if (nextProps.replenishment.jobRunDetail.isSuccess) {
            if (nextProps.replenishment.jobRunDetail.data.resource != null) {
                this.setState({
                    percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                    timeTaken: nextProps.replenishment.jobRunDetail.data.resource.timeTaken,
                    nextScheduled: nextProps.replenishment.jobRunDetail.data.resource.nextSchedule == null ? "NA" : nextProps.replenishment.jobRunDetail.data.resource.nextSchedule,
                    status: nextProps.replenishment.jobRunDetail.data.resource.state,

                })
            }
            var mode = "";

            if (process.env.NODE_ENV == "develop") {
                mode = "-DEV-JOB"
    
            } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality" ) {
                mode = "-PROD-JOB"
            }
                   let ecode =sessionStorage.getItem('partnerEnterpriseCode')
            setTimeout(() => {
                this.props.jobRunDetailRequest('SMGLUE-' + ecode.toUpperCase() + mode);
            }, 5000)

        }else if(nextProps.replenishment.jobRunDetail.isError){
            var mode = "";

            if (process.env.NODE_ENV == "develop") {
                mode = "-DEV-JOB"
            } else if (process.env.NODE_ENV == "production"|| process.env.NODE_ENV == "quality" ) {
                mode = "-PROD-JOB"
            }
                   let ecode =sessionStorage.getItem('partnerEnterpriseCode')
            
           setTimeout(() => {
                this.props.jobRunDetailRequest('SMGLUE-' + ecode.toUpperCase() + mode);
            }, 5000)
        }
        if (nextProps.replenishment.nschedule.isSuccess) {
            if (nextProps.replenishment.nschedule.data.resource != null) {
                var time = nextProps.replenishment.nschedule.data.resource.schedule.split(" ")[3];
                var date = nextProps.replenishment.nschedule.data.resource.schedule.substr(0, 11);
                this.setState({
                    nextScheduled: nextProps.replenishment.nschedule.data.resource.nextSchedule == null ? "NA" : nextProps.replenishment.nschedule.data.resource.nextSchedule,
                    lastDate: date,
                    lastFreq: nextProps.replenishment.nschedule.data.resource.frequency,
                    lastTime: time,
                })
            }
        }
        if (nextProps.replenishment.jobByName.isSuccess) {
            if (nextProps.replenishment.jobByName.data.resource != null) {
                this.setState({
                    lastRun: nextProps.replenishment.jobByName.data.resource.lastEngineRun == null ? "NA" : nextProps.replenishment.jobByName.data.resource.lastEngineRun
                })
            }
        }
    }



    handleChange(e) {
        if (e.target.id == "frequency") {
            e.target.placeholder = e.target.value;
            if (e.target.value == "MONTHLY") {
                this.setState({
                    frequency: "MONTHLY",
                    week: false,
                    month: true
                },
                    () => {
                        this.frequency();
                    })
            } else if (e.target.value == "WEEKLY") {
                this.setState({
                    frequency: "WEEKLY",
                    week: true,
                    month: false
                },
                    () => {
                        this.frequency();
                    })
            } else {
                this.setState({
                    frequency: "DAILY",
                    month: false,
                    week: false
                },
                    () => {
                        this.frequency();
                    })
            }

        } else if (e.target.id == "time") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    time: e.target.value
                },
                () => {
                    this.time();
                }
            );
        } else if (e.target.id == "date") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    date: e.target.value
                },
                () => {
                    this.date();
                }
            );
        }
    }


    frequency() {
        if (
            this.state.frequency == ""
        ) {
            this.setState({
                frequencyerr: true
            });
        } else {
            this.setState({
                frequencyerr: false
            });
        }
    }
    time() {
        if (
            this.state.time == "") {
            this.setState({
                timeerr: true
            });
        } else {
            this.setState({
                timeerr: false
            });
        }
    }
    date() {
        if (
            this.state.date == "") {
            this.setState({
                dateerr: true
            });
        } else {
            this.setState({
                dateerr: false
            });
        }
    }

    onCheckClick(id) {
        for (let i = 0; i < this.state.months.length; i++) {
            if (this.state.months[i].id == id) {
                if (this.state.months[i].checked) {
                    let array = this.state.monthSelected;
                    for (let x = 0; x < array.length; x++) {
                      
                        if (array[x].id == id) {
                            array.splice(x, 1);
                           
                            this.setState({
                                monthSelected: array
                            },()=>{
                                this.monthlyError();
                            })
                            break;
                        }
                    }
                } else {
                    this.setState({
                        monthSelected: this.state.monthSelected.concat(this.state.months[i])
                    },()=>{
                        this.monthlyError();
                    })
                    break;
                }
            }
        }
        if (this.state.months[id - 1].checked) {
            this.state.months[id - 1].checked = false
        } else {
            this.state.months[id - 1].checked = true
        }
    }

    onMonthClick(id) {
        for (let i = 0; i < this.state.days.length; i++) {
            if (this.state.days[i].id == id) {
                if (this.state.days[i].checked) {
                    let array = this.state.daysSelected;
                    for (let x = 0; x < array.length; x++) {
                        if (array[x].id == id) {
                            array.splice(x, 1);
                            this.setState({
                                daysSelected: array
                            },()=>{
                                this.weeklyError()
                            })
                            break;
                        }
                    }
                } else {
                    this.setState({
                        daysSelected: this.state.daysSelected.concat(this.state.days[i])
                    },()=>{
                        this.weeklyError()
                    })
                    break;
                }
                break;
            }
        }
        if (this.state.days[id - 1].checked) {

            this.state.days[id - 1].checked = false
        } else {
            this.state.days[id - 1].checked = true
        }
    }

    contactSubmit(e) {
        e.preventDefault();
        var sortMonth = this.state.monthSelected.length > 0 ? this.state.monthSelected : [];
        var sortDay = this.state.daysSelected.length > 0 ? this.state.daysSelected : [];
        let abc = "";
        if (this.state.daysSelected.length > 0) {
            for (let i = 0; i < sortDay.length; i++) {
                let a = sortDay[i].value;
                if (sortDay.length > 1) {
                    abc = abc == "" ? "".concat(a) : abc.concat(',').concat(a);
                } else {
                    abc = a;
                }
            }
        }
        let xyz = "";
        if (this.state.monthSelected.length > 0) {
            for (let i = 0; i < sortMonth.length; i++) {
                let a = sortMonth[i].value;
                if (sortMonth.length > 1) {
                    xyz = xyz == "" ? "".concat(a) : xyz.concat(',').concat(a);
                } else {
                    xyz = a;
                }
            }
        }
        this.frequency();
        this.date();
        this.time();
      this.state.frequency=="MONTHLY"?  this.monthlyError():null
      this.state.frequency=="WEEKLY"?   this.weeklyError():null
        const t = this;

        setTimeout(function () {

            const { frequency, date, time, frequencyerr, dateerr, timeerr, monthserr, dayserr} = t.state;
            if (!frequencyerr && !dateerr && !timeerr && !monthserr && !dayserr) {
                t.setState({
                    loader: true,
                    alert: false,
                    success: false
                })
                var mode = "";

                if (process.env.NODE_ENV == "develop") {
                    mode = "-DEV-JOB"
                } else if (process.env.NODE_ENV == "production" ||process.env.NODE_ENV == "quality" ) {
                    mode = "-PROD-JOB"
                } 
                let triggerData = {

                    jobName: 'SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode,
                    timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                    schedule: date + " " + time,
                    frequency: frequency,
                    triggerName: sessionStorage.getItem('partnerEnterpriseCode') + '-TRIGGER',
                    type: 'SCHEDULED',
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseCode'),
                    enterpriseId: sessionStorage.getItem('partnerEnterpriseId'),
                    userName: sessionStorage.getItem('userName'),
                    dayMonth: xyz == "" ? "NA" : xyz,
                    dayWeek: abc == "" ? "NA" : abc,

                }

                t.props.createTriggerRequest(triggerData);
                //    onClear(e);
            }
        }, 100)
    }

    monthlyError() {
        var months = this.state.months
        for (var i = 0; i < months.length; i++) {
            if (months[i].checked == false) {
                this.setState({
                    monthserr: true
                })
            } else {
                this.setState({
                    monthserr: false
                })
                break;
            }
        }
    }
    weeklyError() {
        var days = this.state.days
        for (var i = 0; i < days.length; i++) {
            if (days[i].checked == false) {
                this.setState({
                    dayserr: true
                })
            } else {
                this.setState({
                    dayserr: false
                })
                break;
            }
        }
    
    }


    render() {
        const {frequency,frequencyerr,time,timeerr, date, dateerr, monthserr, dayserr} = this.state;
        return (
            <div>
                <div className="container-fluid">
                    <div className="container_div" id="">

                        <div className="container-fluid">
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                <div className="organisation_container height90vh autoConfigHeight">
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <ul className="list_style">
                                            <li>
                                                <label className="contribution_mart">
                                                    AUTO CONFIGURATION
                                            </label>
                                            </li>
                                            <li>
                                                <p className="master_para">Create your rule engine configuration</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-sx-12 pad-0">
                                        <form >
                                            <div className="col-md-7 col-sm-6 pad-0 m-top-30 height76 formDivReplenishment">
                                                {this.state.lastFreq == "" ? null : <div>
                                                    <p>Current Triggered Scheduled Details</p>
                                                    <ul className="list-inline replanishmentList">
                                                        <li>
                                                            <label>FREQUENCY :</label>
                                                            <span>{this.state.lastFreq}</span>
                                                        </li>
                                                        <li>
                                                            <label>TIME :</label>
                                                            <span>{this.state.lastTime}</span>
                                                        </li>
                                                        <li>
                                                            <label>DATE :</label>
                                                            <span>{this.state.lastDate}</span>
                                                        </li>

                                                    </ul>
                                                </div>}

                                                <div className="formDivReplenishment">
                                                    <p>Choose Below options to set scheduler Task</p>
                                                    <div className="dropdownDivReplenishment">
                                                        {/* <select>
                                                        <option>
                                                            Choose Frequency
                                                    </option>
                                                    </select> */}
                                                        {/* <div className="selectboxReplenishment"> */}
                                                        <div className="col-md-3 pad-0 selectInputDiv"><select className={frequencyerr ? "selectboxReplenishment errorBorder displayPointer" : "selectboxReplenishment displayPointer"} onChange={e => this.handleChange(e)} placeholder="Select Frequency" id="frequency" value={frequency}>
                                                            <option value="">Select Frequency</option>
                                                            {/* <option value="hourly">Hourly</option> */}
                                                            <option value="DAILY">Daily</option>
                                                            <option value="WEEKLY">Weekly</option>
                                                            <option value="MONTHLY">Monthly</option>
                                                        </select>
                                                            {/* {frequencyerr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                            {frequencyerr ? (
                                                                <span className="error">
                                                                    Select frequency
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                        {/* <label>Monthly</label>
                                                        <span>
                                                            ▾
                                                        </span>
                                                        <div className="dropdownReplenishmentConfi">
                                                            <ul className="list-inline m-top-10">
                                                                <li>
                                                                    <span>Hourly</span>
                                                                </li>
                                                                <li>
                                                                    <span>Daily</span>
                                                                </li>
                                                                <li>
                                                                    <span>Weekly</span>
                                                                </li>
                                                                <li>
                                                                    <span>Monthly</span>
                                                                </li>
                                                            </ul>
                                                        </div> */}
                                                        <div className="col-md-3 pad-0 selectInputDiv timeZoneLabel">
                                                            <input className={timeerr ? "errorBorder" : ""} type="time" id="time" placeholder="hh:mm" value={time} onChange={e => this.handleChange(e)} />
                                                            {/* <label>Enter Time In 24 Hour Format<br /><span>(HH:MM)</span></label> */}

                                                            {/* {timeerr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                            {timeerr ? (
                                                                <span className="error">
                                                                    Select time
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="col-md-3 pad-0 selectInputDiv">
                                                            <input className={dateerr ? "errorBorder" : ""} type="date" value={date} min={this.state.min} id="date" placeholder={this.state.date == "" ? "choose Date" : this.state.date} onChange={e => this.handleChange(e)} />
                                                            {/* {dateerr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                            {dateerr ? (
                                                                <span className="error">
                                                                    Select date
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                    </div>
                                                    {this.state.month ? <div className="selectedDropdownList">
                                                        <p>Choose Months of Year</p>
                                                        <div className="choosenLIst autoConfigCheck">
                                                            {this.state.months.map((data, key) => (<button type="button" key={key}>
                                                                <p className="para_purchase replenishmentCheck">
                                                                    <input type="text" className="purchaseCheck" onClick={() => this.onCheckClick(`${data.id}`)} value={data.value} type="checkbox" id={data.id} name={data.id} />
                                                                    <label htmlFor={data.id} className="purchaseLabel displayPointer"><span>{data.name}</span></label>
                                                                </p>
                                                            </button>))}
                                                            {monthserr ? (
                                                                <span className="error">
                                                                    Select atleast one
                                                            </span>
                                                            ) : null}
                                                        </div>
                                                    </div> : null}
                                                    {this.state.week ? <div className="selectedDropdownList ">
                                                        <p>Choose Days of Week</p>
                                                        <div className="choosenLIst autoConfigCheck">
                                                            {this.state.days.map((data, key) => (<button type="button" key={key}>
                                                                <p className="para_purchase replenishmentCheck">
                                                                    <input type="text" value={data.value} className="purchaseCheck" onClick={() => this.onMonthClick(`${data.id}`)} type="checkbox" id={data.id} name={data.id} />
                                                                    <label htmlFor={data.id} className="purchaseLabel displayPointer"><span>{data.name}</span></label>
                                                                </p>
                                                            </button>))}
                                                            {dayserr ? (
                                                                <span className="error">
                                                                    Select atleast one
                                                                </span>
                                                            ) : null}

                                                        </div>
                                                    </div> : null}

                                                    <div className="footerReplenishment">
                                                        {/* {this.state.saveconfig ? */}
                                                           {/* {this.state.frequency!=""?   */}
                                                            <button className="" type="button" onClick={(e) => this.contactSubmit(e)}>
                                                            SAVE CONFIGURATION
                                            </button>
                                            {/* :<button className="btnDisabled" type="button">SAVE CONFIGURATION</button>} */}
                                             {/* : null}
                                                        {this.state.reschedule ? <button className="" type="submit">
                                                            RE-SCHEDULE
                                            </button> : null} */}
                                                    </div>
                                                </div></div></form>

                                        <div className="col-md-5 col-sm-6 col-xs-12 pad-0">
                                            <div className="rightReplenishment autoConfigTimer">
                                                <div className="replenishmentAnimateInsideDiv"></div>
                                                <div className="replenishmentAnimateDiv">
                                                    <div className="topReplensh">
                                                        <div className="circle"></div>
                                                        <label className="jobInfo">
                                                            {this.state.status}
                                                        </label>
                                                    </div>
                                                    <div className="middleReplensh">
                                                        <div className="autoConfigTooTip">
                                                            <div className="clearfix">

                                                                <div className="c100 p50 big">
                                                                    <div className="slice">

                                                                        <div className={this.state.percentage < 51 ? `p${this.state.percentage} progress-circle` : `over50 progress-circle p${this.state.percentage}`}>
                                                                            <div className="left-half-clipper">
                                                                                <div className="first50-bar"></div>
                                                                                <div className="value-bar"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <svg xmlns="http://www.w3.org/2000/svg" width="290" height="210" viewBox="0 0 290 210">
                                                                <g fill="none" fillRule="evenodd" transform="translate(1)">

                                                                    <circle cx="44.5" cy="134.5" r="44.5" fill="#F2F2F2" fillRule="nonzero" stroke="#D5D5D5" />
                                                                    {/* <text className="autoConfigMid" fill="#4A4A4A" fontFamily="Montserrat-Medium, Montserrat" fontSize="25" fontWeight="400" letterSpacing=".185">
                                                                        <tspan className="autoComfigTime" x="30.5" y="144">{this.state.timeTaken}</tspan> <tspan x="32.287" y="160" fontSize="12">mins</tspan>
                                                                    </text> */}
                                                                </g>
                                                            </svg>
                                                        </div>
                                                        <div className="autoConfigMid">
                                                            <div className="autoComfigTime onHoverSvg">
                                                                <svg className="toolTipSvg" xmlns="http://www.w3.org/2000/svg" width="290" height="210" viewBox="0 0 290 210">
                                                                    <g fill="none" fillRule="evenodd" transform="translate(1)">

                                                                        <rect width="140" height="58" x="148" fill="#F2F2F2" fillRule="nonzero" rx="8" />
                                                                        <text fill="#4A4A4A" fontFamily="Montserrat-Regular, Montserrat" fontSize="13" letterSpacing=".231">
                                                                            <tspan x="165" y="26">Total Time </tspan> <tspan x="165" y="42.512">lapsed </tspan> <tspan x="213.026" y="42.512" fontFamily="Montserrat-Bold, Montserrat" fontWeight="bold">{this.state.timeTaken} mins</tspan>
                                                                        </text>
                                                                        <path stroke="#D5D5D5" strokeLinecap="square" d="M56.5 122L89 29.5M90 29.5h58" />
                                                                    </g>
                                                                </svg>
                                                                <h3>{this.state.timeTaken}</h3>
                                                                <p>mins</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="bottomReplesh">
                                                        <label className="updateEngin">
                                                            Last Engine Run :  <span>{this.state.lastRun}</span>
                                                        </label>
                                                        <label className="schduleTime">
                                                            <span>Next Trigger scheduled</span>
                                                        </label>
                                                        <span className="timeSchedule">
                                                            {this.state.nextScheduled}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>
        );

    }

}

export default ReplenishmentAutoConfig;
