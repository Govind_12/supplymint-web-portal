import React from 'react';
import Cron from 'custom-cron-generator';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import GenericDataModal from './genericDataModal';

class CreateNewConfiguration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            submit: false,
            //openSetting: '',
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',
            genericDataModalShowRow: {},
            genericDataModalShow: false,

            apiCallType: 'site',
            siteFilters: {},
            itemFilters: {},
            siteRefs: {},
            itemRefs: {},
            genericSearchData: {},
            siteSelectedItems: {},
            itemSelectedItems: {},
            frequency: "Hourly",
            //ABOVE ITEMS ADDED LATELY

            openJobOption: false,
            showPlanningAttribute: false,
            showFilter: false,
            cronExpression: "",

            jobNameOptionData: [],
            jobName: "Select Job",
            manageRuleId: "",
            planningAttribute: [],

            // locationFilter: "",
            // divisionFilter: "",
            // sectionFilter: "",
            // departmentFilter: "",
            // brandFilter: "",
            // mrpRangeFilter: "",
        }
    }

    componentDidMount(){
        this.props.getAllJobRequest('data');
        this.props.getSiteAndItemFilterConfigurationRequest(this.state.apiCallType);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.replenishment.getAllJob.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                jobNameOptionData: nextProps.replenishment.getAllJob.data.resource == null ? [] : nextProps.replenishment.getAllJob.data.resource
            });
        }

        if(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isSuccess && this.state.apiCallType == 'site') {
            this.setState({
                loading: false,
                success: false,
                error: false,
                siteFilters: JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter),
                apiCallType: 'item'
            }, () => this.props.getSiteAndItemFilterConfigurationRequest("item"));
            //this.props.getSiteAndItemFilterConfigurationRequest("item");
        }
        else if(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isSuccess && this.state.apiCallType == 'item') {
            this.setState({
                loading: false,
                success: false,
                error: false,
                itemFilters: JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter)
                //settingModal: true
            });
        }
        else if (nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.status,
                errorCode: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorMessage
            });
        }

        if(nextProps.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                genericSearchData: {...this.state.genericSearchData, data: nextProps.seasonPlanning.getArsGenericFilters.data},
                genericDataModalShow: true
            });
        }
        else if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage,
                //confirmDelete: false
            });
        }

        if (nextProps.replenishment.getAllJob.isLoading || nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isLoading ||nextProps.seasonPlanning.getArsGenericFilters.isLoading) {
            this.setState({
                loading: true
            });
        }
    }

    // static getDerivedStateFromProps(nextProps, preState){
    //     if (nextProps.replenishment.getAllJob.isSuccess) {
    //         return{
    //             jobNameOptionData: nextProps.replenishment.getAllJob.data.resource == null ? [] : nextProps.replenishment.getAllJob.data.resource,
    //         }
    //     }
    //     return null
    // }

    componentDidUpdate(){
        if (this.props.replenishment.getJobData.isSuccess && this.props.replenishment.getJobData.data.resource !== null) {
            this.setState({
                 planningAttribute: this.props.replenishment.getJobData.data.resource == null ? [] : this.props.replenishment.getJobData.data.resource.response !== null &&
                                    this.props.replenishment.getJobData.data.resource.response !== undefined ? this.props.replenishment.getJobData.data.resource.response : [],
            })
        }
        this.props.getJobDataClear()
    }

    openJobOption =(e)=> {
        this.setState({ openJobOption: !this.state.openJobOption });
    }

    selectJob =(e, id)=>{
       this.setState({ 
            jobName: e.target.outerText,
            openJobOption: false,
            showPlanningAttribute: false,
            showFilter: false,
            manageRuleId: id
        },()=>{
           if(id !== undefined)
             this.props.getJobDataRequest(id);
           else
              this.setState({
                  planningAttribute: [],
              })  
        })
    }

    showPlanningAttribute =(e)=> {
        this.setState({
            showPlanningAttribute: !this.state.showPlanningAttribute
        });
    }

    handlePlanningValue =(e, data)=>{
        let planningAttribute = [...this.state.planningAttribute];
        planningAttribute.map( val => val.id == data.id && (val.planningTextValue = e.target.value))
        this.setState({ planningAttribute})
    }

    showFilter =(e)=> {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    handleFilterValue =(e, type)=>{
        if(type == "location")
           this.setState({ locationFilter: e.target.value})
        else if(type == "division")
           this.setState({ divisionFilter: e.target.value})
        else if(type == "section")
           this.setState({ sectionFilter: e.target.value})
        else if(type == "department")
           this.setState({ departmentFilter: e.target.value})
        else if(type == "brand")
           this.setState({ brandFilter: e.target.value})
        else if(type == "mrp")
           this.setState({ mrpRangeFilter: e.target.value})
    }


    saveConfiguration =(e)=>{
        // let stack = [];
        // this.state.planningAttribute.map( data =>{
        //     let paylaod ={
        //             displayName: data.displayName,
        //             type: data.type,
        //             defaultValue: data.defaultValue,
        //             updatedValue: data.planningTextValue
        //         }
        //         stack.push(paylaod)
        // })
        // let payload = {
        //     jobName: this.state.jobName,
        //     cronExpression: this.state.cronExpression,
        //     planningAttribute: {...stack},
        //     locationFilter: this.state.locationFilter,
        //     divisionFilter: this.state.divisionFilter,
        //     sectionFilter: this.state.sectionFilter,
        //     departmentFilter: this.state.departmentFilter,
        //     brandFilter: this.state.brandFilter,
        //     mrpRangeFilter: this.state.mrpRangeFilter,
        // }
        let planningAttributeJson = {};
        this.state.planningAttribute.forEach((item) => {
            planningAttributeJson[item.code] = item.planningTextValue == undefined ? item.defaultValue : item.planningTextValue
        });

        this.props.createTriggerRequest({
            manageRuleId: this.state.manageRuleId,
            frequency: document.getElementsByClassName("cron_builder")[0] == undefined ? "" : document.getElementsByClassName("cron_builder")[0].getElementsByClassName("active")[0].innerText,
            cronExpression: this.state.cronExpression,
            planningAttributeJson: JSON.stringify(planningAttributeJson),
            locationFilterJson: Object.keys(this.state.siteSelectedItems).length == 0 ? "" : JSON.stringify(this.state.siteSelectedItems),
            itemFilterJson: Object.keys(this.state.itemSelectedItems).length == 0 ? "" : JSON.stringify(this.state.itemSelectedItems)
        });
    }

    //FUNCTIONS FOR GENERIC DATA MODAL
    siteData = (e, entity, key, search, pageNo) => {
        if (e.keyCode == 13) {
            this.openGenericDataModal(entity, key, search, pageNo);
        }
    }

    openGenericDataModal = (entity, key, search, pageNo) => {
        if (this.state.genericDataModalShowRow[entity] != key) this.closeGenericDataModal();
        this.props.getArsGenericFiltersRequest({
            entity: entity,
            key: key,
            search: search,
            pageNo: pageNo
        });
        let modal = {};
        modal[entity] = key;
        this.setState({genericSearchData: {payload: {entity: entity, key: key, search: search}}, genericDataModalShowRow: modal});
    }

    closeGenericDataModal = () => {
        let itemRefs = this.state.itemRefs;
        if (itemRefs[this.state.genericDataModalShowRow.item] !== undefined) {
            itemRefs[this.state.genericDataModalShowRow.item].current.value = "";
            this.setState({
                genericDataModalShowRow: '',
                genericDataModalShow: false,
                itemRefs
            });
        }
        let siteRefs = this.state.siteRefs;
        if (siteRefs[this.state.genericDataModalShowRow.site] !== undefined) {
            siteRefs[this.state.genericDataModalShowRow.site].current.value = "";
            this.setState({
                genericDataModalShowRow: '',
                genericDataModalShow: false,
                siteRefs
            });
        }
    }

    selectItems = (entity, key, items) => {
        if (entity == "item") {
            let updateItems = this.state.itemSelectedItems;
            if (items.length !== 0) {
                updateItems[key] = items;
            }
            else if (items.length === 0 && updateItems[key] !== undefined) {
                delete updateItems[key];
            }
            this.setState({
                itemSelectedItems: updateItems
            });
        }
        else if (entity == "site") {
            let updateItems = this.state.siteSelectedItems;
            if (items.length !== 0) {
                updateItems[key] = items;
            }
            else if (items.length === 0 && updateItems[key] !== undefined) {
                delete updateItems[key];
            }
            this.setState({
                siteSelectedItems: updateItems
            });
        }
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    render() {
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content create-new-configuration">
                    <div className="cnc-head">
                        <h3>Create New Trigger</h3>
                        <button type="button" onClick={this.props.closeCreateConfiguration}><img src={require('../../assets/clearSearch.svg')} /></button>
                    </div>
                    <div className="cnc-body">
                        <div className="cncb-menu">
                            {this.state.jobNameOptionData.length == 0 ? null : <button type="button" onClick={(e) => this.openJobOption(e)}>{this.state.jobName}</button>}
                            {this.state.openJobOption &&
                            <div className="cncbm-dropdown">
                                <ul>
                                    <li key="0000" onClick={this.selectJob}>Select Job</li>
                                    {this.state.jobNameOptionData.map((data,key) => 
                                      <li key={key} onClick={(e) =>this.selectJob(e, data.id)}>{data.jobName}</li>
                                    )}
                                </ul>
                            </div>}
                        </div>{
                            this.state.jobNameOptionData.length == 0 ?
                            <div className="summary-page-status m-top-30">
                                <div className="sps-error-msg">
                                    <span className="spsem-msg">
                                        <svg xmlns="http://www.w3.org/2000/svg" id="information" width="14.413" height="14.413" viewBox="0 0 15.413 15.413">
                                            <path fill="#ff8103" id="Path_1092" d="M7.706 0a7.706 7.706 0 1 0 7.706 7.706A7.715 7.715 0 0 0 7.706 0zm0 14.012a6.305 6.305 0 1 1 6.305-6.305 6.312 6.312 0 0 1-6.305 6.305z" class="cls-1"/>
                                            <path fill="#ff8103" id="Path_1093" d="M145.936 70a.934.934 0 1 0 .934.935.935.935 0 0 0-.934-.935z" class="cls-1" transform="translate(-138.23 -66.731)"/>
                                            <path fill="#ff8103" id="Path_1094" d="M150.7 140a.7.7 0 0 0-.7.7v4.2a.7.7 0 0 0 1.4 0v-4.2a.7.7 0 0 0-.7-.7z" class="cls-1" transform="translate(-142.994 -133.461)"/>
                                        </svg>
                                        No Jobs Found!
                                    </span>
                                    <p><p>Currently no jobs has been configured to your account. Please contact support at support@supplymint.com</p></p>
                                </div>
                            </div> :
                            this.state.jobName == "Select Job" ?
                            null :
                            <React.Fragment>
                                <div className="cncb-menu">
                                    <Cron
                                    onChange={(e)=> {this.setState({cronExpression: e})}}
                                    value={this.state.cronExpression}
                                    showResultText={true}
                                    showResultCron={true}
                                    />
                                </div>
                                <div className="cncb-planning">
                                    <div className="cncbp-head" onClick={(e) => {this.state.planningAttribute.length && this.showPlanningAttribute(e)}}>
                                        <h3>Planning Attributes</h3>
                                        <button className={this.state.showPlanningAttribute === false ? "" : "orfh-btn"}>
                                            <img src={require('../../assets/down-arrow-new.svg')} />
                                        </button>
                                    </div>
                                    {this.state.showPlanningAttribute && this.state.planningAttribute.length &&
                                    <div className="cncbp-table">
                                        <table className="table">
                                            <thead>
                                                <tr>
                                                    <th><label>Param Name</label></th>
                                                    <th><label>Default Value</label></th>
                                                    <th><label>Value</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.planningAttribute.length && this.state.planningAttribute.map( (data, key) => <tr key={key}>    
                                                    <td><label>{data.displayName}</label></td>
                                                    <td><label>{data.defaultValue}</label></td>
                                                    <td>
                                                        {data.type == "List" ? <select onChange={(e) => this.handlePlanningValue(e,data)}>
                                                            {data.lov.length && data.lov.split(",").map( (data, key) =>
                                                            <option key={key} value={data}>{data}</option>)}
                                                        </select> :
                                                        data.type == "Disabled" ?
                                                        <input type="text" onChange={(e) =>this.handlePlanningValue(e, data)} value={data.planningTextValue == undefined ? "" : data.planningTextValue} disabled /> :
                                                        <input type="text" onChange={(e) =>this.handlePlanningValue(e, data)} value={data.planningTextValue == undefined ? "" : data.planningTextValue}/>}
                                                    </td>
                                                </tr>)}
                                            </tbody>
                                        </table>
                                    </div>}
                                </div>
                                {this.state.planningAttribute.length == 0? <label className="m-top-5">Select Job to view Planning Attributes</label> : null}
                                <div className="cncb-planning m-top-30">
                                    <div className="cncbp-head" onClick={(e) => this.showFilter(e)}>
                                        <h3>Filter</h3>
                                        <button className={this.state.showFilter === false ? "" : "orfh-btn"} >
                                            <img src={require('../../assets/down-arrow-new.svg')} />
                                        </button>
                                    </div>
                                    {this.state.showFilter &&
                                    <div className="cncbp-filter m-top-10">
                                        <h3>Site Filter</h3>
                                        <div className="pi-new-layout">{
                                            Object.keys(this.state.siteFilters).length == 0 ?
                                            <label><i>No filters found!</i></label> :
                                            Object.keys(this.state.siteFilters).map((key) => {
                                                this.state.siteRefs[key] = React.createRef();
                                                return (
                                                    <div className="col-md-6 pad-lft-0 cncbpf-inner">
                                                        <label>{this.state.siteFilters[key]}</label>
                                                        <div className="inputTextKeyFucMain">
                                                            <input type="text" className="onFocus pnl-purchase-input"
                                                                id={key + "Focus"}
                                                                placeholder={this.state.siteSelectedItems[key] === undefined || this.state.siteSelectedItems[key].length === 0 ? "No " + this.state.siteFilters[key] + " selected" : this.state.siteSelectedItems[key].length === 1 ? this.state.siteSelectedItems[key][0] + " selected" : this.state.siteSelectedItems[key][0] + " + " + (this.state.siteSelectedItems[key].length - 1) + " selected"}
                                                                ref={this.state.siteRefs[key]}
                                                                onFocus={(e) => {if (this.state.genericDataModalShow.site === false || this.state.genericDataModalShowRow.site != key) this.openGenericDataModal("site", key, e.target.value, 1)}}
                                                                //onBlur={(e) => {if (this.state.genericDataModalShowRow != item.key) e.target.value=""}}
                                                                onKeyDown={(e) => this.siteData(e, "site", key, e.target.value, 1)}
                                                            />
                                                            <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("site", key, this.state.siteRefs[key].current.value, 1)} >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                </svg>
                                                            </span>
                                                            {this.state.genericDataModalShow && this.state.genericDataModalShowRow.site == key ? <GenericDataModal data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.siteSelectedItems[key]} /> : null}
                                                        </div>
                                                    </div>
                                                );
                                            })
                                        }</div>
                                        <div className="cncbpf-item">
                                            <h3>Item Filter</h3>
                                            <div className="pi-new-layout">{
                                                Object.keys(this.state.itemFilters).length == 0 ?
                                                <label><i>No filters found!</i></label> :
                                                Object.keys(this.state.itemFilters).map((key) => {
                                                    this.state.itemRefs[key] = React.createRef();
                                                    return (
                                                        <div className="col-md-6 pad-lft-0 cncbpf-inner">
                                                            <label>{this.state.itemFilters[key]}</label>
                                                            <div className="inputTextKeyFucMain">
                                                                <input type="text" className="onFocus pnl-purchase-input"
                                                                    id={key + "Focus"}
                                                                    placeholder={this.state.itemSelectedItems[key] === undefined || this.state.itemSelectedItems[key].length === 0 ? "No " + this.state.itemFilters[key] + " selected" : this.state.itemSelectedItems[key].length === 1 ? this.state.itemSelectedItems[key][0] + " selected" : this.state.itemSelectedItems[key][0] + " + " + (this.state.itemSelectedItems[key].length - 1) + " selected"}
                                                                    ref={this.state.itemRefs[key]}
                                                                    onFocus={(e) => {if (this.state.genericDataModalShow.item === false || this.state.genericDataModalShowRow.item != key) this.openGenericDataModal("item", key, e.target.value, 1)}}
                                                                    //onBlur={(e) => {if (this.state.genericDataModalShowRow != item.key) e.target.value=""}}
                                                                    onKeyDown={(e) => this.siteData(e, "item", key, e.target.value, 1)}
                                                                />
                                                                <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("item", key, this.state.itemRefs[key].current.value, 1)}> {/* PAGE NUMBER HAS TO BE MADE DYNAMIC */}
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </span>
                                                                {this.state.genericDataModalShow && this.state.genericDataModalShowRow.item == key ? <GenericDataModal data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.itemSelectedItems[key]} /> : null}
                                                            </div>
                                                        </div>
                                                    );
                                                })
                                            }</div>
                                        </div>
                                    </div>}
                                </div>
                            </React.Fragment>
                        }</div>
                    <div className="cnc-footer m-top-20">
                        <button type="button" onClick={(e) => { this.state.jobName !== "Select Job" && this.saveConfiguration(e)}}>Create New Trigger</button>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export default CreateNewConfiguration;