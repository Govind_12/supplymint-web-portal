import React from 'react';

export const ViewActionConfigModal = (props) => {

    function deleteConfigdata() {

        props.deleteConfig(props.configModaldata)
        props.viewConfigurationModal()

    }

    return (
        <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
            <div className=" display_block">
                <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain configViewModal customDateMain pad-0">
                    <div className="save-current-data posRelative">
                        <div className="col-md-12 m0 pad-0 displayInline">
                            <div className="modalTop alignMiddle">
                                <h2 className="fontBold">Configuration View Mode</h2>
                            </div>
                            <div className="modalOpertions">
                                <label>Configuration Name</label>
                                <h5>{props.configModaldata}</h5>
                            </div>
                            <div className="modalContent pad-tb-30 displayInline width100">
                                {props.keysConfigData.map((data, key) =>
                                    <div className="col-md-2 pad-0" key={key}><label className="displayBlock">{data.toUpperCase()} </label><span>{props.viewConfigurationdata[data].join(',')}</span></div>
                                )}
                            </div>
                            <div className="displayInline width100 pad-tb-30 m-top-50 alignMiddle">
                                <div className="col-md-6 textLeft pad-lft-0">
                                    <button type="button" className="deleteCongig" onClick={(e) => deleteConfigdata(e)}>Delete Configuration</button>
                                </div>
                                <div className="col-md-6 pad-right-0 textRight">
                                    <button className="clearBtn" type="button" onClick={(e) => props.viewConfigurationModal()}>Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}