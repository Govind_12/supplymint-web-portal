import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import runDemandCircle from "../../assets/rundemandcircle.svg";
import circleInside from "../../assets/circleInside.svg";
import lockIcon from "../../assets/lock.svg";
import SectionLoader from '../loaders/sectionLoader';
import Footer from '../footer'
import Succeeded from "./rodsuccess";
import calenderIcon from "../../assets/calender-icon.svg";
import calenderTime from "../../assets/calender-time.svg";
import calenderNew from "../../assets/calender(2).svg";
import FilterModal from "./filterModal";
import filter from '../../assets/filter-2.svg';
import ToastLoader from "../loaders/toastLoader";
class ReplenishmentRunOnDemandNew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nextScheduleObj:{},
            runState: "NA",
            run: true,
            lastDate: "NA",
            lastTime: "NA",
            openRightBar: false,
            rightbar: false,
            lastRun: "0 Hours ago",
            status: "Not Started",
            nextScheduled: "NA",
            timeTaken: "00",
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            stop: false,
            open: false,
            skecher: false,
            roleData: [],
            zone: "",
            zoneData: [],
            channel: "Retail",
            gradeData: [],
            partner: "",
            partnerData: [],
            grade: "",
            selected: [],
            search: "",
            selectederr: "",
            filter: true,
            remove: false,
            jobRunState: "NA",
            errorCode: "",
            succeeded: false,
            code: "",
            modalState: "",
            percentage: 0,
            lastRunTime: "NA",
            // smallLoader1: true,
            // smallLoader2: true,
            // smallLoader3: true,
            // smallLoader4: true,
            triggerName: "",
            filterStores: "",
            applyCityKart: false,
            appliedStockPoint: sessionStorage.getItem("stockPoint") != undefined ? sessionStorage.getItem("stockPoint") : "ALL",
            stockPoint: sessionStorage.getItem("stockPoint") != undefined ? sessionStorage.getItem("stockPoint") : "ALL",
       
            // _______________________________NEW REPLENISHMENT CODE___________________________________
            getJobs:[],
            selectedData:"",
            nextSchedule:"",
            isEnabled:"false",
            labelConfigFilter:[],
            configurationFilter:{},
            openFilter:false,
            storeCodeList: [],
          
            storeSearch: "",
            apply:false,
            toastLoader:false,
            toastMsg:"",
            checkedValue:[],
            
            getConfigStore:"",
            filterValue: {},
            saveCol:[],
            configParam:"",
          
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
    }
    componentWillMount() {
        this.props.getAllJobRequest('data')
        // var mode = "";
        // if (process.env.NODE_ENV == "develop") {
        //     mode = "-DEV-JOB"
        // } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality") {
        //     mode = "-PROD-JOB"
        // }
        // else {
        //     mode = "-DEV-JOB"
        // }
        // let data = {
        //     channel: this.state.channel,
        // }
        // { sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.props.getPartnerRequest(data) : null }
        // this.props.jobRunDetailRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        // this.props.lastJobRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        // this.props.nscheduleRequest(sessionStorage.getItem('partnerEnterpriseCode') + '-TRIGGER');
        // this.props.getJobByNameRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
    }
    componentWillUnmount() {
        clearInterval(this.statusInterval);
        clearInterval(this.statusIntervall);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.lastJob.isSuccess) {
            if (nextProps.replenishment.lastJob.data.resource != null) {
                this.setState({
                    lastRunTime: nextProps.replenishment.lastJob.data.resource.duration == null ? "NA" : nextProps.replenishment.lastJob.data.resource.duration + " mins ago"
                })
            }else{
                this.setState({
                    lastRunTime: "NA"
                })
            }
        }
        if (!nextProps.replenishment.stopOnDemand.isSuccess && !nextProps.replenishment.stopOnDemand.isError && !nextProps.replenishment.runOnDemand.isSuccess && !nextProps.replenishment.runOnDemand.isError) {
            this.setState({
                loader: false
            })
        }
        if (nextProps.replenishment.jobRunDetail.isSuccess) {
            if (nextProps.replenishment.jobRunDetail.data.resource != null) {
                this.setState({
                    timeTaken: nextProps.replenishment.jobRunDetail.data.resource.timeTaken,
                    status: nextProps.replenishment.jobRunDetail.data.resource.state,
                    lastDate: nextProps.replenishment.jobRunDetail.data.resource.lastEngineRun == null ? "NA" : nextProps.replenishment.jobRunDetail.data.resource.lastEngineRun,
                    runState: nextProps.replenishment.jobRunDetail.data.resource.state,
                })
            }
            sessionStorage.setItem('jobId',nextProps.replenishment.jobRunDetail.data.resource.jobId)
            setTimeout(() => {
                this.props.jobRunDetailRequest(this.state.selectedData == "" ? this.state.getJobs[0] : this.state.selectedData);
            }, 6000)
        } else if (nextProps.replenishment.jobRunDetail.isError) {
            setTimeout(() => {
                this.props.jobRunDetailRequest(this.state.selectedData == "" ? this.state.getJobs[0] : this.state.selectedData);
            }, 6000)
        }
        if (nextProps.replenishment.jobByName.isSuccess) {
            this.setState({
                lastRun: nextProps.replenishment.jobByName.data.resource == null ? "NA" : nextProps.replenishment.jobByName.data.resource.lastEngineRun,
                jobRunState: nextProps.replenishment.jobByName.data.resource == null ? "NA" : nextProps.replenishment.jobByName.data.resource.jobRunState,
            })
            this.props.getjobByNameClear()
        } 
        
        // if (nextProps.replenishment.nschedule.isSuccess) {
        //     this.setState({
        //         nextScheduled: nextProps.replenishment.nschedule.data.resource == null ? "NA" : nextProps.replenishment.nschedule.data.resource.nextSchedule,
        //         smallLoader2: false
        //     })
        // } else if (nextProps.replenishment.nschedule.isLoading) {
        //     this.setState({
        //         smallLoader2: true
        //     })
        // }
        
        if (nextProps.replenishment.runOnDemand.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.replenishment.runOnDemand.isSuccess) {
            if (nextProps.replenishment.runOnDemand.data.resource != null) {
                this.setState({
                    loader: true
                })
                if (nextProps.replenishment.jobRunDetail.data.resource.state == "RUNNING") {
                    this.setState({
                        loader: false
                    })
                    this.props.runOnDemandClear();
                }
            }
        } else if (nextProps.replenishment.runOnDemand.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.runOnDemand.message.status,
                errorMessage: nextProps.replenishment.runOnDemand.message.error == undefined ? undefined : nextProps.replenishment.runOnDemand.message.error.errorMessage,
                errorCode: nextProps.replenishment.runOnDemand.message.error == undefined ? undefined : nextProps.replenishment.runOnDemand.message.error.errorCode
            })
            this.props.runOnDemandClear();
        }
        if (nextProps.replenishment.stopOnDemand.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.replenishment.stopOnDemand.isSuccess) {
            if (nextProps.replenishment.stopOnDemand.data.resource != null) {
                this.setState({
                    loader: true
                })
                if (nextProps.replenishment.jobRunDetail.data.resource.state == "STOPPING") {
                    this.setState({
                        loader: false
                    })
                }
            }
            this.clearFilter()
                    this.props.stopOnDemandClear();
        } else if (nextProps.replenishment.stopOnDemand.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.stopOnDemand.message.status,
                errorMessage: nextProps.replenishment.stopOnDemand.message.error == undefined ? undefined : nextProps.replenishment.stopOnDemand.message.error.errorMessage,
                errorCode: nextProps.replenishment.stopOnDemand.message.error == undefined ? undefined : nextProps.replenishment.stopOnDemand.message.error.errorCode
            })
            this.props.stopOnDemandRequest();
        }
        // // this.setState({
        // //     gradeData: nextProps.replenishment.getGrade.data.resource == undefined ? [] : nextProps.replenishment.getGrade.data.resource.grade,
        // //     zoneData:  nextProps.replenishment.getZone.data.resource == undefined ? [] : nextProps.replenishment.getZone.data.resource.zone,
        // // })
        if (nextProps.replenishment.runOnDemand.isLoading || nextProps.replenishment.stopOnDemand.isLoading) {
            this.setState({
                loader: true
            })
        }
        // // if (nextProps.replenishment.getZone.isSuccess && !nextProps.replenishment.getGrade.isSuccess && !nextProps.replenishment.getStoreCode.isSuccess) {
        // //     if(nextProps.replenishment.getZone.data.resource.zone.length != this.state.zoneData.length || this.state.roleData.concat(this.state.selected).length != nextProps.replenishment.getZone.data.resource.store.length){
        // //        this.setState({
        // //             roleData: nextProps.replenishment.getZone.data.resource.store
        // //         })
        // //     }
        // // }
        // // if (nextProps.replenishment.getGrade.isSuccess && !nextProps.replenishment.getStoreCode.isSuccess) {
        // //     if(this.state.roleData.concat(this.state.selected).length != nextProps.replenishment.getGrade.data.resource.store.length){
        // //         this.setState({
        // //             roleData: nextProps.replenishment.getGrade.data.resource.store
        // //         })
        // //     }
        // // }
        // // if (nextProps.replenishment.getStoreCode.isSuccess) {
        // //     // if(this.state.roleData.concat(this.state.selected).length != nextProps.replenishment.getStoreCode.data.resource.store.length){
        // //     this.setState({
        // //         loader: false,
        // //         zoneData: nextProps.replenishment.getStoreCode.data.resource.zone == null ? [] : nextProps.replenishment.getStoreCode.data.resource.zone,
        // //         gradeData: nextProps.replenishment.getStoreCode.data.resource.grade == null ? [] : nextProps.replenishment.getStoreCode.data.resource.grade,
        // //         roleData: nextProps.replenishment.getStoreCode.data.resource.store == null ? [] : nextProps.replenishment.getStoreCode.data.resource.store
        // //     })
        // //     // }
        // // } else if (nextProps.replenishment.getStoreCode.isLoading) {
        // //     this.setState({
        // //         loader: true
        // //     })
        // // }
        // //partner
        // if (nextProps.replenishment.getPartner.isSuccess) {
        //     if (nextProps.replenishment.getPartner.data.resource != null) {
        //         this.setState({
        //             loader: false,
        //             partnerData: nextProps.replenishment.getPartner.data.resource.partner,
        //             roleData: nextProps.replenishment.getPartner.data.resource.store
        //         })
        //     }
        //     this.props.getPartnerRequest();
        // } else if (nextProps.replenishment.getPartner.isError) {
        //     this.setState({
        //         loader: false,
        //         alert: true,
        //         code: nextProps.replenishment.getPartner.message.status,
        //         errorMessage: nextProps.replenishment.getPartner.message.error == undefined ? undefined : nextProps.replenishment.getPartner.message.error.errorMessage,
        //         errorCode: nextProps.replenishment.getPartner.message.error == undefined ? undefined : nextProps.replenishment.getPartner.message.error.errorCode
        //     })
        //     this.props.getPartnerRequest();
        // }
        // //zone
        // if (nextProps.replenishment.getZone.isSuccess) {
        //     if (nextProps.replenishment.getZone.data.resource != null) {
        //         this.setState({
        //             loader: false,
        //             zoneData: nextProps.replenishment.getZone.data.resource.zone,
        //             roleData: nextProps.replenishment.getZone.data.resource.store
        //         })
        //     }
        //     this.props.getZoneRequest();
        // } else if (nextProps.replenishment.getZone.isError) {
        //     this.setState({
        //         loader: false,
        //         alert: true,
        //         code: nextProps.replenishment.getZone.message.status,
        //         errorMessage: nextProps.replenishment.getZone.message.error == undefined ? undefined : nextProps.replenishment.getZone.message.error.errorMessage,
        //         errorCode: nextProps.replenishment.getZone.message.error == undefined ? undefined : nextProps.replenishment.getZone.message.error.errorCode
        //     })
        //     this.props.getZoneRequest();
        // }
        // //grade
        // if (nextProps.replenishment.getGrade.isSuccess) {
        //     if (nextProps.replenishment.getGrade.data.resource != null) {
        //         this.setState({
        //             loader: false,
        //             gradeData: nextProps.replenishment.getGrade.data.resource.grade,
        //             roleData: nextProps.replenishment.getGrade.data.resource.store
        //         })
        //     }
        //     this.props.getGradeRequest();
        // } else if (nextProps.replenishment.getGrade.isError) {
        //     this.setState({
        //         loader: false,
        //         alert: true,
        //         code: nextProps.replenishment.getGrade.message.status,
        //         errorMessage: nextProps.replenishment.getGrade.message.error == undefined ? undefined : nextProps.replenishment.getGrade.message.error.errorMessage,
        //         errorCode: nextProps.replenishment.getGrade.message.error == undefined ? undefined : nextProps.replenishment.getGrade.message.error.errorCode
        //     })
        //     this.props.getGradeRequest();
        // }
        // //store
        // if (nextProps.replenishment.getStoreCode.isSuccess) {
        //     if (nextProps.replenishment.getStoreCode.data.resource != null) {
        //         this.setState({
        //             loader: false,
        //             roleData: nextProps.replenishment.getStoreCode.data.resource.store
        //         })
        //     }
        //     this.props.getStoreCodeRequest();
        // } else if (nextProps.replenishment.getStoreCode.isError) {
        //     this.setState({
        //         loader: false,
        //         alert: true,
        //         code: nextProps.replenishment.getStoreCode.message.status,
        //         errorMessage: nextProps.replenishment.getStoreCode.message.error == undefined ? undefined : nextProps.replenishment.getStoreCode.message.error.errorMessage,
        //         errorCode: nextProps.replenishment.getStoreCode.message.error == undefined ? undefined : nextProps.replenishment.getStoreCode.message.error.errorCode
        //     })
        //     this.props.getStoreCodeRequest();
        // }
        // if (nextProps.replenishment.applyFilter.isSuccess) {
        //     this.setState({
        //         loader: false,
        //         filter: false,
        //         remove: true,
        //         filterStores: nextProps.replenishment.applyFilter.data.resource == null ? "" : nextProps.replenishment.applyFilter.data.resource.store
        //     })
        //     this.props.applyFilterRequest();
        // } else if (nextProps.replenishment.applyFilter.isError) {
        //     this.setState({
        //         loader: false,
        //         alert: true,
        //         // code: nextProps.replenishment.applyFilter.message.status,
        //         errorMessage: nextProps.replenishment.applyFilter.message.error == undefined ? undefined : nextProps.replenishment.applyFilter.message.error.errorMessage,
        //         // errorCode: nextProps.replenishment.applyFilter.message.error == undefined ? undefined : nextProps.replenishment.applyFilter.message.error.errorCode
        //     })
        //     this.props.applyFilterRequest();
        // }
// _______________________________________GET ALL JOB RECEIVEPROPS__________________________________________
        if (nextProps.replenishment.jobRunDetail.isSuccess) {
            if (nextProps.replenishment.jobRunDetail.data.resource != null) {
                if (nextProps.replenishment.jobRunDetail.data.resource.triggerName == "RUN ON DEMAND") {
                    if (nextProps.replenishment.jobRunDetail.data.resource.state == "NOT STARTED" || nextProps.replenishment.jobRunDetail.data.resource.state == "STOPPED") {
                        this.setState({
                            run: true,
                            stop: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,
                            appliedStockPoint: "ALL",
                            stockPoint: "ALL"
                        })
                    
                        sessionStorage.removeItem("stockPoint")
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "RUNNING") {
                        this.setState({
                            stop: true,
                            run: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,
                        })
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "SUCCEEDED") {
                        this.setState({
                            stop: false,
                            run: true,
                            succeeded: true,
                            modalState: nextProps.replenishment.jobRunDetail.data.resource.state,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,
                            appliedStockPoint: "ALL",
                            stockPoint: "ALL"
                        })
                         this.clearFilter()
                        sessionStorage.removeItem("stockPoint")
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "FAILED") {
                        this.setState({
                            run: true,
                            stop: false,
                            succeeded: true,
                            modalState: nextProps.replenishment.jobRunDetail.data.resource.state,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,
                            appliedStockPoint: "ALL",
                            stockPoint: "ALL"
                        })
                          this.clearFilter()
                        sessionStorage.removeItem("stockPoint")
                    } else {
                        this.setState({
                            run: false,
                            stop: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,
                            appliedStockPoint: "ALL",
                            stockPoint: "ALL"
                        })
                    
                        sessionStorage.removeItem("stockPoint")
                    }
                } else {
                    if (nextProps.replenishment.jobRunDetail.data.resource.state == "NOT STARTED" || nextProps.replenishment.jobRunDetail.data.resource.state == "STOPPED" || nextProps.replenishment.jobRunDetail.data.resource.state == "FAILED") {
                        this.setState({
                            run: true,
                            stop: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                        })
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "RUNNING") {
                        this.setState({
                            stop: false,
                            run: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                        })
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "SUCCEEDED") {
                        this.setState({
                            stop: false,
                            run: true,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                        })
               
                    } else {
                        this.setState({
                            run: false,
                            stop: false
                        })
                    }
                }
            }
        }
        if (nextProps.replenishment.getAllJob.isSuccess) {
            let jobName = [];
            let configName =[];
            Object.keys(nextProps.replenishment.getAllJob.data.resource).map( key=>{
                jobName.push(key);
                configName.push(nextProps.replenishment.getAllJob.data.resource[key]);
            })
            this.setState({
                loader:false,
                getJobs: jobName == null ? [] : jobName,
                selectedData:jobName[0],
                configParam: nextProps.replenishment.getAllJob.data.resource
            })
          
            this.props.jobRunDetailRequest(jobName[0])
            this.props.getJobByNameRequest(jobName[0]);
            this.props.lastJobRequest(jobName[0])
            // this.props.getFiveTriggersRequest()
            this.props.getConfigFilterRequest(configName[0]);
            this.props.getAllJobClear()
        }else if(nextProps.replenishment.getAllJob.isError){
            this.setState({
                loader:false
            })
        }
      
        if (nextProps.replenishment.getFiveTriggers.isSuccess) {
            this.setState({
                loader:false,
                nextScheduleObj:nextProps.replenishment.getFiveTriggers.data.resource == null ? [] :nextProps.replenishment.getFiveTriggers.data.resource.nextSchedule,
                nextSchedule: nextProps.replenishment.getFiveTriggers.data.resource == null ? [] :nextProps.replenishment.getFiveTriggers.data.resource.nextSchedule[this.state.selectedData], 
            })        
        } else if (nextProps.replenishment.getFiveTriggers.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getFiveTriggers.message.error == undefined ? undefined : nextProps.replenishment.getFiveTriggers.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getFiveTriggers.message.status,
                errorCode: nextProps.replenishment.getFiveTriggers.message.error == undefined ? undefined : nextProps.replenishment.getFiveTriggers.message.error.errorCode
            })
            this.props.getFiveTriggersClear();
        } 
        if(nextProps.replenishment.getConfigFilter.isSuccess){
            this.setState({
                loader:false,
                configurationFilter: nextProps.replenishment.getConfigFilter.data.resource,
           })
        //     this.setState({
        //         loader:false,
        //         // getConfigStore: nextProps.replenishment.getConfigFilter.data.resource.storeCode == null ? "" :  nextProps.replenishment.getConfigFilter.data.resource.storeCode ,
        //         // isEnabled: nextProps.replenishment.getConfigFilter.data.resource.isEnabled,
        //         labelConfigFilter:Object.keys(nextProps.replenishment.getConfigFilter.data.resource.label),
        //         configurationFilter: nextProps.replenishment.getConfigFilter.data.resource.label,
        //    })
        //    let configurationFilter = nextProps.replenishment.getConfigFilter.data.resource.label
        //     let filterName = Object.keys(configurationFilter)
        //    let filter = {}
        //    for (let i = 0; i < filterName.length; i++) {
        //        if (configurationFilter[filterName[i]].type == "T") {
        //            if (configurationFilter[filterName[i]].changeValue != "") {
        //                filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
        //            }
        //        } else if (configurationFilter[filterName[i]].type == "L") {
        //            if (configurationFilter[filterName[i]].selectedValue.length != 0) {
        //                filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
        //            }
        //        }
        //    }
        //    let payload = {
        //        filter: filter,
        //        storeCode:nextProps.replenishment.getConfigFilter.data.resource.storeCode == null ? "NA" :  nextProps.replenishment.getConfigFilter.data.resource.storeCode 
        //    }
        //    this.props.getStoreCodeRequest(payload)
              this.props.getConfigFilterClear()

        }else if(nextProps.replenishment.getConfigFilter.isLoading){
            this.setState({
                loader:true
            })
        }else if (nextProps.replenishment.getConfigFilter.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getConfigFilter.message.error == undefined ? undefined : nextProps.replenishment.getConfigFilter.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getConfigFilter.message.status,
                errorCode: nextProps.replenishment.getConfigFilter.message.error == undefined ? undefined : nextProps.replenishment.getConfigFilter.message.error.errorCode
            })
            this.props.getConfigFilterClear();
        } 

        if (nextProps.replenishment.getStoreCode.isSuccess) {
                 let configurationFilter = this.state.configurationFilter
                configurationFilter.storeCode.selectedValue=[]
                configurationFilter.storeCode.isChecked= false
            if (nextProps.replenishment.getStoreCode.data.resource != null) {
                this.setState({
                    filterStores: nextProps.replenishment.getStoreCode.data.resource.storeCode,
                    storeCodeList: nextProps.replenishment.getStoreCode.data.resource.store,
                    selected: [],
                    configurationFilter:configurationFilter

                })
            } else {
                this.setState({
                    storeCodeList: [],
                    selected: [],
                    configurationFilter:configurationFilter

                })
            }
            this.props.getStoreCodeClear()
        }
        
        if (nextProps.replenishment.getConfigFilter.isLoading || nextProps.replenishment.getFiveTriggers.isLoading || nextProps.replenishment.applyFilter.isLoading || nextProps.replenishment.getZone.isLoading || nextProps.replenishment.getPartner.isLoading || nextProps.replenishment.getGrade.isLoading || nextProps.replenishment.getStoreCode.isLoading || nextProps.replenishment.stopOnDemand.isLoading || nextProps.replenishment.runOnDemand.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    // handleChange(e) {
    //     if (e.target.id == "channel") {
    //         this.setState({
    //             channel: e.target.value,
    //             partner: "",
    //             partnerData: [],
    //             zone: "",
    //             grade: "",
    //             zoneData: [],
    //             gradeData: [],
    //             selected: [],
    //             filter: true,
    //             remove: false,
    //         })
    //         let data = {
    //             channel: e.target.value
    //         }
    //         this.props.getPartnerRequest(data);
    //         this.props.getZoneRequest();
    //         this.props.getGradeRequest();
    //         this.props.getStoreCodeRequest();
    //     } else if (e.target.id == "partner") {
    //         this.setState({
    //             partner: e.target.value,
    //             zone: "",
    //             grade: "",
    //             zoneData: [],
    //             gradeData: [],
    //             selected: [],
    //             filter: true,
    //             remove: false,
    //         })
    //         let data = {
    //             channel: this.state.channel,
    //             partner: e.target.value
    //         }
    //         this.props.getZoneRequest(data);
    //         this.props.getGradeRequest();
    //         this.props.getStoreCodeRequest();
    //     } else if (e.target.id == "zone") {
    //         this.setState({
    //             zone: e.target.value,
    //             grade: "",
    //             gradeData: [],
    //             selected: [],
    //             filter: true,
    //             remove: false,
    //         })
    //         let data = {
    //             channel: this.state.channel,
    //             partner: this.state.partner,
    //             zone: e.target.value
    //         }
    //         this.props.getGradeRequest(data);
    //         this.props.getStoreCodeRequest();
    //     } else if (e.target.id == "grade") {
    //         this.setState({
    //             grade: e.target.value,
    //             selected: [],
    //             filter: true,
    //             remove: false,
    //         })
    //         let data = {
    //             channel: this.state.channel,
    //             partner: this.state.partner,
    //             zone: this.state.zone,
    //             grade: e.target.value
    //         }
    //         this.props.getStoreCodeRequest(data);
    //     } else if (e.target.id == "stockPoint") {
    //         this.setState({
    //             applyCityKart: false,
    //             stockPoint: e.target.value
    //         }
    //         )
    //     }
        // if (e.target.id == "channel") {
        //     this.setState({
        //         channel: e.target.value,
        //         zone: "",
        //         grade: "",
        //     })
        //     let t = this;
        //     let f = e.target;
        //     setTimeout(function () {
        //         let data = {
        //             channel: f.value,
        //             zone: "",
        //             grade: ""
        //         }
        //         t.props.getStoreCodeRequest(data);
        //     }, 100)
        // } else if (e.target.id == "grade") {
        //     this.setState({
        //         grade: e.target.value,
        //     })
        //     const t = this;
        //     let f = e.target;
        //     setTimeout(function () {
        //         let data = {
        //             channel: t.state.channel,
        //             zone: t.state.zone,
        //             grade: f.value
        //         }
        //         t.props.getStoreCodeRequest(data);
        //     }, 100)
        // } else if (e.target.id == "zone") {
        //     this.setState({
        //         zone: e.target.value,
        //         grade: "",
        //     })
        //     let t = this;
        //     let f = e.target;
        //     setTimeout(function () {
        //         let data = {
        //             channel: t.state.channel,
        //             grade: "",
        //             zone: f.value
        //         }
        //         t.props.getStoreCodeRequest(data);
        //     }, 100)
        // }
    // }
    updateConfigFilter(configFil){
        this.setState(
            {
                configurationFilter:configFil
            }
        )

    }
    updateSelected(data){
        this.setState({
            selected:data
        })
    }
    updateStoreCodeList(dataList){
        this.setState({
            storeCodeList:dataList
        })

    }
    updateCheckedValue(data){
        this.setState({
            checkedValue:data

        })
    }
    updateStoreSearch(value){
        this.setState({
            storeSearch:value
        })
    }
    onFilterApply(e) {
        let configurationFilter = this.state.configurationFilter
        let filterName = Object.keys(configurationFilter)
        let filterValue=this.state.filterValue
        for (let i = 0; i < filterName.length; i++) {
            if (configurationFilter[filterName[i]].isChecked == "True") {
                if (configurationFilter[filterName[i]].type == "T") {
                    if (configurationFilter[filterName[i]].changeValue != "") {
                        filterValue[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
                    }
                }
                if (configurationFilter[filterName[i]].type == "L") {
                    if (configurationFilter[filterName[i]].selectedValue.length != 0 && configurationFilter[filterName[i]].displayName == "Store" && configurationFilter[filterName[i]].value != "ALL-STORES") {
                        let selectedValue = configurationFilter[filterName[i]].selectedValue
                        let selectedValues = selectedValue.map((item) => {
                            return item.split('-')[0]
                        })
                        filterValue[configurationFilter[filterName[i]].columnName] = selectedValues.length > 1 ? selectedValues.join('|') : selectedValues.join(',')
                    } else if (configurationFilter[filterName[i]].selectedValue.length != 0 && configurationFilter[filterName[i]].displayName == "Store" && configurationFilter[filterName[i]].value == "ALL-STORES") {
                        filterValue[configurationFilter[filterName[i]].columnName] = this.state.filterStores
                    }
                    if (configurationFilter[filterName[i]].selectedValue.length != 0 && configurationFilter[filterName[i]].displayName != "Store") {
                      filterValue[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join(',')
                    }
                }
                this.state.saveCol.push(configurationFilter[filterName[i]].displayName)
            } else {
                if (configurationFilter[filterName[i]].type == "L") {
                    if (configurationFilter[filterName[i]].selectedValue.length == 0 && configurationFilter[filterName[i]].displayName == "Store" && configurationFilter[filterName[i]].value != "ALL-STORES") {
                        filterValue[configurationFilter[filterName[i]].columnName] = this.state.getConfigStore
                    } else if (configurationFilter[filterName[i]].selectedValue.length == 0 && configurationFilter[filterName[i]].displayName == "Store" && configurationFilter[filterName[i]].value == "ALL-STORES") {
                        filterValue[configurationFilter[filterName[i]].columnName] = "NA"
                    }
                }
            }
            if(configurationFilter[filterName[i]].isChecked == "False"){
                for (let j = 0; j < this.state.saveCol.length; j++) {
                    if(this.state.saveCol[j] == configurationFilter[filterName[i]].displayName){
                        this.state.saveCol.splice(j,1)
                    }
                }
            }
        }
     
        this.setState({
            filterValue:filterValue,
            saveCol:this.state.saveCol
        })
        this.functionSave();
    }
    functionSave(){
        if (this.state.saveCol == "") {
            this.setState({
                toastMsg: "Select atleast one" ,
                toastLoader: true,
                apply:false
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        }else{
            this.setState({
                apply:true
            })
            this.closeFilter()
        }
    }
    onStop(e) {
        this.setState({
            loader: true
        })
        e.preventDefault();
        this.props.stopOnDemandRequest(this.state.selectedData == "" ? this.state.getJobs[0] : this.state.selectedData);
    }
    onRun(e) {
        if(!this.state.apply){
            this.setState({
                toastMsg: "Apply Filter",
                toastLoader: true,
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        }else{
            this.setState({
                loader: true
            })
            e.preventDefault();
            setTimeout(() => {
                if (sessionStorage.getItem('partnerEnterpriseName') == "CITYKART") {
                    let data = {
                        jobName: this.state.selectedData == "" ? this.state.getJobs[0] : this.state.selectedData,
                        store: "NA",
                        // selected: this.state.filterStores,
                        filter: this.state.filterValue,
                        runType: "filter",
                        path: "NA",
                        stockPoint: this.state.appliedStockPoint,
                    }
                    sessionStorage.setItem("stockPoint", this.state.appliedStockPoint)
                    this.props.runOnDemandRequest(data);
                }
                else {
                    let data = {
                        jobName: this.state.selectedData == "" ? this.state.getJobs[0] : this.state.selectedData,
                        store: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.state.channel : "NA",
                        // selected: this.state.filterStores,
                        filter:this.state.filterValue,
                        runType: "filter",
                        path: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? "s3://com-supplymint-glue/dev_etl/skechers/output/" : "NA",
                        stockPoint: 'NA',
                        storeCode:this.state.filterValue.store!=""?this.state.filterValue.store:this.state.getConfigStore
                    }
                    this.props.runOnDemandRequest(data);
                }
            }, 10);
        }
        // } else {
        //     let data = {
        //         runType: "no-filter",
        //         jobName: 'SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode,
        //         store: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.state.channel : "NA",
        //         selected: this.state.selected,
        //         path: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? "s3://com-supplymint-glue/dev_etl/skechers/output/" : "NA",
        //     }
        //     this.props.runOnDemandRequest(data);
        // }
    }
    
    closeSucceeded(type) {
        this.setState({
            succeeded: false
        })
        sessionStorage.removeItem('jobId');
        if (type != "close") {
            this.props.history.push('/inventoryPlanning/summary');
        }
    }
    // onCityKartApply() {
    //     this.setState({
    //         applyCityKart: true,
    //         appliedStockPoint: this.state.stockPoint
    //     })
    // }
// ______________________________START RUNONDEMAND GETJOB_______________________________
    getJobDetails(job){
        this.setState({
            selectedData: job,
            nextSchedule:this.state.nextScheduled[job]
        })
        sessionStorage.removeItem('jobId');
        this.props.jobRunDetailRequest(job);
        this.props.getJobByNameRequest(job);
        this.props.lastJobRequest(job)
        this.props.getFiveTriggersRequest()
        Object.keys(this.state.configParam).map(key =>{
            if( key == job){
                this.props.getConfigFilterRequest(this.state.configParam[job]);
            }
        });        
    }
    changeJob(job){
        this.setState({
            selectedData:job,
            successState:true,
            nextSchedule:this.state.nextScheduled[job]
        })
        sessionStorage.removeItem('jobId');
        this.props.jobRunDetailRequest(job);
        this.props.getJobByNameRequest(job);
        this.props.lastJobRequest(job)
        Object.keys(this.state.configParam).map(key =>{
            if( key == job){
                this.props.getConfigFilterRequest(this.state.configParam[job]);
            }
        });
        this.props.getFiveTriggersRequest()
    }
    openFilter() {
        this.setState({
            openFilter: true
        })
    }
    closeFilter(){
        this.setState({
            openFilter: false
        })
    }
    handleChangeFilter(payload){
        let configurationFilter = this.state.configurationFilter
        configurationFilter[payload.data].changeValue = payload.events.target.value
        this.setState({
            configurationFilter: configurationFilter
        })
    }
    updateApply(){
        this.setState({
            apply:false
        })
    }

      clearFilter(){
        let configurationFilters = this.state.configurationFilter
        let filterNames = this.state.labelConfigFilter
        for (let i = 0; i < filterNames.length; i++) {
            if (configurationFilters[filterNames[i]].isChecked == "True") {
                configurationFilters[filterNames[i]].columnName= configurationFilters[filterNames[i]].columnName
                configurationFilters[filterNames[i]].displayName = configurationFilters[filterNames[i]].displayName
                configurationFilters[filterNames[i]].type = configurationFilters[filterNames[i]].type
                configurationFilters[filterNames[i]].value = configurationFilters[filterNames[i]].value 
                configurationFilters[filterNames[i]].changeValue= ""
                configurationFilters[filterNames[i]].defaultValue = ""
                configurationFilters[filterNames[i]].description =  configurationFilters[filterNames[i]].description
                configurationFilters[filterNames[i]].isChecked = "False"
                configurationFilters[filterNames[i]].selectedValue = []
                configurationFilters[filterNames[i]].valueSearch = ""
            }
        }
        this.setState({
configurationFilter:configurationFilters
        })
       
        this.updateApply()
     
    }
    render() {
      
        const { search, roleData, selectederr } = this.state;
        var result = _.filter(roleData, function (data) {
            return _.startsWith(data.storeName.toLowerCase(), search.toLowerCase());
        });
        return (
            <div>
                <div className="container-fluid">
                    <div className="container_div" id="">
                        <div className="container-fluid">
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                <div className="organisation_container heightAuto inventoryAutoConfig">
                                    <div className="col-md-12 col-sm-12 pad-0 heightEqualParent">
                                        <div className="width_29 displayInline pad-lft-10">
                                            <ul className="list_style">
                                                <li>
                                                    <label className="contribution_mart">
                                                        RUN ON DEMAND
                                                    </label>
                                                </li>
                                                <li>
                                                    <p className="master_para">Run Your rule engine here</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="width_69 displayInline pad-lft-10">
                                            <div className="activeJobs">
                                                <h3>Active Jobs</h3>
                                                <ul className="list-inline displayBlock width100 m-top-3">
                                                    {this.state.getJobs.length!= 0?this.state.getJobs.map((data, i) =><li key={i}>
                                                        <button type="button" value={data} className={this.state.selectedData == data ?"jobBtn m-rgt-10 active":"jobBtn m-rgt-10"} onClick={(e)=>this.changeJob(data)}>{"Job" + (i+1)}</button></li>):null}
                                                </ul>
                                                {/* <label className="displayInline m-top-7">Trigger - <span className="fontBold">Run-on-Demand</span></label> */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 heightEqualParent m-top-15">
                                        <div className="runOnDemandLeft pad-right-0 posRelative float_None">
                                            <div className="configJobDetails pad-0">
                                                <div className="forecastTimer">
                                                    <div className="rightReplenishment autoConfigTimer">
                                                        <div className="replenishmentAnimateDiv">
                                                            <div className="middleReplensh">
                                                                <div className="single-chart">
                                                                    <svg viewBox="0 0 36 36" className="circular-chart green m-bot-0">
                                                                        <path className="circle-bg" fill="#fff"
                                                                            d="M18 2.0845
                                                                            a 15.9155 15.9155 0 0 1 0 31.831
                                                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                                                    />
                                                                    <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
                                                                            <stop offset="50%" stopColor="#ce9ffc" />
                                                                            <stop offset="100%" stopColor="#7367f0" />
                                                                        </linearGradient>
                                                                        <path className="circle" stroke="url(#gradient)"
                                                                            strokeDasharray={this.state.timeTaken+", 100" }
                                                                            d="M36 20.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
                                                                        />
                                                                    </svg>
                                                                </div>
                                                                <div className="autoConfigMid">
                                                                    <div className="autoComfigTime onHoverSvg">
                                                                        <h3>{this.state.timeTaken}</h3>
                                                                        <p>Mins elapsed</p>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <div className="currentJobName">
                                                                <h5>Job Name</h5>
                                                                <label>{this.state.selectedData == "" ? this.state.getJobs[0] : this.state.selectedData}</label>
                                                            </div>
                                                            <button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                                               RUN
                                                            </button>
                                                            <div className="settingDrop summaryDrop dropdown displayInline setUdfMappingMain pad-0 updateEngin m-top-25">
                                                                <button className="btn btn-default dropdown-toggle userModalSelect textElippse" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                {this.state.selectedData == "" ? this.state.getJobs[0] : this.state.selectedData}
                                                                    <i className="fa fa-chevron-down"></i>
                                                                </button>
                                                                <ul className="dropdown-menu widthAuto" aria-labelledby="dropdownMenu1">
                                                                    {this.state.getJobs.length != 0 ? this.state.getJobs.map((jobData, jobKey)=>(
                                                                        <li key={jobKey} onClick={() => this.getJobDetails(jobData)}>
                                                                            <a>{jobData}</a>
                                                                        </li>
                                                                    )):null} 
                                                                </ul>
                                                            </div>
                                                            <div className="bottomReplesh">
                                                                {/* <label className=" displayBlock marginAuto">
                                                                    Last Engine Run
                                                                </label> */}
                                                                <label>
                                                                    <span>Engine Run State</span>
                                                                </label>
                                                                <span className="timeSchedule">{this.state.runState}</span>
                                                            </div>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="runOnDemanRight pad-lft-0">
                                            <div className="configurationDetails">
                                                <div className="configContainer">
                                                    <div className="top-config">
                                                    <div className="col-md-12 headers">
                                                                    <h3 className="displayInline">Choose below option to set scheduler task</h3>
                                                                    {/* <button type="button" className={this.state.isEnabled == "true" || this.state.isEnabled=="TRUE" ? "filterButton" : "btnDisabled filterButton"} onClick={(e) =>this.state.isEnabled == "true" || this.state.isEnabled=="TRUE" ? this.openFilter(e) : null }>Config Planning Parameter */}
                                                                    <button type="button" className={"filterButton"} onClick={(e) =>this.openFilter(e) }>Config Planning Parameter
                                                                       <img src={filter} /> 
                                                                    </button>
                                                                </div>
                                                       
                                                    </div>                                              
                                                    <div className="botRunOnDemand">
                                                        <div className="col-md-12 pad-0 m-top-20">
                                                            <div className="col-md-4 pad-lft-35">
                                                            <img src={calenderIcon} />
                                                                <h4>Last Engine Run Details </h4>
                                                                <label>Last Engine Run </label>
                                                                <div className="time">
                                                                {this.state.lastRunTime} 
                                                                </div>
                                                            </div>      
                                                            <div className="col-md-4 pad-lft-35">
                                                                <img src={calenderTime} />
                                                                <h4 className="active">Current Job Details</h4>
                                                                {/* <label>Last Engine Run </label> */}
                                                                <div className="col-md-12 pad-0">
                                                                    <div className="col-md-6 pad-0">
                                                                        <label>Status</label>
                                                                        <h6>{this.state.jobRunState}</h6>
                                                                    </div>
                                                                    <div className="col-md-6 pad-0">
                                                                        <label>Date & Time</label>
                                                                        <h6>{this.state.lastRun}</h6>
                                                                    </div>
                                                                </div>
                                                            </div>      
                                                            <div className="col-md-4 pad-lft-35">
                                                                <img src={calenderNew} />
                                                                <h4>Last Engine Run Details </h4>
                                                                {/* <label>Last Engine Run </label> */}
                                                                <div className="col-md-12 pad-0">
                                                                    <label>Next scheduled at</label>
                                                                    <h6>{this.state.nextSchedule}</h6>
                                                                </div>
                                                            </div>      
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                    <div className="width_29 displayInline text-center">
                                        {/* <button type="button" className="runBtn disable">Run</button> */}

                                        {/* {this.state.isEnabled == "true" || this.state.isEnabled=="TRUE"?
                                          this.state.status !="RUNNING"? this.state.apply ?
                                          <button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                                RUN
                                             </button>:<div><button type="button" className="btnDisabled launchBtn" >
                                                RUN
                                             </button> <span className="dropShow">Please apply filter to run</span> </div> 

                                          :null                                               
                                          :<button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                                RUN
                                             </button> }
                                        
                                         {this.state.stop ? <button type="button" onClick={(e) => this.onStop(e)} className="launchBtn" >
                                                STOP
                                        </button> : null} */}
                                       
                                        <button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                            RUN
                                        </button> 
                                      
                                        {/*{this.state.status == "RUNNING" || this.state.status == "STOPPING" ? null : sessionStorage.getItem('partnerEnterpriseName') == 'SKECHERS' ? !this.state.filter ? this.state.run ? 
                                            <button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                                RUN
                                             </button> : null : <div><button type="button" className="btnDisabled launchBtn" >
                                                RUN
                                             </button> <span className="dropShow">Please apply filter to run</span> </div> : null}
                                             {sessionStorage.getItem('partnerEnterpriseName') != 'SKECHERS' ? this.state.run ? <button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                                RUN
                                             </button> : null : null}*/}
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                    {this.state.openFilter ? <FilterModal {...this.state} {...this.props} updateApply={(e)=>this.updateApply(e)} updateStoreSearch={(e)=>this.updateStoreSearch(e)} updateCheckedValue={(e)=>this.updateCheckedValue(e)} getConfigStore={this.state.getConfigStore} handleChangeFilter={(e)=> this.handleChangeFilter(e)} closeFilter={(e)=>this.closeFilter(e)} Change={(e)=>this.Change(e)} onFilterApply={(e)=> this.onFilterApply(e)} updateConfigFilter={(e)=>this.updateConfigFilter(e)} updateSelected={(e)=>this.updateSelected(e)} updateStoreCodeList={(e)=>this.updateStoreCodeList(e)}/> :null}
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                    {this.state.succeeded ? <Succeeded modalState={this.state.modalState} closeSucceeded={(e) => this.closeSucceeded(e)} /> : null}
                </div >
                <Footer />
            </div>
        );
    }
}
export default ReplenishmentRunOnDemandNew;