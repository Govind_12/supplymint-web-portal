import React from 'react';
import {Link} from 'react-router-dom';
import successIcon from '../../assets/greencheck.png';
import failedIcon from '../../assets/failed-icon.svg';

class Succeeded extends React.Component {
    render() {
        console.log("SUCCEEDED");
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content new-success-modal">
                    <div className="nsm-head">
                        <div className={this.props.modalState == "FAILED" ? "icon nsmh-new" : "icon"}>
                            <img src={this.props.modalState == "FAILED" ? failedIcon : successIcon} />
                            {this.props.modalState == "SUCCEEDED" ? <h2>Succeeded</h2> : <h2 className="textRed">Failed</h2>}
                        </div>
                    </div>
                    <div className="nsm-body">
                        <p>Last engine run <label className="bold">{this.props.modalState}</label>!</p>
                        {this.props.modalState == "SUCCEEDED" ? 
                        <p className="m-top-10">Click on the summary to view report</p>
                         : null}
                    </div>
                    <div className="nsm-footer">
                        {this.props.modalState == "SUCCEEDED" ? 
                        <button type="button" className="confirm-btn" onClick={(e) => this.props.closeSucceeded('summary')} >View Summary</button>
                        : null}
                        <button type="button" onClick={(e) => this.props.closeSucceeded('close')} className="close-btn">Close</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Succeeded;