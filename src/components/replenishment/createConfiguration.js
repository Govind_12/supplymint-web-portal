import React, { useState, useEffect } from 'react';
import SideBar from '../sidebar';
import BraedCrumps from '../breadCrumps';
import editIcon from '../../assets/actions-buttons.svg';
import delIcon from '../../assets/new-delete.svg';
import { ViewActionConfigModal } from './viewActionConfigModal';
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
import downIcon from "../../assets/select-icon.svg";
import circleLoader from "../../assets/loader-small.svg";
import ConfigurationDeleteModal from '../loaders/configurationDeleteModal';

// export const CreateConfiguration = (props) => {
class CreateConfiguration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            storeCodeList: [],
            tabs: "create",
            configurationFilter: {},
            filterName: [],
            showDisplayName: "",
            search: "",
            getSearch: "",
            viewConfiguration: {},
            listConfiguration: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            configModal: false,
            configModaldata: "",
            keysConfigData: [],
            viewConfigurationdata: {},
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",
            storeCode: "",
            toastLoader: false,
            toastMsg: "",
            selected: [],
            selectederr: "",
            storeSearch: "",
            headerMsg: "",
            paraMsg: "",
            radioChange: "",
            confirmModal: false,
            checkedValue: [],
            listConfig:[],
            saveCol:[]
        }

    }
    Change(e) {
        this.setState({
            storeSearch: e.target.value
        })
    }
    selected() {
        if (this.state.selected.length == 0) {
            this.setState({
                selectederr: true
            });
        } else {
            this.setState({
                selectederr: false
            })
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
    }


    componentWillMount() {

        this.props.getFilterSectionRequest('data')
        let payload = {
            type: 1,
            no: 1,
            search: ""

        }
        this.props.invGetAllConfigRequest(payload)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.getStoreCode.isSuccess) {
            
              let configurationFilter = this.state.configurationFilter
                configurationFilter.Store.selectedValue=[]
                configurationFilter.Store.isChecked= false
          

            if (nextProps.replenishment.getStoreCode.data.resource != null) {
       
               
                this.setState({
                    storeCodeList: nextProps.replenishment.getStoreCode.data.resource.store,
                    storeCode:nextProps.replenishment.getStoreCode.data.resource.storeCode,
                    selected: [],
                    configurationFilter:configurationFilter
                })
            } else {
      
                this.setState({
                    storeCodeList: [],
                    selected:[],
                    configurationFilter:configurationFilter
                })
            }
            this.props.getStoreCodeClear()
        }

        if (nextProps.replenishment.getFilterSection.isSuccess) {

            this.setState({
                configurationFilter: nextProps.replenishment.getFilterSection.data.resource,
                filterName: Object.keys(nextProps.replenishment.getFilterSection.data.resource),
                loader: false,
            })
            let configurationFilter = nextProps.replenishment.getFilterSection.data.resource
            let filterName = Object.keys(configurationFilter)
            let filter = {}
            for (let i = 0; i < filterName.length; i++) {
                if (configurationFilter[filterName[i]].type == "T") {
                    if (configurationFilter[filterName[i]].changeValue != "") {
                        filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue

                    }


                } else if (configurationFilter[filterName[i]].type == "L") {
                    if (configurationFilter[filterName[i]].selectedValue.length != 0) {
                        filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                    }
                }
            }
            let payload = {
                filter: filter,
                storeCode:"NA"
            }
            //this.props.getStoreCodeRequest(payload)

            this.props.getFilterSectionClear()
        } else if (nextProps.replenishment.getFilterSection.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getFilterSection.message.error == undefined ? undefined : nextProps.replenishment.getFilterSection.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getFilterSection.message.status,
                errorCode: nextProps.replenishment.getFilterSection.message.error == undefined ? undefined : nextProps.replenishment.getFilterSection.message.error.errorCode
            })
            this.props.getFilterSectionClear();
        } else if (nextProps.replenishment.getFilterSection.isLoading) {
            this.setState({
                loader: true
            })
        }
        if (nextProps.replenishment.invGetAllConfig.isSuccess) {
            this.setState({
                viewConfiguration: nextProps.replenishment.invGetAllConfig.data.resource == null ? [] : nextProps.replenishment.invGetAllConfig.data.resource,
                listConfiguration: nextProps.replenishment.invGetAllConfig.data.resource == null ? [] : Object.keys(nextProps.replenishment.invGetAllConfig.data.resource),
                prev: nextProps.replenishment.invGetAllConfig.data.resource == null ? 0 : nextProps.replenishment.invGetAllConfig.data.prePage,
                current: nextProps.replenishment.invGetAllConfig.data.resource == null ? 0 : nextProps.replenishment.invGetAllConfig.data.currPage,
                next: nextProps.replenishment.invGetAllConfig.data.resource == null ? 0 : nextProps.replenishment.invGetAllConfig.data.currPage + 1,
                maxPage: nextProps.replenishment.invGetAllConfig.data.resource == null ? 0 : nextProps.replenishment.invGetAllConfig.data.maxPage,
                loader: false,
            })
         
            
            this.props.invGetAllConfigClear();
        } else if (nextProps.replenishment.invGetAllConfig.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.invGetAllConfig.message.error == undefined ? undefined : nextProps.replenishment.invGetAllConfig.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.invGetAllConfig.message.status,
                errorCode: nextProps.replenishment.invGetAllConfig.message.error == undefined ? undefined : nextProps.replenishment.invGetAllConfig.message.error.errorCode
            })
            this.props.invGetAllConfigClear();
        } else if (nextProps.replenishment.invGetAllConfig.isLoading) {
            this.setState({
                loader: true
            })
        }

        if (nextProps.replenishment.statusChange.isSuccess) {
            this.setState({

                successMessage: nextProps.replenishment.statusChange.data.message,
                loader: false,
                success: true,

            })
            this.props.statusChangeClear();
        } else if (nextProps.replenishment.statusChange.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.statusChange.message.error == undefined ? undefined : nextProps.replenishment.statusChange.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.statusChange.message.status,
                errorCode: nextProps.replenishment.statusChange.message.error == undefined ? undefined : nextProps.replenishment.statusChange.message.error.errorCode
            })
            this.props.statusChangeClear();
        } else if (nextProps.replenishment.statusChange.isLoading) {
            this.setState({
                loader: true
            })
        }


        if (nextProps.replenishment.deleteConfig.isSuccess) {
            this.setState({

                successMessage: nextProps.replenishment.deleteConfig.data.message,
                loader: false,
                success: true,

            })
            this.props.deleteConfigClear();
        } else if (nextProps.replenishment.deleteConfig.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.deleteConfig.message.error == undefined ? undefined : nextProps.replenishment.deleteConfig.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.deleteConfig.message.status,
                errorCode: nextProps.replenishment.deleteConfig.message.error == undefined ? undefined : nextProps.replenishment.deleteConfig.message.error.errorCode
            })
            this.props.statusChangeClear();
        } else if (nextProps.replenishment.deleteConfig.isLoading) {
            this.setState({
                loader: true
            })
        }

        if (nextProps.replenishment.configCreate.isSuccess) {
            this.setState({

                successMessage: nextProps.replenishment.configCreate.data.message,
                loader: false,
                success: true,

            })
            this.props.configCreateClear();
        } else if (nextProps.replenishment.configCreate.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.configCreate.message.error == undefined ? undefined : nextProps.replenishment.configCreate.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.configCreate.message.status,
                errorCode: nextProps.replenishment.configCreate.message.error == undefined ? undefined : nextProps.replenishment.configCreate.message.error.errorCode
            })
            this.props.configCreateClear();
        } else if (nextProps.replenishment.configCreate.isLoading) {
            this.setState({
                loader: true
            })
        }


    }

    // openDropDown(displayName) {

    //     if (this.state.showDisplayName == displayName) {

    //         this.setState({
    //             showDisplayName: ""
    //         })
    //     } else {
    //         this.setState({
    //             showDisplayName: displayName
    //         })

    //     }
    // }
    handleChange(e, inputid, value) {

        // if (e.target.id == inputid) {

        let filterConfig = this.state.configurationFilter
        filterConfig[value].changeValue = e.target.value

        this.setState({
            configurationFilter: filterConfig
        })


    }

    checkFilter(value) {
        let filterConfig = this.state.configurationFilter
    
        let flag = false
        if (filterConfig[value].type == "T") {
            if (filterConfig[value].changeValue != "") {
                if (filterConfig[value].isChecked == "True") {
                    filterConfig[value].isChecked = "False"
                } else {
                    filterConfig[value].isChecked = "True"
                    this.state.checkedValue.push(filterConfig[value].isChecked)
                }
                flag = true
            } else {
                this.setState({
                    toastMsg: "Enter " + filterConfig[value].displayName,
                    toastLoader: true,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 1500);

            }
        } else if (filterConfig[value].type == "L"  ) {
            if (filterConfig[value].selectedValue.length != 0) {
                if (filterConfig[value].isChecked == "True") {
                    filterConfig[value].isChecked = "False"
                } else {
                    filterConfig[value].isChecked = "True"
                    this.state.checkedValue.push(filterConfig[value].isChecked)
                }
            
                if(filterConfig[value].displayName!="Store"){
                       flag = true
                }
            }
            else {
                this.setState({
                    toastMsg: "Select " + filterConfig[value].displayName,
                    toastLoader: true,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 1500);

            }
        }
        this.setState({
            configurationFilter: filterConfig,
            checkedValue: this.state.checkedValue
        })
        if(flag){
        setTimeout(() => {
        let configurationFilter = this.state.configurationFilter
        let filterName = Object.keys(configurationFilter)
                let filter = {}
        for (let i = 0; i < filterName.length; i++) {
            if(configurationFilter[filterName[i]].displayName != "Store" ){
           for (let i = 0; i < filterName.length; i++) {
                        if (configurationFilter[filterName[i]].isChecked == "True") {
                            if (configurationFilter[filterName[i]].type == "T") {
                                if (configurationFilter[filterName[i]].changeValue != "") {
                                    filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
                                }
                            } else if (configurationFilter[filterName[i]].type == "L") {
                                if (configurationFilter[filterName[i]].selectedValue.length != 0 && configurationFilter[filterName[i]].displayName != "Store") {
                                    filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                                }
                            }
                        }
                    }
                 

            }
        }

                    let payload = {
                        filter: filter,
                        storeCode:"NA"
                    }
            
                    this.props.getStoreCodeRequest(payload)
                }, 10)
        }
        
    }


    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }

    onSearchFunction(id) {
        
        var input = document.getElementById(id);
        var filter = input.value.toUpperCase();
        
        var ul = document.getElementById("myUL");
        
        var li = ul.getElementsByTagName("li");
        for (var i = 0; i < li.length; i++) {
            var a = li[i].getElementsByTagName("label")[0];
            var txtValue = a.textContent || a.innerText;
            
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                
                li[i].classList.remove('hideElement');
            } else {
                li[i].classList.add('hideElement');
            }
        }
        this.displayNoResult(li.length)
    }

    displayNoResult(allLI) {
        var hiddenLILength = document.querySelectorAll('li.hideElement');
        if (allLI === hiddenLILength.length) {
            document.getElementById('noResult').classList.remove('hideElement')
        } else {
            document.getElementById('noResult').classList.add('hideElement')
        }
    }
    inputChange(e, id, data) {
        
        let filterConfig = this.state.configurationFilter
        filterConfig[data].valueSearch = e.target.value
        
        this.setState({
            configurationFilter: filterConfig
        })
        this.onSearchFunction(id)

    }
    checkAllFilter(data) {
        let filterConfig = this.state.configurationFilter
        if (!this.state.configurationFilter[data].selectedValue.includes(this.state.configurationFilter[data].value) && this.state.configurationFilter[data].selectedValue == "") {
            let selectedValue = this.state.configurationFilter[data].selectedValue
            selectedValue.push(...this.state.configurationFilter[data].value)

        } else {
            let selectedValue = this.state.configurationFilter[data].selectedValue
            let values = this.state.configurationFilter[data].value
            for (let i = 0; i < selectedValue.length; i++) {
                for (let j = 0; j < values.length; j++) {
                    if (selectedValue[i] == values[j]) {
                        selectedValue.splice(i, 1)
                    }
                }
            }

        }
        this.setState({
            configurationFilter: filterConfig
        })
        setTimeout(() => {
            let configurationFilter = this.state.configurationFilter
            let filterName = Object.keys(configurationFilter)
            let filter = {}
            let flag = false
            for (let i = 0; i < filterName.length; i++) {
                if (configurationFilter[filterName[i]].isChecked == "True") {
                    flag = true
                    if (configurationFilter[filterName[i]].type == "T") {
                        if (configurationFilter[filterName[i]].changeValue != "") {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
                        }
                    } else if (configurationFilter[filterName[i]].type == "L") {
                        if (configurationFilter[filterName[i]].selectedValue.length != 0) {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                        }
                    }
                }
            }
            if(flag){
            let payload = {
                filter: filter,
                    storeCode:"NA"
            }
            this.props.getStoreCodeRequest(payload)
            }
        }, 50)

    }

    valueCheck(value, data) {

        let filterConfig = this.state.configurationFilter
        if (!this.state.configurationFilter[data].selectedValue.includes(value)) {
            let selectedValue = this.state.configurationFilter[data].selectedValue
            selectedValue.push(value)
        } else {
            let selectedValue = this.state.configurationFilter[data].selectedValue
            for (let i = 0; i < selectedValue.length; i++) {
                if (selectedValue[i] == value) {
                    selectedValue.splice(i, 1)
                }
            }

        }
        this.setState({
            configurationFilter: filterConfig
        })
        setTimeout(() => {
            let configurationFilter = this.state.configurationFilter
            let filterName = Object.keys(configurationFilter)
            let filter = {}
            let flag = false
            for (let i = 0; i < filterName.length; i++) {
                if (configurationFilter[filterName[i]].isChecked == "True") {
                    flag= true
                    if (configurationFilter[filterName[i]].type == "T") {
                        if (configurationFilter[filterName[i]].changeValue != "") {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
                        }
                    } else if (configurationFilter[filterName[i]].type == "L") {
                        if (configurationFilter[filterName[i]].selectedValue.length != 0) {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                        }
                    }
                }
            }
            if(flag){
            let payload = {
                filter: filter,
                    storeCode:"NA"
            }
            this.props.getStoreCodeRequest(payload)
            }
        }, 10)

    }
    blurHandleChange(e, id, value) {
        let filterConfig = this.state.configurationFilter
        if (filterConfig[value].changeValue == "") {
            filterConfig[value].changeValue = filterConfig[value].defaultValue
        }
        this.setState({
            configurationFilter: filterConfig
        })
        setTimeout(() => {
            let configurationFilter = this.state.configurationFilter
            let filterName = Object.keys(configurationFilter)
            let filter = {}
            let flag = false
            for (let i = 0; i < filterName.length; i++) {
                if (configurationFilter[filterName[i]].isChecked == "True") {
                   flag= true
                    if (configurationFilter[filterName[i]].type == "T") {
                        if (configurationFilter[filterName[i]].changeValue != "") {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
                        }
                    } else if (configurationFilter[filterName[i]].type == "L") {
                        if (configurationFilter[filterName[i]].selectedValue.length != 0) {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                        }
                    }
                }
            }
            if(flag){
            let payload = {
                filter: filter,
                    storeCode:"NA"
            }
            this.props.getStoreCodeRequest(payload)
            }
        }, 10)

    }
    enabledCheck(value, data) {
        let configName = data
        let isEnabled = ""
        if (value == "ENABLED") {
            isEnabled = "DISABLED"
        } else {
            isEnabled = "ENABLED"
        }
        let payload = {
            configName: configName,
            isEnabled: isEnabled
        }
        this.props.statusChangeRequest(payload)
    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.replenishment.invGetAllConfig.data.prePage,
                current: this.props.replenishment.invGetAllConfig.data.currPage,
                next: this.props.replenishment.invGetAllConfig.data.currPage + 1,
                maxPage: this.props.replenishment.invGetAllConfig.data.maxPage,
            })
            if (this.props.replenishment.invGetAllConfig.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.invGetAllConfig.data.currPage - 1,
                    search: this.state.getSearch
                }
                this.props.invGetAllConfigRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.replenishment.invGetAllConfig.data.prePage,
                current: this.props.replenishment.invGetAllConfig.data.currPage,
                next: this.props.replenishment.invGetAllConfig.data.currPage + 1,
                maxPage: this.props.replenishment.invGetAllConfig.data.maxPage,
            })
            if (this.props.replenishment.invGetAllConfig.data.currPage != this.props.replenishment.invGetAllConfig.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.invGetAllConfig.data.currPage + 1,
                    search: this.state.getSearch
                }
                this.props.invGetAllConfigRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.replenishment.invGetAllConfig.data.prePage,
                current: this.props.replenishment.invGetAllConfig.data.currPage,
                next: this.props.replenishment.invGetAllConfig.data.currPage + 1,
                maxPage: this.props.replenishment.invGetAllConfig.data.maxPage,
            })
            if (this.props.replenishment.invGetAllConfig.data.currPage <= this.props.replenishment.invGetAllConfig.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.getSearch
                }
                this.props.invGetAllConfigRequest(data)
            }
        }
    }
    onkeySearch(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        this.handleViewSearch(e)
    }

    handleViewSearch(e) {
        this.setState({
            getSearch: e.target.value
        })
    }
    onSearch(e) {
        if (this.state.getSearch == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true,
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false,
                })
            }, 1500)
        } else {
            this.setState({
                type: 3,
                no: 1,
            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.getSearch,
            };
            this.props.invGetAllConfigRequest(data);
        }
    }
    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            getSearch: "",
        })
        let data = {
            no: 1,
            type: 1,
            getSearch: "",

        }
        this.props.invGetAllConfigRequest(data);
    }
    viewConfigurationModal(data, modal) {
        if (modal == "open") {
            let viewConfigurationdata = this.state.viewConfiguration[data]
            let keysviewConfigurationdata = Object.keys(viewConfigurationdata)
            for (let j = 0; j < keysviewConfigurationdata.length; j++) {
                if (keysviewConfigurationdata[j] == "isEnabled") {
                    keysviewConfigurationdata.splice(j, 1)
                }
            }
            this.setState({
                configModal: !this.state.configModal,
                configModaldata: data,
                keysConfigData: keysviewConfigurationdata,
                viewConfigurationdata: viewConfigurationdata
            })
        } else {
            this.setState({
                configModal: !this.state.configModal,
            })
        }
    }
    CreateConfiguration() {
        let configurationFilters = this.state.configurationFilter
        let filterNames = this.state.filterName
        let label = {}
        for (let i = 0; i < filterNames.length; i++) {
            if (configurationFilters[filterNames[i]].isChecked == "True") {
                label[configurationFilters[filterNames[i]].label] = {
                    columnName: configurationFilters[filterNames[i]].columnName,
                    displayName: configurationFilters[filterNames[i]].displayName,
                    type: configurationFilters[filterNames[i]].type,
                    value: configurationFilters[filterNames[i]].type == "T" ? configurationFilters[filterNames[i]].changeValue :
                        configurationFilters[filterNames[i]].selectedValue,
                    changeValue: "",
                    defaultValue: "",
                    description: configurationFilters[filterNames[i]].description,
                    isChecked: "False",
                    selectedValue: [],
                    valueSearch: ""
                }
                this.state.saveCol.push(configurationFilters[filterNames[i]].displayName)
              
            }else if(configurationFilters[filterNames[i]].isChecked == "False"){
                for (let j = 0; j < this.state.saveCol.length; j++) {
                    if(this.state.saveCol[j] == configurationFilters[filterNames[i]].displayName){
                        this.state.saveCol.splice(j,1)
                    }
                }
            }
        }
        this.setState({
            saveCol : this.state.saveCol
        })
       
        this.functionSave(label)
    }
    functionSave(label){
        if (this.state.saveCol != "") {
            let payload = {
                label: label,

            }
            this.props.configCreateRequest(payload)
        }else{
            this.setState({
                toastMsg: "Select atleast one" ,
                toastLoader: true,
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);

        }
    }
    clearConfiguration() {
        this.props.getFilterSectionRequest('data')
    }
    onnChange(id, value, data) {
        let match = _.find(this.state.storeCodeList, function (o) {
            return o.storeCode == id;
        })
        let filterConfig = this.state.configurationFilter
        if (!this.state.configurationFilter[data].selectedValue.includes(value)) {
            let selectedValue = this.state.configurationFilter[data].selectedValue
            selectedValue.push(match.storeCode + "-" + value)
        }
        this.setState({
            selected: this.state.selected.concat(match),
            configurationFilter: filterConfig
        });
        let idd = match.storeCode;
        var array = this.state.storeCodeList;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === idd) {
                array.splice(i, 1);
            }
        }
   
        this.setState({
            storeCodeList: array,
            selectederr: false,
        })
    }

    onDelete(e, value, data) {
        let match = _.find(this.state.selected, function (o) {
            return o.storeCode == e;
        })
        let filterConfig = this.state.configurationFilter
        if (this.state.configurationFilter[data].selectedValue.includes(match.storeCode + "-" + value)) {
            let selectedValue = this.state.configurationFilter[data].selectedValue
            for (let i = 0; i < selectedValue.length; i++) {
                if (selectedValue[i] == match.storeCode + "-" + value) {
                    selectedValue.splice(i, 1)
                }
            }
        }
        this.setState({
            configurationFilter: filterConfig,
            storeCodeList: this.state.storeCodeList.concat(match)
        });

        var array = this.state.selected;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === e) {
                array.splice(i, 1);
            }
        }
        this.setState({
            selected: array,
            // filter: true,
            // remove: false
        })
        // if (this.props.replenishment.getZone.isSuccess && !this.props.replenishment.getGrade.isSuccess && !this.props.replenishment.getStoreCode.isSuccess) {
        //     this.props.replenishment.getZone.data.resource.store = this.state.roleData.concat(match)
        // } else if (this.props.replenishment.getGrade.isSuccess && !this.props.replenishment.getStoreCode.isSuccess) {
        //     this.props.replenishment.getGrade.data.resource.store = this.state.roleData.concat(match)
        // } else if (this.props.replenishment.getStoreCode.isSuccess) {
        //     this.props.replenishment.getStoreCode.data.resource.store = this.state.roleData.concat(match);
        // }

    }

    // ________________--DELETE CONFIRMATION MODAL________________________________
    deleteConfig(dat) {
        this.setState({
            headerMsg: "Are you sure you want to delete",
            paraMsg: "Click confirm to continue.",
            radioChange: dat,
            confirmModal: true,
        })
    }

    deleteConfigData(data) {
        let payload = {
            configName: data
        }
        this.props.deleteConfigRequest(payload)
    }

    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }


    render() {
  
        const { search } = this.state
        var result = _.filter(this.state.filterName, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        });
        const { storeSearch, storeCodeList, selectederr } = this.state;
        var resultStoreCodeList = _.filter(storeCodeList, function (data) {
           
            return _.startsWith(data.storeName.toLowerCase(), storeSearch.toLowerCase());
        });

        return (
            <div>

                <div className="container-fluid">
                    <div className="container_div" id="home">
                        <div className="container-fluid">
                            <div className="container_div" id="">
                                <div className="container-fluid pad-0">
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                        <div className="replenishment_container weekelyPlanning monthlyPlanning">
                                            <div className="col-md-12 pad-0 inventoryAutoConfig createConfigMain">
                                                <div className="col-md-6 col-sm-6 pad-lft-0">
                                                    <ul className="list_style">
                                                        <li>
                                                            <label className="contribution_mart">
                                                                CONFIGURATION
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <p className="master_para">Manage your rule engine Jobs Cofiguration</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            
                                            </div>
                                            <div className="col-md-12 itemUdfSet configTabs">
                                                <div className="col-md-6 pad-0">
                                                    <div className="switchTabs m-top-20">
                                                        <ul className="list_style ">
                                                            <li className={this.state.tabs == "create" ? "displayPointer activeTab m-rgt-25" : "displayPointer m-rgt-25"} onClick={() => this.setState({ tabs: "create" })} >
                                                                <label className="displayPointer">Create Configuration</label>
                                                            </li>
                                                            <li className={this.state.tabs == "view" ? "displayPointer activeTab m-rgt-25" : "displayPointer m-rgt-25"} onClick={() => this.setState({ tabs: "view" })}>
                                                                <label className="displayPointer">View Configurations</label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-12 pad-0 searchDataSync">
                                                    <ul className="list-inline manageSearch width100 textRight m-top-20">
                                                        <li className="paginationWidth50">
                                                            {/* <form className="newSearch m0"> */}
                                                            {this.state.tabs == "view" ? <input type="text" placeholder="Search..." value={this.state.getSearch} onKeyDown={(e) => this.onkeySearch(e)} onChange={(e) => this.handleViewSearch(e)} className="search-box width100" />
                                                                : <input type="text" placeholder="Search..." value={this.state.search} onChange={(e) => this.handleSearch(e)} className="search-box width100" />}
                                                            {/* </form> */}
                                                            {this.state.tabs == "view" && this.state.type == 3 ? <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span> : null}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className=" col-md-12 m-top-20">
                                                <div >
                                                    {this.state.tabs == "create" ? <div><div className="createConifigurations displayInline displayFlex width100">
                                                        <div className={this.state.filterName.length < 12 ? "col-md-12 pad-0" : "col-md-10 pad-0"}>
                                                            {result.map((data, key) =>
                                                                <div className="col-md-3 pad-0 eachConfigMain" key={key}>
                                                                    <div className="eachConfig">
                                                                        <div className="col-md-6 pad-0"><h3>{this.state.configurationFilter[data].displayName}</h3></div>
                                                                        <div className="col-md-6 pad-0">
                                                                            <div className="verticalTop">
                                                                                <ul className="pad-0 posRelative">
                                                                                    <li><label className="checkBoxLabel0 displayPointer pad-0 posInherit">
                                                                                        <input type="checkBox" checked={this.state.configurationFilter[data].isChecked == "True"} onChange={(e) => this.checkFilter(data)} id={this.state.configurationFilter[data].displayName} /> <span className="checkmark1"></span></label>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-md-12 pad-0 m-top-5 posRelative">
                                                                            {this.state.configurationFilter[data].type == "L" && this.state.configurationFilter[data].displayName == "Store" ? <div className="dropdown-toggle displayPointer selectWidth genericSelect2 displayInGrid afteNone itemCenter" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
                                                                                {this.state.selected.length != 0 ? this.state.selected.length + " Stores selected" : "Select " + this.state.configurationFilter[data].displayName}
                                                                            </div> 
                                                                            : <div className="dropdown-toggle displayPointer selectWidth genericSelect2 displayInGrid afteNone itemCenter" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
                                                                                    {this.state.configurationFilter[data].selectedValue.length != 0 ? this.state.configurationFilter[data].selectedValue.join("|") : "Select " + this.state.configurationFilter[data].displayName}
                                                                                </div>}
                                                                            {this.state.configurationFilter[data].type == "T" ?
                                                                                <input type="text" className="selectWidth genericSelect2 createConfigInput displayInGrid itemCenter pad-7" id={"input " + this.state.configurationFilter[data].displayName} value={this.state.configurationFilter[data].changeValue} placeholder={"Enter " + this.state.configurationFilter[data].displayName} onChange={(e) => this.handleChange(e, "input" + this.state.configurationFilter[data].displayName, data)} onBlur={(e) => this.blurHandleChange(e, "input" + this.state.configurationFilter[data].displayName, data)} /> : null}

                                                                            {/* ________________________________________________ */}
                                                                            {this.state.configurationFilter[data].displayName == "Store" ? <div className="dropdownSketchFilter right0 skechersDrop storeDropPos dropDownSkech  dropdown-menu" aria-labelledby="dropdownMenu1">
                                                                                <div className="SketchFilter">
                                                                                    <p>Choose Stores from below list :</p>
                                                                                    <input type="text" placeholder="Type to Search…" value={storeSearch} onChange={(e) => this.Change(e)} />
                                                                                </div>
                                                                                <div className="sketchStoreDrop">
                                                                                    <div className="lftStoreDiv m-rgt-20">
                                                                                        <h2>Choose Stores</h2>
                                                                                        <div className="storeChoose">
                                                                                            <ul className="list-inline">
                                                                                                {resultStoreCodeList.length == 0 ? null : resultStoreCodeList.map((dataa, i) => <li key={i} onClick={(e) => this.onnChange(`${dataa.storeCode}`, dataa.storeName, data)} >

                                                                                                    <span className="onHover"></span>
                                                                                                    <p className="contentStore pad-5">{ dataa.storeCode +"-"+ dataa.storeName}</p>

                                                                                                </li>)}

                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="lftStoreDiv">
                                                                                        <h2>Selected Stores</h2>
                                                                                        <div className="storeChoose pad-10">
                                                                                            <ul className="list-inline">
                                                                                                <li>
                                                                                                    {this.state.selected.length == 0 ? null : this.state.selected.map((dataa, i) => <div key={dataa.storeCode} className="">
                                                                                                        <p className="contentStore">{dataa.storeCode+"-"+ dataa.storeName} </p>
                                                                                                        <span onClick={(e) => this.onDelete(dataa.storeCode, dataa.storeName, data)} value={dataa.storeCode} className="close_btn_demand" aria-hidden="true">&times;</span>
                                                                                                    </div>)}
                                                                                                </li>
                                                                                            </ul>

                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div> : <div className="searchDropdown dropdown-menu" aria-labelledby="dropdownMenu1">
                                                                                    <input type="search" className="textIndent30" placeholder="Search..." id={"search" + this.state.configurationFilter[data].displayName} value={this.state.configurationFilter[data].valueSearch} onChange={(e) => this.inputChange(e, "search" + this.state.configurationFilter[data].displayName, data)} />
                                                                                    <ul className="pad-0 posRelative dropdownCheckBox dropScroll m-top-10" id="myUL">
                                                                                        <li><label className="checkBoxLabel0 displayPointer pad-0 posInherit">
                                                                                            <input type="checkBox" checked={this.state.configurationFilter[data].value.length == this.state.configurationFilter[data].selectedValue.length} onChange={(e) => this.checkAllFilter(data)} id="selectAll" />
                                                                                            <label htmlFor="selectAll">Select All</label>
                                                                                            <span className="checkmark1"></span></label>
                                                                                        </li>
                                                                                        {this.state.configurationFilter[data].value.length != 0 ?
                                                                                            this.state.configurationFilter[data].value.map((vdata, valuekey) =>
                                                                                                <li key={valuekey} ><label className="checkBoxLabel0 displayPointer pad-0 posInherit">
                                                                                                    <input type="checkBox" id={vdata} checked={this.state.configurationFilter[data].selectedValue.includes(vdata)} onChange={(e) => this.valueCheck(vdata, data)} />
                                                                                                    <label htmlFor={vdata}>{vdata}</label>
                                                                                                    <span className="checkmark1"></span></label>
                                                                                                </li>)
                                                                                            : <li ><label>No Data Found</label></li>}
                                                                                        <li className="hideElement" id="noResult">
                                                                                            <label> No Data Found</label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>}

                                                                            <p className="m-top-5"><span>Description :</span> {this.state.configurationFilter[data].description}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )}

                                                        </div>
                                                        {this.state.filterName.length > 12 ? <div className="col-md-2 pad-0 eachConfig configRightPagination displayFlex justifyCenter">
                                                            <div className="configRight textCenter">
                                                                <p>Showing 1 of 5 page</p>
                                                                <label className="mar-bot-10">Prev</label>
                                                                <div className="leftShift">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="21" viewBox="0 0 12 21">
                                                                        <path fill="none" fillRule="evenodd" stroke="#9100ff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.6" d="M10.281.938l-9.208 9.208 9.208 9.208" />
                                                                    </svg>
                                                                </div>
                                                                <div className="rightShift">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="21" viewBox="0 0 12 21">
                                                                        <path fill="none" fillRule="evenodd" stroke="#9100ff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.6" d="M1.719.938l9.208 9.208-9.208 9.208" />
                                                                    </svg>
                                                                </div>
                                                                <label className="m-top-10">Next</label>
                                                            </div>
                                                        </div> : null}
                                                    </div>
                                                        {this.state.tabs == "create" ? <div className="col-md-12 headerTopAction textLeft pad-0 m-top-20">
                                                            <button type="button" className="clearBtnGeneric m-rgt-15" onClick={(e) => this.clearConfiguration(e)}>Clear</button>
                                                            <button type="button" className="saveBtnGeneric" onClick={(e) => this.CreateConfiguration(e)}>Save</button>
                                                        </div> : null}
                                                    </div>
                                                        : this.state.tabs == "view" ? <div className="viewConfig promotionalEvents">
                                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 tableGeneric bordere3e7f3 promotionalEventTable">
                                                                <div className="Zui-wrapper">
                                                                    <div className="scrollableOrgansation zui-scrollerHistory table-scroll scrollableTableFixed heightAuto">
                                                                        <table className="table scrollTable zui-table UserManageTable manageUserTable border-bot-table">
                                                                            <thead className="tableHeadBg">
                                                                                <tr>
                                                                                    <th className="fixed-side-user fixed-side1 bgFixed">
                                                                                        <label className="width65 pad-lft-5">Action</label>
                                                                                    </th>
                                                                                    <th className="positionRelative paginationWidth40">
                                                                                        <label>Configuration Name</label>
                                                                                    </th>
                                                                                    <th className="positionRelative">
                                                                                        <label>Label & Values</label>
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                {this.state.listConfiguration.length != 0 ? this.state.listConfiguration.map((data, key) =>

                                                                                    <tr key={key}>
                                                                                        <td className="fixed-side1 displayFlex">
                                                                                            <ul className="list-inline m0 float_None alignMiddle">
                                                                                                <li>
                                                                                                    <button className="edit_button heightAuto borderNoneAll btnHover"><img src={delIcon} id={"del" + data} className="width15" onClick={(e) => this.deleteConfig(data)} /></button>
                                                                                                    <button className="edit_button heightAuto borderNoneAll btnHover" onClick={(e) => this.viewConfigurationModal(data, "open")}>
                                                                                                        <svg className="width20" xmlns="http://www.w3.org/2000/svg" width="23" height="15" viewBox="0 0 23 15">
                                                                                                            <path fill="#000" fillRule="nonzero" d="M22.794 6.814c-.051-.077-1.244-1.73-3.224-3.382C16.93 1.195 14.138 0 11.498 0c-2.64 0-5.432 1.195-8.071 3.432C1.447 5.085.254 6.737.228 6.814c-.304.406-.304.991 0 1.372.026.077 1.219 1.73 3.199 3.382C6.067 13.805 8.859 15 11.498 15c2.64 0 5.432-1.195 8.072-3.432 1.98-1.678 3.148-3.33 3.198-3.382.305-.38.305-.991.026-1.372zm-.838.788c-.05.05-4.62 6.381-10.458 6.381S1.091 7.653 1.015 7.576c-.025-.025-.025-.127 0-.152.076-.077 4.645-6.407 10.483-6.407 5.838 0 10.407 6.33 10.483 6.407 0 .025.026.127-.025.178zM11.498 3.05C8.985 3.05 6.93 5.059 6.93 7.5c0 2.44 2.056 4.45 4.57 4.45 2.512 0 4.568-2.01 4.568-4.45 0-2.44-2.056-4.45-4.569-4.45zm0 7.881c-1.954 0-3.553-1.55-3.553-3.432 0-1.881 1.599-3.432 3.553-3.432 1.955 0 3.554 1.55 3.554 3.432 0 1.881-1.6 3.432-3.554 3.432zm1.27-3.432c0 .712-.559 1.271-1.27 1.271-.71 0-1.269-.56-1.269-1.271 0-.712.559-1.271 1.27-1.271.71 0 1.268.56 1.268 1.271z" />
                                                                                                        </svg>
                                                                                                    </button>
                                                                                                </li>
                                                                                                <li className={this.state.viewConfiguration[data].isEnabled == "ENABLED" ? "viewConfigToggle addToggleSwitch mainToggle" : "viewConfigToggle mainToggle"}>

                                                                                                    <label className="switchToggle">
                                                                                                        <input type="checkbox" id={data} checked={this.state.viewConfiguration[data].isEnabled == "ENABLED"} onChange={(e) => this.enabledCheck(this.state.viewConfiguration[data].isEnabled, data)} />
                                                                                                        <span className="sliderToggle round">
                                                                                                        </span>
                                                                                                    </label>
                                                                                                    {/* <p className={this.state.viewConfiguration[data].isEnabled == "ENABLED" ? "onActive" : "onInActive"}>Enable</p>
                                                                                                    <p className={this.state.viewConfiguration[data].isEnabled != "ENABLED" ? "onActive" : "onInActive"}>Disable</p> */}
                                                                                                    <p className="onActive">Enable</p>
                                                                                                    <p className="onInActive">Disable</p>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </td>
                                                                                        <td className="curActive"><label>{data}</label></td>
                                                                                        <td className="curActive" ><label>{Object.keys(this.state.viewConfiguration[data]).slice(0,Object.keys(this.state.viewConfiguration[data]).length-1).join(',')} </label></td>
                                                                                    </tr>
                                                                                ) : null}
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div> : null}
                                                </div>
                                                {this.state.tabs == "view" ? <div className="pagerDiv">
                                                    <ul className="list-inline pagination">
                                                        {this.state.current == 1 || this.state.current == 0 ? <li >
                                                            <button className="PageFirstBtn pointerNone" type="button"  >
                                                                First
                  </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                                    First
                  </button>
                                                            </li>}
                                                        {this.state.prev != 0 ? <li >
                                                            <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                                Prev
                  </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn" type="button" disabled>
                                                                    Prev
                  </button>
                                                            </li>}
                                                        <li>
                                                            <button className="PageFirstBtn" type="button">
                                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                                            </button>
                                                        </li>
                                                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                            <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                                Next
                  </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn borderNone" type="button" disabled>
                                                                    Next
                  </button>
                                                            </li> : <li >
                                                                <button className="PageFirstBtn borderNone" type="button" disabled>
                                                                    Next
                  </button>
                                                            </li>}

                                                    </ul>
                                                </div> : null}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.state.configModal ? <ViewActionConfigModal viewConfigurationdata={this.state.viewConfigurationdata} deleteConfig={(e) => this.deleteConfig(e)} viewConfiguration={this.state.viewConfiguration} keysConfigData={this.state.keysConfigData} configModaldata={this.state.configModaldata} viewConfigurationModal={(e) => this.viewConfigurationModal(e)} /> : null}
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmModal ? <ConfigurationDeleteModal {...this.state} {...this.props} deleteConfigData={(e) => this.deleteConfigData(e)} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}
            </div>

        );
    }
}

export default CreateConfiguration;