import React, { Component } from 'react'
import ToastLoader from '../loaders/toastLoader';
import closeImg from '../../assets/close-recently.svg'
import searchImg from '../../assets/search.svg'
export default class FmcgFilterModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            motherComp: [],
            selectedValue: this.props.motherCompany,
            toastMsg: "",
            searchMotherComp: "",
            toastLoader: false,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: 1
        }
    }

    // ______________________________  RECEIVE PROPS ___________________________

    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.getMotherComp.isSuccess) {
            if (nextProps.replenishment.getMotherComp.data.resource != null) {
                this.setState({
                    motherComp: nextProps.replenishment.getMotherComp.data.resource,
                    prev: nextProps.replenishment.getMotherComp.data.prePage,
                    current: nextProps.replenishment.getMotherComp.data.currPage,
                    next: nextProps.replenishment.getMotherComp.data.currPage + 1,
                    maxPage: nextProps.replenishment.getMotherComp.data.maxPage,
                })
            } else {
                this.setState({
                    motherComp: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.replenishment.getMotherComp.data.prePage,
                current: this.props.replenishment.getMotherComp.data.currPage,
                next: this.props.replenishment.getMotherComp.data.currPage + 1,
                maxPage: this.props.replenishment.getMotherComp.data.maxPage,
            })
            if (this.props.replenishment.getMotherComp.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.getMotherComp.data.currPage - 1,
                    search: this.state.searchMotherComp
                }
                this.props.getMotherCompRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.replenishment.getMotherComp.data.prePage,
                current: this.props.replenishment.getMotherComp.data.currPage,
                next: this.props.replenishment.getMotherComp.data.currPage + 1,
                maxPage: this.props.replenishment.getMotherComp.data.maxPage,
            })
            if (this.props.replenishment.getMotherComp.data.currPage != this.props.replenishment.getMotherComp.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.getMotherComp.data.currPage + 1,
                    search: this.state.searchMotherComp
                }
                this.props.getMotherCompRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.replenishment.getMotherComp.data.prePage,
                current: this.props.replenishment.getMotherComp.data.currPage,
                next: this.props.replenishment.getMotherComp.data.currPage + 1,
                maxPage: this.props.replenishment.getMotherComp.data.maxPage,
            })
            if (this.props.replenishment.getMotherComp.data.currPage <= this.props.replenishment.getMotherComp.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.searchMotherComp
                }
                this.props.getMotherCompRequest(data)
            }
        }
    }

    selectedValue(value) {
        let motherComp = this.state.motherComp
        let selectedValue = [...this.state.selectedValue]
        for (let i = 0; i < motherComp.length; i++) {
            if (motherComp[i] == value) {
                if (selectedValue.includes(motherComp[i])) {
                    var index = selectedValue.indexOf(motherComp[i]);
                    if (index > -1) {
                        selectedValue.splice(index, 1)
                    }

                } else {
                    selectedValue.push(motherComp[i])
                }
            } else {

            }
        }
        this.setState({
            selectedValue: selectedValue
        })

    }

    handleChange(e) {
        if (e.target.id == "searchMotherComp") {
            this.setState({
                searchMotherComp: e.target.value
            })
        }
    }
    searchMotherComp() {
        if (this.state.searchMotherComp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
        } else {
            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.searchMotherComp
            }
            this.props.getMotherCompRequest(data)
        }
    }
    _handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.searchMotherComp()
        }
    }
    onClearSearch() {
        if (this.state.type == 3) {
            this.setState({
                searchMotherComp: "",
                type: 1,
                no: 1
            })
            var data = {
                type: 1,
                no: 1,
                search: "",
            }
            this.props.getMotherCompRequest(data)
        } else {
            this.setState({
                searchMotherComp: "",

            })
        }
    }

    motherCompSave(e) {
        if (this.state.selectedValue == "") {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            this.props.updateMotherComp(this.state.selectedValue)
        }
    }

    selectall(e) {
        let motherComp = this.state.motherComp
        let selectedValue = [...this.state.selectedValue]
        for (let i = 0; i < motherComp.length; i++) {
            if (!selectedValue.includes(motherComp[i])) {
                selectedValue.push(motherComp[i])
            }
        }
        this.setState({
            selectedValue: selectedValue,
        })
        document.getElementById("selectAll").checked = true
    }
    deselectall(e) {
        let selectedValue = [...this.state.selectedValue]
        this.setState({
            selectedValue: []
        })
        document.getElementById("deselectAll").checked = true
    }

    render() {

        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content modalShow pad-0">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle">
                                    <div className="col-md-6 pad-0">
                                        <h2>Mother Company</h2>
                                    </div>
                                    <div className="col-md-6 pad-0">
                                        <div className="modalHandlers">
                                            <button type="button" className="" onClick={(e) => this.motherCompSave(e)}>Done</button>
                                            <button type="button" onClick={(e) => this.props.closeMotherComp()} >Close</button>
                                        </div>
                                    </div>
                                </div>

                                <div className="modalSearch">
                                    <div className="col-md-6"></div>
                                    <div className="col-md-6">
                                        <div className="dropdown searchStoreProfileMain">
                                            {/* <form > */}
                                            <div className="searchBtnClick">
                                                <input type="search" className="search-box" onKeyDown={this._handleKeyDown} onChange={e => this.handleChange(e)} value={this.state.searchMotherComp} id="searchMotherComp" placeholder="Type to search" />
                                                <button className="m0" onClick={(e) => this.searchMotherComp(e)}><img src={searchImg} /></button>
                                                {this.state.searchMotherComp == "" ? null : <img src={closeImg} className="clearImg" onClick={(e) => this.onClearSearch(e)} />}
                                            </div>
                                            {/* {this.state.type == 3 ? <span onClick={(e) => this.onClearSearch(e)} className="clearSearchFilter">
                                                    Clear Search Filter
                                                </span> : null} */}
                                            {/* </form> */}
                                        </div>
                                    </div>
                                </div>
                                <div className="modalContentMid">
                                    <div className="col-md-12 col-sm-12 pad-0 modalTableNew">
                                        <div className="modal_table fmcgFilterTable tableHeadFixed">
                                            <table className="table tableModal">
                                                <thead>
                                                    <tr>
                                                        <th className="zIndex2"><label>Select</label></th>
                                                        <th className="zIndex2"><label>Mother Company</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.motherComp.length != 0 ? this.state.motherComp.map((modata, key) => (
                                                        <tr key={key}>
                                                            <td className="checkBoxSquare">
                                                                <div className="checkBoxRowClick"><label className="checkBoxLabel0 displayPointer"><input type="checkBox" id={modata} onChange={(e) => this.selectedValue(`${modata}`)} checked={this.state.selectedValue.includes(`${modata}`)} className="checkBoxTab" /> <span className="checkmark1"></span> </label></div>
                                                            </td>
                                                            <td className="fontWeig600">{modata}</td>
                                                        </tr>
                                                    )) : <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr>}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline m-top-10 modal-select">
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">SELECT ALL
                                            <input type="radio" name="colorRadio" id="selectAll" onClick={(e) => this.selectall(e)} />
                                                <span className="checkradio-select select_all"></span>
                                            </label>
                                        </li>
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">DESELECT ALL
                                            <input type="radio" name="colorRadio" id="deselectAll" onClick={(e) => this.deselectall(e)} />
                                                <span className="checkradio-select deselect_all"></span>
                                            </label>
                                        </li>
                                        <li className="float_right">

                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                                                    </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                                                    </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li>}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
            </div>
        )
    }
}
