import React from "react";
import Footer from '../footer';
import moment from 'moment';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import { CONFIG } from "../../config/index";
import axios from 'axios';
import exclaimIcon from "../../assets/info.svg";

class Summary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lodaer: true,
            stores: [],
            lastSummary: [],
            summary: [],
            scode: "",
            prev: "",
            current: 1,
            next: "",
            maxPage: "",
            link: "",
            csvLinkFile: "",
            code: "",
            errorCode: "",
            errorMessage: "",
            alert: false,
            toCode: "INPROGRESS",
            fileInfo: "Transfer Order Inprogress ...",
            csvDropDown : false,
            popup: false,
            dpLinks: []
        }
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }
    setWrapperRef(node){
        this.wrapperRef = node;
    }
    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == undefined || this.state.current == 1) {

            } else {

                this.setState({
                    prev: this.props.replenishment.summaryDetail.data.prePage,
                    current: this.props.replenishment.summaryDetail.data.currPage,
                    next: this.props.replenishment.summaryDetail.data.currPage + 1,
                    maxPage: this.props.replenishment.summaryDetail.data.maxPage,
                    loader: true
                })
                if (this.props.replenishment.summaryDetail.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.replenishment.summaryDetail.data.currPage - 1,
                        scode: this.state.scode
                    }
                    this.props.summaryDetailRequest(data)
                }
            }
        } else if (e.target.id == "next") {
            if (this.state.current == undefined) {

            } else {
                this.setState({
                    prev: this.props.replenishment.summaryDetail.data.prePage,
                    current: this.props.replenishment.summaryDetail.data.currPage,
                    next: this.props.replenishment.summaryDetail.data.currPage + 1,
                    maxPage: this.props.replenishment.summaryDetail.data.maxPage,
                    loader: true
                })
                if (this.props.replenishment.summaryDetail.data.currPage != this.props.replenishment.summaryDetail.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.replenishment.summaryDetail.data.currPage + 1,
                        scode: this.state.scode
                    }
                    this.props.summaryDetailRequest(data)
                }
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            } else {
                this.setState({
                    prev: this.props.replenishment.summaryDetail.data.prePage,
                    current: this.props.replenishment.summaryDetail.data.currPage,
                    next: this.props.replenishment.summaryDetail.data.currPage + 1,
                    maxPage: this.props.replenishment.summaryDetail.data.maxPage,
                    loader: true
                })
                if (this.props.replenishment.summaryDetail.data.currPage <= this.props.replenishment.summaryDetail.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        scode: this.state.scode
                    }
                    this.props.summaryDetailRequest(data)
                }
            }
        }
    }
    componentDidMount(){
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    componentWillMount() {
        var mode = "";
        if (process.env.NODE_ENV == "develop") {
            mode = "-DEV-JOB"
        } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality") {
            mode = "-PROD-JOB"
        }
        else {
            mode = "-DEV-JOB"
        }
        this.props.toStatusRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        this.props.allStoreCodeRequest();
        this.props.getCSVLinkRequest("NA");
        this.props.lastJobRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        let data = {
            type: 1,
            no: 1,
            scode: ""
        }
        this.props.summaryDetailRequest(data);
        this.statusIntervall = setInterval(() => {
            this.props.toStatusRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        }, 5000);
    }

    componentWillUnmount() {
        clearInterval(this.statusIntervall);
        document.removeEventListener("mousedown", this.handleClickOutside);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.toStatus.isSuccess) {

            this.setState({
                toCode: nextProps.replenishment.toStatus.data.resource == null ? "INPROGRESS" : nextProps.replenishment.toStatus.data.resource.code,
                fileInfo: nextProps.replenishment.toStatus.data.resource == null ? "No Data Found" : nextProps.replenishment.toStatus.data.resource.fileInfo
            })
            if (nextProps.replenishment.toStatus.data.resource != null) {
                if (nextProps.replenishment.toStatus.data.resource.code == "SUCCESS") {
                    clearInterval(this.statusIntervall);
                }
            }
        }
        if (nextProps.replenishment.getCSVLink.isSuccess) {
            if(nextProps.replenishment.getCSVLink.data.resource.isPopup){
                this.setState({
                    popup: nextProps.replenishment.getCSVLink.data.resource.isPopup,
                    csvLinkFile: "",
                    dpLinks: nextProps.replenishment.getCSVLink.data.resource.link
                })
            }else{
                this.setState({
                    popup: nextProps.replenishment.getCSVLink.data.resource.isPopup,
                    csvLinkFile: nextProps.replenishment.getCSVLink.data.resource.URL,
                    dpLinks: []
                })
            }
        }
        if (nextProps.replenishment.allStoreCode.isSuccess || (nextProps.replenishment.lastJob.isError || nextProps.replenishment.lastJob.isSuccess) || nextProps.replenishment.summaryDetail.isSuccess) {
            this.setState({
                loader: false,
                stores: nextProps.replenishment.allStoreCode.data.resource,
                lastSummary: nextProps.replenishment.lastJob.data.resource,
                summary: nextProps.replenishment.summaryDetail.data.resource,
                prev: nextProps.replenishment.summaryDetail.data.prePage,
                current: nextProps.replenishment.summaryDetail.data.currPage,
                next: nextProps.replenishment.summaryDetail.data.currPage + 1,
                maxPage: nextProps.replenishment.summaryDetail.data.maxPage,
            })

        } else if (nextProps.replenishment.allStoreCode.isLoading) {
            this.setState({
                // loader: true
            })
        }


        if (nextProps.replenishment.summaryCsv.isSuccess) {
            this.setState({
                link: nextProps.replenishment.summaryCsv.data.resource
            })
        } else if (nextProps.replenishment.summaryCsv.isError) {
            this.setState({
                link: "https://com-supplymint-glue.s3.ap-south-1.amazonaws.com/dev_etl/citylife/output/run-1543280737338-part-r-00000?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20181127T015514Z&X-Amz-SignedHeaders=host&X-Amz-Expires=359993&X-Amz-Credential=AKIAJ4VC37HSIUDIPMYA%2F20181127%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Signature=d9542230e3ecac07c3b00c61dc62bf8ffaf1567d7779a8124fa16d80231c5698"
            })
        } else {
            // if(nextProps.replenishment.lastJob.isSuccess){
            //     let date =  moment(nextProps.replenishment.lastJob.data.resource.startedOn).format("YYYY-MM-DD HH:mm");
            //     let date1 = date == "" ? "" : date.replace("-", "_");
            //     let date2 =   date1 == "" ? "" : date1.replace("-", "_");
            //     let csvLink = date2 == "" ? "" : date2.replace(" ", "_");
            //     this.props.summaryCsvRequest(csvLink);
            // }
        }
        if (nextProps.replenishment.summaryDetail.isSuccess) {
            this.setState({
                loader: false
            })
        } else if (nextProps.replenishment.summaryDetail.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.replenishment.summaryDetail.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.summaryDetail.message.status,
                errorCode: nextProps.replenishment.summaryDetail.message.error == undefined ? undefined : nextProps.replenishment.summaryDetail.message.error.errorCode,
                errorMessage: nextProps.replenishment.summaryDetail.message.error == undefined ? undefined : nextProps.replenishment.summaryDetail.message.error.errorMessage
            })
            this.props.summaryDetailRequest();
        }
    }

    onStoreCode(e) {
        this.setState({
            scode: e.target.value,
            type: 2,
            current: 1,
            loader: true
        })
        let data = {
            scode: e.target.value,
            no: 1,
            type: 2
        }
        this.props.summaryDetailRequest(data);
    }

    // downloadCsv(csvLink){

    // }

    onRemoveFilter(e) {
        this.setState({
            current: 1,
            type: 1,
            scode: "",
            loader: true
        })
        let data = {
            no: 1,
            type: 1,
            scode: ""
        }
        this.props.summaryDetailRequest(data);
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }

    // onCSVClick(e){
    //  e.preventDefault();
    //  let headers = {
    //     'X-Auth-Token': sessionStorage.getItem('token'),
    //     'Content-Type': 'application/json'
    // }
    // axios.get(`${CONFIG.BASE_URL}/aws/s3bucket/download/OUTPUT`, { headers: headers })
    //     .then(res => {
    //         console.log(res);

    //     }).catch((error) => {
    //         console.log(error);
    //     })   
    // }

    getTO() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}/aws/s3bucket/download/GENERATE_TRANSFER_ORDER`, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource.URL}`)
            }).catch((error) => {
                console.log(error);
            });
    }
    manageDrop(){
        this.setState({
            csvDropDown : !this.state.csvDropDown
        })
    }
    handleClickOutside(event) {
        var id = event.target.id
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            if(id == "csvDropMultiple"){
            }else{
                this.setState({
                    csvDropDown: false
                })
            }
        }
    }

    render() {
        // let csvLinkfile = this.state.csvLinkFile;
        // this.state.csvLinkFile.toString();
        let csvLinkfile = this.state.csvLinkFile;
        let date = this.state.lastSummary == undefined ? "" : moment(this.state.lastSummary.startedOn).format("YYYY-MM-DD HH:mm");
        let date1 = date == "" ? "" : date.replace("-", "_");
        let date2 = date1 == "" ? "" : date1.replace("-", "_");
        let csvLink = date2 == "" ? "" : date2.replace(" ", "_");
        // console.log(csvLinkfile, csvLink);

        // let csvLink = "2018_11_26_10:10"
        return (
            <div className="container-fluid">

                <div className="container-fluid">
                    <div className="container_div" id="">

                        {/* <div className="container-fluid"> */}
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                            <div className="replenishment_container">
                                <div className="col-md-12 col-sm-12 pad-0">
                                    <ul className="list_style">
                                        <li>
                                            <label className="contribution_mart">
                                                ENGINE RUN SUMMARY
                                            </label>
                                        </li>
                                        <li>
                                            <p className="master_para">Last engine run summary</p>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="col-md-7 col-sm-12 col-xs-12 pad-lft-0">
                                        <div className="runSummary summaryResp">
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                                <div className="col-md-12 col-xs-12 pad-0">
                                                    <div className="headSummary">
                                                        <h2>LAST JOB RUN SUMMARY</h2>

                                                        <ul className="list-inline jobSummaaryDetail">
                                                            <li>
                                                                <label>Job Id</label>
                                                                <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.jobId}</span>
                                                            </li>
                                                            <li>
                                                                <label>Start Time</label>
                                                                <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.startedOn}</span>
                                                            </li>
                                                            <li>
                                                                <label>End Time</label>
                                                                <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.completeOn}</span>
                                                            </li>
                                                            <li>
                                                                <label>Replenish Date</label>
                                                                <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.repDate}</span>
                                                            </li>
                                                            <li>
                                                                <label>Trigger Name</label>
                                                                <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.triggerName}</span>
                                                            </li>
                                                            <li>
                                                                <label>Job Status</label>
                                                                {this.state.lastSummary == undefined ? <span>NA</span>
                                                                    : <div className="statusDiv summaryStatus">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="263" height="33" viewBox="0 0 263 33">
                                                                            <g fill="none" fillRule="nonzero" transform="translate(-1)">
                                                                                <rect width="160" height="33" x="2" fill="#7ED321" rx="4" />
                                                                                <path fill="#FDFDFD" d="M162 25c2.21 0 1.5-4 1.5-8s.71-8-1.5-8a8 8 0 1 0 0 16zM3 25A8 8 0 1 0 3 9c-2.21 0-2 4-2 8s-.21 8 2 8z" />
                                                                            </g>
                                                                        </svg>
                                                                        <label>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.jobRunState}</label>
                                                                    </div>}
                                                                {/* <div className="statusDiv displaynone">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="263" height="33" viewBox="0 0 263 33">
                                                                        <g fill="none" fillRule="nonzero" transform="translate(-1)">
                                                                            <rect width="261" height="33" x="2" fill="#C44569" rx="4" />
                                                                            <path fill="#FDFDFD" d="M262 25c2.21 0 1.5-4 1.5-8s.71-8-1.5-8a8 8 0 1 0 0 16zM3 25A8 8 0 1 0 3 9c-2.21 0-2 4-2 8s-.21 8 2 8z" />
                                                                        </g>
                                                                    </svg>

                                                                    <label>UNSUCCESSFUL</label>
                                                                </div> */}
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-5 col-sm-12 col-xs-12 pad-0">
                                        <div className="runSummary blankDiv">
                                        </div>
                                    </div>

                                </div>
                                <div className="col-md-12 col-sm-12 pad-0 m-top-35 summaryBtns">
                                    <div className="col-md-8 col-sm-6 col-xs-6 pad-0">
                                        <ul className="list_style">
                                            <li>
                                                <label className="contribution_mart">
                                                    SUMMARY IN DETAIL
                                                </label>


                                            </li>
                                            <li>
                                                <div className="downloadSummaryButton m-top-10">

                                                    {this.state.toCode == "SUCCESS" ? <button type="button" onClick={(e) => this.getTO(e)}>Download Transfer Order</button> :
                                                        <button type="button" className="btnDisabled" >Download Transfer Order</button>}

                                                    <img src={exclaimIcon} />
                                                    <p className="master_para">{this.state.fileInfo}</p>

                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                    <div className="col-md-4 col-sm-6 col-xs-6 pad-0">
                                        <div className="summaryExportData">
                                            <h2>EXPORT </h2>
                                            <ul className={csvLinkfile == null ? "list-inline jobSummaaryDetail1" : csvLinkfile == "" ? "list-inline jobSummaaryDetail1" : "list-inline jobSummaaryDetail1 csvBtn csvSummary"}>
                                                <li>
                                                    {/* <a href={`${CONFIG.BASE_URL}/core/aws/s3bucket/downloads?key=${csvLink}`} download>
                                                        <button type="button" >XLS</button>
                                                    </a> */}
                                                    <a href={csvLinkfile == null ? null : csvLinkfile == "" ? null : csvLinkfile} download>
                                                        {!this.state.popup ? <button className={csvLinkfile == null ? "btnDisabled pointerNone" : csvLinkfile == "" ? "btnDisabled pointerNone" : "displayPointer"} type="button">CSV</button>
                                                         : <button type="button" id="csvDropMultiple" onClick={() => this.manageDrop()}>CSV</button>}
                                                        {this.state.csvDropDown ? <div className="csvDropDown assortmentSelect"  ref={(e) => this.setWrapperRef(e)}>
                                                            <div className="assortSelectDiv" >
                                                                <div className="dropDownItems m-top-5">
                                                                <h4>Available Downloads</h4>
                                                                    <ul className="pad-0 m-top-15">
                                                                        {this.state.dpLinks.length == 0 ? null : this.state.dpLinks.map((data, key) => (<li key={key} onClick={(e) => window.open(data.value)}><span>{data.key}</span> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path fill="#000" fill-rule="nonzero" d="M14 10.112V13.3a.7.7 0 0 1-.7.7H.7a.7.7 0 0 1-.7-.7v-3.188a.7.7 0 0 1 1.4 0V12.6h11.2v-2.488a.7.7 0 0 1 1.4 0zM7 0a.7.7 0 0 0-.7.7v7.021L3.964 5.385a.7.7 0 1 0-.99.99l3.531 3.531a.7.7 0 0 0 .99 0l3.53-3.53a.7.7 0 1 0-.99-.99L7.7 7.72V.7A.7.7 0 0 0 7 0z"/></svg></li>))}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>:null}
                                                    </a>
                                                    {(csvLinkfile == null || csvLinkfile == "") && (this.state.dpLinks.length == 0)  ? <span style={{ display: "block" }} >No attachment found</span> : null}
                                                </li>

                                            </ul>
                                        </div>
                                        <div className="filterSummary">

                                            <label>FILTER BY STORE
                                            {this.state.scode == "" ? null : <button type="button" onClick={(e) => this.onRemoveFilter(e)} className="summaryFilter">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="13" viewBox="0 0 12 13">
                                                        <g fill="#FFF" fillRule="evenodd">
                                                            <path fillRule="nonzero" d="M0 4.333c0 .599.488 1.084 1.09 1.084v5.416C1.09 12.03 2.069 13 3.274 13h5.454c1.205 0 2.182-.97 2.182-2.167V5.417c.603 0 1.091-.485 1.091-1.084V3.25c0-.598-.488-1.083-1.09-1.083H8.726V1.083C8.727.485 8.24 0 7.637 0H4.363C3.76 0 3.273.485 3.273 1.083v1.084H1.09C.488 2.167 0 2.652 0 3.25v1.083zm9.818 6.5c0 .599-.488 1.084-1.09 1.084H3.272c-.603 0-1.091-.485-1.091-1.084V5.417h7.636v5.416zm-5.454-9.75h3.272v1.084H4.364V1.083zM1.09 3.25h9.818v1.083H1.091V3.25z" />
                                                            <path d="M3.818 10.833a.544.544 0 0 0 .546-.541v-3.25c0-.3-.245-.542-.546-.542a.544.544 0 0 0-.545.542v3.25c0 .299.244.541.545.541zM6 10.833a.544.544 0 0 0 .545-.541v-3.25c0-.3-.244-.542-.545-.542a.544.544 0 0 0-.545.542v3.25c0 .299.244.541.545.541zM8.182 10.833a.544.544 0 0 0 .545-.541v-3.25c0-.3-.244-.542-.545-.542a.544.544 0 0 0-.546.542v3.25c0 .299.245.541.546.541z" />
                                                        </g>
                                                    </svg>
                                                    Remove Filter
                                                    </button>}
                                            </label>

                                            <select value={this.state.scode} onChange={(e) => this.onStoreCode(e)} className="summaryfilterSelect displayPointer">
                                                <option value="">Select Store Code</option>
                                                {this.state.stores == undefined ? null : this.state.stores.map((data, key) => (<option key={key} value={data}>{data}</option>))}
                                            </select>
                                        </div>
                                    </div>


                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 m-top-20 pad-0 bordere3e7f3 tableGeneric">
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <div className="zui-wrapper">
                                            <div className="scrollableTableFixed table-scroll zui-scroller1" id="table-scroll">
                                                <table className="table scrollTable main-table zui-table scrollableSummary tableOddColor">

                                                    <thead>
                                                        <tr>
                                                            <th className="fixed-side alignMiddle">
                                                                <ul className="list-inline">
                                                                    {/* <li>
                                                                            <p className="para_purchase">
                                                                                <input type="text" checked={this.state.descriptionChecked ? true : false} onClick={(e) => this.onAllDescription(e)} className="purchaseCheck" type="checkbox" id="c8" name="cb" />
                                                                                <label htmlFor="c1" className="purchaseLabel"></label>
                                                                            </p>
                                                                        </li> */}
                                                                    <li className="width70">
                                                                        <label className="width-45 lableFixed">
                                                                            Order Status
                                                                            </label>
                                                                    </li>
                                                                </ul>

                                                            </th>

                                                            <th className="positionRelative">
                                                                <label>
                                                                    Unique Code
                                    </label>
                                                            </th>
                                                            <th className="positionRelative">
                                                                <label>
                                                                    Wh Code
                                    </label>
                                                            </th>
                                                            <th className="positionRelative">
                                                                <label>
                                                                    Store Code
                                                                    </label>
                                                            </th>
                                                            <th className="positionRelative">
                                                                <label>
                                                                    Item Code
                                                                    </label>
                                                            </th>
                                                            <th className="positionRelative">
                                                                <label>
                                                                    Available Qty
                                                                    </label>
                                                            </th>
                                                            <th className="positionRelative">
                                                                <label>
                                                                    Transfer Order Qty
                                                                </label>
                                                            </th>
                                                            {/* <th className="positionRelative">
                                                                    <label>
                                                                        REPLENISHMENT DATE
                                                                    </label>
                                                                </th> */}

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.state.summary == null ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.summary == "" ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.summary == undefined ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.summary.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.summary.map((data, key) => (
                                                            <tr key={key}>
                                                                <td className="fixed-side alignMiddle">
                                                                    <ul className="list-inline">
                                                                        {/* <li>
                                                                            <p className="para_purchase">
                                                                                <input type="text" checked={this.state.isDescriptionChecked ? true : false} onClick={(e) => this.onDescription(e)} className="purchaseCheck" type="checkbox" id="c2" name="cb" />
                                                                                <label htmlFor="c2" className="purchaseLabel"></label>
                                                                            </p>
                                                                        </li> */}
                                                                        <li className="text-center width70">

                                                                            <svg className={data == null ? "width30" : data.transferOrder > 0 ? "width30 colorCart" : "width30"} xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27">
                                                                                <g fill="#d2d2d2" fillRule="nonzero">
                                                                                    <path d="M16.288 16.549a5.866 5.866 0 0 1-.918-1.462H8.09v1.462h8.198zm-6.39 6.664c.585 0 1.059.464 1.059 1.038 0 .573-.474 1.038-1.06 1.038a1.048 1.048 0 0 1-1.058-1.038c0-.573.475-1.038 1.059-1.038zm6.257 1.038c0-.574.474-1.038 1.058-1.038.586 0 1.058.464 1.058 1.038 0 .576-.47 1.038-1.058 1.038a1.048 1.048 0 0 1-1.058-1.038zM5.57 13.483l9.358.005a5.803 5.803 0 0 1 0-1.456l-9.756-.005-.951-3.483H0v1.461h3.08l3.455 12.651h1.4a2.461 2.461 0 0 0-.586 1.595c0 1.38 1.14 2.5 2.549 2.5 1.408 0 2.55-1.12 2.55-2.5 0-.606-.22-1.162-.587-1.595h3.39a2.461 2.461 0 0 0-.586 1.595c0 1.377 1.144 2.5 2.549 2.5 1.406 0 2.549-1.122 2.549-2.5 0-.606-.22-1.162-.587-1.595h1.4l1.108-4.056a6.115 6.115 0 0 1-1.542.006l-.707 2.588H7.677L5.57 13.483zm12.944 4.659H8.657v1.462h9.857v-1.462zM32.526 6.697l-.359-.244a1.102 1.102 0 0 1-.434-1.22l.113-.413c.19-.639-.245-1.296-.906-1.409l-.434-.075a1.1 1.1 0 0 1-.925-.901l-.076-.432a1.118 1.118 0 0 0-1.416-.882l-.416.13c-.453.132-.963-.037-1.227-.412l-.264-.376A1.118 1.118 0 0 0 24.52.294l-.321.3c-.359.32-.869.395-1.303.151l-.378-.207a1.138 1.138 0 0 0-1.586.564l-.17.413c-.17.45-.623.732-1.095.695l-.434-.02a1.102 1.102 0 0 0-1.171 1.184l.038.432c.037.47-.246.92-.68 1.108l-.397.169a1.12 1.12 0 0 0-.547 1.577l.226.375a1.1 1.1 0 0 1-.132 1.296l-.283.32a1.092 1.092 0 0 0 .208 1.652l.358.244c.397.263.567.77.435 1.22l-.17.432c-.19.639.245 1.296.906 1.408l.434.076a1.1 1.1 0 0 1 .926.9l.075.433c.114.657.774 1.07 1.416.882l.416-.131c.453-.132.963.037 1.227.413l.265.357c.396.544 1.17.62 1.661.169l.321-.3c.36-.32.869-.395 1.303-.151l.378.207c.585.319 1.322.056 1.586-.564l.17-.413c.17-.45.623-.732 1.095-.695l.435.02a1.102 1.102 0 0 0 1.17-1.184l-.019-.432c-.037-.47.246-.92.68-1.108l.397-.169a1.12 1.12 0 0 0 .547-1.577l-.226-.375a1.1 1.1 0 0 1 .132-1.296l.283-.32a1.089 1.089 0 0 0-.17-1.652zm-7.34 3.625L23.558 11.9l-1.609-1.598L20.4 8.763l1.629-1.578 1.549 1.54L27.29 5.1 28.9 6.698l-3.714 3.624z" />
                                                                                </g>
                                                                            </svg>

                                                                        </li>
                                                                    </ul>
                                                                </td>

                                                                <td>
                                                                    <label>{data == null ? "" : data.uniqueCode}</label>
                                                                </td>
                                                                <td>  <label>{data == null ? "" : data.whCode}</label></td>
                                                                <td>
                                                                    <label>{data == null ? "" : data.storeCode}</label>
                                                                </td>

                                                                <td>
                                                                    <label>{data == null ? "" : data.itemCode}</label>
                                                                </td>
                                                                <td>
                                                                    <label>{data == null ? "" : data.availableStock}</label>
                                                                </td>
                                                                <td>
                                                                    <label>{data == null ? "" : data.transferOrder}</label>
                                                                </td>
                                                                {/* <td>
                                                                    <label>ITM0083637</label>
                                                                </td> */}
                                                            </tr>))}
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="pagerDiv">
                                    <ul className="list-inline pagination">
                                        <li >
                                            <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                                First
                  </button>
                                        </li>

                                        <li>
                                            <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                                Prev
                  </button>
                                        </li>

                                        <li>
                                            <button className="PageFirstBtn pointerNone">
                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                            </button>
                                        </li>
                                        {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                            <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                Next
                  </button>
                                        </li> : <li >
                                                <button className="PageFirstBtn borderNone" disabled>
                                                    Next
                  </button>
                                            </li>}


                                        {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                {/* </div > */}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                <Footer />
            </div>
        );

    }

}

export default Summary;
