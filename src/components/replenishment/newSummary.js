import React from "react";
import moment from 'moment';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import { CONFIG } from "../../config/index";
import axios from 'axios';
import exclaimIcon from "../../assets/info.svg";
import successIcon from '../../assets/success-summary.svg';
import failedSummary from '../../assets/summary-failed.svg';
import csvIcon from '../../assets/csv-icon.svg';
import ColoumSetting from "./coloumSetting";
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from "constants";
import ConfirmationSummaryModal from "./confirmationReset";
import RequestSuccess from "../loaders/requestSuccess";
import Pagination from '../pagination';

class NewSummary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allocationTable: [],
            requirementTable: [],
            tableHeaders: {},
            type: 1,
            totalRecord: 0,
            currPage: 0,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            sortedBy: "",
            sortedIn: "DESC",
            startTime: 'NA',
            endTime: 'NA',
            repDate: 'NA',
            triggerName: 'NA',
            summaryInfo: '', 
            summaryStatus: '',
            jobListSummary:[],
            activeJobIdSummary:'',
            activeJobNameSummary:'',
            lodaer: true,
            successMessage: "",
            success: false,
            stores: [],
            lastSummary: [],
            summary: [],
            scode: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            link: "",
            csvLinkFile: "",
            code: "",
            errorCode: "",
            errorMessage: "",
            alert: false,
            toCode: "INPROGRESS",
            fileInfo: "Transfer Order Inprogress ...",
            csvDropDown: false,
            popup: false,
            dpLinks: [],
            tableTab: "forecast",
            downloadType: "csv",
            getHeaderConfig: [],
            fixedHeader: [],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],
            customHeadersState: [],
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            headerCondition: false,
            tableCustomHeader: [],
            tableGetHeaderConfig: [],
            saveState: [],
            getJobs: [],
            selectedJob: "",
            exportToExcel: false,
            currentTab: "requirement"
        }
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () => {
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }
    setWrapperRef(node) {
        this.wrapperRef = node;
    }
    // page(e) {
    //     if (e.target.id == "prev") {
    //         if (this.state.current == undefined || this.state.current == 1) {
    //         } else {
    //             this.setState({
    //                 prev: this.props.replenishment.summaryDetail.data.prePage,
    //                 current: this.props.replenishment.summaryDetail.data.currPage,
    //                 next: this.props.replenishment.summaryDetail.data.currPage + 1,
    //                 maxPage: this.props.replenishment.summaryDetail.data.maxPage,
    //                 loader: true
    //             })
    //             if (this.props.replenishment.summaryDetail.data.currPage != 0) {
    //                 let data = {
    //                     type: this.state.type,
    //                     no: this.props.replenishment.summaryDetail.data.currPage - 1,
    //                     scode: this.state.scode,
    //                     displayName: this.state.tableTab == "requirement" ? "Requirement Summary" : "Allocation Summary",
    //                     jobName: this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob
    //                 }
    //                // this.props.summaryDetailRequest(data)
    //             }
    //         }
    //     } else if (e.target.id == "next") {
    //         if (this.state.current == undefined) {
    //         } else {
    //             this.setState({
    //                 prev: this.props.replenishment.summaryDetail.data.prePage,
    //                 current: this.props.replenishment.summaryDetail.data.currPage,
    //                 next: this.props.replenishment.summaryDetail.data.currPage + 1,
    //                 maxPage: this.props.replenishment.summaryDetail.data.maxPage,
    //                 loader: true
    //             })
    //             if (this.props.replenishment.summaryDetail.data.currPage != this.props.replenishment.summaryDetail.data.maxPage) {
    //                 let data = {
    //                     type: this.state.type,
    //                     no: this.props.replenishment.summaryDetail.data.currPage + 1,
    //                     scode: this.state.scode,
    //                     displayName: this.state.tableTab == "requirement" ? "Requirement Summary" : "Allocation Summary",
    //                     jobName: this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob
    //                 }
    //                // this.props.summaryDetailRequest(data)
    //             }
    //         }
    //     }
    //     else if (e.target.id == "first") {
    //         if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
    //         } else {
    //             this.setState({
    //                 prev: this.props.replenishment.summaryDetail.data.prePage,
    //                 current: this.props.replenishment.summaryDetail.data.currPage,
    //                 next: this.props.replenishment.summaryDetail.data.currPage + 1,
    //                 maxPage: this.props.replenishment.summaryDetail.data.maxPage,
    //                 loader: true
    //             })
    //             if (this.props.replenishment.summaryDetail.data.currPage <= this.props.replenishment.summaryDetail.data.maxPage) {
    //                 let data = {
    //                     type: this.state.type,
    //                     no: 1,
    //                     scode: this.state.scode,
    //                     displayName: this.state.tableTab == "requirement" ? "Requirement Summary" : "Allocation Summary",
    //                     jobName: this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob
    //                 }
    //               //  this.props.summaryDetailRequest(data)
    //             }
    //         }
    //     }
    // }
    componentDidMount() {
        this.props.getHeaderConfigRequest({
            enterpriseName: "TURNINGCLOUD",
            attributeType: "TABLE HEADER",
            displayName: "REPLENISHMENT_REQUIREMENT_SUMMARY",
        });
        this.props.getAllJobRequest('data')
        document.addEventListener("mousedown", this.handleClickOutside);
    }
    componentWillMount() {
        // var mode = "";
        // if (process.env.NODE_ENV == "develop") {
        //     mode = "-DEV-JOB"
        // } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality") {
        //     mode = "-PROD-JOB"
        // }
        // else {
        //     mode = "-DEV-JOB"
        // }
        // this.props.toStatusRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);

        // this.props.getCSVLinkRequest();
        // this.props.lastJobRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        // // _____________________requirement summary _____________________________
        // this.props.getCSVLinkRequest("NA");
        // let payload = {
        //     enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
        //     attributeType: "table header",
        //     displayName: this.state.tableTab == "requirement" ? "Requirement Summary" : "Allocation Summary"
        // }
        

        // this.statusIntervall = setInterval(() => {
        //     this.props.toStatusRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        // }, 5000);
    }
    componentWillUnmount() {
        clearInterval(this.statusIntervall);
        document.removeEventListener("mousedown", this.handleClickOutside);
    }
    componentWillReceiveProps(nextProps) {
        
        if (nextProps.replenishment.getCSVLink.isSuccess) {
            if (nextProps.replenishment.getCSVLink.data.resource.isPopup) {
                this.setState({
                    popup: nextProps.replenishment.getCSVLink.data.resource.isPopup,
                    csvLinkFile: "",
                    dpLinks: nextProps.replenishment.getCSVLink.data.resource.link
                })
            } else {
                this.setState({
                    popup: nextProps.replenishment.getCSVLink.data.resource.isPopup,
                    csvLinkFile: nextProps.replenishment.getCSVLink.data.resource.URL,
                    dpLinks: []
                })
            }
            this.props.getCSVLinkClear()
        } else if (nextProps.replenishment.getCSVLink.isError) {
        
                   this.props.getCSVLinkClear()
        }
        if (nextProps.replenishment.allStoreCode.isSuccess) {
            this.setState({
                loader: false,
                stores: nextProps.replenishment.allStoreCode.data.resource,
            })

        }
        if (nextProps.replenishment.lastJob.isSuccess) {
            this.setState({
                loader: false,
                lastSummary: nextProps.replenishment.lastJob.data.resource,
            })
        } else if (nextProps.replenishment.lastJob.isError) {
            this.setState({
                loader: false,
                // alert: true,
                // code: nextProps.replenishment.lastJob.message.status,
                // errorCode: nextProps.replenishment.lastJob.message.error == undefined ? undefined : nextProps.replenishment.lastJob.message.error.errorCode,
                // errorMessage: nextProps.replenishment.summalastJobryDetail.message.error == undefined ? undefined : nextProps.replenishment.lastJob.message.error.errorMessage
            })
        }
        if (nextProps.replenishment.summaryCsv.isSuccess) {
            this.setState({
                link: nextProps.replenishment.summaryCsv.data.resource
            })
        } else if (nextProps.replenishment.summaryCsv.isError) {
            this.setState({
                link: "https://com-supplymint-glue.s3.ap-south-1.amazonaws.com/dev_etl/citylife/output/run-1543280737338-part-r-00000?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20181127T015514Z&X-Amz-SignedHeaders=host&X-Amz-Expires=359993&X-Amz-Credential=AKIAJ4VC37HSIUDIPMYA%2F20181127%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Signature=d9542230e3ecac07c3b00c61dc62bf8ffaf1567d7779a8124fa16d80231c5698"
            })
        } else {
            // if(nextProps.replenishment.lastJob.isSuccess){
            //     let date =  moment(nextProps.replenishment.lastJob.data.resource.startedOn).format("YYYY-MM-DD HH:mm");
            //     let date1 = date == "" ? "" : date.replace("-", "_");
            //     let date2 =   date1 == "" ? "" : date1.replace("-", "_");
            //     let csvLink = date2 == "" ? "" : date2.replace(" ", "_");
            //     this.props.summaryCsvRequest(csvLink);
            // }
        }
        if (nextProps.replenishment.summaryDetail.isSuccess) {
            if (nextProps.replenishment.summaryDetail.data.resource != null) {
                this.setState({
                    loader: false,
                    summary: nextProps.replenishment.summaryDetail.data.resource,
                    prev: nextProps.replenishment.summaryDetail.data.prePage,
                    current: nextProps.replenishment.summaryDetail.data.currPage,
                    next: nextProps.replenishment.summaryDetail.data.currPage + 1,
                    maxPage: nextProps.replenishment.summaryDetail.data.maxPage,
                })
            } else {
                this.setState({
                    loader: false,
                    summary: [],
                    prev: "",
                    current: "",
                    next: "",
                    maxPage: "",
                })

            }
        } else if (nextProps.replenishment.summaryDetail.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.summaryDetail.message.status,
                errorCode: nextProps.replenishment.summaryDetail.message.error == undefined ? undefined : nextProps.replenishment.summaryDetail.message.error.errorCode,
                errorMessage: nextProps.replenishment.summaryDetail.message.error == undefined ? undefined : nextProps.replenishment.summaryDetail.message.error.errorMessage
            })
           // this.props.summaryDetailRequest();
        }
        
        
        // if (nextProps.replenishment.getAllJob.isSuccess) {
        //     let jobName = [];
        //     Object.keys(nextProps.replenishment.getAllJob.data.resource).map( key=>{
        //         jobName.push(key);
        //     })
        //     this.setState({
        //         loader: false,
        //         getJobs: jobName == null ? [] : jobName
        //     })
        //     this.props.toStatusRequest(jobName[0]);

        //     this.props.getCSVLinkRequest();
        //     this.props.lastJobRequest(nextProps.replenishment.getAllJob.data.resource[0]);
        //     // let data = {
        //     //     jobName: nextProps.replenishment.getAllJob.data.resource[0]
        //     // }
        //     // console.log("allStoreCodeRequest", data)
        //     // this.props.allStoreCodeRequest(data);
        //     // _____________________requirement summary _____________________________
        //     let payload = {
        //         enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
        //         attributeType: "table header",
        //         displayName: this.state.tableTab == "requirement" ? "Requirement Summary" : "Allocation Summary"
        //     }
        //     this.props.getHeaderConfigRequest(payload)
        //     this.statusIntervall = setInterval(() => {
        //         this.props.toStatusRequest(this.state.selectedJob == "" ? jobName[0] : this.state.selectedJob);
        //     }, 5000);

        //     this.props.getAllJobClear()
        // } else if (nextProps.replenishment.getAllJob.isError) {
        //     this.setState({
        //         loader: false
        //     })
        // }
        if(nextProps.replenishment.getAllocation.isSuccess){
            if(nextProps.replenishment.getAllocation.data.resource != null){
                this.setState({
                    loader: false,
                    current: nextProps.replenishment.getAllocation.data.currPage,
                    currPage: nextProps.replenishment.getAllocation.data.currPage,
                    maxPage: nextProps.replenishment.getAllocation.data.maxPage,
                    prev: nextProps.replenishment.getAllocation.data.previousPage,
                    next: nextProps.replenishment.getAllocation.data.maxPage > nextProps.replenishment.getAllocation.data.currPage ? nextProps.replenishment.getAllocation.data.currPage +1 : nextProps.replenishment.getAllocation.data.currPage,
                    allocationTable: nextProps.replenishment.getAllocation.data.resource,
                })
            } else if(nextProps.replenishment.getAllocation.data.resource == null){
                this.setState({
                    loader: false, 
                    totalRecord: 0,
                    current: 0,
                    currPage: 0,
                    maxPage: 0,               
                    prev: 0,
                    next: 0,
                })
            }
            this.props.getAllocationClear();
        } else if(nextProps.replenishment.getAllocation.isError){
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getAllocation.message.status,
                errorCode: nextProps.replenishment.getAllocation.message.error == undefined ? undefined : nextProps.replenishment.getAllocation.message.error.errorCode,
                errorMessage: nextProps.replenishment.getAllocation.message.error == undefined ? undefined : nextProps.replenishment.getAllocation.message.error.errorMessage
            })
        }

        if(nextProps.replenishment.getRequirementSummary.isSuccess){
            if(nextProps.replenishment.getRequirementSummary.data.resource != null){
                this.setState({
                    loader: false,
                    current: nextProps.replenishment.getRequirementSummary.data.resource.currPage,
                    currPage: nextProps.replenishment.getRequirementSummary.data.resource.currPage,
                    maxPage: nextProps.replenishment.getRequirementSummary.data.resource.maxPage,
                    prev: nextProps.replenishment.getRequirementSummary.data.resource.prePage,
                    next: nextProps.replenishment.getRequirementSummary.data.resource.maxPage > nextProps.replenishment.getRequirementSummary.data.resource.currPage ? nextProps.replenishment.getRequirementSummary.data.resource.currPage +1 : nextProps.replenishment.getRequirementSummary.data.currPage,
                    requirementTable: nextProps.replenishment.getRequirementSummary.data.resource.response === null ? [] : nextProps.replenishment.getRequirementSummary.data.resource.response,
                })
            } else if(nextProps.replenishment.getRequirementSummary.data.resource == null){
                this.setState({
                    loader: false, 
                    totalRecord: 0,
                    current: 0,
                    currPage: 0,
                    maxPage: 0,               
                    prev: 0,
                    next: 0,
                })
            }
            this.props.getRequirementSummaryClear();
        } else if(nextProps.replenishment.getRequirementSummary.isError){
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getRequirementSummary.message.status,
                errorCode: nextProps.replenishment.getRequirementSummary.message.error == undefined ? undefined : nextProps.replenishment.getRequirementSummary.message.error.errorCode,
                errorMessage: nextProps.replenishment.getRequirementSummary.message.error == undefined ? undefined : nextProps.replenishment.getRequirementSummary.message.error.errorMessage
            })
            this.props.getRequirementSummaryClear();
        }

        if(nextProps.replenishment.getAllJob.isSuccess){
            if(nextProps.replenishment.getAllJob.data.resource != null && nextProps.replenishment.getAllJob.data.resource.length != 0){                
                this.setState({
                    loader: false,
                    jobListSummary: nextProps.replenishment.getAllJob.data.resource,
                    activeJobIdSummary: nextProps.replenishment.getAllJob.data.resource[0].id,
                    activeJobNameSummary: nextProps.replenishment.getAllJob.data.resource[0].jobName,
                })
                // this.props.toStatusRequest(nextProps.replenishment.getAllJob.data.resource[0].id)
                this.props.getRequirementSummaryRequest({pageNo: 1, type: 1, manageRuleId: nextProps.replenishment.getAllJob.data.resource[0].id});
                this.props.lastJobRequest(nextProps.replenishment.getAllJob.data.resource[0].id);
            }
            this.props.getAllJobClear()
        }
        if(nextProps.replenishment.toStatus.isSuccess){
            if(nextProps.replenishment.toStatus.data.resource != null){
                this.setState({
                    loader: false,
                    summaryStatus: nextProps.replenishment.toStatus.data.resource.code,
                    summaryInfo: nextProps.replenishment.toStatus.data.resource.fileInfo,
                })
            } else{
                this.setState({
                    loader: false,
                    summaryStatus: 'NA',
                    summaryInfo: 'No data found',
                })
            }
        }
        if(nextProps.replenishment.lastJob.isSuccess){
            if(nextProps.replenishment.lastJob.data.resource != null){
                this.setState({
                    loader: false,
                    startTime: nextProps.replenishment.lastJob.data.resource.startedOn,
                    endTime: nextProps.replenishment.lastJob.data.resource.endTime != null ? nextProps.replenishment.lastJob.data.resource.endTime : 'NA',
                    repDate: nextProps.replenishment.lastJob.data.resource.repDate,
                    triggerName: nextProps.replenishment.lastJob.data.resource.triggerName,
                })
            } else{
                this.setState({
                    loader: false,
                    startTime: 'NA',
                    endTime: 'NA',
                    repDate: 'NA',
                    triggerName: 'NA',
                })
            }
        }

        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            this.setState({
                loading: false,
                error: false,
                success: false,
                tableHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]
            });
            this.props.getHeaderConfigClear();
        }
        else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage,
                tableHeaders: {}
            });
        }

        if (nextProps.replenishment.downloadRepReport.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.replenishment.downloadRepReport.data.message,
                alert: false
            });
            window.location.href = nextProps.replenishment.downloadRepReport.data.resource;
            this.props.downloadRepReportClear();
        }
        else if (nextProps.replenishment.downloadRepReport.isError) {
            this.setState({
                loading: false,
                success: false,
                alert: true,
                code: nextProps.replenishment.downloadRepReport.message.status,
                errorCode: nextProps.replenishment.downloadRepReport.message.error == undefined ? undefined : nextProps.replenishment.downloadRepReport.message.error.errorCode,
                errorMessage: nextProps.replenishment.downloadRepReport.message.error == undefined ? undefined : nextProps.replenishment.downloadRepReport.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.downloadRepReportClear();
        }

        if (nextProps.replenishment.getHeaderConfig.isLoading || nextProps.replenishment.allStoreCode.isLoading || nextProps.replenishment.summaryDetail.isLoading || nextProps.replenishment.toStatus.isLoading || nextProps.replenishment.lastJob.isLoading || nextProps.replenishment.getAllocation.isLoading
            || nextProps.replenishment.createHeaderConfig.isLoading || nextProps.replenishment.getAllJob.isLoading || nextProps.replenishment.getRequirementSummary.isLoading || nextProps.replenishment.downloadRepReport.isLoading) {
            this.setState({
                loader: true
            })
        }
    }


    //----------------------------- Showing job list----------------------------------
    openRunningJobs = () =>{
        this.setState(prevState => ({
            runningJobs: !prevState.runningJobs,
        }))
    }

    setJobHandler = (job) =>{
        this.setState({
            activeJobIdSummary: job.id,
            activeJobNameSummary: job.jobName,
            runningJobs: false,
        })
        this.props.toStatusRequest(job.id)
        this.props.lastJobRequest(job.id)
    }

    //.......................allocation table summary............................

    pageHandler = (e) =>{
        this.setState({
            currPage: e.target.value
        })        
    }

    jumpToPage = (e) =>{
        if(e.key === 'Enter' && e.target.value <= this.state.maxPage){
            if (this.state.currentTab == "allocation") {
                let payload = {
                    pageNo:  e.target.value,
                    type: this.state.type,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getAllocationRequest(payload)
            }
            else {
                let payload = {
                    pageNo:  e.target.value,
                    type: 1,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getRequirementSummaryRequest(payload)
            }
        }
    }

    page = (e) => {
        if (this.state.currentTab == "allocation") {
            if(e.target.id == 'next'){
                let payload = {
                    pageNo:  this.state.next,
                    type: this.state.type,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getAllocationRequest(payload)
            } else if(e.target.id == 'prev'){
                let payload = {
                    pageNo:  this.state.prev,
                    type: this.state.type,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getAllocationRequest(payload)
            } else if(e.target.id == 'first'){
                let payload = {
                    pageNo:  1,
                    type: this.state.type,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getAllocationRequest(payload)
            } else if(e.target.id == 'last'){
                let payload = {
                    pageNo:  this.state.maxPage,
                    type: this.state.type,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getAllocationRequest(payload)
            }
        }
        else {
            if(e.target.id == 'next'){
                let payload = {
                    pageNo:  this.state.next,
                    type: 1,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getRequirementSummaryRequest(payload)
            } else if(e.target.id == 'prev'){
                let payload = {
                    pageNo:  this.state.prev,
                    type: 1,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getRequirementSummaryRequest(payload)
            } else if(e.target.id == 'first'){
                let payload = {
                    pageNo:  1,
                    type: 1,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getRequirementSummaryRequest(payload)
            } else if(e.target.id == 'last'){
                let payload = {
                    pageNo:  this.state.maxPage,
                    type: 1,
                    sortedBy: this.state.sortedBy,
                    sortedIn: this.state.sortedIn,
                    manageRuleId: this.state.activeJobIdSummary
                }
                this.props.getRequirementSummaryRequest(payload)
            }
        }
    }


    onStoreCode(e) {
        this.setState({
            scode: e.target.value,
            type: 2,
            current: 1,
            loader: true
        })
        let data = {
            scode: e.target.value,
            no: 1,
            type: 2,
            displayName: this.state.tableTab == "requirement" ? "Requirement Summary" : "Allocation Summary",
            jobName: this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob
        }
      //  this.props.summaryDetailRequest(data);
    }
    onRemoveFilter(e) {
        this.setState({
            current: 1,
            type: 1,
            scode: "",
            loader: true
        })
        let data = {
            no: 1,
            type: 1,
            scode: "",
            displayName: this.state.tableTab == "requirement" ? "Requirement Summary" : "Allocation Summary",
            jobName: this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob
        }
       // this.props.summaryDetailRequest(data);
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    // onCSVClick(e){
    //  e.preventDefault();
    //  let headers = {
    //     'X-Auth-Token': sessionStorage.getItem('token'),
    //     'Content-Type': 'application/json'
    // }
    // axios.get(`${CONFIG.BASE_URL}/aws/s3bucket/download/OUTPUT`, { headers: headers })
    //     .then(res => {
    //     }).catch((error) => {
    //     })   
    // }
    getTO() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let data = {
            jobId: "NA"
        }
        axios.post(`${CONFIG.BASE_URL}/aws/s3bucket/download/GENERATE_TRANSFER_ORDER`, data, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource.URL}`)
            }).catch((error) => {
            });
    }
    manageDrop() {
        this.setState({
            csvDropDown: !this.state.csvDropDown
        })
    }
    handleClickOutside(event) {
        var id = event.target.id
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            if (id == "csvDropMultiple") {
            } else {
                this.setState({
                    csvDropDown: false
                })
            }
        }
    }
    openColoumSetting(data) {
        if (this.state.customHeadersState.length == 0) {
            this.setState({
                headerCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                coloumSetting: true
            })
        } else {
            this.setState({
                coloumSetting: false
            })
        }
    }
    pushColumnData(data) {
        let getHeaderConfig = this.state.getHeaderConfig
        let customHeadersState = this.state.customHeadersState
        let headerConfigDataState = this.state.headerConfigDataState
        let customHeaders = this.state.customHeaders
        let saveState = this.state.saveState
        if (this.state.headerCondition) {
            if (!data.includes(getHeaderConfig) || this.state.getHeaderConfig.length == 0) {
                getHeaderConfig.push(data)
                if (!getHeaderConfig.includes(headerConfigDataState)) {
                    let keyget = (_.invert(headerConfigDataState))[data];
                    Object.assign(customHeaders, { [keyget]: data })
                    saveState.push(keyget)
                }
                if (!Object.keys(customHeaders).includes(this.state.defaultHeaderMap)) {
                    let keygetvalue = (_.invert(headerConfigDataState))[data];
                    this.state.defaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(customHeadersState) || this.state.customHeadersState.length == 0) {
                customHeadersState.push(data)
                if (!customHeadersState.includes(headerConfigDataState)) {
                    let keyget = (_.invert(headerConfigDataState))[data];
                    Object.assign(customHeaders, { [keyget]: data })
                    saveState.push(keyget)
                }
                if (!Object.keys(customHeaders).includes(this.state.headerSummary)) {
                    let keygetvalue = (_.invert(headerConfigDataState))[data];
                    this.state.headerSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            getHeaderConfig,
            customHeadersState,
            customHeaders,
            saveState
        })
    }
    closeColumn(data) {
        let getHeaderConfig = this.state.getHeaderConfig
        let headerConfigState = this.state.headerConfigState
        let customHeaders = []
        let customHeadersState = this.state.customHeadersState
        if (!this.state.headerCondition) {
            for (let j = 0; j < customHeadersState.length; j++) {
                if (data == customHeadersState[j]) {
                    customHeadersState.splice(j, 1)
                }
            }
            for (var key in headerConfigState) {
                if (!customHeadersState.includes(headerConfigState[key])) {
                    customHeaders.push(key)
                }
            }
            if (this.state.customHeadersState.length == 0) {
                this.setState({
                    headerCondition: false
                })
            }
        } else {
            for (var i = 0; i < getHeaderConfig.length; i++) {
                if (data == getHeaderConfig[i]) {
                    getHeaderConfig.splice(i, 1)
                }
            }
            for (var key in headerConfigState) {
                if (!getHeaderConfig.includes(headerConfigState[key])) {
                    customHeaders.push(key)
                }
            }
        }
        customHeaders.forEach(e => delete headerConfigState[e]);
        this.setState({
            getHeaderConfig,
            customHeaders: headerConfigState,
            customHeadersState,
        })
        setTimeout(() => {
            let keygetvalue = (_.invert(this.state.headerConfigDataState))[data];
            let saveState = this.state.saveState
            saveState.push(keygetvalue)
            let headerSummary = this.state.headerSummary
            let defaultHeaderMap = this.state.defaultHeaderMap
            if (!this.state.headerCondition) {
                for (let j = 0; j < headerSummary.length; j++) {
                    if (keygetvalue == headerSummary[j]) {
                        headerSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < defaultHeaderMap.length; i++) {
                    if (keygetvalue == defaultHeaderMap[i]) {
                        defaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                headerSummary,
                defaultHeaderMap,
                saveState
            })
        }, 100);
    }
    saveColumnSetting(e) {
        this.setState({
            coloumSetting: false,
            headerCondition: false,
            saveState: []
        })
        let payload = {
            module: "INVENTORY PLANNING",
            subModule: "REPLENISHMENT",
            section: "SUMMARY",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.state.tableTab == "requirement" ? "REQUIREMENT SUMMARY" : "ALLOCATION SUMMARY",
            fixedHeaders: this.state.fixedHeaderData,
            defaultHeaders: this.state.headerConfigDataState,
            customHeaders: this.state.customHeaders
        }
        this.props.createHeaderConfigRequest(payload)
    }
    resetColumnConfirmation() {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // paraMsg: "Click confirm to continue.",
            confirmModal: true,
        })
    }
    resetColumn() {
        let payload = {
            module: "INVENTORY PLANNING",
            subModule: "REPLENISHMENT",
            section: "SUMMARY",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.state.tableTab == "requirement" ? "REQUIREMENT SUMMARY" : "ALLOCATION SUMMARY",
            fixedHeaders: this.state.fixedHeaderData,
            defaultHeaders: this.state.headerConfigDataState,
            customHeaders: this.state.headerConfigDataState
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            headerCondition: true,
            coloumSetting: false,
            saveState: []
        })
    }
    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }
    onChange(data) {
        if (data != this.state.tableTab) {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "table header",
                displayName: data == "requirement" ? "Requirement Summary" : "Allocation Summary"
            }
            this.props.getHeaderConfigRequest(payload)
        }
        if (data == 'forecast') {
            this.setState({ tableTab: "forecast", coloumSetting: false })
        } else if (data == 'requirement') {
            this.setState({ tableTab: "requirement", coloumSetting: false })
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    // __________________GET JOB DATA ______________________

    getJobDetails(job) {
        this.setState({
            selectedJob: job
        })

        this.props.toStatusRequest(job);

        this.props.getCSVLinkRequest();
        this.props.lastJobRequest(job);
        // _____________________requirement summary _____________________________
        // let data = {
        //     jobName: job
        // }
        // console.log("allStoreCodeRequest2", data)
        // this.props.allStoreCodeRequest(data);
        let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "table header",
            displayName: this.state.tableTab == "requirement" ? "Requirement Summary" : "Allocation Summary"
        }
        this.props.getHeaderConfigRequest(payload)
    }

    xlscsv = () => {
        if (this.state.currentTab == "allocation") {
            let data = {
                pageNo: 1,
                type: this.state.type,
                manageRuleId: this.state.activeJobIdSummary
            };
            this.props.downloadRepReportRequest({
                module: "REPLENISHMENT_ALLOCATION_SUMMARY",
                data: data
            });
            this.setState({exportToExcel: false});
        }
        else {
            let data = {
                pageNo: 1,
                type: 3,
                manageRuleId: this.state.activeJobIdSummary
            };
            this.props.downloadRepReportRequest({
                module: "REPLENISHMENT_REQUIREMENT_SUMMARY",
                data: data
            });
            this.setState({exportToExcel: false});
        }
    }

    sortData = (key) => {
        if (this.state.currentTab === "allocation") {
            this.props.getAllocationRequest({
                pageNo: this.state.current,
                type: this.state.type,
                sortedBy: key,
                sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC",
                manageRuleId: this.state.activeJobIdSummary
            });
            this.setState({
                sortedBy: key,
                sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
            });
        }
        else {
            this.props.getRequirementSummaryRequest({
                pageNo: this.state.current,
                type: 1,
                sortedBy: key,
                sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC",
                manageRuleId: this.state.activeJobIdSummary
            });
            this.setState({
                sortedBy: key,
                sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
            });
        }
    }

    downloadXLS = () => {
        if (this.state.currentTab === "requirement" && this.props.replenishment.lastJob.data.resource !== null && this.props.replenishment.lastJob.data.resource.excelRequirementPath !== null) {
            window.location.href = this.props.replenishment.lastJob.data.resource.excelRequirementPath;
        }
        else if (this.state.currentTab === "allocation" && this.props.replenishment.lastJob.data.resource !== null && this.props.replenishment.lastJob.data.resource.excelAllocationPath !== null) {
            window.location.href = this.props.replenishment.lastJob.data.resource.excelAllocationPath;
        }
    }

    downloadTO = () => {
        if (this.props.replenishment.lastJob.data.resource !== null && this.props.replenishment.lastJob.data.resource.excelsZipPath !== null) {
            window.location.href = this.props.replenishment.lastJob.data.resource.excelsZipPath;
        }
    }

    render() {
        var Allocation_Summary = JSON.parse(sessionStorage.getItem('Allocation Summary')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Allocation Summary')).crud);
        var Requirement_Summary = JSON.parse(sessionStorage.getItem('Requirement Summary')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Requirement Summary')).crud);
        // let csvLinkfile = this.state.csvLinkFile;
        // this.state.csvLinkFile.toString();
        let csvLinkfile = this.state.csvLinkFile;
        let date = this.state.lastSummary == undefined ? "" : moment(this.state.lastSummary.startedOn).format("YYYY-MM-DD HH:mm");
        let date1 = date == "" ? "" : date.replace("-", "_");
        let date2 = date1 == "" ? "" : date1.replace("-", "_");
        let csvLink = date2 == "" ? "" : date2.replace(" ", "_");
        // let csvLink = "2018_11_26_10:10"
        return (
            <div className="container-fluid pad-l50">
                <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                    <div className="auto-confing-head">
                        <div className="ach-left">
                            <h3>Job Run - <span>Summary</span></h3>
                        </div>
                        <div className="ach-right">
                            <div className="achjbt-row">
                                <p>Showing Summary for</p>
                                <button className="achjbt-jobs" type="button" onClick={(e) => this.openRunningJobs(e)}>{this.state.activeJobNameSummary}</button>
                                {this.state.runningJobs &&
                                <div className="achjbtj-dropdown">
                                   <ul>
                                        {this.state.jobListSummary.map((job, index)=>{
                                            return <li key={index} onClick={() => this.setJobHandler(job)}>{job.jobName}</li>
                                        })}                                        
                                    </ul>
                                </div>}
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.jobListSummary.length == 0 ?
                    <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-30">
                        <div className="summary-page-status m-top-30">
                            <div className="sps-error-msg">
                                <span className="spsem-msg">
                                    <svg xmlns="http://www.w3.org/2000/svg" id="information" width="14.413" height="14.413" viewBox="0 0 15.413 15.413">
                                        <path fill="#ff8103" id="Path_1092" d="M7.706 0a7.706 7.706 0 1 0 7.706 7.706A7.715 7.715 0 0 0 7.706 0zm0 14.012a6.305 6.305 0 1 1 6.305-6.305 6.312 6.312 0 0 1-6.305 6.305z" class="cls-1"/>
                                        <path fill="#ff8103" id="Path_1093" d="M145.936 70a.934.934 0 1 0 .934.935.935.935 0 0 0-.934-.935z" class="cls-1" transform="translate(-138.23 -66.731)"/>
                                        <path fill="#ff8103" id="Path_1094" d="M150.7 140a.7.7 0 0 0-.7.7v4.2a.7.7 0 0 0 1.4 0v-4.2a.7.7 0 0 0-.7-.7z" class="cls-1" transform="translate(-142.994 -133.461)"/>
                                    </svg>
                                    No Jobs Found!
                                </span>
                                <p>Currently no jobs has been configured to your account. Please contact support at support@supplymint.com</p>
                            </div>
                        </div>
                    </div> :
                    <React.Fragment>
                        <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-30">
                            <div className="summary-page-status">
                                <div className="sps-head">
                                    <div className="spsh-left">
                                        <img src={require('../../assets/checked.svg')} />
                                        <div className="spsh-text">
                                            <span className="spsht-heading">{this.state.summaryStatus}</span>
                                            <p>{this.state.summaryInfo}</p>
                                        </div>
                                        <div className="spsh-text spsht-border">
                                            <p>Job ID</p>
                                            <span className="spsht-heading">{this.state.lastSummary.jobId === undefined? "NA" : this.state.lastSummary.jobId}</span>
                                        </div>
                                    </div>
                                    <div className="spsh-right">
                                        <button type="button" onClick={this.downloadTO}>Download Transfer Order</button>
                                    </div>
                                </div>
                                <div className="sps-bottom">
                                    <div className="spsb-col">
                                        <h3>Start Time</h3>
                                        <p>{this.state.startTime}</p>
                                    </div>
                                    <div className="spsb-col">
                                        <h3>End Time</h3>
                                        <p>{this.state.endTime}</p>
                                    </div>
                                    <div className="spsb-col">
                                        <h3>Replenish Date</h3>
                                        <p>{this.state.repDate}</p>
                                    </div>
                                    <div className="spsb-col">
                                        <h3>Trigger Name</h3>
                                        <p>{this.state.triggerName}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                            <div className="auto-confing-head m-top-10">
                                <div className="ach-left">
                                    <div className="global-search-tab gcl-tab">
                                        <ul className="nav nav-tabs gst-inner" role="tablist">
                                            <li className="nav-item active">
                                                <a className="nav-link gsti-btn" href="#requirementsummary" role="tab" data-toggle="tab" onClick={() => {
                                                    this.setState({currentTab: "requirement", sortedBy: "", sortedIn: "DESC"});
                                                    this.props.getHeaderConfigRequest({
                                                        enterpriseName: "TURNINGCLOUD",
                                                        attributeType: "TABLE HEADER",
                                                        displayName: "REPLENISHMENT_REQUIREMENT_SUMMARY",
                                                    });
                                                    this.props.getRequirementSummaryRequest({pageNo: 1, type: 1, manageRuleId: this.state.activeJobIdSummary})}}>Requirement Summary</a>
                                            </li>
                                            <li className="nav-item" >
                                                <a className="nav-link gsti-btn" href="#allocationsummary" role="tab" data-toggle="tab" onClick={() => {
                                                    this.setState({currentTab: "allocation", sortedBy: "", sortedIn: "DESC"}); this.props.getHeaderConfigRequest({
                                                        enterpriseName: "TURNINGCLOUD",
                                                        attributeType: "TABLE HEADER",
                                                        displayName: "REPLENISHMENT_ALLOCATION_SUMMARY",
                                                    });
                                                    this.props.getAllocationRequest({pageNo: 1, type: this.state.type, manageRuleId: this.state.activeJobIdSummary})}}>Allocation Summary</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="ach-right">
                                    <div className="gvpd-download-drop">
                                        <button type="button" style={{width: "auto"}} onClick={this.downloadXLS}
                                            className={(this.state.currentTab === "requirement" && this.props.replenishment.lastJob.data.resource !== undefined && this.props.replenishment.lastJob.data.resource !== null && this.props.replenishment.lastJob.data.resource.excelRequirementPath !== null) || (this.state.currentTab === "allocation" && this.props.replenishment.lastJob.data.resource !== undefined && this.props.replenishment.lastJob.data.resource !== null && this.props.replenishment.lastJob.data.resource.excelAllocationPath !== null) ? "otbrc-down-report" : "otbrc-down-report btnDisabled"}>XLS{" "}
                                            <span className="otbrcdr-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                                    <path fill={(this.state.currentTab === "requirement" && this.props.replenishment.lastJob.data.resource !== undefined && this.props.replenishment.lastJob.data.resource !== null && this.props.replenishment.lastJob.data.resource.excelRequirementPath !== null) || (this.state.currentTab === "allocation" && this.props.replenishment.lastJob.data.resource !== undefined && this.props.replenishment.lastJob.data.resource !== null && this.props.replenishment.lastJob.data.resource.excelAllocationPath !== null) ? "#12203c" : "#d8d3d3"}
                                                        id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                    <div className="gvpd-download-drop">
                                        <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                                <g>
                                                    <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                    <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                    <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                                </g>
                                            </svg>
                                        </button>
                                        {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button" onClick={this.xlscsv}> 
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                Export to Excel</button>
                                            </li>
                                        </ul>}
                                    </div>
                                    {/* <div className="achjbt-row">
                                        <p>Filter by Store</p>
                                        <button className="achjbt-jobs" type="button">SM-TURNINGCLOUD-D-IPOAC</button>
                                        {this.state.runningJobsFilter &&
                                        <div className="achjbtj-dropdown">
                                            <ul>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                            </ul>
                                        </div>}
                                    </div> */}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 col-md-12 p-lr-47">
                            <div className="tab-content">
                                <div className="tab-pane fade in" id="allocationsummary" role="tabpanel">
                                    <div className="vendor-gen-table">
                                        <div className="manage-table">
                                        <ColoumSetting {...this.props} {...this.state} coloumSetting={this.state.coloumSetting} getHeaderConfig={this.state.getHeaderConfig} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} />
                                            <table className="table gen-main-table">
                                                <thead>
                                                    <tr>
                                                    {
                                                        Object.keys(this.state.tableHeaders).map((key) => <th key={key} className={this.state.sortedBy == key && this.state.sortedIn == "ASC" ? "rotate180" : ""} onClick={() => this.sortData(key)}><label>{this.state.tableHeaders[key]}</label><img src={require('../../assets/headerFilter.svg')} /></th>)    
                                                    }
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.allocationTable.length == 0 || Object.keys(this.state.tableHeaders).length == 0 ?
                                                    <tr>
                                                        <td align="center" colSpan="12">
                                                            <label>No Data Available</label>
                                                        </td>
                                                    </tr>
                                                    :this.state.allocationTable.map((data, indx) =>(
                                                    <tr key={indx}>
                                                    {
                                                        Object.keys(this.state.tableHeaders).map((key) =>
                                                        key == "storeCode" ? <td><label className="bold">{data[key]}</label></td> :
                                                        <td><label>{data[key]}</label></td>)
                                                    }
                                                    </tr>
                                                    ))}                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="col-md-12 pad-0" >
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={this.state.currPage} onChange={this.pageHandler} onKeyPress={this.jumpToPage} />
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={this.page}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane fade in active" id="requirementsummary" role="tabpanel">
                                    <div className="vendor-gen-table">
                                        <div className="manage-table">
                                            <table className="table gen-main-table">
                                                <thead>
                                                    <tr>
                                                    {
                                                        Object.keys(this.state.tableHeaders).map((key) => <th key={key} className={this.state.sortedBy == key && this.state.sortedIn == "ASC" ? "rotate180" : ""} onClick={() => this.sortData(key)}><label>{this.state.tableHeaders[key]}</label><img src={require('../../assets/headerFilter.svg')} /></th>)    
                                                    }
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.requirementTable.length == 0 || Object.keys(this.state.tableHeaders).length == 0 ?
                                                    <tr>
                                                        <td align="center" colSpan="12">
                                                            <label>No Data Available</label>
                                                        </td>
                                                    </tr>
                                                    :this.state.requirementTable.map((data, indx) =>(
                                                        <tr key={indx}>
                                                            {
                                                                Object.keys(this.state.tableHeaders).map((key) =>
                                                                key == "storecode" ? <td><label className="bold">{data[key]}</label></td> :
                                                                <td><label>{data[key]}</label></td>)
                                                            }
                                                        </tr>
                                                    ))}                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="col-md-12 pad-0" >
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={this.state.currPage} onChange={this.pageHandler} onKeyPress={this.jumpToPage} />
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={this.page}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                }
                {/* <div className="replenishment_container newSummary">
                    <div className="col-md-12 col-sm-12 pad-0 alignMiddle">
                        <div className="col-md-6 col-sm-6 pad-0">
                            <ul className="list_style m0">
                                <li>
                                    <label className="contribution_mart">
                                        ENGINE RUN SUMMARY
                                </label>
                                </li>
                                <li>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-6 col-sm-6 pad-lft-0 summaryDrop text-right">
                            <p className="displayInline">Showing Summary for</p>
                            <div className="settingDrop dropdown displayInline setUdfMappingMain pad-0">
                                <button className="btn btn-default dropdown-toggle userModalSelect textElippse" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    {this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob}
                                    <i className="fa fa-chevron-down"></i>
                                </button>

                                <ul className="dropdown-menu widthAuto" aria-labelledby="dropdownMenu1">
                                    {Object.keys(this.state.getJobs).map((data, key) => (
                                        <li key={key} onClick={(e) => this.getJobDetails(this.state.getJobs[data])}>
                                            <a>{data}</a>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-10">
                        <div className="col-md-2 col-sm-12 col-xs-12 pad-lft-0">
                            <div className="runSummary">
                                <div className="statusTop">
                                    <img src={successIcon} />
                                    <h5>Succeeded !</h5>
                                </div>
                                <div className="statusBottom">
                                    <label>Your last job run is completed <br /> successfully</label>
                                </div>
                            </div>
                            <div className="runSummary">
                                <div className="statusTop">
                                    <img src={failedSummary} />
                                    <h5>Failure !</h5>
                                </div>
                                <div className="statusBottom">
                                    <label>Your last job run has been failed</label>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-10 col-sm-12 col-xs-12 pad-lft-0">
                            <div className="runSummary summaryResp">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="col-md-12 col-xs-12 pad-0">
                                        <div className="headSummary">
                                            <h2>LAST JOB RUN SUMMARY</h2>
                                            <div className="col-md-12 jobId">
                                                <label>Job Id</label>
                                                <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.jobId}</span>
                                            </div>
                                            <div className="col-md-12 pad-0 jobBottom">
                                                <div className="jobDetails">
                                                    <div className="col-md-3 pad-0">
                                                        <label>Start Time</label>
                                                        <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.startedOn}</span>
                                                    </div>
                                                    <div className="col-md-3 pad-0">
                                                        <label>End Time</label>
                                                        <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.completeOn}</span>
                                                    </div>
                                                    <div className="col-md-3 pad-0">
                                                        <label>Replenish Date</label>
                                                        <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.repDate}</span>
                                                    </div>
                                                    <div className="col-md-3 pad-0">
                                                        <label>Trigger Name</label>
                                                        <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.triggerName}</span>
                                                    </div>
                                                </div>
                                                <div className="downloadTO">
                                                    <div className="downloadSummaryButton m-top-5">
                                                        <div className="col-md-12 pad-0">
                                                            {this.state.toCode == "SUCCESS" ? <button type="button" onClick={(e) => this.getTO(e)}>Download Transfer Order</button> :
                                                                <button type="button" className="btnDisabled" >Download Transfer Order</button>}
                                                        </div>
                                                        <div className="col-md-12 pad-0 m-top-10 alignMiddle">
                                                            <img src={exclaimIcon} />
                                                            <p className="master_para">{this.state.fileInfo}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    -------------------------------------------OLD summary ------------------------------
                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                        <div className="col-md-7 col-sm-12 col-xs-12 pad-lft-0">
                            <div className="runSummary summaryResp">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="col-md-12 col-xs-12 pad-0">
                                        <div className="headSummary">
                                            <h2>LAST JOB RUN SUMMARY</h2>
                                            <ul className="list-inline jobSummaaryDetail">
                                                <li>
                                                    <label>Job Id</label>
                                                    <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.jobId}</span>
                                                </li>
                                                <li>
                                                    <label>Start Time</label>
                                                    <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.startedOn}</span>
                                                </li>
                                                <li>
                                                    <label>End Time</label>
                                                    <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.completeOn}</span>
                                                </li>
                                                <li>
                                                    <label>Replenish Date</label>
                                                    <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.repDate}</span>
                                                </li>
                                                <li>
                                                    <label>Trigger Name</label>
                                                    <span>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.triggerName}</span>
                                                </li>
                                                <li>
                                                    <label>Job Status</label>
                                                    {this.state.lastSummary == undefined ? <span>NA</span>
                                                        : <div className="statusDiv summaryStatus">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="263" height="33" viewBox="0 0 263 33">
                                                                <g fill="none" fillRule="nonzero" transform="translate(-1)">
                                                                    <rect width="160" height="33" x="2" fill="#7ED321" rx="4" />
                                                                    <path fill="#FDFDFD" d="M162 25c2.21 0 1.5-4 1.5-8s.71-8-1.5-8a8 8 0 1 0 0 16zM3 25A8 8 0 1 0 3 9c-2.21 0-2 4-2 8s-.21 8 2 8z" />
                                                                </g>
                                                            </svg>
                                                            <label>{this.state.lastSummary == undefined ? "NA" : this.state.lastSummary.jobRunState}</label>
                                                        </div>}
                                                    <div className="statusDiv displaynone">
                                                           <svg xmlns="http://www.w3.org/2000/svg" width="263" height="33" viewBox="0 0 263 33">
                                                              <g fill="none" fillRule="nonzero" transform="translate(-1)">
                                                                 <rect width="261" height="33" x="2" fill="#C44569" rx="4" />
                                                                <path fill="#FDFDFD" d="M262 25c2.21 0 1.5-4 1.5-8s.71-8-1.5-8a8 8 0 1 0 0 16zM3 25A8 8 0 1 0 3 9c-2.21 0-2 4-2 8s-.21 8 2 8z" />
                                                            </g>
                                                         </svg>
                                                          <label>UNSUCCESSFUL</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-5 col-sm-12 col-xs-12 pad-0">
                            <div className="runSummary blankDiv">
                            </div>
                        </div>
                    </div>
                    ------------------------------------------Old summary end-----------------------------------
                    <div className="col-md-12 col-sm-12 pad-0 m-top-30 summaryBtns displayFlex">
                        <div className="col-md-8 alignEnd pad-0">
                            <ul className="list-inline tabs">
                                {Allocation_Summary > 0 || !Requirement_Summary > 0 ? <li className={this.state.tableTab == "forecast" ? "active" : ""} onClick={() => this.onChange('forecast')}><label>Allocation Summary</label></li> : null}
                                {Requirement_Summary > 0 ? <li className={this.state.tableTab == "requirement" ? "active" : ""} onClick={() => this.onChange('requirement')}><label>Requirement Summary</label></li> : null}
                            </ul>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-6 pad-0">
                            <div className="summaryExportData">
                                <h2>EXPORT </h2>
                                <ul className={csvLinkfile == null ? "list-inline jobSummaaryDetail1" : csvLinkfile == "" ? "list-inline jobSummaaryDetail1" : "list-inline jobSummaaryDetail1 csvBtn csvSummary"}>
                                    <li>
                                        <a href={`${CONFIG.BASE_URL}/core/aws/s3bucket/downloads?key=${csvLink}`} download>
                                            <button type="button" >XLS</button>
                                        </a>
                                        <a href={csvLinkfile == null ? null : csvLinkfile == "" ? null : csvLinkfile} download>
                                            {!this.state.popup ? <button className={csvLinkfile == null ? "btnDisabled pointerNone" : csvLinkfile == "" ? "btnDisabled pointerNone" : "displayPointer"} type="button">CSV</button>
                                                : <button type="button" id="csvDropMultiple" onClick={() => this.manageDrop()}>CSV</button>}
                                            {this.state.csvDropDown ? <div className="csvDropDown assortmentSelect" ref={(e) => this.setWrapperRef(e)}>
                                                <div className="assortSelectDiv" >
                                                    <div className="dropDownItems m-top-5">
                                                        <h4>Available Downloads</h4>
                                                        <ul className="pad-0 m-top-15">
                                                            {this.state.dpLinks.length == 0 ? null : this.state.dpLinks.map((data, key) => (<li key={key} onClick={(e) => window.open(data.value)}><span>{data.key}</span> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path fill="#000" fill-rule="nonzero" d="M14 10.112V13.3a.7.7 0 0 1-.7.7H.7a.7.7 0 0 1-.7-.7v-3.188a.7.7 0 0 1 1.4 0V12.6h11.2v-2.488a.7.7 0 0 1 1.4 0zM7 0a.7.7 0 0 0-.7.7v7.021L3.964 5.385a.7.7 0 1 0-.99.99l3.531 3.531a.7.7 0 0 0 .99 0l3.53-3.53a.7.7 0 1 0-.99-.99L7.7 7.72V.7A.7.7 0 0 0 7 0z" /></svg></li>))}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> : null}
                                        </a>
                                        <div className="csvDownloadBtn"><img src={csvIcon} /><label className="posRelative">CSV</label><span className="glyphicon glyphicon-menu-down"></span>
                                            <div className="csvDropDown">
                                                <ul>
                                                    <li><img src={csvIcon} />CSV</li>
                                                    <li><img src={csvIcon} />XLS</li>
                                                    <li><img src={csvIcon} />PDF</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {(csvLinkfile == null || csvLinkfile == "") && (this.state.dpLinks.length == 0) ? <span style={{ display: "block" }} >No attachment found</span> : null}
                                    </li>
                                </ul>
                            </div>
                            <div className="filterSummary">
                                <label>FILTER BY STORE
                                {this.state.scode == "" ? null : <button type="button" onClick={(e) => this.onRemoveFilter(e)} className="summaryFilter">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="13" viewBox="0 0 12 13">
                                            <g fill="#FFF" fillRule="evenodd">
                                                <path fillRule="nonzero" d="M0 4.333c0 .599.488 1.084 1.09 1.084v5.416C1.09 12.03 2.069 13 3.274 13h5.454c1.205 0 2.182-.97 2.182-2.167V5.417c.603 0 1.091-.485 1.091-1.084V3.25c0-.598-.488-1.083-1.09-1.083H8.726V1.083C8.727.485 8.24 0 7.637 0H4.363C3.76 0 3.273.485 3.273 1.083v1.084H1.09C.488 2.167 0 2.652 0 3.25v1.083zm9.818 6.5c0 .599-.488 1.084-1.09 1.084H3.272c-.603 0-1.091-.485-1.091-1.084V5.417h7.636v5.416zm-5.454-9.75h3.272v1.084H4.364V1.083zM1.09 3.25h9.818v1.083H1.091V3.25z" />
                                                <path d="M3.818 10.833a.544.544 0 0 0 .546-.541v-3.25c0-.3-.245-.542-.546-.542a.544.544 0 0 0-.545.542v3.25c0 .299.244.541.545.541zM6 10.833a.544.544 0 0 0 .545-.541v-3.25c0-.3-.244-.542-.545-.542a.544.544 0 0 0-.545.542v3.25c0 .299.244.541.545.541zM8.182 10.833a.544.544 0 0 0 .545-.541v-3.25c0-.3-.244-.542-.545-.542a.544.544 0 0 0-.546.542v3.25c0 .299.245.541.546.541z" />
                                            </g>
                                        </svg>
                                        Remove Filter
                                        </button>}
                                </label>
                                <select value={this.state.scode} onChange={(e) => this.onStoreCode(e)} className="summaryfilterSelect displayPointer">
                                    <option value="">Select Store Code</option>
                                    {this.state.stores == undefined ? null : this.state.stores.map((data, key) => (<option key={key} value={data}>{data}</option>))}
                                </select>
                            </div>
                        </div>

                    </div>
                    {this.state.tableTab == "forecast" ? <div><div className="col-md-12 p-lr-47">
                        <div className="vendor-gen-table">
                                <div className="manage-table">
                                <ColoumSetting {...this.props} {...this.state} coloumSetting={this.state.coloumSetting} getHeaderConfig={this.state.getHeaderConfig} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} />
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                                                    <th key={key}>
                                                        <label>{data}</label><img src={require('../../assets/headerFilter.svg')} />
                                                    </th>
                                                )) : this.state.customHeadersState.map((data, key) => (
                                                    <th key={key}>
                                                        <label>{data}</label><img src={require('../../assets/headerFilter.svg')} />
                                                    </th>
                                                ))}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.summary.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.summary.map((data, key) => (
                                            <tr key={key}>
                                                {this.state.headerSummary.length == 0 ? this.state.defaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        <label>{data[hdata]}</label>
                                                    </td>
                                                )) : this.state.headerSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        <label>{data[sdata]}</label>
                                                    </td>
                                                ))}
                                            </tr>))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} />
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <div className="pagination-inner">
                                                <ul className="pagination-item">
                                                    {this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? <li >
                                                        <button className="disable-first-btn" type="button"  >
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> : <li >
                                                        <button className="" type="button" onClick={(e) => this.page(e)} id="first" >
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li>}
                                                    {this.state.prev != 0 && this.state.prev != "" ? <li >
                                                        <button className="" type="button" onClick={(e) => this.page(e)} id="prev">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> : <li >
                                                        <button className="" type="button" disabled>
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li>}
                                                    <li>
                                                        <button className="pi-number-btn">
                                                            <span>{this.state.current}/{this.state.maxPage}</span>
                                                        </button>
                                                    </li>
                                                    {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                                        <button className="next-btn" onClick={(e) => this.page(e)} id="next">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> : <li >
                                                        <button className="dis-next-btn" disabled>
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li>}
                                                    <li >
                                                        <button className="dis-last-btn" disabled>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </div> :
                        <div>
                            <div className="col-md-12 p-lr-47">
                                <div className="vendor-gen-table">
                                <div className="manage-table">
                                <ColoumSetting {...this.props} {...this.state} coloumSetting={this.state.coloumSetting} getHeaderConfig={this.state.getHeaderConfig} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} />
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                                                    <th key={key}>
                                                        <label>{data}</label><img src={require('../../assets/headerFilter.svg')} />
                                                    </th>
                                                )) : this.state.customHeadersState.map((data, key) => (
                                                    <th key={key}>
                                                        <label>{data}</label><img src={require('../../assets/headerFilter.svg')} />
                                                    </th>
                                                ))}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.summary.map((data, key) => (
                                                <tr key={key}>
                                                    {this.state.headerSummary.length == 0 ? this.state.defaultHeaderMap.map((hdata, key) => (
                                                        <td key={key} >
                                                            <label>{data[hdata]}</label>
                                                        </td>
                                                    )) : this.state.headerSummary.map((sdata, keyy) => (
                                                        <td key={keyy} >
                                                            <label>{data[sdata]}</label>
                                                        </td>
                                                    ))}
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} />
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    }
                </div> */}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
            </div>
        );
    }
}
export default NewSummary;