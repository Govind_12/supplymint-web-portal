import React from "react";
import Footer from '../footer';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import { CONFIG } from "../../config/index";
import axios from 'axios';

class HistoryReplenishment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageNo: 1,
            historyData: [],
            loader: true,
            prev: "",
            current: '',
            next: "",
            type: 1,
            maxPage: "",
            from: "",
            to: "",
            max: "",
            weekly: true,
            monthly: false,
            min: "",
            alert: false,
            errorCode: "",
            errorMessage: "",
            code: "",
            showFilter: false,
            fromerr: false,
            toerr: false,
            getJobs: [],
            selectedJob:"",
            downloadDropDown: "",
            csvLinkFile: "",
            dpLinks: [],
            popUpdrop: false,
            selectedJobId: ""
        }
    }
    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.replenishment.jobHistory.data.prePage,
                    current: this.props.replenishment.jobHistory.data.currPage,
                    next: this.props.replenishment.jobHistory.data.currPage + 1,
                    maxPage: this.props.replenishment.jobHistory.data.maxPage,
                })
                if (this.props.replenishment.jobHistory.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.replenishment.jobHistory.data.currPage - 1,
                        from: this.state.from,
                        to: this.state.to,
                        // no: this.state.current
                        jobName: this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob
                    }
                    this.props.jobHistoryRequest(data)
                }
            }
        } else if (e.target.id == "next") {
            if (this.state.current == undefined) {

            } else {
                this.setState({
                    prev: this.props.replenishment.jobHistory.data.prePage,
                    current: this.props.replenishment.jobHistory.data.currPage,
                    next: this.props.replenishment.jobHistory.data.currPage + 1,
                    maxPage: this.props.replenishment.jobHistory.data.maxPage,
                })
                if (this.props.replenishment.jobHistory.data.currPage != this.props.replenishment.jobHistory.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.replenishment.jobHistory.data.currPage + 1,
                        from: this.state.from,
                        to: this.state.to,
                        jobName: this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob
                        // no: this.state.current
                    }
                    this.props.jobHistoryRequest(data)
                }
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.replenishment.jobHistory.data.prePage,
                    current: this.props.replenishment.jobHistory.data.currPage,
                    next: this.props.replenishment.jobHistory.data.currPage + 1,
                    maxPage: this.props.replenishment.jobHistory.data.maxPage,
                })
                if (this.props.replenishment.jobHistory.data.currPage <= this.props.replenishment.jobHistory.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        from: this.state.from,
                        to: this.state.to,
                        jobName: this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob
                        // no: this.state.current
                    }
                    this.props.jobHistoryRequest(data)
                }
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.jobHistory.isSuccess) {
            this.setState({
                historyData: nextProps.replenishment.jobHistory.data.resource,
                loader: false,
                prev: nextProps.replenishment.jobHistory.data.prePage,
                current: nextProps.replenishment.jobHistory.data.currPage,
                next: nextProps.replenishment.jobHistory.data.currPage + 1,
                maxPage: nextProps.replenishment.jobHistory.data.maxPage,
            })

        } else if (nextProps.replenishment.jobHistory.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.replenishment.jobHistory.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.jobHistory.message.status,
                errorMessage: nextProps.replenishment.jobHistory.message.error == undefined ? undefined : nextProps.replenishment.jobHistory.message.error.errorMessage,
                errorCode: nextProps.replenishment.jobHistory.message.error == undefined ? undefined : nextProps.replenishment.jobHistory.message.error.errorCode
            })
        }
        if (nextProps.replenishment.getAllJob.isSuccess) {
            let jobName = [];
            Object.keys(nextProps.replenishment.getAllJob.data.resource).map( key=>{
                jobName.push(key);
            })
            this.setState({
                loader: false,
                getJobs: jobName == null ? [] : jobName
            })
            let data = {
                type: 1,
                from: this.state.from,
                to: this.state.to,
                no: 1,
                jobName: this.state.selectedJob == "" ? jobName[0] : this.state.selectedJob
            }
            this.props.jobHistoryRequest(data);
            this.props.getAllJobClear()
        }else if(nextProps.replenishment.getAllJob.isError){
            this.setState({
                loader:false
            })
        }

        if (nextProps.replenishment.getCSVLink.isSuccess) {
            if(nextProps.replenishment.getCSVLink.data.resource.URL==null && nextProps.replenishment.getCSVLink.data.resource.link==null){
                this.setState({
                 alert: true,
                code:"4000",
                errorMessage:"Requirement not generated",
                errorCode: "4000"
                })
            }
            if (nextProps.replenishment.getCSVLink.data.resource.isPopup) {
                this.positionTable()
                this.setState({
                    csvLinkFile: "",
                    dpLinks: nextProps.replenishment.getCSVLink.data.resource.link,
                    popUpdrop: nextProps.replenishment.getCSVLink.data.resource.isPopup,
                    selectedJobId: nextProps.replenishment.getCSVLink.data.resource.jobId
                })
            } else {
                if (nextProps.replenishment.getCSVLink.data.resource.URL !== null) {
                    window.open(nextProps.replenishment.getCSVLink.data.resource.URL)
                }
                this.setState({
                    csvLinkFile: nextProps.replenishment.getCSVLink.data.resource.URL,
                    dpLinks: [],
                    popUpdrop: nextProps.replenishment.getCSVLink.data.resource.isPopup,
                    selectedJobId: nextProps.replenishment.getCSVLink.data.resource.jobId
                })
            }

            this.props.getCSVLinkClear()
        } else if (nextProps.replenishment.getCSVLink.isLoading) {
            this.setState({ loader: true })
        } else if (nextProps.replenishment.getCSVLink.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getCSVLink.message.status,
                errorMessage: nextProps.replenishment.getCSVLink.message.error == undefined ? undefined : nextProps.replenishment.getCSVLink.message.error.errorMessage,
                errorCode: nextProps.replenishment.getCSVLink.message.error == undefined ? undefined : nextProps.replenishment.getCSVLink.message.error.errorCode
            })
            this.props.getCSVLinkClear()
        }



    }
    componentWillMount() {
        // if (this.props.replenishment.jobHistory.isSuccess) {
        //     this.setState({
        //         prev: this.props.replenishment.jobHistory.data.prePage,
        //         current: this.props.replenishment.jobHistory.data.currPage,
        //         next: this.props.replenishment.jobHistory.data.currPage + 1,
        //         maxPage: this.props.replenishment.jobHistory.data.maxPage,
        //     })
        // } else {
     
        // }
        this.props.getAllJobRequest('data')
    }

    historyDownload(id) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}/aws/s3bucket/download/history/OUTPUT/${id}`, { headers: headers })
            .then(res => {
                window.open(res.data.data.resource)
            }).catch((error) => {
            })
    }


    handleChange(e) {
        e.preventDefault();
        if (e.target.id == "from") {
            e.target.placeholder = e.target.value;
            this.setState({
                from: e.target.value,
                min: e.target.value
            },
                () => {
                    this.from();
                }
            );
        } else if (e.target.id == "to") {
            e.target.placeholder = e.target.value;
            this.setState({
                to: e.target.value
            },
                () => {
                    this.to();
                }
            );
        }
    }

    from() {
        if (
            this.state.from == "" || this.state.from.trim() == "") {
            this.setState({
                fromerr: true
            });
        } else {
            this.setState({
                fromerr: false
            });
        }
    }

    to() {
        if (
            this.state.to == "" || this.state.to.trim() == "") {
            this.setState({
                toerr: true
            });
        } else {
            this.setState({
                toerr: false
            });
        }
    }

    onSubmit(e) {
        e.preventDefault();
        this.from();
        this.to();
        let t = this;
        setTimeout(function () {
            const { toerr, fromerr } = t.state;
            if (!toerr && !fromerr) {
                t.setState({
                    type: 2,
                    loader: true
                })
                let data = {
                    type: 2,
                    from: t.state.from,
                    to: t.state.to,
                    no: t.state.current,
                    jobName: t.state.selectedJob == "" ? t.state.getJobs[0] : t.state.selectedJob
                }
                t.props.jobHistoryRequest(data)
            }
        }, 100)
    }

    onSort(e) {
        if (e.target.id == "monthly") {
            this.setState({
                monthly: true,
                weekly: false
            })
        } else {
            this.setState({
                monthly: false,
                weekly: true
            })
        }
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }

    onClear() {
        if (this.state.from != "" || this.state.to != "") {
            this.setState({
                from: "",
                to: "",
                type: 1
            })
            document.getElementById('from').placeholder = "Start Date";
            document.getElementById('to').placeholder = "End Date";
            let data = {
                type: 1,
                from: "",
                to: "",
                no: 1,
                jobName: this.state.getJobs[0]
            }
            this.props.jobHistoryRequest(data);
        }
    }
    getJobDetails(job){
        console.log(job);
        this.setState({
            selectedJob:job
        })
        let data = {
            type: 1,
            from: this.state.from,
            to: this.state.to,
            no: 1,
            jobName:job
        }
        this.props.jobHistoryRequest(data);
    }

    componentDidMount() {
        document.body.addEventListener('scroll', this.positionTable);
        document.addEventListener("mousedown", this.handleClickOutside);
    }
    componentWillUnmount() {
        document.body.removeEventListener('scroll', this.positionTable);
        document.removeEventListener("mousedown", this.handleClickOutside);
    }
    manageFilesDownload = (e) => {
        if (e.target.id !== this.state.downloadDropDown) {
            this.positionTable()
            var btn = e.target
            if (this.state.downloadDropDown !== event.target.id) {
                this.props.getCSVLinkRequest(e.target.id)
            }
            this.setState({
                downloadDropDown: this.state.downloadDropDown == "" ? e.target.id : "",
                popUpdrop: !this.state.popUpdrop
            }, () => this.positionTable())
        }
    }
    handleClickOutside = (event) => {
        if (this.dropdopwn && !this.dropdopwn.contains(event.target)) {
            if (this.state.downloadDropDown !== event.target.id) {
                this.setState({ downloadDropDown: "", popUpdrop: false })
            }
        } else {
        }
    }
    positionTable() {
        setTimeout(() => {
            if (document.getElementById('csvDropMain') !== null) {
                var element = document.getElementById('csvDropMain')
                var offset = element.getBoundingClientRect()
                var top = offset.top;
                var left = offset.left;
                document.getElementById('csvDrop').style.top = top - 33
                document.getElementById('csvDrop').style.left = left + 105
            }
        }, 1);
        if (document.getElementById('csvDropMain') !== null) {
            var element = document.getElementById('csvDropMain')
            var offset = element.getBoundingClientRect()
            var top = offset.top;
            var left = offset.left;
            document.getElementById('csvDrop').style.top = top - 33
            document.getElementById('csvDrop').style.left = left + 105
        }
    }

    render() {
        let csvLinkfile = this.state.csvLinkFile;


        return (
            <div>
                <div className="container-fluid">
                    <div className="container_div" id="">

                        <div className="container-fluid">
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                <div className="replenishment_container">
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <div className="col-md-6 pad-0">
                                            <ul className="list_style">
                                                <li>
                                                    <label className="contribution_mart">
                                                        JOB RUN HISTORY
                                            </label>
                                                </li>
                                                <li>
                                                    {/* <p className="master_para">Last engine run history</p> */}
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-md-6 col-sm-6 pad-lft-0 summaryDrop text-right">
                                            <p className="displayInline">Showing Summary for</p>
                                            <div className="settingDrop dropdown displayInline setUdfMappingMain pad-0">
                                                <button className="btn btn-default dropdown-toggle userModalSelect textElippse" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                  {this.state.selectedJob == "" ? this.state.getJobs[0] : this.state.selectedJob}
                                            <i className="fa fa-chevron-down"></i>
                                                </button>

                                                <ul className="dropdown-menu widthAuto" aria-labelledby="dropdownMenu1">
                                                    {Object.keys(this.state.getJobs).map((data, key) => (
                                                        <li key={key} onClick={()=> this.getJobDetails(this.state.getJobs[data])}>
                                                            <a>{data}</a>
                                                        </li>
                                                    ))}
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-md-8 col-xs-12 col-sm-12 pad-0">
                                            <div className="historyDataDiv">
                                                <div className="radioBtnDiv">
                                                    {/* <ul className="list-inline ">
                                                    <li>
                                                        <label className="containerHistoryRadio">
                                                            <input type="text" type="radio" checked={this.state.weekly} name="radio" />
                                                            <span  onClick={(e) => this.onSort(e)} id="weekly" className="checkmark"></span>
                                                        </label>
                                                        <div className="contentRadioHistory">
                                                            <span>Weekly</span>
                                                            <p>Showing Job history on weekly
                                                                basis</p>
                                                        </div>

                                                    </li>

                                                    <li>
                                                        <label className="containerHistoryRadio">
                                                            <input type="text" type="radio" name="radio" />
                                                            <span className="checkmark"></span>
                                                        </label>
                                                        <div className="contentRadioHistory">
                                                            <span>Bi Weekly</span>

                                                        </div>

                                                    </li>

                                                    <li>
                                                        <label className="containerHistoryRadio">
                                                            <input onClick={(e) => this.onSort(e)} id="monthly" checked={this.state.monthly} type="text" type="radio" name="radio" />
                                                            <span onClick={(e) => this.onSort(e)} id="monthly" className="checkmark"></span>
                                                        </label>
                                                        <div className="contentRadioHistory">
                                                            <span>Months</span>

                                                        </div>

                                                    </li>
                                                </ul> */}
                                                </div>
                                                <form onSubmit={(e) => this.onSubmit(e)}>
                                                    <div className="formHistoryDiv">
                                                        <h2>
                                                            Sort Date Wise
                                                    </h2>
                                                        <p>
                                                            Showing data on the basis of selected date range
                                                    </p>
                                                        <div className="formHistory ">
                                                            <div className="floatLeft">
                                                                <label className="fromLabel">From</label>
                                                                <input type="date" id="from" onChange={(e) => this.handleChange(e)} className={this.state.fromerr ? "historyInput pad-lft-5  errorBorder" : "historyInput pad-lft-5"} placeholder="Start Date" value={this.state.from} />
                                                                {this.state.fromerr ? <span className="error">Select From Date</span> : null}

                                                            </div>
                                                            <div className="floatRight">
                                                                <label className="toLabel">To</label>
                                                                <input type="date" id="to" max={this.state.max} min={this.state.min} onChange={(e) => this.handleChange(e)} className={this.state.toerr ? "historyInput pad-lft-5  errorBorder" : "historyInput pad-lft-5"} placeholder="End Date" value={this.state.to} />
                                                                {this.state.toerr ? <span className="error">Select To Date</span> : null}
                                                            </div>


                                                        </div>
                                                        <div className="filter">
                                                            <button type="submit" className="filterBtnHistory m-rgt-20  ">
                                                                Filter
                                                            </button>
                                                            {this.state.to == "" && this.state.from == "" ? <label className="pointerNone textDisable resetBtn">Clear</label>
                                                                : <label onClick={() => this.onClear()} className="displayPointer resetBtn">Clear</label>}
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-12 col-sm-12 pad-0 historyMain">
                                        <ul className="list_style">
                                            <li>
                                                <label className="contribution_mart">
                                                    HISTORY IN DETAIL
                                                </label>

                                            </li>

                                        </ul>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0 bordere3e7f3 tableWidth tableGeneric historyTableMain">
                                        <div className="col-md-12 col-sm-12 pad-0">
                                            <div className="Zui-wrapper">
                                                <div className="scrollableTableFixed table-scroll zui-scrollerHistory" id="table-scroll">
                                                    <table className="table scrollTable main-table zui-table scrollerHistory historyTableDownload tableOddColor">

                                                        <thead>
                                                            <tr>
                                                                <th className="fixed-side1 alignMiddle">
                                                                    <ul className="list-inline">
                                                                        {/* <li>
                                                                        <p className="para_purchase">
                                                                            <input type="text" checked={this.state.descriptionChecked ? true : false} onClick={(e) => this.onAllDescription(e)} className="purchaseCheck" type="checkbox" id="c8" name="cb" />
                                                                            <label htmlFor="c1" className="purchaseLabel"></label>
                                                                        </p>
                                                                    </li> */}
                                                                        <li className="width95">
                                                                            <label className="lableFixed">
                                                                                Order Status
                                                                            </label>
                                                                        </li>
                                                                        <li className="width80">
                                                                            <label className="lableFixed ">
                                                                                Status
                                                                            </label>
                                                                        </li>
                                                                        <li className="width65">
                                                                            <label className="lableFixed ">
                                                                                Download Csv
                                                                            </label>
                                                                        </li>
                                                                    </ul>

                                                                </th>
                                                                <th className="positionRelative">
                                                                    <label>
                                                                        Job Id
                                                                    </label>
                                                                </th>
                                                                <th className="positionRelative">
                                                                    <label>
                                                                        Start Time
                                                                    </label>
                                                                </th>
                                                                <th className="positionRelative">
                                                                    <label>
                                                                        End Time
                                                                     </label>
                                                                </th>
                                                                <th className="positionRelative">
                                                                    <label>
                                                                        Duration
                                                                    </label>
                                                                </th>
                                                                <th className="positionRelative">
                                                                    <label>
                                                                        Store Count
                                                                    </label>
                                                                </th>
                                                                <th className="positionRelative">
                                                                    <label>
                                                                        Item Count
                                                                    </label>
                                                                </th>
                                                                <th className="positionRelative">
                                                                    <label>
                                                                        Replenish Date
                                                                    </label>
                                                                </th>
                                                                <th className="positionRelative">
                                                                    <label>
                                                                        Transfer Order
                                                                    </label>
                                                                </th>
                                                                {/* <th className="positionRelative">
                                                                <label>
                                                                    DOWNLOAD CSV
                                                                    </label>
                                                            </th> */}
                                                                {/* <th className="positionRelative">
                                                                    <label>
                                                                        STR
                                                                    </label>
                                                                </th> */}

                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            {this.state.historyData == null ? <tr className="tableNoData"><td colSpan="9"> NO DATA FOUND </td></tr> : this.state.historyData.length == 0 ? <tr className="tableNoData"><td colSpan="9"> NO DATA FOUND </td></tr> : this.state.historyData.map((data, key) => (
                                                                <tr key={key}>
                                                                    <td className="fixed-side1 alignMiddle historyJobStatus">
                                                                        <ul className="list-inline">
                                                                            {/* <li>
                                                                            <p className="para_purchase">
                                                                                <input type="text" checked={this.state.isDescriptionChecked ? true : false} onClick={(e) => this.onDescription(e)} className="purchaseCheck" type="checkbox" id="c2" name="cb" />
                                                                                <label htmlFor="c2" className="purchaseLabel"></label>
                                                                            </p>
                                                                        </li> */}
                                                                            <li className="text-center width70">

                                                                                <svg className={data.totalCount > 0 ? "width30 colorCart" : "width30"} xmlns="http://www.w3.org/2000/svg" width="33" height="27" viewBox="0 0 33 27">
                                                                                    <g fill="#d2d2d2" fillRule="nonzero">
                                                                                        <path d="M16.288 16.549a5.866 5.866 0 0 1-.918-1.462H8.09v1.462h8.198zm-6.39 6.664c.585 0 1.059.464 1.059 1.038 0 .573-.474 1.038-1.06 1.038a1.048 1.048 0 0 1-1.058-1.038c0-.573.475-1.038 1.059-1.038zm6.257 1.038c0-.574.474-1.038 1.058-1.038.586 0 1.058.464 1.058 1.038 0 .576-.47 1.038-1.058 1.038a1.048 1.048 0 0 1-1.058-1.038zM5.57 13.483l9.358.005a5.803 5.803 0 0 1 0-1.456l-9.756-.005-.951-3.483H0v1.461h3.08l3.455 12.651h1.4a2.461 2.461 0 0 0-.586 1.595c0 1.38 1.14 2.5 2.549 2.5 1.408 0 2.55-1.12 2.55-2.5 0-.606-.22-1.162-.587-1.595h3.39a2.461 2.461 0 0 0-.586 1.595c0 1.377 1.144 2.5 2.549 2.5 1.406 0 2.549-1.122 2.549-2.5 0-.606-.22-1.162-.587-1.595h1.4l1.108-4.056a6.115 6.115 0 0 1-1.542.006l-.707 2.588H7.677L5.57 13.483zm12.944 4.659H8.657v1.462h9.857v-1.462zM32.526 6.697l-.359-.244a1.102 1.102 0 0 1-.434-1.22l.113-.413c.19-.639-.245-1.296-.906-1.409l-.434-.075a1.1 1.1 0 0 1-.925-.901l-.076-.432a1.118 1.118 0 0 0-1.416-.882l-.416.13c-.453.132-.963-.037-1.227-.412l-.264-.376A1.118 1.118 0 0 0 24.52.294l-.321.3c-.359.32-.869.395-1.303.151l-.378-.207a1.138 1.138 0 0 0-1.586.564l-.17.413c-.17.45-.623.732-1.095.695l-.434-.02a1.102 1.102 0 0 0-1.171 1.184l.038.432c.037.47-.246.92-.68 1.108l-.397.169a1.12 1.12 0 0 0-.547 1.577l.226.375a1.1 1.1 0 0 1-.132 1.296l-.283.32a1.092 1.092 0 0 0 .208 1.652l.358.244c.397.263.567.77.435 1.22l-.17.432c-.19.639.245 1.296.906 1.408l.434.076a1.1 1.1 0 0 1 .926.9l.075.433c.114.657.774 1.07 1.416.882l.416-.131c.453-.132.963.037 1.227.413l.265.357c.396.544 1.17.62 1.661.169l.321-.3c.36-.32.869-.395 1.303-.151l.378.207c.585.319 1.322.056 1.586-.564l.17-.413c.17-.45.623-.732 1.095-.695l.435.02a1.102 1.102 0 0 0 1.17-1.184l-.019-.432c-.037-.47.246-.92.68-1.108l.397-.169a1.12 1.12 0 0 0 .547-1.577l-.226-.375a1.1 1.1 0 0 1 .132-1.296l.283-.32a1.089 1.089 0 0 0-.17-1.652zm-7.34 3.625L23.558 11.9l-1.609-1.598L20.4 8.763l1.629-1.578 1.549 1.54L27.29 5.1 28.9 6.698l-3.714 3.624z" />
                                                                                    </g>
                                                                                </svg>

                                                                            </li>
                                                                            <li className="text-center width110">

                                                                                <button className={data.jobRunState == "RUNNING" ? "btnStatusTable cursorDefault" : data.jobRunState == "SUCCEEDED" ? "sucessStatusTable cursorDefault" : "failureStatusTable cursorDefault"}>
                                                                                    {data.jobRunState}
                                                                                </button>

                                                                            </li>
                                                                            <li className="text-center width110 " >
                                                                                {/* {console.log(data)} */}
                                                                                {/* <label>{data.jobRunState == "SUCCEEDED" ? <button type="button" onClick={() => this.historyDownload(`${data.jobId}`)} >Download</button> : <button type="button" className="btnDisabled" >Download</button>}</label> */}
                                                                                {/* <button type="button" className="alignMiddle" id={data.jobId} onClick={this.manageFilesDownload} >Download<svg className="m-lft-4" xmlns="http://www.w3.org/2000/svg" width="8" height="9" viewBox="0 0 8 13">
                                                                                    <path fill="#fff" fill-rule="evenodd" d="M0 11.433l4.945-4.955L0 1.522 1.522 0 8 6.478l-6.478 6.477L0 11.433" />
                                                                                </svg></button> */}
                                                                                {/* <a href={this.state.downloadDropDown == data.jobId ? csvLinkfile == null ? null : csvLinkfile == "" ? null : csvLinkfile : null} download> */}

                                                                                {/* {!this.state.popup  ? <span className={csvLinkfile == null ? "alignMiddle btnDisabled pointerNone" : csvLinkfile == "" ? "alignMiddle btnDisabled pointerNone" : "alignMiddle displayPointer"} id="singleUrl" onClick={(e) => this.manageFilesDownload(e)}>Download</span> */}
                                                                                {/* : */}
                                                                                {this.state.selectedJobId == data.jobId && this.state.dpLinks == 0 && this.state.csvLinkFile == null || data.jobRunState == "FAILED" || data.jobRunState=="STOPPED" ||data.jobRunState=="RUNNING" ? <button type="button" className="alignMiddle btnDisabled pointerNone">Download</button> : <button type="button" className="alignMiddle" id={data.jobId} onClick={this.manageFilesDownload}>Download</button>}
                                                                                {/* } */}
                                                                                {this.state.selectedJobId == data.jobId && this.state.popUpdrop && <div id="csvDropMain" ref={node => this.dropdopwn = node} className="csvDropDown assortmentSelect historyDownloadDropdown">
                                                                                    <div className="assortSelectDiv" >
                                                                                        <div id="csvDrop" className="dropDownItems m-top-5">
                                                                                            <h4>Available Downloads</h4>
                                                                                            <ul className="pad-0 m-top-15">
                                                                                                {this.state.dpLinks.length == 0 ? null : this.state.dpLinks.map((data, key) => (<li className="textLeft" key={key} onClick={() => window.open(data.value)}>{data.key} <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path fill="#000" fillRule="nonzero" d="M14 10.112V13.3a.7.7 0 0 1-.7.7H.7a.7.7 0 0 1-.7-.7v-3.188a.7.7 0 0 1 1.4 0V12.6h11.2v-2.488a.7.7 0 0 1 1.4 0zM7 0a.7.7 0 0 0-.7.7v7.021L3.964 5.385a.7.7 0 1 0-.99.99l3.531 3.531a.7.7 0 0 0 .99 0l3.53-3.53a.7.7 0 1 0-.99-.99L7.7 7.72V.7A.7.7 0 0 0 7 0z" /></svg></li>))}
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>}
                                                                                {/* </a> */}
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                    <td>
                                                                        <div className="topToolTip">
                                                                            <label>{data.jobId}</label>
                                                                            <span className="topToolTipText historyToolTip">{data.jobId}</span>
                                                                        </div>
                                                                    </td>
                                                                    <td><label>{data.startedOn}</label></td>

                                                                    <td>
                                                                        <label>{data.completeOn}</label>
                                                                    </td>
                                                                    <td><label>{data.duration==null?"0":data.duration} mins</label></td>
                                                                    <td>
                                                                        <label>{data.storeCount}</label>
                                                                    </td>

                                                                    <td>
                                                                        <label>{data.itemCount}</label>
                                                                    </td>
                                                                    <td>
                                                                        <label>{data.repDate}</label>
                                                                    </td>
                                                                    <td>
                                                                        <label>{data.totalCount}</label>
                                                                    </td>
                                                                    {/* <td>
                                                                    <label>{data.jobRunState == "SUCCEEDED" ? <button type="button" onClick={() => this.historyDownload(`${data.jobId}`)} >Download</button> : <button type="button" className="btnDisabled" >Download</button>}</label>
                                                                </td> */}
                                                                </tr>))}
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="pagerDiv">
                                        <ul className="list-inline pagination">
                                            <li >
                                                <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                                    First
                  </button>
                                            </li>
                                            <li>
                                                <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li>
                                            <li>
                                                <button className="PageFirstBtn pointerNone">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                                <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" disabled>
                                                        Next
                  </button>
                                                </li>}


                                            {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        {this.state.loader ? <FilterLoader /> : null}
                        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                    </div>
                </div>
                <Footer />
            </div>
        );

    }

}

export default HistoryReplenishment;
