import React, { Component } from 'react'
import ToastLoader from '../loaders/toastLoader';
import closeImg from '../../assets/close-recently.svg'
import searchImg from '../../assets/search.svg'
export default class ArticleFilterModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            articleData: [],
            selectedValue: this.props.articleCode,
            searchArticle: "",
            toastLoader: false,
            toastMsg: "",
            type: 1,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
        }
    }

    // ______________________________  RECEIVE PROPS ___________________________

    componentWillMount() {
        this.setState({
            selectedValue: this.props.articleCode,
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.getArticleFmcg.isSuccess) {
            if (nextProps.replenishment.getArticleFmcg.data.resource != null) {
                this.setState({
                    articleData: nextProps.replenishment.getArticleFmcg.data.resource,
                    prev: nextProps.replenishment.getArticleFmcg.data.prePage,
                    current: nextProps.replenishment.getArticleFmcg.data.currPage,
                    next: nextProps.replenishment.getArticleFmcg.data.currPage + 1,
                    maxPage: nextProps.replenishment.getArticleFmcg.data.maxPage,
                })
            } else {
                this.setState({
                    articleData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.replenishment.getArticleFmcg.data.prePage,
                current: this.props.replenishment.getArticleFmcg.data.currPage,
                next: this.props.replenishment.getArticleFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getArticleFmcg.data.maxPage,
            })
            if (this.props.replenishment.getArticleFmcg.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.getArticleFmcg.data.currPage - 1,
                    search: this.state.searchArticle,
                    motherComp: this.props.motherCompany.join('|')
                }
                this.props.getArticleFmcgRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.replenishment.getArticleFmcg.data.prePage,
                current: this.props.replenishment.getArticleFmcg.data.currPage,
                next: this.props.replenishment.getArticleFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getArticleFmcg.data.maxPage,
            })
            if (this.props.replenishment.getArticleFmcg.data.currPage != this.props.replenishment.getArticleFmcg.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.getArticleFmcg.data.currPage + 1,
                    search: this.state.searchArticle,
                    motherComp: this.props.motherCompany.join('|')
                }
                this.props.getArticleFmcgRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.replenishment.getArticleFmcg.data.prePage,
                current: this.props.replenishment.getArticleFmcg.data.currPage,
                next: this.props.replenishment.getArticleFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getArticleFmcg.data.maxPage,
            })
            if (this.props.replenishment.getArticleFmcg.data.currPage <= this.props.replenishment.getArticleFmcg.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.searchArticle,
                    motherComp: this.props.motherCompany.join('|')
                }
                this.props.getArticleFmcgRequest(data)
            }
        }
    }


    handleChange(e) {
        if (e.target.id == "searchArticle") {
            this.setState({
                searchArticle: e.target.value
            })
        }
    }
    searchArticle() {
        if (this.state.searchArticle == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
        } else {
            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.searchArticle,
                motherComp: this.props.motherCompany.join('|')
            }
            this.props.getArticleFmcgRequest(data)
        }
    }
    _handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.searchArticle()
        }
    }
    onClearSearch() {
        if (this.state.type == 3) {
            this.setState({
                searchArticle: "",
                type: 1,
                no: 1
            })
            var data = {
                type: 1,
                no: 1,
                search: "",
                motherComp: this.props.motherCompany.join('|')
            }
            this.props.getArticleFmcgRequest(data)
        } else {
            this.setState({
                searchArticle: "",
            })
        }
    }

    selectedValue(code) {
        let articleData = this.state.articleData
        let selectedValue = [...this.state.selectedValue]
        for (let i = 0; i < articleData.length; i++) {
            if (articleData[i].articleCode == code) {
                if (selectedValue.includes(articleData[i].articleCode)) {

                    var index = selectedValue.indexOf(articleData[i].articleCode);
                    if (index > -1) {
                        selectedValue.splice(index, 1)
                        this.setState({
                            selectedValue: selectedValue
                        })

                    }
                } else {
                    selectedValue.push(articleData[i].articleCode)
                    this.setState({
                        selectedValue: selectedValue
                    })
                }
            }
        }

    }

    onSaveArticle() {
        if (this.state.selectedValue == "") {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            let articleData = this.state.articleData
            for (let i = 0; i < articleData.length; i++) {
                if (this.state.selectedValue.includes(articleData[i].articleCode)) {
                    let data = {
                        Name: articleData[i].articleName,
                        Code: articleData[i].articleCode
                    }
                    this.props.updateArticle(data)
                }
            }
        }
    }
    closeArticleModal() {
        this.setState({
            selectedValue: []
        })
        this.props.closeArticleModal()
    }

    selectall(e) {
        let articleData = this.state.articleData
        let selectedValue = [...this.state.selectedValue]
        for (let i = 0; i < articleData.length; i++) {
            if (!selectedValue.includes(articleData[i].articleCode)) {
                selectedValue.push(articleData[i].articleCode)
            }
        }
        this.setState({
            selectedValue: selectedValue,
        })
        document.getElementById("selectAll").checked = true
    }
    deselectall(e) {
        this.setState({
            selectedValue: []
        })
        document.getElementById("deselectAll").checked = true
    }

    render() {
        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content modalShow pad-0">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle">
                                    <div className="col-md-6 pad-0">
                                        <h2>Article</h2>
                                    </div>
                                    <div className="col-md-6 pad-0">
                                        <div className="modalHandlers">
                                            <button type="button" className="" onClick={(e) => this.onSaveArticle(e)}>Done</button>

                                            <button type="button" onClick={(e) => this.closeArticleModal(e)}>Close</button>
                                        </div>
                                    </div>
                                </div>

                                <div className="modalSearch">
                                    <div className="col-md-6"></div>
                                    <div className="col-md-6">
                                        <div className="dropdown searchStoreProfileMain">
                                            <div className="searchBtnClick">
                                                <input type="search" className="search-box" onKeyDown={this._handleKeyDown} onChange={e => this.handleChange(e)} value={this.state.searchArticle} id="searchArticle" placeholder="Type to search" />
                                                <button className="m0" onClick={(e) => this.searchArticle(e)}><img src={searchImg} /></button>
                                                {this.state.searchArticle == "" ? null : <img src={closeImg} className="clearImg" onClick={(e) => this.onClearSearch(e)} />}
                                            </div>
                                            {/* {this.state.type == 3 ? <span onClick={(e) => this.onClearSearch(e)} className="clearSearchFilter">
                                                    Clear Search Filter
                                                </span> : null} */}
                                        </div>
                                    </div>
                                </div>
                                <div className="modalContentMid">
                                    <div className="col-md-12 col-sm-12 pad-0 modalTableNew">
                                        <div className="modal_table fmcgFilterTable">
                                            <table className="table tableModal">
                                                <thead>
                                                    <tr>
                                                        <th className="zIndex2"><label>Select</label></th>
                                                        <th className="zIndex2"><label>Article Name</label></th>
                                                        <th className="zIndex2"><label>Article Code</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.articleData.length != 0 ? this.state.articleData.map((data, key) => (
                                                        <tr key={key}>
                                                            <td className="checkBoxSquare" >
                                                                <div className="checkBoxRowClick"><label className="checkBoxLabel0 displayPointer"><input type="checkBox" id={data.articleCode} checked={this.state.selectedValue.includes(`${data.articleCode}`)} onChange={(e) => this.selectedValue(`${data.articleCode}`)} className="checkBoxTab" /> <span className="checkmark1"></span> </label></div>
                                                            </td>
                                                            <td className="fontWeig600">{data.articleName}</td>
                                                            <td>{data.articleCode}</td>
                                                        </tr>
                                                    )) : <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr>}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline m-top-10 modal-select">
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">SELECT ALL
                                            <input type="radio" name="colorRadio" id="selectAll" onKeyDown={(e) => this.selectallKeyDown(e)} onClick={(e) => this.selectall(e)} />
                                                <span className="checkradio-select select_all"></span>
                                            </label>
                                        </li>
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">DESELECT ALL
                                            <input type="radio" name="colorRadio" id="deselectAll" onKeyDown={(e) => this.deselectallKeyDown(e)} onClick={(e) => this.deselectall(e)} />
                                                <span className="checkradio-select deselect_all"></span>
                                            </label>
                                        </li>
                                        <li className="float_right">

                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                                                    </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                                                    </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li>}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
            </div>
        )
    }
}
