import React from 'react';
import CreateNewConfiguration from './createNewConfiguration';
import ManageConfiguration from './manageConfiguration';
import Pagination from '../pagination';
import ToastLoader from '../loaders/toastLoader';
import ConfirmationSummaryModal from '../replenishment/confirmationReset';
import RequestError from '../../components/loaders/requestError';
import RequestSuccess from '../../components/loaders/requestSuccess';
import FilterLoader from '../../components/loaders/filterLoader';

class AutoConfigNew extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            createConfiguration: false,
            editConfiguration: false,
            tableHeaders: {},
            allJobData: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            totalCount: 0,
            jumpPage: 1,

            toastMsg: "",
            toastLoader: false,
            confirmModal: false,
            headerMsg: "",
            deleteId: "",

            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",

            editConfigurationData: {},
        }
    }

    componentDidMount(){
        this.props.getHeaderConfigRequest({
            enterpriseName: "TURNINGCLOUD",
            attributeType: "TABLE HEADER",
            displayName: "REPLENISHMENT_SCHEDULER_AUTO_CONFIG",
        });
        let payload ={
            type: 1,
            pageNo: 1
        }
        this.props.getFiveTriggersRequest(payload);

        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);

        sessionStorage.setItem('currentPage', "RDAREPSCHMAIN")
        sessionStorage.setItem('currentPageName', "Auto-Config")
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    static getDerivedStateFromProps(nextProps, preState){
        if (nextProps.replenishment.getFiveTriggers.isSuccess) {
            return{
                allJobData: nextProps.replenishment.getFiveTriggers.data.resource == null ? [] : nextProps.replenishment.getFiveTriggers.data.resource.jobTriggers !== null 
                            && nextProps.replenishment.getFiveTriggers.data.resource.jobTriggers !== undefined ? nextProps.replenishment.getFiveTriggers.data.resource.jobTriggers : [],
                prev: nextProps.replenishment.getFiveTriggers.data.prePage,
                current: nextProps.replenishment.getFiveTriggers.data.currPage,
                next: nextProps.replenishment.getFiveTriggers.data.currPage + 1,
                maxPage: nextProps.replenishment.getFiveTriggers.data.maxPage,
                totalCount: nextProps.replenishment.getFiveTriggers.data.totalCount == undefined || nextProps.replenishment.getFiveTriggers.data.totalCount == null 
                               ? 0 : nextProps.replenishment.getFiveTriggers.data.totalCount,
                jumpPage: nextProps.replenishment.getFiveTriggers.data.currPage
            }
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            return{
                loader: false,
                alert: false,
                success: false,
                tableHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]
            }
        }
        else if (nextProps.replenishment.getHeaderConfig.isError) {
            return{
                loader: false,
                alert: true,
                success: false,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage,
                tableHeaders: {}
            }
        }
        return null
    }

    componentDidUpdate(){

        if (this.props.replenishment.getFiveTriggers.isSuccess) {
            this.setState({
                loader:false,
            })
            this.props.getFiveTriggersClear()
    
        } else if (this.props.replenishment.getFiveTriggers.isError) {
            this.setState({
                errorMessage: this.props.replenishment.getFiveTriggers.message.error == undefined ? undefined : this.props.replenishment.getFiveTriggers.message.error.errorMessage,
                loader: false,
                alert: true,
                code: this.props.replenishment.getFiveTriggers.message.status,
                errorCode: this.props.replenishment.getFiveTriggers.message.error == undefined ? undefined : this.props.replenishment.getFiveTriggers.message.error.errorCode
            })
            this.props.getFiveTriggersClear();
        }
    
        if (this.props.replenishment.getAllJob.isSuccess) {
            this.setState({
                loader:false,
            })
            this.props.getAllJobClear()
    
        }else if(this.props.replenishment.getAllJob.isError){
            this.setState({
                errorMessage: this.props.replenishment.getAllJob.message.error == undefined ? undefined : this.props.replenishment.getAllJob.message.error.errorMessage,
                loader: false,
                alert: true,
                code: this.props.replenishment.getAllJob.message.status,
                errorCode: this.props.replenishment.getAllJob.message.error == undefined ? undefined : this.props.replenishment.getAllJob.message.error.errorCode
            })
            this.props.getAllJobClear();
        }
    
        if (this.props.replenishment.getJobData.isSuccess) {
            this.setState({
                loader:false,
            })
            this.props.getJobDataClear()
    
        }else if(this.props.replenishment.getJobData.isError){
            this.setState({
                errorMessage: this.props.replenishment.getJobData.message.error == undefined ? undefined : this.props.replenishment.getJobData.message.error.errorMessage,
                loader: false,
                alert: true,
                code: this.props.replenishment.getJobData.message.status,
                errorCode: this.props.replenishment.getJobData.message.error == undefined ? undefined : this.props.replenishment.getJobData.message.error.errorCode
            })
            this.props.getJobDataClear();
        }
        if (this.props.replenishment.createTrigger.isSuccess) {
            this.setState({
                successMessage: this.props.replenishment.createTrigger.data.message,
                loader: false,
                success: true,
                editConfiguration: false,
                createConfiguration: false
            })
            this.props.createTriggerClear();
    
        }else if(this.props.replenishment.createTrigger.isError){
            this.setState({
                errorMessage: this.props.replenishment.createTrigger.message.error == undefined ? undefined : this.props.replenishment.createTrigger.message.error.errorMessage,
                loader: false,
                alert: true,
                code: this.props.replenishment.createTrigger.message.status,
                errorCode: this.props.replenishment.createTrigger.message.error == undefined ? undefined : this.props.replenishment.createTrigger.message.error.errorCode
            });
            this.props.createTriggerClear();
        }
    
        if (this.props.replenishment.deleteTrigger.isSuccess) {
            this.setState({
                successMessage: this.props.replenishment.deleteTrigger.data.message,
                loader: false,
                success: true,
                confirmModal: false,
            })
            // this.props.deleteTriggerClear()
    
        }else if(this.props.replenishment.deleteTrigger.isError){
            this.setState({
                errorMessage: this.props.replenishment.deleteTrigger.message.error == undefined ? undefined : this.props.replenishment.deleteTrigger.message.error.errorMessage,
                loader: false,
                alert: true,
                code: this.props.replenishment.deleteTrigger.message.status,
                errorCode: this.props.replenishment.deleteTrigger.message.error == undefined ? undefined : this.props.replenishment.deleteTrigger.message.error.errorCode
            })
            this.props.deleteTriggerClear();
        }
    
        if (!this.state.loader && (this.props.replenishment.getFiveTriggers.isLoading || this.props.replenishment.getAllJob.isLoading
            || this.props.replenishment.getJobData.isLoading  || this.props.replenishment.createTrigger.isLoading
            || this.props.replenishment.deleteTrigger.isLoading || this.props.replenishment.getHeaderConfig.isLoading)) {
            this.setState({ loader: true })
        }
        if (this.props.replenishment.deleteTrigger.isSuccess) {
            let payload ={
                type: 1,
                pageNo: 1
            }
            this.props.getFiveTriggersRequest(payload);
            this.props.deleteTriggerClear();
        }

        if (this.props.replenishment.getHeaderConfig.isSuccess || this.props.replenishment.getHeaderConfig.isError) {
            this.props.getHeaderConfigClear();
        }
    }

    onError =(e)=> {
        this.setState({ alert: false });
    }

    onRequest =(e)=> {
        this.setState({ success: false });
        let payload ={
            type: 1,
            pageNo: 1
        }
        this.props.getFiveTriggersRequest(payload);
    }

    openCreateConfiguration = (e) => {
        e.preventDefault();
        this.setState({
            createConfiguration: !this.state.createConfiguration
        });
    }
    closeCreateConfiguration = (e) => {
        this.setState({
            createConfiguration: false,
        })
    }
    openEditConfiguration(e, data) {
        let payload = {
            
        }
        this.setState({
            editConfigurationData: {
                id: data.id,
                jobName: data.jobName,
                cronExpression: data.cronExpression,
                active: data.active,
                frequency: data.frequency,
                manageRuleId: data.manageRuleId
            },
            editConfiguration: !this.state.editConfiguration
        },()=>{
            //Write Update API Here::
            //this.props.createTriggerRequest(data.id)
        });
    }

    closeEditConfiguration = (e) => {
        this.setState({
            editConfiguration: false,
        })
    }

    getAnyPage = (_) => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload ={
                        type: 1,
                        pageNo: _.target.value,
                    }
                    this.props.getFiveTriggersRequest(payload);
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 5000);
                }
            }

        }
    }

    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {

                this.setState({
                    prev: this.props.replenishment.getFiveTriggers.data.prePage,
                    current: this.props.replenishment.getFiveTriggers.data.currPage,
                    next: this.props.replenishment.getFiveTriggers.data.currPage + 1,
                    maxPage: this.props.replenishment.getFiveTriggers.data.maxPage,
                })
                if (this.props.replenishment.getFiveTriggers.data.currPage != 0) {
                    let payload ={
                        type: 1,
                        pageNo: this.props.replenishment.getFiveTriggers.data.currPage - 1,
                    }
                    this.props.getFiveTriggersRequest(payload);
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.replenishment.getFiveTriggers.data.prePage,
                current: this.props.replenishment.getFiveTriggers.data.currPage,
                next: this.props.replenishment.getFiveTriggers.data.currPage + 1,
                maxPage: this.props.replenishment.getFiveTriggers.data.maxPage,
            })
            if (this.props.replenishment.getFiveTriggers.data.currPage != this.props.replenishment.getFiveTriggers.data.maxPage) {
                let payload ={
                    type: 1,
                    pageNo: this.props.replenishment.getFiveTriggers.data.currPage + 1,
                }
                this.props.getFiveTriggersRequest(payload);
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            }
            else {
                this.setState({
                    prev: this.props.replenishment.getFiveTriggers.data.prePage,
                    current: this.props.replenishment.getFiveTriggers.data.currPage,
                    next: this.props.replenishment.getFiveTriggers.data.currPage + 1,
                    maxPage: this.props.replenishment.getFiveTriggers.data.maxPage,
                })
                if (this.props.replenishment.getFiveTriggers.data.currPage <= this.props.replenishment.getFiveTriggers.data.maxPage) {
                    let payload ={
                        type: 1,
                        pageNo: 1,
                    }
                    this.props.getFiveTriggersRequest(payload);
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.replenishment.getFiveTriggers.data.prePage,
                    current: this.props.replenishment.getFiveTriggers.data.currPage,
                    next: this.props.replenishment.getFiveTriggers.data.currPage + 1,
                    maxPage: this.props.replenishment.getFiveTriggers.data.maxPage,
                })
                if (this.props.replenishment.getFiveTriggers.data.currPage <= this.props.replenishment.getFiveTriggers.data.maxPage) {
                    let payload ={
                        type: 1,
                        pageNo: this.props.replenishment.getFiveTriggers.data.maxPage,
                    }
                    this.props.getFiveTriggersRequest(payload);
                }
            }
        }
    }
    
    deleteConfirmModal =(e, data)=> {
        this.setState({
            headerMsg: "Are you sure you want to delete",
            confirmModal: true,
            deleteId: data.id,
        })
    }

    closeConfirmModal =(e)=> {
        this.setState({ confirmModal: !this.state.confirmModal })
    }

    resetColumn =()=> {
        let payload={
            id: this.state.deleteId
        }
        this.props.deleteTriggerRequest(payload)
    }

    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
            this.setState({ createConfiguration: false, editConfiguration: false, })
        }
    }

    render() {
        return(
            <div className="container-fluid pad-l50">
                <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                    <div className="auto-confing-head">
                        <div className="ach-left">
                            <div className="achl-auto-text">
                                <h3>Auto Configuration</h3>
                                <p>All Configured & Running Triggers</p>
                            </div>
                            {/* <div className="global-search-tab gcl-tab">
                                <ul className="nav nav-tabs gst-inner" role="tablist">
                                    <li className="nav-item active" >
                                        <a className="nav-link gsti-btn" href="#searchpages" role="tab" data-toggle="tab">Configured Jobs</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link gsti-btn" href="#useractivities" role="tab" data-toggle="tab">Active Jobs</a>
                                    </li>
                                </ul>
                            </div> */}
                        </div>
                        <div className="ach-right">
                            <button type="button" className="new-trigger" onClick={(e) => this.openCreateConfiguration(e)}>
                                <svg xmlns="http://www.w3.org/2000/svg" id="plus_4_" width="16" height="16" viewBox="0 0 19.846 19.846">
                                    <g id="Group_3035">
                                        <g id="Group_3034">
                                            <path fill="#51aa77" id="Path_948" d="M9.923 0a9.923 9.923 0 1 0 9.923 9.923A9.934 9.934 0 0 0 9.923 0zm0 18.308a8.386 8.386 0 1 1 8.386-8.386 8.4 8.4 0 0 1-8.386 8.386z" className="cls-1"/>
                                        </g>
                                    </g>
                                    <g id="Group_3037" transform="translate(5.311 5.242)">
                                        <g id="Group_3036">
                                            <path fill="#51aa77" id="Path_949" d="M145.477 139.081H142.4v-3.074a.769.769 0 0 0-1.537 0v3.074h-3.074a.769.769 0 0 0 0 1.537h3.074v3.074a.769.769 0 1 0 1.537 0v-3.074h3.074a.769.769 0 0 0 0-1.537z" className="cls-1" transform="translate(-137.022 -135.238)"/>
                                        </g>
                                    </g>
                                </svg>
                                New Trigger</button>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 p-lr-47">
                    <div className="vendor-gen-table" >
                        <div className="manage-table">
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn"></th>
                                        {
                                            Object.keys(this.state.tableHeaders).map((key) => <th><label>{this.state.tableHeaders[key]}</label><img src={require('../../assets/headerFilter.svg')} /></th>)    
                                        }
                                        {/* <th><label>Job Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Current Date/Time</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Start Date/Time</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Next Scheduled</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Job Status</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Frequency</label><img src={require('../../assets/headerFilter.svg')} /></th> */}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.allJobData.length == 0 || Object.keys(this.state.tableHeaders).length == 0 ? <tr><td><label>NO DATA FOUND!</label></td></tr> : this.state.allJobData.map( (data,key) => <tr key={key}>
                                        <td className="fix-action-btn manage-role-fix">
                                            <ul className="table-item-list">
                                                <li key={key} className="til-inner til-edit-btn" onClick={(e) => this.openEditConfiguration(e, data)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                    </svg>
                                                </li>
                                                <li key={key+"1"} className="til-inner til-delete-btn" onClick={(e) => this.deleteConfirmModal(e, data)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        {
                                            Object.keys(this.state.tableHeaders).map((headerKey) => 
                                            headerKey == "status" ? <td><label className="bold">{data.status}</label><img className="acp-running" src={require('../../assets/wall-clock.svg')} /></td> :
                                            <td><label>{data[headerKey]}</label></td>)
                                        }
                                        {/* <td><label>{data.jobName}</label></td>
                                        <td><label>{data.createdOn}</label></td>
                                        <td><label>{data.startDateTime}</label></td>
                                        <td><label>{data.nextSchedule}</label></td>
                                        <td><label className="bold">{data.status}</label><img className="acp-running" src={require('../../assets/wall-clock.svg')} /></td>
                                        <td><label>{data.frequency}</label></td> */}
                                    </tr>)}
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalCount}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                       <Pagination {...this.state} page={this.page} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.createConfiguration && <CreateNewConfiguration {...this.state} {...this.props} closeCreateConfiguration={this.closeCreateConfiguration} />}
                {this.state.editConfiguration && <ManageConfiguration data={this.state.editConfigurationData} closeEditConfiguration={this.closeEditConfiguration} saveConfiguration={this.props.createTriggerRequest} />}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
            </div>
        )
    }
}
export default AutoConfigNew;