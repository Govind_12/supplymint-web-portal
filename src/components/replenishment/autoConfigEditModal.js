import React, { useState } from 'react';

class AutoConfigEditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            timeDrop: false,
            editDate: "",
            editTime: ""
        }
    }
    contactSubmit() {
        let data = {
            editDate: this.state.editDate,
            editTime: this.state.editTime
        }
        this.props.editConfiguration(data)
    }
    OnDeleteConf(e) {
        this.props.deleteConfiguration()
    }

    componentWillMount() {

        for (let i = 0; i < this.props.replenishment.getFiveTriggers.data.resource.jobTriggers.length; i++) {
            if (this.props.replenishment.getFiveTriggers.data.resource.jobTriggers[i].triggerName == this.props.editTriggerName) {
                this.setState({
                    editTime: this.props.replenishment.getFiveTriggers.data.resource.jobTriggers[i].schedule.slice(12, 17),
                    editDate: this.props.replenishment.getFiveTriggers.data.resource.jobTriggers[i].schedule.slice(0, 11)
                })
            }
        }
    }

    handleChange(e) {
        if (e.target.id == "editTime") {
            e.target.placeholder = e.target.value
            this.setState({
                editTime: e.target.value
            });
        } else if (e.target.id == "editDate") {
            this.setState({
                editDate: e.target.value
            })
        }
    }

    render() {
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>
                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain autoConfigInventory customDateMain pad-0">
                        <div className="col-md-12 pad-0 m-top-20">
                            <div className="save-current-data posRelative">
                                <div className="col-md-12 m0">
                                    <div className="modal-header pad-lft-0">
                                        <h2 className="fontBold">Edit Configuration</h2>
                                    </div>
                                    <div className="col-md-12 customDate m-top-20 pad-0">
                                    <div className="col-md-6">
                                        <label>Job Name</label>
                                        <span>{this.props.editjobName}</span>
                                    </div>
                                    <div className="col-md-6 ">
                                        <label>Trigger Name</label>
                                        <span>{this.props.editTriggerName}</span>
                                    </div>
                                    

                                        <div className="col-md-5 pad-lft-0">
                                            <h5>Choose Time</h5>
                                            <input className="inputTime inputTimer selectWidth" type="time" id="editTime" placeholder={this.state.editTime == "" ? "Choose Time" : this.state.editTime} value={this.state.editTime} onChange={(e) => this.handleChange(e)} />

                                            {/* <input type="text" onClick={()=>this.setState({timeDrop:!this.state.timeDrop})} className="inputTime" placeholder={this.props.editTime!=""?this.props.editTime:"Choose Time"} readOnly  value={this.props.editTime}/>
                                        {this.state.timeDrop ? <div className="dropdownTime">
                                            <div className="col-md-12 timer">
                                                <div className="col-md-4 hour">
                                                    <svg className="topArrow displayPointer" xmlns="http://www.w3.org/2000/svg" width="13" height="8" viewBox="0 0 13 8">
                                                        <path fill="#8B8B8B" fillRule="evenodd" d="M1.522 8l4.956-4.945L11.433 8l1.522-1.522L6.478 0 0 6.478 1.522 8" />
                                                    </svg>
                                                    <ul className="list-inline">
                                                        <li>06</li>
                                                        <li>07</li>
                                                        <li>08</li>
                                                        <li>09</li>
                                                        <li>10</li>
                                                        <li>11</li>
                                                        <li>12</li>
                                                    </ul>
                                                    <svg className="bottomArrow displayPointer" xmlns="http://www.w3.org/2000/svg" width="13" height="8" viewBox="0 0 13 8">
                                                        <path fill="#8B8B8B" fillRule="evenodd" d="M1.522 0l4.956 4.945L11.433 0l1.522 1.522L6.478 8 0 1.522 1.522 0" />
                                                    </svg>
                                                </div>
                                                <div className="col-md-4 minute">
                                                    <svg className="topArrow displayPointer" xmlns="http://www.w3.org/2000/svg" width="13" height="8" viewBox="0 0 13 8">
                                                        <path fill="#8B8B8B" fillRule="evenodd" d="M1.522 8l4.956-4.945L11.433 8l1.522-1.522L6.478 0 0 6.478 1.522 8" />
                                                    </svg>
                                                    <ul className="list-inline">
                                                        <li>42</li>
                                                        <li>43</li>
                                                        <li>44</li>
                                                        <li>45</li>
                                                        <li>46</li>
                                                        <li>47</li>
                                                        <li>48</li>
                                                    </ul>
                                                    <svg className="bottomArrow displayPointer" xmlns="http://www.w3.org/2000/svg" width="13" height="8" viewBox="0 0 13 8">
                                                        <path fill="#8B8B8B" fillRule="evenodd" d="M1.522 0l4.956 4.945L11.433 0l1.522 1.522L6.478 8 0 1.522 1.522 0" />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div> : null} */}
                                        </div>
                                        <div className="col-md-5 pad-lft-0">
                                            <h5 className="purchaseLabel">Date</h5>
                                            <input type="date" id="editDate" className="dateGeneric" placeholder={this.state.editDate != "" ? this.state.editDate : "Select Date"} min={this.props.min} value={this.state.editDate} onChange={(e) => this.handleChange(e)} />
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <div className="col-md-6 textLeft pad-lft-0">
                                            <button type="button" className="deleteCongig" onClick={(e) => this.OnDeleteConf(e)}>Delete Configuration</button>
                                        </div>
                                        <div className="col-md-6 pad-right-0">
                                            <button className="clearBtn" type="button" onClick={(e) => this.props.hide(e)}>Cancel</button>
                                            <button className="saveBtn" type="button" onClick={(e) => this.contactSubmit(e)} >Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        );

    }

}
export default AutoConfigEditModal;