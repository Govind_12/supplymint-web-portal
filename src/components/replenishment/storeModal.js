import React, { Component } from 'react'
import ToastLoader from '../loaders/toastLoader';
import closeImg from '../../assets/close-recently.svg'
import searchImg from '../../assets/search.svg'

export default class StoreModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchStore: "",
            storeData: [],
            selectedValue: this.props.storeCodeUpdate,
            selectedName:this.props.storeUpdatesName,
            toastLoader: false,
            toastMsg: "",
            type: 1,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            viewAllStoresData: false
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.getStoreFmcg.isSuccess) {
            if (nextProps.replenishment.getStoreFmcg.data.resource != null) {
                this.setState({
                    storeData: nextProps.replenishment.getStoreFmcg.data.resource,
                    prev: nextProps.replenishment.getStoreFmcg.data.prePage,
                    current: nextProps.replenishment.getStoreFmcg.data.currPage,
                    next: nextProps.replenishment.getStoreFmcg.data.currPage + 1,
                    maxPage: nextProps.replenishment.getStoreFmcg.data.maxPage,
                })
            } else {
                this.setState({
                    storeData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.replenishment.getStoreFmcg.data.prePage,
                current: this.props.replenishment.getStoreFmcg.data.currPage,
                next: this.props.replenishment.getStoreFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getStoreFmcg.data.maxPage,
            })
            if (this.props.replenishment.getStoreFmcg.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.getStoreFmcg.data.currPage - 1,
                    search: this.state.searchStore,
                    motherComp: this.props.motherCompany.join('|'),
                    articleCode: this.props.articleCode.join('|'),
                    vendorCode: this.props.vendorCode.join('|')
                }
                this.props.getStoreFmcgRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.replenishment.getStoreFmcg.data.prePage,
                current: this.props.replenishment.getStoreFmcg.data.currPage,
                next: this.props.replenishment.getStoreFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getStoreFmcg.data.maxPage,
            })
            if (this.props.replenishment.getStoreFmcg.data.currPage != this.props.replenishment.getStoreFmcg.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.getStoreFmcg.data.currPage + 1,
                    search: this.state.searchStore,
                    motherComp: this.props.motherCompany.join('|'),
                    articleCode: this.props.articleCode.join('|'),
                    vendorCode: this.props.vendorCode.join('|')
                }
                this.props.getStoreFmcgRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.replenishment.getStoreFmcg.data.prePage,
                current: this.props.replenishment.getStoreFmcg.data.currPage,
                next: this.props.replenishment.getStoreFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getStoreFmcg.data.maxPage,
            })
            if (this.props.replenishment.getStoreFmcg.data.currPage <= this.props.replenishment.getStoreFmcg.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.searchStore,
                    motherComp: this.props.motherCompany.join('|'),
                    articleCode: this.props.articleCode.join('|'),
                    vendorCode: this.props.vendorCode.join('|')
                }
                this.props.getStoreFmcgRequest(data)
            }
        }
    }
handleChange(e){
 this.setState({
            searchStore: e.target.value
        })
}


    searchStore(e) {
       
        if (e.keyCode == 13) {
            if (this.state.searchStore == "") {
                this.setState({
                    toastMsg: "Enter text on search input ",
                    toastLoader: true
                })
                const t = this;
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                })
            } else {
                this.setState({
                    type: 3,
                })
                let data = {
                    type: 3,
                    no: 1,
                    search: this.state.searchStore,
                    motherComp: this.props.motherCompany.join('|'),
                    articleCode: this.props.articleCode.join('|'),
                    vendorCode: this.props.vendorCode.join('|')
                }
                this.props.getStoreFmcgRequest(data)
            }
        }
    }

    onClearSearch() {
        if (this.state.type == 3) {
            this.setState({
                searchStore: "",
                type: 1,
                no: 1
            })
            let data = {
                type: 1,
                no: 1,
                search: "",
                motherComp: this.props.motherCompany.join('|'),
                articleCode: this.props.articleCode.join('|'),
                vendorCode: this.props.vendorCode.join('|')
            }
            this.props.getStoreFmcgRequest(data)
        } else {
            this.setState({
                searchStore: "",
            })
        }
    }

    selectedValue(code,name) {
        let storeData = this.state.storeData
        let selectedValue = [...this.state.selectedValue]
       let selectedName= [...this.state.selectedName]
        for (let i = 0; i < storeData.length; i++) {
            if (storeData[i].storeCode == code) {
                if (selectedValue.includes(storeData[i].storeCode)) {
                    var index = selectedValue.indexOf(storeData[i].storeCode);
                    if (index > -1) {
                        selectedValue.splice(index, 1)
                        this.setState({
                            selectedValue: selectedValue
                        })
                    }
                } else {
                    selectedValue.push(storeData[i].storeCode)
                    this.setState({
                        selectedValue: selectedValue
                    })
                }
            }
        }

           for (let i = 0; i < storeData.length; i++) {
            if (storeData[i].storeName == name) {
                if (selectedName.includes(storeData[i].storeName)) {
                    var index = selectedName.indexOf(storeData[i].storeName);
                    if (index > -1) {
                        selectedName.splice(index, 1)
                        this.setState({
                            selectedName: selectedName
                        })
                    }
                } else {
                    selectedName.push(storeData[i].storeName)
                    this.setState({
                        selectedName: selectedName
                    })
                }
            }
        }
         document.getElementById("deselectAll").checked = false
    }

    onSaveStore() {
        if (this.state.selectedValue == "") {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            let storeData = this.state.storeData
       
       
                    let data = {
                        Name: this.state.selectedName,
                        Code: this.state.selectedValue
                    }
                    this.props.updateStore(data)
             
        }
    }

    selectall(e) {
        let storeData = this.state.storeData
        let selectedValue = [...this.state.selectedValue]
         let selectedName= [...this.state.selectedName]
        for (let i = 0; i < storeData.length; i++) {
            if (!selectedValue.includes(storeData[i].storeCode)) {
                selectedValue.push(storeData[i].storeCode)
            }
        }
         for (let i = 0; i < storeData.length; i++) {
            if (!selectedName.includes(storeData[i].storeName)) {
                selectedName.push(storeData[i].storeName)
            }
        }
        this.setState({
            selectedValue: selectedValue,
            selectedName:selectedName
        })
        document.getElementById("selectAll").checked = true
    }
    deselectall(e) {
        this.setState({
            selectedValue: [],
            selectedName:[]
        })
        document.getElementById("deselectAll").checked = true
    }

    viewAllStores() {
        this.setState({
            viewAllStoresData: true
        })
    }

    render() {
        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content modalShow pad-0">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle">
                                    <div className="col-md-5 pad-0">
                                        <h2>Choose Stores from the below list:</h2>
                                    </div>
                                    <div className="col-md-7 pad-0">
                                        <div className="modalHandlers">
                                            <button type="button" className="" onClick={(e) => this.viewAllStores(e)}>View Selected Stores</button>
                                            <button type="button" className="doneButton" onClick={(e) => this.onSaveStore(e)}>Done</button>
                                            <button type="button" onClick={(e) => this.props.closeStoreModal(e)}>Close</button>
                                        </div>
                                    </div>
                                </div>

                                <div className="modalSearch">
                                    <div className="col-md-6"></div>
                                    <div className="col-md-6">
                                        <div className="dropdown searchStoreProfileMain">
                                            <div className="searchBtnClick">
                                                <input type="search" className="search-box" onKeyDown={(e)=>this.searchStore(e)} onChange={(e) => this.handleChange(e)} value={this.state.searchStore} id="searchStore" placeholder="Type to search" />
                                                <button className="m0" onClick={(e) => this.searchStore(e)} ><img src={searchImg} /></button>
                                                {this.state.searchStore == "" ? null : <img src={closeImg} className="clearImg" onClick={(e) => this.onClearSearch(e)} />}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modalContentMid">
                                    <div className="col-md-12 col-sm-12 pad-0 modalTableNew">
                                        <div className="modal_table fmcgFilterTable tableHeadFixed">
                                            <table className="table tableModal">
                                                <thead>
                                                    <tr>
                                                        <th className="zIndex2"><label>Select</label></th>
                                                        <th className="zIndex2"><label>Available Stores</label></th>
                                                        <th className="zIndex2"><label>Selected Stores</label></th>
                                                    </tr>
                                                </thead>
                                                {!this.state.viewAllStoresData ? <tbody> {this.state.storeData.length != 0 ? this.state.storeData.map((data, key) => (
                                                    <tr key={key}>
                                                        <td className="checkBoxSquare" >
                                                            <div className="checkBoxRowClick">
                                                                <label className="checkBoxLabel0 displayPointer"><input type="checkBox" id={data.storeCode} checked={this.state.selectedValue.includes(`${data.storeCode}`)} onChange={(e) => this.selectedValue(`${data.storeCode}` ,data.storeName)} className="checkBoxTab" /> <span className="checkmark1"></span> </label>
                                                            </div>
                                                        </td>
                                                        <td className="fontWeig600">{data.storeName + "-" + data.storeCode}</td>
                                                        <td>{this.state.selectedValue.includes(`${data.storeCode}`) ? data.storeName : ""}</td>
                                                    </tr>
                                                )) : <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr>}
                                                </tbody> : <tbody>
                                                        {this.state.storeData.length != 0 ? this.state.storeData.map((data, key) => (
                                                            this.state.selectedValue.includes(`${data.storeCode}`) ? <tr key={key}>
                                                                <td className="checkBoxSquare" >
                                                                    <div className="checkBoxRowClick">
                                                                        <label className="checkBoxLabel0 displayPointer"><input type="checkBox" id={data.storeCode} checked={this.state.selectedValue.includes(`${data.storeCode}`)} onChange={(e) => this.selectedValue(`${data.storeCode}` ,data.storeName)} className="checkBoxTab" /> <span className="checkmark1"></span> </label>
                                                                    </div>
                                                                </td>
                                                                <td className="fontWeig600">{this.state.selectedValue.includes(`${data.storeCode}`) && data.storeName + "-" + data.storeCode}</td>
                                                                <td>{this.state.selectedValue.includes(`${data.storeCode}`) ? data.storeName : ""}</td>
                                                            </tr>:null
                                                        )) : <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr>}
                                                    </tbody>}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline m-top-10 modal-select">
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">SELECT ALL
                                            <input type="radio" name="colorRadio" id="selectAll" onClick={(e) => this.selectall(e)} />
                                                <span className="checkradio-select select_all"></span>
                                            </label>
                                        </li>
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">DESELECT ALL
                                            <input type="radio" name="colorRadio" id="deselectAll" onClick={(e) => this.deselectall(e)} />
                                                <span className="checkradio-select deselect_all"></span>
                                            </label>
                                        </li>
                                        <li className="float_right">

                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                                                    </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                                                    </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li>}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
            </div>
        )
    }
}
