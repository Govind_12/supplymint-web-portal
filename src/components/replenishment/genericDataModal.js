import React from 'react';

class GenericDataModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sites: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            totalItems: "",
            checkedItems: []
            // sites: this.props.data.data === undefined ? [] : [...this.props.data.data.resource.searchResultNew],
            // prev: this.props.data.data === undefined ? "" : this.props.data.data.resource.previousPage,
            // current: this.props.data.data === undefined ? "" : this.props.data.data.resource.currPage,
            // next: this.props.data.data === undefined ? "" : this.props.data.data.resource.currPage + 1,
            // maxPage: this.props.data.data === undefined ? "" : this.props.data.data.resource.maxPage,
            // totalItems: this.props.data.data === undefined ? "" : this.props.data.data.resource.totalCount,
            // checkedItems: this.props.selectedItems == undefined ? [] : [...this.props.selectedItems]
        }
    }

    componentDidMount() {
        document.addEventListener("click", this.closeModal);
        document.addEventListener("keydown", this.closeModalEsc);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.data !== undefined) {
            this.setState({
                sites: [...nextProps.data.data.resource.searchResultNew],
                prev: nextProps.data.data.resource.previousPage,
                current: nextProps.data.data.resource.currPage,
                next: nextProps.data.data.resource.currPage + 1,
                maxPage: nextProps.data.data.resource.maxPage,
                totalItems: nextProps.data.data.resource.totalCount,
                checkedItems: nextProps.selectedItems == undefined ? [] : [...nextProps.selectedItems]
            });
        }
    }

    componentWillUnmount() {
        document.removeEventListener("click", this.closeModal);
        document.removeEventListener("keydown", this.closeModalEsc);
    }

    closeModal = (e) => {
        if (!(e.path.some((element) => element.id === "genericDataModal")) && e.target.id !== this.props.data.payload.key + "Focus") {
            this.props.close();
        }
    }

    closeModalEsc = (e) => {
        if(e.keyCode == 27) {
            this.props.close();
        }
    }

    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.data.data.resource.previousPage,
                    current: this.props.data.data.resource.currPage,
                    next: this.props.data.data.resource.currPage + 1,
                    maxPage: this.props.data.data.resource.maxPage
                });
                if (this.props.data.data.resource.previousPage != 0) {
                    if (this.props.data.payload.key === "name1") {
                        this.props.action({
                            entity: this.props.data.payload.entity,
                            key: "name1",
                            code: "site_code",
                            search: this.props.data.payload.search,
                            pageNo: this.props.data.data.resource.previousPage
                        });
                    }
                    else {
                        this.props.action({
                            entity: this.props.data.payload.entity,
                            key: this.props.data.payload.key,
                            search: this.props.data.payload.search,
                            pageNo: this.props.data.data.resource.previousPage
                        });
                    }
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.data.data.resource.previousPage,
                    current: this.props.data.data.resource.currPage,
                    next: this.props.data.data.resource.currPage + 1,
                    maxPage: this.props.data.data.resource.maxPage
                })
            if (this.props.data.data.resource.currPage != this.props.data.data.resource.maxPage) {
                if (this.props.data.payload.key === "name1") {
                    this.props.action({
                        entity: this.props.data.payload.entity,
                        key: "name1",
                        code: "site_code",
                        search: this.props.data.payload.search,
                        pageNo: this.props.data.data.resource.currPage + 1
                    });
                }
                else {
                    this.props.action({
                        entity: this.props.data.payload.entity,
                        key: this.props.data.payload.key,
                        search: this.props.data.payload.search,
                        pageNo: this.props.data.data.resource.currPage + 1
                    });
                }
            }
        }
    }

    handleChange = (item) => {
        let checkedItems = [...this.state.checkedItems];
        let index = checkedItems.indexOf(item);
        if (index === -1) {
            checkedItems.push(item);
        }
        else {
            checkedItems.splice(index, 1);
        }
        this.setState({checkedItems});
        this.props.select(this.props.data.payload.entity, this.props.data.payload.key, checkedItems);
    }

    render() {
        console.log("render props:", this.props);
        console.log("render state:", this.state);
        return (
            <div className="dropdown-menu-city1 gen-width-auto zi999" id="genericDataModal">
                <ul className="dropdown-menu-city-item">
                {
                    this.state.checkedItems.length === 0 ? null :
                    <div className="dmci-checked">
                    {
                        this.state.checkedItems.map((item) =>
                            <li>
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" checked={this.state.checkedItems.indexOf(item) !== -1} onChange={() => this.handleChange(item)} />
                                    <span className="checkmark1"></span>
                                    {item}
                                </label>
                            </li>
                        )
                    }
                    </div>
                }
                {
                    this.state.sites.length == 0 ?
                    <li>
                        <span className="vendor-details displayBlock">
                            <span className="vd-name div-col-1">No search data found!</span>
                        </span>
                    </li> :
                    this.state.sites.map((item) =>
                        this.state.checkedItems.indexOf(item[this.props.data.payload.key]) !== -1 ?
                        null :
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" checked={this.state.checkedItems.indexOf(item[this.props.data.payload.key]) !== -1} onChange={() => this.handleChange(item[this.props.data.payload.key])} />
                                <span className="checkmark1"></span>
                                {item.code === undefined ? item[this.props.data.payload.key] : item.code + " - " + item[this.props.data.payload.key]}
                            </label>
                        </li>
                    )
                }
                </ul>
                <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        {/* <button className="btn-close" type="button" id="btn-close" onClick={this.props.close}>Close</button> */}
                    </div>
                    <div className="page-next-prew-btn">
                        <button className="pnpb-prev" type="button" id="prev" onClick={(e) => this.page(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button>
                        <button className="pnpb-no" type="button" disabled>{this.state.current}/{this.state.maxPage}</button>
                        <button className="pnpb-next" type="button" id="next" onClick={(e) => this.page(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default GenericDataModal;