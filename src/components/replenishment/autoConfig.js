import React from 'react';
import runningjobIcon from '../../assets/live-job.svg';
import BraedCrumps from '../breadCrumps';
import SideBar from '../sidebar';
import editIcon from '../../assets/editIcon.svg';
import AutoConfigEditModal from './autoConfigEditModal';
import filter from '../../assets/filter-2.svg';
import exclaimIcon from '../../assets/exclain.svg';
import errorIcon from "../../assets/error_icon.svg";
import circleIcon from "../../assets/circleLoader.svg";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import { getDate } from '../../helper';
import Succeeded from "./rodsuccess";
import FilterModal from './filterModal';
import ToastLoader from '../loaders/toastLoader';

class AutoConfig extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nextScheduleObj:{},
            modalState:"",
             succeeded: false,
            jobRunState: "NA",
            runJobId: "",
            selectedJob: "",
            jobName: "",
            allTriggers: {},
            allTriggersValue: [],
            lastCreated: false,
            lastFreq: "",
            lastTime: "",
            lastDate: "",
            frequency: "",
            saveconfig: true,
            reschedule: false,
            frequencyerr: false,
            time: "12:45",
            timeerr: false,
            lastRun: "0 Hours ago",
            date: "",
            dateerr: false,
            status: "No Job in progress",
            nextScheduled: "NA",
            progress: "",
            timeTaken: "00",
            monthserr: false,
            dayserr: false,
            months: [
                { id: 1, name: "JAN", value: "JAN", checked: false },
                { id: 2, name: "FEB", value: "FEB", checked: false },
                { id: 3, name: "MAR", value: "MAR", checked: false },
                { id: 4, name: "APR", value: "APR", checked: false },
                { id: 5, name: "MAY", value: "MAY", checked: false },
                { id: 6, name: "JUN", value: "JUN", checked: false },
                { id: 7, name: "JULY", value: "JUL", checked: false },
                { id: 8, name: "AUG", value: "AUG", checked: false },
                { id: 9, name: "SEP", value: "SEP", checked: false },
                { id: 10, name: "OCT", value: "OCT", checked: false },
                { id: 11, name: "NOV", value: "NOV", checked: false },
                { id: 12, name: "DEC", value: "DEC", checked: false },
            ],
            days: [
                { id: 1, name: "SUN", value: "SUN", checked: false },
                { id: 2, name: "MON", value: "MON", checked: false },
                { id: 3, name: "TUE", value: "TUE", checked: false },
                { id: 4, name: "WED", value: "WED", checked: false },
                { id: 5, name: "THU", value: "THU", checked: false },
                { id: 6, name: "FRI", value: "FRI", checked: false },
                { id: 7, name: "SAT", value: "SAT", checked: false }
            ],
            month: false,
            week: false,
            monthSelected: [],
            daysSelected: [],
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",
            percentage: 0,
            min: getDate(),
            autoConfig: false,
            timeDrop: false,
            channel: "",
            partnerData: [],
            partner: "",
            zoneData: [],
            zone: "",
            openFilter: false,
            stores: "",
            hours: ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15",
                "16", "17", "18", "19", "20", "21", "22", "23"],
            mins: ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15",
                "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45",
                "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"],
            editjobName: "",
            editTriggerName: "",
            editFrequency: "",
            alljobNames: [],
            editMonth: "",
            editWeek: "",
            editChannel: "",
            editPartner: "",
            editZone: "",
            editGrade: "",
            editStoreCode: "",
            editDate: "",
            editTime: "",
            successState: false,
            timeSet: "",
            startTime: "",
            endTime: "",
            nextSchedule: "",
            editnextSchedule: "",
            runningjob: [],
            isEnabled: "false",
            // ___________________________filtermodal states_________________________________
            labelConfigFilter: [],
            configurationFilter: {},
            storeCodeList: [],
            selected: [],
            storeSearch: "",
            checkedValue: [],
            apply: false,
            filterStores: "",
            getConfigStore: [],
            filter: {},
            saveCol: [],
            toastLoader: false,
            toastMsg: "",
            configParam: ""

        };
    }
    hide() {
        this.setState({
            autoConfig: false
        })
    }
    show(jobName, triggerName, frequency, month, week, channel, partner, zone, grade, storeCode, nextSchedule) {
        this.setState({
            autoConfig: true,
            editjobName: jobName,
            editTriggerName: triggerName,
            editFrequency: frequency,
            editMonth: month,
            editWeek: week,
            editChannel: channel,
            editPartner: partner,
            editZone: zone,
            editGrade: grade,
            editStoreCode: storeCode,
            editnextSchedule: nextSchedule
        })
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
    }
    componentWillMount() {
       

        // this.props.nscheduleRequest(sessionStorage.getItem('partnerEnterpriseCode') + '-TRIGGER');
        this.props.getFiveTriggersRequest();
        this.props.getAllJobRequest('data');

    }

    componentWillReceiveProps(nextProps) {
 

        if (nextProps.replenishment.jobByName.isSuccess) {
         

            this.setState({
                lastRun: nextProps.replenishment.jobByName.data.resource == null ? "NA" : nextProps.replenishment.jobByName.data.resource.lastEngineRun,
                jobRunState: nextProps.replenishment.jobByName.data.resource == null ? "NA" : nextProps.replenishment.jobByName.data.resource.jobRunState,
            })

        } else if (nextProps.replenishment.jobByName.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.jobByName.message.error == undefined ? undefined : nextProps.replenishment.jobByName.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.jobByName.message.status,
                errorCode: nextProps.replenishment.jobByName.message.error == undefined ? undefined : nextProps.replenishment.jobByName.message.error.errorCode
            })
        }
        if (nextProps.replenishment.getFiveTriggers.isSuccess) {


            this.setState({
                // nextScheduleObj:nextProps.replenishment.getFiveTriggers.data.resource == null ? [] : nextProps.replenishment.getFiveTriggers.data.resource.nextSchedule,
                // nextSchedule: nextProps.replenishment.getFiveTriggers.data.resource == null ? [] : nextProps.replenishment.getFiveTriggers.data.resource.nextSchedule[nextProps.replenishment.getFiveTriggers.data.resource.jobNames[0]],
                // allTriggers: nextProps.replenishment.getFiveTriggers.data.resource == null ? [] : nextProps.replenishment.getFiveTriggers.data.resource.jobTriggers[0],
                // allTriggersValue: nextProps.replenishment.getFiveTriggers.data.resource== null ? [] : nextProps.replenishment.getFiveTriggers.data.resource.jobTriggers,
                // alljobNames: nextProps.replenishment.getFiveTriggers.data.resource == null ? [] : nextProps.replenishment.getFiveTriggers.data.resource.jobNames,
                // selectedJob: nextProps.replenishment.getFiveTriggers.data.resource == null ? "" : nextProps.replenishment.getFiveTriggers.data.resource.jobNames[0],
                // jobName:nextProps.replenishment.getFiveTriggers.data.resource == null?"":nextProps.replenishment.getFiveTriggers.data.resource.jobNames[0],
                    loader: false,
            })


            //   if(nextProps.replenishment.getFiveTriggers.data.resource != null ){
            // this.props.getJobByNameRequest(nextProps.replenishment.getFiveTriggers.data.resource.jobNames[0]);
            // this.props.jobRunDetailRequest(nextProps.replenishment.getFiveTriggers.data.resource.jobNames[0]);
            //   }
            this.props.getFiveTriggersClear();
        } else if (nextProps.replenishment.getFiveTriggers.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getFiveTriggers.message.error == undefined ? undefined : nextProps.replenishment.getFiveTriggers.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getFiveTriggers.message.status,
                errorCode: nextProps.replenishment.getFiveTriggers.message.error == undefined ? undefined : nextProps.replenishment.getFiveTriggers.message.error.errorCode
            })
            this.props.getFiveTriggersClear();
        }

        if (nextProps.replenishment.createTrigger.isSuccess) {


            this.setState({
                successMessage: nextProps.replenishment.createTrigger.data.message,
                loader: false,
                success: true,
                frequency: [],
                date: "",
                time: "",
                timeSet:"",
                monthSelected: [],
                daysSelected: [],
            })
            this.props.getJobByNameRequest(this.state.selectedJob);
            this.props.jobRunDetailRequest(this.state.selectedJob);
            this.props.getFiveTriggersRequest()
            this.props.createTriggerClear();
        } else if (nextProps.replenishment.createTrigger.isError) {

            this.setState({
                errorMessage: nextProps.replenishment.createTrigger.message.error == undefined ? undefined : nextProps.replenishment.createTrigger.message.error.errorMessage,
                // errorMessage: nextProps.replenishment.createTrigger.message.error == null ? nextProps.replenishment.createTrigger.message.data.message : [],
                loader: false,
                alert: true,
                code: nextProps.replenishment.createTrigger.message.status,
                errorCode: nextProps.replenishment.createTrigger.message.error == undefined ? undefined : nextProps.replenishment.createTrigger.message.error.errorCode
            })
            this.props.createTriggerClear();
        }
        if (nextProps.replenishment.jobRunDetail.isSuccess) {
            if (nextProps.replenishment.jobRunDetail.data.resource != null) {

                this.setState({
                    percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                    timeTaken: nextProps.replenishment.jobRunDetail.data.resource.timeTaken,
                    nextScheduled: nextProps.replenishment.jobRunDetail.data.resource.nextSchedule == null ? "NA" : nextProps.replenishment.jobRunDetail.data.resource.nextSchedule[this.state.selectedJob],
                    status: nextProps.replenishment.jobRunDetail.data.resource.state,
                    successState: false,
                    loader: false,
                    runningjob: nextProps.replenishment.jobRunDetail.data.resource.runningjob
                })
                sessionStorage.setItem('jobId', nextProps.replenishment.jobRunDetail.data.resource.jobId)
            }
            setTimeout(() => {
                this.props.jobRunDetailRequest(this.state.selectedJob);
            }, 10000)

        } else if (nextProps.replenishment.jobRunDetail.isError) {

            setTimeout(() => {
                this.props.jobRunDetailRequest(this.state.selectedJob);
            }, 10000)
        }

        if (nextProps.replenishment.jobRunDetail.isSuccess) {
            if (nextProps.replenishment.jobRunDetail.data.resource != null) {
                if (nextProps.replenishment.jobRunDetail.data.resource.state === "NOT STARTED" || nextProps.replenishment.jobRunDetail.data.resource.state === "STOPPED" ) {
                    this.setState({
                       
                        percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                        triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                    })
                } else if (nextProps.replenishment.jobRunDetail.data.resource.state ==="RUNNING") {
                    this.setState({
                     
                        percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                        triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                    })
                } else if (nextProps.replenishment.jobRunDetail.data.resource.state === "SUCCEEDED") {
                    this.setState({
                        succeeded: true,
                        modalState: nextProps.replenishment.jobRunDetail.data.resource.state,
                      
                        percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                        triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                    })

                } else if (nextProps.replenishment.jobRunDetail.data.resource.state === "FAILED") {
               
                    this.setState({
                       
                        succeeded: true,
                        modalState: nextProps.replenishment.jobRunDetail.data.resource.state,
                        percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                        triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,
                    })

                }

                
            }
        }
        if (this.state.successState) {
            this.setState({
                loader: true
            })
        }
        if (nextProps.replenishment.nschedule.isSuccess) {
            if (nextProps.replenishment.nschedule.data.resource != null) {
                var time = nextProps.replenishment.nschedule.data.resource.schedule.split(" ")[3];
                var date = nextProps.replenishment.nschedule.data.resource.schedule.substr(0, 11);
                this.setState({
                    nextScheduled: nextProps.replenishment.nschedule.data.resource.nextSchedule == null ? "NA" : nextProps.replenishment.nschedule.data.resource.nextSchedule[this.state.selectedJob],
                    lastDate: date,
                    lastFreq: nextProps.replenishment.nschedule.data.resource.frequency,
                    lastTime: time,
                })
            }
            this.setState({
                loader: false
            })
        }

        if (nextProps.replenishment.getPartner.isSuccess) {
            if (nextProps.replenishment.getPartner.data.resource != null) {
                this.setState({
                    partnerData: nextProps.replenishment.getPartner.data.resource.partner
                })

            }
            this.setState({
                loader: false,
            })
            this.props.getPartnerClear();
        } else if (nextProps.replenishment.getPartner.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getPartner.message.error == undefined ? undefined : nextProps.replenishment.getPartner.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getPartner.message.status,
                errorCode: nextProps.replenishment.getPartner.message.error == undefined ? undefined : nextProps.replenishment.getPartner.message.error.errorCode
            })
            this.props.getPartnerClear();
        }
        if (nextProps.replenishment.getZone.isSuccess) {
            if (nextProps.replenishment.getZone.data.resource != null) {
                this.setState({
                    zoneData: nextProps.replenishment.getZone.data.resource.zone
                })
            }
            this.setState({
                loader: false,
            })
            this.props.getZoneClear();
        } else if (nextProps.replenishment.getZone.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getZone.message.error == undefined ? undefined : nextProps.replenishment.getZone.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getZone.message.status,
                errorCode: nextProps.replenishment.getZone.message.error == undefined ? undefined : nextProps.replenishment.getZone.message.error.errorCode
            })
            this.props.getZoneClear();
        }
        if (nextProps.replenishment.applyFilter.isSuccess) {
            if (nextProps.replenishment.applyFilter.data.resource != null) {
                this.setState({
                    stores: nextProps.replenishment.applyFilter.data.resource.store
                })
            }
            this.setState({
                loader: false,
            })
            this.props.applyFilterClear();
        } else if (nextProps.replenishment.applyFilter.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.applyFilter.message.error == undefined ? undefined : nextProps.replenishment.applyFilter.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.applyFilter.message.status,
                errorCode: nextProps.replenishment.applyFilter.message.error == undefined ? undefined : nextProps.replenishment.applyFilter.message.error.errorCode
            })
            this.props.applyFilterClear();
        }

        if (nextProps.replenishment.getAllJob.isSuccess) {
            let jobName = [];
            let configName =[];
            Object.keys(nextProps.replenishment.getAllJob.data.resource).map( key=>{
                jobName.push(key);
                configName.push(nextProps.replenishment.getAllJob.data.resource[key]);
            })
            this.setState({
                loader:false,
                alljobNames: jobName == null ? [] : jobName,
                selectedJob:jobName[0],
                configParam: nextProps.replenishment.getAllJob.data.resource
            })
            this.props.getConfigFilterRequest(configName[0]);
            this.props.getAllJobClear()

        }else if(nextProps.replenishment.getAllJob.isError){
            this.setState({
                loader:false
            })
        }

        if (nextProps.replenishment.getConfigFilter.isSuccess) {
            this.setState({
                loader:false,
                configurationFilter: nextProps.replenishment.getConfigFilter.data.resource,
            })
            // this.setState({
            //     loader: false,
            //     getConfigStore: nextProps.replenishment.getConfigFilter.data.resource.storeCode == null ? [] : nextProps.replenishment.getConfigFilter.data.resource.storeCode,
            //     isEnabled: nextProps.replenishment.getConfigFilter.data.resource.isEnabled,
            //     labelConfigFilter: Object.keys(nextProps.replenishment.getConfigFilter.data.resource.label),
            //     configurationFilter: nextProps.replenishment.getConfigFilter.data.resource.label,
            // })
            // let configurationFilter = nextProps.replenishment.getConfigFilter.data.resource.label
            // let filterName = Object.keys(configurationFilter)
            // let filter = {}
            // for (let i = 0; i < filterName.length; i++) {
            //     if (configurationFilter[filterName[i]].type == "T") {
            //         if (configurationFilter[filterName[i]].changeValue != "") {
            //             filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
            //         }
            //     } else if (configurationFilter[filterName[i]].type == "L") {
            //         if (configurationFilter[filterName[i]].selectedValue.length != 0) {
            //             filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
            //         }
            //     }
            // }
            // let payload = {
            //     filter: filter
            // }
            // this.props.getStoreCodeRequest(payload)
            this.props.getConfigFilterClear()

        } else if (nextProps.replenishment.getConfigFilter.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getConfigFilter.message.error == undefined ? undefined : nextProps.replenishment.getConfigFilter.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getConfigFilter.message.status,
                errorCode: nextProps.replenishment.getConfigFilter.message.error == undefined ? undefined : nextProps.replenishment.getConfigFilter.message.error.errorCode
            })
            this.props.getConfigFilterClear();
        }
        if (nextProps.replenishment.getStoreCode.isSuccess) {
            if (nextProps.replenishment.getStoreCode.data.resource != null) {
                this.setState({
                    filterStores: nextProps.replenishment.getStoreCode.data.resource.storeCode,
                    storeCodeList: nextProps.replenishment.getStoreCode.data.resource.store,
                    selected: [],
                    loader: false
                })
            } else {
                this.setState({
                    storeCodeList: [],
                    selected: [],
                    loader: false
                })
            }
            this.props.getStoreCodeClear()
        }
        if (nextProps.replenishment.nschedule.isLoading || nextProps.replenishment.createTrigger.isLoading || nextProps.replenishment.getConfigFilter.isLoading || nextProps.replenishment.getFiveTriggers.isLoading
            || nextProps.replenishment.getPartner.isLoading || nextProps.replenishment.getZone.isLoading || nextProps.replenishment.applyFilter.isLoading) {
            this.setState({
                loader: true
            })
        }

    }
    handleChange(e) {
        if (e.target.id == "frequency") {
            e.target.placeholder = e.target.value;
            if (e.target.value == "MONTHLY") {
                this.setState({
                    frequency: "MONTHLY",
                    week: false,
                    month: true
                },
                    () => {
                        this.frequency();
                    })
            } else if (e.target.value == "WEEKLY") {
                this.setState({
                    frequency: "WEEKLY",
                    week: true,
                    month: false
                },
                    () => {
                        this.frequency();
                    })
            } else if (e.target.value == "DAILY") {
                this.setState({
                    frequency: "DAILY",
                    month: false,
                    week: false
                },
                    () => {
                        this.frequency();
                    })
            } else {
                this.setState({
                    frequency: "",
                    month: false,
                    week: false
                },
                    () => {
                        this.frequency();
                    })

            }

        } else if (e.target.id == "timeSet") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    timeSet: e.target.value
                }, () => {
                    this.time()
                });
        } else if (e.target.id == "date") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    date: e.target.value
                },
                () => {
                    this.date();
                }
            );
        }
    }


    frequency() {
        if (
            this.state.frequency == ""
        ) {
            this.setState({
                frequencyerr: true
            });
        } else {
            this.setState({
                frequencyerr: false
            });
        }
    }
    time() {
        if (
            this.state.timeSet == "") {
            this.setState({
                timeerr: true
            });
        } else {
            this.setState({
                timeerr: false
            });
        }
    }
    date() {
        if (
            this.state.date == "") {
            this.setState({
                dateerr: true
            });
        } else {
            this.setState({
                dateerr: false
            });
        }
    }

    onCheckClick(id) {
        for (let i = 0; i < this.state.months.length; i++) {
            if (this.state.months[i].id == id) {
                if (this.state.months[i].checked) {
                    let array = this.state.monthSelected;
                    for (let x = 0; x < array.length; x++) {

                        if (array[x].id == id) {
                            array.splice(x, 1);

                            this.setState({
                                monthSelected: array
                            }, () => {
                                this.monthlyError();
                            })
                            break;
                        }
                    }
                } else {
                    this.setState({
                        monthSelected: this.state.monthSelected.concat(this.state.months[i])
                    }, () => {
                        this.monthlyError();
                    })
                    break;
                }
            }
        }
        if (this.state.months[id - 1].checked) {
            this.state.months[id - 1].checked = false
        } else {
            this.state.months[id - 1].checked = true
        }
    }

    onMonthClick(id) {
        for (let i = 0; i < this.state.days.length; i++) {
            if (this.state.days[i].id == id) {
                if (this.state.days[i].checked) {
                    let array = this.state.daysSelected;
                    for (let x = 0; x < array.length; x++) {
                        if (array[x].id == id) {
                            array.splice(x, 1);
                            this.setState({
                                daysSelected: array
                            }, () => {
                                this.weeklyError()
                            })
                            break;
                        }
                    }
                } else {
                    this.setState({
                        daysSelected: this.state.daysSelected.concat(this.state.days[i])
                    }, () => {
                        this.weeklyError()
                    })
                    break;
                }
                break;
            }
        }
        if (this.state.days[id - 1].checked) {

            this.state.days[id - 1].checked = false
        } else {
            this.state.days[id - 1].checked = true
        }
    }

    onFilterApply(e) {
        let configurationFilter = this.state.configurationFilter
        let filterName = Object.keys(configurationFilter)
        for (let i = 0; i < filterName.length; i++) {
            if (configurationFilter[filterName[i]].isChecked == "True") {
                if (configurationFilter[filterName[i]].type == "T") {
                    if (configurationFilter[filterName[i]].changeValue != "") {
                        this.state.filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
                    }
                }
                if (configurationFilter[filterName[i]].type == "L") {
                    if (configurationFilter[filterName[i]].selectedValue.length != 0 && configurationFilter[filterName[i]].displayName == "Store" && configurationFilter[filterName[i]].value != "ALL-STORES") {
                        let selectedValue = configurationFilter[filterName[i]].selectedValue
                        let selectedValues = selectedValue.map((item) => {
                            return item.split('-')[0]
                        })
                        this.state.filter[configurationFilter[filterName[i]].columnName] = selectedValues.length > 1 ? selectedValues.join('|') : selectedValues.join(',')
                    } else if (configurationFilter[filterName[i]].selectedValue.length != 0 && configurationFilter[filterName[i]].displayName == "Store" && configurationFilter[filterName[i]].value == "ALL-STORES") {
                        this.state.filter[configurationFilter[filterName[i]].columnName] = this.state.filterStores
                    }
                    if (configurationFilter[filterName[i]].selectedValue.length != 0 && configurationFilter[filterName[i]].displayName != "Store") {
                        this.state.filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join(',')
                    }
                }
                this.state.saveCol.push(configurationFilter[filterName[i]].displayName)
            } else {
                if (configurationFilter[filterName[i]].type == "L") {
                    if (configurationFilter[filterName[i]].selectedValue.length == 0 && configurationFilter[filterName[i]].displayName == "Store" && configurationFilter[filterName[i]].value != "ALL-STORES") {
                        this.state.filter[configurationFilter[filterName[i]].columnName] = this.state.getConfigStore
                    } else if (configurationFilter[filterName[i]].selectedValue.length == 0 && configurationFilter[filterName[i]].displayName == "Store" && configurationFilter[filterName[i]].value == "ALL-STORES") {
                        this.state.filter[configurationFilter[filterName[i]].columnName] = "NA"
                    }
                }
            }
            if (configurationFilter[filterName[i]].isChecked == "False") {
                for (let j = 0; j < this.state.saveCol.length; j++) {
                    if (this.state.saveCol[j] == configurationFilter[filterName[i]].displayName) {
                        this.state.saveCol.splice(j, 1)
                    }
                }
            }
        }
        this.setState({
            filter: this.state.filter,
            saveCol: this.state.saveCol
        })
        this.functionSave();
    }
    functionSave() {
        if (this.state.saveCol == "") {
            this.setState({
                toastMsg: "Select atleast one",
                toastLoader: true,
                apply: false
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);

        } else {
            this.setState({
                apply: true
            })
            this.closeFilter()
        }
    }

    contactSubmit(e) {
        e.preventDefault();
        var sortMonth = this.state.monthSelected.length > 0 ? this.state.monthSelected : [];
        var sortDay = this.state.daysSelected.length > 0 ? this.state.daysSelected : [];
        let abc = "";
        if (this.state.daysSelected.length > 0) {
            for (let i = 0; i < sortDay.length; i++) {
                let a = sortDay[i].value;

                if (sortDay.length > 1) {

                    abc = abc == "" ? "".concat(a) : abc.concat(',').concat(a);

                } else {
                    abc = a;

                }
            }
        }
        let xyz = "";
        if (this.state.monthSelected.length > 0) {
            for (let i = 0; i < sortMonth.length; i++) {
                let a = sortMonth[i].value;
                if (sortMonth.length > 1) {
                    xyz = xyz == "" ? "".concat(a) : xyz.concat(',').concat(a);
                } else {
                    xyz = a;
                }
            }
        }
        this.frequency();
        this.date();
        this.time();
        this.state.frequency == "MONTHLY" ? this.monthlyError() : null
        this.state.frequency == "WEEKLY" ? this.weeklyError() : null

        const t = this;

        setTimeout(function () {
            const { frequency, date, timeSet, frequencyerr, dateerr, timeerr, monthserr, dayserr, filter } = t.state;
            if (!frequencyerr && !dateerr && !timeerr && !monthserr && !dayserr) {
                t.setState({
                    loader: true,
                    alert: false,
                    success: false
                })
                let triggerData = {
                    jobName: t.state.selectedJob,
                    timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                    schedule: date + " " + timeSet,
                    frequency: frequency,
                    triggerName: 'NA',
                    type: 'SCHEDULED',
                    enterpriseName: sessionStorage.getItem('partnerEnterpriseCode'),
                    enterpriseId: sessionStorage.getItem('partnerEnterpriseId'),
                    userName: sessionStorage.getItem('userName'),
                    dayMonth: xyz == "" ? "NA" : xyz,
                    dayWeek: abc == "" ? "NA" : abc,
                    // channel: t.state.channel,
                    // partner: t.state.partner,
                    // grade: "",
                    // storeCode: t.state.stores,
                    // zone: t.state.zone,
                    filter: !t.state.apply ? {} : t.state.filter,
                    // filter: this.state.apply ? filter : {}
                }
                t.props.createTriggerRequest(triggerData);
            }
        }, 100)
    }

    monthlyError() {
        var months = this.state.months
        for (var i = 0; i < months.length; i++) {
            if (months[i].checked == false) {
                this.setState({
                    monthserr: true
                })
            } else {
                this.setState({
                    monthserr: false
                })
                break;
            }
        }
    }
    weeklyError() {
        var days = this.state.days
        for (var i = 0; i < days.length; i++) {
            if (days[i].checked == false) {
                this.setState({
                    dayserr: true
                })
            } else {
                this.setState({
                    dayserr: false
                })
                break;
            }
        }

    }
    SelectChannel(e) {
        if (this.state.channel != e.target.value) {
            this.setState({
                partner: "",
                zone: ""
            })
        }
        this.setState({
            channel: e.target.value

        })
        if (e.target.value != "") {
            let data = {
                channel: e.target.value
            }
            this.props.getPartnerRequest(data)
        }
    }
    SelectPartner(e) {
        if (this.state.partner != e.target.value) {
            this.setState({
                zone: ""
            })
        }
        this.setState({
            partner: e.target.value
        })
        if (e.target.value != "") {
            let data = {
                channel: this.state.channel,
                partner: e.target.value

            }
            this.props.getZoneRequest(data)
        }

    }
    SelectZone(e) {
        this.setState({
            zone: e.target.value
        })
    }
    // applyFilter() {
    //     let payload = {
    //         channel: this.state.channel,
    //         partner: this.state.partner,
    //         zone: this.state.zone,
    //         grade: "",
    //         storeCode: "NA"
    //     }
    //     this.props.applyFilterRequest(payload)
    // }

    openFilter() {
        this.setState({
            openFilter: true
        })
    }

    closeFilter() {
        this.setState({
            openFilter: false
        })
    }
    timeDropHandle() {
        this.setState({
            timeDrop: !this.state.timeDrop
        })
    }
    onSelectJobName(e) {

        this.setState({
            selectedJob: e.target.value,
            nextSchedule:this.state.nextScheduleObj[e.target.value]
        })
        Object.keys(this.state.configParam).map(key =>{
            if( key == e.target.value){
                this.props.getConfigFilterRequest(this.state.configParam[key]);
            }
        });
    }
    deleteConfiguration() {
        let payload = {
            triggerName: this.state.editTriggerName
        }
        this.props.deleteTriggerRequest(payload)
        this.hide()
    }

    editConfiguration(data) {
        let triggerData = {
            jobName: this.state.editjobName,
            timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
            schedule: data.editDate + " " + data.editTime,
            frequency: this.state.editFrequency,
            triggerName: this.state.editTriggerName,
            type: 'SCHEDULED',
            enterpriseName: sessionStorage.getItem('partnerEnterpriseCode'),
            enterpriseId: sessionStorage.getItem('partnerEnterpriseId'),
            userName: sessionStorage.getItem('userName'),
            dayMonth: this.state.editMonth,
            dayWeek: this.state.editWeek,
            // channel: this.state.editChannel,
            // partner: this.state.editPartner,
            // grade: this.state.editGrade,
            // storeCode: this.state.editStoreCode,
            // zone: this.state.editZone
            filter: !this.state.apply ? {} : this.state.filterStores,
            // filter: this.state.apply ? filter : {}
        }

        this.props.createTriggerRequest(triggerData);
        this.hide()
    }

    changeJob(job) {
       
        this.setState({
            selectedJob: job,
            successState: true
        })

        sessionStorage.removeItem('jobId');
        this.props.jobRunDetailRequest(job);
        this.props.getJobByNameRequest(job);
        // this.props.getFiveTriggersRequest();
        Object.keys(this.state.configParam).map(key =>{
            if( key == job){
                this.props.getConfigFilterRequest(this.state.configParam[key]);
            }
        });
    }
    timerScroll = (shiftDiv, id) => e => {
        var element = document.getElementById(id)
        var allLi = element.querySelectorAll('li')
        allLi.forEach(e => e.style.fontWeight = "normal");
        setTimeout(() => {
            if (id == "hourScroll") {
                if (shiftDiv == "top") {
                    if (i > 0) {
                        i--
                    }

                    element.scrollTop -= 24
                    allLi[i].style.fontWeight = "bold"
                    this.setState({
                        startTime: i
                    })
                } else if (shiftDiv == "bottom") {
                    if (i < 23) {
                        i++
                    }
                    allLi[i].style.fontWeight = "bold"
                    element.scrollTop += 24
                    this.setState({
                        startTime: i
                    })
                }
            }
            if (id == "timeScroll") {
                if (shiftDiv == "top") {
                    j--
                    element.scrollTop -= 24
                    allLi[j].style.fontWeight = "bold"
                    this.setState({
                        endTime: j
                    })
                } else if (shiftDiv == "bottom") {
                    j++
                    allLi[j].style.fontWeight = "bold"
                    element.scrollTop += 24
                    this.setState({
                        endTime: j
                    })
                }
            }
        }, 100)

    }

    handleChangeFilter(payload) {
        let configurationFilter = this.state.configurationFilter
        configurationFilter[payload.data].changeValue = payload.events.target.value
        this.setState({
            configurationFilter: configurationFilter
        })

    }

    // ____________________SCROLL CONFIGURED DETAILS CODE___________________________

    FunLeft() {
        var element = document.getElementById('scrollMe');
        var positionInfo = element.getBoundingClientRect();
        var width = positionInfo.width;
        var elmnt = document.getElementById("scrollMe");
        elmnt.scrollLeft = elmnt.scrollLeft + width;
        elmnt.style.transition = "5s";
    }
    FunRight() {
        var element = document.getElementById('scrollMe');
        var positionInfo = element.getBoundingClientRect();
        var width = positionInfo.width;
        var elmnt = document.getElementById("scrollMe");
        elmnt.scrollLeft = elmnt.scrollLeft - width;
    }
      closeSucceeded(type) {
        this.setState({
            succeeded: false
        })
        sessionStorage.removeItem('jobId');
  
        if (type != "close") {

            this.props.history.push('/inventoryPlanning/summary');
        }
    }
    render() {

        const { frequency, frequencyerr, time, timeerr, date, dateerr, monthserr, dayserr } = this.state;
        return (
            <div>
                <div className="container-fluid">
                    <div className="container_div" id="home">
                        <div className="container-fluid">
                            <div className="container_div" id="">
                                <div className="container-fluid pad-0">
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                        <div className="replenishment_container weekelyPlanning monthlyPlanning">
                                            <div className="col-md-12 pad-0 inventoryAutoConfig">
                                                <div className="autoconfiglLeft pad-lft-0">
                                                    <ul className="list_style">
                                                        <li>
                                                            <label className="contribution_mart">
                                                                AUTO CONFIGURATION
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <p className="master_para">Create your rule engine configuration</p>
                                                        </li>
                                                    </ul>
                                                    <div className="configurationDetails m-top-50">
                                                        <div className="configContainer">
                                                            <div className="top-config">
                                                                <div className="col-md-12 headers">
                                                                    <h3 className="displayInline">Choose below option to set scheduler task</h3>
                                                                    {/* <button type="button" className={this.state.isEnabled == "true" ? "filterButton" : "btnDisabled filterButton"} onClick={(e) => this.state.isEnabled == "true" ? this.openFilter(e) : null}>Config Planning Parameter 
                                                                        <img src={filter} />
                                                                    </button> */}
                                                                    <button type="button" className={"filterButton"} onClick={(e) =>this.openFilter(e) }>Config Planning Parameter
                                                                       <img src={filter} /> 
                                                                    </button>
                                                                </div>
                                                              
                                                                <div className="col-md-12 m-top-10 predectionTime pad-tb-20">
                                                                    <div className="chooseYear">
                                                                        <div className="fieldWidth pad-lft-0">
                                                                            <h5>Select Job</h5>
                                                                            <ul className="pad-0">
                                                                                <li className="m-rgt-0">
                                                                                    <select className="displayPointer selectWidth textElippse" value={this.state.selectedJob} onChange={(e) => this.onSelectJobName(e)}>
                                                                                        <option value="">Select Job</option>
                                                                                        {this.state.alljobNames.map((jdata, jobkey) => (
                                                                                            <option value={jdata} key={jobkey}>{jdata}</option>
                                                                                        ))}
                                                                                    </select>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div className="fieldWidth pad-lft-0">
                                                                            <h5>Choose Frequency</h5>
                                                                            <ul className="pad-0">
                                                                                <li className="m-rgt-0">

                                                                                    <select className={frequencyerr ? "selectWidth errorBorder displayPointer" : "selectWidth displayPointer"} onChange={e => this.handleChange(e)} placeholder="Select Frequency" id="frequency" value={frequency}>
                                                                                        <option value="">Select Frequency</option>
                                                                                        {/* <option value="hourly">Hourly</option> */}
                                                                                        <option value="DAILY">Daily</option>
                                                                                        <option value="WEEKLY">Weekly</option>
                                                                                        <option value="MONTHLY">Monthly</option>
                                                                                    </select>
                                                                                    {/* {frequencyerr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                                                    {frequencyerr ? (
                                                                                        <span className="error">
                                                                                            Select frequency
                                                                </span>
                                                                                    ) : null}
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div className="fieldWidth pad-lft-0">
                                                                            <h5>Choose Time</h5>
                                                                            <input className="inputTime selectWidth inputTimer" type="time" id="timeSet" placeholder={this.state.timeSet == "" ? "hh:mm" : this.state.timeSet} value={this.state.timeSet} onChange={e => this.handleChange(e)} />
                                                                            {timeerr ? (
                                                                                <span className="error">
                                                                                    Select time
                                                                </span>
                                                                            ) : null}
                                                                           
                                                                        </div>
                                                                        <div className="fieldWidth pad-lft-0">
                                                                            <h5>Date</h5>
                                                                            <input className={dateerr ? "errorBorder genericBorder fontBold" : "genericBorder fontBold"} type="date" value={date} min={this.state.min} id="date" placeholder={this.state.date == "" ? "Choose Date" : this.state.date} onChange={e => this.handleChange(e)} />

                                                                            {dateerr ? (
                                                                                <span className="error">
                                                                                    Select date
                                                                </span>
                                                                            ) : null}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {this.state.month ? <div className="selectedDropdownList">
                                                                <p>Choose Months of Year</p>
                                                                <div className="choosenLIst autoConfigCheck">
                                                                    {this.state.months.map((data, key) => (<button type="button" key={key}>
                                                                        <p className="para_purchase replenishmentCheck">
                                                                            <input type="text" className="purchaseCheck" onClick={() => this.onCheckClick(`${data.id}`)} value={data.value} type="checkbox" id={data.id} name={data.id} />
                                                                            <label htmlFor={data.id} className="purchaseLabel displayPointer"><span>{data.name}</span></label>
                                                                        </p>
                                                                    </button>))}
                                                                    {monthserr ? (
                                                                        <span className="error">
                                                                            Select atleast one
                                                            </span>
                                                                    ) : null}
                                                                </div>
                                                            </div> : null}
                                                            {this.state.week ? <div className="selectedDropdownList ">
                                                                <p>Choose Days of Week</p>
                                                                <div className="choosenLIst autoConfigCheck">
                                                                    {this.state.days.map((data, key) => (<button type="button" key={key}>
                                                                        <p className="para_purchase replenishmentCheck">
                                                                            <input type="text" value={data.value} className="purchaseCheck" onClick={() => this.onMonthClick(`${data.id}`)} type="checkbox" id={data.id} name={data.id} />
                                                                            <label htmlFor={data.id} className="purchaseLabel displayPointer"><span>{data.name}</span></label>
                                                                        </p>
                                                                    </button>))}
                                                                    {dayserr ? (
                                                                        <span className="error">
                                                                            Select atleast one
                                                                </span>
                                                                    ) : null}

                                                                </div>
                                                            </div> : null}
                                                            <div className="bottom-config m-top-30 displayInline width100">
                                                                <div className="col-md-10 left">
                                                                    <div className="headingMain">
                                                                        <h3>Previously Configured Details</h3>
                                                                        <p>Showing {this.state.allTriggersValue.length==0 ? null : this.state.allTriggersValue.length} of 5 results</p>
                                                                    </div>
                                                                    <div className="scrollMainDiv" id="scrollMe">
                                                                        <div className="pad-0 m-top-20 innerCustomDiv" >
                                                                            {this.state.allTriggersValue.map((data, tkey) => (
                                                                                <div className="previousConfigTrigg pad-lft-0" key={tkey}>
                                                                                    <div className="eachconfigTask">
                                                                                        <h5>{data.frequency}</h5>
                                                                                        <p>{data.schedule.slice(11, 17)}</p>
                                                                                        <span className="JobName">{data.jobName}</span>
                                                                                        <label>Scheduled on <span>{data.schedule.slice(0, 11)}</span></label>
                                                                                        {tkey == 0 ? <span className="recentAdded displayBlock">Recently added</span> : null}
                                                                                        <button type="button" className="editBtn" onClick={(e) => this.show(data.jobName, data.triggerName, data.frequency, data.dayMonth, data.dayWeek, data.channel, data.partner, data.zone, data.grade, data.storeCode)}>Edit<img src={editIcon} /></button>
                                                                                    </div>
                                                                                </div>
                                                                            ))}
                                                                        </div>
                                                                    </div>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" className="blink-img displayPointer" width="17" height="19" viewBox="0 0 17 19">
                                                                        <g fill="#6629DB" fillRule="nonzero">
                                                                            <path d="M6.991 17.923a.61.61 0 0 0 0 .887c.128.127.277.19.447.19.17 0 .318-.063.446-.19l8.925-8.867a.61.61 0 0 0 0-.886L7.884.19a.619.619 0 0 0-.893 0 .61.61 0 0 0 0 .887L15.47 9.5l-8.479 8.423z" opacity=".419" />
                                                                            <path d="M8.67 9.5L.191 1.077a.61.61 0 0 1 0-.887.619.619 0 0 1 .893 0l8.925 8.867a.61.61 0 0 1 0 .886L1.084 18.81a.65.65 0 0 1-.447.19.65.65 0 0 1-.446-.19.61.61 0 0 1 0-.887L8.67 9.5z" opacity=".688" />
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <div className="col-md-2 previousConfigRight m-top-20">
                                                                    <div className="leftShift" onClick={(e) => this.FunRight(e)}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="21" viewBox="0 0 12 21">
                                                                            <path fill="none" fillRule="evenodd" stroke="#9100ff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.6" d="M10.281.938l-9.208 9.208 9.208 9.208" />
                                                                        </svg>
                                                                    </div>
                                                                    <div className="rightShift" onClick={(e) => this.FunLeft(e)}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="21" viewBox="0 0 12 21">
                                                                            <path fill="none" fillRule="evenodd" stroke="#9100ff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.6" d="M1.719.938l9.208 9.208-9.208 9.208" />
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="autoConfigRight pad-right-0 posRelative">
                                                    <div className="jobStatusForecast jobsTrigger">
                                                        <img src={runningjobIcon} />
                                                        {/* <label>{this.state.status}</label> */}
                                                        <label className="jobsTriggerLabel">Triggered Job Details</label>
                                                        <div className="jobs displayInline">
                                                            {this.state.runningjob.length != 0 || this.state.runningjob != "" ? this.state.runningjob.map((datajob, keyjob) => (
                                                                <div className="col-md-12 pad-0 m-top-40 eachTrigger" key={keyjob}>
                                                                    <div className="col-md-8">
                                                                        <h5>{datajob.jobName}</h5>
                                                                        <label>{datajob.triggerName}</label>
                                                                    </div>
                                                                    <div className="col-md-4 text-center circleLoader">
                                                                        <img className="rotateAnimation" src={circleIcon} />
                                                                        <p><span>{datajob.timetaken}</span> min Elapsed</p>
                                                                    </div>
                                                                </div>
                                                            )) : null}
                                                        </div>
                                                    </div>
                                                    <div className="configJobDetails">
                                                        <div className="forecastTimer">
                                                            <div className="rightReplenishment autoConfigTimer">
                                                                <div className="runningJobs width100">
                                                                    <ul className="list-inline">
                                                                        {this.state.alljobNames.length != 0 ? this.state.alljobNames.map((data, i) =>
                                                                            <li key={i} value={data} className={this.state.selectedJob == data ? "jobBtn m-rgt-10 active":"jobBtn m-rgt-10"} onClick={(e) => this.changeJob(data)}>{"Job" + (i + 1)}</li>
                                                                        ) : null}

                                                                    </ul>
                                                                    {/* <div className="jobNamePagination">
                                                                        <label>Prev</label>
                                                                        <label>Next</label>
                                                                    </div> */}
                                                                </div>
                                                                <div className="replenishmentAnimateDiv">
                                                                    <div className="middleReplensh">
                                                                        <div className="single-chart">
                                                                            <svg viewBox="0 0 36 36" className="circular-chart green m-bot-0">
                                                                                <path className="circle-bg" fill="#fff"
                                                                                    d="M18 2.0845
                                                                                    a 15.9155 15.9155 0 0 1 0 31.831
                                                                                a 15.9155 15.9155 0 0 1 0 -31.831"
                                                                            />
                                                                            <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
                                                                                    <stop offset="50%" stopColor="#ffd3a5" />
                                                                                    <stop offset="100%" stopColor="#fd6585" />
                                                                                </linearGradient>
                                                                                <path className="circle" stroke="url(#gradient)"
                                                                                    strokeDasharray={this.state.timeTaken + ", 100"}
                                                                                    d="M36 20.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"
                                                                                />
                                                                            </svg>
                                                                        </div>
                                                                        <div className="autoConfigMid">
                                                                            <div className="autoComfigTime onHoverSvg">
                                                                                <h3>{this.state.timeTaken}</h3>
                                                                                <p>Mins elapsed</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="currentJobName">
                                                                        <h5>Job Name</h5>
                                                                        <label>{this.state.allTriggers == undefined ? "NA" : this.state.allTriggers.jobName}</label>
                                                                    </div>
                                                                    <div className="bottomReplesh">
                                                                        <label className="updateEngin displayBlock marginAuto">
                                                                            Last Engine Run
                                                                        </label>
                                                                        <span className="time">{this.state.jobRunState == null ? "NA" : this.state.jobRunState}</span>
                                                                        <label className="schduleTime">
                                                                            <span>Next Trigger scheduled</span>
                                                                        </label>

                                                                        <span className="timeSchedule displayInitial" >
                                                                            {this.state.nextSchedule == undefined ? "NA" : this.state.nextSchedule}
                                                                        </span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                                <div className="footerDivForm height4per">
                                                    <ul className="list-inline m-lft-0 m-top-10">
                                                        <li><button className="clear_button_vendor" type="reset">Clear</button>
                                                        </li>
                                                        <li><button type="button" className="save_button_vendor" id="saveButton" onClick={(e) => this.contactSubmit(e)} > Save</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.succeeded ? <Succeeded modalState={this.state.modalState} closeSucceeded={(e) => this.closeSucceeded(e)} /> : null}

                    {this.state.openFilter ? <FilterModal {...this.state} {...this.props} handleChangeFilter={(e) => this.handleChangeFilter(e)} closeFilter={(e) => this.closeFilter(e)} Change={(e) => this.Change(e)} onFilterApply={(e) => this.onFilterApply(e)} /> : null}
                    {this.state.autoConfig ? <AutoConfigEditModal {...this.state} {...this.props} editTime={this.state.editTime} channel={this.state.channel} editConfiguration={(e) => this.editConfiguration(e)} handleChange={(e) => this.handleChange(e)} editDateChange={(e) => this.editDateChange(e)} editDate={this.state.editDate} partner={this.state.partner} zone={this.state.zone} hide={() => this.hide()} deleteConfiguration={(e) => this.deleteConfiguration(e)} editTriggerName={this.state.editTriggerName} /> : null}
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                </div>
            </div>

        );

    }

}

export default AutoConfig;