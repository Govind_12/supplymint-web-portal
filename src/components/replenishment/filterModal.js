import React, { Component } from 'react'
import ToastLoader from '../loaders/toastLoader';
export default class FilterModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toastLoader: false,
            toastMsg: ""
        }
    }
    handleChangeFilters(e, value, data) {
        let payload = {
            events: e,
            value: value,
            data: data
        }
        this.props.handleChangeFilter(payload)
    }
    onnChange(id, value, data) {
        let match = _.find(this.props.storeCodeList, function (o) {
            return o.storeCode == id;
        })
        let filterConfig = this.props.configurationFilter
        if (!this.props.configurationFilter[data].selectedValue.includes(value)) {
            let selectedValue = this.props.configurationFilter[data].selectedValue
            selectedValue.push(match.storeCode + "-" + value)
        }

        
          let  selected=this.props.selected.concat(match)
        this.props.updateSelected(selected)
    

         this.props.updateConfigFilter(filterConfig)

        let idd = match.storeCode;
        var array = this.props.storeCodeList;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === idd) {
                array.splice(i, 1);
            }
        }
        this.props.updateStoreCodeList(array)
     
    }

    onDelete(e, value, data) {
        let match = _.find(this.props.selected, function (o) {
            return o.storeCode == e;
        })
        let filterConfig = this.props.configurationFilter
        if (this.props.configurationFilter[data].selectedValue.includes(match.storeCode + "-" + value)) {
            let selectedValue = this.props.configurationFilter[data].selectedValue
            for (let i = 0; i < selectedValue.length; i++) {
                if (selectedValue[i] == match.storeCode + "-" + value) {
                    selectedValue.splice(i, 1)
                }
            }
        }
      
          let   storeCodeList= this.props.storeCodeList.concat(match)
this.props.updateStoreCodeList(storeCodeList)
this.props.updateConfigFilter(filterConfig)


        var array = this.props.selected;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === e) {
                array.splice(i, 1);
            }
        }
        this.props.updateSelected(array)
      
    }
    Change(e) {
        this.props.updateStoreSearch(e.target.value)
    
    }
    checkFilter(value) {
        console.log(value)
        let filterConfig = this.props.configurationFilter
        let checkedValue = this.props.checkedValue
        let filterValue = false
        if (filterConfig[value].type == "T") {
            if (filterConfig[value].changeValue != "" || filterConfig[value].isChecked=="True") {
               filterValue=true
                if (filterConfig[value].isChecked == "True") {
                    filterConfig[value].isChecked = "False"
                } else {
                    filterConfig[value].isChecked = "True"
                  checkedValue.push(filterConfig[value].isChecked)
                }
            } else {
                this.setState({
                    toastMsg: "Enter " + filterConfig[value].displayName,
                    toastLoader: true,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 1500);

            }
        } else if (filterConfig[value].type == "L") {
            if (filterConfig[value].selectedValue.length != 0 || filterConfig[value].isChecked=="True")  {
                filterValue=true
                if (filterConfig[value].isChecked == "True") {
                    filterConfig[value].isChecked = "False"
                } else {
                    filterConfig[value].isChecked = "True"
                    checkedValue.push(filterConfig[value].isChecked)
                }
            }
            else {
                this.setState({
                    toastMsg: "Select " + filterConfig[value].displayName,
                    toastLoader: true,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 1500);
            }
        }
   
        this.props.updateCheckedValue(checkedValue)
        this.props.updateConfigFilter(filterConfig)
      
if(filterValue){
          setTimeout(() => {
        let configurationFilter = this.props.configurationFilter
        let filterName = Object.keys(configurationFilter)
                let filter = {}
           
        for (let i = 0; i < filterName.length; i++) {
            if(configurationFilter[filterName[i]].displayName != "Store" ){
            
           for (let i = 0; i < filterName.length; i++) {
                        if (configurationFilter[filterName[i]].isChecked == "True") {
                            if (configurationFilter[filterName[i]].type == "T") {
                                if (configurationFilter[filterName[i]].changeValue != "") {
                                    filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
                                }
                            } else if (configurationFilter[filterName[i]].type == "L") {
                                if (configurationFilter[filterName[i]].selectedValue.length != 0 && configurationFilter[filterName[i]].displayName != "Store") {
                                    filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                                }
                            }
                        }
                    }
                 

            }
        }
            if(value!="storeCode"){

                    let payload = {
                        filter: filter,
                        storeCode:this.props.getConfigStore==""?"NA":this.props.getConfigStore
                        
                    }
            
                    this.props.getStoreCodeRequest(payload)
            }
                }, 10)
}
    }
    blurHandleChange(e, id, value) {
        let filterConfig = this.props.configurationFilter
        if (filterConfig[value].changeValue == "") {
            filterConfig[value].changeValue = filterConfig[value].defaultValue
        }
       this.props.updateConfigFilter(filterConfig)

        setTimeout(() => {
            let configurationFilter = this.props.configurationFilter
            let filterName = Object.keys(configurationFilter)
            let filter = {}
            let flag = false
            for (let i = 0; i < filterName.length; i++) {
                if (configurationFilter[filterName[i]].isChecked == "True") {
                    flag=true
                    if (configurationFilter[filterName[i]].type == "T") {
                    
                        if (configurationFilter[filterName[i]].changeValue != "") {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue

                        }
                    } else if (configurationFilter[filterName[i]].type == "L") {
                        if (configurationFilter[filterName[i]].selectedValue.length != 0) {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                        }
                    }
                }
            }
            if(flag){
            let payload = {
                filter: filter,
                storeCode:this.props.getConfigStore==""?"NA":this.props.getConfigStore
                
            }
            this.props.getStoreCodeRequest(payload)
            }
        }, 10)
    }

    checkAllFilter(data) {
        let filterConfig = this.props.configurationFilter
        if (!this.props.configurationFilter[data].selectedValue.includes(this.props.configurationFilter[data].value) && this.props.configurationFilter[data].selectedValue == "") {
            let selectedValue = this.props.configurationFilter[data].selectedValue
            selectedValue.push(...this.props.configurationFilter[data].value)

        } else {
            let selectedValue = this.props.configurationFilter[data].selectedValue
            let values = this.props.configurationFilter[data].value
            for (let i = 0; i < selectedValue.length; i++) {
                for (let j = 0; j < values.length; j++) {
                    if (selectedValue[i] == values[j]) {
                        selectedValue.splice(i, 1)
                    }
                }
            }

        }
        this.props.updateConfigFilter(filterConfig)
     
        setTimeout(() => {
            let configurationFilter = this.props.configurationFilter
            let filterName = Object.keys(configurationFilter)
            let filter = {}
            let flag = false
            for (let i = 0; i < filterName.length; i++) {
                if (configurationFilter[filterName[i]].isChecked == "True") {
                    flag=true
                    if (configurationFilter[filterName[i]].type == "T") {
                        if (configurationFilter[filterName[i]].changeValue != "") {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue

                        }
                    } else if (configurationFilter[filterName[i]].type == "L") {
                        if (configurationFilter[filterName[i]].selectedValue.length != 0) {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                        }
                    }
                }
            }
            if(data!="storeCode"){
            if(flag){
            let payload = {
                filter: filter,
                storeCode:this.props.getConfigStore==""?"NA":this.props.getConfigStore
              
            }
            this.props.getStoreCodeRequest(payload)
        }
            }
        }, 50)
    }

    // __________________________________dropdown checkbox_________________________________
    valueCheck(value, data) {
        let filterConfig = this.props.configurationFilter
        if (!this.props.configurationFilter[data].selectedValue.includes(value)) {
            let selectedValue = this.props.configurationFilter[data].selectedValue
            selectedValue.push(value)
        } else {
            let selectedValue = this.props.configurationFilter[data].selectedValue
            for (let i = 0; i < selectedValue.length; i++) {
                if (selectedValue[i] == value) {
                    selectedValue.splice(i, 1)
                }
            }
        }

          this.props.updateConfigFilter(filterConfig)

   
       if(data!="storeCode"){
        setTimeout(() => {
            let configurationFilter = this.props.configurationFilter
            let filterName = Object.keys(configurationFilter)
            let filter = {}
            let flag = false
            for (let i = 0; i < filterName.length; i++) {
                if (configurationFilter[filterName[i]].isChecked == "True") {
                    flag= true
                    if (configurationFilter[filterName[i]].type == "T") {
                        if (configurationFilter[filterName[i]].changeValue != "") {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].changeValue
                        }
                    } else if (configurationFilter[filterName[i]].type == "L") {
                        if (configurationFilter[filterName[i]].selectedValue.length != 0) {
                            filter[configurationFilter[filterName[i]].columnName] = configurationFilter[filterName[i]].selectedValue.join('|')
                        }
                    }
                }
            }
            if(flag){
            let payload = {
                filter: filter,
                storeCode:this.props.getConfigStore==""?"NA":this.props.getConfigStore
                   
            }
            this.props.getStoreCodeRequest(payload)
            }
        }, 10)
       }
    }

    // ___________________________________INPUT SEARCH CODE _______________________________

    inputChange(e, id, data) {
        let filterConfig = this.props.configurationFilter
        filterConfig[data].valueSearch = e.target.value
      this.props.updateConfigFilter(filterConfig)
        this.onSearchFunction(id)
    }

    onSearchFunction(id) {
        var input = document.getElementById(id);
        var filter = input.value.toUpperCase();
        var ul = document.getElementById("myUL");
        var li = ul.getElementsByTagName("li");
        for (var i = 0; i < li.length; i++) {
            var a = li[i].getElementsByTagName("label")[0];
            var txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {

                li[i].classList.remove('hideElement');
            } else {
                li[i].classList.add('hideElement');
            }
        }
        this.displayNoResult(li.length)
    }
    displayNoResult(allLI) {
        var hiddenLILength = document.querySelectorAll('li.hideElement');
        if (allLI === hiddenLILength.length) {
            document.getElementById('noResult').classList.remove('hideElement')
        } else {
            document.getElementById('noResult').classList.add('hideElement')
        }
    }

    // ___________________________CLEAR FILTER __________________________________

    clearFilter(){
        let configurationFilters = this.props.configurationFilter
        let filterNames = this.props.labelConfigFilter
        for (let i = 0; i < filterNames.length; i++) {
            if (configurationFilters[filterNames[i]].isChecked == "True") {
                configurationFilters[filterNames[i]].columnName= configurationFilters[filterNames[i]].columnName
                configurationFilters[filterNames[i]].displayName = configurationFilters[filterNames[i]].displayName
                configurationFilters[filterNames[i]].type = configurationFilters[filterNames[i]].type
                configurationFilters[filterNames[i]].value = configurationFilters[filterNames[i]].value 
                configurationFilters[filterNames[i]].changeValue= ""
                configurationFilters[filterNames[i]].defaultValue = ""
                configurationFilters[filterNames[i]].description =  configurationFilters[filterNames[i]].description
                configurationFilters[filterNames[i]].isChecked = "False"
                configurationFilters[filterNames[i]].selectedValue = []
                configurationFilters[filterNames[i]].valueSearch = ""
            }
        }
        this.props.updateConfigFilter(configurationFilters)
        this.props.updateApply()
     
    }
    
    render() {

        let rows = [];
        Object.keys(this.props.configurationFilter.config).map( key =>{
            let length = this.props.configurationFilter.config[key].value.length;
            let options = [];
            if( this.props.configurationFilter.config[key].type == "List"){
               for( let i = 0; i < length; i++){                
                  options.push(<option key={this.props.configurationFilter.config[key].value[i]}>{this.props.configurationFilter.config[key].value[i]}</option>);
                 }
                 rows.push(<tr key={key}>
                    <td>{this.props.configurationFilter.config[key].columnName}</td>
                    <td><select>{options}</select></td>
                    </tr>);

            } else{
                rows.push(<tr key={key}>
                    <td>{this.props.configurationFilter.config[key].columnName}</td>
                    <td><input type="text"/></td>
                    </tr>);
            }
           
        });

        return (
            <div className="modal display_block" id="pocolorModel">
                <div className="backdrop display_block"></div>
                <div className="modal_Indent display_block manageRuleModal filterModalMain">
                    <div className="modal-content modalpoColor modalShow pad-0">
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="modal_Color selectVendorPopUp supplierSelectModal">
                                <div className="alignMiddle pad-15">
                                    <div className="col-md-7">
                                        {/* <h2>Filters</h2> */}
                                        <h2>Config Planning Parameter</h2>
                                    </div>
                                    <div className="col-md-5">
                                        <div className="textRight">
                                            <div className="pad-0 filterBtns">
                                                {/* <button type="button" className="filterBtn m-rgt-15" onClick={(e) => this.props.onFilterApply(e)}>Apply Filter</button>
                                                <button type="button" className="filterBtn m-rgt-15" onClick={(e) => this.clearFilter()}>Clear Filter</button> */}
                                                <button type="button" className="filterBtn removeBtn" onClick={(e) => this.props.closeFilter(e)}>Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0 predectionTime filterDiv">
                                <div className="chooseYear ">
                                <div className="createConifigurations displayInline width100">
                                  <table>
                                      <tbody>
                                          {rows}
                                      </tbody>
                                  </table>
                                    {/* {this.props.labelConfigFilter.map((data, key) => (
                                        <div className="col-md-3 pad-0 eachConfigMain" key={key}>
                                            <div className="eachConfig ">
                                                <div className="col-md-6 pad-0"><h3>{this.props.configurationFilter[data].displayName}</h3></div>
                                                <div className="col-md-6 pad-0">
                                                    <div className="verticalTop">
                                                        <ul className="pad-0 posRelative">
                                                            <li><label className="checkBoxLabel0 displayPointer pad-0 posInherit">
                                                                <input type="checkBox" checked={this.props.configurationFilter[data].isChecked == "True"} onChange={(e) => this.checkFilter(data)} id={this.props.configurationFilter[data].displayName} /> <span className="checkmark1"></span></label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 pad-0 m-top-5">
                                                    {this.props.configurationFilter[data].type == "L" ? <div className="dropdown-toggle displayPointer selectWidth genericSelect2 displayInGrid afteNone itemCenter" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
                                                        {this.props.configurationFilter[data].selectedValue.length != 0 ? this.props.configurationFilter[data].selectedValue.join(",") : "Select " + this.props.configurationFilter[data].displayName}
                                                    </div> : null}
                                                    {this.props.configurationFilter[data].type == "T" ?
                                                        <input type="text" className="selectWidth genericSelect2 createConfigInput displayInGrid itemCenter pad-7" id={"input " + this.props.configurationFilter[data].displayName} value={this.props.configurationFilter[data].changeValue} placeholder={"Enter " + this.props.configurationFilter[data].displayName} onChange={(e) => this.handleChangeFilters(e, "input" + this.props.configurationFilter[data].displayName, data)} onBlur={(e) => this.blurHandleChange(e, "input" + this.props.configurationFilter[data].displayName, data)} /> : null}
                                                        

                                                    {this.props.configurationFilter[data].displayName == "Store" && this.props.configurationFilter[data].value == "ALL-STORES" ? <div className="dropdownSketchFilter right0 skechersDrop dropDownSkech  dropdown-menu" aria-labelledby="dropdownMenu1">
                                                        <div className="SketchFilter">
                                                            <p>Choose Stores from below list :</p>
                                                            <input type="text" placeholder="Type to Search…" value={storeSearch} onChange={(e) => this.Change(e)} />
                                                        </div>
                                                        <div className="sketchStoreDrop">
                                                            <div className="lftStoreDiv m-rgt-20">
                                                                <h2>Choose Stores</h2>
                                                                <div className="storeChoose">
                                                                    <ul className="list-inline">
                                                                        {resultStoreCodeList.length == 0 ? null : resultStoreCodeList.map((dataa, i) => <li key={i} onClick={(e) => this.onnChange(`${dataa.storeCode}`, dataa.storeName, data)} >

                                                                            <span className="onHover"></span>
                                                                            <p className="contentStore pad-5">{dataa.storeName}</p>

                                                                        </li>)}

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="lftStoreDiv">
                                                                <h2>Selected Stores</h2>
                                                                <div className="storeChoose pad-10">
                                                                    <ul className="list-inline">
                                                                        <li>
                                                                            {this.props.selected.length == 0 ? null : this.props.selected.map((dataa, i) => <div key={dataa.storeCode} className="">
                                                                                <p className="contentStore">{dataa.storeName} </p>
                                                                                <span onClick={(e) => this.onDelete(dataa.storeCode, dataa.storeName, data)} value={dataa.storeCode} className="close_btn_demand" aria-hidden="true">&times;</span>
                                                                            </div>)}
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> :
                                                        <div className="searchDropdown dropdown-menu" aria-labelledby="dropdownMenu1">
                                                            <input type="search" className="textIndent30" placeholder="Search..." id={"search" + this.props.configurationFilter[data].displayName} value={this.props.configurationFilter[data].valueSearch} onChange={(e) => this.inputChange(e, "search" + this.props.configurationFilter[data].displayName, data)} />
                                                            <ul className="pad-0 posRelative dropdownCheckBox dropScroll m-top-10" id="myUL">
                                                                <li><label className="checkBoxLabel0 displayPointer pad-0 posInherit">
                                                                    <input type="checkBox" checked={this.props.configurationFilter[data].value.length == this.props.configurationFilter[data].selectedValue.length} onChange={(e) => this.checkAllFilter(data)} id="selectAll" />
                                                                    <label htmlFor="selectAll">Select All</label>
                                                                    <span className="checkmark1"></span></label>
                                                                </li>
                                                                {this.props.configurationFilter[data].value.length != 0 ?
                                                                    this.props.configurationFilter[data].value.map((vdata, valuekey) =>
                                                                        <li key={valuekey} ><label className="checkBoxLabel0 displayPointer pad-0 posInherit">
                                                                            <input type="checkBox" id={vdata} checked={this.props.configurationFilter[data].selectedValue.includes(vdata)} onChange={(e) => this.valueCheck(vdata, data)} />
                                                                            <label htmlFor={vdata}>{vdata}</label>
                                                                            <span className="checkmark1"></span></label>
                                                                        </li>)
                                                                    : <li ><label>No Data Found</label></li>}
                                                                <li className="hideElement" id="noResult">
                                                                    <label> No Data Found</label>
                                                                </li>
                                                            </ul>
                                                        </div>}

                                                </div>
                                            </div>
                                        </div>
                                    ))} */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}
