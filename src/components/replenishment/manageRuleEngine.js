import React from 'react';
import BraedCrumps from '../breadCrumps';
import SideBar from '../sidebar';
import editIcon from '../../assets/editIcon.svg'
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
import { AssingManageRuleModal } from './assignManageRuleModal';

import ViewAttributesModal from './viewAttributesModal';


class ManageRuleEngine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            updatedJobData : '',
            activeJobId: '',
            configName: '',
            jobNames: [],
            assignModal: false,
            editAdd: "ASSIGN",
            selectedJobName: "",
            jobData: [],            
            isEnabled: "TRUE",
            selection: false,
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",
            viewAttr: false,
            selectedConfigName: "",
            confignameCheck: ""

        }
    }


    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
    }

    componentDidMount() {
        this.props.getJobNameRequest('data')
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.getJobName.isSuccess) {
            let jobId = nextProps.replenishment.getJobName.data.resource == null ? '' : nextProps.replenishment.getJobName.data.resource[0].id;
            this.setState({
                jobNames: nextProps.replenishment.getJobName.data.resource == null ? [] : nextProps.replenishment.getJobName.data.resource ,
                loader: false,
                activeJobId: jobId
            })
            this.getJobsDetails(jobId)
            this.props.getJobNameClear()
        } else if (nextProps.replenishment.getJobName.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getJobName.message.error == undefined ? undefined : nextProps.replenishment.getJobName.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getJobName.message.status,
                errorCode: nextProps.replenishment.getJobName.message.error == undefined ? undefined : nextProps.replenishment.getJobName.message.error.errorCode
            })
            this.props.getJobNameClear();
        } else if (nextProps.replenishment.getJobName.isLoading) {
            this.setState({
                loader: true
            })
        }
        if (nextProps.replenishment.getJobData.isSuccess) {
            if (nextProps.replenishment.getJobData.data.resource != null) {
                let data = nextProps.replenishment.getJobData.data.resource.response;
                this.setState({
                    jobData: data,
                    configName: nextProps.replenishment.getJobData.data.resource.response.configName,
                    updatedJobData: '',
                }, () => {
                    data.map(item => {
                        if(item.type.toUpperCase() == 'LIST'){[
                            document.getElementById(`lov${item.id}`).value = 'initial'
                        ]}
                    })
                })
                
            } else {
                this.setState({
                    jobData: [],
                })
            }
            this.setState({
                loader: false
            })
            this.props.getJobDataClear();
        } else if (nextProps.replenishment.getJobData.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.getJobData.message.error == undefined ? undefined : nextProps.replenishment.getJobData.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getJobData.message.status,
                errorCode: nextProps.replenishment.getJobData.message.error == undefined ? undefined : nextProps.replenishment.getJobData.message.error.errorCode
            })
            this.props.getJobDataClear();
        } else if (nextProps.replenishment.getJobData.isLoading) {
            this.setState({
                loader: true
            })
        }

        if(nextProps.replenishment.updateJobData.isSuccess){
            this.setState({
                success: nextProps.replenishment.updateJobData.data.message != '' ? nextProps.replenishment.updateJobData.data.message : 'Changes saved Successfully!',
                successMessage: nextProps.replenishment.updateJobData.data.message != '' ? nextProps.replenishment.updateJobData.data.message : 'Changes saved Successfully!',
                loader: false
            })
            this.clearChanges()
            this.props.updateJobDataClear();
        } else if(nextProps.replenishment.updateJobData.isError){
            this.setState({
                errorMessage: nextProps.replenishment.getJobData.message.error == undefined ? undefined : nextProps.replenishment.getJobData.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.getJobData.message.status,
                errorCode: nextProps.replenishment.getJobData.message.error == undefined ? undefined : nextProps.replenishment.getJobData.message.error.errorCode
            })
        } else if(nextProps.replenishment.updateJobData.isLoading){
            this.setState({
                loader: true
            })
        }
         
        if (nextProps.replenishment.glueParameter.isSuccess) {

            this.setState({
                loader: false,
                isEnabled:"TRUE"
            })
            this.props.getJobNameRequest('data')
            this.props.glueParameterClear();
        } else if (nextProps.replenishment.glueParameter.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.glueParameter.message.status,
                errorMessage: nextProps.replenishment.glueParameter.message.error == undefined ? undefined : nextProps.replenishment.glueParameter.message.error.errorMessage,
                errorCode: nextProps.replenishment.glueParameter.message.error == undefined ? undefined : nextProps.replenishment.glueParameter.message.error.errorCode
            })
            this.props.glueParameterClear();
        } else if (nextProps.replenishment.glueParameter.isLoading) {
            this.setState({
                loader: true
            })
        }

    }

    getJobsDetails = (data) =>{
        this.props.getJobDataRequest(data)
        this.setState({
            activeJobId: data
        })
    }

    changeJobData = (e, id) =>{

        let changedJobData = [...this.state.updatedJobData];
        let data = {
                    id: id,
                    defaultValue: e.target.value
                }
        if(changedJobData.filter(job => job.id == id) == '' || changedJobData ==''){
            changedJobData.push(data)
        } else {
            changedJobData =  changedJobData.map(job=>{
                if(job.id == id){
                    return data
                } else{
                    return job
                }
            })
        }
        
        
        this.setState({
            updatedJobData: changedJobData
        })
    }

    clearChanges = () =>{
        let jobs = [...this.state.jobData];
        let updatedJobs = jobs.map(job=>(
            {...job, lov:''}
        ))
        let activeJobId = this.state.activeJobId;
        this.setState({
            jobData: updatedJobs
        }, () => this.props.getJobDataRequest(activeJobId))        
    }

    saveChanges = () =>{
        this.props.updateJobDataRequest(this.state.updatedJobData)
    }

    assignedModal(jname, editAdd, configName) {
        this.setState({
            assignModal: true,
            editAdd: editAdd,
            selectedJobName: jname,
            confignameCheck: configName
        })
        // let payload = {
        //     type: 1,
        //     no: 1,
        //     jobName: jname
        // }
        let payload = {
            configName: configName
        }

        this.props.getJobDataRequest(payload)

    }
    closeAssignModal() {
        this.setState({
            assignModal: false
        })
    }

 
    changeConfig() {
        if (this.state.isEnabled=="TRUE") {
            this.setState({
                isEnabled: "FALSE"
            })
        } else {
            this.setState({
                isEnabled: "TRUE"
            })
        }

    }
    // selectedDiv(value) {
    //     this.setState({
    //         selection: true
    //     })
    //     let jobData = this.state.jobData
    //     let keysJobData = this.state.keysJobData
    //     for (let i = 0; i < keysJobData.length; i++) {
    //         if (keysJobData[i] == value) {
    //             if (jobData[value].isSelected == "TRUE") {
    //                 jobData[value].isSelected = "FALSE"
    //                 this.setState({
    //                     isEnabled: "FALSE"
    //                 })
    //             } else {
    //                 jobData[value].isSelected = "TRUE"
    //                 this.setState({
    //                     isEnabled: "TRUE"
    //                 })

    //             }
    //         } else if (keysJobData[i] != value) {
    //             jobData[keysJobData[i]].isSelected = "FALSE"
    //         }
    //     }
    //     this.setState({
    //         jobData: jobData
    //     })

    // }
    saveGlueParameter() {

        let jobData = this.state.jobData
        let keysJobData = this.state.keysJobData
        let configName = ""
        for (let i = 0; i < keysJobData.length; i++) {
            if (jobData[keysJobData[i]].isSelected == "TRUE") {
                configName = keysJobData[i]
            }

        }
        let payload = {
            jobName: this.state.selectedJobName,
            configName: configName,
            isEnabled: this.state.isEnabled,
            isAssigned: "TRUE"
        }

        this.props.glueParameterRequest(payload)
        this.closeAssignModal()

    }
    viewAttributes(data) {
        this.setState({
            selectedConfigName: data,
            viewAttr: true
        })


    }
    closeAttr(){
        this.setState({
            viewAttr:false
        })
    }
    
    small = (str) => {
        if (str != null) {
            var str = str.toString()
            if (str.length <= 45) {
                return false;
            }
            return true;
        }
    }

    render() {
        console.log('updated', this.state.updatedJobData)
        return (
            <div>
                <div className="container-fluid pad-rgt-0 pad-l65">
                    <div className="col-lg-12 pad-0">
                        <div className="manage-rule-engine-col-3">
                            <div className="manage-rule-engine-job-history">
                                <div className="mrejh-head">
                                    <h3>Jobs List</h3>
                                </div>
                                <div className="mrejh-body">
                                    <ul className="nav nav-tabs" role="tablist">
                                        {this.state.jobNames.length == 0 ?
                                        <li className="mrejh-nojobsfound">
                                            <span className="mrejhn-head">No Jobs Found!</span>
                                            <span className="mrejhn-descri"><p>Currently no jobs has been configured to your account. Please contact support at support@supplymint.com</p></span>
                                        </li> :
                                        this.state.jobNames.map(job=>(
                                            <li className={this.state.activeJobId == job.id ? "nav-item active" : "nav-item"} key={job.id} id={job.id} onClick={() => this.getJobsDetails(job.id)}>
                                            <a className="nav-link" href="#smjobs" role="tab" data-toggle="tab">{job.jobName}</a>
                                        </li>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="manage-rule-engine-col-9">
                            <div className="tab-content">
                                <div className="tab-pane fade in active" id="smjobs" role="tabpanel">
                                    {this.state.jobNames.length == 0 ?
                                    <div className="manage-rule-engine-right">
                                        <div className="mrer-error-msg">
                                            <div className="mrerem-inner">
                                                <span className="mreremi-msg">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="information" width="14.413" height="14.413" viewBox="0 0 15.413 15.413">
                                                        <path fill="#ff8103" id="Path_1092" d="M7.706 0a7.706 7.706 0 1 0 7.706 7.706A7.715 7.715 0 0 0 7.706 0zm0 14.012a6.305 6.305 0 1 1 6.305-6.305 6.312 6.312 0 0 1-6.305 6.305z" class="cls-1"/>
                                                        <path fill="#ff8103" id="Path_1093" d="M145.936 70a.934.934 0 1 0 .934.935.935.935 0 0 0-.934-.935z" class="cls-1" transform="translate(-138.23 -66.731)"/>
                                                        <path fill="#ff8103" id="Path_1094" d="M150.7 140a.7.7 0 0 0-.7.7v4.2a.7.7 0 0 0 1.4 0v-4.2a.7.7 0 0 0-.7-.7z" class="cls-1" transform="translate(-142.994 -133.461)"/>
                                                    </svg>
                                                    Currently no jobs has been configured to your account. Please contact support at support@supplymint.com
                                                </span>
                                            </div>
                                        </div>
                                    </div>  :
                                    <div className="manage-rule-engine-right">
                                        <div className="mrer-head">
                                            <div className="mrerh-left">
                                                <h3>Planning Parameters</h3>
                                            </div>
                                            <div className="mrerh-right">
                                                {this.state.updatedJobData == '' ? <button type="button" className="mrerh-save-change btnDisabled">Save Changes</button>
                                                :<button type="button" className="mrerh-save-change" onClick={this.saveChanges}>Save Changes</button>}
                                                {this.state.updatedJobData == '' ? <button type="button" className='btnDisabled'>Cancel</button>
                                                :<button type="button" onClick={this.clearChanges}>Cancel</button> }
                                            </div>
                                        </div>
                                            <div className="mrer-body">
                                                <h3>Attributes</h3>
                                                <div className="mrerb-table">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <th><label>Param Name</label></th>
                                                                {/* <th><label>Type</label></th> */}
                                                                <th><label>Default Value</label></th>
                                                                <th><label>Value</label></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.jobData.map((data, indx) => {
                                                                return <tr key={indx}>
                                                                            <td>
                                                                                <label className="mrer-depart">{data.displayName}</label>
                                                                                <p>{`(${data.description})`}</p>
                                                                            </td>
                                                                            {/* <td><label>{data.type}</label></td> */}
                                                                            <td><label>{data.defaultValue}</label></td>
                                                                            <td>
                                                                                {data.type.toUpperCase() != 'LIST' ? 
                                                                                <input disabled={data.type.toUpperCase() == 'DISABLED'} placeholder='Please Enter a Value' onChange={ (e) => this.changeJobData(e, data.id)}></input>   :
                                                                                <select onChange={ (e) => this.changeJobData(e, data.id)} id={'lov'+data.id}>
                                                                                        <option value='initial'>Please Select!</option>
                                                                                    {(data.lov).split(",").map((dt, index)=>(
                                                                                        <option value={dt} key={index}>{dt}</option>
                                                                                    ))}
                                                                                    
                                                                                </select> 
                                                                            }
                                                                                
                                                                            </td>
                                                                        </tr>
                                                            })}                                                        
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                    </div>}
                                </div>                                
                            </div>
                        </div>
                    </div>
                    {/* <div className="container_div" id="home">
                        <div className="container-fluid">
                            <div className="container_div" id="">
                                <div className="container-fluid pad-0">
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                        <div className="replenishment_container height75vh weekelyPlanning monthlyPlanning">
                                            <div className="col-md-12 pad-0 inventoryAutoConfig">
                                                <div className="col-md-6 col-sm-6 pad-lft-0">
                                                    <ul className="list_style">
                                                        <li>
                                                            <label className="contribution_mart">
                                                                Manage Rule Engine
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <p className="master_para">Manage your rule engine Jobs Cofiguration</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="col-md-6 headerTopAction textRight">
                                                </div>
                                                <div className="col-md-12 pad-0 m-top-20">
                                                    {this.state.jobNames.length != 0 ? this.state.jobNames.map((data, key) =>
                                                        <div className="col-md-3 pad-lft-0 " key={key}>
                                                            <div className="assginRole">
                                                                <h3>Job Name</h3>
                                                                <h2 title={data.jobName}>{data.jobName}</h2>
                                                                <div className="col-md-12 marginAlignBot alignFooter pad-0 alignEnd m-top-15">
                                                                    <div className="col-md-8 pad-0">
                                                                        <label>{data.isAssigned == "TRUE" ? "Assigned Configuration" : "Unassigned Configuration"}</label>
                                                                        <p className="m0">{data.configName}</p>
                                                                    </div>
                                                                    <div className="col-md-4 pad-0 textRight">
                                                                        {data.isAssigned == "TRUE" ? <button type="button" className="editButton" onClick={(e) => this.assignedModal(data.jobName, "EDIT", data.configName)}>Edit <img src={editIcon} /></button>
                                                                            : <button type="button" className="assignBtn" onClick={(e) => this.assignedModal(data.jobName, "ASSIGN", data.configName)}>View Config</button>}
                                                                        <button type="button" className="assignBtn" onClick={(e) => this.assignedModal(data.jobName, "ASSIGN", data.configName)}>View Config</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ) : null}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {this.state.assignModal ? <AssingManageRuleModal {...this.state} {...this.props} selectedConfigName={this.state.selectedConfigName} viewAttr={this.state.viewAttr} viewAttributes={(e) => this.viewAttributes(e)} saveGlueParameter={(e) => this.saveGlueParameter(e)} closeAssignModal={(e) => this.closeAssignModal(e)} selection={this.state.selection} selectedDiv={(e) => this.selectedDiv(e)} current={this.state.current} maxPage={this.state.maxPage} isEnabled={this.state.isEnabled} changeConfig={(e) => this.changeConfig(e)} editAdd={this.state.editAdd} page={(e) => this.page(e)} selectedJobName={this.state.selectedJobName} keysJobData={this.state.keysJobData} jobData={this.state.jobData} /> : null}
                                <ViewAttributesModal />
                            </div>
                        </div>
                    </div> */}
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.viewAttr ? <ViewAttributesModal viewAttributes={(e) => this.viewAttributes(e)} closeAttr={(e)=>this.closeAttr(e)} selectedConfigName={this.state.selectedConfigName} jobData={this.state.jobData} /> : null}
            </div >


        );
    }
}

export default ManageRuleEngine;