import React from 'react';


class ViewAttributesModal extends React.Component {

  
    render() {

        let rows = [];
        Object.keys(this.props.jobData.config).map( key =>{
            let length = this.props.jobData.config[key].value.length;
            let options = [];
            if( this.props.jobData.config[key].type == "List"){
               for( let i = 0; i < length; i++){                
                  options.push(<option key={this.props.jobData.config[key].value[i]}>{this.props.jobData.config[key].value[i]}</option>);
                 }
                 rows.push(<tr id={key}>
                    <td>{this.props.jobData.config[key].columnName}</td>
                    <td>{this.props.jobData.config[key].type}</td>
                    <td>{this.props.jobData.config[key].defaultValue}</td>
                    <td><select>{options}</select></td>
                    </tr>);

            } else{
                rows.push(<tr id={key}>
                    <td>{this.props.jobData.config[key].columnName}</td>
                    <td>{this.props.jobData.config[key].type}</td>
                    <td>{this.props.jobData.config[key].defaultValue}</td>
                    <td><input type="text"/></td>
                    </tr>);
            }
           
        });

        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block viewAttributesModalMain">
                        <div className="col-md-12 col-sm-12 modalpoColor modal-content modalShow pad-0 viewAttributesModal">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle">
                                    <div className="col-md-6 pad-0">
                                        <h2>Attributes</h2>
                                    </div>
                                    <div className="col-md-6 pad-0">

                                    </div>
                                </div>
                                <div className="row modalContent margin-right0 m-lft-0">
                                    <div className="col-md-12">
                                        <label>Config Name</label>
                                        <h3>{this.props.jobData.response[0].configName}</h3>
                                    </div>
                                    <div className="col-md-12 m-top-30 padRightNone">
                                        <div className="col-md-7 pad-0">
                                            <div className="row rowMargin">
                                                {/* {Object.keys(this.props.jobData[this.props.selectedConfigName].data).map((da, key) =>
                                                <div className="col-md-4 pad-lft-0" key={key}>
                                                    <h4>{this.props.jobData[this.props.selectedConfigName].data[da].displayName}</h4>
                                                    <p>{this.props.jobData[this.props.selectedConfigName].data[da].value.join(',')}</p>
                                                </div>
                                                )} */}
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th>Param Name</th>
                                                            <th>Type</th>
                                                            <th>Default Value</th>
                                                            <th>Value</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       {rows}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                
                                <div className="modalBottom m-top-40">
                                    <button type="button" className="blueCloseBtn" onClick={(e)=>this.props.closeAttr(e)}>Close</button>
                                </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}
export default ViewAttributesModal;