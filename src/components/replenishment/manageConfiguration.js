import React from 'react';
import Cron from 'custom-cron-generator'; 

class ManageConfiguration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.data.id,
            jobName: this.props.data.jobName,
            cronExpression: this.props.data.cronExpression,
            active: this.props.data.active,
            frequency: this.props.data.frequency,
            manageRuleId: this.props.data.manageRuleId
            // openJobOption: false,
            // showPlanningAttribute: false,
            // showFilter: false,
            // cronExpression: "",

            // jobNameOptionData: [],
            // jobName: "Select Job",
            // planningAttribute: [],

            // locationFilter: "",
            // divisionFilter: "",
            // sectionFilter: "",
            // departmentFilter: "",
            // brandFilter: "",
            // mrpRangeFilter: "",
        }
    }

    // componentDidMount(){
    //     this.props.getAllJobRequest('data');
    // }

    // static getDerivedStateFromProps(nextProps, preState){
    //     if (nextProps.replenishment.getAllJob.isSuccess) {
    //         return{
    //             jobNameOptionData: nextProps.replenishment.getAllJob.data.resource == null ? [] : nextProps.replenishment.getAllJob.data.resource,
    //         }
    //     }
    //     return null
    // }

    // componentDidUpdate(){
    //     if (this.props.replenishment.getJobData.isSuccess && this.props.replenishment.getJobData.data.resource !== null) {
    //         this.setState({
    //              planningAttribute: this.props.replenishment.getJobData.data.resource == null ? [] : this.props.replenishment.getJobData.data.resource.response !== null &&
    //                                 this.props.replenishment.getJobData.data.resource.response !== undefined ? this.props.replenishment.getJobData.data.resource.response : [],
    //         })
    //     }
    //     this.props.getJobDataClear()
    // }

    openJobOption =(e)=> {
        this.setState({ openJobOption: !this.state.openJobOption });
    }

    selectJob =(e, id)=>{
       this.setState({ 
            jobName: e.target.outerText,
            openJobOption: false,
            showPlanningAttribute: false,
            showFilter: false
        },()=>{
           if(id !== undefined)
             this.props.getJobDataRequest(id);
           else
              this.setState({
                  planningAttribute: [],
              })  
        })
    }

    showPlanningAttribute =(e)=> {
        this.setState({
            showPlanningAttribute: !this.state.showPlanningAttribute
        });
    }

    handlePlanningValue =(e, data)=>{
        let planningAttribute = [...this.state.planningAttribute];
        planningAttribute.map( val => val.id == data.id && (val.planningTextValue = e.target.value))
        this.setState({ planningAttribute})
    }

    showFilter =(e)=> {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    handleFilterValue =(e, type)=>{
        if(type == "location")
           this.setState({ locationFilter: e.target.value})
        else if(type == "division")
           this.setState({ divisionFilter: e.target.value})
        else if(type == "section")
           this.setState({ sectionFilter: e.target.value})
        else if(type == "department")
           this.setState({ departmentFilter: e.target.value})
        else if(type == "brand")
           this.setState({ brandFilter: e.target.value})
        else if(type == "mrp")
           this.setState({ mrpRangeFilter: e.target.value})
    }


    saveConfiguration =(e)=>{
        this.props.saveConfiguration({
            id: this.state.id,
            cronExpression: this.state.cronExpression,
            active: this.state.active,
            frequency: document.getElementsByClassName("cron_builder")[0] == undefined ? "" : document.getElementsByClassName("cron_builder")[0].getElementsByClassName("active")[0].innerText,
            manageRuleId: this.props.data.manageRuleId
        });
        // let stack = [];
        // this.state.planningAttribute.map( data =>{
        //     let paylaod ={
        //             displayName: data.displayName,
        //             type: data.type,
        //             defaultValue: data.defaultValue,
        //             updatedValue: data.planningTextValue
        //         }
        //         stack.push(paylaod)
        // })
        // let payload = {
        //     jobName: this.state.jobName,
        //     cronExpression: this.state.cronExpression,
        //     planningAttribute: {...stack},
        //     locationFilter: this.state.locationFilter,
        //     divisionFilter: this.state.divisionFilter,
        //     sectionFilter: this.state.sectionFilter,
        //     departmentFilter: this.state.departmentFilter,
        //     brandFilter: this.state.brandFilter,
        //     mrpRangeFilter: this.state.mrpRangeFilter,
        // }
        // this.props.createTriggerRequest(payload)
    }

    render() {
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content create-new-configuration">
                    <div className="cnc-head">
                        <h3>Update Trigger</h3>
                        <button type="button" onClick={this.props.closeEditConfiguration}><img src={require('../../assets/clearSearch.svg')} /></button>
                    </div>
                    <div className="cnc-body">
                        <div className="cncb-menu">
                            <button type="button">{this.state.jobName}</button>
                            {/* {this.state.openJobOption &&
                            <div className="cncbm-dropdown">
                                <ul>
                                    <li key="0000" onClick={this.selectJob}>Select Job</li>
                                    {this.state.jobNameOptionData.length && this.state.jobNameOptionData.map((data,key) => 
                                      <li key={key} onClick={(e) =>this.selectJob(e, data.id)}>{data.jobName}</li>
                                    )}
                                </ul>
                            </div>} */}
                        </div>
                        <div className="cncb-menu">
                            <Cron
                            onChange={(e)=> {this.setState({cronExpression: e})}}
                            value={this.state.cronExpression}
                            showResultText={true}
                            showResultCron={true}
                            />
                        </div>
                        <div className="cncb-menu-toggle">
                            <div className="nph-switch-btn">
                                <label className="tg-switch" >
                                    {this.state.active == 1 ? <input type="checkbox" checked onChange={() => this.setState({active: 0})} /> : <input type="checkbox" onChange={() => this.setState({active: 1})} />}
                                    <span className="tg-slider tg-round"></span>
                                    <span className="nph-wbtext">{this.state.active == 1 ? "Enabled" : "Disabled"}</span>
                                    {/* <span className="nph-wbtext">Enable</span> */}
                                </label>
                            </div>
                        </div>
                        {/* <div className="cncb-planning">
                            <div className="cncbp-head">
                                <h3>Planning Attributes</h3>
                                <button className={this.state.showPlanningAttribute === false ? "" : "orfh-btn"} onClick={(e) => {this.state.planningAttribute.length && this.showPlanningAttribute(e)}}>
                                    <img src={require('../../assets/down-arrow-new.svg')} />
                                </button>
                            </div>
                            {this.state.showPlanningAttribute && this.state.planningAttribute.length &&
                            <div className="cncbp-table">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th><label>Param Name</label></th>
                                            <th><label>Type</label></th>
                                            <th><label>Default Value</label></th>
                                            <th><label>Value</label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.planningAttribute.length && this.state.planningAttribute.map( (data, key) => <tr key={key}>    
                                            <td><label>{data.displayName}</label></td>
                                            <td><label>{data.type}</label></td>
                                            <td><label>{data.defaultValue}</label></td>
                                            <td>
                                                {data.type == "List" ? <select onChange={(e) => this.handlePlanningValue(e,data)}>
                                                    {data.lov.length && data.lov.split(",").map( (data, key) =>
                                                    <option key={key} value={data}>{data}</option>)}
                                                </select> :
                                                <input type="text" onChange={(e) =>this.handlePlanningValue(e, data)} value={data.planningTextValue == undefined ? "" : data.planningTextValue}/>}
                                            </td>
                                        </tr>)}
                                    </tbody>
                                </table>
                            </div>}
                        </div> */}
                        {/* <div className="cncb-planning m-top-30">
                            <div className="cncbp-head">
                                <h3>Filter</h3>
                                <button className={this.state.showFilter === false ? "" : "orfh-btn"} onClick={(e) => this.showFilter(e)}>
                                    <img src={require('../../assets/down-arrow-new.svg')} />
                                </button>
                            </div>
                            {this.state.showFilter &&
                            <div className="cncbp-filter m-top-10">
                                <h3>Location Filter</h3>
                                <div className="cncbpf-location">
                                    <label>Location</label>
                                    <select onChange={(e) => this.handleFilterValue(e, "location")}>
                                        <option>Site1</option>
                                    </select>
                                </div>
                                <div className="cncbpf-item">
                                    <h3>Item Filter</h3>
                                    <div className="cncbpfi-inner pad-rgt-7">
                                        <label>Division</label>
                                        <select onChange={(e) => this.handleFilterValue(e, "item")}>
                                            <option>Select Division</option>
                                        </select>
                                    </div>
                                    <div className="cncbpfi-inner pad-lft-7">
                                        <label>Section</label>
                                        <select onChange={(e) => this.handleFilterValue(e, "section")}>
                                            <option>Select Section</option>
                                        </select>
                                    </div>
                                    <div className="cncbpfi-inner pad-rgt-7">
                                        <label>Department</label>
                                        <select onChange={(e) => this.handleFilterValue(e, "department")}>
                                            <option>Select Department</option>
                                        </select>
                                    </div>
                                    <div className="cncbpfi-inner pad-lft-7">
                                        <label>Brand</label>
                                        <select onChange={(e) => this.handleFilterValue(e, "brand")}>
                                            <option>Select Brand</option>
                                        </select>
                                    </div>
                                    <div className="cncbpfi-inner pad-rgt-7">
                                        <label>MRP Range</label>
                                        <select onChange={(e) => this.handleFilterValue(e, "mrp")}>
                                            <option>Select MRP Range</option>
                                        </select>
                                    </div>
                                </div>
                            </div>}
                        </div> */}
                    </div>
                    <div className="cnc-footer m-top-20">
                        <button type="button" onClick={(e) => { this.state.jobName !== "Select Job" && this.saveConfiguration(e)}}>Update Trigger</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ManageConfiguration;