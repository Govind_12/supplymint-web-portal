import React from 'react';
import { Link } from 'react-router-dom';
import successIcon from '../../assets/greencheck.png'
import closeSearch from "../../assets/close-recently.svg"
import _ from 'lodash';
import cross from '../../assets/group-4.svg'
class ColoumSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            coloumSetting: this.props.coloumSetting,
            search: "",
            tabValue:1,
            isProcPage: window.location.hash == '#/purchase/purchaseIndentHistory' 
                        || window.location.hash == '#/purchase/purchaseOrderHistory' ? true : false,
        }
        this.myRef = React.createRef();
        this.dragNode = React.createRef();
    }
    componentDidMount(){
        this.setState({
            tabValue:this.props.tabVal
        })
    }
   
    
    onDragListItemStart(e,key){
        this.props.handleDragStart(e,key,this.myRef,this.dragNode)
    }
    
    handleDragEnter(e,key){
        this.props.handleDragEnter(e,key,this.myRef,this.dragNode)
    }

    openColoumSetting(data) {
        this.props.openColoumSetting(data)
    }

    closeColumns(e,data) {
        e.preventDefault();
        this.props.closeColumn(data)
    }
    handleChange(e) {
        this.setState({
            search: e.target.value
        })
    }
    pushColumnData(e,data){
        e.preventDefault();
        this.props.pushColumnData(e,data)
    }
    clearSearch() {
        this.setState({
            search: ""
        })
    }
    render() {
        const { search, isProcPage } = this.state
        //console.log(this.props.mainAvailableHeaders,"mainAvailableHeaders")
        var result = this.props.tabVal == 1 ?
         _.filter(this.props.mainAvailableHeaders, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        }):
        this.props.tabVal == 2 ?
        _.filter(this.props.setAvailableHeaders, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        }) :
        this.props.tabVal == 3 ?
        (!isProcPage ?
            _.filter(this.props.itemAvailableHeaders, function (data) {
                return _.startsWith(data.toLowerCase(), search.toLowerCase());
            }) : 
            _.filter(this.props.setUdfAvailableHeaders, function (data) {
                return _.startsWith(data.toLowerCase(), search.toLowerCase());
            })
        ) :
        this.props.tabVal == 4 ? 
        _.filter(this.props.itemUdfAvailableHeaders, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        }) :
        this.props.tabVal == 5 ? 
        _.filter(this.props.catDescAvailableHeaders, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        }) :
        this.props.tabVal == 6 ? 
        _.filter(this.props.lineItemAvailableHeaders, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        }) : []
        return (
            <div>
                {this.props.coloumSetting ? <div className="columnFilterGeneric" >
                    <span className="glyphicon glyphicon-menu-right" onClick={(e) => this.openColoumSetting("false")}></span>
                    <div className="columnSetting" onClick={(e) => this.openColoumSetting("false")}>Columns Setting</div>
                    <div className="backdrop-transparent"></div>
                    <div className="columnFilterDropDown custom-header-modal">
                        <div className="col-lg-12 filterHeader chm-head">
                            <div className="col-lg-7 col-md-7 pad-0">
                                <div className="chmh-search">
                                    <input type="search" value={this.state.search} onChange={(e) => this.handleChange(e)} placeholder="Search Columns…" />
                                    {this.state.search != "" ? <div className="crossIcon"><img src={require('../../assets/clearSearch.svg')} onClick={(e) => this.clearSearch(e)} /></div> : null}
                                    <img src={require('../../assets/searchicon.svg')} />
                                </div>
                            </div> 
                            <div className="col-lg-5 col-md-5 pad-right-0 columns">
                                {/* <div className="col-lg-7 pad-0">
                                    <h3>Visible Columns <span>{this.props.headerCondition ? this.props.getHeaderConfig.length : this.props.customHeadersState.length}</span></h3>
                                </div>  */}
                                {this.props.tabVal == 1 ?
                                <div className="chmh-right">
                                    <button className="resetBtn" onClick={(e) => this.props.resetColumnConfirmation(e)}>Reset to default</button>
                                    <button className={ !this.props.changesInMainHeaders ? "opacity saveBtn": "saveBtn" } onClick={(e) => !this.props.changesInMainHeaders ? null : this.props.saveColumnSetting(e)}>Save</button>:
                                </div>:
                                this.props.tabVal == 2 ?
                                <div className="chmh-right">
                                    <button className= "resetBtn" onClick={(e) => this.props.resetColumnConfirmation(e)}>Reset to default</button>
                                    <button className={ !this.props.changesInSetHeaders ? "opacity saveBtn": "saveBtn" } onClick={(e) =>!this.props.changesInSetHeaders ?null : this.props.saveColumnSetting(e)}>Save</button>
                                </div>:
                                this.props.tabVal == 3 ?
                                !isProcPage ?
                                    <div className="chmh-right">
                                        <button className="resetBtn" onClick={(e) => this.props.resetColumnConfirmation(e)}>Reset to default</button>
                                        <button className={!this.props.changesInItemHeaders ? "opacity saveBtn": "saveBtn" } onClick={(e) =>!this.props.changesInItemHeaders ?null : this.props.saveColumnSetting(e)}>Save</button>
                                    </div> : 
                                    <div className="chmh-right">
                                        <button className="resetBtn" onClick={(e) => this.props.resetColumnConfirmation(e)}>Reset to default</button>
                                        <button className={!this.props.changesInSetUdfHeaders ? "opacity saveBtn": "saveBtn" } onClick={(e) =>!this.props.changesInSetUdfHeaders ? null : this.props.saveColumnSetting(e)}>Save</button>
                                    </div>
                                :
                                this.props.tabVal == 4 ?
                                <div className="chmh-right">
                                    <button className= "resetBtn" onClick={(e) => this.props.resetColumnConfirmation(e)}>Reset to default</button>
                                    <button className={ !this.props.changesInItemUdfHeaders ? "opacity saveBtn": "saveBtn" } onClick={(e) =>!this.props.changesInItemUdfHeaders ?null : this.props.saveColumnSetting(e)}>Save</button>
                                </div>:
                                this.props.tabVal == 5 ?
                                <div className="chmh-right">
                                    <button className= "resetBtn" onClick={(e) => this.props.resetColumnConfirmation(e)}>Reset to default</button>
                                    <button className={ !this.props.changesInCatDescHeaders ? "opacity saveBtn": "saveBtn" } onClick={(e) =>!this.props.changesInCatDescHeaders ?null : this.props.saveColumnSetting(e)}>Save</button>
                                </div>:
                                this.props.tabVal == 6 ?
                                <div className="chmh-right">
                                    <button className= "resetBtn" onClick={(e) => this.props.resetColumnConfirmation(e)}>Reset to default</button>
                                    <button className={ !this.props.changesInLineItemHeaders ? "opacity saveBtn": "saveBtn" } onClick={(e) =>!this.props.changesInLineItemHeaders ?null : this.props.saveColumnSetting(e)}>Save</button>
                                </div> : null}
                            </div>
                        </div>
                        <div className="col-lg-12 pad-0">
                            <div className="chm-body">
                                <div className="chmb-sidebar">
                                    {this.props.isReportPageFlag === undefined ? 
                                        <ul className="chmbs-menu">
                                            <li className={this.props.tabVal == 1 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(1)}><span>Main</span></li>
                                            {window.location.hash == '#/purchase/purchaseIndentHistory' || window.location.hash == '#/purchase/purchaseOrderHistory' ?
                                            <li className={this.props.tabVal == 2 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(2)}><span>Item</span></li>:
                                            <li className={this.props.tabVal == 2 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(2)}><span>Set</span></li>}
                                            { !isProcPage ? 
                                                <li className={this.props.tabVal == 3 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(3)}><span>Item</span></li>
                                                : (<ul className="chmbs-menu">{this.props.getSetUdfHeaderConfig.length || this.props.setUdfCustomHeadersState.length ? <li className={this.props.tabVal == 3 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(3)}><span>SetUdf</span></li> : null}
                                                  {this.props.getItemUdfHeaderConfig.length || this.props.itemUdfCustomHeadersState.length ? <li className={this.props.tabVal == 4 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(4)}><span>ItemUdf</span></li> : null}
                                                  {this.props.getCatDescHeaderConfig.length || this.props.catDescCustomHeadersState.length ? <li className={this.props.tabVal == 5 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(5)}><span>CatDesc</span></li> : null}
                                                 <li className={this.props.tabVal == 6 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(6)}><span>LineItem</span></li></ul>)     
                                            }
                                        </ul>
                                    : this.props.isInvoicePageFlag === undefined ? 
                                       <ul className="chmbs-menu">
                                          <li className={this.props.tabVal == 1 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(1)}><span>Main</span></li>
                                       </ul> :
                                       <ul className="chmbs-menu">
                                          <li className={this.props.tabVal == 1 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(1)}><span>Main</span></li>
                                          <li className={this.props.tabVal == 2 ? "chmbsm-item active":"chmbsm-item"} onClick={()=>this.props.onHeadersTabClick(2)}><span>Expand</span></li>
                                       </ul>
                                    }
                                </div>
                                <div className="chmb-left">
                                    <div className="chmbl-head">
                                        <h3>Available Headers</h3>
                                        <span>Total Items <span className="bold">
                                        {result.length}</span></span>
                                    </div>
                                    <ul className="chmbl-headers">
                                        {result.length != 0 ? result.map((data, key) => (
                                        <li key={key} className="chmblh-items">
                                            <label className= "checkBoxLabel0" onClick={(e) => this.pushColumnData(e,data)}>
                                                <input type="checkBox" />
                                                <span className="checkmark1"></span>
                                                {data}
                                            </label> 
                                        </li>
                                        )) : <div className="chmbl-nodata">
                                            <img src={require('../../assets/no-header.svg')} />
                                            <span>No Headers available</span>
                                            </div>}
                                    </ul>
                                </div>
                                <div className="chmb-right">
                                    <div className="chmbr-head">
                                        <h3>Selected Headers</h3>
                                        <span>Total Items <span className="bold"></span>
                                        {this.props.tabVal == 1 ? 
                                        this.props.headerCondition ?
                                        this.props.getMainHeaderConfig.length :
                                        this.props.mainCustomHeadersState.length :
                                        this.props.tabVal == 2 ? 
                                        this.props.setHeaderCondition ?
                                        this.props.getSetHeaderConfig.length :
                                        this.props.setCustomHeadersState.length :
                                        this.props.tabVal == 3 ? 
                                             !isProcPage ?
                                            this.props.itemHeaderCondition ?
                                            this.props.getItemHeaderConfig.length :
                                            this.props.itemCustomHeadersState.length
                                            : this.props.setUdfHeaderCondition ?
                                            this.props.getSetUdfHeaderConfig.length :
                                            this.props.setUdfCustomHeadersState.length
                                        : this.props.tabVal == 4 ? 
                                        this.props.itemUdfHeaderCondition ?
                                        this.props.getItemUdfHeaderConfig.length :
                                        this.props.itemUdfCustomHeadersState.length :
                                        this.props.tabVal == 5 ? 
                                        this.props.catDescHeaderCondition ?
                                        this.props.getCatDescHeaderConfig.length :
                                        this.props.catDescCustomHeadersState.length :
                                        this.props.tabVal == 6 ? 
                                        this.props.lineItemHeaderCondition ?
                                        this.props.getLineItemHeaderConfig.length :
                                        this.props.lineItemCustomHeadersState.length :    
                                        null}</span>
                                    </div>
                                    <ul className="chmbr-headers">
                                    {console.log('mainHeader', this.props.headerCondition, 'config', this.props.getMainHeaderConfig, 'custom', this.props.mainCustomHeadersState)}
                                        {this.props.tabVal == 1 ? 
                                        this.props.headerCondition ?
                                        this.props.getMainHeaderConfig.map((data, key) => (
                                            <li key={key} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,key)} onDragEnter={(e)=>this.handleDragEnter(e,key)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{data}</span>
                                                    {this.props.mandateHeaderMain.length != 0 ? !this.props.mandateHeaderMain.includes(data) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span onClick={(e) => this.closeColumns(e,data)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                    </span>}
                                                    {this.props.mandateHeaderMain.length != 0 ? this.props.mandateHeaderMain.includes(data) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : <span className="chmbrhi-fixed">Fixed</span>}
                                                </span>
                                            </li>
                                        )) : this.props.mainCustomHeadersState.map((dataa, keyy) => (
                                            <li key={keyy} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,keyy)} onDragEnter={(e)=>this.handleDragEnter(e,keyy)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text 2">{dataa}</span>
                                                    {this.props.mandateHeaderMain.length != 0 ? !this.props.mandateHeaderMain.includes(dataa) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                    {this.props.mandateHeaderMain.length != 0 ? this.props.mandateHeaderMain.includes(dataa) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : <span className="chmbrhi-fixed">Fixed</span>}
                                                    {/* <span className="chmbrhi-fixed">Fixed</span> */}
                                                </span>
                                            </li>
                                        )):null}
                                        {console.log('item', this.props.setHeaderCondition, 'config', this.props.getSetHeaderConfig, 'custom', this.props.setCustomHeadersState)}
                                        {this.props.tabVal == 2 ? this.props.setHeaderCondition ? this.props.getSetHeaderConfig.map((data, key) => (
                                            <li key={key} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,key)} onDragEnter={(e)=>this.handleDragEnter(e,key)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                <span className="chmbrhi-text">{data}</span>
                                                {this.props.mandateHeaderSet.length != 0 ? !this.props.mandateHeaderSet.includes(data) && 
                                                    <span className="chmbrhi-close"  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                     {this.props.mandateHeaderSet.length != 0 ? this.props.mandateHeaderSet.includes(data) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : <span className="chmbrhi-fixed">Fixed</span>}
                                                </span>
                                            </li>
                                        )) : this.props.setCustomHeadersState.map((dataa, keyy) => (
                                            <li key={keyy} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,keyy)} onDragEnter={(e)=>this.handleDragEnter(e,keyy)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{dataa}</span>
                                                    {this.props.mandateHeaderSet.length != 0 ? !this.props.mandateHeaderSet.includes(dataa) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                    {this.props.mandateHeaderSet.length != 0 ? this.props.mandateHeaderSet.includes(dataa) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : <span className="chmbrhi-fixed">Fixed</span>}
                                                </span>
                                            </li>
                                        )):null}
                                        {this.props.tabVal == 3 ? !isProcPage ? (this.props.itemHeaderCondition ? this.props.getItemHeaderConfig.map((data, key) => (
                                            <li key={key} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,key)} onDragEnter={(e)=>this.handleDragEnter(e,key)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{data}</span>
                                                    {this.props.mandateHeaderItem.length != 0 ? !this.props.mandateHeaderItem.includes(data) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                    {this.props.mandateHeaderItem.length != 0 ? this.props.mandateHeaderItem.includes(data) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : <span className="chmbrhi-fixed">Fixed</span>}
                                                </span>
                                            </li>
                                        )) : this.props.itemCustomHeadersState.map((dataa, keyy) => (
                                            <li key={keyy} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,keyy)} onDragEnter={(e)=>this.handleDragEnter(e,keyy)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{dataa}</span>
                                                    {this.props.mandateHeaderItem.length != 0 ? !this.props.mandateHeaderItem.includes(dataa) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                     {this.props.mandateHeaderItem.length != 0 ? this.props.mandateHeaderItem.includes(dataa) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : <span className="chmbrhi-fixed">Fixed</span>} 
                                                </span>
                                            </li>
                                        ))) : (this.props.setUdfHeaderCondition ? this.props.getSetUdfHeaderConfig.map((data, key) => (
                                            <li key={key} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,key)} onDragEnter={(e)=>this.handleDragEnter(e,key)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{data}</span>
                                                    {this.props.mandateHeaderSetUdf.length != 0 ? !this.props.mandateHeaderSetUdf.includes(data) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                    {this.props.mandateHeaderSetUdf.length != 0 ? this.props.mandateHeaderSetUdf.includes(data) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : null}
                                                </span>
                                            </li>
                                        )) : this.props.setUdfCustomHeadersState.map((dataa, keyy) => (
                                            <li key={keyy} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,keyy)} onDragEnter={(e)=>this.handleDragEnter(e,keyy)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{dataa}</span>
                                                    {this.props.mandateHeaderSetUdf.length != 0 ? !this.props.mandateHeaderSetUdf.includes(dataa) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                     {this.props.mandateHeaderSetUdf.length != 0 ? this.props.mandateHeaderSetUdf.includes(dataa) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : null} 
                                                </span>
                                            </li>
                                        ))) : null}
                                        {console.log('itemHeader', this.props.itemUdfHeaderCondition, 'config', this.props.getItemUdfHeaderConfig, 'custom', this.props.itemUdfCustomHeadersState)}
                                        {this.props.tabVal == 4 ? this.props.itemUdfHeaderCondition ? this.props.getItemUdfHeaderConfig.map((data, key) => (
                                            <li key={key} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,key)} onDragEnter={(e)=>this.handleDragEnter(e,key)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                <span className="chmbrhi-text">{data}</span>
                                                {this.props.mandateHeaderItemUdf.length != 0 ? !this.props.mandateHeaderItemUdf.includes(data) && 
                                                    <span className="chmbrhi-close"  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                     {this.props.mandateHeaderItemUdf.length != 0 ? this.props.mandateHeaderItemUdf.includes(data) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : null}
                                                </span>
                                            </li>
                                        )) : this.props.itemUdfCustomHeadersState.map((dataa, keyy) => (
                                            <li key={keyy} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,keyy)} onDragEnter={(e)=>this.handleDragEnter(e,keyy)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{dataa}</span>
                                                    {this.props.mandateHeaderItemUdf.length != 0 ? !this.props.mandateHeaderItemUdf.includes(dataa) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                    {this.props.mandateHeaderItemUdf.length != 0 ? this.props.mandateHeaderItemUdf.includes(dataa) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : null}
                                                </span>
                                            </li>
                                        )):null}
                                        {console.log('catHeader', this.props.catDescHeaderCondition, 'config', this.props.getCatDescHeaderConfig, 'custom', this.props.catDescCustomHeadersState)}
                                        {this.props.tabVal == 5 ? this.props.catDescHeaderCondition ? this.props.getCatDescHeaderConfig.map((data, key) => (
                                            <li key={key} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,key)} onDragEnter={(e)=>this.handleDragEnter(e,key)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                <span className="chmbrhi-text">{data}</span>
                                                {this.props.mandateHeaderCatDesc.length != 0 ? !this.props.mandateHeaderCatDesc.includes(data) && 
                                                    <span className="chmbrhi-close"  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                     {this.props.mandateHeaderCatDesc.length != 0 ? this.props.mandateHeaderCatDesc.includes(data) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : null}
                                                </span>
                                            </li>
                                        )) : this.props.catDescCustomHeadersState.map((dataa, keyy) => (
                                            <li key={keyy} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,keyy)} onDragEnter={(e)=>this.handleDragEnter(e,keyy)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{dataa}</span>
                                                    {this.props.mandateHeaderCatDesc.length != 0 ? !this.props.mandateHeaderCatDesc.includes(dataa) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                    {this.props.mandateHeaderCatDesc.length != 0 ? this.props.mandateHeaderCatDesc.includes(dataa) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : null}
                                                </span>
                                            </li>
                                        )):null}
                                        {console.log('lineHeader', this.props.lineItemHeaderCondition, 'config', this.props.getLineItemHeaderConfig, 'custom', this.props.lineItemCustomHeadersState)}
                                        {this.props.tabVal == 6 ? this.props.lineItemHeaderCondition ? this.props.getLineItemHeaderConfig.map((data, key) => (
                                            <li key={key} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,key)} onDragEnter={(e)=>this.handleDragEnter(e,key)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                <span className="chmbrhi-text">{data}</span>
                                                {this.props.mandateHeaderLineItem.length != 0 ? !this.props.mandateHeaderLineItem.includes(data) && 
                                                    <span className="chmbrhi-close"  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,data)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                     {this.props.mandateHeaderLineItem.length != 0 ? this.props.mandateHeaderLineItem.includes(data) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : <span className="chmbrhi-fixed">Fixed</span>}
                                                </span>
                                            </li>
                                        )) : this.props.lineItemCustomHeadersState.map((dataa, keyy) => (
                                            <li key={keyy} ref={this.myRef} draggable onDragStart={(e)=>this.onDragListItemStart(e,keyy)} onDragEnter={(e)=>this.handleDragEnter(e,keyy)} className={this.props.dragOn ? "li-shift chmbrh-items":"chmbrh-items"}>
                                                <span className="chmbrhi-inner">
                                                    <span className="chmbrhi-text">{dataa}</span>
                                                    {this.props.mandateHeaderLineItem.length != 0 ? !this.props.mandateHeaderLineItem.includes(dataa) && 
                                                    <span  className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img src={require('../../assets/clearSearch.svg')} />
                                                    </span>
                                                    : <span className="chmbrhi-close" onClick={(e) => this.closeColumns(e,dataa)}>
                                                        <img  src={require('../../assets/clearSearch.svg')} />
                                                     </span>}
                                                    {this.props.mandateHeaderLineItem.length != 0 ? this.props.mandateHeaderLineItem.includes(dataa) && 
                                                    <span className="chmbrhi-fixed">Fixed</span>
                                                    : <span className="chmbrhi-fixed">Fixed</span>}
                                                </span>
                                            </li>
                                        )):null}
                                    </ul>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div> : <div className="columnFilterGeneric" onClick={(e) => this.openColoumSetting("true")}>
                        <span className="glyphicon glyphicon-menu-left"></span></div>}
            </div>
        )
    }
}

export default ColoumSetting;