import React from 'react';
import CreateNewConfiguration from './createNewConfiguration';
import ManageConfiguration from './manageConfiguration';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import GenericDataModal from './genericDataModal';
import Succeeded from "./rodsuccess";

var interval;

class RunOnDemand extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            submit: false,
            //openSetting: '',
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',
            genericDataModalShowRow: {},
            genericDataModalShow: false,

            createConfiguration: false,
            selectJobs: false,
            runningJobs: false,

            jobNames: [],
            selectedJob: "Select Job",
            manageRuleId: "",
            lastEngineRun: "NA",
            lastEngineRunStatus: "NA",
            jobRunState: "NOT STARTED",
            percentage: 0,
            successFail: false,
            runningOnce: false,
            modalState: "",
            timeTaken: 0,
            // showModalOnce: false,

            apiCallType: 'site',
            siteFilters: {},
            itemFilters: {},
            siteRefs: {},
            itemRefs: {},
            genericSearchData: {},
            siteSelectedItems: {},
            itemSelectedItems: {},
            planningAttribute: []
        }
    }

    componentDidMount() {
        this.props.getSiteAndItemFilterConfigurationRequest(this.state.apiCallType);
        //this.props.getSiteAndItemFilterConfigurationRequest("item");
        this.props.getAllJobRequest("data");
        // interval = window.setInterval(() => {
        //     this.props.jobRunDetailRequest("1");
        // }, 5000);
    }

    componentWillUnmount() {
        clearInterval(interval);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isSuccess && this.state.apiCallType == 'site') {
            this.setState({
                loading: false,
                siteFilters: JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter),
                apiCallType: 'item'
            }, () => this.props.getSiteAndItemFilterConfigurationRequest("item"));
        }
        else if(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isSuccess && this.state.apiCallType == 'item') {
            this.setState({
                loading: false,
                itemFilters: JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter)
            });
        }
        else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.status,
                errorCode: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorMessage
            });
        }

        if(nextProps.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                loading: false,
                genericSearchData: {...this.state.genericSearchData, data: nextProps.seasonPlanning.getArsGenericFilters.data},
                genericDataModalShow: true
            });
        }
        else if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage,
                //confirmDelete: false
            });
        }

        if (nextProps.replenishment.getAllJob.isSuccess) {
            if (nextProps.replenishment.getAllJob.data.resource != null && nextProps.replenishment.getAllJob.data.resource.length != 0) {
                let item = nextProps.replenishment.getAllJob.data.resource[0];
                this.setState({
                    loading: false,
                    jobNames: nextProps.replenishment.getAllJob.data.resource,
                    selectedJob: item.jobName,
                    manageRuleId: item.id
                });
                this.props.getJobDataRequest(item.id);
                this.props.lastJobRequest(item.id);
                this.props.jobRunDetailRequest(item.id)
                interval = window.setInterval(() => {
                    this.props.jobRunDetailRequest(item.id);
                }, 5000);
            }
            this.props.getAllJobClear();
        }
        else if (nextProps.replenishment.getAllJob.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.replenishment.getAllJob.message.status,
                errorCode: nextProps.replenishment.getAllJob.message.error == undefined ? undefined : nextProps.replenishment.getAllJob.message.error.errorCode,
                errorMessage: nextProps.replenishment.getAllJob.message.error == undefined ? undefined : nextProps.replenishment.getAllJob.message.error.errorMessage,
            });
        }

        if (nextProps.replenishment.getJobData.isSuccess) {
            this.setState({
                loading: false,
                planningAttribute: nextProps.replenishment.getJobData.data.resource.response,
                runningJobs: false
            });
            this.props.getJobDataClear();
        }
        else if (nextProps.replenishment.getJobData.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.replenishment.getJobData.message.status,
                errorCode: nextProps.replenishment.getJobData.message.error == undefined ? undefined : nextProps.replenishment.getJobData.message.error.errorCode,
                errorMessage: nextProps.replenishment.getJobData.message.error == undefined ? undefined : nextProps.replenishment.getJobData.message.error.errorMessage
            });
            this.props.getJobDataClear();
        }

        if (nextProps.replenishment.lastJob.isSuccess) {
            this.setState({
                loading: false,
                // lastEngineRun: nextProps.replenishment.lastJob.data.resource == null ? "NA" : nextProps.replenishment.lastJob.data.resource.lastEngineRun
            });
            this.props.lastJobClear();
        }
        else if (nextProps.replenishment.lastJob.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.replenishment.lastJob.message.status,
                errorCode: nextProps.replenishment.lastJob.message.error == undefined ? undefined : nextProps.replenishment.lastJob.message.error.errorCode,
                errorMessage: nextProps.replenishment.lastJob.message.error == undefined ? undefined : nextProps.replenishment.lastJob.message.error.errorMessage
            });
            this.props.lastJobClear();
        }

        if (nextProps.replenishment.jobRunDetail.isSuccess) {
            if (nextProps.replenishment.jobRunDetail.data.resource !== null) {
                this.setState({
                    jobRunState: nextProps.replenishment.jobRunDetail.data.resource.runningjob.length === 0 ? "NOT STARTED" : "RUNNING",
                    percentage: parseInt(nextProps.replenishment.jobRunDetail.data.resource.percentage),
                    lastEngineRun: nextProps.replenishment.jobRunDetail.data.resource.lastEngineRunDetail,
                    lastEngineRunStatus: nextProps.replenishment.jobRunDetail.data.resource.lastEngineRun,
                    timeTaken: nextProps.replenishment.jobRunDetail.data.resource.runningjob.length === 0 ? 0 : nextProps.replenishment.jobRunDetail.data.resource.runningjob[0].timetaken
                });
                if (nextProps.replenishment.jobRunDetail.data.resource.runningjob.length !== 0) {
                    this.setState({
                        runningOnce: true
                    });
                }
                if ((this.state.runningOnce && nextProps.replenishment.jobRunDetail.data.resource.runningjob.length === 0) && (nextProps.replenishment.jobRunDetail.data.resource.lastEngineRun.toLowerCase() === "succeeded" || nextProps.replenishment.jobRunDetail.data.resource.lastEngineRun.toLowerCase() === "failed")) {
                    this.setState({
                        successFail: true,
                        modalState: nextProps.replenishment.jobRunDetail.data.resource.lastEngineRun,
                        // showModalOnce: true
                    });
                }
            }
            this.props.jobRunDetailClear();
        }
        else if (nextProps.replenishment.jobRunDetail.isError) {
            console.log("jobRunDetail error");
            this.props.jobRunDetailClear();
        }

        if (nextProps.replenishment.runOnDemand.isSuccess) {
            this.setState({
                loading: false,
                jobRunState: nextProps.replenishment.runOnDemand.data.resource !== null ? nextProps.replenishment.runOnDemand.data.resource.jobRunState : "RUNNING"
            });
            this.props.runOnDemandClear();
        }
        else if (nextProps.replenishment.runOnDemand.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.replenishment.runOnDemand.message.status,
                errorCode: nextProps.replenishment.runOnDemand.message.error == undefined ? undefined : nextProps.replenishment.runOnDemand.message.error.errorCode,
                errorMessage: nextProps.replenishment.runOnDemand.message.error == undefined ? undefined : nextProps.replenishment.runOnDemand.message.error.errorMessage
            });
            this.props.runOnDemandClear();
        }

        if (nextProps.replenishment.stopOnDemand.isSuccess) {
            this.setState({
                loading: false,
                jobRunState: nextProps.replenishment.stopOnDemand.data.resource !== null ? nextProps.replenishment.stopOnDemand.data.resource.state : "STOPPING"
            });
            this.props.stopOnDemandClear();
        }
        else if (nextProps.replenishment.stopOnDemand.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.replenishment.stopOnDemand.message.status,
                errorCode: nextProps.replenishment.stopOnDemand.message.error == undefined ? undefined : nextProps.replenishment.stopOnDemand.message.error.errorCode,
                errorMessage: nextProps.replenishment.stopOnDemand.message.error == undefined ? undefined : nextProps.replenishment.stopOnDemand.message.error.errorMessage
            });
            this.props.stopOnDemandClear();
        }

        if (nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isLoading || nextProps.seasonPlanning.getArsGenericFilters.isLoading || nextProps.replenishment.getAllJob.isLoading || nextProps.replenishment.getJobData.isLoading || nextProps.replenishment.runOnDemand.isLoading || nextProps.replenishment.stopOnDemand.isLoading) {
            this.setState({
                loading: true
            });
        }
    }

    openCreateConfiguration(e) {
        e.preventDefault();
        this.setState({
            createConfiguration: !this.state.createConfiguration
        });
    }
    CloseCreateConfiguration = (e) => {
        this.setState({
            createConfiguration: false,
        })
    }
    openSelectJobs(e) {
        e.preventDefault();
        this.setState({
            selectJobs: !this.state.selectJobs
        });
    }
    openRunningJobs(e) {
        e.preventDefault();
        this.setState({
            runningJobs: !this.state.runningJobs
        });
    }

    selectJob = (item) => {
        this.setState({selectedJob: item.jobName, manageRuleId: item.id});
        this.props.getJobDataRequest(item.id);
        this.props.lastJobRequest(item.id);
        this.props.jobRunDetailRequest(item.id);
        clearInterval(interval);
        interval = window.setInterval(() => {
            this.props.jobRunDetailRequest(item.id);
        }, 5000);
    }

    //FUNCTIONS FOR GENERIC DATA MODAL
    siteData = (e, entity, key, search, pageNo) => {
        if (e.keyCode == 13) {
            this.openGenericDataModal(entity, key, search, pageNo);
        }
    }

    openGenericDataModal = (entity, key, search, pageNo) => {
        if (this.state.genericDataModalShowRow[entity] != key) this.closeGenericDataModal();
        this.props.getArsGenericFiltersRequest({
            entity: entity,
            key: key,
            search: search,
            pageNo: pageNo
        });
        let modal = {};
        modal[entity] = key;
        this.setState({genericSearchData: {payload: {entity: entity, key: key, search: search}}, genericDataModalShowRow: modal});
    }

    closeGenericDataModal = () => {
        let itemRefs = this.state.itemRefs;
        if (itemRefs[this.state.genericDataModalShowRow.item] !== undefined) {
            itemRefs[this.state.genericDataModalShowRow.item].current.value = "";
            this.setState({
                genericDataModalShowRow: '',
                genericDataModalShow: false,
                itemRefs
            });
        }
        let siteRefs = this.state.siteRefs;
        if (siteRefs[this.state.genericDataModalShowRow.site] !== undefined) {
            siteRefs[this.state.genericDataModalShowRow.site].current.value = "";
            this.setState({
                genericDataModalShowRow: '',
                genericDataModalShow: false,
                siteRefs
            });
        }
    }

    handlePlanningValue =(e, data)=>{
        let planningAttribute = [...this.state.planningAttribute];
        planningAttribute.map( val => val.id == data.id && (val.planningTextValue = e.target.value))
        this.setState({ planningAttribute})
    }

    selectItems = (entity, key, items) => {
        if (entity == "item") {
            let updateItems = this.state.itemSelectedItems;
            if (items.length !== 0) {
                updateItems[key] = items;
            }
            else if (items.length === 0 && updateItems[key] !== undefined) {
                delete updateItems[key];
            }
            this.setState({
                itemSelectedItems: updateItems
            });
        }
        else if (entity == "site") {
            let updateItems = this.state.siteSelectedItems;
            if (items.length !== 0) {
                updateItems[key] = items;
            }
            else if (items.length === 0 && updateItems[key] !== undefined) {
                delete updateItems[key];
            }
            this.setState({
                siteSelectedItems: updateItems
            });
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        }, () => {

        });
        this.props.getAllJobRequest("data");
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    runOnDemand = () => {
        let planningAttributeJson = {};
        this.state.planningAttribute.forEach((item) => {
            planningAttributeJson[item.code] = item.planningTextValue == undefined ? item.defaultValue : item.planningTextValue
        });
        this.props.runOnDemandRequest({
            manageRuleId: this.state.manageRuleId,
            planningAttributeJson: JSON.stringify(planningAttributeJson),
            locationFilterJson: Object.keys(this.state.siteSelectedItems).length == 0 ? "" : JSON.stringify(this.state.siteSelectedItems),
            itemFilterJson: Object.keys(this.state.itemSelectedItems).length == 0 ? "" : JSON.stringify(this.state.itemSelectedItems)
        });
        this.setState({
            jobRunState: "WAITING"
        });
    }

    stopOnDemand = () => {
        this.props.stopOnDemandRequest({
            jobName: this.state.selectedJob,
            jobId: this.props.replenishment.jobRunDetail.data.resource !== null ? this.props.replenishment.jobRunDetail.data.resource.jobId : ""
        });
        this.setState({
            jobRunState: "STOPPING"
        });
    }

    closeSucceeded(type) {
        this.setState({
            successFail: false,
            runningOnce: false
        });
        sessionStorage.removeItem('jobId');
        if (type != "close") {
            this.props.history.push('/inventoryPlanning/summary');
        }
    }

    render() {
        return(
            <div className="container-fluid pad-l50">
                <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                    <div className="auto-confing-head">
                        <div className="ach-left">
                            <h3>Run On Demand - <span>All Jobs</span></h3>
                        </div>
                        <div className="ach-right">
                            <div className="achjbt-row">
                                <p>Select Job</p>
                                <button className="achjbt-jobs" type="button" onClick={(e) => this.openRunningJobs(e)}>{this.state.selectedJob}</button>
                                {this.state.runningJobs &&
                                <div className="achjbtj-dropdown">
                                    <ul>
                                        {/* <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                        <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                        <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                        <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                        <li>SM-TURNINGCLOUD-D-IPOAC</li> */}
                                        {
                                            this.state.jobNames.map((item) => <li onClick={() => this.selectJob(item)}>{item.jobName}</li>)
                                        }
                                    </ul>
                                </div>}
                            </div>
                            {/* <button type="button">
                                <svg xmlns="http://www.w3.org/2000/svg" id="plus_4_" width="16" height="16" viewBox="0 0 19.846 19.846">
                                    <g id="Group_3035">
                                        <g id="Group_3034">
                                            <path fill="#8b77fa" id="Path_948" d="M9.923 0a9.923 9.923 0 1 0 9.923 9.923A9.934 9.934 0 0 0 9.923 0zm0 18.308a8.386 8.386 0 1 1 8.386-8.386 8.4 8.4 0 0 1-8.386 8.386z" className="cls-1"/>
                                        </g>
                                    </g>
                                    <g id="Group_3037" transform="translate(5.311 5.242)">
                                        <g id="Group_3036">
                                            <path fill="#8b77fa" id="Path_949" d="M145.477 139.081H142.4v-3.074a.769.769 0 0 0-1.537 0v3.074h-3.074a.769.769 0 0 0 0 1.537h3.074v3.074a.769.769 0 1 0 1.537 0v-3.074h3.074a.769.769 0 0 0 0-1.537z" className="cls-1" transform="translate(-137.022 -135.238)"/>
                                        </g>
                                    </g>
                                </svg>
                                Add Filter
                            </button> */}
                        </div>
                    </div>
                </div>
                {
                    this.state.jobNames.length == 0 ?
                    <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-30">
                        <div className="summary-page-status m-top-30">
                            <div className="sps-error-msg">
                                <span className="spsem-msg">
                                    <svg xmlns="http://www.w3.org/2000/svg" id="information" width="14.413" height="14.413" viewBox="0 0 15.413 15.413">
                                        <path fill="#ff8103" id="Path_1092" d="M7.706 0a7.706 7.706 0 1 0 7.706 7.706A7.715 7.715 0 0 0 7.706 0zm0 14.012a6.305 6.305 0 1 1 6.305-6.305 6.312 6.312 0 0 1-6.305 6.305z" class="cls-1"/>
                                        <path fill="#ff8103" id="Path_1093" d="M145.936 70a.934.934 0 1 0 .934.935.935.935 0 0 0-.934-.935z" class="cls-1" transform="translate(-138.23 -66.731)"/>
                                        <path fill="#ff8103" id="Path_1094" d="M150.7 140a.7.7 0 0 0-.7.7v4.2a.7.7 0 0 0 1.4 0v-4.2a.7.7 0 0 0-.7-.7z" class="cls-1" transform="translate(-142.994 -133.461)"/>
                                    </svg>
                                    No Jobs Found!
                                </span>
                                <p>Currently no jobs has been configured to your account. Please contact support at support@supplymint.com</p>
                            </div>
                        </div>
                    </div> :
                    <div className="col-lg-12 p-lr-32 m-top-10">
                        <div className="col-lg-8">
                            <div className="run-on-demand-filter">
                                <div className="rodf-inner">
                                    <div className="rodfi-filter">
                                        <h3>Filters</h3>
                                        <div className="rodfif-item">
                                            <h3>Site Filter</h3>
                                            <div className="rodfifi-filter">
                                                <div className="pi-new-layout">{
                                                    Object.keys(this.state.siteFilters).length == 0 ?
                                                    <label><i>No filters found!</i></label> :
                                                    Object.keys(this.state.siteFilters).map((key) => {
                                                        this.state.siteRefs[key] = React.createRef();
                                                        return (
                                                            <div className="col-md-4 col-sm-6 pad-lft-0 rodfifif-inner">
                                                                <label>{this.state.siteFilters[key]}</label>
                                                                <div className="inputTextKeyFucMain">
                                                                    <input type="text" className="onFocus pnl-purchase-input"
                                                                        id={key + "Focus"}
                                                                        placeholder={this.state.siteSelectedItems[key] === undefined || this.state.siteSelectedItems[key].length === 0 ? "No " + this.state.siteFilters[key] + " selected" : this.state.siteSelectedItems[key].length === 1 ? this.state.siteSelectedItems[key][0] + " selected" : this.state.siteSelectedItems[key][0] + " + " + (this.state.siteSelectedItems[key].length - 1) + " selected"}
                                                                        ref={this.state.siteRefs[key]}
                                                                        onFocus={(e) => {if (this.state.genericDataModalShow.site === false || this.state.genericDataModalShowRow.site != key) this.openGenericDataModal("site", key, e.target.value, 1)}}
                                                                        //onBlur={(e) => {if (this.state.genericDataModalShowRow != item.key) e.target.value=""}}
                                                                        onKeyDown={(e) => this.siteData(e, "site", key, e.target.value, 1)}
                                                                    />
                                                                    <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("site", key, this.state.siteRefs[key].current.value, 1)}> {/* PAGE NUMBER HAS TO BE MADE DYNAMIC */}
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                        </svg>
                                                                    </span>
                                                                {this.state.genericDataModalShow && this.state.genericDataModalShowRow.site == key ? <GenericDataModal data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.siteSelectedItems[key]} /> : null}
                                                                </div>
                                                            </div>
                                                        );
                                                    })
                                                }</div>
                                            </div>
                                        </div>
                                        <div className="rodfif-item">
                                            <h3>Item Filter</h3>
                                            <div className="rodfifi-filter">
                                                <div className="pi-new-layout">{
                                                    Object.keys(this.state.itemFilters).length == 0 ?
                                                    <label><i>No filters found!</i></label> :
                                                    Object.keys(this.state.itemFilters).map((key) => {
                                                        this.state.itemRefs[key] = React.createRef();
                                                        return (
                                                            <div className="col-md-4 col-sm-6 pad-lft-0 rodfifif-inner">
                                                                <label>{this.state.itemFilters[key]}</label>
                                                                <div className="inputTextKeyFucMain">
                                                                    <input type="text" className="onFocus pnl-purchase-input"
                                                                        id={key + "Focus"}
                                                                        placeholder={this.state.itemSelectedItems[key] === undefined || this.state.itemSelectedItems[key].length === 0 ? "No " + this.state.itemFilters[key] + " selected" : this.state.itemSelectedItems[key].length === 1 ? this.state.itemSelectedItems[key][0] + " selected" : this.state.itemSelectedItems[key][0] + " + " + (this.state.itemSelectedItems[key].length - 1) + " selected"}
                                                                        ref={this.state.itemRefs[key]}
                                                                        onFocus={(e) => {if (this.state.genericDataModalShow.item === false || this.state.genericDataModalShowRow.item != key) this.openGenericDataModal("item", key, e.target.value, 1)}}
                                                                        //onBlur={(e) => {if (this.state.genericDataModalShowRow != item.key) e.target.value=""}}
                                                                        onKeyDown={(e) => this.siteData(e, "item", key, e.target.value, 1)}
                                                                    />
                                                                    <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("item", key, this.state.itemRefs[key].current.value, 1)}> {/* PAGE NUMBER HAS TO BE MADE DYNAMIC */}
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                        </svg>
                                                                    </span>
                                                                    {this.state.genericDataModalShow && this.state.genericDataModalShowRow.item == key ? <GenericDataModal data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.itemSelectedItems[key]} /> : null}
                                                                </div>
                                                            </div>
                                                        );
                                                    })
                                                }</div>
                                            </div>
                                        </div>
                                        <div className="rodfif-item">
                                            <h3>Planning Attributes</h3>
                                            <div className="rodfifi-table">
                                                <table className="table">
                                                    <thead>{
                                                        this.state.planningAttribute.length == 0 ? <tr><th><label>No attributes found!</label></th></tr> :
                                                            <tr>
                                                            <th><label>Param Name</label></th>
                                                            <th><label>Default Value</label></th>
                                                            <th><label>Value</label></th>
                                                        </tr>
                                                    }</thead>
                                                    <tbody>
                                                        {this.state.planningAttribute.length == 0 ? null : this.state.planningAttribute.map( (data, key) => <tr key={key}>    
                                                            <td><label>{data.displayName}</label></td>
                                                            <td><label>{data.defaultValue}</label></td>
                                                            <td>
                                                                {data.type == "List" ? <select onChange={(e) => this.handlePlanningValue(e,data)}>
                                                                    {data.lov.length && data.lov.split(",").map( (data, key) =>
                                                                    <option key={key} value={data}>{data}</option>)}
                                                                </select> :
                                                                data.type == "Disabled" ?
                                                                <input type="text" onChange={(e) =>this.handlePlanningValue(e, data)} value={data.planningTextValue == undefined ? "" : data.planningTextValue} disabled /> :
                                                                <input type="text" onChange={(e) =>this.handlePlanningValue(e, data)} value={data.planningTextValue == undefined ? "" : data.planningTextValue}/>}
                                                            </td>
                                                        </tr>)}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <div className="rodfi-job-details">
                                        <div className="rodfijd-head">
                                            <h3>Jobs Details</h3>
                                        </div>
                                        <div className="rodfijd-body">
                                            <div className="rodfijdb-text">
                                                <h5>Last Engine Run Details</h5>
                                                <p>3 Mins ago</p>
                                            </div>
                                            <div className="rodfijdb-text rodfijdb-border">
                                                <h5>Current Job Details</h5>
                                                <p>NA</p>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                        {/* <div className="col-lg-4">
                            <div className="ach-job-box">
                                <div className="achjb-round">
                                    <div className="achjbr-inner">
                                    <svg className="achjb-rotate" xmlns="http://www.w3.org/2000/svg" width="160" height="160" viewBox="0 0 182 182">
                                        <defs>
                                            <filter id="Ellipse_189" width="158" height="158" x="12" y="12" filterUnits="userSpaceOnUse">
                                                <feOffset/>
                                                <feGaussianBlur result="blur" stdDeviation="3"/>
                                                <feFlood flood-color="#d8deea"/>
                                                <feComposite in2="blur" operator="in"/>
                                                <feComposite in="SourceGraphic"/>
                                            </filter>
                                        </defs>
                                        <g id="meter" transform="translate(-1091 -272)">
                                            <g id="outer_circle" transform="translate(1091 272)">
                                                <circle id="Ellipse_188" cx="91" cy="91" r="91" fill="#f8f7fe"/>
                                                <g filter="url(#Ellipse_189)">
                                                    <circle id="Ellipse_189-2" cx="70" cy="70" r="70" fill="#fff" transform="translate(21 21)"/>
                                                </g>
                                                <g id="Ellipse_190" fill="#fff" stroke="#8b77fa" strokeDasharray="322 430" strokeLinecap="round" strokeWidth="10px" transform="rotate(180 80 80)">
                                                    <circle cx="69" cy="69" r="69" stroke="none"/>
                                                    <circle cx="69" cy="69" r="64" fill="none"/>
                                                </g>
                                            </g>
                                            <g id="seperation" transform="translate(1096.873 277.873)">
                                                <g id="Group_3031" transform="translate(85.585)">
                                                    <rect fill="#d1e2f4" id="Rectangle_704" width="2" height="10" className="cls-7" rx="1" transform="translate(-.458 .127)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_705" width="2" height="10" className="cls-7" rx="1" transform="translate(-.458 161.127)"/>
                                                </g>
                                                <g id="Group_3028" transform="translate(0 84.746)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706" width="1" height="10" className="cls-7" rx=".5" transform="rotate(-90 .754 .627)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707" width="1" height="10" className="cls-7" rx=".5" transform="rotate(-90 81.254 -79.873)"/>
                                                </g>
                                                <g id="Group_3029" transform="rotate(-45 188.301 42.199)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706-2" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 .839 .839)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707-2" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 81.39 -79.712)"/>
                                                </g>
                                                <g id="Group_3030" transform="rotate(-135 103.556 43.038)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706-3" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 .839 .839)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707-3" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 81.39 -79.712)"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>

                                        <div className="achjbri-text">
                                            <span className="achjbri-time">2:00 <span>Hrs</span></span>
                                            <span className="achjbri-els">Elapsed Time</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="achjb-text">
                                    <div className="achjbt-row">
                                        <p>Job Name</p>
                                        <h5>SM-TURNINGCLOUD-D-IPOAC</h5>
                                    </div>
                                    <div className="achjbt-row">
                                        <div className="achjbtrwidth50">
                                            <p>Current Job Status</p>
                                            <h5>Running</h5>
                                        </div>
                                        <div className="achjbtrwidth50">
                                            <p>Last Job Status</p>
                                            <h5>
                                                <span className="achjbtr-tick">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.249" height="10.064" viewBox="0 0 14.249 11.064">
                                                        <path fill="#8b77fa" d="M5.456 11.258a.727.727 0 0 1-1.029 0L.32 7.149a1.091 1.091 0 0 1 0-1.543l.514-.515a1.091 1.091 0 0 1 1.543 0l2.565 2.565 6.93-6.93a1.091 1.091 0 0 1 1.543 0l.514.515a1.091 1.091 0 0 1 0 1.543zm0 0" transform="translate(0 -.406)"/>
                                                    </svg>
                                                </span>
                                                Successful
                                            </h5>
                                        </div>
                                    </div>
                                    <div className="achjbt-row m-top-60">
                                        <button type="button" className="achjbt-stop-process">Stop Process</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 align-center">
                            <div className="ach-job-box">
                                <div className="achjb-round">
                                    <div className="achjbr-inner">
                                    <svg className="achjb-rotate" xmlns="http://www.w3.org/2000/svg" width="160" height="160" viewBox="0 0 182 182">
                                        <defs>
                                            <filter id="Ellipse_189" width="158" height="158" x="12" y="12" filterUnits="userSpaceOnUse">
                                                <feOffset/>
                                                <feGaussianBlur result="blur" stdDeviation="3"/>
                                                <feFlood flood-color="#d8deea"/>
                                                <feComposite in2="blur" operator="in"/>
                                                <feComposite in="SourceGraphic"/>
                                            </filter>
                                        </defs>
                                        <g id="meter" transform="translate(-1091 -272)">
                                            <g id="outer_circle" transform="translate(1091 272)">
                                                <circle id="Ellipse_188" cx="91" cy="91" r="91" fill="#f8f7fe"/>
                                                <g filter="url(#Ellipse_189)">
                                                    <circle id="Ellipse_189-2" cx="70" cy="70" r="70" fill="#fff" transform="translate(21 21)"/>
                                                </g>
                                                <g id="Ellipse_190" fill="#fff" stroke="#8b77fa" strokeDasharray="200 430" strokeLinecap="round" strokeWidth="10px" transform="rotate(180 80 80)">
                                                    <circle cx="69" cy="69" r="69" stroke="none"/>
                                                    <circle cx="69" cy="69" r="64" fill="none"/>
                                                </g>
                                            </g>
                                            <g id="seperation" transform="translate(1096.873 277.873)">
                                                <g id="Group_3031" transform="translate(85.585)">
                                                    <rect fill="#d1e2f4" id="Rectangle_704" width="2" height="10" className="cls-7" rx="1" transform="translate(-.458 .127)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_705" width="2" height="10" className="cls-7" rx="1" transform="translate(-.458 161.127)"/>
                                                </g>
                                                <g id="Group_3028" transform="translate(0 84.746)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706" width="1" height="10" className="cls-7" rx=".5" transform="rotate(-90 .754 .627)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707" width="1" height="10" className="cls-7" rx=".5" transform="rotate(-90 81.254 -79.873)"/>
                                                </g>
                                                <g id="Group_3029" transform="rotate(-45 188.301 42.199)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706-2" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 .839 .839)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707-2" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 81.39 -79.712)"/>
                                                </g>
                                                <g id="Group_3030" transform="rotate(-135 103.556 43.038)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706-3" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 .839 .839)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707-3" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 81.39 -79.712)"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>

                                        <div className="achjbri-text">
                                            <span className="achjbri-time">1:24 <span>Hrs</span></span>
                                            <span className="achjbri-els">Elapsed Time</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="achjb-text">
                                    <div className="achjbt-row">
                                        <p>Job Name</p>
                                        <h5>SM-TURNINGCLOUD-D-IPOAC</h5>
                                    </div>
                                    <div className="achjbt-row">
                                        <div className="achjbtrwidth50">
                                            <p>Current Job Status</p>
                                            <h5>Running</h5>
                                        </div>
                                        <div className="achjbtrwidth50">
                                            <p>Last Job Status</p>
                                            <h5>
                                                <span className="achjbtr-tick">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="11.249" height="10.064" viewBox="0 0 14.249 11.064">
                                                        <path fill="#8b77fa" d="M5.456 11.258a.727.727 0 0 1-1.029 0L.32 7.149a1.091 1.091 0 0 1 0-1.543l.514-.515a1.091 1.091 0 0 1 1.543 0l2.565 2.565 6.93-6.93a1.091 1.091 0 0 1 1.543 0l.514.515a1.091 1.091 0 0 1 0 1.543zm0 0" transform="translate(0 -.406)"/>
                                                    </svg>
                                                </span>
                                                Successful
                                            </h5>
                                        </div>
                                    </div>
                                    <div className="achjbt-row m-top-60">
                                        <button type="button" className="achjbt-stop-process">Stop Process</button>
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        <div className="col-lg-4 align-end">
                            <div className="ach-job-box">
                                <div className="achjb-round">
                                    <div className="achjbr-inner">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="160" height="160" viewBox="0 0 182 182" className={this.state.jobRunState.toLowerCase() === "running" ? "achjb-rotate" : ""}>
                                        <defs>
                                            <filter id="Ellipse_189" width="158" height="158" x="12" y="12" filterUnits="userSpaceOnUse">
                                                <feOffset/>
                                                <feGaussianBlur result="blur" stdDeviation="3"/>
                                                <feFlood floodColor="#d8deea"/>
                                                <feComposite in2="blur" operator="in"/>
                                                <feComposite in="SourceGraphic"/>
                                            </filter>
                                        </defs>
                                        <g id="meter" transform="translate(-1091 -272)">
                                            <g id="outer_circle" transform="translate(1091 272)">
                                                <circle id="Ellipse_188" cx="91" cy="91" r="91" fill="#f8f7fe"/>
                                                <g filter="url(#Ellipse_189)">
                                                    <circle id="Ellipse_189-2" cx="70" cy="70" r="70" fill="#fff" transform="translate(21 21)"/>
                                                </g>
                                                <g id="Ellipse_190" fill="#fff" stroke="#97abbf" strokeDasharray={`${(this.state.percentage * 3.9) + 10} 430`} strokeLinecap="round" strokeWidth="10px" transform="rotate(180 80 80)">
                                                    <circle cx="69" cy="69" r="69" stroke="none"/>
                                                    <circle cx="69" cy="69" r="64" fill="none"/>
                                                </g>
                                            </g>
                                            <g id="seperation" transform="translate(1096.873 277.873)">
                                                <g id="Group_3031" transform="translate(85.585)">
                                                    <rect fill="#d1e2f4" id="Rectangle_704" width="2" height="10" className="cls-7" rx="1" transform="translate(-.458 .127)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_705" width="2" height="10" className="cls-7" rx="1" transform="translate(-.458 161.127)"/>
                                                </g>
                                                <g id="Group_3028" transform="translate(0 84.746)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706" width="1" height="10" className="cls-7" rx=".5" transform="rotate(-90 .754 .627)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707" width="1" height="10" className="cls-7" rx=".5" transform="rotate(-90 81.254 -79.873)"/>
                                                </g>
                                                <g id="Group_3029" transform="rotate(-45 188.301 42.199)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706-2" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 .839 .839)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707-2" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 81.39 -79.712)"/>
                                                </g>
                                                <g id="Group_3030" transform="rotate(-135 103.556 43.038)">
                                                    <rect fill="#d1e2f4" id="Rectangle_706-3" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 .839 .839)"/>
                                                    <rect fill="#d1e2f4" id="Rectangle_707-3" width="1.678" height="10.069" className="cls-7" rx=".839" transform="rotate(-90 81.39 -79.712)"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                        <div className="achjbri-text">
                                            <span className="achjbri-time">{new Date(1000 * this.state.timeTaken).toISOString().substr(11, 8)} <span>Hrs</span></span>
                                            <span className="achjbri-els">Elapsed Time</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="achjb-engine-state">
                                    <p>Engine Run State</p>
                                    <span>{this.state.jobRunState}</span>
                                </div>
                                {/* <div className="achjb-text">
                                    <div className="achjbt-row">
                                        <p>Select Job</p>
                                        <button className="achjbt-jobs" type="button" onClick={(e) => this.openSelectJobs(e)}>SM-TURNINGCLOUD-D-IPOAC</button>
                                        {this.state.selectJobs &&
                                        <div className="achjbtj-dropdown">
                                            <ul>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                                <li>SM-TURNINGCLOUD-D-IPOAC</li>
                                            </ul>
                                        </div>}
                                    </div>
                                </div> */}
                                <div className="achjb-text">
                                    <div className="achjbt-row">
                                        {this.state.jobRunState.toLowerCase() === "not started" || this.state.jobRunState.toLowerCase() === "failed" || this.state.jobRunState.toLowerCase() === "succeeded" ?
                                        <button className="achjbt-run" type="button" onClick={this.runOnDemand}>Run</button> :
                                        this.state.jobRunState.toLowerCase() === "running" ?
                                        <button className="achjbt-run" type="button" onClick={this.stopOnDemand}>Stop</button> :
                                        <button className="achjbt-run" type="button" style={{opacity: "0.5"}} disabled>
                                            <div class="spinner-border text-light" style={{width: "1.5rem", height: "1.5rem", borderWidth: "0.2em"}}></div>
                                        </button>}
                                    </div>
                                    <div className="achjbt-row">
                                        <div className="rodfijdb-text">
                                            <p>Last Engine Run Details</p>
                                            <h5>{this.state.lastEngineRun}</h5>
                                        </div>
                                        <div className="rodfijdb-text">
                                            <p>Last Engine Run Status</p>
                                            <h5>{this.state.lastEngineRunStatus}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.runningOnce && this.state.successFail ? <Succeeded modalState={this.state.modalState} closeSucceeded={(e) => this.closeSucceeded(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}
export default RunOnDemand;