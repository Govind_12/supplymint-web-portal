import React from 'react';
import exclaimIcon from '../../assets/exclain.svg';
import bullet from '../../assets/bullets.svg';
// import { ViewAttributesModal } from './viewAttributesModal';


export const AssingManageRuleModal = (props) => {
    // console.log(props.current,props.maxPage)
    return (

        <div className="modal display_block" id="pocolorModel">
            <div className="backdrop display_block"></div>
            <div className="modal_Indent display_block manageRuleModal">
                <div className="modal-content modalpoColor modalShow ">
                    <div className="col-md-12 col-sm-12">
                        <div className="modal_Color selectVendorPopUp supplierSelectModal">
                            <div className="alignMiddle">
                                <div className="col-md-7 pad-0">
                                    {/* <h2>{props.editAdd=="EDIT"?"Edit Configuration for":"Assign Configuration for"} <span>{props.selectedJobName}</span></h2> */}
                                    <h2>Assigned Configuration for <span>{props.selectedJobName}</span></h2>
                                </div>
                                <div className="col-md-5">
                                    <div className="textRight m-rgt-30">
                                        <button type="button" className="clearBtnGeneric m-rgt-15" onClick={(e)=>props.closeAssignModal(e)}>Cancel</button>
                                      {/* {props.selection || props.isEnabled == "TRUE" || props.isEnabled =="FALSE" ?<button type="button" className="saveBtnGeneric" onClick={(e)=>props.saveGlueParameter(e)}>Save</button>:
                                      <button type="button" className="saveBtnGeneric btnDisabled">Save</button>} */}
                                    </div>
                                </div>
                            </div>
                            <div className="modalContent displayInline width100 m-top-20">
                                <h4>Available Configuration for the above selected job</h4>
                                <div className="col-md-12  pad-0">
                                   {props.keysJobData.map((data, key) =>
                                   
                                    <div className="col-md-4 pad-lft-0 m-top-20" key={key}>

                                     <div className="col-md-12 eachConfigCard">
                                        {/* <div className={props.jobData[data].isSelected=="TRUE"?"eachConfigCard active":"eachConfigCard"}> */}
                                        {/* <div className={data==props.confignameCheck?"eachConfigCard active":"eachConfigCard"}> */}
                                            <div className="col-md-12 pad-0">
                                                <div className="col-md-6 pad-0">
                                                    <label>Config Name</label>
                                                </div>
                                               <div className="col-md-6 pad-0 textRight displayPointer" onClick={(e)=>props.viewAttributes(data)}>
                                                    <span>view all attributes</span>
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-10">
                                                {/* {Object.values(props.jobData[data].data).map((sdata, key) =>
                                                <div className="configDetail" key={key}>
                                                    <h3>{sdata.displayName}</h3>
                                                    <p>{sdata.value.join(',')}</p>
                                                </div>
                                             )} */}
                                                <h3>{props.jobData.response[0].configName}</h3>
                                                <p>{props.jobData.response[0].parameterName}</p>
                                                <div className="configDetail">
                                                  <img src={bullet} />

                                                </div>
                                            </div>
                                            {/* <div className="col-md-12 selectBtn">
                                                <button type="button" onClick={(e)=>props.selectedDiv(data)} >{props.jobData[data].isSelected=="TRUE"?"Selected":"Select"}</button>
                                            </div> */}
                                        </div>
                                     </div>
                                    )}
                                
                                </div>

                            </div>
                            <div className="col-md-12 pad-0 m-top-30 modalBot">
                                <div className="col-md-9 left">
                                    <label><img src={exclaimIcon} className="m-rgt-10" />System will set this configuration enabled by default , you can change this setting now and later from this section</label>
                                    <ul className="pad-0 posRelative">
                                        <li className="pad-lft-25"><label className="checkBoxLabel0 displayPointer pad-0 posInherit">
                                            <input type="checkBox" id="enable" checked={props.isEnabled=="TRUE"} onChange={(e)=>{props.changeConfig(e)}}/> <span className="checkmark1"></span></label>
                                            <label htmlFor="enable">Enable/Disable Config</label>
                                        </li>
                                    </ul>
                                </div>
                                {/* <div className="col-md-3 right previousConfigRight textCenter">
                                    <label className="displayBlock">Showing {props.current} of {props.maxPage} page</label>
                                    <div className="leftRightScroll alignMiddle justifyCenter m-top-15">
                                        <p className="displayinline">Prev</p>
                                        <div className={props.current==1 || props.current==0?"leftShift btnDisabled":"leftShift"} id="prev" onClick={props.current==1||props.current==0? null:(e)=>props.page("prev")}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="21" viewBox="0 0 12 21">
                                                <path fill="none" fillRule="evenodd" stroke="#9100ff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.6" d="M10.281.938l-9.208 9.208 9.208 9.208" />
                                            </svg>
                                        </div>
                                        <div className={props.current!=props.maxPage?"rightShift":"rightShift btnDisabled"} id="next" onClick={props.current!=props.maxPage?(e)=>props.page("next"):null}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="21" viewBox="0 0 12 21">
                                                <path fill="none" fillRule="evenodd" stroke="#9100ff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.6" d="M1.719.938l9.208 9.208-9.208 9.208" />
                                            </svg>
                                        </div>
                                        <p className="displayinline"  >Next</p>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          
        </div>


    );
}