import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import runDemandCircle from "../../assets/rundemandcircle.svg";
import circleInside from "../../assets/circleInside.svg";
import lockIcon from "../../assets/lock.svg";
import arrowIcon from "../../assets/selectIcon.svg";
import SectionLoader from '../loaders/sectionLoader';
import Footer from '../footer'
import Succeeded from "./rodsuccess";
import FmcgFilterModal from "./fmcgFilterModal";
import ArticleFilterModal from "./articleFilterModal";
import VendorFilterModal from "./vendorFilterModal";
import ToastLoader from "../loaders/toastLoader";
import StoreModal from "./storeModal";
class ReplenishmentRunOnDemand extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orgNameGlobal: "",
            orgIdGlobal: "",
            orgCodeGlobal: "",

            runState: "NA",
            run: true,
            lastDate: "NA",
            lastTime: "NA",
            openRightBar: false,
            rightbar: false,
            lastRun: "0 Hours ago",
            status: "Not Started",
            nextScheduled: "NA",
            scheduled: "NA",
            timeTaken: "00",
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            stop: false,
            open: false,
            skecher: false,
            roleData: [],
            zone: [],
            zoneData: [],
            channel: "",
            gradeData: [],
            partner: "",
            partnerData: [],
            grade: [],
            selected: [],
            search: "",
            selectederr: "",
            filter: true,
            remove: false,
            jobRunState: "NA",
            errorCode: "",
            succeeded: false,
            code: "",
            modalState: "",
            percentage: 0,
            lastRunTime: "NA",
            smallLoader1: true,
            smallLoader2: true,
            smallLoader3: true,
            smallLoader4: true,
            triggerName: "",
            filterStores: "",
            applyCityKart: false,
            appliedStockPoint: sessionStorage.getItem("stockPoint") != undefined ? sessionStorage.getItem("stockPoint") : "ALL",
            stockPoint: sessionStorage.getItem("stockPoint") != undefined ? sessionStorage.getItem("stockPoint") : "ALL",

            // _________________________ FILTER NEW STATE__________________________

            modalMotherComapny: false,
            motherCompany: [],
            articleModal: false,
            articleUpdatedData: [],
            vendorModal: false,
            vendorName: [],
            vendorCode: [],
            articleCode: [],
            storeUpdatesName: [],
            storeCodeUpdate: [],
            storeModal: false,
            storeCodeData: "ALL_STORES",
            toastLoader: false,
            toastMsg: "",
            assortmentType: "requirement",
            assortmentFilter: {},
            requirementFilter: [],
            allocationFilter: []

        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    componentDidMount() {
        if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
            let data = {
                motherComp: "",
                articleCode: "",
                vendorCode: "",
                storeCode: ""
            }
            this.props.applyFilterFmcgRequest(data);
            let store = {
                motherComp: "",
                no: 1,
                articleCode: "",
                vendorCode: ""
            }
            this.props.getStoreFmcgRequest(store)
        }

    }



    componentWillMount() {
        var mode = "";
        let orgIdGlobal = ""
        let orgNameGlobal = ""
        let orgCodeGlobal = ""
        if (sessionStorage.getItem('orgId-name') != null) {
            let organization = JSON.parse(sessionStorage.getItem('orgId-name'))

            for (let i = 0; i < organization.length; i++) {
                if (organization[i].active == "TRUE") {
                    orgIdGlobal = organization[i].orgId
                    orgNameGlobal = organization[i].orgName
                    orgCodeGlobal = organization[i].orgCode
                    this.setState({
                        orgNameGlobal: organization[i].orgName,
                        orgIdGlobal: organization[i].orgId,
                        channel: organization[i].orgName,
                        orgCodeGlobal: organization[i].orgCode

                    })
                }
            }
        }
        if (process.env.NODE_ENV == "develop") {
            mode = "-DEV-JOB"
        } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality") {
            mode = "-PROD-JOB"
        }
        else {
            mode = "-DEV-JOB"
        }
        let data = {
            channel: orgCodeGlobal,
        }
        let zonePayload = {
            channel: orgCodeGlobal,
            partner: ""
        }
        let gradeData = {
            channel: orgCodeGlobal,
            partner: "",
            zone: []
        }
        let storedata = {
            channel: orgCodeGlobal,
            partner: "",
            zone: [],
            grade: []
        }


        { sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.props.getPartnerRequest(data) : null }
        { sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.props.getZoneRequest(zonePayload) : null }
        { sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.props.getGradeRequest(gradeData) : null }
        { sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.props.getStoreCodeRequest(storedata) : null }

        this.props.jobRunDetailRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        this.props.lastJobRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);

        this.props.nscheduleRequest(sessionStorage.getItem('partnerEnterpriseCode') + '-TRIGGER');
        this.props.getJobByNameRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);

        // { sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? this.props.customFilterMenuRequest() : null }
    }
    componentWillUnmount() {
        clearInterval(this.statusInterval);
        clearInterval(this.statusIntervall);

    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.replenishment.customFilterMenu.isSuccess) {
            if (nextProps.replenishment.customFilterMenu.data.resource != null) 
            {
               
                this.setState({
                    assortmentFilter: nextProps.replenishment.customFilterMenu.data.resource.filter,
                    loader:false
                })
            }
          this.props.customFilterMenuClear()
        }else if (nextProps.replenishment.customFilterMenu.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.customFilterMenu.message.status,
                errorMessage: nextProps.replenishment.customFilterMenu.message.error == undefined ? undefined : nextProps.replenishment.customFilterMenu.message.error.errorMessage,
                errorCode: nextProps.replenishment.customFilterMenu.message.error == undefined ? undefined : nextProps.replenishment.customFilterMenu.message.error.errorCode
            })
            this.props.customFilterMenuClear();
        }
        if (nextProps.replenishment.lastJob.isSuccess) {
            if (nextProps.replenishment.lastJob.data.resource != null) {
                this.setState({
                    lastRunTime: nextProps.replenishment.lastJob.data.resource.duration == null ? "NA" : nextProps.replenishment.lastJob.data.resource.duration + " mins"
                })
            }
        }
        if (!nextProps.replenishment.stopOnDemand.isSuccess && !nextProps.replenishment.stopOnDemand.isError && !nextProps.replenishment.runOnDemand.isSuccess && !nextProps.replenishment.runOnDemand.isError) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.replenishment.jobRunDetail.isSuccess) {
            if (nextProps.replenishment.jobRunDetail.data.resource != null) {
                this.setState({
                    timeTaken: nextProps.replenishment.jobRunDetail.data.resource.timeTaken,
                    status: nextProps.replenishment.jobRunDetail.data.resource.state,
                    // status: "RUNNING",
                    lastDate: nextProps.replenishment.jobRunDetail.data.resource.lastEngineRun == null ? "NA" : nextProps.replenishment.jobRunDetail.data.resource.lastEngineRun,
                    runState: nextProps.replenishment.jobRunDetail.data.resource.state,
                    smallLoader3: false
                })
            }
            var mode = "";

            if (process.env.NODE_ENV == "develop") {
                mode = "-DEV-JOB"
            } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality") {
                mode = "-PROD-JOB"
            }
            else {
                mode = "-DEV-JOB"
            }
            setTimeout(() => {
                this.props.jobRunDetailRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
            }, 10000)
        } else if (nextProps.replenishment.jobRunDetail.isError) {
            var mode = "";

            if (process.env.NODE_ENV == "develop") {
                mode = "-DEV-JOB"
            } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality") {
                mode = "-PROD-JOB"
            }
            else {
                mode = "-DEV-JOB"
            }
            setTimeout(() => {
                this.props.jobRunDetailRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
            }, 10000)
        }
        else if (nextProps.replenishment.jobRunDetail.isLoading) {
            this.setState({
                smallLoader3: true
            })
        }
        if (nextProps.replenishment.jobRunDetail.isSuccess) {
            if (nextProps.replenishment.jobRunDetail.data.resource != null) {

                if (nextProps.replenishment.jobRunDetail.data.resource.triggerName == "RUN ON DEMAND") {

                    if (nextProps.replenishment.jobRunDetail.data.resource.state == "NOT STARTED" || nextProps.replenishment.jobRunDetail.data.resource.state == "STOPPED") {
                        this.setState({
                            run: true,
                            stop: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,

                        })
                        sessionStorage.removeItem("stockPoint")
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "RUNNING") {
                        this.setState({
                            stop: true,
                            run: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,

                        })
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "SUCCEEDED") {
                        this.setState({
                            stop: false,
                            run: true,
                            succeeded: true,
                            modalState: nextProps.replenishment.jobRunDetail.data.resource.state,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,
                            appliedStockPoint: "ALL",
                            stockPoint: "ALL"
                        })
                        sessionStorage.removeItem("stockPoint")
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "FAILED") {
                        this.setState({
                            run: true,
                            stop: false,
                            succeeded: true,
                            modalState: nextProps.replenishment.jobRunDetail.data.resource.state,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,

                        })
                        sessionStorage.removeItem("stockPoint")
                    } else {
                        this.setState({
                            run: false,
                            stop: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName,

                        })
                        sessionStorage.removeItem("stockPoint")
                    }
                } else {
                    if (nextProps.replenishment.jobRunDetail.data.resource.state == "NOT STARTED" || nextProps.replenishment.jobRunDetail.data.resource.state == "STOPPED" || nextProps.replenishment.jobRunDetail.data.resource.state == "FAILED") {
                        this.setState({
                            run: true,
                            stop: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                        })
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "RUNNING") {
                        this.setState({
                            stop: false,
                            run: false,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                        })
                    } else if (nextProps.replenishment.jobRunDetail.data.resource.state == "SUCCEEDED") {
                        this.setState({
                            stop: false,
                            run: true,
                            percentage: nextProps.replenishment.jobRunDetail.data.resource.percentage,
                            triggerName: nextProps.replenishment.jobRunDetail.data.resource.triggerName
                        })
                   
                 } else {
                        this.setState({
                            run: false,
                            stop: false
                        })
                    }
                }
            }
        }
        if (nextProps.replenishment.nschedule.isSuccess) {
            this.setState({
                nextScheduled: nextProps.replenishment.nschedule.data.resource == null ? "NA" : nextProps.replenishment.nschedule.data.resource.nextSchedule,
                scheduled: nextProps.replenishment.nschedule.data.resource == null ? "NA" : nextProps.replenishment.nschedule.data.resource.schedule,
                smallLoader2: false
            })
        } else if (nextProps.replenishment.nschedule.isLoading) {
            this.setState({
                smallLoader2: true
            })
        }
        if (nextProps.replenishment.jobByName.isSuccess) {
            this.setState({
                smallLoader1: false,
                lastRun: nextProps.replenishment.jobByName.data.resource == null ? "NA" : nextProps.replenishment.jobByName.data.resource.lastEngineRun,
                jobRunState: nextProps.replenishment.jobByName.data.resource == null ? "NA" : nextProps.replenishment.jobByName.data.resource.jobRunState,
                // nextScheduled: nextProps.replenishment.jobByName.data.resource.
            })
        } else if (nextProps.replenishment.jobByName.isLoading) {
            this.setState({
                smallLoader1: true
            })
        }
        if (nextProps.replenishment.runOnDemand.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.replenishment.runOnDemand.isSuccess) {
            if (nextProps.replenishment.runOnDemand.data.resource != null) {
                this.setState({
                    loader: true
                })
                if (nextProps.replenishment.jobRunDetail.data.resource.state == "RUNNING") {
                    this.setState({
                        loader: false
                    })
                    this.props.runOnDemandRequest();
                }
            }

        } else if (nextProps.replenishment.runOnDemand.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.runOnDemand.message.status,
                errorMessage: nextProps.replenishment.runOnDemand.message.error == undefined ? undefined : nextProps.replenishment.runOnDemand.message.error.errorMessage,
                errorCode: nextProps.replenishment.runOnDemand.message.error == undefined ? undefined : nextProps.replenishment.runOnDemand.message.error.errorCode
            })
            this.props.runOnDemandRequest();
        }
        if (nextProps.replenishment.stopOnDemand.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.replenishment.stopOnDemand.isSuccess) {
            if (nextProps.replenishment.stopOnDemand.data.resource != null) {
                this.setState({
                    loader: true
                })
                if (nextProps.replenishment.jobRunDetail.data.resource.state == "STOPPING") {
                    this.setState({
                        loader: false
                    })
                    this.props.stopOnDemandRequest();
                }
            }

        } else if (nextProps.replenishment.stopOnDemand.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.stopOnDemand.message.status,
                errorMessage: nextProps.replenishment.stopOnDemand.message.error == undefined ? undefined : nextProps.replenishment.stopOnDemand.message.error.errorMessage,
                errorCode: nextProps.replenishment.stopOnDemand.message.error == undefined ? undefined : nextProps.replenishment.stopOnDemand.message.error.errorCode
            })
            this.props.stopOnDemandRequest();
        }
        // this.setState({
        //     gradeData: nextProps.replenishment.getGrade.data.resource == undefined ? [] : nextProps.replenishment.getGrade.data.resource.grade,
        //     zoneData:  nextProps.replenishment.getZone.data.resource == undefined ? [] : nextProps.replenishment.getZone.data.resource.zone,
        // })
        // if (nextProps.replenishment.runOnDemand.isLoading || nextProps.replenishment.stopOnDemand.isLoading) {
        //     this.setState({
        //         loader: true
        //     })
        // }
        // if (nextProps.replenishment.getZone.isSuccess && !nextProps.replenishment.getGrade.isSuccess && !nextProps.replenishment.getStoreCode.isSuccess) {
        //     if(nextProps.replenishment.getZone.data.resource.zone.length != this.state.zoneData.length || this.state.roleData.concat(this.state.selected).length != nextProps.replenishment.getZone.data.resource.store.length){
        //        this.setState({

        //             roleData: nextProps.replenishment.getZone.data.resource.store
        //         })
        //     }
        // }
        // if (nextProps.replenishment.getGrade.isSuccess && !nextProps.replenishment.getStoreCode.isSuccess) {
        //     if(this.state.roleData.concat(this.state.selected).length != nextProps.replenishment.getGrade.data.resource.store.length){

        //         this.setState({

        //             roleData: nextProps.replenishment.getGrade.data.resource.store
        //         })
        //     }
        // }

        // if (nextProps.replenishment.getStoreCode.isSuccess) {
        //     // if(this.state.roleData.concat(this.state.selected).length != nextProps.replenishment.getStoreCode.data.resource.store.length){
        //     this.setState({
        //         loader: false,
        //         zoneData: nextProps.replenishment.getStoreCode.data.resource.zone == null ? [] : nextProps.replenishment.getStoreCode.data.resource.zone,
        //         gradeData: nextProps.replenishment.getStoreCode.data.resource.grade == null ? [] : nextProps.replenishment.getStoreCode.data.resource.grade,
        //         roleData: nextProps.replenishment.getStoreCode.data.resource.store == null ? [] : nextProps.replenishment.getStoreCode.data.resource.store
        //     })
        //     // }
        // } else if (nextProps.replenishment.getStoreCode.isLoading) {
        //     this.setState({
        //         loader: true
        //     })
        // }

        //partner
        if (nextProps.replenishment.getPartner.isSuccess) {
            if (nextProps.replenishment.getPartner.data.resource != null) {
                this.setState({
                    loader: false,
                    partnerData: nextProps.replenishment.getPartner.data.resource.partner,
                    roleData: nextProps.replenishment.getPartner.data.resource.store
                })
            }
            this.props.getPartnerRequest();
        } else if (nextProps.replenishment.getPartner.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getPartner.message.status,
                errorMessage: nextProps.replenishment.getPartner.message.error == undefined ? undefined : nextProps.replenishment.getPartner.message.error.errorMessage,
                errorCode: nextProps.replenishment.getPartner.message.error == undefined ? undefined : nextProps.replenishment.getPartner.message.error.errorCode
            })
            this.props.getPartnerRequest();
        }
        //zone
        if (nextProps.replenishment.getZone.isSuccess) {
            if (nextProps.replenishment.getZone.data.resource != null) {
                this.setState({
                    loader: false,
                    zoneData: nextProps.replenishment.getZone.data.resource.zone,
                    roleData: nextProps.replenishment.getZone.data.resource.store
                })
            }
            this.props.getZoneRequest();
        } else if (nextProps.replenishment.getZone.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getZone.message.status,
                errorMessage: nextProps.replenishment.getZone.message.error == undefined ? undefined : nextProps.replenishment.getZone.message.error.errorMessage,
                errorCode: nextProps.replenishment.getZone.message.error == undefined ? undefined : nextProps.replenishment.getZone.message.error.errorCode
            })
            this.props.getZoneRequest();
        }
        //grade
        if (nextProps.replenishment.getGrade.isSuccess) {
            if (nextProps.replenishment.getGrade.data.resource != null) {
                this.setState({
                    loader: false,
                    gradeData: nextProps.replenishment.getGrade.data.resource.grade,
                    roleData: nextProps.replenishment.getGrade.data.resource.store
                })
            }
            this.props.getGradeRequest();
        } else if (nextProps.replenishment.getGrade.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getGrade.message.status,
                errorMessage: nextProps.replenishment.getGrade.message.error == undefined ? undefined : nextProps.replenishment.getGrade.message.error.errorMessage,
                errorCode: nextProps.replenishment.getGrade.message.error == undefined ? undefined : nextProps.replenishment.getGrade.message.error.errorCode
            })
            this.props.getGradeRequest();
        }

        if (nextProps.replenishment.applyFilter.isSuccess) {
            this.setState({
                loader: false,
                filter: false,
                remove: true,
                filterStores: nextProps.replenishment.applyFilter.data.resource == null ? "" : nextProps.replenishment.applyFilter.data.resource.store
            })
            this.props.applyFilterRequest();
        } else if (nextProps.replenishment.applyFilter.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.applyFilter.message.status,
                errorMessage: nextProps.replenishment.applyFilter.message.error == undefined ? undefined : nextProps.replenishment.applyFilter.message.error.errorMessage,
                errorCode: nextProps.replenishment.applyFilter.message.error == undefined ? undefined : nextProps.replenishment.applyFilter.message.error.errorCode
            })
            this.props.applyFilterRequest();
        }

        // ________________________________RECEIVE PROPS FILTER API'S_______________________________

        if (nextProps.replenishment.getMotherComp.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getMotherCompClear();
        } else if (nextProps.replenishment.getMotherComp.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getMotherComp.message.status,
                errorMessage: nextProps.replenishment.getMotherComp.message.error == undefined ? undefined : nextProps.replenishment.getMotherComp.message.error.errorMessage,
                errorCode: nextProps.replenishment.getMotherComp.message.error == undefined ? undefined : nextProps.replenishment.getMotherComp.message.error.errorCode
            })
            this.props.getMotherCompClear();
        }

        if (nextProps.replenishment.getArticleFmcg.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getArticleFmcgClear();
        } else if (nextProps.replenishment.getArticleFmcg.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getArticleFmcg.message.status,
                errorMessage: nextProps.replenishment.getArticleFmcg.message.error == undefined ? undefined : nextProps.replenishment.getArticleFmcg.message.error.errorMessage,
                errorCode: nextProps.replenishment.getArticleFmcg.message.error == undefined ? undefined : nextProps.replenishment.getArticleFmcg.message.error.errorCode
            })
            this.props.getArticleFmcgClear();
        }

        if (nextProps.replenishment.getVendorFmcg.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getVendorFmcgClear();
        } else if (nextProps.replenishment.getVendorFmcg.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getVendorFmcg.message.status,
                errorMessage: nextProps.replenishment.getVendorFmcg.message.error == undefined ? undefined : nextProps.replenishment.getVendorFmcg.message.error.errorMessage,
                errorCode: nextProps.replenishment.getVendorFmcg.message.error == undefined ? undefined : nextProps.replenishment.getVendorFmcg.message.error.errorCode
            })
            this.props.getVendorFmcgClear();
        }
        // ______________________________STORE FILTER _______________________________

        if (nextProps.replenishment.getStoreFmcg.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getStoreFmcgClear();
        } else if (nextProps.replenishment.getStoreFmcg.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getStoreFmcg.message.status,
                errorMessage: nextProps.replenishment.getStoreFmcg.message.error == undefined ? undefined : nextProps.replenishment.getStoreFmcg.message.error.errorMessage,
                errorCode: nextProps.replenishment.getStoreFmcg.message.error == undefined ? undefined : nextProps.replenishment.getStoreFmcg.message.error.errorCode
            })
            this.props.getStoreFmcgClear();
        }

        //store

        if (nextProps.replenishment.getStoreCode.isSuccess) {
            if (nextProps.replenishment.getStoreCode.data.resource != null) {
                this.setState({
                    loader: false,
                    roleData: nextProps.replenishment.getStoreCode.data.resource.store
                })
            }
            this.props.getStoreCodeClear();
        } else if (nextProps.replenishment.getStoreCode.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getStoreCode.message.status,
                errorMessage: nextProps.replenishment.getStoreCode.message.error == undefined ? undefined : nextProps.replenishment.getStoreCode.message.error.errorMessage,
                errorCode: nextProps.replenishment.getStoreCode.message.error == undefined ? undefined : nextProps.replenishment.getStoreCode.message.error.errorCode
            })
            this.props.getStoreCodeClear();
        }

        if (nextProps.replenishment.applyFilterFmcg.isSuccess) {
            this.setState({
                loader: false,
                storeCodeData: nextProps.replenishment.applyFilterFmcg.data.resource.store
            })
            this.props.applyFilterFmcgClear();
        } else if (nextProps.replenishment.applyFilterFmcg.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.applyFilterFmcg.message.status,
                errorMessage: nextProps.replenishment.applyFilterFmcg.message.error == undefined ? undefined : nextProps.replenishment.applyFilterFmcg.message.error.errorMessage,
                errorCode: nextProps.replenishment.applyFilterFmcg.message.error == undefined ? undefined : nextProps.replenishment.applyFilterFmcg.message.error.errorCode
            })
            this.props.applyFilterFmcgClear();
        }

        if (nextProps.replenishment.customFilterMenu.isLoading || nextProps.replenishment.applyFilter.isLoading || nextProps.replenishment.getZone.isLoading || nextProps.replenishment.getPartner.isLoading || nextProps.replenishment.getGrade.isLoading || nextProps.replenishment.getStoreCode.isLoading || nextProps.replenishment.stopOnDemand.isLoading || nextProps.replenishment.runOnDemand.isLoading
            || nextProps.replenishment.getMotherComp.isLoading || nextProps.replenishment.getArticleFmcg.isLoading || nextProps.replenishment.getVendorFmcg.isLoading || nextProps.replenishment.applyFilterFmcg.isLoading || nextProps.replenishment.getStoreFmcg.isLoading) {
            this.setState({
                loader: true
            })
        }

    }
    styleBazzarFilter(type, value) {
        let requirementFilter = this.state.requirementFilter
        let allocationFilter = this.state.allocationFilter
        if (type == "requirement") {
            if (requirementFilter.includes(value)) {
                var index = requirementFilter.indexOf(value)
                if (index > -1) {
                    requirementFilter.splice(index, 1)
                }

            } else {
                requirementFilter.push(value)

            }
            this.setState({
                requirementFilter
            })
        }
        if (type == "allocation") {
            if (allocationFilter.includes(value)) {
                var index = allocationFilter.indexOf(value)
                if (index > -1) {
                    allocationFilter.splice(index, 1)
                }

            } else {
                allocationFilter.push(value)

            }
              this.setState({
                allocationFilter
            })
        }

    }

    handleChange(e) {

        if (e.target.id == "channel") {
            this.setState({
                channel: e.target.value,
                partner: "",
                partnerData: [],
                zone: [],
                grade: [],
                zoneData: [],
                gradeData: [],
                selected: [],
                filter: true,
                remove: false,
            })
            let data = {
                channel: e.target.value
            }
            let zonePayload = {
                channel: e.target.value,
                partner: ""
            }
            let gradeData = {
                channel: e.target.value,
                partner: "",
                zone: []
            }
            let storedata = {
                channel: e.target.value,
                partner: "",
                zone: [],
                grade: []
            }
            this.props.getPartnerRequest(data);
            this.props.getZoneRequest(zonePayload);
            this.props.getGradeRequest(gradeData);
            this.props.getStoreCodeRequest(storedata);
        } else if (e.target.id == "partner") {
            this.setState({
                partner: e.target.value,
                zone: [],
                grade: [],
                zoneData: [],
                gradeData: [],
                selected: [],
                filter: true,
                remove: false,
            })
            let data = {
                channel: this.state.orgCodeGlobal,
                partner: e.target.value
            }
            let gradeData = {
                channel: this.state.orgCodeGlobal,
                partner: e.target.value,
                zone: []
            }
            let storedata = {
                channel: this.state.orgCodeGlobal,
                partner: e.target.value,
                zone: [],
                grade: []
            }
            this.props.getZoneRequest(data);
            this.props.getGradeRequest(gradeData);
            this.props.getStoreCodeRequest(storedata);
        } else if (e.target.id == "stockPoint") {
            this.setState({
                applyCityKart: false,
                stockPoint: e.target.value
            }
            )
        }
        //  else if (e.target.id == "zone") {
        //     this.setState({
        //         zone: e.target.value,
        //         grade: "",
        //         gradeData: [],
        //         selected: [],
        //         filter: true,
        //         remove: false,
        //     })
        //     let data = {
        //         channel: this.state.orgCodeGlobal,
        //         partner: this.state.partner,
        //         zone: e.target.value
        //     }
        //     let storedata = {
        //         channel: this.state.orgCodeGlobal,
        //         partner: this.state.partner,
        //         zone: e.target.value,
        //         grade: ""
        //     }
        //     this.props.getGradeRequest(data);
        //     this.props.getStoreCodeRequest(storedata);
        // } else if (e.target.id == "grade") {
        //     this.setState({
        //         grade: e.target.value,
        //         selected: [],
        //         filter: true,
        //         remove: false,
        //     })
        //     let data = {
        //         channel: this.state.orgCodeGlobal,
        //         partner: this.state.partner,
        //         zone: this.state.zone,
        //         grade: e.target.value
        //     }
        //     this.props.getStoreCodeRequest(data);
        // } 
        // if (e.target.id == "channel") {
        //     this.setState({
        //         channel: e.target.value,
        //         zone: "",
        //         grade: "",
        //     })
        //     let t = this;
        //     let f = e.target;
        //     setTimeout(function () {
        //         let data = {
        //             channel: f.value,
        //             zone: "",
        //             grade: ""
        //         }
        //         t.props.getStoreCodeRequest(data);
        //     }, 100)
        // } else if (e.target.id == "grade") {
        //     this.setState({
        //         grade: e.target.value,
        //     })
        //     const t = this;
        //     let f = e.target;
        //     setTimeout(function () {
        //         let data = {
        //             channel: t.state.orgCodeGlobal,
        //             zone: t.state.zone,
        //             grade: f.value
        //         }
        //         t.props.getStoreCodeRequest(data);
        //     }, 100)
        // } else if (e.target.id == "zone") {
        //     this.setState({
        //         zone: e.target.value,
        //         grade: "",
        //     })
        //     let t = this;
        //     let f = e.target;
        //     setTimeout(function () {
        //         let data = {
        //             channel: t.state.orgCodeGlobal,
        //             grade: "",
        //             zone: f.value
        //         }
        //         t.props.getStoreCodeRequest(data);
        //     }, 100)
        // }
    }

    onStop(e) {
        this.setState({
            loader: true,




        })
        e.preventDefault();
        var mode = "";

        if (process.env.NODE_ENV == "develop") {
            mode = "-DEV-JOB"
        } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality") {
            mode = "-PROD-JOB"
        }
        else {
            mode = "-DEV-JOB"
        }
        this.props.stopOnDemandRequest('SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode);
        this.props.nscheduleRequest(sessionStorage.getItem('partnerEnterpriseCode') + '-TRIGGER');
    }

    onRun(e) {
        this.setState({
            loader: true,
            filter: true,
            remove: false,



        })
        e.preventDefault();
        var mode = "";

        if (process.env.NODE_ENV == "develop") {
            mode = "-DEV-JOB"
        } else if (process.env.NODE_ENV == "production" || process.env.NODE_ENV == "quality") {
            mode = "-PROD-JOB"
        }
        else {
            mode = "-DEV-JOB"
        }
        // if (this.state.selected.length == 0) {
        // this.setState({
        //     selected: this.state.roleData,
        //     roleData: []
        // })
        if (sessionStorage.getItem('partnerEnterpriseName') == "CITYKART") {
            let data = {
                jobName: 'SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode,
                store: "NA",
                selected: this.state.filterStores,
                runType: "filter",
                path: "NA",
                stockPoint: this.state.appliedStockPoint

            }


            sessionStorage.setItem("stockPoint", this.state.appliedStockPoint)

            this.props.runOnDemandRequest(data);


        } else if (sessionStorage.getItem('partnerEnterpriseName') == "VMART") {
            let data = {
                jobName: 'SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode,
                store: "fmcg",
                storeCode: this.state.storeCodeData,
                // storeCode: sessionStorage.getItem('partnerEnterpriseName') == "VMART" ? this.state.storeCodeData : "ALL_STORES",
                runType: "filter",
                path: "NA",
                stockPoint: this.state.appliedStockPoint
            }


            sessionStorage.setItem("stockPoint", this.state.appliedStockPoint)

            this.props.runOnDemandRequest(data);
        } else {
            let data = {
                jobName: 'SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode,
                store: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.state.orgCodeGlobal : "NA",
                selected: this.state.filterStores,
                // storeCode: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.state.selected : "ALL_STORES",
                runType: "filter",
                path: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? "s3://com-supplymint-glue/dev_etl/skechers/output/" : "NA",
                stockPoint: 'NA',
                requirement:this.state.requirementFilter.length!=0?this.state.requirementFilter.join('|'):"NA",
                allocation:this.state.allocationFilter.length!=0?this.state.allocationFilter.join('|'):"NA",
            }


            this.props.runOnDemandRequest(data);
        }
        // } else {
        //     let data = {
        //         runType: "no-filter",
        //         jobName: 'SMGLUE-' + sessionStorage.getItem('partnerEnterpriseCode').toUpperCase() + mode,
        //         store: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? this.state.orgCodeGlobal : "NA",
        //          : this.state.selected,
        //         path: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? "s3://com-supplymint-glue/dev_etl/skechers/output/" : "NA",
        //     }
        //     this.props.runOnDemandRequest(data);
        // }
    }
    // openClose(e) {
    //     e.preventDefault();
    //     this.setState({
    //         open: !this.state.open
    //     })
    // }

    Change(e) {
        this.setState({
            search: e.target.value
        })
    }
    selected() {
        if (this.state.selected.length == 0) {
            this.setState({
                selectederr: true
            });
        } else {
            this.setState({
                selectederr: false
            })
        }
    }
    onnChange(id) {
        this.setState({
            current: id,
        })
        let match = _.find(this.state.roleData, function (o) {
            return o.storeCode == id;
        })
        this.setState({
            selected: this.state.selected.concat(match)
        });
        let idd = match.storeCode;
        var array = this.state.roleData;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === idd) {
                array.splice(i, 1);
            }
            else {

            }
        }
        this.setState({
            roleData: array,
            selectederr: false,
            filter: true,
            remove: false
        })

    }
    onDelete(e) {
        let match = _.find(this.state.selected, function (o) {
            return o.storeCode == e;
        })
        this.setState({
            roleData: this.state.roleData.concat(match)
        });

        var array = this.state.selected;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === e) {
                array.splice(i, 1);
            }
        }
        this.setState({
            selected: array,
            filter: true,
            remove: false
        })
        if (this.props.replenishment.getZone.isSuccess && !this.props.replenishment.getGrade.isSuccess && !this.props.replenishment.getStoreCode.isSuccess) {
            this.props.replenishment.getZone.data.resource.store = this.state.roleData.concat(match)
        } else if (this.props.replenishment.getGrade.isSuccess && !this.props.replenishment.getStoreCode.isSuccess) {
            this.props.replenishment.getGrade.data.resource.store = this.state.roleData.concat(match)
        } else if (this.props.replenishment.getStoreCode.isSuccess) {
            this.props.replenishment.getStoreCode.data.resource.store = this.state.roleData.concat(match);
        }

    }
    onFilterApply(e) {

        e.preventDefault();
        if (this.state.selected.length == 0) {

            let data = {
                channel: this.state.orgCodeGlobal,
                zone: this.state.zone.length == 0 ? [] : this.state.zone,
                grade: this.state.grade.length == 0 ? [] : this.state.grade,
                partner: this.state.partner == "" ? "" : this.state.partner,
                storeCode: "NA"
            }
            this.props.applyFilterRequest(data);
        } else {
            let abc = "";
            for (let i = 0; i < this.state.selected.length; i++) {
                let a = this.state.selected[i].storeCode;
                if (this.state.selected.length > 1) {
                    abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
                } else {
                    abc = a;
                }
            }
            let data = {
                channel: this.state.orgCodeGlobal,
                zone: this.state.zone.length == 0 ? [] : this.state.zone,
                grade: this.state.grade.length == 0 ? [] : this.state.grade,
                partner: this.state.partner == "" ? "" : this.state.partner,
                storeCode: abc
            }
            this.props.applyFilterRequest(data);
        }

    }
    clearFilterdData(e) {
        e.preventDefault();
        this.setState({
            roleData: [],
            zone: [],
            zoneData: [],
            gradeData: [],
            partnerData: [],
            partner: "",
            grade: [],
            selected: [],
            remove: false,
            filter: true,




        })
        let data = {
            channel: this.state.orgCodeGlobal
        }
        let zonePayload = {
            channel: this.state.orgCodeGlobal,
            partner: ""
        }
        let gradeData = {
            channel: this.state.orgCodeGlobal,
            partner: "",
            zone: []
        }
        let storedata = {
            channel: this.state.orgCodeGlobal,
            partner: "",
            zone: [],
            grade: []
        }

        this.props.getPartnerRequest(data);
        this.props.getZoneRequest(zonePayload);
        this.props.getGradeRequest(gradeData);
        this.props.getStoreCodeRequest(storedata);
    }

    closeSucceeded(type) {
        this.setState({
            succeeded: false
        })
        sessionStorage.removeItem('jobId');
        if (type != "close") {

            this.props.history.push('/inventoryPlanning/summary');
        }
    }
    onCityKartApply() {
        this.setState({
            applyCityKart: true,
            appliedStockPoint: this.state.stockPoint
        })
    }
    // _______________________________________  REPLENISHMENT NEW FILTER INTEGRATION _________________________

    montherCompanyOpenModal() {
        this.setState({
            modalMotherComapny: true
        })
        let motherData = {
            no: 1,
            search: "",
            type: 1
        }
        this.props.getMotherCompRequest(motherData)
    }
    updateMotherComp(data) {
        this.setState({
            motherCompany: data
        }, () => {
            let storeData = {
                motherComp: this.state.motherCompany.join('|'),
                no: 1,
                articleCode: this.state.articleCode == "" ? "" : this.state.articleCode,
                vendorCode: this.state.vendorCode == "" ? "" : this.state.vendorCode
            }
            this.storeUpdate(storeData)
        })
        this.closeMotherComp();
        this.setState({

            vendorCode: [],
            vendorName: [],
            articleUpdatedData: [],
            articleCode: [],
            selected: [],
            storeCodeData: "",
            storeUpdatesName: [],
            storeCodeUpdate: [],
            filter: true,
            remove: false
        })
    }
    closeMotherComp() {
        this.setState({
            modalMotherComapny: false
        })
    }

    // ___________________________-OPEN ARTICLE MODAL______________________________

    ArticleModalOpen(e) {
        // if (this.state.motherCompany == "") {

        //     this.setState({
        //         toastMsg: "Please select mother company ",
        //     })
        //     setTimeout(() => {
        //         this.setState({
        //             toastLoader: false
        //         })
        //     },1000)
        // } else {
        this.setState({
            articleModal: true
        })
        let articleData = {
            no: 1,
            search: "",
            type: 1,
            motherComp: this.state.motherCompany.join('|')
        }
        this.props.getArticleFmcgRequest(articleData)
        // }
    }

    updateArticle(data) {
        if (!this.state.articleCode.includes(data.Code)) {
            this.state.articleCode.push(data.Code)
            this.setState({
                articleUpdatedData: data.Name,
                articleCode: this.state.articleCode
            }, () => {
                let storeData = {
                    motherComp: this.state.motherCompany.join('|'),
                    no: 1,
                    articleCode: this.state.articleCode == "" ? "" : this.state.articleCode.join('|'),
                    vendorCode: this.state.vendorCode == "" ? "" : this.state.vendorCode.join('|')
                }
                this.storeUpdate(storeData)
            })
        }
        this.closeArticleModal()
        this.setState({

            vendorCode: [],
            vendorName: [],

            selected: [],
            storeCodeData: "",
            storeUpdatesName: [],
            storeCodeUpdate: [],
            filter: true,
            remove: false
        })
    }
    closeArticleModal() {
        this.setState({
            articleModal: false
        })
    }
    // ____________________________VENDOR MODAL CODE ______________________________

    vendorModalOpen(e) {
        // if (this.state.articleUpdatedData == "") {
        //     this.setState({
        //         toastMsg: "Please select article",
        //         toastLoader: true
        //     })
        //     setTimeout(() => {
        //         this.setState({
        //             toastLoader: false
        //         })
        //     },1000)
        // } else {
        this.setState({
            vendorModal: true
        })
        let vendorData = {
            no: 1,
            search: "",
            type: 1,
            motherComp: this.state.motherCompany.join('|'),
            articleCode: this.state.articleCode.join('|')
        }
        this.props.getVendorFmcgRequest(vendorData)
        // }
    }

    updateVendor(data) {
        if (!this.state.vendorCode.includes(data.Code)) {
            this.state.vendorCode.push(data.Code)
            this.setState({
                vendorName: data.Name,
                vendorCode: this.state.vendorCode
            }, () => {
                let storeData = {
                    motherComp: this.state.motherCompany.join('|'),
                    no: 1,
                    articleCode: this.state.articleCode == "" ? "" : this.state.articleCode.join('|'),
                    vendorCode: this.state.vendorCode == "" ? "" : this.state.vendorCode.join('|')
                }
                this.storeUpdate(storeData)
            })
        }
        this.setState({

            selected: [],
            storeCodeData: "",
            storeUpdatesName: [],
            storeCodeUpdate: [],
            filter: true,
            remove: false
        })
        this.closeVendorModal()
    }
    closeVendorModal() {
        this.setState({
            vendorModal: false
        })
    }
    // ___________________________STORE MODAL CODE______________________________-

    storeModalOpen() {
        this.setState({
            storeModal: true
        })
        let store = {
            no: 1,
            type: 1,
            search: "",
            motherComp: this.state.motherCompany.join('|'),

            articleCode: this.state.articleCode.join('|'),
            vendorCode: this.state.vendorCode.join('|')
        }
        this.props.getStoreFmcgRequest(store)
    }

    closeStoreModal(e) {
        this.setState({
            storeModal: false
        })
    }
    updateStore(data) {


        this.setState({
            storeUpdatesName: data.Name,
            storeCodeUpdate: data.Code
        })

        this.closeStoreModal()
    }

    // ______________________________UPDATE STORE CODE___________________________
    storeUpdate(data) {
        let store = {
            motherComp: data.motherComp,
            no: 1,
            articleCode: data.articleCode,
            vendorCode: data.vendorCode
        }
        this.props.getStoreFmcgRequest(store)
    }

    applyFiltermrt(e) {

        let data = {
            motherComp: this.state.motherCompany.join('|'),
            articleCode: this.state.articleCode.join('|'),
            vendorCode: this.state.vendorCode.join('|'),
            storeCode: this.state.storeCodeUpdate.length != 0 ? this.state.storeCodeUpdate.join('|') : ""
        }


        this.props.applyFilterFmcgRequest(data);
        this.setState({
            filter: false,
            remove: true
        })
    }
    clearFiltermrt() {
        this.setState({
            motherCompany: [],
            vendorCode: [],
            vendorName: [],
            articleUpdatedData: [],
            articleCode: [],
            selected: [],
            storeCodeData: "",
            storeUpdatesName: [],
            storeCodeUpdate: [],
            filter: true,
            remove: false
        })
    }
    ZoneChange(data) {

        let zone = this.state.zone
        var index = zone.indexOf(data);
        if (index > -1) {
            zone.splice(index, 1);
        } else {
            zone.push(data)
        }


        this.setState({
            zone,

            grade: [],
            gradeData: [],
            selected: [],
            filter: true,
            remove: false,
        }, () => {
            let data = {
                channel: this.state.orgCodeGlobal,
                partner: this.state.partner,
                zone: this.state.zone
            }
            let storedata = {
                channel: this.state.orgCodeGlobal,
                partner: this.state.partner,
                zone: this.state.zone,
                grade: this.state.grade
            }

            this.props.getGradeRequest(data);
            this.props.getStoreCodeRequest(storedata);
        })

    }

    gradeChange(data) {

        let grade = this.state.grade
        var index = grade.indexOf(data);
        if (index > -1) {
            grade.splice(index, 1);
        } else {
            grade.push(data)
        }

        this.setState({
            grade,

            selected: [],
            filter: true,
            remove: false,
        }, () => {

            let data = {
                channel: this.state.orgCodeGlobal,
                partner: this.state.partner,
                zone: this.state.zone,
                grade: this.state.grade
            }
            this.props.getStoreCodeRequest(data);
        })


    }
    assortmentTypeFun(data) {
        this.setState({
            assortmentType: data
        })

    }
    onFilterApplySB(){
        if(this.state.assortmentFilter.length!=0 && this.state.requirementFilter.length!=0){

        this.setState({
              filter:false,
            remove:true
        })
    }else{

            this.setState({
                toastMsg: "Please select filters first",
                  toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            },2000)

    }

    }
    clearFilterDataSB(){
        this.setState({
            allocationFilter:[],
            requirementFilter:[],
            filter:true,
            remove:false

        })
    }
    /**
   * Set the wrapper ref
   */


    render() {
        console.log(this.state.allocationFilter,this.state.requirementFilter,this.state.assortmentFilter)
        const { search, roleData, selectederr } = this.state;
        var result = _.filter(roleData, function (data) {
            return _.startsWith(data.storeName.toLowerCase(), search.toLowerCase());
        });
        return (
            <div>
                <div className="container-fluid">

                    <div className="container_div" id="">

                        <div className="container-fluid">
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                <div className="organisation_container heightAuto">
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <div className="col-md-5 col-sm-6 pad-0">
                                            <ul className="list_style">
                                                <li>
                                                    <label className="contribution_mart">
                                                        RUN ON DEMAND
                                                    </label>
                                                </li>
                                                <li>
                                                    <p className="master_para">Run your rule engine here</p>
                                                </li>
                                            </ul>
                                        </div>
                                        {sessionStorage.getItem('partnerEnterpriseName') == "VMART" && <div className="col-md-7 col-xs-6">
                                            <div className="headingfilterSketch">
                                                <h2>FILTERS</h2>
                                            </div>

                                        </div>}
                                        {sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" || sessionStorage.getItem('partnerEnterpriseName') == "CITYKART" ? <div className="col-md-7 col-xs-6">
                                            <div className="headingfilterSketch">
                                                <h2>FILTERS</h2>
                                            </div>
                                        </div> : null}
                                        {/*{sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" && <div className="col-md-7 col-xs-6">
                                            <div className="headingfilterSketch">
                                                <h2>FILTERS</h2>
                                            </div>

                                        </div>}*/}

                                    </div>
                                    <div className="col-md-12 col-sm-12 col-sx-12 pad-0">
                                        <div className="col-md-5 col-sm-6 col-xs-12 pad-0 m-top-10">
                                            <div className="runDemandSvg">
                                                <div className="runDemandBg">
                                                    <img src={runDemandCircle} className="firstSvg" />
                                                    <img src={circleInside} className="midSvg" />
                                                </div>
                                                {/* <img src={counterArea} className="lastImg" /> */}
                                                <div className="timeRunDemandMid">
                                                    <div className="clearfix">

                                                        <div className="c101 p51 big1">
                                                            <div className="slice1">
                                                                <div className={this.state.percentage < 51 ? `d${this.state.percentage} progress-circle-demand` : `over51 progress-circle-demand d${this.state.percentage}`}>
                                                                    <div className="left-half-clipper-demand">
                                                                        <div className="first50-bar-demand"></div>
                                                                        <div className="value-bar-demand"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="runDemandMiddle">
                                                    <div className="midSvgbg">
                                                        <svg className="lastImg" xmlns="http://www.w3.org/2000/svg" width="193" height="194" viewBox="0 0 193 194">

                                                            <g fill="none" fillRule="evenodd">

                                                                {/* <text fill="#4A4A4A" fontFamily="Montserrat-Bold, Montserrat" fontSize="35" fontWeight="bold" letterSpacing=".222" transform="translate(-3.6 -3.2)">
                                                            <tspan x="50.471" y="51.492" fontFamily="Helvetica" color=" #8f8f8f" fontSize="14" fontWeight="600">Elapsed Time</tspan>
                                                                
                                                                    <tspan x="82.483" y="95">{this.state.timeTaken}</tspan>  <tspan x="72.471" y="130.492" fontFamily="Montserrat-Medium, Montserrat" fontSize="24" fontWeight="500">Min</tspan>
                                                                </text> */}
                                                            </g>
                                                        </svg>
                                                    </div>
                                                    <div className="autoConfigMid">
                                                        <div className="runDemandTimeElapsed">
                                                            <h5>Time Elapsed</h5>
                                                            <h3>{this.state.timeTaken}</h3>
                                                            {/* <h3>0</h3> */}
                                                            <p>Min</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div className="lastEngineRunTime">
                                                <p>Last Engine Run Time <span>{this.state.lastRunTime}</span></p>
                                            </div>
                                            <div className="lftSideContent">
                                                <ul className="list-inline width100">
                                                    <li>
                                                        <h3>Engine Run State : <span>{this.state.status}</span></h3>
                                                    </li>
                                                    <li>
                                                        {this.state.runState == "RUNNING" && this.state.triggerName == `${sessionStorage.getItem('partnerEnterpriseCode')}-TRIGGER` ? <p>
                                                            <svg className="m-rgt-10" xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17 17">
                                                                <defs>
                                                                    <linearGradient id="a" x1="50%" x2="50%" y1="0%" y2="100%">
                                                                        <stop offset="0%" stopColor="#FBD249" />
                                                                        <stop offset="100%" stopColor="#F5A623" />
                                                                    </linearGradient>
                                                                </defs>
                                                                <path fill="url(#a)" fillRule="nonzero" d="M8.5 0C3.812 0 0 3.812 0 8.5 0 13.188 3.812 17 8.5 17c4.688 0 8.5-3.812 8.5-8.5C17 3.812 13.188 0 8.5 0zm0 1.133A7.358 7.358 0 0 1 15.867 8.5 7.358 7.358 0 0 1 8.5 15.867 7.358 7.358 0 0 1 1.133 8.5 7.358 7.358 0 0 1 8.5 1.133zm0 1.134c-.626 0-1.133.507-1.133 1.133l.189 7.367a.944.944 0 0 0 1.888 0l.19-7.367c0-.626-.508-1.133-1.134-1.133zm0 10.2a1.133 1.133 0 1 0 0 2.266 1.133 1.133 0 0 0 0-2.266z" />
                                                            </svg>

                                                            You can't stop engine now as its already running from Auto Configuration.
                                                        </p> : null}
                                                    </li>
                                                    <li>

                                                        {this.state.status == "RUNNING" || this.state.status == "STOPPING" ? null : sessionStorage.getItem('partnerEnterpriseName') == 'SKECHERS' ? !this.state.filter ? this.state.run ? <button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                                            RUN
                                                        </button> : null : <div><button type="button" className="btnDisabled launchBtn" >
                                                                RUN
                                                        </button> <span className="dropShow">Please apply filter to run</span> </div> : null}

                                                        {/*{this.state.status == "RUNNING" || this.state.status == "STOPPING" ? null : sessionStorage.getItem('partnerEnterpriseCode') == 'STYLEBAZAAR' ? !this.state.filter ? this.state.run ? <button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                                            RUN
                                                        </button> : null : <div><button type="button" className="btnDisabled launchBtn" >
                                                                RUN
                                                        </button> <span className="dropShow">Please apply filter to run</span> </div> : null}*/}
                                                         {/*&&  sessionStorage.getItem("partnerEnterpriseCode") != "STYLEBAZAAR"*/}
                                                        {sessionStorage.getItem('partnerEnterpriseName') != 'SKECHERS'
                                                         ? this.state.run ? <button type="button" onClick={(e) => this.onRun(e)} className="launchBtn" >
                                                            RUN
                                                        </button> : null : null}
                                                        {/* </div>:null} */}

                                                        {this.state.stop ? <button type="button" onClick={(e) => this.onStop(e)} className="launchBtn" >
                                                            STOP
                                                        </button> : null}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-md-7 col-xs-12 pad-0">
                                            {/*{sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" && <div>
                                                {this.state.status == "RUNNING" ? <div className="toolTipMainHover skechersPatch">
                                                    <div className="noClickDiv">
                                                    </div>
                                                    <div className="topToolTip ">

                                                        <span className="topToolTipText skechersTooltip">
                                                            <img src={lockIcon} />
                                                            <h3>Temporary Locked</h3>
                                                            <p>You can’t apply filter as its already running, This feature will
                                                                    be available after job completion</p>
                                                        </span>

                                                    </div>
                                                </div> : null}
                                                <div className="col-md-12 col-sm-12 pad-0 filterRunOnDemand" >
                                                    <div className="col-md-12 col-xs-12 col-sm-12">
                                                        <div className="filterSketches">
                                                            <div className="radioFilters">
                                                                <button type="button" className={this.state.assortmentType == "requirement" ? "blueCurveButton m-rgt-10" : "notActiveBtn m-rgt-10"} onClick={(e) => this.assortmentTypeFun("requirement")}>Assortment for requirement</button>
                                                                <button type="button" className={this.state.assortmentType == "allocation" ? "blueCurveButton" : "notActiveBtn"} onClick={(e) => this.assortmentTypeFun("allocation")}>Assortment for allocation</button>
                                                                <div className="row m0">
                                                                    <div className="col-md-12 pad-0 m-top-15">
                                                                        {this.state.assortmentType == "requirement" ?Object.keys(this.state.assortmentFilter).length!=0? Object.keys(this.state.assortmentFilter.requirement).map((data, idx) => (
                                                                            <div className="col-md-3 pad-0" key={idx}>
                                                                                <label className="checkBoxLabel0 displayPointer"><input type="checkBox" className="checkBoxTab" checked={this.state.requirementFilter.includes(this.state.assortmentFilter.requirement[data])} onClick={(e) => this.styleBazzarFilter("requirement", this.state.assortmentFilter.requirement[data])} /> <span className="checkmark1"></span> <h4 className="sketchesFormInput">{data}</h4></label>
                                                                            </div>
                                                                        )):null :Object.keys(this.state.assortmentFilter).length!=0? Object.keys(this.state.assortmentFilter.allocation).map((data, idxxx) => (
                                                                            <div className="col-md-3 pad-0" key={idxxx}>
                                                                                <label className="checkBoxLabel0 displayPointer"><input type="checkBox" className="checkBoxTab" checked={this.state.allocationFilter.includes(this.state.assortmentFilter.allocation[data])} onClick={(e) => this.styleBazzarFilter("allocation", this.state.assortmentFilter.allocation[data])} /> <span className="checkmark1"></span> <h4 className="sketchesFormInput">{data}</h4></label>
                                                                            </div>
                                                                        )):null}


                                                                    </div>
                                                                   
                                                                        {this.state.filter ? <button className="sktechFilterBth m-top-20" onClick={(e) => this.onFilterApplySB(e)}>
                                                                            Apply Filters
                                                                    </button> : null}

                                                                        {this.state.remove ? <button className="sktechFilterBth m-top-20" onClick={(e) => this.clearFilterDataSB(e)}>
                                                                            Remove
                                                                    </button> : null}

                                                                </div>
                                                            </div> </div></div></div></div>}*/}
                                            {sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ?
                                                <div>
                                                    {this.state.status == "RUNNING" ? <div className="toolTipMainHover skechersPatch">
                                                        <div className="noClickDiv">
                                                        </div>
                                                        <div className="topToolTip ">

                                                            <span className="topToolTipText skechersTooltip">
                                                                {/* You cannot apply filter as the job is already running. */}
                                                                <img src={lockIcon} />
                                                                <h3>Temporary Locked</h3>
                                                                <p>You can’t apply filter as its already running, This feature will
                                                                    be available after job completion</p>
                                                            </span>

                                                        </div>
                                                    </div> : null}



                                                    <div className="col-md-12 col-sm-12 pad-0 filterRunOnDemand" >
                                                        <div className="col-md-12 col-xs-12 col-sm-12">
                                                            <div className="filterSketches">

                                                                <div className="formSketches">
                                                                    <div className="sketchesFormDiv">
                                                                        <label className="sketchesFormInput displayBlock">
                                                                            Channel
                                                            </label>
                                                                        {/*<select onChange={(e) => this.handleChange(e)} value={this.state.channel} id="channel" className="purchaseSelectBox">

                                                                            <option value="Retail">Retail</option>
                                                                            <option value="MBO">MBO</option>

                                                                        </select>*/}
                                                                        <input type="text" value={this.state.channel} className="form-box width100" id="channel" placeholder="channel" disabled />

                                                                    </div>
                                                                    <div className="sketchesFormDiv">
                                                                        <label className="sketchesFormInput">
                                                                            Partner
                                                                     </label>
                                                                        <select onChange={(e) => this.handleChange(e)} value={this.state.partner} id="partner" className="purchaseSelectBox">
                                                                            <option value="">
                                                                                Select Partner
                                                                        </option>
                                                                            {this.state.partnerData.length == 0 ? null : this.state.partnerData == null ? null : this.state.partnerData.map((data, key) => (<option key={key} value={data}>
                                                                                {data}
                                                                            </option>))}
                                                                        </select>
                                                                    </div>

                                                                    <div className="sketchesFormDiv">
                                                                        <label className="sketchesFormInput">
                                                                            Zone
                                                                     </label>
                                                                        <div className="settingDrop checkboxDrop dropdown displayInline pad-0 width100 udfDropDown setUdfMappingMain rondZone">
                                                                            <button className="btn btn-default dropdown-toggle userModalSelect onFocus textOverflowNone " type="button">

                                                                                {this.state.zone.length == 0 ? "Select Zone" : this.state.zone.join(',')}

                                                                                <i className="fa fa-chevron-down"></i>
                                                                            </button>


                                                                            <ul className="zoneli dropdown-menu " >
                                                                                {this.state.zoneData.length == 0 || this.state.zoneData == null ? <li > <a className="hoverBgNone">No Data Found</a>
                                                                                </li> : this.state.zoneData.map((data, key) => (
                                                                                    <li key={key}>
                                                                                        <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input className="checkBoxTab" type="checkBox" checked={this.state.zone.includes(data)} onClick={(e) => this.ZoneChange(data)} /> <span className="checkmark1"></span> </label>
                                                                                        <a className="hoverBgNone">{data}</a>
                                                                                    </li>
                                                                                ))}
                                                                            </ul>

                                                                        </div>
                                                                    </div>
                                                                    <div className="sketchesFormDiv">
                                                                        <label className="sketchesFormInput">
                                                                            Grade
                                                                     </label>
                                                                        <div className="settingDrop checkboxDrop dropdown displayInline pad-0 width100 udfDropDown setUdfMappingMain rondGrade" >
                                                                            <button className="btn btn-default dropdown-toggle userModalSelect onFocus textOverflowNone" type="button" >
                                                                                {this.state.grade.length == 0 ? "Select Grade" : this.state.grade.join(',')}
                                                                                <i className="fa fa-chevron-down"></i>
                                                                            </button>


                                                                            <ul className="gradeli dropdown-menu " >
                                                                                {this.state.gradeData.length == 0 || this.state.gradeData == null ? <li > <a className="hoverBgNone">No Data Found</a>
                                                                                </li> : this.state.gradeData.map((data, key) => (
                                                                                    <li key={key}>
                                                                                        <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input className="checkBoxTab" type="checkBox" checked={this.state.grade.includes(data)} onClick={(e) => this.gradeChange(data)} /> <span className="checkmark1"></span> </label>
                                                                                        <a className="hoverBgNone">{data}</a>
                                                                                    </li>
                                                                                ))}
                                                                            </ul>

                                                                        </div>
                                                                    </div>
                                                                    {/*<select onChange={(e) => this.handleChange(e)} value={this.state.zone} id="zone" className="purchaseSelectBox">
                                                                            <option value="">
                                                                                Select Zone
                                                                        </option>
                                                                            {this.state.zoneData.length == 0 ? null : this.state.zoneData == null ? null : this.state.zoneData.map((data, key) => (<option key={key} value={data}>
                                                                                {data}
                                                                            </option>))}
                                                                        </select>   */}

                                                                    {/*<div className="sketchesFormDiv storeGradeMain">
                                                                        <label className="sketchesFormInput">
                                                                            Store Grade
                                                            </label>
                                                                        <select onChange={(e) => this.handleChange(e)} value={this.state.grade} id="grade" className="purchaseSelectBox">
                                                                            <option value="">
                                                                                Select Store Grade
                                                                </option>
                                                                            {this.state.gradeData.length == 0 ? null : this.state.gradeData == null ? null : this.state.gradeData.map((data, key) => (<option key={key} value={data}>
                                                                                {data}
                                                                            </option>))}
                                                                        </select>
                                                                    </div>*/}

                                                                    <div className="sketchesFormDiv m-top-20 width50">
                                                                        <label className="sketchesFormInput" >
                                                                            Choose Stores
                                                            <span> ( you can choose multiple stores at a time )</span>
                                                                        </label>
                                                                        <div className="purchaseSelectBox runOndemandDrop width60per">
                                                                            <label className="SketchBox">{this.state.selected.length} Stores selected</label>
                                                                            <span className="spanSketch">
                                                                                ▾
                                                                        </span>
                                                                        </div>

                                                                        <div className="dropdownSketchFilter skechersDrop dropDownSkech">
                                                                            <div className="SketchFilter">
                                                                                <p>Choose Stores from below list :</p>
                                                                                <input type="text" placeholder="Type to Search…" value={search} onChange={(e) => this.Change(e)} />
                                                                            </div>
                                                                            <div className="sketchStoreDrop">
                                                                                <div className="lftStoreDiv m-rgt-20">
                                                                                    <h2>Choose Stores</h2>
                                                                                    <div className="storeChoose">
                                                                                        <ul className="list-inline">
                                                                                            {result.length == 0 ? null : result.map((data, i) => <li key={i} onClick={(e) => this.onnChange(`${data.storeCode}`)} >
                                                                                                {/* <p className="para_purchase">
                                                                                            <input type="text" className="purchaseCheck" type="checkbox" id="c8" name="cb" />
                                                                                            <label htmlFor="c8" className="purchaseLabel"></label>
                                                                                        </p> */}
                                                                                                <span className="onHover"></span>
                                                                                                <p className="contentStore pad-5">{data.storeName}</p>

                                                                                            </li>)}

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="lftStoreDiv">
                                                                                    <h2>Selected Stores</h2>
                                                                                    <div className="storeChoose pad-10">
                                                                                        <ul className="list-inline">
                                                                                            <li>
                                                                                                {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.storeCode} className="">
                                                                                                    <p className="contentStore">{data.storeName} </p>
                                                                                                    <span onClick={(e) => this.onDelete(data.storeCode)} value={data.storeCode} className="close_btn_demand" aria-hidden="true">&times;</span>
                                                                                                </div>)}
                                                                                            </li>
                                                                                        </ul>

                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div className="sketchesFormDiv m-top-20">
                                                                        {this.state.filter ? <button className="sktechFilterBth " onClick={(e) => this.onFilterApply(e)}>
                                                                            Apply Filters
                                                                    </button> : null}

                                                                        {this.state.remove ? <button className="sktechFilterBth" onClick={(e) => this.clearFilterdData(e)}>
                                                                            Remove
                                                                    </button> : null}

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-top-10">
                                                            <span className="borderFilter"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                : null}
                                            {sessionStorage.getItem('partnerEnterpriseName') == "CITYKART" ?
                                                <div>
                                                    {this.state.status == "RUNNING" ? <div className="toolTipMainHover skechersPatch cityKartNoClick">
                                                        <div className="noClickDiv">
                                                        </div>
                                                        <div className="topToolTip ">

                                                            <span className="topToolTipText skechersTooltip">
                                                                {/* You cannot apply filter as the job is already running. */}
                                                                <img src={lockIcon} />
                                                                <h3>Temporary Locked</h3>
                                                                <p>You can’t apply filter as its already running, This feature will
                                                                    be available after job completion</p>
                                                            </span>

                                                        </div>
                                                    </div> : null}


                                                    <div className="col-md-12 col-sm-12 pad-0 filterRunOnDemand " >
                                                        <div className="col-md-12 col-xs-12 col-sm-12">
                                                            <div className="filterSketches">
                                                                <div className="formSketches">

                                                                    <div className="sketchesFormDiv verticalTop">
                                                                        <label className="sketchesFormInput">
                                                                            Stock point
                                                                     </label>
                                                                        <select onChange={(e) => this.handleChange(e)} value={this.state.stockPoint} id="stockPoint" className="purchaseSelectBox">

                                                                            <option value="ALL">
                                                                                ALL
                                                                        </option>
                                                                            <option value="GRC">
                                                                                GRC
                                                                        </option>
                                                                            <option value="MSA">
                                                                                MSA
                                                                        </option>

                                                                        </select>
                                                                    </div>

                                                                    <div className="sketchesFormDiv m-top-20">
                                                                        {this.state.stockPoint != "" ? <button className="sktechFilterBth" onClick={() => this.onCityKartApply()}>
                                                                            Apply Filter
                                                                    </button> : <button className="sktechFilterBth btnDisabled" >
                                                                                Apply Filter
                                                                    </button>}
                                                                        {this.state.appliedStockPoint != "" ? <p>Applied Stock Point :{this.state.appliedStockPoint}</p> : null}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-top-10">
                                                            <span className="borderFilter"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                : null}

                                            {sessionStorage.getItem('partnerEnterpriseName') == "VMART" && <div>
                                                <div className="col-md-12 col-sm-12 pad-0 filterRunOnDemand " >
                                                    <div className="col-md-12 col-xs-12 col-sm-12">
                                                        {this.state.status == "RUNNING" ? <div className="toolTipMainHover skechersPatch cityKartNoClick">
                                                            <div className="noClickDiv displayBlock height210 width_95">
                                                            </div>
                                                            <div className="topToolTip ">

                                                                <span className="topToolTipText skechersTooltip">
                                                                    {/* You cannot apply filter as the job is already running. */}
                                                                    <img src={lockIcon} />
                                                                    <h3>Temporary Locked</h3>
                                                                    <p>You can’t apply filter as its already running, This feature will
                                                                    be available after job completion</p>
                                                                </span>

                                                            </div>
                                                        </div> : null}
                                                        <div className="filterSketches">
                                                            <div className="formSketches">
                                                                <div className="sketchesFormDiv verticalTop masterFromInput multipleSelection">
                                                                    <div className="fieldWid width100 pad-0">
                                                                        <label>Mother Company</label>
                                                                        <div className="dropdown" onClick={(e) => this.montherCompanyOpenModal(e)}>
                                                                            <div className="inputMaster dropdown-toggle width_90">
                                                                                <span data-toggle="dropdown">{this.state.motherCompany == "" ? "Select Mother Company" : this.state.motherCompany.join(',')}
                                                                                </span>
                                                                            </div>
                                                                            <img src={arrowIcon} className="selectArrow" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="sketchesFormDiv verticalTop masterFromInput multipleSelection">
                                                                    <div className="fieldWid width100 pad-0">
                                                                        <label>Article</label>
                                                                        <div className="dropdown" onClick={(e) => this.ArticleModalOpen(e)} >
                                                                            <div className="inputMaster dropdown-toggle width_90">
                                                                                <span data-toggle="dropdown">{this.state.articleUpdatedData == "" ? "Select Article" : this.state.articleUpdatedData}
                                                                                </span>
                                                                            </div>
                                                                            <img src={arrowIcon} className="selectArrow" />

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="sketchesFormDiv verticalTop masterFromInput multipleSelection">
                                                                    <div className="fieldWid width100 pad-0">
                                                                        <label>Vendor</label>
                                                                        <div className="dropdown" onClick={(e) => this.vendorModalOpen(e)}>
                                                                            <div className="inputMaster dropdown-toggle width_90">
                                                                                <span data-toggle="dropdown">{this.state.vendorName == "" ? "Select Vendor" : this.state.vendorName}
                                                                                </span>
                                                                            </div>
                                                                            <img src={arrowIcon} className="selectArrow" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="sketchesFormDiv verticalTop masterFromInput multipleSelection mar-top-15">
                                                                    <div className="fieldWid width100 pad-0">
                                                                        <label>Choose Store</label>
                                                                        <div className="dropdown" onClick={(e) => this.storeModalOpen(e)}>
                                                                            <div className="inputMaster dropdown-toggle width_90">
                                                                                <span data-toggle="dropdown">{this.state.storeUpdatesName == "" ? "Choose Stores" : this.state.storeUpdatesName}
                                                                                </span>
                                                                            </div>
                                                                            <img src={arrowIcon} className="selectArrow" />
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                {/* <div className="sketchesFormDiv m-top-20 masterFromInput">
                                                                    <label className="sketchesFormInput" >
                                                                        Choose Stores
                                                            <span> ( you can choose multiple stores at a time )</span>
                                                                    </label>
                                                                    <div className="runOndemandDrop">
                                                                        <div className="dropdown">
                                                                            <div className="inputMaster dropdown-toggle width_90">
                                                                                <span>
                                                                                    <label className="SketchBox posInherit m0">{this.state.selected.length} Stores selected</label>
                                                                                </span>
                                                                            </div>
                                                                            <img src={arrowIcon} className="selectArrow" />
                                                                        </div>
                                                                    </div>

                                                                    <div className="dropdownSketchFilter skechersDrop dropDownSkech">
                                                                        <div className="SketchFilter">
                                                                            <p>Choose Stores from below list :</p>
                                                                            <input type="text" placeholder="Type to Search…" value={this.state.search} onChange={(e) => this.Change(e)} />
                                                                        </div>
                                                                        <div className="sketchStoreDrop">
                                                                            <div className="lftStoreDiv m-rgt-20">
                                                                                <h2>Choose Stores</h2>
                                                                                <div className="storeChoose">
                                                                                    <ul className="list-inline">
                                                                                        {result.length == 0 ? null : result.map((data, i) => <li key={i} onClick={(e) => this.onnChange(`${data.storeCode}`)} >
                                                                                            <span className="onHover"></span>
                                                                                            <p className="contentStore pad-5">{data.storeName + "-" + data.storeCode}</p>
                                                                                        </li>)}
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                            <div className="lftStoreDiv">
                                                                                <h2>Selected Stores</h2>
                                                                                <div className="storeChoose pad-10">
                                                                                    <ul className="list-inline">
                                                                                        <li>
                                                                                            {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.storeCode} className="">
                                                                                                <p className="contentStore">{data.storeName}  </p>

                                                                                                <span onClick={(e) => this.onDelete(data.storeCode)} value={data.storeCode} className="close_btn_demand" aria-hidden="true">&times;</span>
                                                                                            </div>)}
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                </div> */}

                                                                <div className="sketchesFormDiv m-top-40 filterBtn mar-top-15" >
                                                                    {this.state.filter ? <button className="sktechFilterBth mar-top-15" onClick={(e) => this.applyFiltermrt(e)}>
                                                                        Apply Filter
                                                                        </button> : null}

                                                                    {this.state.remove ? <button className="sktechFilterBthClear mar-top-15" onClick={(e) => this.clearFiltermrt(e)}>
                                                                        Remove Filter
                                                                    </button> : null}
                                                                    {/* {this.state.motherCompany == "" && this.state.articleUpdatedData == "" && this.state.vendorName == "" && this.state.selected.length == 0 ?
                                                                        <div> <button className="sktechFilterBthClear textDisable" >
                                                                            Clear Filter
                                                                        </button> </div>

                                                                        : <div><button className="sktechFilterBthClear" onClick={(e) => this.clearFiltermrt(e)}>
                                                                            Clear Filter
                                                                        </button></div>} */}
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                                            <span className="borderFilter"></span>
                                                        </div>
                                                    </div>
                                                </div> </div>}

                                            {/* {sessionStorage.getItem('partnerEnterpriseName') == "Nysaa Retail Pvt Ltd" ?
                                                <div className="col-md-7">
                                                    <div className="col-md-6 pad-lft-0">
                                                        <label className="displayBlock">From</label>
                                                        <input type="date" className="purchaseOrderTextbox organistionFilterModal" placeholder="Select date from" />
                                                    </div>
                                                    <div className="col-md-6 pad-lft-0">
                                                        <label className="displayBlock">To</label>
                                                        <input type="date" className="purchaseOrderTextbox organistionFilterModal" placeholder="Select to from" />
                                                    </div>
                                                    <div className="sketchesFormDiv m-top-20">
                                                        <button className="sktechFilterBth"> Apply Filter</button>
                                                    </div>
                                                </div> : null} */}


                                            <div className="col-md-12 col-xs-12 m-top-50">
                                                <div className="col-md-6 col-xs-12 ">
                                                    <div className="detailDemand">
                                                        <h2>Current Job Details</h2>
                                                    </div>
                                                    <div className="numberDetail col-md-10 col-md-offset-1">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="86" height="85" viewBox="0 0 86 85">
                                                            <g fill="none" fillRule="evenodd" transform="translate(.5)">
                                                                <circle cx="42.5" cy="42.5" r="42.5" fill="#ECECF6" fillRule="nonzero" />
                                                                <g fill="#6D6DC9">
                                                                    <path fillRule="nonzero" d="M60 27.872c0-2.498-2.052-4.524-4.583-4.524H31.583c-2.531 0-4.583 2.026-4.583 4.524v28.256c0 2.498 2.052 4.524 4.583 4.524h23.834c2.531 0 4.583-2.026 4.583-4.524V27.872zm-1.833 28.256c0 1.499-1.232 2.714-2.75 2.714H31.583c-1.518 0-2.75-1.215-2.75-2.714V27.872c0-1.499 1.232-2.714 2.75-2.714h23.834c1.518 0 2.75 1.215 2.75 2.714v28.256z" />
                                                                    <path d="M34.952 41.321h-1.81a.91.91 0 0 0-.917.905c0 .5.41.905.917.905h1.81a.91.91 0 0 0 .917-.905.91.91 0 0 0-.917-.905zM53.858 41.321H39.05a.91.91 0 0 0-.917.905c0 .5.41.905.917.905h14.808a.91.91 0 0 0 .917-.905.91.91 0 0 0-.917-.905zM34.952 33.179h-1.81a.91.91 0 0 0-.917.904c0 .5.41.905.917.905h1.81a.91.91 0 0 0 .917-.905.91.91 0 0 0-.917-.904zM53.858 33.179H39.05a.91.91 0 0 0-.917.904c0 .5.41.905.917.905h14.808a.91.91 0 0 0 .917-.905.91.91 0 0 0-.917-.904zM34.952 49.464h-1.81a.91.91 0 0 0-.917.905c0 .5.41.905.917.905h1.81a.91.91 0 0 0 .917-.905.91.91 0 0 0-.917-.905zM53.858 49.464H39.05a.91.91 0 0 0-.917.905c0 .5.41.905.917.905h14.808a.91.91 0 0 0 .917-.905.91.91 0 0 0-.917-.905z" />
                                                                </g>
                                                            </g>
                                                        </svg>
                                                        <div className="listDemand">
                                                            <ul className="list-inline floatNone">
                                                                <li>
                                                                    <h5>State</h5>
                                                                    <span>{this.state.runState}</span>
                                                                </li>
                                                                <li>
                                                                    <h5>Date & Time</h5>
                                                                    <span>{this.state.lastDate}</span>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        {/* <img src={runDemandSvg} className="demandArrow" /> */}

                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-xs-12 ">
                                                    <div className="detailDemand">
                                                        <h2>Last Engine Run Details</h2>
                                                        {/* <p>
                                                            Lorem ipsum doler immet , lorem ipsum bua
                                                                immet noi sora quese
                                                        </p> */}
                                                    </div>
                                                    <div className="numberDetail runDemandSuccess col-md-10 col-md-offset-1">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="86" height="85" viewBox="0 0 86 85">
                                                            <g fill="none" fillRule="evenodd" transform="translate(.5)">
                                                                <circle cx="42.5" cy="42.5" r="42.5" fill="#ECECF6" fillRule="nonzero" />
                                                                <g fill="#6D6DC9">
                                                                    <path d="M43 61c-10.476 0-19-8.524-19-19s8.524-19 19-19a1.188 1.188 0 0 1 0 2.375c-9.168 0-16.625 7.457-16.625 16.625S33.832 58.625 43 58.625 59.625 51.168 59.625 42A1.187 1.187 0 1 1 62 42c0 10.476-8.524 19-19 19z" />
                                                                    <path d="M43 25.375c-.309 0-.618-.13-.843-.344a1.249 1.249 0 0 1-.344-.843c0-.31.13-.618.344-.844.451-.439 1.235-.439 1.686 0 .214.226.344.535.344.843 0 .31-.13.618-.344.844a1.249 1.249 0 0 1-.843.344z" />
                                                                    <path fillRule="nonzero" d="M59.055 37.69a1.19 1.19 0 0 1 .843-1.45 1.186 1.186 0 0 1 1.46.832 1.202 1.202 0 0 1-.842 1.46 1.37 1.37 0 0 1-.31.036 1.19 1.19 0 0 1-1.151-.879zm-1.662-4.002h.011a1.186 1.186 0 1 1 2.054-1.188c.321.558.13 1.294-.439 1.615-.19.107-.391.166-.593.166-.405 0-.808-.213-1.033-.593zm-2.638-3.444a1.183 1.183 0 0 1 0-1.675 1.183 1.183 0 0 1 1.674 0h.012a1.192 1.192 0 0 1-.01 1.675 1.138 1.138 0 0 1-.832.344c-.308 0-.617-.107-.844-.344zm-3.443-2.648a1.17 1.17 0 0 1-.427-1.615c.32-.57 1.045-.76 1.615-.44a1.2 1.2 0 0 1 .44 1.627c-.226.38-.62.594-1.034.594-.202 0-.404-.06-.594-.166zm-4.001-1.651a1.208 1.208 0 0 1-.844-1.46 1.19 1.19 0 0 1 1.45-.844h.011a1.19 1.19 0 1 1-.309 2.34c-.107 0-.213-.012-.308-.036z" />
                                                                    <path d="M60.813 43.188c-.322 0-.62-.131-.844-.345a1.243 1.243 0 0 1-.344-.843c0-.309.13-.617.344-.843.427-.44 1.235-.44 1.687 0 .213.226.344.534.344.843 0 .309-.13.617-.344.843a1.249 1.249 0 0 1-.843.344zM50.124 47.938c-.226 0-.455-.066-.658-.2l-7.125-4.75a1.189 1.189 0 0 1-.529-.988V30.125a1.188 1.188 0 0 1 2.376 0v11.24l6.596 4.397a1.187 1.187 0 0 1-.66 2.175z" />
                                                                </g>
                                                            </g>
                                                        </svg>
                                                        <div className="listDemand borderTop">
                                                            <ul className="list-inline">
                                                                <li>
                                                                    <h5>Last Engine Run</h5>
                                                                    {this.state.smallLoader1 ? <SectionLoader /> : <div><span className="fontWeig600">{this.state.lastRun}</span>
                                                                        <span className={this.state.jobRunState == "SUCCEEDED" ? "runStateDemand m-top-10" : "runDemandStop m-top-10"}>{this.state.jobRunState == "NA" ? null : this.state.jobRunState}</span>
                                                                        {this.state.jobRunState == "NA" ? null : <button type="button" onClick={() => this.props.history.push('/inventoryPlanning/summary')} className="launchBtn btnHover" >Summary</button>}</div>}
                                                                </li>
                                                                <li>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                                                                        <defs>
                                                                            <radialGradient id="a" cx="0%" cy="0%" r="393.125%" fx="0%" fy="0%" gradientTransform="scale(1 .97819) rotate(45.181)">
                                                                                <stop offset="0%" stopColor="#3B3B98" />
                                                                                <stop offset="7.548%" stopColor="#3D3C99" />
                                                                                <stop offset="100%" stopColor="#C86DD7" />
                                                                            </radialGradient>
                                                                            <radialGradient id="b" cx="0%" cy="0%" r="433.002%" fx="0%" fy="0%" gradientTransform="scale(1 .81983) rotate(50.212)">
                                                                                <stop offset="0%" stopColor="#3B3B98" />
                                                                                <stop offset="7.548%" stopColor="#3D3C99" />
                                                                                <stop offset="100%" stopColor="#C86DD7" />
                                                                            </radialGradient>
                                                                        </defs>
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="url(#a)" fillRule="nonzero" d="M2.518 26.413h10.74c1.658 2.056 4.118 3.261 6.723 3.294a8.631 8.631 0 0 0 6.184-2.594 9.04 9.04 0 0 0 2.58-6.309c-.049-2.974-1.496-5.742-3.888-7.434V5.054c0-1.434-1.243-2.445-2.645-2.445h-2.135V.848a.463.463 0 0 0-.125-.394.44.44 0 0 0-.385-.128H16.54c-.351 0-.606.163-.606.522v1.76h-7.01V.849c0-.359-.415-.522-.766-.522H5.131c-.35 0-.67.163-.67.522v1.76H2.519C1.115 2.609 0 3.62 0 5.055v18.913c0 1.435 1.115 2.446 2.518 2.446zm24.92-5.543c-.017 4.21-3.364 7.61-7.478 7.597-4.114-.012-7.442-3.431-7.436-7.64.006-4.21 3.343-7.62 7.457-7.62 1.984 0 3.885.808 5.285 2.246a7.722 7.722 0 0 1 2.172 5.417zm-7.393-8.968a8.598 8.598 0 0 0-6.182 2.573 9.006 9.006 0 0 0-2.582 6.297 8.698 8.698 0 0 0 1.116 4.337h-9.88c-.7 0-1.242-.424-1.242-1.142V10.435h22.307v2.25a9.376 9.376 0 0 0-3.537-.783zM17.21 1.63h1.593v2.935H17.21V1.63zm-11.473 0h1.912v2.935H5.736V1.63zM2.518 3.913h1.944v1.24a.743.743 0 0 0 .669.717h3.027a.836.836 0 0 0 .765-.718V3.913h7.011v1.24a.68.68 0 0 0 .606.717h3.027c.35 0 .51-.36.51-.718V3.913h2.135c.701 0 1.37.424 1.37 1.141V9.13H1.275V5.054c0-.717.541-1.141 1.243-1.141z" transform="translate(.5)" />
                                                                            <path fill="url(#b)" d="M18.802 16.957v4.565a.77.77 0 0 0 .765.652h3.41a.645.645 0 0 0 .637-.652.645.645 0 0 0-.637-.652h-2.9v-3.913a.645.645 0 0 0-.637-.653.645.645 0 0 0-.638.653z" transform="translate(.5)" />
                                                                        </g>
                                                                    </svg>

                                                                </li>
                                                                <li>
                                                                    <h5>Next scheduled at</h5>
                                                                    {sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? <span>{this.state.nextScheduled}</span> : this.state.smallLoader2 ? <SectionLoader /> : <span>{this.state.scheduled}</span>}

                                                                </li>
                                                            </ul>
                                                        </div>
                                                        {/* <button className="demandSchduleBtn">
                                                        Schedule Now
                                                        </button> */}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                    {this.state.succeeded ? <Succeeded modalState={this.state.modalState} closeSucceeded={(e) => this.closeSucceeded(e)} /> : null}
                    {this.state.modalMotherComapny ? <FmcgFilterModal {...this.state} {...this.props} updateMotherComp={(e) => this.updateMotherComp(e)} closeMotherComp={(e) => this.closeMotherComp(e)} /> : null}
                    {this.state.articleModal ? <ArticleFilterModal {...this.state} {...this.props} updateArticle={(e) => this.updateArticle(e)} closeArticleModal={(e) => this.closeArticleModal(e)} /> : null}
                    {this.state.vendorModal ? <VendorFilterModal {...this.state} {...this.props} updateVendor={(e) => this.updateVendor(e)} closeVendorModal={(e) => this.closeVendorModal(e)} /> : null}
                    {this.state.storeModal ? <StoreModal {...this.state} {...this.props} closeStoreModal={(e) => this.closeStoreModal(e)} updateStore={(e) => this.updateStore(e)} /> : null}
                    {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
                </div >
                <Footer />
            </div>
        );

    }

}

export default ReplenishmentRunOnDemand;
