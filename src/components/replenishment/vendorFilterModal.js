import React, { Component } from 'react'
import ToastLoader from '../loaders/toastLoader';
import closeImg from '../../assets/close-recently.svg'
import searchImg from '../../assets/search.svg'

export default class VendorFilterModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            vendorData: [],
            selectedValue: this.props.vendorCode,
            searchVendor: "",
            toastLoader: false,
            toastMsg: "",
            type: 1,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
        }
    }

    // ______________________________  RECEIVE PROPS ___________________________

    componentWillReceiveProps(nextProps) {
        if (nextProps.replenishment.getVendorFmcg.isSuccess) {
            if (nextProps.replenishment.getVendorFmcg.data.resource != null) {
                this.setState({
                    vendorData: nextProps.replenishment.getVendorFmcg.data.resource,
                    prev: nextProps.replenishment.getVendorFmcg.data.prePage,
                    current: nextProps.replenishment.getVendorFmcg.data.currPage,
                    next: nextProps.replenishment.getVendorFmcg.data.currPage + 1,
                    maxPage: nextProps.replenishment.getVendorFmcg.data.maxPage,
                })
            } else {
                this.setState({
                    vendorData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.replenishment.getVendorFmcg.data.prePage,
                current: this.props.replenishment.getVendorFmcg.data.currPage,
                next: this.props.replenishment.getVendorFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getVendorFmcg.data.maxPage,
            })
            if (this.props.replenishment.getVendorFmcg.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.getVendorFmcg.data.currPage - 1,
                    search: this.state.searchVendor,
                    motherComp: this.props.motherCompany.join('|'),
                    articleCode: this.props.articleCode.join('|')
                }
                this.props.getVendorFmcgRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.replenishment.getVendorFmcg.data.prePage,
                current: this.props.replenishment.getVendorFmcg.data.currPage,
                next: this.props.replenishment.getVendorFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getVendorFmcg.data.maxPage,
            })
            if (this.props.replenishment.getVendorFmcg.data.currPage != this.props.replenishment.getVendorFmcg.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.replenishment.getVendorFmcg.data.currPage + 1,
                    search: this.state.searchVendor,
                    motherComp: this.props.motherCompany.join('|'),
                    articleCode: this.props.articleCode.join('|')
                }
                this.props.getVendorFmcgRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.replenishment.getVendorFmcg.data.prePage,
                current: this.props.replenishment.getVendorFmcg.data.currPage,
                next: this.props.replenishment.getVendorFmcg.data.currPage + 1,
                maxPage: this.props.replenishment.getVendorFmcg.data.maxPage,
            })
            if (this.props.replenishment.getVendorFmcg.data.currPage <= this.props.replenishment.getVendorFmcg.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.searchVendor,
                    motherComp: this.props.motherCompany.join('|'),
                    articleCode: this.props.articleCode.join('|')
                }
                this.props.getVendorFmcgRequest(data)
            }
        }
    }

    handleChange(e) {
        if (e.target.id == "searchVendor") {
            this.setState({
                searchVendor: e.target.value
            })
        }
    }
    searchVendor() {
        if (this.state.searchVendor == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
        } else {
            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.searchVendor,
                motherComp: this.props.motherCompany.join('|'),
                articleCode: this.props.articleCode.join('|')
            }
            this.props.getVendorFmcgRequest(data)
        }
    }
    _handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.searchVendor()
        }
    }
    onClearSearch() {
        if (this.state.type == 3) {
            this.setState({
                searchVendor: "",
                type: 1,
                no: 1
            })
            var data = {
                type: 1,
                no: 1,
                search: "",
                motherComp: this.props.motherCompany.join('|'),
                articleCode: this.props.articleCode.join('|')
            }
            this.props.getVendorFmcgRequest(data)
        } else {
            this.setState({
                searchVendor: "",
            })
        }

    }

    selectedValue(code) {
        let vendorData = this.state.vendorData
        let selectedValue = [...this.state.selectedValue]
        for (let i = 0; i < vendorData.length; i++) {
            if (vendorData[i].vendorCode == code) {
                if (selectedValue.includes(vendorData[i].vendorCode)) {
                    var index = selectedValue.indexOf(vendorData[i].vendorCode);
                    if (index > -1) {
                        selectedValue.splice(index, 1)
                        this.setState({
                            selectedValue: selectedValue
                        })
                    }
                } else {
                    selectedValue.push(vendorData[i].vendorCode)
                    this.setState({
                        selectedValue: selectedValue
                    })
                }
            } else {

            }
        }

    }

    onSaveVendor() {
        if (this.state.selectedValue == "") {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            let vendorData = this.state.vendorData
            for (let i = 0; i < vendorData.length; i++) {
                if (this.state.selectedValue.includes(vendorData[i].vendorCode)) {
                    let data = {
                        Name: vendorData[i].vendorName,
                        Code: vendorData[i].vendorCode
                    }
                    this.props.updateVendor(data)
                }
            }
        }
    }

    selectall(e) {
        let vendorData = this.state.vendorData
        let selectedValue = [...this.state.selectedValue]
        for (let i = 0; i < vendorData.length; i++) {
            if (!selectedValue.includes(vendorData[i].vendorCode)) {
                selectedValue.push(vendorData[i].vendorCode)
            }
        }
        this.setState({
            selectedValue: selectedValue,
        })
        document.getElementById("selectAll").checked = true
    }
    deselectall(e) {
        this.setState({
            selectedValue: []
        })
        document.getElementById("deselectAll").checked = true
    }

    closeVendorModal() {
        this.setState({
            selectedValue: []
        })
        this.props.closeVendorModal()
    }
    render() {

        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content modalShow pad-0">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle">
                                    <div className="col-md-6 pad-0">
                                        <h2>Vendor</h2>
                                    </div>
                                    <div className="col-md-6 pad-0">
                                        <div className="modalHandlers">
                                            <button type="button" className="" onClick={(e) => this.onSaveVendor(e)}>Done</button>

                                            <button type="button" onClick={(e) => this.closeVendorModal(e)}>Close</button>
                                        </div>
                                    </div>
                                </div>

                                <div className="modalSearch">
                                    <div className="col-md-6"></div>
                                    <div className="col-md-6">
                                        <div className="dropdown searchStoreProfileMain">
                                            {/* <form > */}
                                            <div className="searchBtnClick">
                                                <input type="search" className="search-box" onKeyDown={this._handleKeyDown} onChange={e => this.handleChange(e)} value={this.state.searchVendor} id="searchVendor" placeholder="Type to search" />
                                                <button className="m0" onClick={(e) => this.searchVendor(e)} ><img src={searchImg} /></button>
                                                {this.state.searchVendor == "" ? null : <img src={closeImg} className="clearImg" onClick={(e) => this.onClearSearch(e)} />}
                                            </div>
                                            {/* {this.state.type == 3 ? <span onClick={(e) => this.onClearSearch(e)} className="clearSearchFilter">
                                                    Clear Search Filter
                                                </span> : null} */}
                                            {/* </form> */}
                                        </div>
                                    </div>
                                </div>
                                <div className="modalContentMid">
                                    <div className="col-md-12 col-sm-12 pad-0 modalTableNew">
                                        <div className="modal_table fmcgFilterTable tableHeadFixed">
                                            <table className="table tableModal">
                                                <thead>
                                                    <tr>
                                                        <th className="zIndex2"><label>Select</label></th>
                                                        <th className="zIndex2"><label>Vendor Name</label></th>
                                                        <th className="zIndex2"><label>Vendor Code</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.vendorData.length != 0 ? this.state.vendorData.map((data, key) => (
                                                        <tr key={key}>
                                                            <td className="checkBoxSquare" >
                                                                <div className="checkBoxRowClick">

                                                                    <label className="checkBoxLabel0 displayPointer"><input type="checkBox" id={data.vendorCode} checked={this.state.selectedValue.includes(`${data.vendorCode}`)} onChange={(e) => this.selectedValue(`${data.vendorCode}`)} className="checkBoxTab" /> <span className="checkmark1"></span> </label>
                                                                </div>
                                                            </td>
                                                            <td className="fontWeig600">{data.vendorName}</td>
                                                            <td>{data.vendorCode}</td>
                                                        </tr>
                                                    )) : <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr>}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline m-top-10 modal-select">
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">SELECT ALL
                                            <input type="radio" name="colorRadio" id="selectAll" onKeyDown={(e) => this.selectallKeyDown(e)} onClick={(e) => this.selectall(e)} />
                                                <span className="checkradio-select select_all"></span>
                                            </label>
                                        </li>
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">DESELECT ALL
                                            <input type="radio" name="colorRadio" id="deselectAll" onKeyDown={(e) => this.deselectallKeyDown(e)} onClick={(e) => this.deselectall(e)} />
                                                <span className="checkradio-select deselect_all"></span>
                                            </label>
                                        </li>
                                        <li className="float_right">

                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                                                    </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                                                    </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li>}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader {...this.state} {...this.props} /> : null}
            </div>
        )
    }
}
