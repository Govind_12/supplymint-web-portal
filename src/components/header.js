import React from "react";
import { Link } from "react-router-dom";
import Notification from "./notification";
import bugShape from "../assets/bug-2.svg";
import activeRole from "../assets/greencheck.png";
import DownArrow from "../assets/down-arrow-new.svg";
import VendorLogo from "../assets/vendorLogo.svg";
import EnterpriseLogo from "../assets/enterpriseLogo.svg";
import moment from "moment";
import { AUTH_CONFIG } from "../authConfig/index";
import axios from 'axios';
import { setToken } from '../helper';
import GlobalSearch from "./globalSearch";
import CacheChecker from "./cacheChecker";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notification: 1,
      notifyShow: false,
      loginTime: [],
      profileDrop: false,
      showGlobalSearch: false
    };
    this.profileDrop = this.profileDrop.bind(this);
    this.closeProfileDrop = this.closeProfileDrop.bind(this);
  }
  openGlobalSearch  = () =>  {
    this.setState({
      showGlobalSearch: !this.state.showGlobalSearch});
  }
  CloseGlobalSearch = (e) => {
    this.setState({
      showGlobalSearch: false});
  }

  componentDidMount() {
    document.addEventListener("keydown", this.escFun, false);
    document.addEventListener("click", this.escFun, false);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.escFun, false);
    document.removeEventListener("click", this.escFun, false);
  }

  escFun = (e) =>{  
    if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
      this.setState({ showGlobalSearch: false, notifyShow: false, })
    }
  }

  profileDrop(event) {
    event.preventDefault();
    // this.setState({ profileDrop: true }, () => {
    //   document.addEventListener('click', this.closeProfileDrop);
    // });
  }

  closeProfileDrop() {
    this.setState({ profileDrop: false }, () => {
      document.removeEventListener('click', this.closeProfileDrop);
    });
  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }
  onShowHide = () => {
    // this.closingAllModal();
    this.setState({ notifyShow: !this.state.notifyShow 
    })
  }
  CloseNotification = (e) => {
    this.setState({ notifyShow: false 
    })
  }

  resetNotify() {
    this.setState({
      notification: 0
    })
  }
  onLogout(e) {
    e.preventDefault();
    this.props.dashboardTilesRequest();
    this.props.storesArticlesRequest();
    this.props.salesTrendGraphRequest();
    this.props.slowFastArticleRequest();
    let payload = {
      userName: sessionStorage.getItem('userName'),
      token: sessionStorage.getItem('token'),
      type: "LOGGEDOUT",
      emailId: sessionStorage.getItem('email'),
      keepMeSignIn: "false",
    }
    this.props.userSessionCreateRequest(payload);
    this.props.userSessionCreateRequest();
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('partnerEnterpriseId');
    sessionStorage.removeItem('partnerEnterpriseName');
    sessionStorage.removeItem('firstName');
    sessionStorage.removeItem('lastName');
    sessionStorage.removeItem('roles');
    sessionStorage.removeItem('storeCode');
    document.getElementById("chatIconContainer").style.display='none';
    sessionStorage.clear();
    this.props.history.push('/');
  }
  // ________________________________SWITCHORGANIZATION_________________________________

  switchOrganisation(data1) {
    console.log("came inside the function")
    // sessionStorage.removeItem('token');
    // sessionStorage.removeItem('partnerEnterpriseId');
    // sessionStorage.removeItem('partnerEnterpriseName');
    // sessionStorage.removeItem('firstName');
    // sessionStorage.removeItem('lastName');
    // sessionStorage.removeItem('roles');
    // sessionStorage.removeItem('storeCode');
    // sessionStorage.clear();
    // let session = JSON.parse(sessionStorage.getItem('orgId-name'));
    // session.map((data) => {
    //   if (data.active == "TRUE") {
    //     data.active = "FALSE"
    //   } else {
    //     data.active = "TRUE"
    //   }
    //   return data;
    // })
    // sessionStorage.setItem('orgId-name', JSON.stringify(session))

    let data = {
      // username: sessionStorage.getItem('userName'),
      entId: data1.entId,
      orgId: data1.orgId,
      token: sessionStorage.getItem('token'),

    }
    this.props.switchOrganisationRequest(data)
    //this.props.dashboardTilesRequest('data');
    //this.props.storesArticlesRequest('data');
    //this.props.salesTrendGraphRequest('currentmonth');
    //this.props.slowFastArticleRequest('data');

    this.props.history.push('/home');
  }
  vendorLogout(e) {
    let payload = {
      // userName: sessionStorage.getItem('userName'),
      token: sessionStorage.getItem('token'),
      type: "LOGGEDOUT",
      emailId: sessionStorage.getItem('email'),

      keepMeSignIn: "false",
    }
    this.props.userSessionCreateRequest(payload);
    this.props.userSessionCreateRequest();
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('partnerEnterpriseId');
    sessionStorage.removeItem('partnerEnterpriseName');
    sessionStorage.removeItem('firstName');
    sessionStorage.removeItem('lastName');
    sessionStorage.removeItem('roles');
    sessionStorage.removeItem('storeCode');
    sessionStorage.clear();
    this.props.history.push('/');
  }
  switchRole(id) {
    let modulee = JSON.parse(sessionStorage.getItem('modules'));
    let midd = JSON.parse(sessionStorage.getItem('mid'));
    let dataa = modulee[midd]
    for (let i = 0; i < dataa.length; i++) {
      sessionStorage.removeItem(`${dataa[i].name}`);
    }

    sessionStorage.removeItem('mid');
    sessionStorage.setItem('mid', JSON.stringify(id));
    let mid = JSON.parse(sessionStorage.getItem('mid'));
    let module = JSON.parse(sessionStorage.getItem('modules'));
    let data = module[mid];
    for (let i = 0; i < data.length; i++) {
      sessionStorage.setItem(`${data[i].name}`, JSON.stringify(data[i]));
    }
    this.props.history.push('/home');
  }
  switchDropDown() {
    var cls = document.getElementById("openSwitchRole").classList.value;
    if (cls !== null) {
      document.getElementById("openSwitchRole").classList.add("swichIn")
      var tbody = document.getElementById('app');
      tbody.addEventListener('click', function () {
        dropdown.classList.remove('swichIn');
      })
      var dropdown = document.querySelector('#openSwitchRole')
      dropdown.addEventListener('mouseleave', function () {
        dropdown.classList.remove('swichIn');
      })
    }
  }

  closeSwitchDrop() {
    var cls = document.getElementById("openSwitchRole").classList.value;
    if (cls !== null) {
      document.getElementById("openSwitchRole").classList.remove("swichIn")

    }
  }
  switchEnterprise(data) {
    console.log("came inside the function Vendor")

    let payload = {
      entId: data.eid,
      orgId: data.oid,
      token: sessionStorage.getItem('token'),
      // username: sessionStorage.getItem('userName'),
      // uType: "VENDOR"

    }
    this.props.switchEntRequest(payload)
    this.props.history.push('/home')

  }


  handleProfileDrop = () => {
    event.preventDefault();
    // this.setState({ profileDrop: !this.state.profileDrop })
    this.setState({ profileDrop: true }, () => {
      document.addEventListener('click', this.closeProfileDrop);
    });
  }
  render() {
    var activeRetialer = sessionStorage.getItem('enterprises') !== null && JSON.parse(sessionStorage.getItem('enterprises')).filter((_) => _.active === 1);
    var Change_Setting = JSON.parse(sessionStorage.getItem('Change Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Change Setting')).crud);
    let miid = JSON.parse(sessionStorage.getItem('mid'));
    let role = "";
    let data = JSON.parse(sessionStorage.getItem('roles'));
    if (data != null) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].id == miid) {
          role = data[i].name;
        }
      }
    }
    const { notification } = this.state;
    return (
      
      <div className="header-outer">
        <CacheChecker />
      <header className="header header2">
        <div className="col-md-12 col-sm-12 pad-0">
          <div className="col-md-3 col-xs-6 col-sm-6 m-top-10">
            <ul className="list-inline">
              <li ><Link to="/home">
                {/* <img src={network} className="logo_img m-lft-10" /> */}
                {(sessionStorage.getItem('uType') === "VENDOR") ?
                  <img className="logo_img new-logo-img" src={VendorLogo} />
                  :
                  // <svg className="logo_img" xmlns="http://www.w3.org/2000/svg" width="145" height="42" viewBox="0 0 195 42">
                  //   <g fill="none" fillRule="evenodd">
                  //     <path fill="#000" d="M56.675 29.65c-1.3 0-2.546-.208-3.737-.625-1.192-.417-2.113-.967-2.763-1.65l.725-1.425c.633.633 1.475 1.146 2.525 1.538a9.226 9.226 0 0 0 3.25.587c1.567 0 2.742-.287 3.525-.863.783-.575 1.175-1.32 1.175-2.237 0-.7-.212-1.258-.638-1.675a4.304 4.304 0 0 0-1.562-.963c-.617-.225-1.475-.47-2.575-.737-1.317-.333-2.367-.654-3.15-.963a5.172 5.172 0 0 1-2.013-1.412c-.558-.633-.837-1.492-.837-2.575 0-.883.233-1.687.7-2.412.467-.726 1.183-1.305 2.15-1.738.967-.433 2.167-.65 3.6-.65 1 0 1.98.137 2.938.412.958.276 1.787.655 2.487 1.138l-.625 1.475a8.602 8.602 0 0 0-2.35-1.088 8.771 8.771 0 0 0-2.45-.362c-1.533 0-2.687.296-3.462.887-.776.592-1.163 1.355-1.163 2.288 0 .7.212 1.262.638 1.688.425.425.958.75 1.6.974.641.226 1.504.471 2.587.738 1.283.317 2.32.63 3.112.938a5.109 5.109 0 0 1 2.013 1.4c.55.625.825 1.47.825 2.537 0 .883-.237 1.687-.713 2.413-.475.725-1.204 1.3-2.187 1.724-.983.426-2.192.638-3.625.638zM78.7 16.35V29.5H77v-2.4a4.868 4.868 0 0 1-1.925 1.862c-.817.442-1.75.663-2.8.663-1.717 0-3.07-.48-4.063-1.438-.991-.958-1.487-2.362-1.487-4.212V16.35H68.5v7.45c0 1.383.342 2.433 1.025 3.15.683.717 1.658 1.075 2.925 1.075 1.383 0 2.475-.42 3.275-1.262.8-.842 1.2-2.013 1.2-3.513v-6.9H78.7zm11.75-.1c1.233 0 2.35.28 3.35.837a5.989 5.989 0 0 1 2.35 2.363c.567 1.017.85 2.175.85 3.475 0 1.317-.283 2.483-.85 3.5a6.013 6.013 0 0 1-2.338 2.363c-.991.558-2.112.837-3.362.837-1.067 0-2.03-.22-2.888-.663a5.472 5.472 0 0 1-2.112-1.937v7.325h-1.775v-18h1.7v2.6a5.468 5.468 0 0 1 2.112-2c.876-.467 1.863-.7 2.963-.7zm-.125 11.8c.917 0 1.75-.212 2.5-.637a4.503 4.503 0 0 0 1.763-1.813c.425-.783.637-1.675.637-2.675 0-1-.212-1.887-.637-2.663a4.61 4.61 0 0 0-1.763-1.812c-.75-.433-1.583-.65-2.5-.65-.933 0-1.77.217-2.513.65a4.636 4.636 0 0 0-1.75 1.813c-.425.775-.637 1.662-.637 2.662s.212 1.892.638 2.675a4.527 4.527 0 0 0 1.75 1.813c.741.425 1.579.637 2.512.637zm17.075-11.8c1.233 0 2.35.28 3.35.837a5.989 5.989 0 0 1 2.35 2.363c.567 1.017.85 2.175.85 3.475 0 1.317-.283 2.483-.85 3.5a6.013 6.013 0 0 1-2.337 2.363c-.992.558-2.113.837-3.363.837-1.067 0-2.03-.22-2.888-.663a5.472 5.472 0 0 1-2.112-1.937v7.325h-1.775v-18h1.7v2.6a5.468 5.468 0 0 1 2.112-2c.876-.467 1.863-.7 2.963-.7zm-.125 11.8c.917 0 1.75-.212 2.5-.637a4.503 4.503 0 0 0 1.762-1.813c.426-.783.638-1.675.638-2.675 0-1-.212-1.887-.638-2.663a4.61 4.61 0 0 0-1.762-1.812c-.75-.433-1.583-.65-2.5-.65-.933 0-1.77.217-2.513.65a4.636 4.636 0 0 0-1.75 1.813c-.425.775-.637 1.662-.637 2.662s.212 1.892.638 2.675a4.527 4.527 0 0 0 1.75 1.813c.741.425 1.579.637 2.512.637zm10.3-17.1h1.775V29.5h-1.775V10.95zm17.775 5.4l-6.575 14.725c-.533 1.233-1.15 2.108-1.85 2.625-.7.517-1.542.775-2.525.775-.633 0-1.225-.1-1.775-.3-.55-.2-1.025-.5-1.425-.9l.825-1.325c.667.667 1.467 1 2.4 1 .6 0 1.112-.167 1.538-.5.425-.333.82-.9 1.187-1.7l.575-1.275-5.875-13.125h1.85l4.95 11.175 4.95-11.175h1.75zm18.9-.1c1.65 0 2.946.475 3.887 1.425.942.95 1.413 2.35 1.413 4.2V29.5h-1.775v-7.45c0-1.367-.33-2.408-.988-3.125-.658-.717-1.587-1.075-2.787-1.075-1.367 0-2.442.42-3.225 1.262-.783.842-1.175 2.005-1.175 3.488v6.9h-1.775v-7.45c0-1.367-.33-2.408-.987-3.125-.659-.717-1.596-1.075-2.813-1.075-1.35 0-2.42.42-3.213 1.262-.791.842-1.187 2.005-1.187 3.488v6.9h-1.775V16.35h1.7v2.4a4.743 4.743 0 0 1 1.95-1.85c.833-.433 1.792-.65 2.875-.65 1.1 0 2.054.233 2.863.7.808.467 1.412 1.158 1.812 2.075.483-.867 1.18-1.546 2.087-2.037.909-.492 1.946-.738 3.113-.738zm10.125.1h1.775V29.5h-1.775V16.35zm.9-2.875c-.367 0-.675-.125-.925-.375s-.375-.55-.375-.9a1.2 1.2 0 0 1 .375-.875c.25-.25.558-.375.925-.375s.675.12.925.363c.25.241.375.529.375.862 0 .367-.125.675-.375.925s-.558.375-.925.375zM177.8 16.25c1.65 0 2.962.48 3.938 1.438.975.958 1.462 2.354 1.462 4.187V29.5h-1.775v-7.45c0-1.367-.342-2.408-1.025-3.125-.683-.717-1.658-1.075-2.925-1.075-1.417 0-2.537.42-3.362 1.262-.826.842-1.238 2.005-1.238 3.488v6.9H171.1V16.35h1.7v2.425a4.938 4.938 0 0 1 2.012-1.862c.859-.442 1.855-.663 2.988-.663zm17.15 12.45c-.333.3-.746.53-1.237.688a4.985 4.985 0 0 1-1.538.237c-1.233 0-2.183-.333-2.85-1-.667-.667-1-1.608-1-2.825v-7.95h-2.35v-1.5h2.35v-2.875h1.775v2.875h4v1.5h-4v7.85c0 .783.196 1.38.588 1.788.391.408.954.612 1.687.612.367 0 .72-.058 1.063-.175.341-.117.637-.283.887-.5l.625 1.275z" />
                  //     <g fillRule="nonzero">
                  //       <rect width="41.5" height="42" fill="#3B3B98" rx="9" />
                  //       <g fill="#FFF" transform="rotate(2 -139.047 175.047)">
                  //         <ellipse cx="15.75" cy="2.662" rx="2.662" ry="2.662" />
                  //         <ellipse cx="15.75" cy="15.526" rx="2.662" ry="2.662" />
                  //         <ellipse cx="2.662" cy="15.305" rx="2.662" ry="2.662" />
                  //         <ellipse cx="7.986" cy="26.838" rx="2.662" ry="2.662" />
                  //         <ellipse cx="23.292" cy="26.838" rx="2.662" ry="2.662" />
                  //         <ellipse cx="28.838" cy="15.305" rx="2.662" ry="2.662" />
                  //         <path d="M14.641 5.102h2.44v8.429h-2.44z" />
                  //         <path d="M13.516 3.147l1.695 1.755L3.563 16.15 1.87 14.395zM14.244 15.309l1.974 1.434-7.17 9.87-1.974-1.435z" />
                  //         <path d="M4.338 15.943l-2.212 1.031 4.406 9.448 2.211-1.03z" />
                  //         <path d="M4.474 14.603l-1.257 2.092 18.497 11.114 1.256-2.092z" />
                  //         <path d="M26.997 14.5l1.257 2.092L9.757 27.706 8.5 25.614z" />
                  //         <path d="M26.94 15.943l2.212 1.03-4.406 9.45-2.212-1.032z" />
                  //       </g>
                  //     </g>
                  //   </g>
                  // </svg>
                  <img className="logo_img enterprise-logo-icon" src={EnterpriseLogo} />}
              </Link>
              </li>
              {/* // className={this.props.show ? "humburger openSideBar pad-lft-30" : "pad-lft-30 closeSideBar"}
              // onClick={e => this.props.showHide(e)} */}
              {/* <li className="humburger"
                id="toggle_sidemenu">
                <svg className="humburgerIcon" xmlns="http://www.w3.org/2000/svg" width="17" height="23" viewBox="0 0 25 23">
                  <g fill="#010101" fillRule="nonzero">
                    <path d="M23.36.04H1.562C.703.04 0 .741 0 1.601c0 .859.703 1.562 1.563 1.562H23.32c.86 0 1.563-.703 1.563-1.562 0-.86-.664-1.563-1.524-1.563zM18.477 9.766H1.562C.704 9.766 0 10.469 0 11.328c0 .86.703 1.563 1.563 1.563h16.914c.859 0 1.562-.704 1.562-1.563 0-.86-.703-1.562-1.562-1.562zM18.477 19.453H1.562c-.859 0-1.562.703-1.562 1.563 0 .859.703 1.562 1.563 1.562h16.914c.859 0 1.562-.703 1.562-1.562 0-.86-.703-1.563-1.562-1.563z" />
                  </g>
                </svg>
              </li> */}
            </ul>
          </div>

          <div className="col-md-9 col-xs-6 col-sm-6 pad-0">
            < ul className="dropdown list-inline float_right">
              {/* <li>
            <input
              className="search_input"
              type="search"
              placeholder="Search"
            />
            <svg className="searchbarHeader" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                <path fill="#C5C5C5" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z"/>
            </svg>
          </li> */}
              {(sessionStorage.getItem('uType') === "VENDOR") ?
                <li className="header-search-vend">
                  <button className="hsv-inner" onClick={(e) => this.openGlobalSearch(e)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 21.142 21.142">
                      <g id="prefix__search_1_" data-name="search (1)" transform="translate(0 -.003)">
                        <g id="prefix__Group_2741" data-name="Group 2741" transform="translate(0 .003)">
                          <path id="prefix__Path_773" fill="#d6dce6" d="M20.884 19.641l-6.012-6.012a8.384 8.384 0 1 0-1.246 1.246l6.012 6.012a.881.881 0 1 0 1.246-1.246zM8.369 14.978a6.607 6.607 0 1 1 6.607-6.607 6.614 6.614 0 0 1-6.607 6.607z" data-name="Path 773" transform="translate(0 -.003)" />
                        </g>
                      </g>
                    </svg>
                  </button>
                </li> :
                <li className="header-search-ent">
                  <button className="hse-inner" onClick={(e) => this.openGlobalSearch(e)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 21.142 21.142">
                      <g id="prefix__search_1_" data-name="search (1)" transform="translate(0 -.003)">
                        <g id="prefix__Group_2741" data-name="Group 2741" transform="translate(0 .003)">
                          <path id="prefix__Path_773" fill="#d6dce6" d="M20.884 19.641l-6.012-6.012a8.384 8.384 0 1 0-1.246 1.246l6.012 6.012a.881.881 0 1 0 1.246-1.246zM8.369 14.978a6.607 6.607 0 1 1 6.607-6.607 6.614 6.614 0 0 1-6.607 6.607z" data-name="Path 773" transform="translate(0 -.003)" />
                        </g>
                      </g>
                    </svg>
                  </button>
                </li>}
              {sessionStorage.getItem('uType') === "VENDOR" &&
                <li className="switch-retailer" data-toggle="dropdown">
                  {/* <p>Retailer</p> */}
                  <span>{activeRetialer[0] !== undefined && activeRetialer[0].ename}</span><span className="switch-r-image"><img src={DownArrow} /></span>
                </li>}
              <ul className="dropdown-menu switch-retailer-dropdown" >
                <span className="srd-name">Available Retailers</span>
                {sessionStorage.getItem('enterprises') != null ?
                  JSON.parse(sessionStorage.getItem('enterprises')).map((data, key) => (
                    <li className="srau-name" key={key} onClick={data.active == 0 ? (e) => this.switchEnterprise(data) : null}>
                      <label >{data.ename}</label>
                      {data.active == 1 ? <span className="srd-active-user"></span> : null}
                    </li>
                  )) : null}
              </ul>
              {(sessionStorage.getItem('uType') === "VENDOR") ?
                <li className="pad-right-25">
                  <div className="bell_div vendor-notifibell">
                    <svg
                      className="bell_icon"
                      onClick={() => this.onShowHide()}
                      xmlns="http://www.w3.org/2000/svg"
                      width="25"
                      height="31"
                      viewBox="0 0 25 31"
                    >
                      <g fill="none" fillRule="nonzero">
                        <path
                          fill="#d6dce6"
                          d="M12.5 30.405a3.2 3.2 0 0 0 3.084-2.347H9.416a3.2 3.2 0 0 0 3.084 2.347z"
                        />
                        <path
                          fill="#d6dce6"
                          d="M24.539 24.03c-1.173-1.747-2.132-7.572-2.132-12.945 0-4.934-3.76-9.044-8.595-9.682.002-.029.005-.057.005-.086a1.317 1.317 0 0 0-2.634 0c0 .029.003.057.005.086-4.835.638-8.595 4.748-8.595 9.682 0 5.373-.96 11.198-2.132 12.945-1.172 1.746 2.662 3.175 8.521 3.175h7.035c5.86 0 9.694-1.428 8.522-3.175z"
                        />
                      </g>
                    </svg>
                    {/* <span className="badge" onClick={() => this.onShowHide()}>
                {notification}
              </span> */}
                  </div>
                  {/* <Notification resetNotifyCount={() => this.resetNotify()} notifyShow={this.state.notifyShow} /> */}
                  {/* ) : null} */}
                </li> :
                <li className="pad-right-25">
                  <div className="bell_div new-bell-div">
                    <svg
                      className="bell_icon"
                      onClick={() => this.onShowHide()}
                      xmlns="http://www.w3.org/2000/svg"
                      width="25"
                      height="31"
                      viewBox="0 0 25 31"
                    >
                      <g fill="none" fillRule="nonzero">
                        <path
                          fill="#d6dce6"
                          d="M12.5 30.405a3.2 3.2 0 0 0 3.084-2.347H9.416a3.2 3.2 0 0 0 3.084 2.347z"
                        />
                        <path
                          fill="#d6dce6"
                          d="M24.539 24.03c-1.173-1.747-2.132-7.572-2.132-12.945 0-4.934-3.76-9.044-8.595-9.682.002-.029.005-.057.005-.086a1.317 1.317 0 0 0-2.634 0c0 .029.003.057.005.086-4.835.638-8.595 4.748-8.595 9.682 0 5.373-.96 11.198-2.132 12.945-1.172 1.746 2.662 3.175 8.521 3.175h7.035c5.86 0 9.694-1.428 8.522-3.175z"
                        />
                      </g>
                    </svg>
                    {/* <span className="badge" onClick={() => this.onShowHide()}>
                    {notification}
                  </span> */}
                  </div>
                  {/* <Notification resetNotifyCount={() => this.resetNotify()} notifyShow={this.state.notifyShow} /> */}
                  {/* ) : null} */}
                </li>}
              {/* user-info-dropopen */}
              <ul className={this.state.profileDrop ? "user-info-drop user-info-dropopen" : "user-info-drop"} onClick={this.handleProfileDrop}>
                <li className="user-short-name" >
                  <div className="profileDiv">
                    {/*{sessionStorage.getItem("profile") != null ? <img className="header_profile_img headerProfileImg" src={sessionStorage.getItem("profile")} /> : <img className="header_profile_img headerProfileImg" src={profile} />} </div> */}
                    <div className="user-name-icon">
                      <span className="firstLetter">
                        {sessionStorage.getItem('firstName') == null ? "" : sessionStorage.getItem('firstName').charAt(0)}
                      </span>
                    </div>
                  </div>
                </li>
                <li>
                  <div className={"dropdown-header-menu"}>
                    <button className="name_para  name-para-2" >
                      {sessionStorage.getItem('firstName') == null ? "" : sessionStorage.getItem('firstName')} {sessionStorage.getItem('lastName') == null ? "" : sessionStorage.getItem('lastName')}
                    </button>
                    <span className="dt-down-arrow">
                      <svg xmlns="http://www.w3.org/2000/svg" width="14.293" height="8.146" viewBox="0 0 14.293 8.146">
                        <path fill="none" stroke="#dce6f0" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2px" id="prefix__Path_377" d="M6 14.732L11.732 9l5.732 5.732" data-name="Path 377" transform="translate(-4.586 -8)" />
                      </svg>
                    </span>
                    {sessionStorage.getItem("uType") == "ENT" && <ul className={this.state.profileDrop ? "dropdown-menu header-user-new-dropdown displayBlock" : "dropdown-menu header-user-new-dropdown "}>
                      <li onClick={() => this.props.history.push('/profile')}>
                        <span className="hund-icon">
                          <svg xmlns="http://www.w3.org/2000/svg" id="prefix__user_2_" width="13.744" height="13.744" data-name="user (2)" viewBox="0 0 13.744 13.744">
                            <g id="prefix__Group_2544" data-name="Group 2544">
                              <path fill="#fff" id="prefix__Path_625" d="M11.731 8.885A6.846 6.846 0 0 0 9.12 7.247a3.973 3.973 0 1 0-4.5 0A6.883 6.883 0 0 0 0 13.744h1.074a5.8 5.8 0 0 1 11.6 0h1.074a6.827 6.827 0 0 0-2.017-4.859zM6.872 6.872a2.9 2.9 0 1 1 2.9-2.9 2.9 2.9 0 0 1-2.9 2.9z" data-name="Path 625" />
                            </g>
                          </svg>
                        </span>
                        <label>
                          Profile
                      </label>
                      </li>
                      <li className="hoverMulti_dropdown" id="switchRoleHover" onMouseOver={() => this.switchDropDown()} onMouseLeave={() => this.closeSwitchDrop()}>
                        <span className="hund-icon">
                          <svg xmlns="http://www.w3.org/2000/svg" width="13.744" height="15.503" viewBox="0 0 13.744 15.503">
                            <g id="prefix__refresh" transform="translate(-29.049)">
                              <path fill="#fff" id="prefix__Path_637" d="M83.32 2.417L80.632.151a.646.646 0 0 0-.831.989l1.333 1.133h-6.385a3.67 3.67 0 0 0-3.666 3.667V7.1a.646.646 0 0 0 1.291 0V5.928a2.379 2.379 0 0 1 2.375-2.375h6.388L79.8 4.686a.646.646 0 0 0 .831.989l2.693-2.265a.646.646 0 0 0 .08-.91.659.659 0 0 0-.08-.08z" className="prefix__cls-1" data-name="Path 637" transform="translate(-40.761)" />
                              <path fill="#fff" id="prefix__Path_638" d="M40.856 255.877a.646.646 0 0 0-.646.646v1.171a2.379 2.379 0 0 1-2.375 2.375h-6.388l1.333-1.133a.646.646 0 1 0-.625-1.13.638.638 0 0 0-.187.156l-2.688 2.265a.646.646 0 0 0-.08.909.653.653 0 0 0 .08.08l2.688 2.265a.646.646 0 0 0 .831-.989l-1.333-1.133h6.37a3.67 3.67 0 0 0 3.666-3.666v-1.17a.645.645 0 0 0-.646-.646z" className="prefix__cls-1" data-name="Path 638" transform="translate(0 -248.129)" />
                            </g>
                          </svg>
                        </span>
                        <label className="roleHeader">
                          Switch Role
                      </label>
                        <button className="btnHeader">
                          {JSON.parse(sessionStorage.getItem('roles')) == null ? null : JSON.parse(sessionStorage.getItem('roles')).length}
                        </button>
                        <ul className="dropdown-submenu switchRoleDrop" id="openSwitchRole">
                          {JSON.parse(sessionStorage.getItem('roles')) == null ? null : JSON.parse(sessionStorage.getItem('roles')).map((data, key) => (
                            <li key={key} className="curren" onClick={() => this.switchRole(`${data.id}`)} >
                              <label>
                                {data.name}
                              </label>
                              {data.id == parseInt(JSON.parse(sessionStorage.getItem('mid'))) ?
                                // <img src={activeRole} /> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.292" height="7.215" viewBox="0 0 9.292 7.215">
                                  <path fill="#fff" id="prefix__correct_1_" d="M3.558 7.482a.474.474 0 0 1-.671 0L.208 4.8a.711.711 0 0 1 0-1.006l.335-.335a.711.711 0 0 1 1.006 0l1.674 1.675L7.741.615a.711.711 0 0 1 1.006 0l.336.335a.711.711 0 0 1 0 1.006zm0 0" data-name="correct (1)" transform="translate(0 -.406)" />
                                </svg>
                                : null}
                            </li>))}
                        </ul>

                      </li>
                      {/* <li onClick={() => this.props.history.push('/profile/changePassword')}>
                        <span className="hund-icon">
                          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="8.55" viewBox="0 0 17 8.55">
                            <g id="prefix__Group_2549" data-name="Group 2549" transform="translate(-1202 -173)">
                              <g stroke="#fff" strokeWidth="1.5px" fill="none" id="prefix__Rectangle_506" data-name="Rectangle 506" transform="translate(1202 173)" >
                                <rect stroke="none" width="17" height="8.55" rx="3" />
                                <rect fill="none" width="15.5" height="7.05" x=".75" y=".75" rx="2.25" />
                              </g>
                              <circle fill="#fff" id="prefix__Ellipse_83" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 83" transform="translate(1205.398 176.275)" />
                              <circle fill="#fff" id="prefix__Ellipse_84" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 84" transform="translate(1209.5 176.275)" />
                              <circle fill="#fff" id="prefix__Ellipse_85" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 85" transform="translate(1213.602 176.275)" />
                            </g>
                          </svg>
                        </span>

                        <label>
                          Change Password
                      </label>
                      </li> */}
                      {/* {Change_Setting > 0 ? */}
                      <li onClick={() => this.props.history.push('/changeSetting')}>
                        <span className="hund-icon">
                          <svg xmlns="http://www.w3.org/2000/svg" id="prefix__settings_2_" width="14.305" height="14.305" data-name="settings (2)" viewBox="0 0 14.305 14.305">
                            <path fill="#fff" id="prefix__Path_629" d="M137.029 133.609a3.419 3.419 0 1 0 3.419 3.419 3.423 3.423 0 0 0-3.419-3.419zm0 5.984a2.565 2.565 0 1 1 2.565-2.565 2.568 2.568 0 0 1-2.565 2.565zm0 0" className="prefix__cls-1" data-name="Path 629" transform="translate(-129.876 -129.876)" />
                            <path fill="#fff" id="prefix__Path_630" d="M13.968 5.524l-1.1-.239a5.983 5.983 0 0 0-.354-.853l.609-.947a.428.428 0 0 0-.057-.533l-1.713-1.714a.428.428 0 0 0-.533-.057l-.947.609a5.983 5.983 0 0 0-.853-.354L8.781.337A.427.427 0 0 0 8.363 0H5.941a.427.427 0 0 0-.418.337l-.239 1.1a5.983 5.983 0 0 0-.853.354l-.947-.609a.428.428 0 0 0-.533.057L1.238 2.951a.428.428 0 0 0-.057.533l.609.947a5.983 5.983 0 0 0-.354.853l-1.1.239A.428.428 0 0 0 0 5.941v2.422a.428.428 0 0 0 .337.418l1.1.239a5.983 5.983 0 0 0 .354.853l-.609.947a.428.428 0 0 0 .057.533l1.713 1.713a.428.428 0 0 0 .533.057l.947-.609a5.983 5.983 0 0 0 .853.354l.239 1.1a.427.427 0 0 0 .418.337h2.421a.427.427 0 0 0 .418-.337l.239-1.1a5.983 5.983 0 0 0 .853-.354l.947.609a.427.427 0 0 0 .533-.057l1.713-1.713a.428.428 0 0 0 .057-.533l-.609-.947a5.983 5.983 0 0 0 .354-.853l1.1-.239a.428.428 0 0 0 .337-.418V5.941a.428.428 0 0 0-.337-.417zm-.518 2.5l-1.008.219a.428.428 0 0 0-.321.3 5.128 5.128 0 0 1-.474 1.142.427.427 0 0 0 .013.441l.558.868-1.225 1.226-.868-.558a.427.427 0 0 0-.441-.013 5.131 5.131 0 0 1-1.142.474.428.428 0 0 0-.3.321l-.223 1.006H6.286l-.219-1.008a.428.428 0 0 0-.3-.321 5.128 5.128 0 0 1-1.142-.474.428.428 0 0 0-.441.013l-.868.558-1.229-1.225.558-.868a.427.427 0 0 0 .013-.441 5.134 5.134 0 0 1-.474-1.142.428.428 0 0 0-.321-.3L.855 8.019V6.286l1.008-.219a.428.428 0 0 0 .321-.3 5.129 5.129 0 0 1 .474-1.142.427.427 0 0 0-.013-.441l-.558-.868 1.225-1.229.868.558a.427.427 0 0 0 .441.013 5.132 5.132 0 0 1 1.142-.474.428.428 0 0 0 .3-.321L6.286.855h1.733l.219 1.008a.428.428 0 0 0 .3.321 5.129 5.129 0 0 1 1.142.474.428.428 0 0 0 .441-.013l.868-.558 1.225 1.226-.558.868a.427.427 0 0 0-.013.441 5.129 5.129 0 0 1 .474 1.142.428.428 0 0 0 .321.3l1.008.219zm0 0" className="prefix__cls-1" data-name="Path 630" />
                          </svg>
                        </span>

                        <label>
                          Change Settings
                      </label>
                      </li>
                      {/* : null} */}

                      {/* ____________________________SWITCH ORGANISATION______________________ */}

                      <li className="hoverMulti_dropdown" id="switchOrgHover" >
                        <span className="hund-icon">
                          <svg xmlns="http://www.w3.org/2000/svg" id="building" width="14.86" height="14.86" viewBox="0 0 14.86 14.86">
                            <path fill="#fff" id="Path_1364" d="M11.346 3.505V0L3.514 2.635v2.842L0 6.355v8.505h14.86V2.627zM3.514 13.989h-.886v-2.223h-.871v2.223H.871V7.035l2.644-.661zm5.2-.011h-.848v-1.773h.854zm-1.724 0h-.849v-1.773h.854zm3.48.011h-.88v-2.655H5.27v2.655h-.885V3.261l6.09-2.049zm3.514 0H13.1v-2.216h-.871v2.216h-.886V4.4l2.644-.661z" className="cls-1"/>
                            <path fill="#fff" id="Path_1365" d="M181.588 314.637h.871v1.188h-.871z" className="cls-1" transform="translate(-176.318 -305.505)"/>
                            <path fill="#fff" id="Path_1366" d="M300.412 314.637h.871v1.188h-.871z" className="cls-1" transform="translate(-291.693 -305.505)"/>
                            <path fill="#fff" id="Path_1367" d="M241 314.889h.871v1.188H241z" className="cls-1" transform="translate(-234.005 -305.75)"/>
                            <path fill="#fff" id="Path_1368" d="M181.588 224.034h.871v1.188h-.871z" className="cls-1" transform="translate(-176.318 -217.532)"/>
                            <path fill="#fff" id="Path_1369" d="M300.412 224.034h.871v1.188h-.871z" className="cls-1" transform="translate(-291.693 -217.532)"/>
                            <path fill="#fff" id="Path_1370" d="M241 224.286h.871v1.188H241z" className="cls-1" transform="translate(-234.005 -217.776)"/>
                            <path fill="#fff" id="Path_1371" d="M181.588 133.377h.871v1.188h-.871z" className="cls-1" transform="translate(-176.318 -129.506)"/>
                            <path fill="#fff" id="Path_1372" d="M300.412 93.029h.871v2.359h-.871z" className="cls-1" transform="translate(-291.693 -90.329)"/>
                            <path fill="#fff" id="Path_1373" d="M241 113.455h.871v1.773H241z" className="cls-1" transform="translate(-234.005 -110.162)"/>
                            <path fill="#fff" id="Path_1374" d="M421.457 314.889h.871v1.188h-.871z" className="cls-1" transform="translate(-409.225 -305.75)"/>
                            <path fill="#fff" id="Path_1375" d="M60.543 273.606h.871v2.242h-.871z" className="cls-1" transform="translate(-58.786 -265.665)"/>
                            <path fill="#fff" id="Path_1376" d="M421.457 187.973h.871v2.242h-.871z" className="cls-1" transform="translate(-409.225 -182.517)"/>
                          </svg>
                        </span>
                        <label className="roleHeader">
                          Switch Organization
                      </label>
                        <button className="btnHeader">
                          {JSON.parse(sessionStorage.getItem('orgId-name')) == null ? null : JSON.parse(sessionStorage.getItem('orgId-name')).length}
                        </button>
                        <ul className="dropdown-submenu switchRoleDrop" id="openSwitchOrg">
                          {JSON.parse(sessionStorage.getItem('orgId-name')) == null ? null : JSON.parse(sessionStorage.getItem('orgId-name')).map((data, key) => (
                            <li key={key} className="curren" onClick={data.active == 0 ? () => this.switchOrganisation(data) : null} >
                              <label>
                                {data.orgName}
                              </label>
                              {data.active == 1 ?
                                // <img src={activeRole} />
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.292" height="7.215" viewBox="0 0 9.292 7.215">
                                  <path fill="#fff" id="prefix__correct_1_" d="M3.558 7.482a.474.474 0 0 1-.671 0L.208 4.8a.711.711 0 0 1 0-1.006l.335-.335a.711.711 0 0 1 1.006 0l1.674 1.675L7.741.615a.711.711 0 0 1 1.006 0l.336.335a.711.711 0 0 1 0 1.006zm0 0" data-name="correct (1)" transform="translate(0 -.406)" />
                                </svg>
                                : null}
                            </li>))}
                        </ul>

                      </li>

                      <li className="reportBug" onClick={()=> window.open("https://turningcloud.freshdesk.com/support/tickets/new", "_blank")}>
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.518" height="14.518" viewBox="0 0 14.518 14.518">
                              <g id="prefix__bug" transform="translate(-.002)">
                                <g id="prefix__Group_2548" data-name="Group 2548" transform="translate(.002)">
                                  <path fill="#fff" id="prefix__Path_631" d="M14.1 7.4h-2.512v-.872a2.656 2.656 0 0 0 2.233-2.619v-.558a.419.419 0 0 0-.838 0v.558a1.818 1.818 0 0 1-1.4 1.765v-.369a.7.7 0 0 0-.7-.7h-.419V3.072a1.818 1.818 0 0 0-1.465-1.78l.312-.7a.419.419 0 1 0-.765-.34l-.441 1.005H6.416L5.968.249a.419.419 0 1 0-.768.34l.312.7A1.818 1.818 0 0 0 4.05 3.072v1.535h-.419a.7.7 0 0 0-.7.7v.369a1.818 1.818 0 0 1-1.4-1.765v-.56a.419.419 0 0 0-.838 0v.558a2.656 2.656 0 0 0 2.24 2.619V7.4H.421a.419.419 0 0 0 0 .838h2.512v.871A2.656 2.656 0 0 0 .7 11.726v.558a.419.419 0 1 0 .838 0v-.558a1.818 1.818 0 0 1 1.4-1.765v.23a4.327 4.327 0 1 0 8.655 0v-.23a1.818 1.818 0 0 1 1.4 1.765v.558a.419.419 0 0 0 .838 0v-.558a2.656 2.656 0 0 0-2.233-2.619v-.871H14.1a.419.419 0 0 0 0-.838zM4.888 3.072a.978.978 0 0 1 .977-.977h2.792a.978.978 0 0 1 .977.977v1.535H4.888zm5.863 7.119a3.49 3.49 0 0 1-6.98 0V5.445h3.071v4.048a.419.419 0 1 0 .838 0V5.445h3.071z" data-name="Path 631" transform="translate(-.002)" />
                                </g>
                              </g>
                            </svg>
                          </span>
                          <label>Report an issue</label>
                      </li>
                      <li className="reportBug" onClick={()=> window.open("http://support.supplymint.com/", "_blank")}>
                            {/* <img src={bugShape} /> */}
                            <span className="hund-icon">
                              <svg xmlns="http://www.w3.org/2000/svg" id="help" width="14.501" height="14.501" viewBox="0 0 14.501 14.501">
                                <path fill="#fff" id="Path_1359" d="M14.5 9.814A4.7 4.7 0 0 0 11.068 5.3a5.537 5.537 0 1 0-10.3 3.051l-.747 2.7 2.7-.747a5.516 5.516 0 0 0 2.574.762 4.689 4.689 0 0 0 6.9 2.781l2.282.631-.628-2.278a4.671 4.671 0 0 0 .651-2.386zM2.857 9.387l-1.618.448.448-1.618-.1-.16a4.686 4.686 0 1 1 1.429 1.432zm10.405 3.875l-1.2-.332-.16.1a3.839 3.839 0 0 1-5.726-2 5.547 5.547 0 0 0 4.862-4.862 3.839 3.839 0 0 1 2 5.726l-.1.16zm0 0" className="cls-1"/>
                                <path fill="#fff" id="Path_1360" d="M180.5 271h.85v.85h-.85zm0 0" className="cls-1" transform="translate(-175.388 -263.325)"/>
                                <path fill="#fff" id="Path_1361" d="M138.049 91.7a.841.841 0 0 1-.277.627l-1 .913v1.037h.85v-.663l.722-.661A1.7 1.7 0 1 0 135.5 91.7h.85a.85.85 0 1 1 1.7 0zm0 0" className="cls-1" transform="translate(-131.662 -87.451)"/>
                              </svg>
                            </span>
                            <label>Help & Support</label>
                        </li>
                      {/* <li onClick={(e) => this.onLogout(e)}>
                        <span className="hund-icon">
                          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 20 19">
                            <g fill="none" fillRule="evenodd" stroke="#fff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
                              <path d="M5.402 2.5A8.183 8.183 0 0 0 10 17.5a8.183 8.183 0 0 0 4.598-15M10 1v8.25" />
                            </g>
                          </svg>
                        </span>
                        <label>
                          Logout
                          </label>
                      </li> */}
                      <li className="last-login-detail">
                        <span>
                          Last Login: 
                            <span  className="lld-time">
                            {sessionStorage.getItem("loginTime") != null ? sessionStorage.getItem("loginTime") : null}
                          </span>
                        </span>
                      </li>

                    </ul>}


                    {/* _________________________________VENDOR HEADER DROPDOWN________________ */}
                    {sessionStorage.getItem("uType") == "VENDOR" &&
                      <ul className={this.state.profileDrop ? "displayBlock dropdown-menu header-user-new-dropdown" : "dropdown-menu header-user-new-dropdown"}>
                        <li onClick={() => this.props.history.push('/profile')}>
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__user_2_" width="13.744" height="13.744" data-name="user (2)" viewBox="0 0 13.744 13.744">
                              <g id="prefix__Group_2544" data-name="Group 2544">
                                <path fill="#fff" id="prefix__Path_625" d="M11.731 8.885A6.846 6.846 0 0 0 9.12 7.247a3.973 3.973 0 1 0-4.5 0A6.883 6.883 0 0 0 0 13.744h1.074a5.8 5.8 0 0 1 11.6 0h1.074a6.827 6.827 0 0 0-2.017-4.859zM6.872 6.872a2.9 2.9 0 1 1 2.9-2.9 2.9 2.9 0 0 1-2.9 2.9z" data-name="Path 625" />
                              </g>
                            </svg>
                          </span>

                          <label>
                            Profile
                    </label>
                        </li>
                        {/* <li className="hoverMulti_dropdown" id="switchRoleHover" onMouseOver={() => this.switchDropDown()} onMouseLeave={() => this.closeSwitchDrop()}>
                          <svg className="imagedropdown1" xmlns="http://www.w3.org/2000/svg" width="22" height="19" viewBox="0 0 22 19">
                            <g fill="#4A4A4A" fillRule="nonzero">
                              <path d="M9.724 13.005a1.903 1.903 0 0 1-.233-.907c0-.292.087-.565.212-.823 1.108-.756 1.855-2.113 1.855-3.677 0-2.382-1.716-4.312-3.833-4.312S3.89 5.216 3.89 7.598c0 1.564.748 2.921 1.856 3.677.124.258.211.53.211.823 0 .325-.085.632-.233.907-3.263.44-5.667 1.927-5.667 3.697v1.917h15.333v-1.917c0-1.77-2.404-3.256-5.667-3.697zm4.572 4.519H1.153v-.822c0-.97 1.765-2.213 4.719-2.611l.552-.075.265-.49c.238-.442.364-.936.364-1.428 0-.429-.105-.854-.32-1.3l-.128-.263-.24-.165c-.863-.588-1.378-1.624-1.378-2.772 0-1.774 1.228-3.217 2.738-3.217s2.738 1.443 2.738 3.217c0 1.148-.515 2.184-1.378 2.772l-.241.165-.127.263c-.216.446-.321.871-.321 1.3 0 .492.126.986.365 1.427l.264.491.553.075c2.953.398 4.718 1.64 4.718 2.611v.822zM20.905 4.381V3.286a3.286 3.286 0 1 0-6.572 0V4.38c-.605 0-1.095.49-1.095 1.095v4.381c0 .605.49 1.095 1.095 1.095h6.572c.605 0 1.095-.49 1.095-1.095v-4.38c0-.606-.49-1.096-1.095-1.096zm-5.476-1.095c0-1.208.982-2.19 2.19-2.19s2.19.982 2.19 2.19V4.38h-4.38V3.286zm5.476 6.571h-6.572v-4.38h6.572v4.38zm-4.381-2.19a1.095 1.095 0 1 1 2.19 0 1.095 1.095 0 0 1-2.19 0z" />
                            </g>
                          </svg>

                          <label className="roleHeader">
                            Switch Retailer
                    </label>
                          <button className="btnHeader">
                            {JSON.parse(sessionStorage.getItem('roles')) == null ? null : JSON.parse(sessionStorage.getItem('roles')).length}
                          </button>
                          <ul className="dropdown-submenu switchRoleDrop" id="openSwitchRole">
                            previous commented
                            {JSON.parse(sessionStorage.getItem('roles')) == null ? null : JSON.parse(sessionStorage.getItem('roles')).map((data, key) => (
                              <li key={key} className="curren" onClick={() => this.switchRole(`${data.id}`)} >
                                <label>
                                  {data.name}
                                </label>
                                {data.id == parseInt(JSON.parse(sessionStorage.getItem('mid'))) ? <img src={activeRole} /> : null}
                              </li>))}
                              ---------------
                            {sessionStorage.getItem('enterprises') != null ?
                              JSON.parse(sessionStorage.getItem('enterprises')).map((data, key) => (
                                <li key={key} onClick={data.active == "FALSE" ? (e) => this.switchEnterprise(data.eid) : null}>
                                  <label >{data.ename}</label>
                                  {data.active == "TRUE" ? <img src={activeRole} className="width-19" /> : null}
                                </li>
                              )) : null}
                          </ul>
                        </li> */}
                        {/* <li onClick={() => this.props.history.push('/profile/changePassword')}>
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="8.55" viewBox="0 0 17 8.55">
                              <g id="prefix__Group_2549" data-name="Group 2549" transform="translate(-1202 -173)">
                                <g stroke="#fff" strokeWidth="1.5px" fill="none" id="prefix__Rectangle_506" data-name="Rectangle 506" transform="translate(1202 173)" >
                                  <rect stroke="none" width="17" height="8.55" rx="3" />
                                  <rect fill="none" width="15.5" height="7.05" x=".75" y=".75" rx="2.25" />
                                </g>
                                <circle fill="#fff" id="prefix__Ellipse_83" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 83" transform="translate(1205.398 176.275)" />
                                <circle fill="#fff" id="prefix__Ellipse_84" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 84" transform="translate(1209.5 176.275)" />
                                <circle fill="#fff" id="prefix__Ellipse_85" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 85" transform="translate(1213.602 176.275)" />
                              </g>
                            </svg>
                          </span>

                          <label>
                            Change Password
                    </label>
                        </li> */}
                        {/* {Change_Setting > 0 ? */}
                        <li onClick={() => this.props.history.push('/changeSetting')}>
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__settings_2_" width="14.305" height="14.305" data-name="settings (2)" viewBox="0 0 14.305 14.305">
                              <path fill="#fff" id="prefix__Path_629" d="M137.029 133.609a3.419 3.419 0 1 0 3.419 3.419 3.423 3.423 0 0 0-3.419-3.419zm0 5.984a2.565 2.565 0 1 1 2.565-2.565 2.568 2.568 0 0 1-2.565 2.565zm0 0" className="prefix__cls-1" data-name="Path 629" transform="translate(-129.876 -129.876)" />
                              <path fill="#fff" id="prefix__Path_630" d="M13.968 5.524l-1.1-.239a5.983 5.983 0 0 0-.354-.853l.609-.947a.428.428 0 0 0-.057-.533l-1.713-1.714a.428.428 0 0 0-.533-.057l-.947.609a5.983 5.983 0 0 0-.853-.354L8.781.337A.427.427 0 0 0 8.363 0H5.941a.427.427 0 0 0-.418.337l-.239 1.1a5.983 5.983 0 0 0-.853.354l-.947-.609a.428.428 0 0 0-.533.057L1.238 2.951a.428.428 0 0 0-.057.533l.609.947a5.983 5.983 0 0 0-.354.853l-1.1.239A.428.428 0 0 0 0 5.941v2.422a.428.428 0 0 0 .337.418l1.1.239a5.983 5.983 0 0 0 .354.853l-.609.947a.428.428 0 0 0 .057.533l1.713 1.713a.428.428 0 0 0 .533.057l.947-.609a5.983 5.983 0 0 0 .853.354l.239 1.1a.427.427 0 0 0 .418.337h2.421a.427.427 0 0 0 .418-.337l.239-1.1a5.983 5.983 0 0 0 .853-.354l.947.609a.427.427 0 0 0 .533-.057l1.713-1.713a.428.428 0 0 0 .057-.533l-.609-.947a5.983 5.983 0 0 0 .354-.853l1.1-.239a.428.428 0 0 0 .337-.418V5.941a.428.428 0 0 0-.337-.417zm-.518 2.5l-1.008.219a.428.428 0 0 0-.321.3 5.128 5.128 0 0 1-.474 1.142.427.427 0 0 0 .013.441l.558.868-1.225 1.226-.868-.558a.427.427 0 0 0-.441-.013 5.131 5.131 0 0 1-1.142.474.428.428 0 0 0-.3.321l-.223 1.006H6.286l-.219-1.008a.428.428 0 0 0-.3-.321 5.128 5.128 0 0 1-1.142-.474.428.428 0 0 0-.441.013l-.868.558-1.229-1.225.558-.868a.427.427 0 0 0 .013-.441 5.134 5.134 0 0 1-.474-1.142.428.428 0 0 0-.321-.3L.855 8.019V6.286l1.008-.219a.428.428 0 0 0 .321-.3 5.129 5.129 0 0 1 .474-1.142.427.427 0 0 0-.013-.441l-.558-.868 1.225-1.229.868.558a.427.427 0 0 0 .441.013 5.132 5.132 0 0 1 1.142-.474.428.428 0 0 0 .3-.321L6.286.855h1.733l.219 1.008a.428.428 0 0 0 .3.321 5.129 5.129 0 0 1 1.142.474.428.428 0 0 0 .441-.013l.868-.558 1.225 1.226-.558.868a.427.427 0 0 0-.013.441 5.129 5.129 0 0 1 .474 1.142.428.428 0 0 0 .321.3l1.008.219zm0 0" className="prefix__cls-1" data-name="Path 630" />
                            </svg>
                          </span>

                          <label>
                            Change Settings
                    </label>
                        </li>
                        {/* : null} */}
                        <li className="reportBug">
                          <a onClick={() => this.props.history.push('/vendorAdmin')}>
                            {/* <img src={bugShape} /> */}
                            <span className="hund-icon">
                              <svg className="imagedropdown1" xmlns="http://www.w3.org/2000/svg" width="22" height="14" viewBox="0 0 22 19">
                                <g fill="#fff" fillRule="nonzero">
                                  <path d="M9.724 13.005a1.903 1.903 0 0 1-.233-.907c0-.292.087-.565.212-.823 1.108-.756 1.855-2.113 1.855-3.677 0-2.382-1.716-4.312-3.833-4.312S3.89 5.216 3.89 7.598c0 1.564.748 2.921 1.856 3.677.124.258.211.53.211.823 0 .325-.085.632-.233.907-3.263.44-5.667 1.927-5.667 3.697v1.917h15.333v-1.917c0-1.77-2.404-3.256-5.667-3.697zm4.572 4.519H1.153v-.822c0-.97 1.765-2.213 4.719-2.611l.552-.075.265-.49c.238-.442.364-.936.364-1.428 0-.429-.105-.854-.32-1.3l-.128-.263-.24-.165c-.863-.588-1.378-1.624-1.378-2.772 0-1.774 1.228-3.217 2.738-3.217s2.738 1.443 2.738 3.217c0 1.148-.515 2.184-1.378 2.772l-.241.165-.127.263c-.216.446-.321.871-.321 1.3 0 .492.126.986.365 1.427l.264.491.553.075c2.953.398 4.718 1.64 4.718 2.611v.822zM20.905 4.381V3.286a3.286 3.286 0 1 0-6.572 0V4.38c-.605 0-1.095.49-1.095 1.095v4.381c0 .605.49 1.095 1.095 1.095h6.572c.605 0 1.095-.49 1.095-1.095v-4.38c0-.606-.49-1.096-1.095-1.096zm-5.476-1.095c0-1.208.982-2.19 2.19-2.19s2.19.982 2.19 2.19V4.38h-4.38V3.286zm5.476 6.571h-6.572v-4.38h6.572v4.38zm-4.381-2.19a1.095 1.095 0 1 1 2.19 0 1.095 1.095 0 0 1-2.19 0z" />
                                </g>
                              </svg>
                            </span>
                            <label >Administration</label>
                          </a>
                        </li>
                        <li className="reportBug" onClick={()=> window.open("https://turningcloud.freshdesk.com/support/tickets/new", "_blank")}>
                            {/* <img src={bugShape} /> */}
                            <span className="hund-icon">
                              <svg xmlns="http://www.w3.org/2000/svg" width="14.518" height="14.518" viewBox="0 0 14.518 14.518">
                                <g id="prefix__bug" transform="translate(-.002)">
                                  <g id="prefix__Group_2548" data-name="Group 2548" transform="translate(.002)">
                                    <path fill="#fff" id="prefix__Path_631" d="M14.1 7.4h-2.512v-.872a2.656 2.656 0 0 0 2.233-2.619v-.558a.419.419 0 0 0-.838 0v.558a1.818 1.818 0 0 1-1.4 1.765v-.369a.7.7 0 0 0-.7-.7h-.419V3.072a1.818 1.818 0 0 0-1.465-1.78l.312-.7a.419.419 0 1 0-.765-.34l-.441 1.005H6.416L5.968.249a.419.419 0 1 0-.768.34l.312.7A1.818 1.818 0 0 0 4.05 3.072v1.535h-.419a.7.7 0 0 0-.7.7v.369a1.818 1.818 0 0 1-1.4-1.765v-.56a.419.419 0 0 0-.838 0v.558a2.656 2.656 0 0 0 2.24 2.619V7.4H.421a.419.419 0 0 0 0 .838h2.512v.871A2.656 2.656 0 0 0 .7 11.726v.558a.419.419 0 1 0 .838 0v-.558a1.818 1.818 0 0 1 1.4-1.765v.23a4.327 4.327 0 1 0 8.655 0v-.23a1.818 1.818 0 0 1 1.4 1.765v.558a.419.419 0 0 0 .838 0v-.558a2.656 2.656 0 0 0-2.233-2.619v-.871H14.1a.419.419 0 0 0 0-.838zM4.888 3.072a.978.978 0 0 1 .977-.977h2.792a.978.978 0 0 1 .977.977v1.535H4.888zm5.863 7.119a3.49 3.49 0 0 1-6.98 0V5.445h3.071v4.048a.419.419 0 1 0 .838 0V5.445h3.071z" data-name="Path 631" transform="translate(-.002)" />
                                  </g>
                                </g>
                              </svg>
                            </span>
                            <label>Report an issue</label>
                        </li>
                        <li className="reportBug" onClick={()=> window.open("http://support.supplymint.com/", "_blank")}>
                            {/* <img src={bugShape} /> */}
                            <span className="hund-icon">
                              <svg xmlns="http://www.w3.org/2000/svg" id="help" width="14.501" height="14.501" viewBox="0 0 14.501 14.501">
                                <path fill="#fff" id="Path_1359" d="M14.5 9.814A4.7 4.7 0 0 0 11.068 5.3a5.537 5.537 0 1 0-10.3 3.051l-.747 2.7 2.7-.747a5.516 5.516 0 0 0 2.574.762 4.689 4.689 0 0 0 6.9 2.781l2.282.631-.628-2.278a4.671 4.671 0 0 0 .651-2.386zM2.857 9.387l-1.618.448.448-1.618-.1-.16a4.686 4.686 0 1 1 1.429 1.432zm10.405 3.875l-1.2-.332-.16.1a3.839 3.839 0 0 1-5.726-2 5.547 5.547 0 0 0 4.862-4.862 3.839 3.839 0 0 1 2 5.726l-.1.16zm0 0" className="cls-1"/>
                                <path fill="#fff" id="Path_1360" d="M180.5 271h.85v.85h-.85zm0 0" className="cls-1" transform="translate(-175.388 -263.325)"/>
                                <path fill="#fff" id="Path_1361" d="M138.049 91.7a.841.841 0 0 1-.277.627l-1 .913v1.037h.85v-.663l.722-.661A1.7 1.7 0 1 0 135.5 91.7h.85a.85.85 0 1 1 1.7 0zm0 0" className="cls-1" transform="translate(-131.662 -87.451)"/>
                              </svg>
                            </span>
                            <label>Help & Support</label>
                        </li>
                        {/* <li className="menuLogOut" onClick={(e) => this.onLogout(e)}>
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 20 19">
                              <g fill="none" fillRule="evenodd" stroke="#fff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
                                <path d="M5.402 2.5A8.183 8.183 0 0 0 10 17.5a8.183 8.183 0 0 0 4.598-15M10 1v8.25" />
                              </g>
                            </svg>
                          </span>
                          <label>
                            Logout
                          </label>
                        </li> */}
                        <li className="last-login-detail">
                          <span>
                            Last Login: 
                            <span className="lld-time">
                              {sessionStorage.getItem("loginTime") != null ? sessionStorage.getItem("loginTime") : null}
                            </span>
                          </span>
                        </li>

                      </ul>
                    }
                    {sessionStorage.getItem("uType") == "CUSTOMER" &&
                      <ul className={this.state.profileDrop ? "displayBlock dropdown-menu header-user-new-dropdown" : "dropdown-menu header-user-new-dropdown"}>
                        <li onClick={() => this.props.history.push('/profile')}>
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__user_2_" width="13.744" height="13.744" data-name="user (2)" viewBox="0 0 13.744 13.744">
                              <g id="prefix__Group_2544" data-name="Group 2544">
                                <path fill="#fff" id="prefix__Path_625" d="M11.731 8.885A6.846 6.846 0 0 0 9.12 7.247a3.973 3.973 0 1 0-4.5 0A6.883 6.883 0 0 0 0 13.744h1.074a5.8 5.8 0 0 1 11.6 0h1.074a6.827 6.827 0 0 0-2.017-4.859zM6.872 6.872a2.9 2.9 0 1 1 2.9-2.9 2.9 2.9 0 0 1-2.9 2.9z" data-name="Path 625" />
                              </g>
                            </svg>
                          </span>

                          <label>
                            Profile
                          </label>
                        </li>
                        {/* <li className="hoverMulti_dropdown" id="switchRoleHover" onMouseOver={() => this.switchDropDown()} onMouseLeave={() => this.closeSwitchDrop()}>
                        <svg className="imagedropdown1" xmlns="http://www.w3.org/2000/svg" width="22" height="19" viewBox="0 0 22 19">
                          <g fill="#4A4A4A" fillRule="nonzero">
                            <path d="M9.724 13.005a1.903 1.903 0 0 1-.233-.907c0-.292.087-.565.212-.823 1.108-.756 1.855-2.113 1.855-3.677 0-2.382-1.716-4.312-3.833-4.312S3.89 5.216 3.89 7.598c0 1.564.748 2.921 1.856 3.677.124.258.211.53.211.823 0 .325-.085.632-.233.907-3.263.44-5.667 1.927-5.667 3.697v1.917h15.333v-1.917c0-1.77-2.404-3.256-5.667-3.697zm4.572 4.519H1.153v-.822c0-.97 1.765-2.213 4.719-2.611l.552-.075.265-.49c.238-.442.364-.936.364-1.428 0-.429-.105-.854-.32-1.3l-.128-.263-.24-.165c-.863-.588-1.378-1.624-1.378-2.772 0-1.774 1.228-3.217 2.738-3.217s2.738 1.443 2.738 3.217c0 1.148-.515 2.184-1.378 2.772l-.241.165-.127.263c-.216.446-.321.871-.321 1.3 0 .492.126.986.365 1.427l.264.491.553.075c2.953.398 4.718 1.64 4.718 2.611v.822zM20.905 4.381V3.286a3.286 3.286 0 1 0-6.572 0V4.38c-.605 0-1.095.49-1.095 1.095v4.381c0 .605.49 1.095 1.095 1.095h6.572c.605 0 1.095-.49 1.095-1.095v-4.38c0-.606-.49-1.096-1.095-1.096zm-5.476-1.095c0-1.208.982-2.19 2.19-2.19s2.19.982 2.19 2.19V4.38h-4.38V3.286zm5.476 6.571h-6.572v-4.38h6.572v4.38zm-4.381-2.19a1.095 1.095 0 1 1 2.19 0 1.095 1.095 0 0 1-2.19 0z" />
                          </g>
                        </svg>

                        <label className="roleHeader">
                          Switch Retailer
                        </label>
                        <button className="btnHeader">
                          {JSON.parse(sessionStorage.getItem('roles')) == null ? null : JSON.parse(sessionStorage.getItem('roles')).length}
                        </button>
                        <ul className="dropdown-submenu switchRoleDrop" id="openSwitchRole">
                          previous commented
                          {JSON.parse(sessionStorage.getItem('roles')) == null ? null : JSON.parse(sessionStorage.getItem('roles')).map((data, key) => (
                            <li key={key} className="curren" onClick={() => this.switchRole(`${data.id}`)} >
                              <label>
                                {data.name}
                              </label>
                              {data.id == parseInt(JSON.parse(sessionStorage.getItem('mid'))) ? <img src={activeRole} /> : null}
                            </li>))}
                            ---------------
                          {sessionStorage.getItem('enterprises') != null ?
                            JSON.parse(sessionStorage.getItem('enterprises')).map((data, key) => (
                              <li key={key} onClick={data.active == "FALSE" ? (e) => this.switchEnterprise(data.eid) : null}>
                                <label >{data.ename}</label>
                                {data.active == "TRUE" ? <img src={activeRole} className="width-19" /> : null}
                              </li>
                            )) : null}
                        </ul>
                        </li> */}
                        {/* <li onClick={() => this.props.history.push('/profile/changePassword')}>
                        <span className="hund-icon">
                          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="8.55" viewBox="0 0 17 8.55">
                            <g id="prefix__Group_2549" data-name="Group 2549" transform="translate(-1202 -173)">
                              <g stroke="#fff" strokeWidth="1.5px" fill="none" id="prefix__Rectangle_506" data-name="Rectangle 506" transform="translate(1202 173)" >
                                <rect stroke="none" width="17" height="8.55" rx="3" />
                                <rect fill="none" width="15.5" height="7.05" x=".75" y=".75" rx="2.25" />
                              </g>
                              <circle fill="#fff" id="prefix__Ellipse_83" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 83" transform="translate(1205.398 176.275)" />
                              <circle fill="#fff" id="prefix__Ellipse_84" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 84" transform="translate(1209.5 176.275)" />
                              <circle fill="#fff" id="prefix__Ellipse_85" cx="1" cy="1" r="1" className="prefix__cls-2" data-name="Ellipse 85" transform="translate(1213.602 176.275)" />
                            </g>
                          </svg>
                        </span>

                        <label>
                          Change Password
                        </label>
                        </li> */}
                        {/* {Change_Setting > 0 ? */}
                        <li onClick={() => this.props.history.push('/changeSetting')}>
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__settings_2_" width="14.305" height="14.305" data-name="settings (2)" viewBox="0 0 14.305 14.305">
                              <path fill="#fff" id="prefix__Path_629" d="M137.029 133.609a3.419 3.419 0 1 0 3.419 3.419 3.423 3.423 0 0 0-3.419-3.419zm0 5.984a2.565 2.565 0 1 1 2.565-2.565 2.568 2.568 0 0 1-2.565 2.565zm0 0" className="prefix__cls-1" data-name="Path 629" transform="translate(-129.876 -129.876)" />
                              <path fill="#fff" id="prefix__Path_630" d="M13.968 5.524l-1.1-.239a5.983 5.983 0 0 0-.354-.853l.609-.947a.428.428 0 0 0-.057-.533l-1.713-1.714a.428.428 0 0 0-.533-.057l-.947.609a5.983 5.983 0 0 0-.853-.354L8.781.337A.427.427 0 0 0 8.363 0H5.941a.427.427 0 0 0-.418.337l-.239 1.1a5.983 5.983 0 0 0-.853.354l-.947-.609a.428.428 0 0 0-.533.057L1.238 2.951a.428.428 0 0 0-.057.533l.609.947a5.983 5.983 0 0 0-.354.853l-1.1.239A.428.428 0 0 0 0 5.941v2.422a.428.428 0 0 0 .337.418l1.1.239a5.983 5.983 0 0 0 .354.853l-.609.947a.428.428 0 0 0 .057.533l1.713 1.713a.428.428 0 0 0 .533.057l.947-.609a5.983 5.983 0 0 0 .853.354l.239 1.1a.427.427 0 0 0 .418.337h2.421a.427.427 0 0 0 .418-.337l.239-1.1a5.983 5.983 0 0 0 .853-.354l.947.609a.427.427 0 0 0 .533-.057l1.713-1.713a.428.428 0 0 0 .057-.533l-.609-.947a5.983 5.983 0 0 0 .354-.853l1.1-.239a.428.428 0 0 0 .337-.418V5.941a.428.428 0 0 0-.337-.417zm-.518 2.5l-1.008.219a.428.428 0 0 0-.321.3 5.128 5.128 0 0 1-.474 1.142.427.427 0 0 0 .013.441l.558.868-1.225 1.226-.868-.558a.427.427 0 0 0-.441-.013 5.131 5.131 0 0 1-1.142.474.428.428 0 0 0-.3.321l-.223 1.006H6.286l-.219-1.008a.428.428 0 0 0-.3-.321 5.128 5.128 0 0 1-1.142-.474.428.428 0 0 0-.441.013l-.868.558-1.229-1.225.558-.868a.427.427 0 0 0 .013-.441 5.134 5.134 0 0 1-.474-1.142.428.428 0 0 0-.321-.3L.855 8.019V6.286l1.008-.219a.428.428 0 0 0 .321-.3 5.129 5.129 0 0 1 .474-1.142.427.427 0 0 0-.013-.441l-.558-.868 1.225-1.229.868.558a.427.427 0 0 0 .441.013 5.132 5.132 0 0 1 1.142-.474.428.428 0 0 0 .3-.321L6.286.855h1.733l.219 1.008a.428.428 0 0 0 .3.321 5.129 5.129 0 0 1 1.142.474.428.428 0 0 0 .441-.013l.868-.558 1.225 1.226-.558.868a.427.427 0 0 0-.013.441 5.129 5.129 0 0 1 .474 1.142.428.428 0 0 0 .321.3l1.008.219zm0 0" className="prefix__cls-1" data-name="Path 630" />
                            </svg>
                          </span>

                          <label>
                            Change Settings
                          </label>
                        </li>
                        {/* : null} */}
                        <li className="reportBug">
                          <a onClick={() => this.props.history.push('/customerAdmin')}>
                            {/* <img src={bugShape} /> */}
                            <span className="hund-icon">
                              <svg className="imagedropdown1" xmlns="http://www.w3.org/2000/svg" width="22" height="14" viewBox="0 0 22 19">
                                <g fill="#fff" fillRule="nonzero">
                                  <path d="M9.724 13.005a1.903 1.903 0 0 1-.233-.907c0-.292.087-.565.212-.823 1.108-.756 1.855-2.113 1.855-3.677 0-2.382-1.716-4.312-3.833-4.312S3.89 5.216 3.89 7.598c0 1.564.748 2.921 1.856 3.677.124.258.211.53.211.823 0 .325-.085.632-.233.907-3.263.44-5.667 1.927-5.667 3.697v1.917h15.333v-1.917c0-1.77-2.404-3.256-5.667-3.697zm4.572 4.519H1.153v-.822c0-.97 1.765-2.213 4.719-2.611l.552-.075.265-.49c.238-.442.364-.936.364-1.428 0-.429-.105-.854-.32-1.3l-.128-.263-.24-.165c-.863-.588-1.378-1.624-1.378-2.772 0-1.774 1.228-3.217 2.738-3.217s2.738 1.443 2.738 3.217c0 1.148-.515 2.184-1.378 2.772l-.241.165-.127.263c-.216.446-.321.871-.321 1.3 0 .492.126.986.365 1.427l.264.491.553.075c2.953.398 4.718 1.64 4.718 2.611v.822zM20.905 4.381V3.286a3.286 3.286 0 1 0-6.572 0V4.38c-.605 0-1.095.49-1.095 1.095v4.381c0 .605.49 1.095 1.095 1.095h6.572c.605 0 1.095-.49 1.095-1.095v-4.38c0-.606-.49-1.096-1.095-1.096zm-5.476-1.095c0-1.208.982-2.19 2.19-2.19s2.19.982 2.19 2.19V4.38h-4.38V3.286zm5.476 6.571h-6.572v-4.38h6.572v4.38zm-4.381-2.19a1.095 1.095 0 1 1 2.19 0 1.095 1.095 0 0 1-2.19 0z" />
                                </g>
                              </svg>
                            </span>
                            <label >Administration</label>
                          </a>
                        </li>
                        <li className="reportBug" onClick={() => window.open("https://turningcloud.freshdesk.com/support/tickets/new", "_blank")}>
                          {/* <img src={bugShape} /> */}
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.518" height="14.518" viewBox="0 0 14.518 14.518">
                              <g id="prefix__bug" transform="translate(-.002)">
                                <g id="prefix__Group_2548" data-name="Group 2548" transform="translate(.002)">
                                  <path fill="#fff" id="prefix__Path_631" d="M14.1 7.4h-2.512v-.872a2.656 2.656 0 0 0 2.233-2.619v-.558a.419.419 0 0 0-.838 0v.558a1.818 1.818 0 0 1-1.4 1.765v-.369a.7.7 0 0 0-.7-.7h-.419V3.072a1.818 1.818 0 0 0-1.465-1.78l.312-.7a.419.419 0 1 0-.765-.34l-.441 1.005H6.416L5.968.249a.419.419 0 1 0-.768.34l.312.7A1.818 1.818 0 0 0 4.05 3.072v1.535h-.419a.7.7 0 0 0-.7.7v.369a1.818 1.818 0 0 1-1.4-1.765v-.56a.419.419 0 0 0-.838 0v.558a2.656 2.656 0 0 0 2.24 2.619V7.4H.421a.419.419 0 0 0 0 .838h2.512v.871A2.656 2.656 0 0 0 .7 11.726v.558a.419.419 0 1 0 .838 0v-.558a1.818 1.818 0 0 1 1.4-1.765v.23a4.327 4.327 0 1 0 8.655 0v-.23a1.818 1.818 0 0 1 1.4 1.765v.558a.419.419 0 0 0 .838 0v-.558a2.656 2.656 0 0 0-2.233-2.619v-.871H14.1a.419.419 0 0 0 0-.838zM4.888 3.072a.978.978 0 0 1 .977-.977h2.792a.978.978 0 0 1 .977.977v1.535H4.888zm5.863 7.119a3.49 3.49 0 0 1-6.98 0V5.445h3.071v4.048a.419.419 0 1 0 .838 0V5.445h3.071z" data-name="Path 631" transform="translate(-.002)" />
                                </g>
                              </g>
                            </svg>
                          </span>
                          <label>Report an issue</label>
                        </li>
                        <li className="reportBug" onClick={() => window.open("http://support.supplymint.com/", "_blank")}>
                          {/* <img src={bugShape} /> */}
                          <span className="hund-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" id="help" width="14.501" height="14.501" viewBox="0 0 14.501 14.501">
                              <path fill="#fff" id="Path_1359" d="M14.5 9.814A4.7 4.7 0 0 0 11.068 5.3a5.537 5.537 0 1 0-10.3 3.051l-.747 2.7 2.7-.747a5.516 5.516 0 0 0 2.574.762 4.689 4.689 0 0 0 6.9 2.781l2.282.631-.628-2.278a4.671 4.671 0 0 0 .651-2.386zM2.857 9.387l-1.618.448.448-1.618-.1-.16a4.686 4.686 0 1 1 1.429 1.432zm10.405 3.875l-1.2-.332-.16.1a3.839 3.839 0 0 1-5.726-2 5.547 5.547 0 0 0 4.862-4.862 3.839 3.839 0 0 1 2 5.726l-.1.16zm0 0" className="cls-1" />
                              <path fill="#fff" id="Path_1360" d="M180.5 271h.85v.85h-.85zm0 0" className="cls-1" transform="translate(-175.388 -263.325)" />
                              <path fill="#fff" id="Path_1361" d="M138.049 91.7a.841.841 0 0 1-.277.627l-1 .913v1.037h.85v-.663l.722-.661A1.7 1.7 0 1 0 135.5 91.7h.85a.85.85 0 1 1 1.7 0zm0 0" className="cls-1" transform="translate(-131.662 -87.451)" />
                            </svg>
                          </span>
                          <label>Help & Support</label>
                        </li>
                        {/* <li className="menuLogOut" onClick={(e) => this.onLogout(e)}>
                        <span className="hund-icon">
                          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 20 19">
                            <g fill="none" fillRule="evenodd" stroke="#fff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
                              <path d="M5.402 2.5A8.183 8.183 0 0 0 10 17.5a8.183 8.183 0 0 0 4.598-15M10 1v8.25" />
                            </g>
                          </svg>
                        </span>
                        <label>
                          Logout
                        </label>
                        </li> */}
                        <li className="last-login-detail">
                          <span>
                            Last Login:
                          <span className="lld-time">
                              {sessionStorage.getItem("loginTime") != null ? sessionStorage.getItem("loginTime") : null}
                            </span>
                          </span>
                        </li>

                      </ul>}
                    {/* <ul className="dropdown-menu vendorDropDown">
                      <ul className="insideDrop">
                        <li>
                          <label>Switch Enterprise</label>
                        </li>
                        {sessionStorage.getItem('enterprises') != null ?
                          JSON.parse(sessionStorage.getItem('enterprises')).map((data, key) => (
                            <li key={key} onClick={data.active == "FALSE" ? (e) => this.switchEnterprise(data.eid) : null}>
                              <label >{data.ename}</label>
                              {data.active == "TRUE" ? <img src={activeRole} className="width-19" /> : null}
                            </li>
                          )) : null}


                      </ul>
                      <li>
                        <div className="col-md-6">
                          <button type="button" onClick={(e) => this.vendorLogout(e)}>Logout</button>
                        </div>
                        <div className="col-md-6">
                          <span>Last logged in</span>
                          <span>
                            {sessionStorage.getItem("loginTime") != null ? sessionStorage.getItem("loginTime") : null}
                          </span>
                        </div>
                      </li>
                    </ul>  */}
                  </div>
                  <ul className="list-inline">
                    <li>
                      <p className="admin_para admin-para-2">{role}</p>
                    </li>
                  </ul>
                </li>
              </ul>
            </ul>
          </div>
        </div>
        {this.state.notifyShow && <Notification {...this.state} {...this.props} resetNotifyCount={() => this.resetNotify()} CloseNotification={this.CloseNotification} />}
      </header >
        {this.state.showGlobalSearch && <GlobalSearch {...this.state} {...this.props} CloseGlobalSearch={this.CloseGlobalSearch} />}
      </div>
    );
  }
}

export default Header;
