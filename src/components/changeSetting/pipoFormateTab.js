import React from 'react';

class PiPoFormate extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            isDataSet: false,
            reset: false,
            pro_config: undefined,
            pi_data_break_level: undefined,
            po_data_break_level: undefined,
            pi_pdf_config: undefined,
            po_pdf_config: undefined
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.tab === "pdf" && Object.keys(nextProps.data).length !== 0 && (!prevState.isDataSet || prevState.reset)) {
            return {
                isDataSet: true,
                reset: false,
                pro_config: nextProps.data.PRO_CONFIG === undefined ? undefined : {...nextProps.data.PRO_CONFIG},
                pi_data_break_level: nextProps.data.PI_DATA_BREAK_LEVEL === undefined ? undefined : {...nextProps.data.PI_DATA_BREAK_LEVEL},
                po_data_break_level: nextProps.data.PO_DATA_BREAK_LEVEL === undefined ? undefined : {...nextProps.data.PO_DATA_BREAK_LEVEL},
                pi_pdf_config: nextProps.data.PI_PDF_CONFIG === undefined ? undefined : {...nextProps.data.PI_PDF_CONFIG},
                po_pdf_config: nextProps.data.PO_PDF_CONFIG === undefined ? undefined : {...nextProps.data.PO_PDF_CONFIG},
            };
        }

        return null;
    }

    save = () => {
        this.props.save({
            PRO_CONFIG: this.state.pro_config,
            PI_DATA_BREAK_LEVEL: this.state.pi_data_break_level,
            PO_DATA_BREAK_LEVEL: this.state.po_data_break_level,
            PI_PDF_CONFIG: this.state.pi_pdf_config,
            PO_PDF_CONFIG: this.state.po_pdf_config
        });
    }

    render(){
        console.log("format", this.state);
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="gen-vendor-potal-design p-lr-47 borderNone">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <h3>Search Format Settings</h3>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <button type="button" className="gen-save" onClick={this.save}>Save</button>
                                <button type="button" className="gen-clear" onClick={() => this.setState({reset: true})}>Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="gen-pi-formate">{
                        this.state.pro_config === undefined ?
                        <label>No data found!</label> :
                        <ul className="gpf-radio-list">
                            <li>
                                <label className="gen-radio-btn">
                                    <input type="radio" name="searchformate" checked={this.state.pro_config.searchBy === "startswith"} onClick={() => this.setState({pro_config: {...this.state.pro_config, searchBy: "startswith"}})} />
                                    <span className="checkmark"></span>
                                    Starts with
                                </label>
                            </li>
                            <li>
                                <label className="gen-radio-btn">
                                    <input type="radio" name="searchformate" checked={this.state.pro_config.searchBy === "contains"} onClick={() => this.setState({pro_config: {...this.state.pro_config, searchBy: "contains"}})} />
                                    <span className="checkmark"></span>
                                    Contains
                                </label>
                            </li>
                        </ul>
                        }
                        <div className="gpf-dbs m-top-30">
                            <h3>Document Breakup Settings</h3>
                            <div className="gpfdbs-piform m-top-30">
                                <h5>Purchase Indent Form</h5>{
                                    this.state.pi_data_break_level === undefined ?
                                    <label>No data found!</label> :
                                    <ul className="gpf-check-list m-top-20">{
                                        Object.keys(this.state.pi_data_break_level).map((key) =>
                                            key !== "hl1Code" && key !== "hl2Code" && key !== "hl3Code" && key !== "hl4Code" ? null :
                                            <li>
                                                <label className="checkBoxLabel0">
                                                    <input type="checkbox" checked={this.state.pi_data_break_level[key]} onChange={() => this.setState({pi_data_break_level: {...this.state.pi_data_break_level, [key]: !this.state.pi_data_break_level[key]}})} />
                                                    <span className="checkmark1"></span>
                                                    {key}
                                                </label>
                                            </li>
                                        )
                                    }</ul>
                                }
                            </div>
                            <div className="gpfdbs-piform m-top-30">
                                <h5>Purchase Order Form</h5>{
                                    this.state.po_data_break_level === undefined ?
                                    <label>No data found!</label> :
                                    <ul className="gpf-check-list m-top-20">{
                                        Object.keys(this.state.po_data_break_level).map((key) =>
                                            key !== "hl1Code" && key !== "hl2Code" && key !== "hl3Code" && key !== "hl4Code" ? null :
                                            <li>
                                                <label className="checkBoxLabel0">
                                                    <input type="checkbox" checked={this.state.po_data_break_level[key]} onChange={() => this.setState({po_data_break_level: {...this.state.po_data_break_level, [key]: !this.state.po_data_break_level[key]}})} />
                                                    <span className="checkmark1"></span>
                                                    {key}
                                                </label>
                                            </li>
                                        )
                                    }</ul>
                                }
                            </div>
                        </div>
                        <div className="pdf-config-setting m-top-30">
                            <h1>PDF Configuration Settings</h1>
                            <div className="pcs-content m-top-20">
                                <h3>Purchase Indent PDF</h3>{
                                    this.state.pi_pdf_config === undefined ?
                                    <label>No data found!</label> :
                                    <div className="pcsc-inner m-top-20">
                                        <div className="col-lg-12 col-md-12 pad-0">
                                            <h5>Set Based Configuration</h5>
                                            {
                                                Object.keys(this.state.pi_pdf_config.setBasedConfig).map((key) =>
                                                    <div className="col-lg-3 col-md-3 pad-lft-0 m-top-20">
                                                        <div className="pcsc-box">
                                                            <label>{key}</label>
                                                            <input type="text" value={this.state.pi_pdf_config.setBasedConfig[key]} onChange={(e) => this.setState({pi_pdf_config: {...this.state.pi_pdf_config, setBasedConfig: {...this.state.pi_pdf_config.setBasedConfig, [key]: e.target.value}}})} />
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        </div>
                                        <div className="col-lg-12 col-md-12 pad-0 m-top-20">
                                            <h5>Non-set Based Configuration</h5>
                                            {
                                                Object.keys(this.state.pi_pdf_config.nonSetBasedConfig).map((key) =>
                                                    <div className="col-lg-3 col-md-3 pad-lft-0 m-top-20">
                                                        <div className="pcsc-box">
                                                            <label>{key}</label>
                                                            <input type="text" value={this.state.pi_pdf_config.nonSetBasedConfig[key]} onChange={(e) => this.setState({pi_pdf_config: {...this.state.pi_pdf_config, nonSetBasedConfig: {...this.state.pi_pdf_config.nonSetBasedConfig, [key]: e.target.value}}})} />
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                }
                            </div>
                            <div className="pcs-content m-top-20">
                                <h3>Purchase Order PDF</h3>{
                                    this.state.po_pdf_config === undefined ?
                                    <label>No data found!</label> :
                                    <div className="pcsc-inner m-top-20">
                                        <div className="col-lg-12 col-md-12 pad-0">
                                            <h5>Set Based Configuration</h5>
                                            {
                                                Object.keys(this.state.po_pdf_config.setBasedConfig).map((key) =>
                                                    key === "setTableNo" || key === "tableNo" ? null :
                                                    <div className="col-lg-3 col-md-3 pad-lft-0 m-top-20">
                                                        <div className="pcsc-box">
                                                            <label>{key}</label>
                                                            <input type="text" value={this.state.po_pdf_config.setBasedConfig[key]} onChange={(e) => this.setState({po_pdf_config: {...this.state.po_pdf_config, setBasedConfig: {...this.state.po_pdf_config.setBasedConfig, [key]: e.target.value}}})} />
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        </div>
                                        <div className="col-lg-12 col-md-12 pad-0 m-top-20">
                                            <h5>Non-set Based Configuration</h5>
                                            {
                                                Object.keys(this.state.po_pdf_config.nonSetBasedConfig).map((key) =>
                                                    key === "setTableNo" || key === "tableNo" ? null :
                                                    <div className="col-lg-3 col-md-3 pad-lft-0 m-top-20">
                                                        <div className="pcsc-box">
                                                            <label>{key}</label>
                                                            <input type="text" value={this.state.po_pdf_config.nonSetBasedConfig[key]} onChange={(e) => this.setState({po_pdf_config: {...this.state.po_pdf_config, nonSetBasedConfig: {...this.state.po_pdf_config.nonSetBasedConfig, [key]: e.target.value}}})} /> 
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default PiPoFormate;