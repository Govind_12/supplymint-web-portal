import React from 'react';

class EmailScheduler extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            chooseDaysDrop: false
        }
    }

    openChooseDaysDrop(e) {
        e.preventDefault();
        this.setState({
            chooseDaysDrop: !this.state.chooseDaysDrop
        }, () => document.addEventListener('click', this.closechooseDaysDrop));
    }
    render() {
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47 border-btm">
                    <div className="gen-congiguration">
                        <div className="col-lg-6 pad-0">
                            <div className="gc-left">
                                <div className="global-search-tab gcl-tab">
                                    <ul className="nav nav-tabs gst-inner" role="tablist">
                                        <li className="nav-item active" >
                                            <a className="nav-link gsti-btn" href="#create" role="tab" data-toggle="tab">Create</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#managescheduler" role="tab" data-toggle="tab">Manage Scheduler</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gc-right">
                                <button type="button" className="gen-save">Schedule Now</button>
                                <button type="button" className="gen-clear">Clear</button>
                            </div>
                        </div> 
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="create" role="tabpanel">
                            <div className="email-schedular">
                                <div className="es-head-input">
                                    <input type="text" placeholder="Scheduler title" />
                                </div>
                                <div className="ermb-input-field">
                                    <span className="ermbif-tag"><span className="ermbift-mailid">govind@turningcloud.com</span><img src={require('../../assets/clearSearch.svg')} /></span>
                                    <span className="ermbif-tag"><span className="ermbift-mailid">amish.verma@vmart.in</span><img src={require('../../assets/clearSearch.svg')} /></span>
                                    <span className="ermbif-tag"><span className="ermbift-mailid">sujit.sijwali@gmail.com</span><img src={require('../../assets/clearSearch.svg')} /></span>
                                </div>
                                <div className="ermb-input-field">
                                    <span className="ermbif-tag"><span className="ermbift-mailid">akash.singh097@gmail.com</span><img src={require('../../assets/clearSearch.svg')} /></span>
                                    <span className="ermbif-tag"><span className="ermbift-mailid">suresh.singh@gmail.com</span><img src={require('../../assets/clearSearch.svg')} /></span>
                                </div>
                                <div className="ermb-dashdata">
                                    <h3>Frequency</h3>
                                    <div className="ermbdd-inner">
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />Daily
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />Weekly
                                                <span className="checkmark1"></span>
                                            </label>
                                            <button className="ermbdd-choosedays" onClick={(e) => this.openChooseDaysDrop(e)}>Choose Day
                                                <span className="ermbdd-down                                                                                                                                    ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="10.88" height="6.44" viewBox="0 0 10.88 6.44">
                                                        <path id="Path_867" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 867" transform="translate(-4.586 -7.586)"/>
                                                    </svg>
                                                </span>
                                            </button>
                                            {this.state.chooseDaysDrop && <div className="ermbddiw-days">
                                                <ul className="ermbddiwd-inner">
                                                    <li>Monday</li>
                                                    <li>Tuesday</li>
                                                    <li>Wednesday</li>
                                                    <li>Thursday</li>
                                                    <li>Friday</li>
                                                    <li>Saturday</li>
                                                </ul>
                                            </div>}
                                        </div>
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />Biweekly
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />Monthly
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="ermb-dashdata">
                                    <h3>Attachments</h3>
                                    <div className="ermbdd-inner">
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />Has Attachment
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="gen-pi-formate">
                                            <ul className="gpf-radio-list">
                                                <li>
                                                    <label className="gen-radio-btn">
                                                        <input type="radio" name="searchformate" />
                                                        <span className="checkmark"></span>
                                                        Line Item level
                                                    </label>
                                                </li>
                                                <li>
                                                    <label className="gen-radio-btn">
                                                        <input type="radio" name="searchformate" />
                                                        <span className="checkmark"></span>
                                                        Header Level
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="ermb-dashdata">
                                    <div className="ermbd-toggle">
                                        <h3>Dashboard Data</h3>
                                        <div className="nph-switch-btn">
                                            <label className="tg-switch" >
                                                <input type="checkbox" />
                                                <span className="tg-slider tg-round"></span>
                                                {/* <span className="nph-wbtext nph-wbtext-left">PO</span> */}
                                                <span className="nph-wbtext">PI</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="ermbdd-inner">
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />All Data
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />PI Pending for Approval
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />PI Saved as Draft
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />PI Approved
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ermbddi-checkbox">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />PI Rejected
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="ermb-add-filter">
                                    <div className="ermbaf-button">
                                        <button type="button" className="ermbaf-add-fil-btn">
                                            <span className="ermbafafb">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 12.594 12.594">
                                                    <path id="close_1_" fill="#f77647" d="M5.268 4.588l3.468-3.467A.577.577 0 0 0 7.92.3L4.453 3.773.985.3a.577.577 0 0 0-.816.816l3.468 3.472L.169 8.056a.577.577 0 1 0 .816.816L4.453 5.4 7.92 8.872a.577.577 0 0 0 .816-.816zm0 0" data-name="close (1)" transform="rotate(45 3.312 7.669)"/>
                                                </svg>
                                            </span>
                                            Add Filters
                                        </button>
                                    </div>
                                    <div className="ermb-applied-filter">
                                        <div className="show-applied-filter">
                                            <button type="button" className="saf-clear-all" onClick={(e)=>this.clearAllTag(e)}>Clear All</button>
                                                <button type="button" className="saf-btn">Date Range
                                                <img src={require('../../assets/clearSearch.svg')} />
                                                <span className="generic-tooltip">12 May 2020 - 25 May 2020</span>
                                            </button>
                                        </div>
                                        <div className="show-applied-filter">
                                            <button type="button" className="saf-btn">Plant Code
                                                <img src={require('../../assets/clearSearch.svg')} />
                                                <span className="generic-tooltip">554442</span>
                                            </button>
                                        </div>
                                        <div className="show-applied-filter">
                                            <button type="button" className="saf-btn">Division
                                                <img src={require('../../assets/clearSearch.svg')} />
                                                <span className="generic-tooltip">554442</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="managescheduler" role="tabpanel">
                            <div className="manage-schedular">
                                <div className="ms-box">
                                    <div className="msb-inner">
                                        <div className="msbi-head">
                                            <h3>Dashboard Summary Rep...</h3>
                                            <span className="asns-dot-btn"></span>
                                            <span className="msbih-mail">govind@turningcloud.com <span className="msbihm-more">+2more</span></span>
                                        </div>
                                        <div className="msbi-body">
                                            <span className="msbib-status">Active</span>
                                        </div>
                                        <div className="msbi-footer">
                                            <span className="msbif-daily">Daily</span>
                                            <span className="msbif-pi">PI</span>
                                            <span className="msbif-filter">Filter applied</span>
                                            <span className="msbif-img">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="attach_1_" width="18.526" height="18.526" data-name="attach (1)" viewBox="0 0 21.526 21.526">
                                                    <path id="Path_851" fill="#e3f8fa" d="M17.49 21.526H4.036A4.036 4.036 0 0 1 0 17.49V4.036A4.036 4.036 0 0 1 4.036 0H17.49a4.036 4.036 0 0 1 4.036 4.036V17.49a4.036 4.036 0 0 1-4.036 4.036z" data-name="Path 851"/>
                                                    <path id="Path_852" fill="#26c6da" d="M18.314 12.675v.843a2.466 2.466 0 1 1-4.933 0V12.4h-.713a.115.115 0 0 1-.076-.032l-.637-.636a.777.777 0 0 0-.555-.232H9.233A1.232 1.232 0 0 0 8 12.733v5.606a1.233 1.233 0 0 0 1.233 1.234h8.3a1.233 1.233 0 0 0 1.234-1.233v-4.71a1.246 1.246 0 0 0-.453-.955z" data-name="Path 852" transform="translate(-2.618 -3.764)"/>
                                                    <path id="Path_853" fill="#8ce1eb" d="M18.9 14.433a1.572 1.572 0 0 1-1.569-1.569v-2.355a1.009 1.009 0 1 1 2.018 0v2.242a.336.336 0 1 1-.673 0v-2.242a.336.336 0 0 0-.673 0v2.354a.9.9 0 0 0 1.793 0V9.836a.336.336 0 1 1 .673 0v3.027a1.572 1.572 0 0 1-1.569 1.57z" data-name="Path 853" transform="translate(-5.673 -3.109)"/>
                                                </svg>
                                                <span className="generic-tooltip">Image</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="ms-box">
                                    <div className="msb-inner">
                                        <div className="msbi-head">
                                            <h3>Digiproc Dashboard Sum…</h3>
                                            <span className="asns-dot-btn"></span>
                                            <span className="msbih-mail">amish.verma@gmail.com <span className="msbihm-more">+2more</span></span>
                                        </div>
                                        <div className="msbi-body">
                                            <span className="msbib-status">Active</span>
                                        </div>
                                        <div className="msbi-footer">
                                            <span className="msbif-daily">Daily</span>
                                            <span className="msbif-pi">PI</span>
                                            <span className="msbif-filter">Filter applied</span>
                                            <span className="msbif-img">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="attach_1_" width="18.526" height="18.526" data-name="attach (1)" viewBox="0 0 21.526 21.526">
                                                    <path id="Path_851" fill="#e3f8fa" d="M17.49 21.526H4.036A4.036 4.036 0 0 1 0 17.49V4.036A4.036 4.036 0 0 1 4.036 0H17.49a4.036 4.036 0 0 1 4.036 4.036V17.49a4.036 4.036 0 0 1-4.036 4.036z" data-name="Path 851"/>
                                                    <path id="Path_852" fill="#26c6da" d="M18.314 12.675v.843a2.466 2.466 0 1 1-4.933 0V12.4h-.713a.115.115 0 0 1-.076-.032l-.637-.636a.777.777 0 0 0-.555-.232H9.233A1.232 1.232 0 0 0 8 12.733v5.606a1.233 1.233 0 0 0 1.233 1.234h8.3a1.233 1.233 0 0 0 1.234-1.233v-4.71a1.246 1.246 0 0 0-.453-.955z" data-name="Path 852" transform="translate(-2.618 -3.764)"/>
                                                    <path id="Path_853" fill="#8ce1eb" d="M18.9 14.433a1.572 1.572 0 0 1-1.569-1.569v-2.355a1.009 1.009 0 1 1 2.018 0v2.242a.336.336 0 1 1-.673 0v-2.242a.336.336 0 0 0-.673 0v2.354a.9.9 0 0 0 1.793 0V9.836a.336.336 0 1 1 .673 0v3.027a1.572 1.572 0 0 1-1.569 1.57z" data-name="Path 853" transform="translate(-5.673 -3.109)"/>
                                                </svg>
                                                <span className="generic-tooltip">Image</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="ms-box">
                                    <div className="msb-inner">
                                        <div className="msbi-head">
                                            <h3>Scheduler #3</h3>
                                            <span className="asns-dot-btn"></span>
                                            <span className="msbih-mail">amish.verma@gmail.com <span className="msbihm-more">+5more</span></span>
                                        </div>
                                        <div className="msbi-body">
                                            <span className="msbib-status">Active</span>
                                        </div>
                                        <div className="msbi-footer">
                                            <span className="msbif-daily">Monthly</span>
                                            <span className="msbif-pi">PI</span>
                                            <span className="msbif-filter">Filter applied</span>
                                            <span className="msbif-img">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="attach_1_" width="18.526" height="18.526" data-name="attach (1)" viewBox="0 0 21.526 21.526">
                                                    <path id="Path_851" fill="#e3f8fa" d="M17.49 21.526H4.036A4.036 4.036 0 0 1 0 17.49V4.036A4.036 4.036 0 0 1 4.036 0H17.49a4.036 4.036 0 0 1 4.036 4.036V17.49a4.036 4.036 0 0 1-4.036 4.036z" data-name="Path 851"/>
                                                    <path id="Path_852" fill="#26c6da" d="M18.314 12.675v.843a2.466 2.466 0 1 1-4.933 0V12.4h-.713a.115.115 0 0 1-.076-.032l-.637-.636a.777.777 0 0 0-.555-.232H9.233A1.232 1.232 0 0 0 8 12.733v5.606a1.233 1.233 0 0 0 1.233 1.234h8.3a1.233 1.233 0 0 0 1.234-1.233v-4.71a1.246 1.246 0 0 0-.453-.955z" data-name="Path 852" transform="translate(-2.618 -3.764)"/>
                                                    <path id="Path_853" fill="#8ce1eb" d="M18.9 14.433a1.572 1.572 0 0 1-1.569-1.569v-2.355a1.009 1.009 0 1 1 2.018 0v2.242a.336.336 0 1 1-.673 0v-2.242a.336.336 0 0 0-.673 0v2.354a.9.9 0 0 0 1.793 0V9.836a.336.336 0 1 1 .673 0v3.027a1.572 1.572 0 0 1-1.569 1.57z" data-name="Path 853" transform="translate(-5.673 -3.109)"/>
                                                </svg>
                                                <span className="generic-tooltip">Image</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="ms-box">
                                    <div className="msb-inner">
                                        <div className="msbi-head">
                                            <h3>Scheduler #4</h3>
                                            <span className="asns-dot-btn"></span>
                                            <span className="msbih-mail">govind@turningcloud.com <span className="msbihm-more">+2more</span></span>
                                        </div>
                                        <div className="msbi-body">
                                            <span className="msbib-status">Active</span>
                                        </div>
                                        <div className="msbi-footer">
                                            <span className="msbif-daily">Daily</span>
                                            <span className="msbif-pi">PI</span>
                                            <span className="msbif-filter">Filter applied</span>
                                            <span className="msbif-img">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="attach_1_" width="18.526" height="18.526" data-name="attach (1)" viewBox="0 0 21.526 21.526">
                                                    <path id="Path_851" fill="#e3f8fa" d="M17.49 21.526H4.036A4.036 4.036 0 0 1 0 17.49V4.036A4.036 4.036 0 0 1 4.036 0H17.49a4.036 4.036 0 0 1 4.036 4.036V17.49a4.036 4.036 0 0 1-4.036 4.036z" data-name="Path 851"/>
                                                    <path id="Path_852" fill="#26c6da" d="M18.314 12.675v.843a2.466 2.466 0 1 1-4.933 0V12.4h-.713a.115.115 0 0 1-.076-.032l-.637-.636a.777.777 0 0 0-.555-.232H9.233A1.232 1.232 0 0 0 8 12.733v5.606a1.233 1.233 0 0 0 1.233 1.234h8.3a1.233 1.233 0 0 0 1.234-1.233v-4.71a1.246 1.246 0 0 0-.453-.955z" data-name="Path 852" transform="translate(-2.618 -3.764)"/>
                                                    <path id="Path_853" fill="#8ce1eb" d="M18.9 14.433a1.572 1.572 0 0 1-1.569-1.569v-2.355a1.009 1.009 0 1 1 2.018 0v2.242a.336.336 0 1 1-.673 0v-2.242a.336.336 0 0 0-.673 0v2.354a.9.9 0 0 0 1.793 0V9.836a.336.336 0 1 1 .673 0v3.027a1.572 1.572 0 0 1-1.569 1.57z" data-name="Path 853" transform="translate(-5.673 -3.109)"/>
                                                </svg>
                                                <span className="generic-tooltip">Image</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default EmailScheduler;