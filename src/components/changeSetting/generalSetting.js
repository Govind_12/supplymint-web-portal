import React from 'react';

class GeneralSetting extends React.Component {

    render() {
        return (
            <div className="col-lg-12 col-md-12 p-lr-47">
                <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                    <div className="col-md-3 col-sm-3 manage-pad">
                        <div className="change-setting-box">
                            <h4>Number Formate</h4>
                            {/* <div className="col-md-7 pad-right-0">
                                <p className="displayInline m-rgt-5">Current Format </p>
                                <button type="button" className="greenBlue verticalMid pointerNone">{this.props.currentNumberFormat}</button>
                            </div> */}
                            <div className="csb-drop">
                                <select id="numberFormat" onChange={(e) => this.props.handleChange(e)} value={this.props.numberFormat}>
                                    <option value="CUSTOM">Custom</option>
                                    <option value="TEXT NUMBER">Plain Text</option>
                                </select>
                            {this.props.numberFormat != "TEXT NUMBER" ?
                            <div className="csb-drop">
                                <label>Decimal Places</label>
                                <select id="decimalPlaces" onChange={(e) => this.props.handleChange(e)} value={this.props.decimalPlaces}>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>: null}
                                <div className="csb-check m-top-20">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked={this.props.seperator} onChange={(e) => this.props.handleChange(e)} id="seperator" />Use 1000 Seperator ( , ) 
                                        <span className="checkmark1"></span> 
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-3 manage-pad">
                        <div className="change-setting-box">
                            <h4>Currency</h4>
                            {/* <div className="col-md-7 pad-right-0">
                                <p className="displayInline m-rgt-5">Current Format </p>
                                <button type="button" className="greenBlue verticalMid pointerNone">{this.props.currentCurrency}</button>
                            </div> */}
                            <div className="csb-drop">
                                <select id="currency" onChange={(e) => this.props.handleChange(e)} value={this.props.currency}>
                                    <option value="INR">INR ₹</option>
                                    <option value="USD">USD $</option>
                                    <option value="CAD">CAD $</option>
                                    <option value="EUR">EUR €</option>
                                </select>
                            </div>
                            <div className="csb-check m-top-20">
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" checked={this.props.symbol} onChange={(e) => this.props.handleChange(e)} id="symbol" />Use symbols 
                                    <span className="checkmark1"></span>
                                </label>
                            </div>
                            <p>E.g &#8377; 0.00</p>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-3 manage-pad">
                        <div className="change-setting-box">
                            <h4>Time</h4>
                            {/* <div className="col-md-7 pad-right-0">
                                <p className="displayInline m-rgt-5">Current Format </p>
                                <button type="button" className="greenBlue verticalMid pointerNone">{this.props.currentTimeFormat}</button>
                            </div> */}
                            <div className="csb-drop">
                                <select id="timeFormat" onChange={(e) => this.props.handleChange(e)} value={this.props.timeFormat}>
                                    <option value="12 Hour">12 Hour</option>
                                    <option value="24 Hour">24 Hour</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-3 manage-pad">
                        <div className="change-setting-box">
                            <h4>Date</h4>
                            {/* <div className="col-md-9 pad-lft-0 textRight">
                                <p className="displayInline m-rgt-5">Current Format </p>
                                <button type="button" className="greenBlue verticalMid pointerNone widthAuto minWid89">{this.props.currentDateFormat}</button>
                            </div> */}
                            <div className="csb-drop">
                                <select id="dateFormat" onChange={(e) => this.props.handleChange(e)} value={this.props.dateFormat}>
                                    <option value="dd MMMM yyyy">dd MMMM yyyy</option>
                                    <option value="dd MMM yyyy">dd MMM yyyy</option>
                                    <option value="dd-MM-yyyy">dd-MM-yyyy</option>
                                    <option value="yyyy-MM-dd">yyyy-MM-dd</option>
                                    <option value="yyyy/MM/dd">yyyy/MM/dd</option>
                                    <option value="yyyy-MM-dd HH:mm">yyyy-MM-dd HH:mm</option>
                                    <option value="yyyy/MM/dd HH:mm">yyyy/MM/dd HH:mm</option>
                                    <option value="dd MMMM yyyy HH:mm">dd MMMM yyyy HH:mm</option>
                                    <option value="dd MMM yyyy HH:mm">dd MMM yyyy HH:mm</option>
                                    <option value="dd-MM-yyyy HH:mm">dd-MM-yyyy HH:mm</option>
                                    <option value="dd MMM yyyy 'AT' HH:mm">dd MMM yyyy 'AT' HH:mm</option>
                                    <option value="E MMM dd HH:mm">E MMM dd HH:mm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default GeneralSetting;