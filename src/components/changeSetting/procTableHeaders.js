import React from 'react';

class ProcurementTableHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pipo: "piHeaders",

            isDataSet: false,
            reset: false,
            custom: {},
            default: {},
            fixed: {},
            defaultObjects: {},
            customObjects: {}
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if ((nextProps.tab === "piHeaders" || nextProps.tab === "poHeaders") && Object.keys(nextProps.data).length !== 0 && (!prevState.isDataSet || prevState.reset || prevState.pipo != nextProps.tab)) {
            let detail = nextProps.tab === "piHeaders" ? "piDetail" : "poDetails";
            let queue = [{[detail]: nextProps.data["Default Headers"][detail]}];
            let defaultObjects = {[detail]: nextProps.data["Default Headers"][detail]};
            let customObjects = {[detail]: nextProps.data["Custom Headers"] === undefined || Object.keys(nextProps.data["Custom Headers"]).length === 0 ? nextProps.data["Default Headers"][detail] : nextProps.data["Custom Headers"][detail]}
            while (queue.length !== 0) {
                let current = queue.pop();
                let currentKey = Object.keys(current)[0];
                let currentValue = Object.values(current)[0];
                Object.keys(currentValue).forEach((key) => {
                    if (typeof currentValue[key] == "object") {
                        queue.push({[key]: currentValue[key]});
                        defaultObjects[key] = currentValue[key];
                        customObjects[key] = currentValue[key];
                    }
                });
            }
            return {
                isDataSet: true,
                reset: false,
                custom: nextProps.data["Custom Headers"] === undefined ? {} : Object.keys(nextProps.data["Custom Headers"]).length === 0 ? {...nextProps.data["Default Headers"]} : {...nextProps.data["Custom Headers"]},
                default: nextProps.data["Default Headers"] === undefined ? {} : {...nextProps.data["Default Headers"]},
                fixed: nextProps.data["Fixed Headers"] === undefined ? {} : {...nextProps.data["Fixed Headers"]},
                customObjects: customObjects,
                defaultObjects: defaultObjects,
                pipo: nextProps.tab
            };
        }

        return null;
    }

    openBulkAction(e) {
        e.preventDefault();
        this.setState({
            showBulkAction: !this.state.showBulkAction
        });
    }

    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent"))){
            this.setState({ showBulkAction: false })
        }
    }

    renderData = () => {
        let returnData = [];
        if (this.state.customObjects != null && this.state.customObjects !== undefined) {
            Object.keys(this.state.customObjects).forEach((key) => {
                Object.keys(this.state.customObjects[key]).forEach((innerKey) => {
                    if (typeof this.state.defaultObjects[key][innerKey] != "object") {
                        returnData.push(
                            <tr>
                                <td className="border-lft"><label className="bold">{this.state.defaultObjects[key][innerKey]}</label></td>
                                <td><input type="text" className="modal_tableBox " value={this.state.customObjects[key][innerKey] === undefined ? "" : this.state.customObjects[key][innerKey]}
                                    onChange={(e) => {
                                        this.setState({customObjects: {...this.state.customObjects, [key]: {...this.state.customObjects[key], [innerKey]: e.target.value}}})
                                    }} /></td>
                            </tr>
                        );
                    }
                });
            });
        }
        return returnData;
    }

    save = () => {
        let detail = this.state.pipo === "piHeaders" ? "piDetail" : "poDetails";
        let queue = [{[detail]: this.state.custom[detail]}];
        while (queue.length !== 0) {
            let current = queue.pop();
            let currentKey = Object.keys(current)[0];
            let currentValue = Object.values(current)[0];
            Object.keys(currentValue).forEach((key) => {
                if (typeof currentValue[key] == "object") {
                    queue.push({[key]: currentValue[key]});
                }
                else {
                    currentValue[key] = this.state.customObjects[currentKey][key]
                }
            });
        }
        let payload = {
            module: "PROCUREMENT",
            subModule: this.state.pipo === "piHeaders" ? "PURCHASE INDENT" : "PURCHASE ORDER",
            section: "HISTORY",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: this.state.pipo === "piHeaders" ? "PI_TABLE_HEADER" : "PO_TABLE_HEADER",
            displayName: "TABLE HEADER",
            fixedHeaders: this.state.fixed,
            defaultHeaders: this.state.default,
            customHeaders: this.state.custom
        }
        this.props.save(payload);
    }

    changePiPo = () => {
        this.state.pipo === "piHeaders" ? this.props.setHeaders("poHeaders") : this.props.setHeaders("piHeaders");
        this.setState({
            pipo: this.state.pipo === "piHeaders" ? "poHeaders" : "piHeaders"
        });
    }

    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="gen-congiguration">
                        <div className="col-lg-6 pad-0">
                            <div className="gc-left">
                                <div className="global-search-tab gcl-tab">
                                    <ul className="nav nav-tabs gst-inner" role="tablist">
                                        <li className="nav-item active" >
                                            <a className="nav-link gsti-btn" href="#pi" role="tab" data-toggle="tab">{this.state.pipo === "piHeaders" ? "Purhcase Indent Table" : "Purhcase Order Table"}</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#piDetail" role="tab" data-toggle="tab">{this.state.pipo === "piHeaders" ? "Purhcase Indent Details Table" : "Purhcase Order Details Table"}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gc-right">
                                <div className="nph-switch-btn">
                                    <label className="tg-switch tg-switch2" >
                                        <input type="checkbox" checked={this.state.pipo === "poHeaders"} onChange={this.changePiPo} />
                                        <span className="tg-slider tg-round"></span>
                                        {this.state.pipo === "piHeaders" ? <span className="nph-wbtext">PI</span> : <span className="nph-wbtext nph-wbtextset">PO</span>}
                                    </label>
                                </div>
                                <button type="button" className="gen-save" onClick={this.save}>Save</button>
                                <button type="button" className="gen-clear" onClick={() => this.setState({reset: true})}>Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tab-content">
                    <div className="tab-pane fade in active" id="pi" role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="gen-item-udf-table">
                                <div className="giut-headfix" id="table-scroll">
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label>Default Header Name</label>
                                                    <img src={require('../../assets/headerFilter.svg')} /> 
                                                </th>
                                                <th>
                                                    <label>Current Header Name</label>
                                                    <img src={require('../../assets/headerFilter.svg')} /> 
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>{
                                            this.state.pipo === "piHeaders" ?
                                            this.state.default.pi !== undefined && Object.keys(this.state.default.pi).length !== 0 ?
                                            Object.keys(this.state.default.pi).map((key) =>
                                                <tr>
                                                    <td className="border-lft"><label className="bold">{this.state.default.pi[key]}</label></td>
                                                    <td><input type="text" className="modal_tableBox " value={this.state.custom.pi[key] === undefined ? "" : this.state.custom.pi[key]} onChange={(e) => this.setState({custom: {...this.state.custom, pi: {...this.state.custom.pi, [key]: e.target.value}}})} /></td>
                                                </tr>
                                            ) :
                                            <tr><td><label>No headers found!</label></td></tr> :
                                            this.state.default.PO !== undefined && Object.keys(this.state.default.PO).length !== 0 ?
                                            Object.keys(this.state.default.PO).map((key) =>
                                                <tr>
                                                    <td className="border-lft"><label className="bold">{this.state.default.PO[key]}</label></td>
                                                    <td><input type="text" className="modal_tableBox " value={this.state.custom.PO[key] === undefined ? "" : this.state.custom.PO[key]} onChange={(e) => this.setState({custom: {...this.state.custom, PO: {...this.state.custom.PO, [key]: e.target.value}}})} /></td>
                                                </tr>
                                            ) :
                                            <tr><td><label>No headers found!</label></td></tr>
                                        }</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="piDetail" role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="gen-item-udf-table">
                                <div className="giut-headfix" id="table-scroll">
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label>Default Header Name</label>
                                                    <img src={require('../../assets/headerFilter.svg')} /> 
                                                </th>
                                                <th>
                                                    <label>Current Header Name</label>
                                                    <img src={require('../../assets/headerFilter.svg')} /> 
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>{
                                            this.renderData()
                                        }</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProcurementTableHeader;