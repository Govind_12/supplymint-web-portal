import React from 'react';
import exclaimationIcon from '../../assets/exclamation-summary.svg';
import enterIcon from '../../assets/enterIcon.svg';

class NotificationSetting extends React.Component {
    getFocus(e) {
        document.getElementById("too").focus()
    }
    getFocuscc(e) {
        document.getElementById("ccc").focus()
    }
    render() {
        return (
            <div className="notification-main-page">
                <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                    <div className="nmp-head p-lr-47">
                        <div className="nph-switch-btn">
                            <label className="tg-switch" >
                                <input type="checkbox" checked={this.props.emailStatus == "Active" ? "checked" : ""} onChange={(e) => this.props.onEmailStatus(e)} type="checkbox" id="myDiv" />
                                <span className="tg-slider tg-round"></span>
                            </label>
                        </div>
                        {this.props.emailStatus != "Active" ?  
                        <div className="nmph-text">
                            <h3>Setting Disabled</h3>
                            <p>Enable it to customize the setting</p>
                        </div> :
                        <div className="nmph-text">
                            <h3>Setting Enabled</h3>
                            <p>Email notification in enable now. You can now customize the settings.</p>
                        </div>}
                    </div>
                </div>
                <div className="col-md-8 col-sm-12 m-top-30 p-lr-47">
                    {this.props.emailStatus != "Active" ? 
                    <div className="nmph-notifi-disabled">
                        <div className="nmphnd-img">
                            <img src={require('../../assets/disabled-notification.svg')} />
                        </div>
                        <div className="nmphnd-text m-top-30">
                            <h3>Email Notification is Disabled !</h3>
                            <p>Please enable settings by clicking on above toggle button</p>
                        </div>
                    </div>:
                    <div className="nmph-bottom">
                        <div className="nmphb-head">
                            <h4>New Notification</h4>
                            <div className="col-md-4 pad-lft-0">
                                <div className="nmphbh-drop">
                                    <label>Module</label>
                                    <select value={this.props.selectedModule} id="module" onChange={(e) => this.props.handleChange(e)}>
                                        <option value="">Select Module</option>
                                        {this.props.module.length == 0 ? null :
                                            this.props.module.map((data, key) => (
                                                <option key={key} value={data.key}>{data.value}</option>
                                            ))}
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-4 pad-lft-0">
                                <div className="nmphbh-drop">
                                    <label>Functional module</label>
                                    <select value={this.props.selectedSubModule} id="subModule" onChange={(e) => this.props.handleChange(e)}>
                                        <option value="">Select Sub-Module</option>
                                        {this.props.subModule.length == 0 ? null : this.props.subModule.map((data, key) => (<option key={key} value={data.key}>{data.value}</option>))}
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-4 pad-lft-0">
                                <div className="nmphbh-drop">
                                    <label>Configuration Property</label>
                                    <select value={this.props.selectedProperty} id="property" onChange={(e) => this.props.handleChange(e)}>
                                        <option value="">Select Property</option>
                                        {this.props.property.length == 0 ? null : this.props.property.map((data, key) => (<option key={key} value={data.key}>{data.value}</option>))}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="nmph-body">
                            <div className="addEmails nmphb-inner m-top-20">
                                <h4>To</h4>
                                <form id="to" onSubmit={(e) => this.props.addToCc(e)}>
                                    <ul>
                                        <div onClick={(e) => this.getFocus(e)} className={this.props.selectedProperty == "" ? "emails" : "emails visibleEmails"}>
                                            {this.props.to.length == 0 ? null : this.props.to.map((data, key) => (
                                                <li key={key}>{data}<span onClick={(e) => this.props.removeTo(`${data}`)}>&#10005;</span></li>))}
                                            <li>
                                                <input type="text" className="addEmailFocus" value={this.props.addto} disabled={this.props.selectedProperty == "" ? true : false} id="too" onChange={(e) => this.props.handleChange(e)} onKeyDown={(e) => {this.props.tabPressed(e)}}></input>
                                            </li>
                                        </div>
                                    </ul>
                                </form>
                            </div>
                            <div className="addEmails nmphb-inner m-top-20">
                                <h4>CC</h4>
                                <form id="cc" onSubmit={(e) => this.props.addToCc(e)}>
                                    <ul>
                                        <div onClick={(e) => this.getFocuscc(e)} className={this.props.selectedProperty == "" ? "emails" : "emails visibleEmails"}>
                                            {this.props.cc.length == 0 ? null : this.props.cc.map((data, key) => (
                                                <li key={key}>{data}<span onClick={(e) => this.props.removeCc(`${data}`)}>&#10005;</span></li>))}
                                            <li><input value={this.props.addcc} id="ccc" disabled={this.props.selectedProperty == "" ? true : false} onChange={(e) => this.props.handleChange(e)} onKeyDown={(e) => {this.props.tabPressed(e)}} type="text"></input></li>
                                        </div>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <div className="nmph-footer m-top-30">
                            <h5>Note:</h5>
                            <p>Above selected modules are now enable for Email Notification for authorised users</p>
                        </div>
                    </div>}
                </div>
            </div>
        );
    }
}
export default NotificationSetting;