import React from 'react';
import Pagination from '../pagination';

class KeyValidation extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            exportToExcel: false,
            filter: false,
            filterBar: false,

            isDataSet: false,
            reset: false,
            search: "",
            displayNames: {},
            custom_pi: {},
            custom_po: {}
        }
    }
 
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.tab === "pipo" && Object.keys(nextProps.data).length !== 0 && (!prevState.isDataSet || prevState.reset)) {
            let displayNames = {};
            nextProps.data.CUSTOM_PI === undefined ? "" : nextProps.data.CUSTOM_PI.forEach((item) =>
                Object.keys(item)[0] === "displayName" ?
                displayNames[Object.keys(item)[1]] = Object.values(item)[0] :
                displayNames[Object.keys(item)[0]] = Object.values(item)[1]
            )

            let custom_pi = {};
            nextProps.data.CUSTOM_PI === undefined ? "" : nextProps.data.CUSTOM_PI.forEach((item) =>
                Object.keys(item)[0] === "displayName" ?
                custom_pi[Object.keys(item)[1]] = Object.values(item)[1] :
                custom_pi[Object.keys(item)[0]] = Object.values(item)[0]
            )

            let custom_po = {};
            nextProps.data.CUSTOM_PO === undefined ? "" : nextProps.data.CUSTOM_PO.forEach((item) =>
                Object.keys(item)[0] === "displayName" ?
                custom_po[Object.keys(item)[1]] = Object.values(item)[1] :
                custom_po[Object.keys(item)[0]] = Object.values(item)[0]
            )
            return {
                isDataSet: true,
                displayNames,
                custom_pi,
                custom_po,
                reset: false
            }
        }

        return null;
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () => {
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
            
        });
    }

    save = () => {
        this.props.save({
            CUSTOM_PI: this.state.custom_pi,
            CUSTOM_PO: this.state.custom_po
        });
    }

    render(){
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type To Search" onChange={(e) => this.setState({search: e.target.value})} />
                                    <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                    {this.state.search == "" ? null : <span className="closeSearch"><img src={require('../../assets/clearSearch.svg')} onClick={() => this.setState({search: ""})} /></span>}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <button type="button" className="gen-save" onClick={this.save}>Save</button>
                                <button type="button" className="gen-clear" onClick={() => this.setState({reset: true})}>Reset</button>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                    {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button">
                                                    {/* <img src={ExportExcel} /> */}
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                            <li>
                                                <button className="export-excel" type="button">
                                                    <span className="pi-export-svg">
                                                        <img src={require('../../assets/downloadAll.svg')} />
                                                    </span>
                                                Download All</button>
                                            </li>
                                        </ul>}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                    {/* {this.state.filter && <VendorFilter />} */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-lr-47">
                    <div className="vendor-gen-table" >
                        <div className="manage-table" id="scrollDiv">
                            <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div>
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn">
                                        </th>
                                        <th><label>Key Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Display Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>PI Validation</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        {/* <th><label>PI Restrictions</label><img src={require('../../assets/headerFilter.svg')} /></th> */}
                                        <th><label>PO Validation</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        {/* <th><label>Display Required on PO</label><img src={require('../../assets/headerFilter.svg')} /></th> */}
                                        {/* <th><label>Display Required on PO</label><img src={require('../../assets/headerFilter.svg')} /></th> */}
                                    </tr>
                                </thead>
                                <tbody>{
                                    Object.keys(this.state.displayNames).length == 0 ?
                                    <tr><td><label>No keys found!</label></td></tr> :
                                    Object.keys(this.state.displayNames).map((key) =>
                                        key.toLowerCase().includes(this.state.search) ?
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner">
                                                        <label className="checkBoxLabel0">
                                                            <input type="checkBox" name="selectEach" />
                                                            <span className="checkmark1"></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td><label className="bold">{key}</label></td>
                                            <td><input type="text" className="vgt-display-name" value={this.state.displayNames[key]} disabled /></td>
                                            <td>
                                                <div className="vgdt-toggle">
                                                    <div className="nph-switch-btn">
                                                        <label className="tg-switch" >
                                                            <input type="checkbox" checked={this.state.custom_pi[key]} onChange={() => this.setState({custom_pi: {...this.state.custom_pi, [key]: !this.state.custom_pi[key]}})} />
                                                            <span className="tg-slider tg-round"></span>
                                                            {this.state.custom_pi[key] ? <span className="nph-wbtext nph-wbtext-left">On</span> : <span className="nph-wbtext">Off</span>}
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="vgdt-toggle">
                                                    <div className="nph-switch-btn">
                                                        <label className="tg-switch" >
                                                            <input type="checkbox" checked={this.state.custom_po[key]} onChange={() => this.setState({custom_po: {...this.state.custom_po, [key]: !this.state.custom_po[key]}})} />
                                                            <span className="tg-slider tg-round"></span>
                                                            {this.state.custom_po[key] ? <span className="nph-wbtext nph-wbtext-left">On</span> : <span className="nph-wbtext">Off</span>}
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr> :
                                        null
                                    )
                                    
                                }</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default KeyValidation;