import React from 'react';
import Pagination from '../pagination';

class CatDescUdfTab extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isapparel: true,
            catDescInitial: [],
            itemUdfInitial: [],
            setUdfInitial: [],
            catDescVal: [],
            itemUdfVal: [],
            setUdfVal: [],
            catDescPayload: [],
            itemUdfPayload: [],
            setUdfPayload: [],
            tabType: 'catdesc',
        }
    }

    componentDidMount(){
        this.props.getGeneralMappingRequest(this.state.isapparel)
    }

    static getDerivedStateFromProps = (props, state) => {
        if(props.changeSetting.getGeneralMapping.isSuccess){
            props.getGeneralMappingClear();
            if(props.changeSetting.getGeneralMapping.data.resource != null){
                return{
                    catDescVal: props.changeSetting.getGeneralMapping.data.resource.catDesc,
                    itemUdfVal: props.changeSetting.getGeneralMapping.data.resource.itemUdf,
                    setUdfVal: props.changeSetting.getGeneralMapping.data.resource.setUdf,
                    catDescInitial: props.changeSetting.getGeneralMapping.data.resource.catDesc,
                    itemUdfInitial: props.changeSetting.getGeneralMapping.data.resource.itemUdf,
                    setUdfInitial: props.changeSetting.getGeneralMapping.data.resource.setUdf,
                }
            } else {
                return {
                    itemUdfVal: [],
                    catDescVal: [],
                    setUdfVal: [],
                    catDescInitial: [],
                    itemUdfInitial: [],
                    setUdfInitial: [],
                }
            }
        } 
        if(props.changeSetting.updateGeneralMapping.isSuccess){
            return {
                catDescPayload: [],
                itemUdfPayload: [],
                setUdfPayload: [],
            }
        }        
    }

    updateCatDesc = (e, data, index) => {
        let catVal = [...this.state.catDescVal];
        let catPayload = [...this.state.catDescPayload]
        if(e.target.id == 'required') {
            let required = data.required == 'true' ? 'false' : 'true'
            catVal[index] = {...data, required: required}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, required: required})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, required: required}
                    } else {
                        return catp
                    }
                })
            }
        } else if(e.target.id == 'displayPi') {
            let displayPi = data.displayPi == 'true' ? 'false' : 'true'
            catVal[index] = {...data, displayPi: displayPi}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, displayPi: displayPi})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, displayPi: displayPi}
                    } else {
                        return catp
                    }
                })
            }
        } else if(e.target.id == 'isCompulsoryPI') {
            let isCompulsoryPI = data.isCompulsoryPI == 'true' ? 'false' : 'true'
            catVal[index] = {...data, isCompulsoryPI: isCompulsoryPI}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, isCompulsoryPI: isCompulsoryPI})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, isCompulsoryPI: isCompulsoryPI}
                    } else {
                        return catp
                    }
                })
            }
        } else if(e.target.id == 'isLovPi') {
            let isLovPi = data.isLovPi == 'true' ? 'false' : 'true'
            catVal[index] = {...data, isLovPi: isLovPi}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, isLovPi: isLovPi})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, isLovPi: isLovPi}
                    } else {
                        return catp
                    }
                })
            }
        } else if(e.target.id == 'isValidationPi') {
            let isValidationPi = data.isValidationPi == 'true' ? 'false' : 'true'
            catVal[index] = {...data, isValidationPi: isValidationPi}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, isValidationPi: isValidationPi})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, isValidationPi: isValidationPi}
                    } else {
                        return catp
                    }
                })
            }
        } else if(e.target.id == 'displayPo') {
            let displayPo = data.displayPo == 'true' ? 'false' : 'true'
            catVal[index] = {...data, displayPo: displayPo}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, displayPo: displayPo})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, displayPo: displayPo}
                    } else {
                        return catp
                    }
                })
            }
        } else if(e.target.id == 'isCompulsoryPO') {
            let isCompulsoryPO = data.isCompulsoryPO == 'true' ? 'false' : 'true'
            catVal[index] = {...data, isCompulsoryPO: isCompulsoryPO}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, isCompulsoryPO: isCompulsoryPO})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, isCompulsoryPO: isCompulsoryPO}
                    } else {
                        return catp
                    }
                })
            }
        } else if(e.target.id == 'isLovPo') {
            let isLovPo = data.isLovPo == 'true' ? 'false' : 'true'
            catVal[index] = {...data, isLovPo: isLovPo}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, isLovPo: isLovPo})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, isLovPo: isLovPo}
                    } else {
                        return catp
                    }
                })
            }
        } else if(e.target.id == 'isValidationPo') {
            let isValidationPo = data.isValidationPo == 'true' ? 'false' : 'true'
            catVal[index] = {...data, isValidationPo: isValidationPo}
            if(catPayload.length == 0 || catPayload.filter(cat => cat.catDescUdfType == data.catDescUdfType).length == 0){
                catPayload.push({...data, isValidationPo: isValidationPo})
            } else {
                catPayload = catPayload.map(catp => {
                    if(catp.catDescUdfType == data.catDescUdfType){
                        return {...data, isValidationPo: isValidationPo}
                    } else {
                        return catp
                    }
                })
            }
        }
        this.setState({
            catDescVal: catVal,
            catDescPayload: catPayload
        })
    }

    updateItemUdf = (e, data, index) => {
        let itemVal = [...this.state.itemUdfVal];
        let itemPayload = [...this.state.itemUdfPayload]
        if(e.target.id == 'required') {
            let required = data.required == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, required: required}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, required: required})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, required: required}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'displayPi') {
            let displayPi = data.displayPi == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, displayPi: displayPi}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, displayPi: displayPi})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, displayPi: displayPi}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isCompulsoryPI') {
            let isCompulsoryPI = data.isCompulsoryPI == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, isCompulsoryPI: isCompulsoryPI}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, isCompulsoryPI: isCompulsoryPI})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isCompulsoryPI: isCompulsoryPI}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isLovPi') {
            let isLovPi = data.isLovPi == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, isLovPi: isLovPi}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, isLovPi: isLovPi})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isLovPi: isLovPi}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isValidationPi') {
            let isValidationPi = data.isValidationPi == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, isValidationPi: isValidationPi}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, isValidationPi: isValidationPi})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isValidationPi: isValidationPi}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'displayPo') {
            let displayPo = data.displayPo == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, displayPo: displayPo}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, displayPo: displayPo})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, displayPo: displayPo}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isCompulsoryPO') {
            let isCompulsoryPO = data.isCompulsoryPO == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, isCompulsoryPO: isCompulsoryPO}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, isCompulsoryPO: isCompulsoryPO})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isCompulsoryPO: isCompulsoryPO}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isLovPo') {
            let isLovPo = data.isLovPo == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, isLovPo: isLovPo}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, isLovPo: isLovPo})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isLovPo: isLovPo}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isValidationPo') {
            let isValidationPo = data.isValidationPo == 'true' ? 'false' : 'true'
            itemVal[index] = {...data, isValidationPo: isValidationPo}
            if(itemPayload.length == 0 || itemPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                itemPayload.push({...data, isValidationPo: isValidationPo})
            } else {
                itemPayload = itemPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isValidationPo: isValidationPo}
                    } else {
                        return itemP
                    }
                })
            }
        } 
        this.setState({
            itemUdfVal: itemVal,
            itemUdfPayload: itemPayload
        })
    }

    updateSetUdf = (e, data, index) => {
        let setVal = [...this.state.setUdfVal];
        let setPayload = [...this.state.setUdfPayload]
        if(e.target.id == 'required') {
            let required = data.required == 'true' ? 'false' : 'true'
            setVal[index] = {...data, required: required}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, required: required})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, required: required}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'displayPi') {
            let displayPi = data.displayPi == 'true' ? 'false' : 'true'
            setVal[index] = {...data, displayPi: displayPi}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, displayPi: displayPi})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, displayPi: displayPi}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isCompulsoryPI') {
            let isCompulsoryPI = data.isCompulsoryPI == 'true' ? 'false' : 'true'
            setVal[index] = {...data, isCompulsoryPI: isCompulsoryPI}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, isCompulsoryPI: isCompulsoryPI})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isCompulsoryPI: isCompulsoryPI}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isLovPi') {
            let isLovPi = data.isLovPi == 'true' ? 'false' : 'true'
            setVal[index] = {...data, isLovPi: isLovPi}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, isLovPi: isLovPi})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isLovPi: isLovPi}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isValidationPi') {
            let isValidationPi = data.isValidationPi == 'true' ? 'false' : 'true'
            setVal[index] = {...data, isValidationPi: isValidationPi}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, isValidationPi: isValidationPi})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isValidationPi: isValidationPi}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'displayPo') {
            let displayPo = data.displayPo == 'true' ? 'false' : 'true'
            setVal[index] = {...data, displayPo: displayPo}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, displayPo: displayPo})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, displayPo: displayPo}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isCompulsoryPO') {
            let isCompulsoryPO = data.isCompulsoryPO == 'true' ? 'false' : 'true'
            setVal[index] = {...data, isCompulsoryPO: isCompulsoryPO}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, isCompulsoryPO: isCompulsoryPO})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isCompulsoryPO: isCompulsoryPO}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isLovPo') {
            let isLovPo = data.isLovPo == 'true' ? 'false' : 'true'
            setVal[index] = {...data, isLovPo: isLovPo}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, isLovPo: isLovPo})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isLovPo: isLovPo}
                    } else {
                        return itemP
                    }
                })
            }
        } else if(e.target.id == 'isValidationPo') {
            let isValidationPo = data.isValidationPo == 'true' ? 'false' : 'true'
            setVal[index] = {...data, isValidationPo: isValidationPo}
            if(setPayload.length == 0 || setPayload.filter(item => item.catDescUdfType == data.catDescUdfType).length == 0){
                setPayload.push({...data, isValidationPo: isValidationPo})
            } else {
                setPayload = setPayload.map(itemP => {
                    if(itemP.catDescUdfType == data.catDescUdfType){
                        return {...data, isValidationPo: isValidationPo}
                    } else {
                        return itemP
                    }
                })
            }
        } 
        this.setState({
            setUdfVal: setVal,
            setUdfPayload: setPayload
        })
    }

    tabChangeHandler = (data) => {
        this.setState({
            tabType: data
        })
    }

    saveMapping = () =>{
        let payload = '';
        let tabType = this.state.tabType;
        if(tabType == 'catdesc'){
            payload = {
                catDesc : this.state.catDescPayload
            }
        } else if (tabType == 'itemudf') {
            payload = {
                itemUdf : this.state.itemUdfPayload
            }
        } else if (tabType == 'setudf') {
            payload = {
                setUdf : this.state.setUdfPayload
            }
        }
        this.props.updateGeneralMappingRequest(payload)
    }

    clearMapping = () => {
        let tabType = this.state.tabType;
        if(tabType == 'catdesc'){
            this.setState({
                catDescPayload: [],
                catDescVal: this.state.catDescInitial
            })
        } else if (tabType == 'itemudf') {
            this.setState({
                itemUdfPayload: [],
                itemUdfVal: this.state.itemUdfInitial
            })
        } else if (tabType == 'setudf') {
            this.setState({
                setUdfPayload: [],
                setUdfVal: this.state.setUdfInitial
            })
        }
    }

    getMappingData = () =>{
        this.setState({
            isapparel: !this.state.isapparel
        })
        this.props.getGeneralMappingRequest(!this.state.isapparel)
    }

    render(){
        console.log(this.state.tabType)
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="general-setting-tab-cat-desk m-top-20">
                        <span className="gen-info-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="20" viewBox="0 0 14 14">
                                <path id="iconmonstr-info-2" fill="#97abbf" d="M7 1.167A5.833 5.833 0 1 1 1.167 7 5.84 5.84 0 0 1 7 1.167zM7 0a7 7 0 1 0 7 7 7 7 0 0 0-7-7zm.583 10.5H6.417V5.833h1.166zM7 3.354a.729.729 0 1 1-.729.729A.729.729 0 0 1 7 3.354z"/>
                            </svg>
                            <div className="gii-dropdown">
                                <div className="nph-switch-btn nph-togglt1">
                                    <label className="tg-switch">
                                        <input type="checkbox" checked />
                                        <span className="tg-slider tg-round"></span>
                                    </label>
                                    <label className="nph-text">Lorem ipsum dolor sit amet</label>
                                </div>
                                <div className="nph-switch-btn nph-togglt2">
                                    <label className="tg-switch">
                                        <input type="checkbox" checked />
                                        <span className="tg-slider tg-round"></span>
                                    </label>
                                    <label className="nph-text">Lorem ipsum dolor sit amet</label>
                                </div>
                            </div>
                        </span>
                        <div className="nph-switch-btn proc-cat-desc-toggle">
                            <label className="tg-switch" >
                                <input type="checkbox" checked={this.state.isapparel} onChange={this.getMappingData}/>
                                <span className="tg-slider tg-round"></span>
                            </label>
                            {this.state.isapparel && <label className="nph-wbtext">Apparel</label>}
                            {!this.state.isapparel && <label className="nph-wbtext">Non Apparel</label>}
                        </div>
                    </div>
                    <div className="gen-congiguration">
                        <div className="col-lg-6 pad-0">
                            <div className="gc-left">
                                <div className="global-search-tab gcl-tab">
                                    <ul className="nav nav-tabs gst-inner" role="tablist">
                                        <li className="nav-item active" >
                                            <a className="nav-link gsti-btn" href="#catdesc" role="tab" data-toggle="tab" onClick={() => this.tabChangeHandler('catdesc')}>Cat/Desc</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#itemudf" role="tab" data-toggle="tab" onClick={() => this.tabChangeHandler('itemudf')}>Item UDF</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#setudf" role="tab" data-toggle="tab" onClick={() => this.tabChangeHandler('setudf')}>Set UDF</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            {this.state.tabType == 'catdesc' && <div className="gc-right">
                                {this.state.catDescPayload.length == 0 ?
                                 <button type="button" className="gen-save btnDisabled" >Save</button>
                                : <button type="button" className="gen-save" onClick={this.saveMapping}>Save</button>}
                                {this.state.catDescPayload.length == 0 ? 
                                <button type="button" className="gen-clear btnDisabled">Clear</button>                                
                                : <button type="button" className="gen-clear" onClick={this.clearMapping}>Clear</button>}
                            </div>}
                            {this.state.tabType == 'itemudf' && <div className="gc-right">
                                {this.state.itemUdfPayload.length == 0 ?
                                 <button type="button" className="gen-save btnDisabled" >Save</button>
                                : <button type="button" className="gen-save" onClick={this.saveMapping}>Save</button>}
                                {this.state.itemUdfPayload.length == 0 ? 
                                <button type="button" className="gen-clear btnDisabled">Clear</button>                                
                                : <button type="button" className="gen-clear" onClick={this.clearMapping}>Clear</button>}
                            </div>}
                            {this.state.tabType == 'setudf' && <div className="gc-right">
                                {this.state.setUdfPayload.length == 0 ?
                                 <button type="button" className="gen-save btnDisabled" >Save</button>
                                : <button type="button" className="gen-save" onClick={this.saveMapping}>Save</button>}
                                {this.state.setUdfPayload.length == 0 ? 
                                <button type="button" className="gen-clear btnDisabled">Clear</button>                                
                                : <button type="button" className="gen-clear" onClick={this.clearMapping}>Clear</button>}
                            </div>}
                        </div> 
                    </div>
                </div>
                <div className="tab-content">
                    <div className="tab-pane fade in active" id='catdesc' role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="vendor-gen-table">
                                <div className="manage-table" id="scrollDiv">
                                    <div className="columnFilterGeneric">
                                        <span className="glyphicon glyphicon-menu-left"></span>
                                    </div>
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                <th className="fix-action-btn">
                                                </th>
                                                <th><label>Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Compulsory PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Compulsory PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Lov PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Lov PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>PI Validaion</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>PO Validaion</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.catDescVal.length == 0 ?
                                            <tr>
                                                <td align="center" colSpan="12">
                                                    <label>No Data Found</label>
                                                </td>
                                            </tr>
                                            : this.state.catDescVal.map((cat, indx) => (
                                                cat.required == 'false' ? 
                                                <tr key={indx}>
                                                    <td className="fix-action-btn">
                                                     <ul className="table-item-list">
                                                         <li className="til-inner">
                                                             <label className="checkBoxLabel0">
                                                                 <input type="checkBox" name="selectEach" />
                                                                 <span className="checkmark1"></span>
                                                             </label>
                                                         </li>
                                                         <li className="til-inner">
                                                             <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.required == "true" ? true : false} id='required' onChange={(e) => this.updateCatDesc(e, cat, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    <span className="nph-wbtext">Off</span>
                                                                </label>
                                                             </div>
                                                         </li>
                                                     </ul>
                                                 </td>
                                                 <td><label className="bold">{cat.catDescUdfType}</label></td>
                                                 <td><input type="text" className="vgt-clientname" disabled value = {cat.clientName} /></td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                </tr>
                                                : <tr key={indx}>
                                                    <td className="fix-action-btn">
                                                        <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" name="selectEach" />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                            </li>
                                                            <li className="til-inner">
                                                                <div className="nph-switch-btn">
                                                                    <label className="tg-switch" >
                                                                        <input type="checkbox" checked={cat.required == "true" ? true : false} id='required' onChange={(e) => this.updateCatDesc(e, cat, indx)} />
                                                                        <span className="tg-slider tg-round"></span>
                                                                        {cat.required == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                        <span className="nph-wbtext">Off</span>}
                                                                    </label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td><label className="bold">{cat.catDescUdfType}</label></td>
                                                    <td><input type="text" className="vgt-clientname" disabled value={cat.clientName}/></td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.displayPi == "true" ? true : false} id='displayPi' onChange={(e) => this.updateCatDesc(e, cat, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {cat.displayPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}                                                                  
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.displayPo == "true" ? true : false} id='displayPo' onChange={(e) => this.updateCatDesc(e, cat, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {cat.displayPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.isCompulsoryPI == 'true' ? true : false} id='isCompulsoryPI' onChange={(e) => this.updateCatDesc(e, cat, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {cat.isCompulsoryPI == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.isCompulsoryPO == 'true' ? true : false} id='isCompulsoryPO' onChange={(e) => this.updateCatDesc(e, cat, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {cat.isCompulsoryPO == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.isLovPi == 'true' ? true : false} id='isLovPi' onChange={(e) => this.updateCatDesc(e, cat, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {cat.isLovPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.isLovPo == 'true' ? true : false} id='isLovPo' onChange={(e) => this.updateCatDesc(e, cat, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {cat.isLovPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.isValidationPi == "true" ? true : false} id='isValidationPi' onChange={(e) => this.updateCatDesc(e, cat, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {cat.isValidationPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}                                                                  
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={cat.isValidationPo == "true" ? true : false} id='isValidationPo' onChange={(e) => this.updateCatDesc(e, cat, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {cat.isValidationPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}                                                                  
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>))}                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" value="1" />
                                        {/* <span className="ngp-total-item">Total Items </span> <span className="bold"></span> */}
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="itemudf" role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="vendor-gen-table" >
                                <div className="manage-table" id="scrollDiv">
                                    <div className="columnFilterGeneric">
                                        <span className="glyphicon glyphicon-menu-left"></span>
                                    </div>
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                <th className="fix-action-btn">
                                                </th>
                                                <th><label>Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Compulsory PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Compulsory PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Lov PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Lov PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>PI Validaion</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>PO Validaion</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.itemUdfVal.length == 0 ?
                                            <tr>
                                                <td align="center" colSpan="12">
                                                    <label>No Data Found</label>
                                                </td>
                                            </tr>
                                            : this.state.itemUdfVal.map((item, indx) => (                                                
                                                item.required == 'false' ? 
                                                <tr key={indx}>
                                                    <td className="fix-action-btn">
                                                     <ul className="table-item-list">
                                                         <li className="til-inner">
                                                             <label className="checkBoxLabel0">
                                                                 <input type="checkBox" name="selectEach" />
                                                                 <span className="checkmark1"></span>
                                                             </label>
                                                         </li>
                                                         <li className="til-inner">
                                                             <div className="nph-switch-btn">
                                                                 <label className="tg-switch" >
                                                                     <input type="checkbox" checked={item.required == 'true' ? true : false} id='required' onChange={(e) => this.updateItemUdf(e, item, indx)} />
                                                                     <span className="tg-slider tg-round"></span>
                                                                     <span className="nph-wbtext">Off</span>
                                                                 </label>
                                                             </div>
                                                         </li>
                                                     </ul>
                                                 </td>
                                                 <td><label className="bold">{item.catDescUdfType}</label></td>
                                                 <td><input type="text" className="vgt-clientname" disabled value = {item.clientName} /></td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                </tr>
                                                : <tr key={indx}>
                                                    <td className="fix-action-btn">
                                                        <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" name="selectEach" />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                            </li>
                                                            <li className="til-inner">
                                                                <div className="nph-switch-btn">
                                                                    <label className="tg-switch" >
                                                                        <input type="checkbox" checked={item.required == 'true' ? true : false} id='required' onChange={(e) => this.updateItemUdf(e, item, indx)} />
                                                                        <span className="tg-slider tg-round"></span>
                                                                        {item.required == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                        <span className="nph-wbtext">Off</span>}
                                                                    </label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td><label className="bold">{item.catDescUdfType}</label></td>
                                                    <td><input type="text" className="vgt-clientname" disabled value={item.clientName}/></td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={item.displayPi == "true" ? true : false} id='displayPi' onChange={(e) => this.updateItemUdf(e, item, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {item.displayPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={item.displayPo == "true" ? true : false} id='displayPo' onChange={(e) => this.updateItemUdf(e, item, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {item.displayPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={item.isCompulsoryPI == 'true' ? true : false} id='isCompulsoryPI' onChange={(e) => this.updateItemUdf(e, item, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {item.isCompulsoryPI == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={item.isCompulsoryPO == 'true' ? true : false} id='isCompulsoryPO' onChange={(e) => this.updateItemUdf(e, item, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {item.isCompulsoryPO == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={item.isLovPi == 'true' ? true : false} id='isLovPi' onChange={(e) => this.updateItemUdf(e, item, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {item.isLovPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={item.isLovPo == 'true' ? true : false} id='isLovPo' onChange={(e) => this.updateItemUdf(e, item, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {item.isLovPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={item.isValidationPi == "true" ? true : false} id='isValidationPi' onChange={(e) => this.updateItemUdf(e, item, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {item.isValidationPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}                                                                  
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={item.isValidationPo == "true" ? true : false} id='isValidationPo' onChange={(e) => this.updateItemUdf(e, item, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {item.isValidationPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}                                                                  
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>))}                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" value="1" />
                                        {/* <span className="ngp-total-item">Total Items </span> <span className="bold"></span> */}
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="setudf" role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="vendor-gen-table" >
                                <div className="manage-table" id="scrollDiv">
                                    <div className="columnFilterGeneric">
                                        <span className="glyphicon glyphicon-menu-left"></span>
                                    </div>
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                <th className="fix-action-btn">
                                                </th>
                                                <th><label>Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Display PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Compulsory PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Compulsory PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Lov PI</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Lov PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>PI Validaion</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>PO Validaion</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.setUdfVal.length == 0 ?
                                            <tr>
                                                <td align="center" colSpan="12">
                                                    <label>No Data Found</label>
                                                </td>
                                            </tr>
                                            : this.state.setUdfVal.map((set, indx) => (
                                                set.required == 'false' ? 
                                                <tr key={indx}>
                                                    <td className="fix-action-btn">
                                                     <ul className="table-item-list">
                                                         <li className="til-inner">
                                                             <label className="checkBoxLabel0">
                                                                 <input type="checkBox" name="selectEach" />
                                                                 <span className="checkmark1"></span>
                                                             </label>
                                                         </li>
                                                         <li className="til-inner">
                                                             <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.required == 'true' ? true : false} id='required' onChange={(e) => this.updateSetUdf(e, set, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    <span className="nph-wbtext">Off</span>
                                                                </label>
                                                             </div>
                                                         </li>
                                                     </ul>
                                                 </td>
                                                 <td><label className="bold">{set.catDescUdfType}</label></td>
                                                 <td><input type="text" className="vgt-clientname" disabled value = {set.clientName} /></td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                 <td>
                                                     <div className="vgdt-toggle">
                                                         <span className="vgdt-text">N/A</span>
                                                     </div>
                                                 </td>
                                                </tr>
                                                : <tr key={indx}>
                                                    <td className="fix-action-btn">
                                                        <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" name="selectEach" />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                            </li>
                                                            <li className="til-inner">
                                                                <div className="nph-switch-btn">
                                                                    <label className="tg-switch" >
                                                                        <input type="checkbox" checked={set.required == 'true' ? true : false} id='required' onChange={(e) => this.updateSetUdf(e, set, indx)}  />
                                                                        <span className="tg-slider tg-round"></span>
                                                                        {set.required == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                        <span className="nph-wbtext">Off</span>}
                                                                    </label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td><label className="bold">{set.catDescUdfType}</label></td>
                                                    <td><input type="text" className="vgt-clientname" disabled value={set.clientName}/></td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.displayPi == "true" ? true : false} id='displayPi' onChange={(e) => this.updateSetUdf(e, set, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {set.displayPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.displayPo == "true" ? true : false} id='displayPo' onChange={(e) => this.updateSetUdf(e, set, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {set.displayPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.isCompulsoryPI == 'true' ? true : false} id='isCompulsoryPI' onChange={(e) => this.updateSetUdf(e, set, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {set.isCompulsoryPI == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.isCompulsoryPO == 'true' ? true : false} id='isCompulsoryPO' onChange={(e) => this.updateSetUdf(e, set, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {set.isCompulsoryPO == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.isLovPi == 'true' ? true : false} id='isLovPi' onChange={(e) => this.updateSetUdf(e, set, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {set.isLovPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                            <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.isLovPo == 'true' ? true : false} id='isLovPo' onChange={(e) => this.updateSetUdf(e, set, indx)} />
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {set.isLovPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.isValidationPi == "true" ? true : false} id='isValidationPi' onChange={(e) => this.updateSetUdf(e, set, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {set.isValidationPi == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}                                                                  
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="vgdt-toggle">
                                                            <div className="nph-switch-btn">
                                                                <label className="tg-switch" >
                                                                    <input type="checkbox" checked={set.isValidationPo == "true" ? true : false} id='isValidationPo' onChange={(e) => this.updateSetUdf(e, set, indx)}/>
                                                                    <span className="tg-slider tg-round"></span>
                                                                    {set.isValidationPo == 'true' ? <span className="nph-wbtext nph-wbtext-left">On</span>:
                                                                    <span className="nph-wbtext">Off</span>}                                                                  
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>))}                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" value="1" />
                                        {/* <span className="ngp-total-item">Total Items </span> <span className="bold"></span> */}
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CatDescUdfTab;