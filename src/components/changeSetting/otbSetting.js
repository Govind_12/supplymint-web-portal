import React from 'react';

class OtbSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <div className="configurationSetting otbSetting p-lr-47">
                <div className="configuraionAccordian">
                    <div className="accordion" id="accordionExample">
                        <div className="card">
                            <div className="card-header" id="headingOne">
                                <h3 className="mb-0">
                                    OTB Configuration
                                </h3>
                            </div>
                            <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div className="card-body">
                                    <p className="availableOptions">Below are the all available options for OTB configuration</p>
                                    <div className="col-md-12 m-top-20">
                                        <div className="col-md-11 pad-0">
                                            {this.props.otbSettingKeys.length != 0 ? this.props.otbSettingKeys.map((data, key) => (
                                                <div className="col-md-3 pad-0" key={key}>
                                                    <div className="item">
                                                        <span>{this.props.otbSettingData[data]}</span>
                                                        <div className={this.props.checkedData.includes(`${this.props.otbSettingData[data]}`) ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                            <label className="checkBoxLabel0 m0 text-middle displayPointer"><input type="checkBox" id="symbol" checked={this.props.checkedData.includes(`${this.props.otbSettingData[data]}`) ? true : false} onClick={(e) => this.props.checkedDatafun(`${this.props.otbSettingData[data]}`)} />{this.props.checkedData.includes(`${this.props.otbSettingData[data]}`) ? "Selected" : "Select"} <span className="checkmark1"></span> </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            )) : null}
                                        </div>
                                    </div>
                                    <div className="col-md-12 m-top-35">
                                        <p className="note">Note - Only selected items will be saved for OTB configuration.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default OtbSetting; 