import React from 'react';
import {getDate} from '../../helper';


class ProcurementSetting extends React.Component {
    constructor(props){
        super(props);
        this.state = {

        }
    }

    componentDidMount(){
        this.props.getAvailableKeysRequest("proDate")
    }

    render() {
        return (
            <div className="configurationSetting otbSetting procurementSetting p-lr-47 heightAuto">
                <div className="configuraionAccordian">
                    <div className="accordion col-lg-5 col-md-5 col-sm-12 pad-lft-0" id="accordionExample">
                        <div className="card m0">
                            <div className="card-header" id="headingOne">
                                <h3 className="mb-0">
                                    PO Date Range
                                </h3>
                            </div>
                            <div className="card-content">
                                <ul className="pad-0 displayInline width100 lineAfter m-top-10">
                                    <li> <label className="select_modalRadio">
                                        <input type="radio" name="frequency" id="noRestrict" onChange={this.props.handleRestriction} checked={this.props.dateSelect == "noRestriction" ? true : false} /><span>No Restriction</span>
                                        <span className="checkradio-select select_all positionCheckbox"></span>
                                    </label>
                                    </li>
                                    <li className="displayInline m-top-50"> <label className="select_modalRadio">
                                        <input type="radio" name="frequency" id="restriction" onChange={this.props.handleRestriction} checked={this.props.dateSelect == "restriction" ? true : false} /><span></span>
                                        <span className="checkradio-select select_all positionCheckbox"></span>
                                    </label>
                                    </li>
                                    <li className="m-top-40">
                                        <div className="eachDate m-rgt-20">
                                            <label className="labelGlobal">From Date</label>
                                            {this.props.dateSelect == "noRestriction" || this.props.dateSelect == "" ? <input type="date" value={this.props.startDate} className="purchaseOrderTextbox" placeholder={this.props.startDate == "" ? "Select Start Date" : this.props.startDate} disabled />
                                                : <input type="date" className="purchaseOrderTextbox" id="startDate" min={getDate()} value={this.props.startDate} placeholder={this.props.startDate == "" ? "Select Start Date" : this.props.startDate} onChange={this.props.dateHandle} />}
                                            {this.props.startDateErr ? (
                                                <span className="error" >
                                                    Select start date
                                                    </span>
                                            ) : null}
                                        </div>
                                        <div className="eachDate">
                                            <label className="labelGlobal">To Date</label>
                                            {this.props.dateSelect == "noRestriction" || this.props.dateSelect == "" || this.props.startDate == "" ? <input type="date" value={this.props.endDate} className="purchaseOrderTextbox" placeholder={this.props.endDate == "" ? "Select End Date" : this.props.endDate} disabled />
                                                : <input type="date" className="purchaseOrderTextbox" min={this.props.startDate} id="endDate" value={this.props.endDate} placeholder={this.props.endDate == "" ? "Select End Date" : this.props.endDate} onChange={this.props.dateHandle} />}
                                            {this.props.endDateErr ? (
                                                <span className="error" >
                                                    Select end date
                                                    </span>
                                            ) : null}
                                        </div>
                                    </li>
                                </ul>
                                <div className="eachDate m-rgt-20 m-left-59 m-top-20">
                                    <label className="labelGlobal">Buffer Days</label>
                                    <input type="text" className={this.props.bufferDaysErr ? "purchaseOrderTextbox errorBorder" : "purchaseOrderTextbox"} value={this.props.bufferDays} onChange={this.props.bufferDaysHandler} />
                                    {this.props.bufferDaysErr ? (
                                        <span className="error" >
                                            {this.props.invalidBufferDays ? "It's value can't be less than 15 days." : "Please fill buffer days."}
                                            </span>
                                    ) : null}
                                </div>
                                <div className="markDefault m-top-30 displayInline width100">
                                    {/* <label className="checkBoxLabel0 displayPointer width100">Mark as default<input type="checkBox" className="checkBoxTab"/> <span className="checkmark1"></span> </label> */}
                                    <div className="note m-top-30">
                                        <span className="verticalTop">Note : </span>
                                        <label>User cannot create Purchase request on above selected
                                        date range,Please make sure while choosing dates.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="proc-setting-box">
                            <h4>Date</h4>
                            <div class="psb-drop">
                                <select id="dateFormat">
                                    <option value="dd MMMM yyyy">dd MMMM yyyy</option>
                                    <option value="dd MMM yyyy">dd MMM yyyy</option>
                                    <option value="dd-MM-yyyy">dd-MM-yyyy</option>
                                    <option value="yyyy-MM-dd">yyyy-MM-dd</option>
                                    <option value="yyyy/MM/dd">yyyy/MM/dd</option>
                                    <option value="yyyy-MM-dd HH:mm">yyyy-MM-dd HH:mm</option>
                                    <option value="yyyy/MM/dd HH:mm">yyyy/MM/dd HH:mm</option>
                                    <option value="dd MMMM yyyy HH:mm">dd MMMM yyyy HH:mm</option>
                                    <option value="dd MMM yyyy HH:mm">dd MMM yyyy HH:mm</option>
                                    <option value="dd-MM-yyyy HH:mm">dd-MM-yyyy HH:mm</option>
                                    <option value="dd MMM yyyy 'AT' HH:mm">dd MMM yyyy 'AT' HH:mm</option>
                                    <option value="E MMM dd HH:mm">E MMM dd HH:mm</option>
                                </select>
                            </div>
                        </div>
                    </div> */}
                </div>
            </div>
        )
    }
}

export default ProcurementSetting;