import React from 'react';
import ToastLoader from "../loaders/toastLoader";
class ConfigurationSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            saveState:false,
            toastLoader: false,
            toastMsg: "",
            hl1Check: false,
            hl2Check: false,
            hl3Check: false,
            hl4Check: false,
            hl5Check: false,
            hl6Check: false,
            hlOne: "",
            hlTwo: "",
            hlThree: "",
            hlFour: "",
            hlFive: "",
            hlSix: "",
            hierarchyLevelData: [],
            udfData: [],
            focused: "",
            hlOneDrop: false,
            hlTwoDrop: false,
            hlThreeDrop: false,
            hlFourDrop: false,
            hlFiveDrop: false,
            hlSixDrop: false,
            udf1Check: false,
            udf2Check: false,
            udf3Check: false,
            udf4Check: false,
            udf5Check: false,
            udf6Check: false,
            udfOne: "",
            udfTwo: "",
            udfThree: "",
            udfFour: "",
            udfFive: "",
            udfSix: "",
            udfOneDrop: false,
            udfTwoDrop: false,
            udfThreeDrop: false,
            udfFourDrop: false,
            udfFiveDrop: false,
            udfSixDrop: false,
            dataDelete: "",
            configDelete: [],
            //false,
            level1: "Select H Level 1", level2: "Select H Level 2", level3: "Select H Level 3", level4: "Select H Level 4", level5: "Select H Level 5", level6: "Select H Level 6",
            udf1: "Select UDf 1", udf2: "Select UDf 2", udf3: "Select UDf 3", udf4: "Select UDf 4", udf5: "Select UDf 5", udf6: "Select UDf 6",
            hLevel: {
                HL1NAME: "", HL2NAME: "", HL3NAME: "", HL4NAME: "", HL5NAME: "", HL6NAME: ""
            },
            udflevel: {
                UDF1: "", UDF2: "", UDF3: "", UDF4: "", UDF5: "", UDF6: ""
            },
            hlevelValue: [],
            hlevelArray: [], udfValues: [], udfArray: [],
            index1: "", index2: ""

        }
    }
    componentWillMount() {

        this.props.otbGetHlevelRequest();
        this.props.getAllConfigRequest();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.changeSetting.otbGetHlevel.isSuccess) {
            if (nextProps.changeSetting.otbGetHlevel.data.resource != null) {
                let hlevel = nextProps.changeSetting.otbGetHlevel.data.resource.hlevel
                let udf = nextProps.changeSetting.otbGetHlevel.data.resource.udf
                let objectUdf = Object.values(udf)
                let object = Object.values(hlevel)
                let udfData = this.state.udfData
                let hierarchyLevelData = this.state.hierarchyLevelData
                let hlevelValue = this.state.hlevelValue
                let hlevelArray = this.state.hlevelArray
                let udfValues = this.state.udfValues
                let udfArray = this.state.udfArray
                for (var i = 0; i < object.length; i++) {
                    hierarchyLevelData.push(Object.keys(object[i]).join())
                    hlevelValue.push(Object.keys(object[i]).join())
                    hlevelArray.push(Object.keys(object[i]).join())
                }
                for (var j = 0; j < objectUdf.length; j++) {
                    udfData.push(Object.keys(objectUdf[j]).join())
                    udfValues.push(Object.keys(objectUdf[j]).join())
                    udfArray.push(Object.keys(objectUdf[j]).join())
                }
                this.setState({
                    hierarchyLevelData: hierarchyLevelData,
                    udfData: udfData,
                    hlevelValue: hlevelValue,
                    hlevelArray: hlevelArray,
                    udfValues: udfValues,
                    udfArray: udfArray

                })
            }
            this.props.otbGetHlevelClear();
        }
        if (nextProps.changeSetting.getAllConfig.isSuccess) {
            if (nextProps.changeSetting.getAllConfig.data.resource != null) {
                var hierarchyLevelData = this.state.hierarchyLevelData
                var udfData = this.state.udfData

                var hlLevel = nextProps.changeSetting.getAllConfig.data.resource.hlevel
                var configDelete = Object.values(hlLevel)
                var udfLevel = nextProps.changeSetting.getAllConfig.data.resource.udflevel

                var udfDelete = Object.values(udfLevel)
                var configDeletes = []
                var udfDeletes = []

                for (var k = 0; k < configDelete.length; k++) {
                    configDeletes.push(Object.keys(configDelete[k]).join())
                }
                for (var m = 0; m < udfDelete.length; m++) {
                    udfDeletes.push(Object.keys(udfDelete[m]).join())
                }

                for (var i = 0; i < hierarchyLevelData.length; i++) {
                    for (var j = 0; j < configDeletes.length; j++) {
                        if (hierarchyLevelData[i] == configDeletes[j]) {
                            hierarchyLevelData.splice(i, 1)
                        }
                    }
                }

                for (var k = 0; k < udfData.length; k++) {
                    for (var l = 0; l < udfDeletes.length; l++) {
                        if (udfData[k] == udfDeletes[l]) {
                            udfData.splice(k, 1)
                        }
                    }
                }

                var hierDefSelect = nextProps.changeSetting.getAllConfig.data.resource.hlevel
                var udfDefSelect = nextProps.changeSetting.getAllConfig.data.resource.udflevel
                this.setState({
                    hierarchyLevelData,
                    udfData,
                    hlOne: Object.keys(hierDefSelect.HL1NAME).toString() != undefined ? Object.keys(hierDefSelect.HL1NAME).toString() : "",
                    hlTwo: Object.keys(hierDefSelect.HL2NAME).toString() != undefined ? Object.keys(hierDefSelect.HL2NAME).toString() : "",
                    hlThree: Object.keys(hierDefSelect.HL3NAME).toString() != undefined ? Object.keys(hierDefSelect.HL3NAME).toString() : "",
                    hlFour: Object.keys(hierDefSelect.HL4NAME).toString() != undefined ? Object.keys(hierDefSelect.HL4NAME).toString() : "",
                    hlFive: Object.keys(hierDefSelect.HL5NAME).toString() != undefined ? Object.keys(hierDefSelect.HL5NAME).toString() : "",
                    hlSix: Object.keys(hierDefSelect.HL6NAME).toString() != undefined ? Object.keys(hierDefSelect.HL6NAME).toString() : "",
                    udfOne: Object.keys(udfDefSelect.UDF1).toString() != undefined ? Object.keys(udfDefSelect.UDF1).toString() : "",
                    udfTwo: Object.keys(udfDefSelect.UDF2).toString() != undefined ? Object.keys(udfDefSelect.UDF2).toString() : "",
                    udfThree: Object.keys(udfDefSelect.UDF3).toString() != undefined ? Object.keys(udfDefSelect.UDF3).toString() : "",
                    udfFour: Object.keys(udfDefSelect.UDF4).toString() != undefined ? Object.keys(udfDefSelect.UDF4).toString() : "",
                    udfFive: Object.keys(udfDefSelect.UDF5).toString() != undefined ? Object.keys(udfDefSelect.UDF5).toString() : "",
                    udfSix: Object.keys(udfDefSelect.UDF6).toString() != undefined ? Object.keys(udfDefSelect.UDF6).toString() : "",
                    hl1Check: Object.keys(hierDefSelect.HL1NAME).toString() != "" ? true : false,
                    hl2Check: Object.keys(hierDefSelect.HL2NAME).toString() != "" ? true : false,
                    hl3Check: Object.keys(hierDefSelect.HL3NAME).toString() != "" ? true : false,
                    hl4Check: Object.keys(hierDefSelect.HL4NAME).toString() != "" ? true : false,
                    hl5Check: Object.keys(hierDefSelect.HL5NAME).toString() != "" ? true : false,
                    hl6Check: Object.keys(hierDefSelect.HL6NAME).toString() != "" ? true : false,
                    udf1Check: Object.keys(udfDefSelect.UDF1).toString() != "" ? true : false,
                    udf2Check: Object.keys(udfDefSelect.UDF2).toString() != "" ? true : false,
                    udf3Check: Object.keys(udfDefSelect.UDF3).toString() != "" ? true : false,
                    udf4Check: Object.keys(udfDefSelect.UDF4).toString() != "" ? true : false,
                    udf5Check: Object.keys(udfDefSelect.UDF5).toString() != "" ? true : false,
                    udf6Check: Object.keys(udfDefSelect.UDF6).toString() != "" ? true : false,
                })

            }
        }
        if (nextProps.changeSetting.settingOtbCreate.isSuccess) {
            this.setState({
                saveState:false
            })
        }
    }

    checkedhl(type) {
        if (type == "hl1Check") {
            if (this.state.hl1Check) {
                if (this.state.hl2Check) {
                    this.setState({
                        hl1Check: true,
                        //true
                    })
                } else {
                    this.setState({
                        hl1Check: false,
                        //false
                    })
                }
            } else if (this.state.hlOne != "") {
                this.setState({
                    hl1Check: true,
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select H Level 1",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)

            }
        } else if (type == "hl2Check") {
            if (this.state.hl2Check) {
                if (this.state.hl3Check) {
                    this.setState({
                        hl2Check: true,
                        hl1Check: true
                        //true
                    })

                } else {
                    this.setState({
                        hl2Check: false,
                        hl3Check: false,
                        hl4Check: false,
                        hl5Check: false,
                        hl6Check: false
                        //false
                    })
                }

            } else if (this.state.hlTwo != "") {

                this.setState({
                    hl2Check: true,
                    hl1Check: true
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select H Level 2",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "hl3Check") {
            if (this.state.hl3Check) {
                if (this.state.hl4Check) {
                    this.setState({
                        hl3Check: true,
                        hl2Check: true,
                        hl1Check: true
                        //true
                    })
                } else {
                    this.setState({
                        hl3Check: false,
                        hl4Check: false,
                        hl5Check: false,
                        hl6Check: false
                        //false
                    })
                }
            } else if (this.state.hlThree != "") {
                this.setState({
                    hl3Check: true,
                    hl2Check: true,
                    hl1Check: true
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select H Level 3",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "hl4Check") {

            if (this.state.hl4Check) {
                if (this.state.hl5Check) {
                    this.setState({
                        hl4Check: true,
                        hl3Check: true,
                        hl2Check: true,
                        hl1Check: true
                        //true
                    })
                } else {
                    this.setState({
                        hl4Check: false,
                        hl5Check: false,
                        hl6Check: false
                        //false
                    })
                }
            } else if (this.state.hlFour != "") {
                this.setState({
                    hl4Check: true,
                    hl3Check: true,
                    hl2Check: true,
                    hl1Check: true
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select H Level 4",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "hl5Check") {
            if (this.state.hl5Check) {
                if (this.state.hl6Check) {
                    this.setState({
                        hl5Check: true,
                        hl4Check: true,
                        hl3Check: true,
                        hl2Check: true,
                        hl1Check: true
                        //true
                    })

                }
                else {
                    this.setState({
                        hl5Check: false,
                        hl6Check: false
                        //false
                    })
                }
            } else if (this.state.hlFive != "") {
                this.setState({
                    hl5Check: true,
                    hl4Check: true,
                    hl3Check: true,
                    hl2Check: true,
                    hl1Check: true
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select H Level 5",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "hl6Check") {
            if (this.state.hl6Check) {
                this.setState({
                    hl6Check: false,
                    //false
                })
            } else if (this.state.hlSix != "") {
                this.setState({
                    hl6Check: true,
                    hl5Check: true,
                    hl4Check: true,
                    hl3Check: true,
                    hl2Check: true,
                    hl1Check: true
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select H Level 6",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "udf1Check") {
            if (this.state.udf1Check) {
                if (this.state.udf2Check) {
                    this.setState({
                        udf1Check: true,

                    })
                } else {
                    this.setState({
                        udf1Check: false,

                    })
                }
            } else if (this.state.udfOne != "") {
                this.setState({
                    udf1Check: true,

                })
            } else {
                this.setState({
                    toastMsg: "select Udf 1",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "udf2Check") {

            if (this.state.udf2Check) {
                if (this.state.udf3Check) {
                    this.setState({
                        udf2Check: true,
                    })
                } else {

                    this.setState({
                        udf2Check: false,
                        //false
                    })
                }
            } else if (this.state.udfTwo != "") {
                this.setState({
                    udf2Check: true,
                    udf1Check: true
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select Udf 2",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "udf3Check") {
            if (this.state.udf3Check) {
                if (this.state.udf4Check) {
                    this.setState({
                        udf3Check: true,

                    })
                } else {
                    this.setState({
                        udf3Check: false,
                        //false
                    })
                }
            } else if (this.state.udfThree != "") {
                this.setState({
                    udf3Check: true,
                    udf2Check: true,
                    udf1Check: true
                })
            } else {
                this.setState({
                    toastMsg: "select Udf 3",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "udf4Check") {
            if (this.state.udf4Check) {
                if (this.state.udf5Check) {
                    this.setState({
                        udf4Check: true,

                    })
                }
                else {
                    this.setState({
                        udf4Check: false,
                        //false
                    })
                }
            } else if (this.state.udfFour != "") {
                this.setState({
                    udf4Check: true,
                    udf3Check: true,
                    udf2Check: true,
                    udf1Check: true

                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select Udf 4",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "udf5Check") {

            if (this.state.udf5Check) {
                if (this.state.udf6Check) {
                    this.setState({
                        udf5Check: true,
                        //true
                    })
                }
                else {
                    this.setState({
                        udf5Check: false,
                        //false
                    })
                }
            } else if (this.state.udfFive != "") {
                this.setState({
                    udf5Check: true,
                    udf4Check: true,
                    udf3Check: true,
                    udf2Check: true,
                    udf1Check: true
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select Udf 5",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        } else if (type == "udf6Check") {
            if (this.state.udf6Check) {

                this.setState({
                    udf6Check: false,
                    //false
                })
            } else if (this.state.udfSix != "") {
                this.setState({
                    udf6Check: true,
                    udf5Check: true,
                    udf4Check: true,
                    udf3Check: true,
                    udf2Check: true,
                    udf1Check: true
                    //true
                })
            } else {
                this.setState({
                    toastMsg: "select Udf 6",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        }

    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    /**
     * Alert if clicked on outside of element
     */
    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({
                udfOneDrop: false,
                udfTwoDrop: false,
                udfThreeDrop: false,
                udfFourDrop: false,
                udfFiveDrop: false,
                udfSixDrop: false,
                hlOneDrop: false,
                hlTwoDrop: false,
                hlThreeDrop: false,
                hlFourDrop: false,
                hlFiveDrop: false,
                hlSixDrop: false,
            })
        }
    }

    addDelete(value, type) {
        this.setState({
           saveState:true
        })
           
        let hlevelValue = this.state.hlevelValue
        let hierarchyLevelData = this.state.hierarchyLevelData
        let index = ""
        if (value != "" && value != "Clear H Level" && value != this.state.focused) {

            for (let i = 0; i < hierarchyLevelData.length; i++) {
                if (hierarchyLevelData[i] == value) {
                    index = i.toString()
                    hierarchyLevelData.splice(i, 1)

                }
            }
            if (this.state.focused != "") {
                hierarchyLevelData.push(this.state.focused)
            }
            this.setState({
                hierarchyLevelData: hierarchyLevelData
            })
        }

        if (value != "" && value != "Clear H Level" && value != this.state.focused) {

            if (!hlevelValue.includes(value)) {
                hlevelValue.splice(Number(index), 0, value);
            }
         
        }

        setTimeout(() => {
            if (type == 'hl1') {

                this.setState({
                    hlTwo: "",
                    hlThree: "",
                    hlFour: "",
                    hlFive: "",
                    hlSix: "",
                    hl2Check: false,
                    hl3Check: false,
                    hl4Check: false,
                    hl5Check: false,
                    hl6Check: false,
                    hlevelArray: hlevelValue

                })
            } else if (type == 'hl2') {

                this.setState({
                    hlThree: "",
                    hlFour: "",
                    hlFive: "",
                    hlSix: "",
                    hl3Check: false,
                    hl4Check: false,
                    hl5Check: false,
                    hl6Check: false,
                    hlevelArray: hlevelValue
                })


            } else if (type == 'hl3') {

                this.setState({
                    hlFour: "",
                    hlFive: "",
                    hlSix: "",
                    hl4Check: false,
                    hl5Check: false,
                    hl6Check: false,
                    hlevelArray: hlevelValue
                })
            } else if (type == 'hl4') {


                this.setState({
                    hlFive: "",
                    hlSix: "",
                    hl5Check: false,
                    hl6Check: false,
                    hlevelArray: hlevelValue

                })
            } else if (type == 'hl5') {

                this.setState({
                    hlSix: "",
                    hl6Check: false,
                    hlevelArray: hlevelValue

                })
            } else if (type == 'hl6') {
                this.setState({
                    hlevelArray: hlevelValue
                })

            }
        }, 10)
    }
    addDeleteUdf(value, type) {
        this.setState({
            saveState:true
         })
        let udfValues = this.state.udfValues
        let udfData = this.state.udfData
        let index=""
        if (value != "" && value != this.state.focused) {
            for (let i = 0; i < udfData.length; i++) {
                if (udfData[i] == value) {
                    index= i.toString()
                    udfData.splice(i, 1)
                }

            }
            if (this.state.focused != "") {
                udfData.push(this.state.focused)
            }
            this.setState({
                udfData: udfData
            })

        }

        if (value != "" && value != "Clear UDF" && value != this.state.focused) {

            if (!udfValues.includes(value)) {
                udfValues.splice(Number(index), 0, value);
            }
        
        }
        setTimeout(() => {
            if (type == 'udf1') {


                this.setState({
                    udfTwo: "",
                    udfThree: "",
                    udfFour: "",
                    udfFive: "",
                    udfSix: "",
                    udf2Check: false,
                    udf3Check: false,
                    udf4Check: false,
                    udf5Check: false,
                    udf6Check: false,
                    udfArray: udfValues
                })
            } else if (type == 'udf2') {


                this.setState({
                    udfThree: "",
                    udfFour: "",
                    udfFive: "",
                    udfSix: "",
                    udf3Check: false,
                    udf4Check: false,
                    udf5Check: false,
                    udf6Check: false,
                    udfArray: udfValues
                })
            } else if (type == 'udf3') {


                this.setState({
                    udfFour: "",
                    udfFive: "",
                    udfSix: "",
                    udf4Check: false,
                    udf5Check: false,
                    udf6Check: false,
                    udfArray: udfValues
                })
            } else if (type == 'udf4') {



                this.setState({
                    udfFive: "",
                    udfSix: "",
                    udf5Check: false,
                    udf6Check: false,
                    udfArray: udfValues
                })
            } else if (type == 'udf5') {


                this.setState({
                    udfSix: "",
                    udf6Check: false,
                    udfArray: udfValues
                })
            } else if (type == 'udf6') {


                this.setState({
                    udfArray: udfValues
                })
            }

        }, 10)
    }
    openDropDown(drop) {

        let hlevelArray = this.state.hlevelArray

        let hLevelData = []
        let udfArray = this.state.udfArray
        let udfData = []

        if (drop == "hlOneDrop") {
            let index = ""
            if (this.state.hlOne != "") {
                for (var i = 0; i < hlevelArray.length; i++) {
                    if (hlevelArray[i] == this.state.hlOne) {
                        index = i
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlTwo != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }

                    }
                }
            } else {
                hLevelData = hlevelArray
            }


            this.setState({
                hlOneDrop: true,
                hlTwoDrop: false,
                hlThreeDrop: false,
                hlFourDrop: false,
                hlFiveDrop: false,
                hlSixDrop: false,
                focused: this.state.hlOne,
                hierarchyLevelData: hLevelData


            })

        } else if (drop == "hlTwoDrop") {
            let index = ""
            if (this.state.hlTwo != "") {
                for (var i = 0; i < hlevelArray.length; i++) {
                    if (hlevelArray[i] == this.state.hlTwo) {
                        index = i
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }
                    }
                }
            } else {
                for (let k = 0; k < hlevelArray.length; k++) {
                    if (hlevelArray[k] == this.state.hlOne) {
                        index = k
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }

                    }
                }
            }


            this.setState({
                hlOneDrop: false,
                hlTwoDrop: true,
                hlThreeDrop: false,
                hlFourDrop: false,
                hlFiveDrop: false,
                hlSixDrop: false,
                focused: this.state.hlTwo,

                hierarchyLevelData: hLevelData
            })
        } else if (drop == "hlThreeDrop") {
            let index = ""
            if (this.state.hlThree != "") {
                for (var i = 0; i < hlevelArray.length; i++) {
                    if (hlevelArray[i] == this.state.hlThree) {
                        index = i
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlTwo != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }
                    }
                }
            } else {
                for (let k = 0; k < hlevelArray.length; k++) {
                    if (hlevelArray[k] == this.state.hlTwo) {
                        index = k
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlTwo != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }

                    }
                }
            }
            this.setState({
                hlOneDrop: false,
                hlTwoDrop: false,
                hlThreeDrop: true,
                hlFourDrop: false,
                hlFiveDrop: false,
                hlSixDrop: false,
                focused: this.state.hlThree,

                hierarchyLevelData: hLevelData
            })


        } else if (drop == "hlFourDrop") {

            let index = ""
            if (this.state.hlFour != "") {
                for (var i = 0; i < hlevelArray.length; i++) {
                    if (hlevelArray[i] == this.state.hlFour) {
                        index = i
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlTwo != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }
                    }
                }
            } else {
                for (let k = 0; k < hlevelArray.length; k++) {
                    if (hlevelArray[k] == this.state.hlThree) {
                        index = k
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlTwo != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }

                    }
                }


            }
            this.setState({
                hlOneDrop: false,
                hlTwoDrop: false,
                hlThreeDrop: false,
                hlFourDrop: true,
                hlFiveDrop: false,
                hlSixDrop: false,
                focused: this.state.hlFour,
                hierarchyLevelData: hLevelData
            })

        } else if (drop == "hlFiveDrop") {
            let index = ""
            if (this.state.hlFive != "") {
                for (var i = 0; i < hlevelArray.length; i++) {
                    if (hlevelArray[i] == this.state.hlFive) {
                        index = i
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlTwo != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }
                    }
                }
            } else {

                for (let k = 0; k < hlevelArray.length; k++) {
                    if (hlevelArray[k] == this.state.hlFour) {
                        index = k
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlTwo != hlevelArray[j] && this.state.hlSix != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }


                    }
                }





            }
            this.setState({
                hlOneDrop: false,
                hlTwoDrop: false,
                hlThreeDrop: false,
                hlFourDrop: false,
                hlFiveDrop: true,
                hlSixDrop: false,
                focused: this.state.hlFive,
                hierarchyLevelData: hLevelData
            })

        } else if (drop == "hlSixDrop") {
            let index = ""
            if (this.state.hlSix != "") {
                for (var i = 0; i < hlevelArray.length; i++) {
                    if (hlevelArray[i] == this.state.hlSix) {
                        index = i
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlTwo != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }
                    }
                }
            } else {

                for (let k = 0; k < hlevelArray.length; k++) {
                    if (hlevelArray[k] == this.state.hlFive) {
                        index = k
                    }
                }
                for (let j = 0; j < hlevelArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.hlOne != hlevelArray[j] && this.state.hlThree != hlevelArray[j] && this.state.hlFour != hlevelArray[j] && this.state.hlFive != hlevelArray[j] && this.state.hlTwo != hlevelArray[j]) {
                            hLevelData.push(hlevelArray[j])
                        }

                    }
                }

            }
            this.setState({
                hlOneDrop: false,
                hlTwoDrop: false,
                hlThreeDrop: false,
                hlFourDrop: false,
                hlFiveDrop: false,
                hlSixDrop: true,
                focused: this.state.hlSix,
                hierarchyLevelData: hLevelData
            })
        } else if (drop == "udfOneDrop") {

            let index = ""
            if (this.state.udfOne != "") {
                for (var i = 0; i < udfArray.length; i++) {
                    if (udfArray[i] == this.state.udfOne) {
                        index = i
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfTwo != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }

                    }
                }
            } else {
                udfData = udfArray
            }
            this.setState({
                udfOneDrop: true,
                udfTwoDrop: false,
                udfThreeDrop: false,
                udfFourDrop: false,
                udfFiveDrop: false,
                udfSixDrop: false,
                focused: this.state.udfOne,
                udfData: udfData
            })
        }
        else if (drop == "udfTwoDrop") {

            let index = ""
            if (this.state.udfTwo != "") {
                for (var i = 0; i < udfArray.length; i++) {
                    if (udfArray[i] == this.state.udfTwo) {
                        index = i
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }
                    }
                }
            } else {
                for (let k = 0; k < udfArray.length; k++) {
                    if (udfArray[k] == this.state.udfOne) {
                        index = k
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }

                    }
                }
            }


            this.setState({
                udfOneDrop: false,
                udfTwoDrop: true,
                udfThreeDrop: false,
                udfFourDrop: false,
                udfFiveDrop: false,

                udfSixDrop: false,
                focused: this.state.udfTwo,
                udfData: udfData
            })
        }
        else if (drop == "udfThreeDrop") {
            let index = ""
            if (this.state.udfThree != "") {
                for (var i = 0; i < udfArray.length; i++) {
                    if (udfArray[i] == this.state.udfThree) {
                        index = i
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfTwo != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }
                    }
                }
            } else {
                for (let k = 0; k < udfArray.length; k++) {
                    if (udfArray[k] == this.state.udfTwo) {
                        index = k
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfTwo != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }

                    }
                }
            }
            this.setState({
                udfOneDrop: false,
                udfTwoDrop: false,
                udfThreeDrop: true,
                udfFourDrop: false,
                udfFiveDrop: false,
                udfSixDrop: false,
                focused: this.state.udfThree,
                udfData: udfData
            })
        }
        else if (drop == "udfFourDrop") {
            let index = ""
            if (this.state.udfFour != "") {
                for (var i = 0; i < udfArray.length; i++) {
                    if (udfArray[i] == this.state.udfFour) {
                        index = i
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfTwo != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }
                    }
                }
            } else {
                for (let k = 0; k < udfArray.length; k++) {
                    if (udfArray[k] == this.state.udfThree) {
                        index = k
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfTwo != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }

                    }
                }


            }
            this.setState({
                udfOneDrop: false,
                udfTwoDrop: false,
                udfThreeDrop: false,
                udfFourDrop: true,
                udfFiveDrop: false,
                udfSixDrop: false,
                focused: this.state.udfFour,
                udfData: udfData
            })
        }
        else if (drop == "udfFiveDrop") {

            let index = ""
            if (this.state.udfFive != "") {
                for (var i = 0; i < udfArray.length; i++) {
                    if (udfArray[i] == this.state.udfFive) {
                        index = i
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfTwo != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }
                    }
                }
            } else {

                for (let k = 0; k < udfArray.length; k++) {
                    if (udfArray[k] == this.state.udfFour) {
                        index = k
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfTwo != udfArray[j] && this.state.udfSix != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }


                    }
                }
            }

            this.setState({
                udfOneDrop: false,
                udfTwoDrop: false,
                udfThreeDrop: false,
                udfFourDrop: false,
                udfFiveDrop: true,
                udfSixDrop: false,
                focused: this.state.udfFive,
                udfData: udfData
            })
        }
        else if (drop == "udfSixDrop") {
            let index = ""
            if (this.state.udfSix != "") {
                for (var i = 0; i < udfArray.length; i++) {
                    if (udfArray[i] == this.state.udfSix) {
                        index = i
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfTwo != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }
                    }
                }
            } else {

                for (let k = 0; k < udfArray.length; k++) {
                    if (udfArray[k] == this.state.udfFive) {
                        index = k
                    }
                }
                for (let j = 0; j < udfArray.length; j++) {
                    if (j.toString() > index.toString()) {
                        if (this.state.udfOne != udfArray[j] && this.state.udfThree != udfArray[j] && this.state.udfFour != udfArray[j] && this.state.udfFive != udfArray[j] && this.state.udfTwo != udfArray[j]) {
                            udfData.push(udfArray[j])
                        }

                    }
                }

            }

            this.setState({
                udfOneDrop: false,
                udfTwoDrop: false,
                udfThreeDrop: false,
                udfFourDrop: false,
                udfFiveDrop: false,
                udfSixDrop: true,
                focused: this.state.udfSix,
                udfData: udfData
            })
        }
    }

    onnChange(data, type) {


        if (type == "hlOne") {

            this.setState({
                hlOne: data,
                hlOneDrop: false,
            })

            this.addDelete(data, 'hl1');

        } else if (type == "hlTwo") {

            this.setState({
                hlTwo: data,
                hlTwoDrop: false,
            })
            this.addDelete(data, 'hl2');

        } else if (type == "hlThree") {

            this.setState({
                hlThree: data,
                hlThreeDrop: false,
            })
            this.addDelete(data, 'hl3');

        } else if (type == "hlFour") {

            this.setState({
                hlFour: data,
                hlFourDrop: false
            })
            this.addDelete(data, 'hl4');

        } else if (type == "hlFive") {

            this.setState({
                hlFive: data,
                hlFiveDrop: false

            })
            this.addDelete(data, 'hl5');

        } else if (type == "hlSix") {

            this.setState({
                hlSix: data,
                hlSixDrop: false

            })
            this.addDelete(data, 'hl6');

        } else if (type == "udfOne") {
            this.setState({
                udfOne: data,
                udfOneDrop: false

            })
            this.addDeleteUdf(data, 'udf1');
        }
        else if (type == "udfTwo") {

            this.setState({
                udfTwo: data,
                udfTwoDrop: false

            })
            this.addDeleteUdf(data, 'udf2');

        }
        else if (type == "udfThree") {

            this.setState({
                udfThree: data,
                udfThreeDrop: false

            })
            this.addDeleteUdf(data, 'udf3');

        }
        else if (type == "udfFour") {

            this.setState({
                udfFour: data,
                udfFourDrop: false

            })
            this.addDeleteUdf(data, 'udf4');

        }
        else if (type == "udfFive") {

            this.setState({
                udfFive: data,
                udfFiveDrop: false

            })
            this.addDeleteUdf(data, 'udf5');

        }
        else if (type == "udfSix") {

            this.setState({
                udfSix: data,
                udfSixDrop: false

            })
            this.addDeleteUdf(data, 'udf6');

        }
    }
    onClear() {
        this.setState({
            hlOne: "",
            hlTwo: "",
            hlThree: "",
            hlFour: "",
            hlFive: "",
            hlSix: "",
            udfOne: "",
            udfTwo: "",
            udfThree: "",
            udfFour: "",
            udfFive: "",
            udfSix: "",
            hl1Check: false,
            hl2Check: false,
            hl3Check: false,
            hl4Check: false,
            hl5Check: false,
            hl6Check: false,
            udf1Check: false,
            udf2Check: false,
            udf3Check: false,
            udf4Check: false,
            udf5Check: false,
            udf6Check: false,

        })

    }
    onSubmit() {
        var data = this.props.changeSetting.otbGetHlevel.data.resource.hlevel
        var udfData = this.props.changeSetting.otbGetHlevel.data.resource.udf
        var objectLevel = Object.values(data)
        var objectUdf = Object.values(udfData)
        var hl1data = []
        var udfDataArr = []
        const { hlOne, hLevel, hlThree, hlTwo, hlFour, hlFive, hlSix } = this.state
        const { udfOne, udfTwo, udfThree, udfFour, udfFive, udfSix, udflevel } = this.state
        for (var i = 0; i < objectLevel.length; i++) {
            hl1data.push(objectLevel[i])
        }
        for (var j = 0; j < hl1data.length; j++) {
            var hl1Value = Object.keys(hl1data[j]).join()
            if (hl1Value == hlOne) {
                hLevel.HL1NAME = this.state.hl1Check ? hl1data[j] : ""
            } else if (hl1Value == hlTwo) {
                hLevel.HL2NAME = this.state.hl2Check ? hl1data[j] : ""
            } else if (hl1Value == hlThree) {
                hLevel.HL3NAME = this.state.hl3Check ? hl1data[j] : ""
            } else if (hl1Value == hlFour) {
                hLevel.HL4NAME = this.state.hl4Check ? hl1data[j] : ""
            } else if (hl1Value == hlFive) {
                hLevel.HL5NAME = this.state.hl5Check ? hl1data[j] : ""
            } else if (hl1Value == hlSix) {
                hLevel.HL6NAME = this.state.hl6Check ? hl1data[j] : ""
            }
        }
        for (var k = 0; k < objectUdf.length; k++) {
            udfDataArr.push(objectUdf[k])
        }
        for (var l = 0; l < udfDataArr.length; l++) {
            var udfValue = Object.keys(udfDataArr[l]).join()
            if (udfValue == udfOne) {
                udflevel.UDF1 = this.state.udf1Check ? udfDataArr[l]:""
            } else if (udfValue == udfTwo) {
                udflevel.UDF2 =this.state.udf2Check? udfDataArr[l]:""
            } else if (udfValue == udfThree) {
                udflevel.UDF3 =this.state.udf3Check? udfDataArr[l]:""
            } else if (udfValue == udfFour) {
                udflevel.UDF4 =this.state.udf4Check? udfDataArr[l]:""
            } else if (udfValue == udfFive) {
                udflevel.UDF5 =this.state.udf5Check? udfDataArr[l]:""
            } else if (udfValue == udfSix) {
                udflevel.UDF6 = this.state.udf6Check? udfDataArr[l]:""
            }
        }

        setTimeout(() => {
            let payload = {
                hLevel,
                udflevel
            }
            this.props.settingOtbCreateRequest(payload)
            this.onClear()

        }, 10);

    }
    onnChangeDefault(e, data) {

        if (e.target.id == "level1") {
            this.setState({
                hlOne: "",
                level1: "Select H Level 1",
                hlOneDrop: false,
                focused: data,
                hl1Check: false
            })
            this.addDelete("Clear H Level", "hl1");
        } else if (e.target.id == "level2") {
            this.setState({
                hlTwo: "",
                level2: "Select H Level 2",
                hlTwoDrop: false,
                focused: data,
                hl2Check: false
            })
            this.addDelete("Clear H Level", "hl2");
        } else if (e.target.id == "level3") {
            this.setState({
                hlThree: "",
                level3: "Select H Level 3",
                hlThreeDrop: false,
                focused: data,
                hl3Check: false
            })
            this.addDelete("Clear H Level", "hl3");
        } else if (e.target.id == "level4") {
            this.setState({
                hlFour: "",
                level4: "Select H Level 4",
                hlFourDrop: false,
                focused: data,
                hl4Check: false
            })
            this.addDelete("Clear H Level", "hl4");
        } else if (e.target.id == "level5") {
            this.setState({
                hlFive: "",
                level5: "Select H Level 5",
                hlFiveDrop: false,
                focused: data,
                hl5Check: false
            })
            this.addDelete("Clear H Level", "hl5");
        } else if (e.target.id == "level6") {
            this.setState({
                hlSix: "",
                level6: "Select H Level 6",
                hlSixDrop: false,
                focused: data,
                hl6Check: false
            })
            this.addDelete("Clear H Level", "hl6");
        }

    }
    onchangeUdf(e, data) {
        if (e.target.id == "udf1") {
            this.setState({
                udfOne: "",
                udf1: "Select UDF 1",
                udfOneDrop: false,
                focused: data,
                udf1Check: false
            })
            this.addDeleteUdf("Clear UDF", "udf1")
        } else if (e.target.id == "udf2") {
            this.setState({
                udfTwo: "",
                udf2: "Select UDF 2",
                udfTwoDrop: false,
                focused: data,
                udf2Check: false
            })
            this.addDeleteUdf("Clear UDF", "udf2")
        } else if (e.target.id == "udf3") {
            this.setState({
                udfThree: "",
                udf3: "Select UDF 3",
                udfThreeDrop: false,
                focused: data,
                udf3Check: false
            })
            this.addDeleteUdf("Clear UDF", "udf3")
        } else if (e.target.id == "udf4") {
            this.setState({
                udfFour: "",
                udf4: "Select UDF 4",
                udfFourDrop: false,
                focused: data,
                udf4Check: false
            })
            this.addDeleteUdf("Clear UDF", "udf4")
        } else if (e.target.id == "udf5") {
            this.setState({
                udfFive: "",
                udf5: "Select UDF 5",
                udfFiveDrop: false,
                focused: data,
                udf5Check: false
            })
            this.addDeleteUdf("Clear UDF", "udf5")
        } else if (e.target.id == "udf6") {
            this.setState({
                udfSix: "",
                udf6: "Select UDF 6",
                udfSixDrop: false,
                focused: data,
                udf6Check: false
            })
            this.addDeleteUdf("Clear UDF", "udf6")
        }

    }

    render() {

        return (
            <div className="configurationSetting p-lr-47">
                <div className="configuraionAccordian">
                    <div className="accordion" id="accordionExample">
                        <div className="card">
                            <div className="card-header" id="headingOne">
                                <h3 className="mb-0">

                                    Hierarchy Configuration
                                </h3>
                            </div>
                            <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div className="card-body">
                                    <div className="col-md-4">
                                        <div className="width_70 displayInline">
                                            <div className="row">
                                                <div className="col-md-12 alignMiddle">
                                                    <h5>H Level 1</h5>
                                                    <div className={this.state.hl1Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer" ><input type="checkBox" onClick={(e) => this.checkedhl("hl1Check")} id="symbol" checked={this.state.hl1Check ? true : false} />{this.state.hl1Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className="userModalSelect displayPointer" id="hLevelOne" onClick={(e) => this.openDropDown("hlOneDrop")}>
                                                        <label className={this.state.hlOne == "" || this.state.hlOne == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.hlOne == "" ? this.state.level1 : this.state.hlOne}
                                                            <span>▾</span>
                                                        </label>
                                                    </div>
                                                    {this.state.hlOneDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>
                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="level1" onClick={(e) => this.onnChangeDefault(e, this.state.hlOne)} >{this.state.hlOne != "" ? "Clear H Level 1" : "Select H Level 1"}</li>
                                                            {this.state.hierarchyLevelData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "hlOne")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>
                                                    </div> : null}
                                                </div>
                                            </div>
                                            <div className="row m-top-30">
                                                <div className="col-md-12 alignMiddle" >
                                                    <h5>H Level 4</h5>
                                                    <div className={this.state.hl4Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer" ><input type="checkBox" onClick={(e) => this.checkedhl("hl4Check")} id="symbol" checked={this.state.hl4Check ? true : false} />{this.state.hl4Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.hlThree == "" ? "btnDisabled userModalSelect" : "userModalSelect displayPointer"} id="hLevelFour" onClick={(e) => this.state.hlThree == "" ? null : this.openDropDown("hlFourDrop")}>
                                                        <label className={this.state.hlFour == "" || this.state.hlFour == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.hlFour == "" || this.state.hlFour == undefined ? "Select H Level 4" : this.state.hlFour}
                                                            <span>
                                                                ▾
                                                            </span>
                                                        </label>
                                                    </div>
                                                    {this.state.hlFourDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>
                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="level4" onClick={(e) => this.onnChangeDefault(e, this.state.hlFour)} >{this.state.hlFour != "" ? "Clear H Level 4" : "Select H Level 4"} </li>
                                                            {this.state.hierarchyLevelData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "hlFour")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>
                                                    </div> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="width_70 displayInline">
                                            <div className="row">
                                                <div className="col-md-12 alignMiddle"  >
                                                    <h5>H Level 2</h5>
                                                    <div className={this.state.hl2Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer"  ><input type="checkBox" onChange={(e) => this.checkedhl("hl2Check")} id="symbol" checked={this.state.hl2Check ? true : false} />{this.state.hl2Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.hlOne == "" ? "btnDisabled userModalSelect" : "userModalSelect displayPointer"} id="hLevelTwo" onClick={(e) => this.state.hlOne == "" ? null : this.openDropDown("hlTwoDrop")}>
                                                        <label className={this.state.hlTwo == "" || this.state.hlTwo == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.hlTwo == "" || this.state.hlTwo == undefined ? "Select H Level 2" : this.state.hlTwo}
                                                            <span>
                                                                ▾
                                                            </span>
                                                        </label>
                                                    </div>
                                                    {this.state.hlTwoDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="level2" onClick={(e) => this.onnChangeDefault(e, this.state.hlTwo)} >{this.state.hlFour != "" ? "Clear H Level 2" : "Select H Level 2"}</li>
                                                            {this.state.hierarchyLevelData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "hlTwo")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}

                                                </div>
                                            </div>
                                            <div className="row m-top-30">
                                                <div className="col-md-12 alignMiddle" >

                                                    <h5>H Level 5</h5>
                                                    <div className={this.state.hl5Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer" ><input type="checkBox" onClick={(e) => this.checkedhl("hl5Check")} id="symbol" checked={this.state.hl5Check ? true : false} />{this.state.hl5Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.hlFour == "" ? "btnDisabled userModalSelect" : "userModalSelect displayPointer"} id="hLevelFive" onClick={(e) => this.state.hlFour == "" ? null : this.openDropDown("hlFiveDrop")}>
                                                        <label className={this.state.hlFive == "" || this.state.hlFive == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.hlFive == "" || this.state.hlFive == undefined ? "Select H Level 5" : this.state.hlFive}
                                                            <span>
                                                                ▾
                                                            </span>
                                                        </label>
                                                    </div>


                                                    {this.state.hlFiveDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="level5" onClick={(e) => this.onnChangeDefault(e, this.state.hlFive)} >{this.state.hlFive != "" ? "Clear H Level 5" : "Select H Level 5"}</li>
                                                            {this.state.hierarchyLevelData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "hlFive")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="width_70 displayInline">
                                            <div className="row">
                                                <div className="col-md-12 alignMiddle" >

                                                    <h5>H Level 3</h5>
                                                    <div className={this.state.hl3Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer" ><input type="checkBox" id="symbol" onClick={(e) => this.checkedhl("hl3Check")} checked={this.state.hl3Check ? true : false} />{this.state.hl3Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.hlTwo == "" ? "btnDisabled userModalSelect" : "userModalSelect displayPointer"} id="hLevelThree" onClick={(e) => this.state.hlTwo == "" ? null : this.openDropDown("hlThreeDrop")}>

                                                        <label className={this.state.hlThree == "" || this.state.hlThree == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.hlThree == "" || this.state.hlThree == undefined ? "Select H Level 3" : this.state.hlThree}
                                                            <span>
                                                                ▾
                                                           </span>
                                                        </label>


                                                    </div>


                                                    {this.state.hlThreeDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="level3" onClick={(e) => this.onnChangeDefault(e, this.state.hlThree)} >{this.state.hlThree != "" ? "Clear H Level 3" : "Select H Level 3"}</li>
                                                            {this.state.hierarchyLevelData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "hlThree")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}
                                                </div>
                                            </div>
                                            <div className="row m-top-30">
                                                <div className="col-md-12 alignMiddle" >

                                                    <h5>H Level 6</h5>
                                                    <div className={this.state.hl6Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer" ><input type="checkBox" onClick={(e) => this.checkedhl("hl6Check")} id="symbol" checked={this.state.hl6Check ? true : false} />{this.state.hl6Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.hlFive == "" ? "btnDisabled userModalSelect" : "userModalSelect displayPointer"} id="hLevelSix" onClick={(e) => this.state.hlFive == "" ? null : this.openDropDown("hlSixDrop")}>

                                                        <label className={this.state.hlSix == "" || this.state.hlSix == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.hlSix == "" || this.state.hlSix == undefined ? "Select H Level 6" : this.state.hlSix}
                                                            <span>
                                                                ▾
                                                            </span>
                                                        </label>
                                                    </div>
                                                    {this.state.hlSixDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="level6" onClick={(e) => this.onnChangeDefault(e, this.state.hlSix)} >{this.state.hlThree != "" ? "Clear H Level 6" : "Select H Level 6"}</li>
                                                            {this.state.hierarchyLevelData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "hlSix")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 m-top-25">
                                        <p className="note">Note - Only selected items will be saved for application Hierarchy configuration.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header" id="headingTwo">
                                <h3 className="mb-0">
                                    UDF Configuration
                                    </h3>
                            </div>
                            <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div className="card-body">
                                    <div className="col-md-4">
                                        <div className="width_70 displayInline">
                                            <div className="row">
                                                <div className="col-md-12 alignMiddle">

                                                    <h5>UDF 1</h5>
                                                    <div className={this.state.udf1Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer"  ><input type="checkBox" onChange={(e) => this.checkedhl("udf1Check")} id="symbol" checked={this.state.udf1Check ? true : false} />{this.state.udf1Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className="userModalSelect displayPointer" id="udfOne" onClick={(e) => this.openDropDown("udfOneDrop")}>

                                                        <label className={this.state.udfOne == "" || this.state.udfOne == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.udfOne == "" || this.state.udfOne == undefined ? "Select Udf 1" : this.state.udfOne}
                                                            <span>
                                                                ▾
                                                            </span>
                                                        </label>
                                                    </div>
                                                    {this.state.udfOneDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="udf1" onClick={(e) => this.onchangeUdf(e, this.state.udfOne)} >{this.state.udfOne != "" ? "Clear UDF 1" : "Select UDF 1"}</li>
                                                            {this.state.udfData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "udfOne")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}

                                                </div>
                                            </div>
                                            <div className="row m-top-30">
                                                <div className="col-md-12 alignMiddle">

                                                    <h5>UDF 4</h5>
                                                    <div className={this.state.udf4Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer"  ><input type="checkBox" onChange={(e) => this.checkedhl("udf4Check")} id="symbol" checked={this.state.udf4Check ? true : false} />{this.state.udf4Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.udfThree == "" ? "btnDisabled userModalSelect" : " userModalSelect displayPointer"} id="udfFour" onClick={(e) => this.state.udfThree == "" ? null : this.openDropDown("udfFourDrop")}>

                                                        <label className={this.state.udfFour == "" || this.state.udfFour == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.udfFour == "" || this.state.udfFour == undefined ? "Select Udf 4" : this.state.udfFour}
                                                            <span>
                                                                ▾
                                                            </span>
                                                        </label>


                                                    </div>


                                                    {this.state.udfFourDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="udf4" onClick={(e) => this.onchangeUdf(e, this.state.udfFour)} >{this.state.udfFour != "" ? "Clear UDF 4" : "Select UDF 4"}</li>
                                                            {this.state.udfData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "udfFour")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="width_70 displayInline">
                                            <div className="row">
                                                <div className="col-md-12 alignMiddle">

                                                    <h5>UDF 2</h5>
                                                    <div className={this.state.udf2Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer"  ><input type="checkBox" onChange={(e) => this.checkedhl("udf2Check")} id="symbol" checked={this.state.udf2Check ? true : false} />{this.state.udf2Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.udfOne == "" ? "btnDisabled userModalSelect" : " userModalSelect displayPointer"} id="udfTwo" onClick={(e) => this.state.udfOne == "" ? null : this.openDropDown("udfTwoDrop")}>

                                                        <label className={this.state.udfTwo == "" || this.state.udfTwo == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.udfTwo == "" || this.state.udfTwo == undefined ? "Select Udf 2" : this.state.udfTwo}
                                                            <span>
                                                                ▾
    </span>
                                                        </label>


                                                    </div>


                                                    {this.state.udfTwoDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="udf2" onClick={(e) => this.onchangeUdf(e, this.state.udfTwo)} >{this.state.udfTwo != "" ? "Clear UDF 2" : "Select UDF 2"}</li>
                                                            {this.state.udfData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "udfTwo")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}
                                                </div>
                                            </div>
                                            <div className="row m-top-30">
                                                <div className="col-md-12 alignMiddle">

                                                    <h5>UDF 5</h5>
                                                    <div className={this.state.udf5Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer"  ><input type="checkBox" onChange={(e) => this.checkedhl("udf5Check")} id="symbol" checked={this.state.udf5Check ? true : false} />{this.state.udf5Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.udfFour == "" ? "btnDisabled userModalSelect" : " userModalSelect displayPointer"} id="udfFive" onClick={(e) => this.state.udfFour == "" ? null : this.openDropDown("udfFiveDrop")}>

                                                        <label className={this.state.udfFive == "" || this.state.udfFive == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.udfFive == "" || this.state.udfFive == undefined ? "Select Udf 5" : this.state.udfFive}
                                                            <span>
                                                                ▾
</span>
                                                        </label>


                                                    </div>


                                                    {this.state.udfFiveDrop ? <div className="dropdownDiv m-top-10" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="udf5" onClick={(e) => this.onchangeUdf(e, this.state.udfFive)} >{this.state.udfFive != "" ? "Clear UDF 5" : "Select UDF 5"}</li>
                                                            {this.state.udfData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "udfFive")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="width_70 displayInline">
                                            <div className="row">
                                                <div className="col-md-12 alignMiddle">
                                                    <h5>UDF 3</h5>
                                                    <div className={this.state.udf3Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer"  ><input type="checkBox" onChange={(e) => this.checkedhl("udf3Check")} id="symbol" checked={this.state.udf3Check ? true : false} />{this.state.udf3Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>

                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.udfTwo == "" ? "btnDisabled userModalSelect" : " userModalSelect displayPointer"} id="udfThree" onClick={(e) => this.state.udfTwo == "" ? null : this.openDropDown("udfThreeDrop")}>

                                                        <label className={this.state.udfThree == "" || this.state.udfThree == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.udfThree == "" || this.state.udfThree == undefined ? "Select Udf 3" : this.state.udfThree}
                                                            <span>
                                                                ▾
</span>
                                                        </label>

                                                    </div>


                                                    {this.state.udfThreeDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="udf3" onClick={(e) => this.onchangeUdf(e, this.state.udfThree)} >{this.state.udfThree != "" ? "Clear UDF 3" : "Select UDF 3"}</li>
                                                            {this.state.udfData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "udfThree")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}

                                                </div>
                                            </div>
                                            <div className="row m-top-30">
                                                <div className="col-md-12 alignMiddle">

                                                    <h5>UDF 6</h5>
                                                    <div className={this.state.udf6Check ? "configrationCheckbox active float_Right m-lft-auto" : "configrationCheckbox float_Right m-lft-auto"}>
                                                        <label className="checkBoxLabel0 m0 text-middle displayPointer"  ><input type="checkBox" onChange={(e) => this.checkedhl("udf6Check")} id="symbol" checked={this.state.udf6Check ? true : false} />{this.state.udf6Check ? "Selected" : "Select"}<span className="checkmark1"></span> </label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 m-top-10">
                                                    <div className={this.state.udfFive == "" ? "btnDisabled userModalSelect" : " userModalSelect displayPointer"} id="udfSix" onClick={(e) => this.state.udfFive == "" ? null : this.openDropDown("udfSixDrop")}>

                                                        <label className={this.state.udfSix == "" || this.state.udfSix == undefined ? "colorDb displayPointer" : "displayPointer"}>{this.state.udfSix == "" || this.state.udfSix == undefined ? "Select Udf 6" : this.state.udfSix}
                                                            <span>
                                                                ▾
</span>
                                                        </label>


                                                    </div>


                                                    {this.state.udfSixDrop ? <div className="dropdownDiv m-top-5" ref={(e) => this.setWrapperRef(e)}>

                                                        <ul className="dropdownFilterOption">
                                                            <li value="" id="udf6" onClick={(e) => this.onchangeUdf(e, this.state.udfSix)} >{this.state.udfSix != "" ? "Clear UDF 6" : "Select UDF 6"}</li>
                                                            {this.state.udfData.map((data, i) => <li value={data} key={i} onClick={(e) => this.onnChange(data, "udfSix")} >
                                                                {data}
                                                            </li>)}
                                                        </ul>

                                                    </div> : null}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 m-top-25">
                                        <p className="note">Note - Only selected items will be saved for UDF configuration.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="footerDivForm height4per m-top-30">
                            <ul className="list-inline m-lft-0 m-top-10">
                                <li>
                                    {this.state.saveState ?
                                        <li><button className="clear_button_vendor" type="button" onClick={(e) => this.onClear(e)}>Clear</button>
                                            <button type="button" className=" save_button_vendor" onClick={(e) => this.onSubmit(e)}>Save</button>
                                        </li> : <li> <button type="button" className=" clear_button_vendor btnDisabled">Save</button>
                                            <button type="button" className=" save_button_vendor btnDisabled">Clear</button></li>}
                                </li>
                            </ul></div>
                    </div>
                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                </div>
            </div>

        )
    }
}
export default ConfigurationSetting; 