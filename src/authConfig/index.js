import config_development from "./development";
import config_production from "./production";
import config_quality from "./quality";

let AUTH_CONFIG = config_development;

console.log("Environment :: " + process.env.NODE_ENV);

if (process.env.NODE_ENV === "production") {
    AUTH_CONFIG = config_production;
}
if (process.env.NODE_ENV === "quality") {
  AUTH_CONFIG = config_quality;
}

AUTH_CONFIG["ADMIN"] = "Admin";

export { AUTH_CONFIG };