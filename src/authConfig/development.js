const AUTH_CONFIG = {
  // BASE_URL: "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/dev/user/auth",
  BASE_URL:"https://devauth.supplymint.com/user/auth",
  // BASE_URL:"https://qaauth.supplymint.com/user/auth",
  // BASE_URL:"https://prodauth.supplymint.com/user/auth",
  LOGIN: "/login",
  FORGOT_PASSWORD: "/forgot/password",
  RESET_PASSWORD: "/reset/password",
  FORGOT_USER: "/forgot/username",
  ACTIVATE_USER: "/activate/user",
  CHANGE_PASSWORD: "/change/password",
  SWITCH_ORGANIZATION: "/switch/org/info"
  // PAGE_NAME:                 '/page/path'
};    
// https://jowa03ruod.execute-api.ap-south-1.amazonaws.com/dev/user/auth/switch/org/info
export default AUTH_CONFIG;
