export function getDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    let date = yyyy + '-' + mm + '-' + dd
    return date;
}

export function getDateddmmyy() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    let date = dd + '-' + mm + '-' + yyyy
    return date;
}

export function get_mmddyy(value) {
    let dd = value.split('-')[0]
    let mm = value.split('-')[1]
    let yy = value.split('-')[2]
    let date = mm + '-' + dd + '-' + yy
    return date

}

export function getTime() {
    var today = new Date();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return time;
}

export function getMonth() {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var now = new Date();
    let month = months[now.getMonth()]
    return month;
}

export function daysInMonth(month, year) {
    var no = new Date(year, month, 0).getDate().toString();
    return no;
}

export function getYear() {
    var d = new Date();
    var n = d.getFullYear();
    return n;
}

export function getMonthCount() {
    var d = new Date();
    var n = d.getMonth();
    return n + 1;
}

// export  function getcurrentStart() {
//     var today = new Date();
//     var dd = today.getDate();
//     var mm = today.getMonth()+1;
//     var yyyy = today.getFullYear();
//     if (dd < 10) {
//         dd = '0' + dd
//     }
//     if (mm < 10) {
//         mm = '0' + mm
//     }
//     let date = yyyy + '-' + mm + '-' + dd
//     return date;
// }
// export  function getCurrentEnd() {
//     var today = new Date();
//     var dd = today.getDate();
//     var mm = today.getMonth()+1;
//     var yyyy = today.getFullYear()+2;
//     if (dd < 10) {
//         dd = '0' + dd
//     }
//     if (mm < 10) {
//         mm = '0' + mm
//     }
//     let date = yyyy + '-' + mm + '-' + dd
//     return date;
// }

export function getForecastStart() {
    var today = new Date();
    var dd = "1";
    var mm = "5";
    var yyyy = today.getFullYear() - 1;
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    let date = yyyy + '-' + mm + '-' + dd
    return date;
}

export function getForecastEnd() {
    var today = new Date();
    var dd = "31";
    var mm = "3";
    var yyyy = today.getFullYear() + 1;
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    let date = yyyy + '-' + mm + '-' + dd
    return date;
}

export function getMonthByNumber(count) {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let month = months[count - 1]
    return month;
}

export function openMyDrop(e) {
    var nameOfClass = "dropdown-content";
    document.getElementById("dropMenu").classList.add(nameOfClass);
}

export function closeMyDrop(e) {
    var nameOfClass = "dropdown-content";
    document.getElementById("dropMenu").classList.remove(nameOfClass);
}

export function formatComma(value) {
    if (value > 999) {
        let split = value.toString().split('');
        split[value.toString().length - 3 - 1] = split[value.toString().length - 3 - 1].concat(',');
        split = split.join('');
        return split;
    }
    return value;
}
export function FilterData(search, data) {
    return data
        .filter(data => {
            if (data.site.toLowerCase().includes(search.toLowerCase())) {
                return true;
            }
            return false;
        })
}
export function handleChange(e) {
    var panel = document.getElementById("scrollDiv");
    var total = panel.scrollWidth - panel.offsetWidth;
    var percentage = total * ((e.target.value * 2) / 100);
    panel.scrollLeft = percentage;
    if (e.target.value == 1) {
        panel.scrollLeft = 0
    }
    var rangeVal = e.target.value
    return { rangeVal, total }
}
export function handleChangeModal(e) {
    var panel = document.getElementById("scrollDivModal");
    var total = panel.scrollWidth - panel.offsetWidth;
    var percentage = total * ((e.target.value * 2) / 100);
    panel.scrollLeft = percentage;
    if (e.target.value == 1) {
        panel.scrollLeft = 0
    }
    var rangeVal = e.target.value
    return { rangeVal, total }
}


export function FilterStore(search, data) {
    return data
        .filter(data => {
            if (data.toLowerCase().includes(search.toLowerCase())) {
                return true;
            }
            return false;
        })
}

export function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);

};

export const decodeToken = () => {
    var token = sessionStorage.getItem('token')
    let decode = token != "" ? parseJwt(token) : ""
    return decode
}

export function genericDateFormat(format) {
    var requiredFormat = format != undefined && (format.split(/[\H']+/)[0]).toUpperCase();
    requiredFormat = requiredFormat.replace("E", "ddd")
    return requiredFormat
}

export function setToken(resource) {

    sessionStorage.setItem('token', resource.token);
    sessionStorage.setItem('partnerEnterpriseId', resource.user.eid);
    sessionStorage.setItem('partnerEnterpriseName', resource.user.ename);
    sessionStorage.setItem('firstName', resource.user.firstName);
    sessionStorage.setItem('lastName', resource.user.lastName);
    sessionStorage.setItem('roles', JSON.stringify(resource.roles));
    sessionStorage.setItem('userName', resource.user.username);
    sessionStorage.setItem('email', resource.user.email);
    sessionStorage.setItem('bucket', resource.user.bucket);
    sessionStorage.setItem('partnerEnterpriseCode', resource.user.ecode);
    sessionStorage.setItem('gstin', resource.user.gstin)
    sessionStorage.setItem('orgId-name', JSON.stringify(resource.user.organisations))
    sessionStorage.setItem('fileUpload', false);
    sessionStorage.setItem('uType', resource.user.uType)

    sessionStorage.setItem('weeklyAssortmentCode', "");
    sessionStorage.setItem('monthlyAssortmentCode', "");
    sessionStorage.setItem('monthlyStart', "");
    sessionStorage.setItem('monthlyEnd', "");
    sessionStorage.setItem('weeklyStart', "");
    sessionStorage.setItem('weeklyEnd', "");
    sessionStorage.setItem('weeklyGraph', JSON.stringify([]));
    sessionStorage.setItem('monthlyGraph', JSON.stringify([]));
    sessionStorage.setItem('enterprises', JSON.stringify(resource.user.enterprises))
    sessionStorage.setItem('modules', JSON.stringify(resource.modules));
    if (resource.roles.length != 0) {
        sessionStorage.setItem('mid', JSON.stringify(resource.roles[0].id));
        let module = JSON.parse(sessionStorage.getItem('modules'));
        let mid = JSON.parse(sessionStorage.getItem('mid'));
        let data = module[mid]


        for (let i = 0; i < data.length; i++) {
            sessionStorage.setItem(`${data[i].name}`, JSON.stringify(data[i]));
        }
    }

}
export function removeTableScrollDrag(id) {
    const slider = document.querySelector(`#${id}`);
    slider.removeEventListener('mousedown', (e) => {
        isDown = true;
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
    });
}
export function tableScrollDrag(id) {
    const slider = document.querySelector(`#${id}`);
    let isDown = false;
    let startX;
    let scrollLeft;
    slider.addEventListener('mousedown', (e) => {
        isDown = true;
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', () => {
        isDown = false;
    });
    slider.addEventListener('mouseup', () => {
        isDown = false;
    });
    slider.addEventListener('mousemove', (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 2; //scroll-fast
        slider.scrollLeft = scrollLeft - walk;
        console.log(walk);
    });
}