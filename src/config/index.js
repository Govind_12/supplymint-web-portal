import config_development from "./development";
import config_production from "./production";
import config_quality from "./quality";

let CONFIG = config_development;

console.log("Environment :: " + process.env.NODE_ENV);

if (process.env.NODE_ENV === "production") {
  CONFIG = config_production;
}
if (process.env.NODE_ENV === "quality") {
  CONFIG = config_quality;
}
if(process.env.NODE_ENV==="develop"){
  CONFIG = config_development
}
// CONFIG["ADMIN"] = "Admin";

export { CONFIG };
