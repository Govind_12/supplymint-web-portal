const CONFIG = {
  BASE_URL: "https://prodserver.supplymint.com",
  //  BASE_URL:"http://192.168.1.54:5000",
  //      BASE_URL:"http://192.168.43.238:5000",
  //  BASE_URL:"http://192.168.15.98:5000",


  ROLES: "/admin/role",
  SITE_MAPPING: "/admin/sitemap",
  USER: "/admin/user",
  VENDOR_USER: "/core/user",
  CORE_VENDOR: "/core/vendor",
  ORGANIZATION: "/core/org",
  SITE: "/admin/site",
  ALLOCATION: "/admin/allocation",
  MCSTR: "/admin/mcstr",
  MODULE: "/core/rolemaster",
  PI: '/admin/pi',
  PO: '/admin/po',
  VENDOR: "/tenant/vendor",
  REPLENISHMENT: "/rule/engine/job",
  ASSORTMENT: "/assortment",
  INV: "/inv/plan",
  OTB: "/demand/otb",
  STORE_PROFILE: "/analytics/storeprofile",
  DEMAND_PLANNING: "/demand/plan",
  BUDGETED: "/demand/budgeted",
  DATASYNC: "/custom/datasync",
  INV_CONFIG: "/inv/config",

  VENDORPROTAL: "/vendorportal",
  // VENDORLOGIS:"/vendorportal/vendorlogi"
  ARS: "/ars/plan",
  SYSTEM_CONFIG: "/system/config",
  VENDINVOICEMANAGEMENT: "/vendor/invoice/management",
};

export default CONFIG;   