import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import { invalid } from 'moment';
import { OganisationIdName } from '../../../organisationIdName';
export function* forcastRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.forcastClear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/forecast/ondemand`, {
                demandForecast: action.payload.demandForecast,
                frequency: action.payload.frequency,
                predictPeriod: action.payload.predictPeriod,
                chooseYear: action.payload.chooseYear,
                startMonth: action.payload.startMonth,
                configuredOn: action.payload.configuredOn,
                startedOn: action.payload.startedOn,
                totalStore: action.payload.totalStore,
                totalAssortment: action.payload.totalAssortment
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.forcastSuccess(response.data.data));
                let data = {
                    no: 1,
                    type: 1,
                    frequency: "",
                    predictPeriod: "",
                    search: "",
                    status: "",
                }
                yield put(actions.lastForecastRequest(data));
                yield put(actions.getForcastRequest(data));
            } else if (finalResponse.failure) {
                yield put(actions.forcastError(response.data));
            }

        } catch (e) {
            yield put(actions.forcastError("error occurs"));
            console.warn('Some error found in "forcastRequest" action\n', e);
        }
    }
}

export function* storeDPRequest(action) {
    // if (action.payload == undefined) {
    //     // yield put(actions.storeDPClear());
    // } else {
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/store`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.storeDPSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.storeDPError(response.data));
        }

    } catch (e) {
        yield put(actions.storeDPError("error occurs"));
        console.warn('Some error found in "storeDPRequest" action\n', e);
    }
    // }
}

export function* assortmentDPRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.assortmentDPClear());
    } else {
        try {
            let no = action.payload.no;
            let search = action.payload.search;
            let type = action.payload.type;
            let totalCount = action.payload.totalCount;
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/assortment?pageNo=${no}&type=${type}&search=${search}&totalCount=${totalCount}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.assortmentDPSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.assortmentDPError(response.data));
            }

        } catch (e) {
            yield put(actions.assortmentDPError("error occurs"));
            console.warn('Some error found in "assortmentDPRequest" action\n', e);
        }
    }

}


export function* getForcastRequest(action) {
    try {
        let no = action.payload.no;
        let type = action.payload.type;
        let frequency = action.payload.frequency;
        let predictPeriod = action.payload.predictPeriod;
        let search = action.payload.search;
        let status = action.payload.status;
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/custom/get/all?pageNo=${no}&type=${type}&frequency=${frequency}&predictPeriod=${predictPeriod}&status=${status}&search=${search}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getForcastSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getForcastError(response.data));
        }

    } catch (e) {
        yield put(actions.getForcastError("error occurs"));
        console.warn('Some error found in "getForcastRequest" action\n', e);
    }
}

export function* weeklyDPRequest(action) {
    try {
        let type = action.payload;
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/choose/schedule?type=${type}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.weeklyDPSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.weeklyDPError(response.data));
        }

    } catch (e) {
        yield put(actions.weeklyDPError("error occurs"));
        console.warn('Some error found in "weeklyDPRequest" action\n', e);
    }
}

export function* monthlyForRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.monthlyForClear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/generate/monthly/forecast`, {
                assortment: action.payload.assortment,
                startDate: action.payload.startDate,
                endDate: action.payload.endDate,
                userName: sessionStorage.getItem('userName')
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.monthlyForSuccess(response.data.data));
                if (response.data.data.resource != null) {
                    sessionStorage.setItem('monthlyUuid', response.data.data.resource.uuid);
                }
            } else if (finalResponse.failure) {
                yield put(actions.monthlyForError(response.data));
            }

        } catch (e) {
            yield put(actions.monthlyForError("error occurs"));
            console.warn('Some error found in "monthlyForRequest" action\n', e);
        }
    }
}

export function* weeklyForRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.weeklyForClear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/generate/weekly/forecast`, {
                assortment: action.payload.assortment,
                startDate: action.payload.startDate,
                endDate: action.payload.endDate,
                userName: sessionStorage.getItem('userName')
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.weeklyForSuccess(response.data.data));
                if (response.data.data.resource != null) {
                    sessionStorage.setItem('weeklyUuid', response.data.data.resource.uuid);
                }
            } else if (finalResponse.failure) {
                yield put(actions.weeklyForError(response.data));
            }

        } catch (e) {
            yield put(actions.weeklyForError("error occurs"));
            console.warn('Some error found in "weeklyForRequest" action\n', e);
        }
    }
}

export function* tableRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.tableClear());
    } else {
        try {
            let no = action.payload.no;
            let search = action.payload.search;
            let type = action.payload.type;
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/details?pageNo=${no}&type=${type}&search=${search}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.tableSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.tableError(response.data));
            }

        } catch (e) {
            yield put(actions.tableError("error occurs"));
            console.warn('Some error found in "tableRequest" action\n', e);
        }
    }
}

export function* processingBSRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.processingBSClear());
    } else {
        try {
            let fileName = action.payload
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/process?fileName=${fileName}&userName=${sessionStorage.getItem('userName')}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.processingBSSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.processingBSError(response.data));
            }

        } catch (e) {
            yield put(actions.processingBSError("error occurs"));
            console.warn('Some error found in "processingBSRequest" action\n', e);
        }
    }
}

export function* verifyBSRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.verifyBSClear());
    } else {
        try {
            let frequency = action.payload
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/verify?type=${frequency}&userName=${sessionStorage.getItem('userName')}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.verifyBSSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.verifyBSError(response.data));
            }

        } catch (e) {
            yield put(actions.verifyBSError("error occurs"));
            console.warn('Some error found in "verifyBSRequest" action\n', e);
        }
    }
}

export function* successBSRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.successBSClear());
    } else {
        try {
            let frequency = action.payload
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/finaly/data?type=${frequency}&userName=${sessionStorage.getItem('userName')}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.successBSSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.successBSError(response.data));
            }

        } catch (e) {
            yield put(actions.successBSError("error occurs"));
            console.warn('Some error found in "successBSRequest" action\n', e);
        }
    }
}

export function* dpProgressRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.dpProgressClear());
    } else {
        try {
            let frequency = action.payload;
            let uuid = frequency == "WEEKLY" ? sessionStorage.getItem('weeklyUuid') == null || sessionStorage.getItem('weeklyUuid') == "" ? "NA" : sessionStorage.getItem('weeklyUuid') : sessionStorage.getItem('monthlyUuid') == null || sessionStorage.getItem('monthlyUuid') == "" ? "NA" : sessionStorage.getItem('monthlyUuid');
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/download/report?frequency=${frequency}&uuid=${uuid}&userName=${sessionStorage.getItem('userName')}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                if (response.data.data.resource != null) {
                    if (frequency == "WEEKLY") {
                        sessionStorage.setItem('weeklyUuid', response.data.data.resource.uuid);
                    } else {
                        sessionStorage.setItem('monthlyUuid', response.data.data.resource.uuid);
                    }
                }
                yield put(actions.dpProgressSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.dpProgressError(response.data));
            }

        } catch (e) {
            yield put(actions.dpProgressError("error occurs"));
            console.warn('Some error found in "dpProgressRequest" action\n', e);
        }
    }
}

export function* bsStatusRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.bsStatusClear());
    } else {
        try {
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.BUDGETED}/file/upload/status?userName=${sessionStorage.getItem('userName')}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.bsStatusSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.bsStatusError(response.data));
            }

        } catch (e) {
            yield put(actions.bsStatusError("error occurs"));
            console.warn('Some error found in "bsStatusRequest" action\n', e);
        }
    }
}

export function* bsHistoryRequest(action) {
    var { orgIdGlobal } = OganisationIdName()
    if (action.payload == undefined) {
        yield put(actions.bsHistoryClear());
    } else {
        try {
            let no = action.payload.no;
            let type = action.payload.type;
            let search = action.payload.search;
            let uploadedDate = action.payload.uploadedDate == undefined ? "" : action.payload.uploadedDate;
            let fileName = action.payload.fileName == undefined ? "" : action.payload.fileName;
            let totalData = action.payload.totalData == undefined ? "" : action.payload.totalData;
            let valid = action.payload.valid == undefined ? "" : action.payload.valid;
            let invalid = action.payload.invalid == undefined ? "" : action.payload.invalid;
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.BUDGETED}/summary?pageNo=${no}&type=${type}&search=${search}&uploadedDate=${uploadedDate}&fileName=${fileName}&totalData=${totalData}&valid=${valid}&invalid=${invalid}&orgId=${orgIdGlobal}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.bsHistorySuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.bsHistoryError(response.data));
            }

        } catch (e) {
            yield put(actions.bsHistoryError("error occurs"));
            console.warn('Some error found in "bsHistoryRequest" action\n', e);
        }
    }
}


export function* mwHistoryRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.mwHistoryClear());
    } else {
        try {
            let no = action.payload.no;
            let type = action.payload.type;
            let search = action.payload.search;
            let frequency = action.payload.frequency;
            let assortment = action.payload.assortment == undefined ? "" : action.payload.assortment;
            let startDate = action.payload.startDate == undefined ? "" : action.payload.startDate;
            let endDate = action.payload.endDate == undefined ? "" : action.payload.endDate;
            let createdOn = action.payload.createdOn == undefined ? "" : action.payload.createdOn;
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/all/history?pageNo=${no}&type=${type}&frequency=${frequency}&assortment=${assortment}&startDate=${startDate}&endDate=${endDate}&createdOn=${createdOn}&search=${search}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.mwHistorySuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.mwHistoryError(response.data));
            }

        } catch (e) {
            yield put(actions.mwHistoryError("error occurs"));
            console.warn('Some error found in "mwHistoryRequest" action\n', e);
        }
    }
}

export function* getForecastStatusRequest(action) {
    try {
        let runId = action.payload;
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/forecast/status?runId=${runId}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getForecastStatusSuccess(response.data.data));
            if (response.data.data.resource.status == "Failed" || response.data.data.resource.status == "Succeeded") {
                yield put(actions.lastForecastRequest());
            }
        } else if (finalResponse.failure) {
            yield put(actions.getForecastStatusError(response.data));
        }

    } catch (e) {
        yield put(actions.getForecastStatusError("error occurs"));
        console.warn('Some error found in "getForecastStatusRequest" action\n', e);
    }
}

export function* lastForecastRequest(action) {
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/lastForecast/history`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.lastForecastSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.lastForecastError(response.data));
        }

    } catch (e) {
        yield put(actions.lastForecastError("error occurs"));
        console.warn('Some error found in "lastForecastRequest" action\n', e);
    }
}

//otb

export function* createPlanRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.createPlanClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.OTB}/create`, {
                'planName': action.payload.planName,
                'planType': action.payload.planType,
                'isUfAsd': action.payload.isUfAsd,
                'isUfDfd': action.payload.isUfDfd,
                'increasePercentageAsd': action.payload.increasePercentageAsd,
                'increasePercentageDfd': action.payload.increasePercentageDfd,
                'activeDataOption': action.payload.activeDataOption,
                'makeCopy': action.payload.makeCopy,
                'createdCopy': action.payload.createdCopy,
                'isUserOTBPlan': action.payload.isUserOTBPlan

            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.createPlanSuccess(response.data.data));
                yield put(actions.getOtbStatusRequest());

            } else if (finalResponse.failure) {
                yield put(actions.createPlanError(response.data));
            }

        } catch (e) {
            yield put(actions.createPlanError("error occurs"));
            console.warn('Some error found in "createPlan" action\n', e);
        }
    }
}


export function* getActivePlanRequest(action) {
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.OTB}/get/activePlan`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getActivePlanSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getActivePlanError(response.data));
        }

    } catch (e) {
        yield put(actions.getActivePlanError("error occurs"));
        console.warn('Some error found in "getActivePlan" action\n', e);
    }
}



export function* updatePlanRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updatePlanClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.OTB}/update`, {
                "planId": action.payload.planId,
                "planName": action.payload.planName,
                "isUfAsd": action.payload.isUfAsd,
                "isUfDfd": action.payload.isUfDfd,
                "increasePercentageAsd": action.payload.increasePercentageAsd,
                "increasePercentageDfd": action.payload.increasePercentageDfd,
                "activeDataOption": action.payload.activeDataOption,
                "isUserOTBPlan": action.payload.isUserOTBPlan

            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updatePlanSuccess(response.data.data));
                yield put(actions.getOtbStatusRequest());
            } else if (finalResponse.failure) {
                yield put(actions.updatePlanError(response.data));
            }

        } catch (e) {
            yield put(actions.updatePlanError("error occurs"));
            console.warn('Some error found in "getActivePlan" action\n', e);
        }
    }
}


export function* updateActivePlanRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updateActivePlanClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.OTB}/update/activePlan`, {
                "planName": action.payload.planName,
                "planId": action.payload.planId,

            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateActivePlanSuccess(response.data.data));
                yield put(actions.getActivePlanRequest());
            } else if (finalResponse.failure) {
                yield put(actions.updateActivePlanError(response.data));
            }

        } catch (e) {
            yield put(actions.updateActivePlanError("error occurs"));
            console.warn('Some error found in "getActivePlan" action\n', e);
        }
    }
}


export function* removePlanRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.removePlanClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.OTB}/remove/activePlan`, {
                "planId": action.payload.planId,
                "planName": action.payload.planName,


            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.removePlanSuccess(response.data.data));
                yield put(actions.getActivePlanRequest());
            } else if (finalResponse.failure) {
                yield put(actions.removePlanError(response.data));
            }

        } catch (e) {
            yield put(actions.removePlanError("error occurs"));
            console.warn('Some error found in "getActivePlan" action\n', e);
        }
    }
}



// ______________________--USE FOR ANOTHER API_______________________________________

export function* getMakeCopyRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getMakeCopyClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.OTB}/remove/activePlan`, {
                'planName': action.payload.planName,
                'planType': action.payload.planType,
                'isUfAsd': action.payload.isUfAsd,
                'isUfDfd': action.payload.isUfDfd,
                'increasePercentageAsd': action.payload.increasePercentageAsd,
                'increasePercentageDfd': action.payload.increasePercentageDfd,
                'activeDataOption': action.payload.activeDataOption,
                'makeCopy': action.payload.makeCopy,
                'createdCopy': action.payload.createdCopy,

            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getMakeCopySuccess(response.data.data));
                yield put(actions.getOtbStatusRequest());

            } else if (finalResponse.failure) {
                yield put(actions.getMakeCopyError(response.data));
            }

        } catch (e) {
            yield put(actions.getMakeCopyError("error occurs"));
            console.warn('Some error found in "getActivePlan" action\n', e);
        }
    }
}

//get otb plan
export function* getOtbPlanRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getOtbPlanClear());
    }
    else {
        try {
            let no = action.payload.no;
            let activePlan = action.payload.activePlan;

            let frequency = action.payload.frequency;
            let startDate = action.payload.startDate;
            let endDate = action.payload.endDate;
            let planId = action.payload.planId;
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.OTB}/get/otbplan?pageNo=${no}&activePlan=${activePlan}&frequency=${frequency}&startDate=${startDate}&endDate=${endDate}&planId=${planId}`, {


            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getOtbPlanSuccess(response.data.data));

            } else if (finalResponse.failure) {
                yield put(actions.getOtbPlanError(response.data));
            }

        } catch (e) {
            yield put(actions.getOtbPlanError("error occurs"));
            console.warn('Some error found in "getOtbPlan" action\n', e);
        }
    }
}


// ________________________UPDATESALE AND TOTAL______________________________________

export function* totalOtbRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.totalOtbClear());
    }
    else {
        try {
            let activePlan = action.payload.activePlan;

            let frequency = action.payload.frequency;
            let startDate = action.payload.startDate;
            let endDate = action.payload.endDate;
            let planId = action.payload.planId;
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.OTB}/get/total/otbplan?activePlan=${activePlan}&frequency=${frequency}&startDate=${startDate}&endDate=${endDate}&planId=${planId}`, {


            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.totalOtbSuccess(response.data.data));

            } else if (finalResponse.failure) {
                yield put(actions.totalOtbError(response.data));
            }

        } catch (e) {
            yield put(actions.totalOtbError("error occurs"));
            console.warn('Some error found in "totalOtb" action\n', e);
        }
    }
}

export function* planSaleUpdateRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.planSaleUpdateClear());
    }
    else {
        try {

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.OTB}/update/modification`, {
                "activePlan": action.payload.activePlan,

                "billDate": action.payload.billDate,
                "assortmentCode": action.payload.assortmentCode,
                "newPlanSales": action.payload.newPlanSales,
                "planId": action.payload.planId,
                "otbValue": action.payload.otbValue,
                "openPurchaseOrder": action.payload.openPurchaseOrder,
                "openingStock": action.payload.openingStock,
                "closingInventory": action.payload.closingInventory,
                "markDown": action.payload.markDown,


                "isUserCreatePlan": action.payload.isUserCreatePlan

            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.planSaleUpdateSuccess(response.data.data));

            } else if (finalResponse.failure) {
                yield put(actions.planSaleUpdateError(response.data));
            }

        } catch (e) {
            yield put(actions.planSaleUpdateError("error occurs"));
            console.warn('Some error found in "planSaleUpdate" action\n', e);
        }
    }
}

export function* getOtbStatusRequest(action) {

    try {

        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.OTB}/get/otbStatus`, {


        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getOtbStatusSuccess(response.data.data));

        } else if (finalResponse.failure) {
            yield put(actions.getOtbStatusError(response.data));
        }

    } catch (e) {
        yield put(actions.getOtbStatusError("error occurs"));
        console.warn('Some error found in "getOtbStatus" action\n', e);
    }
}

export function* getAssortmentHistoryRequest(action) {
    try {
        let no = action.payload.no;
        let search = action.payload.search;
        let type = action.payload.type;
        let totalCount = action.payload.totalCount
        let year = action.payload.year
        let frequency = action.payload.frequency
        let startDate = action.payload.startDate
        let endDate = action.payload.endDate
        let duration = action.payload.duration
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/assortment/history?pageNo=${no}&type=${type}&totalCount=${totalCount}&search=${search}&year=${year}&frequency=${frequency}&startDate=${startDate}&endDate=${endDate}&duration=${duration}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAssortmentHistorySuccess(response.data.data));

        } else if (finalResponse.failure) {
            yield put(actions.getAssortmentHistoryError(response.data));
        }
    } catch (e) {
        yield put(actions.getAssortmentHistoryError("error occurs"));
        console.warn('Some error found in "getAssortmentHistory" action\n', e);
    }
}

export function* getPreviousAssortmentRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/get/previous/assortment`, {
            "pageNo": action.payload.no,
            "type": action.payload.type,
            "totalCount": action.payload.totalCount,
            "runId": action.payload.runId,
            "search": action.payload.search,
            "assortmentCode": action.payload.assortmentCode,
            "startDate": action.payload.startDate,
            "endDate": action.payload.endDate
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getPreviousAssortmentSuccess(response.data.data));

        } else if (finalResponse.failure) {
            yield put(actions.getPreviousAssortmentError(response.data));
        }
    } catch (e) {
        yield put(actions.getPreviousAssortmentError("error occurs"));
        console.warn('Some error found in "getPreviousAssortment" action\n', e);
    }
}



export function* downloadPreviousForcastRequest(action) {
    try {

        let frequency = action.payload.frequency
        let startDate = action.payload.startDate
        let endDate = action.payload.endDate
        let runId = action.payload.runId
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/download/previous/forecast?runId=${runId}&startDate=${startDate}&endDate=${endDate}&frequency=${frequency}`, {

        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.downloadPreviousForcastSuccess(response.data.data));

        } else if (finalResponse.failure) {
            yield put(actions.downloadPreviousForcastError(response.data));
        }
    } catch (e) {
        yield put(actions.downloadPreviousForcastError("error occurs"));
        console.warn('Some error found in "downloadPreviousForcast" action\n', e);
    }
}