import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';


let initialState = {
    forcast: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    storeDP: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    assortmentDP: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getForcast: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    weeklyDP: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    monthlyFor: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    weeklyFor: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    table: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    processingBS: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    verifyBS: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    successBS: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    dpProgress: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    bsStatus: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    bsHistory: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    mwHistory: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getForecastStatus: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    lastForecast: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    createPlan:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getActivePlan:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''    
    },
    updatePlan:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''     
    },
    removePlan:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''    
    },
    updateActivePlan:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''     
    },
    getMakeCopy:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''     
    },
    getOtbPlan:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''    
    },
    totalOtb:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''     
    },
    planSaleUpdate:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''    
    },
    getOtbStatus:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''  
    },
    getAssortmentHistory:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''  
    },
    getPreviousAssortment:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    downloadPreviousForcast:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    }
};




const forcastRequest = (state, action) => update(state, {
    forcast: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const forcastSuccess = (state, action) => update(state, {
    forcast: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'forcast success' }
    }
});
const forcastError = (state, action) => update(state, {
    forcast: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const forcastClear = (state, action) => update(state, {
    forcast: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const storeDPRequest = (state, action) => update(state, {
    storeDP: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});


const storeDPClear = (state, action) => update(state, {
    storeDP: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const storeDPSuccess = (state, action) => update(state, {
    storeDP: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'storeDP success' }
    }
});
const storeDPError = (state, action) => update(state, {
    storeDP: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const assortmentDPRequest = (state, action) => update(state, {
    assortmentDP: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const assortmentDPClear = (state, action) => update(state, {
    assortmentDP: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const assortmentDPSuccess = (state, action) => update(state, {
    assortmentDP: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'assortmentDP success' }
    }
});
const assortmentDPError = (state, action) => update(state, {
    assortmentDP: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getForcastRequest = (state, action) => update(state, {
    getForcast: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getForcastSuccess = (state, action) => update(state, {
    getForcast: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getForcast success' }
    }
});
const getForcastError = (state, action) => update(state, {
    getForcast: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const weeklyDPRequest = (state, action) => update(state, {
    weeklyDP: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const weeklyDPSuccess = (state, action) => update(state, {
    weeklyDP: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'weeklyDP success' }
    }
});
const weeklyDPError = (state, action) => update(state, {
    weeklyDP: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const monthlyForRequest = (state, action) => update(state, {
    monthlyFor: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const monthlyForSuccess = (state, action) => update(state, {
    monthlyFor: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'monthlyFor success' }
    }
});
const monthlyForError = (state, action) => update(state, {
    monthlyFor: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const monthlyForClear = (state, action) => update(state, {
    monthlyFor: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const weeklyForRequest = (state, action) => update(state, {
    weeklyFor: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const weeklyForSuccess = (state, action) => update(state, {
    weeklyFor: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'weeklyFor success' }
    }
});
const weeklyForError = (state, action) => update(state, {
    weeklyFor: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const weeklyForClear = (state, action) => update(state, {
    weeklyFor: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const tableRequest = (state, action) => update(state, {
    table: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const tableSuccess = (state, action) => update(state, {
    table: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'table success' }
    }
});
const tableError = (state, action) => update(state, {
    table: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const tableClear = (state, action) => update(state, {
    table: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const processingBSRequest = (state, action) => update(state, {
    processingBS: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const processingBSSuccess = (state, action) => update(state, {
    processingBS: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'processingBS success' }
    }
});
const processingBSError = (state, action) => update(state, {
    processingBS: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const processingBSClear = (state, action) => update(state, {
    processingBS: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const verifyBSRequest = (state, action) => update(state, {
    verifyBS: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const verifyBSSuccess = (state, action) => update(state, {
    verifyBS: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'verifyBS success' }
    }
});
const verifyBSError = (state, action) => update(state, {
    verifyBS: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const verifyBSClear = (state, action) => update(state, {
    verifyBS: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const successBSRequest = (state, action) => update(state, {
    successBS: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const successBSSuccess = (state, action) => update(state, {
    successBS: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'successBS success' }
    }
});
const successBSError = (state, action) => update(state, {
    successBS: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const successBSClear = (state, action) => update(state, {
    successBS: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const dpProgressRequest = (state, action) => update(state, {
    dpProgress: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const dpProgressSuccess = (state, action) => update(state, {
    dpProgress: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'dpProgress success' }
    }
});
const dpProgressError = (state, action) => update(state, {
    dpProgress: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const dpProgressClear = (state, action) => update(state, {
    dpProgress: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const bsStatusRequest = (state, action) => update(state, {
    bsStatus: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const bsStatusSuccess = (state, action) => update(state, {
    bsStatus: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'bsStatus success' }
    }
});
const bsStatusError = (state, action) => update(state, {
    bsStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const bsStatusClear = (state, action) => update(state, {
    bsStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const bsHistoryRequest = (state, action) => update(state, {
    bsHistory: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const bsHistorySuccess = (state, action) => update(state, {
    bsHistory: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'bsHistory success' }
    }
});
const bsHistoryError = (state, action) => update(state, {
    bsHistory: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const bsHistoryClear = (state, action) => update(state, {
    bsHistory: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const mwHistoryRequest = (state, action) => update(state, {
    mwHistory: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const mwHistorySuccess = (state, action) => update(state, {
    mwHistory: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'mwHistory success' }
    }
});
const mwHistoryError = (state, action) => update(state, {
    mwHistory: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const mwHistoryClear = (state, action) => update(state, {
    mwHistory: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


const getForecastStatusRequest = (state, action) => update(state, {
    getForecastStatus: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getForecastStatusSuccess = (state, action) => update(state, {
    getForecastStatus: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getForecastStatus success' }
    }
});
const getForecastStatusError = (state, action) => update(state, {
    getForecastStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getForecastStatusClear = (state, action) => update(state, {
    getForecastStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const lastForecastRequest = (state, action) => update(state, {
    lastForecast: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const lastForecastSuccess = (state, action) => update(state, {
    lastForecast: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'lastForecast success' }
    }
});
const lastForecastError = (state, action) => update(state, {
    lastForecast: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const lastForecastClear = (state, action) => update(state, {
    lastForecast: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


const createPlanRequest = (state, action) => update(state, {
    createPlan: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const createPlanSuccess = (state, action) => update(state, {
    createPlan: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const createPlanError = (state, action) => update(state, {
    createPlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const createPlanClear = (state, action) => update(state, {
    createPlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


const getActivePlanRequest = (state, action) => update(state, {
    getActivePlan: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getActivePlanSuccess = (state, action) => update(state, {
    getActivePlan: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const getActivePlanError = (state, action) => update(state, {
    getActivePlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getActivePlanClear = (state, action) => update(state, {
    getActivePlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


const updatePlanRequest = (state, action) => update(state, {
    updatePlan: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updatePlanSuccess = (state, action) => update(state, {
    updatePlan: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const updatePlanError = (state, action) => update(state, {
    updatePlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const updatePlanClear = (state, action) => update(state, {
    updatePlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


const updateActivePlanRequest = (state, action) => update(state, {
    updateActivePlan: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateActivePlanSuccess = (state, action) => update(state, {
    updateActivePlan: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const updateActivePlanError = (state, action) => update(state, {
    updateActivePlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const updateActivePlanClear = (state, action) => update(state, {
    updateActivePlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const removePlanRequest = (state, action) => update(state, {
    removePlan: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const removePlanSuccess = (state, action) => update(state, {
    removePlan: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const removePlanError = (state, action) => update(state, {
    removePlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const removePlanClear = (state, action) => update(state, {
    removePlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

// ___________________________________MAKE A COPY_______________________________


const getMakeCopyRequest = (state, action) => update(state, {
    getMakeCopy: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getMakeCopySuccess = (state, action) => update(state, {
    getMakeCopy: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const getMakeCopyError = (state, action) => update(state, {
    getMakeCopy: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getMakeCopyClear = (state, action) => update(state, {
    getMakeCopy: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});



const getOtbPlanRequest = (state, action) => update(state, {
    getOtbPlan: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getOtbPlanSuccess = (state, action) => update(state, {
    getOtbPlan: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const getOtbPlanError = (state, action) => update(state, {
    getOtbPlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getOtbPlanClear = (state, action) => update(state, {
    getOtbPlan: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const totalOtbRequest = (state, action) => update(state, {
    totalOtb: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const totalOtbSuccess = (state, action) => update(state, {
    totalOtb: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const totalOtbError = (state, action) => update(state, {
    totalOtb: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const totalOtbClear = (state, action) => update(state, {
    totalOtb: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


const planSaleUpdateRequest = (state, action) => update(state, {
    planSaleUpdate: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const planSaleUpdateSuccess = (state, action) => update(state, {
    planSaleUpdate: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const planSaleUpdateError = (state, action) => update(state, {
    planSaleUpdate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const planSaleUpdateClear = (state, action) => update(state, {
    planSaleUpdate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});
//getOtbStatus
const getOtbStatusRequest = (state, action) => update(state, {
    getOtbStatus: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getOtbStatusSuccess = (state, action) => update(state, {
    getOtbStatus: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const getOtbStatusError = (state, action) => update(state, {
    getOtbStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getOtbStatusClear = (state, action) => update(state, {
    getOtbStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});

const getAssortmentHistoryRequest = (state, action) => update(state, {
    getAssortmentHistory: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAssortmentHistorySuccess = (state, action) => update(state, {
    getAssortmentHistory: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const getAssortmentHistoryError = (state, action) => update(state, {
    getAssortmentHistory: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getAssortmentHistoryClear = (state, action) => update(state, {
    getAssortmentHistory: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


const getPreviousAssortmentRequest = (state, action) => update(state, {
    getPreviousAssortment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getPreviousAssortmentSuccess = (state, action) => update(state, {
    getPreviousAssortment: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const getPreviousAssortmentError = (state, action) => update(state, {
    getPreviousAssortment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getPreviousAssortmentClear = (state, action) => update(state, {
    getPreviousAssortment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


const downloadPreviousForcastRequest = (state, action) => update(state, {
    downloadPreviousForcast: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const downloadPreviousForcastSuccess = (state, action) => update(state, {
    downloadPreviousForcast: { 
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: {  $set: action.payload  }
    }
});
const downloadPreviousForcastError = (state, action) => update(state, {
    downloadPreviousForcast: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const downloadPreviousForcastClear = (state, action) => update(state, {
    downloadPreviousForcast: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: ''}
    }
});


export default handleActions({
    [constants.FORCAST_CLEAR]: forcastClear,
    [constants.FORCAST_REQUEST]: forcastRequest,
    [constants.FORCAST_SUCCESS]: forcastSuccess,
    [constants.FORCAST_ERROR]: forcastError,

    [constants.STOREDP_CLEAR]: storeDPClear,
    [constants.STOREDP_REQUEST]: storeDPRequest,
    [constants.STOREDP_SUCCESS]: storeDPSuccess,
    [constants.STOREDP_ERROR]: storeDPError,

    [constants.ASSORTMENTDP_CLEAR]: assortmentDPClear,
    [constants.ASSORTMENTDP_REQUEST]: assortmentDPRequest,
    [constants.ASSORTMENTDP_SUCCESS]: assortmentDPSuccess,
    [constants.ASSORTMENTDP_ERROR]: assortmentDPError,

    [constants.GET_FORCAST_REQUEST]: getForcastRequest,
    [constants.GET_FORCAST_SUCCESS]: getForcastSuccess,
    [constants.GET_FORCAST_ERROR]: getForcastError,

    [constants.WEEKLYDP_REQUEST]: weeklyDPRequest,
    [constants.WEEKLYDP_SUCCESS]: weeklyDPSuccess,
    [constants.WEEKLYDP_ERROR]: weeklyDPError,

    [constants.MONTHLYFOR_REQUEST]: monthlyForRequest,
    [constants.MONTHLYFOR_SUCCESS]: monthlyForSuccess,
    [constants.MONTHLYFOR_ERROR]: monthlyForError,
    [constants.MONTHLYFOR_CLEAR]: monthlyForClear,

    [constants.WEEKLYFOR_REQUEST]: weeklyForRequest,
    [constants.WEEKLYFOR_SUCCESS]: weeklyForSuccess,
    [constants.WEEKLYFOR_ERROR]: weeklyForError,
    [constants.WEEKLYFOR_CLEAR]: weeklyForClear,

    [constants.TABLE_REQUEST]: tableRequest,
    [constants.TABLE_SUCCESS]: tableSuccess,
    [constants.TABLE_ERROR]: tableError,
    [constants.TABLE_CLEAR]: tableClear,

    [constants.PROCESSINGBS_REQUEST]: processingBSRequest,
    [constants.PROCESSINGBS_SUCCESS]: processingBSSuccess,
    [constants.PROCESSINGBS_ERROR]: processingBSError,
    [constants.PROCESSINGBS_CLEAR]: processingBSClear,

    [constants.VERIFYBS_REQUEST]: verifyBSRequest,
    [constants.VERIFYBS_SUCCESS]: verifyBSSuccess,
    [constants.VERIFYBS_ERROR]: verifyBSError,
    [constants.VERIFYBS_CLEAR]: verifyBSClear,

    [constants.SUCCESSBS_REQUEST]: successBSRequest,
    [constants.SUCCESSBS_SUCCESS]: successBSSuccess,
    [constants.SUCCESSBS_ERROR]: successBSError,
    [constants.SUCCESSBS_CLEAR]: successBSClear,

    [constants.DPPROGRESS_REQUEST]: dpProgressRequest,
    [constants.DPPROGRESS_SUCCESS]: dpProgressSuccess,
    [constants.DPPROGRESS_ERROR]: dpProgressError,
    [constants.DPPROGRESS_CLEAR]: dpProgressClear,

    [constants.BSSTATUS_REQUEST]: bsStatusRequest,
    [constants.BSSTATUS_SUCCESS]: bsStatusSuccess,
    [constants.BSSTATUS_ERROR]: bsStatusError,
    [constants.BSSTATUS_CLEAR]: bsStatusClear,

    [constants.BSHISTORY_REQUEST]: bsHistoryRequest,
    [constants.BSHISTORY_SUCCESS]: bsHistorySuccess,
    [constants.BSHISTORY_ERROR]: bsHistoryError,
    [constants.BSHISTORY_CLEAR]: bsHistoryClear,

    [constants.MWHISTORY_REQUEST]: mwHistoryRequest,
    [constants.MWHISTORY_SUCCESS]: mwHistorySuccess,
    [constants.MWHISTORY_ERROR]: mwHistoryError,
    [constants.MWHISTORY_CLEAR]: mwHistoryClear,

    [constants.GET_FORECAST_STATUS_REQUEST]: getForecastStatusRequest,
    [constants.GET_FORECAST_STATUS_SUCCESS]: getForecastStatusSuccess,
    [constants.GET_FORECAST_STATUS_ERROR]: getForecastStatusError,
    [constants.GET_FORECAST_STATUS_CLEAR]: getForecastStatusClear,

    [constants.LAST_FORECAST_REQUEST]: lastForecastRequest,
    [constants.LAST_FORECAST_SUCCESS]: lastForecastSuccess,
    [constants.LAST_FORECAST_ERROR]: lastForecastError,
    [constants.LAST_FORECAST_CLEAR]: lastForecastClear,
   

    [constants.CREATE_PLAN_REQUEST]: createPlanRequest,
    [constants.CREATE_PLAN_SUCCESS]: createPlanSuccess,
    [constants.CREATE_PLAN_ERROR]: createPlanError,
    [constants.CREATE_PLAN_CLEAR]: createPlanClear,

    [constants.GET_ACTIVE_PLAN_REQUEST]: getActivePlanRequest,
    [constants.GET_ACTIVE_PLAN_SUCCESS]: getActivePlanSuccess,
    [constants.GET_ACTIVE_PLAN_ERROR]: getActivePlanError,
    [constants.GET_ACTIVE_PLAN_CLEAR]: getActivePlanClear,

    [constants.UPDATE_PLAN_REQUEST]: updatePlanRequest,
    [constants.UPDATE_PLAN_SUCCESS]: updatePlanSuccess,
    [constants.UPDATE_PLAN_ERROR]: updatePlanError,
    [constants.UPDATE_PLAN_CLEAR]: updatePlanClear,


    [constants.UPDATE_ACTIVE_PLAN_REQUEST]: updateActivePlanRequest,
    [constants.UPDATE_ACTIVE_PLAN_SUCCESS]: updateActivePlanSuccess,
    [constants.UPDATE_ACTIVE_PLAN_ERROR]: updateActivePlanError,
    [constants.UPDATE_ACTIVE_PLAN_CLEAR]: updateActivePlanClear,

    [constants.REMOVE_PLAN_REQUEST]: removePlanRequest,
    [constants.REMOVE_PLAN_SUCCESS]: removePlanSuccess,
    [constants.REMOVE_PLAN_ERROR]: removePlanError,
    [constants.REMOVE_PLAN_CLEAR]: removePlanClear,

    [constants.GET_MAKE_COPY_REQUEST]: getMakeCopyRequest,
    [constants.GET_MAKE_COPY_SUCCESS]: getMakeCopySuccess,
    [constants.GET_MAKE_COPY_ERROR]: getMakeCopyError,
    [constants.GET_MAKE_COPY_CLEAR]: getMakeCopyClear,

    [constants.GET_OTB_PLAN_REQUEST]:getOtbPlanRequest,
    [constants.GET_OTB_PLAN_SUCCESS] : getOtbPlanSuccess,
    [constants.GET_OTB_PLAN_ERROR] :getOtbPlanError,
    [constants.GET_OTB_PLAN_CLEAR]:getOtbPlanClear,

    [constants.TOTAL_OTB_REQUEST]:totalOtbRequest,
    [constants.TOTAL_OTB_SUCCESS] :totalOtbSuccess,
    [constants.TOTAL_OTB_ERROR] :totalOtbError,
    [constants.TOTAL_OTB_CLEAR]:totalOtbClear,

    [constants.PLAN_SALE_UPDATE_REQUEST]:planSaleUpdateRequest,
    [constants.PLAN_SALE_UPDATE_SUCCESS] : planSaleUpdateSuccess,
    [constants.PLAN_SALE_UPDATE_ERROR] :planSaleUpdateError,
    [constants.PLAN_SALE_UPDATE_CLEAR]:planSaleUpdateClear,

    [constants.GET_OTB_STATUS_REQUEST]:getOtbStatusRequest,
    [constants.GET_OTB_STATUS_SUCCESS] :getOtbStatusSuccess,
    [constants.GET_OTB_STATUS_ERROR] :getOtbStatusError,
    [constants.GET_OTB_STATUS_CLEAR]:getOtbStatusClear,

    [constants.GET_ASSORTMENT_HISTORY_REQUEST]:getAssortmentHistoryRequest,
    [constants.GET_ASSORTMENT_HISTORY_SUCCESS] :getAssortmentHistorySuccess,
    [constants.GET_ASSORTMENT_HISTORY_ERROR] :getAssortmentHistoryError,
    [constants.GET_ASSORTMENT_HISTORY_CLEAR]:getAssortmentHistoryClear,

    [constants.GET_PREVIOUS_ASSORTMENT_REQUEST]:getPreviousAssortmentRequest,
    [constants.GET_PREVIOUS_ASSORTMENT_SUCCESS] :getPreviousAssortmentSuccess,
    [constants.GET_PREVIOUS_ASSORTMENT_ERROR] :getPreviousAssortmentError,
    [constants.GET_PREVIOUS_ASSORTMENT_CLEAR]:getPreviousAssortmentClear,

    [constants.DOWNLOAD_PREVIOUS_FORCAST_REQUEST]:downloadPreviousForcastRequest,
    [constants.DOWNLOAD_PREVIOUS_FORCAST_ERROR] :downloadPreviousForcastError,
    [constants.DOWNLOAD_PREVIOUS_FORCAST_SUCCESS] :downloadPreviousForcastSuccess,
    [constants.DOWNLOAD_PREVIOUS_FORCAST_CLEAR]:downloadPreviousForcastClear,


}, initialState);
