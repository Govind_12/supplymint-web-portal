import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';


let initialState = {

  profileImage: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  dashboardTiles: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  storesArticles: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  salesTrendGraph: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  slowFastArticle: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  userSession: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  switchOrganisation: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  switchEnt: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  userActivity: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  searchData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  }
}

const userSessionCreateRequest = (state, action) => update(state, {
  userSession: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const userSessionCreateSuccess = (state, action) => update(state, {
  userSession: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'profile Image success' }
  }
});

const userSessionCreateError = (state, action) => update(state, {
  userSession: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const userSessionCreateClear = (state, action) => update(state, {
  userSession: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const profileImageRequest = (state, action) => update(state, {
  profileImage: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const profileImageSuccess = (state, action) => update(state, {
  profileImage: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'profile Image success' }
  }
});

const profileImageError = (state, action) => update(state, {
  profileImage: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const profileImageClear = (state, action) => update(state, {
  profileImage: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const dashboardTilesRequest = (state, action) => update(state, {
  dashboardTiles: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const dashboardTilesSuccess = (state, action) => update(state, {
  dashboardTiles: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'profile Image success' }
  }
});

const dashboardTilesError = (state, action) => update(state, {
  dashboardTiles: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const dashboardTilesClear = (state, action) => update(state, {
  dashboardTiles: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const storesArticlesRequest = (state, action) => update(state, {
  storesArticles: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const storesArticlesSuccess = (state, action) => update(state, {
  storesArticles: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'profile Image success' }
  }
});

const storesArticlesError = (state, action) => update(state, {
  storesArticles: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const storesArticlesClear = (state, action) => update(state, {
  storesArticles: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const salesTrendGraphRequest = (state, action) => update(state, {
  salesTrendGraph: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const salesTrendGraphSuccess = (state, action) => update(state, {
  salesTrendGraph: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'profile Image success' }
  }
});

const salesTrendGraphError = (state, action) => update(state, {
  salesTrendGraph: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const salesTrendGraphClear = (state, action) => update(state, {
  salesTrendGraph: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const slowFastArticleRequest = (state, action) => update(state, {
  slowFastArticle: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const slowFastArticleSuccess = (state, action) => update(state, {
  slowFastArticle: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'profile Image success' }
  }
});

const slowFastArticleError = (state, action) => update(state, {
  slowFastArticle: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const slowFastArticleClear = (state, action) => update(state, {
  slowFastArticle: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const switchOrganisationRequest = (state, action) => update(state, {
  switchOrganisation: {
    isLoading: { $set: true },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const switchOrganisationSuccess = (state, action) => update(state, {
  switchOrganisation: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'switchOrganisation success' }
  }
});

const switchOrganisationError = (state, action) => update(state, {
  switchOrganisation: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const switchOrganisationClear = (state, action) => update(state, {
  switchOrganisation: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const switchEntRequest = (state, action) => update(state, {
  switchEnt: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const switchEntSuccess = (state, action) => update(state, {
  switchEnt: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'switchEnt success' }
  }
});

const switchEntError = (state, action) => update(state, {
  switchEnt: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const switchEntClear = (state, action) => update(state, {
  switchEnt: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const userActivityRequest = (state, action) => update(state, {
  userActivity: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const userActivitySuccess = (state, action) => update(state, {
  userActivity: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'userActivity success' }
  }
});

const userActivityError = (state, action) => update(state, {
  userActivity: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const userActivityClear = (state, action) => update(state, {
  userActivity: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const searchDataRequest = (state, action) => update(state, {
  searchData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const searchDataSuccess = (state, action) => update(state, {
  searchData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'searchData success' }
  }
});

const searchDataError = (state, action) => update(state, {
  searchData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const searchDataClear = (state, action) => update(state, {
  searchData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

export default handleActions({
  [constants.PROFILE_IMAGE_REQUEST]: profileImageRequest,
  [constants.PROFILE_IMAGE_SUCCESS]: profileImageSuccess,
  [constants.PROFILE_IMAGE_ERROR]: profileImageError,
  [constants.PROFILE_IMAGE_CLEAR]: profileImageClear,

  [constants.DASHBOARD_TILES_REQUEST]: dashboardTilesRequest,
  [constants.DASHBOARD_TILES_SUCCESS]: dashboardTilesSuccess,
  [constants.DASHBOARD_TILES_ERROR]: dashboardTilesError,
  [constants.DASHBOARD_TILES_CLEAR]: dashboardTilesClear,

  [constants.STORES_ARTICLES_REQUEST]: storesArticlesRequest,
  [constants.STORES_ARTICLES_SUCCESS]: storesArticlesSuccess,
  [constants.STORES_ARTICLES_ERROR]: storesArticlesError,
  [constants.STORES_ARTICLES_CLEAR]: storesArticlesClear,

  [constants.SALES_TREND_GRAPH_REQUEST]: salesTrendGraphRequest,
  [constants.SALES_TREND_GRAPH_SUCCESS]: salesTrendGraphSuccess,
  [constants.SALES_TREND_GRAPH_ERROR]: salesTrendGraphError,
  [constants.SALES_TREND_GRAPH_CLEAR]: salesTrendGraphClear,

  [constants.SLOW_FAST_ARTICLE_REQUEST]: slowFastArticleRequest,
  [constants.SLOW_FAST_ARTICLE_SUCCESS]: slowFastArticleSuccess,
  [constants.SLOW_FAST_ARTICLE_ERROR]: slowFastArticleError,
  [constants.SLOW_FAST_ARTICLE_CLEAR]: slowFastArticleClear,

  [constants.USER_SESSION_CREATE_REQUEST]: userSessionCreateRequest,
  [constants.USER_SESSION_CREATE_SUCCESS]: userSessionCreateSuccess,
  [constants.USER_SESSION_CREATE_ERROR]: userSessionCreateError,
  [constants.USER_SESSION_CREATE_CLEAR]: userSessionCreateClear,

  [constants.SWITCH_ORGANISATION_REQUEST]: switchOrganisationRequest,
  [constants.SWITCH_ORGANISATION_SUCCESS]: switchOrganisationSuccess,
  [constants.SWITCH_ORGANISATION_ERROR]: switchOrganisationError,
  [constants.SWITCH_ORGANISATION_CLEAR]: switchOrganisationClear,

  [constants.SWITCH_ENT_REQUEST]: switchEntRequest,
  [constants.SWITCH_ENT_SUCCESSS]: switchEntSuccess,
  [constants.SWITCH_ENT_CLEAR]: switchEntClear,
  [constants.SWITCH_ENT_ERROR]: switchEntError,

  [constants.USER_ACTIVITY_REQUEST]: userActivityRequest,
  [constants.USER_ACTIVITY_SUCCESS]: userActivitySuccess,
  [constants.USER_ACTIVITY_CLEAR]: userActivityClear,
  [constants.USER_ACTIVITY_ERROR]: userActivityError,

  [constants.SEARCH_DATA_REQUEST]: searchDataRequest,
  [constants.SEARCH_DATA_SUCCESS]: searchDataSuccess,
  [constants.SEARCH_DATA_CLEAR]: searchDataClear,
  [constants.SEARCH_DATA_ERROR]: searchDataError

}, initialState);