import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';

import { CONFIG } from '../../../config/index';
import { AUTH_CONFIG } from '../../../authConfig/index';
import script from '../../script';
import { OganisationIdName } from '../../../organisationIdName';
import { parseJwt } from "../../../helper/index";


import loginAjax from '../../../services/authServices';
export function* profileImageRequest(action) {
  let userName = action.payload.userName
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/user/get/url?userName=${userName}`, {

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.profileImageSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.profileImageError(response.data));
    }
  } catch (e) {
    yield put(actions.profileImageError("error occurs"));
    console.warn('Some error found in profileImage action\n', e);
  }
}

export function* dashboardTilesRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.dashboardTilesClear());
  } else {
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/dashboard/get/windows`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.dashboardTilesSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.dashboardTilesError(response.data));
      }
    } catch (e) {
      yield put(actions.dashboardTilesError("error occurs"));
      console.warn('Some error found in dashboardTiles action\n', e);
    }
  }
}


export function* storesArticlesRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.storesArticlesClear());
  } else {
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/dashboard/get/toparticlesstores`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.storesArticlesSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.storesArticlesError(response.data));
      }
    } catch (e) {
      yield put(actions.storesArticlesError("error occurs"));
      console.warn('Some error found in storesArticles action\n', e);
    }
  }
}


export function* salesTrendGraphRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.salesTrendGraphClear());
  } else {
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/dashboard/get/salestrend?type=${action.payload}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.salesTrendGraphSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.salesTrendGraphError(response.data));
      }
    } catch (e) {
      yield put(actions.salesTrendGraphError("error occurs"));
      console.warn('Some error found in salesTrendGraph action\n', e);
    }
  }
}


export function* slowFastArticleRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.slowFastArticleClear());
  } else {
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/dashboard/get/articlemoving`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.slowFastArticleSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.slowFastArticleError(response.data));
      }
    } catch (e) {
      yield put(actions.slowFastArticleError("error occurs"));
      console.warn('Some error found in slowFastArticle action\n', e);
    }
  }
}


export function* userSessionCreateRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.userSessionCreateClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/user/session/create`, {
        "userName": action.payload.userName,
        "token": action.payload.token,
        "emailId": action.payload.emailId,
        "type": action.payload.type,
        "mode": "WEB",
        "keepMeSignIn": action.payload.keepMeSignIn,
        "autoSignIn": "NA"
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.userSessionCreateSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.userSessionCreateError(response.data));
      }
    } catch (e) {
      yield put(actions.userSessionCreateError("error occurs"));
      console.warn('Some error found in userSessionCreate action\n', e);
    }
  }
}
// ___________________________ORGANISATION LIST API ______________________________

export function* switchOrganisationRequest(action) {
  console.log(action)
  var { orgIdGlobal } = OganisationIdName()
  try {
    const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}${AUTH_CONFIG.SWITCH_ORGANIZATION}`, {
      // 'username': action.payload.username,
      'orgId': action.payload.orgId,
      'entId': Number(action.payload.entId),
      // 'uType':sessionStorage.getItem('uType'),
      'token': action.payload.token,
      'fcmToken': localStorage.getItem('firebase-token')
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      sessionStorage.setItem('token', response.data.data.resource.token);
      sessionStorage.setItem('eid', response.data.data.resource.user.eid);
      sessionStorage.setItem('oid', response.data.data.resource.user.coreOrgID);
      console.log("eid----" + response.data.data.resource.user.eid);
      console.log("oid----" + response.data.data.resource.user.coreOrgID);
      sessionStorage.setItem('subscription', response.data.data.resource.subscription);
      if (response.data.data.resource.token != null)
        sessionStorage.setItem('prn', parseJwt(response.data.data.resource.token).prn)
      sessionStorage.setItem('partnerEnterpriseId', response.data.data.resource.user.eid);
      sessionStorage.setItem('partnerEnterpriseName', response.data.data.resource.user.ename);
      sessionStorage.setItem('firstName', response.data.data.resource.user.firstName);
      sessionStorage.setItem('lastName', response.data.data.resource.user.lastName);
      sessionStorage.setItem('roles', JSON.stringify(response.data.data.resource.roles));
      sessionStorage.setItem('userName', response.data.data.resource.user.username);
      sessionStorage.setItem('email', response.data.data.resource.user.email);
      sessionStorage.setItem('bucket', response.data.data.resource.user.bucket);

      sessionStorage.setItem('partnerEnterpriseCode', response.data.data.resource.user.ecode);
      sessionStorage.setItem('gstin', response.data.data.resource.user.gstin)
      // let org =[{orgId:"201",orgName:"tcloud",active:"TRUE"},{orgId:"221",orgName:"xyz",active:"FALSE"}]
      sessionStorage.setItem('orgId-name', JSON.stringify(response.data.data.resource.user.organisations))
      sessionStorage.setItem('fileUpload', false);
      sessionStorage.setItem('uType', response.data.data.resource.user.uType)

      sessionStorage.setItem('weeklyAssortmentCode', "");
      sessionStorage.setItem('monthlyAssortmentCode', "");
      sessionStorage.setItem('monthlyStart', "");
      sessionStorage.setItem('monthlyEnd', "");
      sessionStorage.setItem('weeklyStart', "");
      sessionStorage.setItem('weeklyEnd', "");
      sessionStorage.setItem('weeklyGraph', JSON.stringify([]));
      sessionStorage.setItem('monthlyGraph', JSON.stringify([]));
      sessionStorage.setItem('uType', response.data.data.resource.user.uType)

      sessionStorage.setItem('enterprises', JSON.stringify(response.data.data.resource.user.enterprises))
      sessionStorage.setItem('modules', JSON.stringify(response.data.data.resource.modules));
      if (response.data.data.resource.roles.length != 0) {
        sessionStorage.setItem('mid', JSON.stringify(response.data.data.resource.roles[0].id));
        let module = JSON.parse(sessionStorage.getItem('modules'));
        let mid = JSON.parse(sessionStorage.getItem('mid'));
        let data = module[mid]


        for (let i = 0; i < data.length; i++) {
          sessionStorage.setItem(`${data[i].name}`, JSON.stringify(data[i]));
        }
      }

      /*if (response.data.data.resource.token != null) {
        if (sessionStorage.getItem('firebase-token') != null) {
          registerFcmToken(sessionStorage.getItem('firebase-token'));
        }
      }*/
      yield put(actions.switchOrganisationSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.switchOrganisationError(response.data));
    }
  } catch (e) {
    yield put(actions.switchOrganisationError("error occurs"));
    console.warn('Some error found in switchOrganisation action\n', e);
  }
}

export function* switchEntRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.switchEntClear());
  }
  else {
    try {
      const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}/switch/ent/info`, {
        // "username": action.payload.username,
        'orgId': action.payload.orgId,
        'entId': Number(action.payload.entId),
        // 'uType':sessionStorage.getItem('uType'),
        'token': action.payload.token,
        'fcmToken': localStorage.getItem('firebase-token')

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        sessionStorage.setItem('token', response.data.data.resource.token);
      sessionStorage.setItem('eid', response.data.data.resource.user.eid);
      sessionStorage.setItem('oid', response.data.data.resource.user.coreOrgID);
      console.log("eid----" + response.data.data.resource.user.eid);
      console.log("oid----" + response.data.data.resource.user.coreOrgID);
      sessionStorage.setItem('subscription', response.data.data.resource.subscription);
      if (response.data.data.resource.token != null)
        sessionStorage.setItem('prn', parseJwt(response.data.data.resource.token).prn)
      sessionStorage.setItem('partnerEnterpriseId', response.data.data.resource.user.eid);
      sessionStorage.setItem('partnerEnterpriseName', response.data.data.resource.user.ename);
      sessionStorage.setItem('firstName', response.data.data.resource.user.firstName);
      sessionStorage.setItem('lastName', response.data.data.resource.user.lastName);
      sessionStorage.setItem('roles', JSON.stringify(response.data.data.resource.roles));
      sessionStorage.setItem('userName', response.data.data.resource.user.username);
      sessionStorage.setItem('email', response.data.data.resource.user.email);
      sessionStorage.setItem('bucket', response.data.data.resource.user.bucket);

      sessionStorage.setItem('partnerEnterpriseCode', response.data.data.resource.user.ecode);
      sessionStorage.setItem('gstin', response.data.data.resource.user.gstin)
      // let org =[{orgId:"201",orgName:"tcloud",active:"TRUE"},{orgId:"221",orgName:"xyz",active:"FALSE"}]
      sessionStorage.setItem('orgId-name', JSON.stringify(response.data.data.resource.user.organisations))
      sessionStorage.setItem('fileUpload', false);
      sessionStorage.setItem('uType', response.data.data.resource.user.uType)

      sessionStorage.setItem('weeklyAssortmentCode', "");
      sessionStorage.setItem('monthlyAssortmentCode', "");
      sessionStorage.setItem('monthlyStart', "");
      sessionStorage.setItem('monthlyEnd', "");
      sessionStorage.setItem('weeklyStart', "");
      sessionStorage.setItem('weeklyEnd', "");
      sessionStorage.setItem('weeklyGraph', JSON.stringify([]));
      sessionStorage.setItem('monthlyGraph', JSON.stringify([]));
      sessionStorage.setItem('uType', response.data.data.resource.user.uType)

      sessionStorage.setItem('enterprises', JSON.stringify(response.data.data.resource.user.enterprises))
      sessionStorage.setItem('modules', JSON.stringify(response.data.data.resource.modules));
      if (response.data.data.resource.roles.length != 0) {
        sessionStorage.setItem('mid', JSON.stringify(response.data.data.resource.roles[0].id));
        let module = JSON.parse(sessionStorage.getItem('modules'));
        let mid = JSON.parse(sessionStorage.getItem('mid'));
        let data = module[mid]


        for (let i = 0; i < data.length; i++) {
          sessionStorage.setItem(`${data[i].name}`, JSON.stringify(data[i]));
        }
      }

        yield put(actions.switchEntSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.switchEntError(response.data));
      }
    } catch (e) {
      yield put(actions.switchEntError("error occurs"));
      console.warn('Some error found in switchEnt action\n', e);
    }
  }
}

export function* userActivityRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/org/get/activity`, action.payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.userActivitySuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.userActivityError(response.data));
    }
  } catch (e) {
    yield put(actions.userActivityError("error occurs"));
    console.warn('Some error found in userActivity action\n', e);
  }
}

export function* searchDataRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/global/search`, action.payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.searchDataSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.searchDataError(response.data));
    }
  } catch (e) {
    yield put(actions.searchDataError("error occurs"));
    console.warn('Some error found in searchData action\n', e);
  }
}