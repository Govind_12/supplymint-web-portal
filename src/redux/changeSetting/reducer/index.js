import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';



let initialState = {

    createSetting: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    getSetting: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getModule: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getSubModule: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getProperty: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getEmailStatus: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    updateEmailStatus: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getToCc: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    saveToCc: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    updateToCc: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
   
    settingAssortOtb: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    settingOtbCreate: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    otbGetDefault: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    otbGetHlevel: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAllConfig: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAllOtb: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    purchaseOrderDateRange:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAsnFormat : {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    createAsnFormat : { 
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    invoiceApprovalBasedLR : { 
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAvailableKeys:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getGeneralMapping: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    updateGeneralMapping: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    updateAvailableKeys:{
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
}
// ___________________________PURCHASE ORDER DATEW RANGE________________

const purchaseOrderDateRangeRequest = (state, action) => update(state, {
    purchaseOrderDateRange: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
const purchaseOrderDateRangeSuccess = (state, action) => update(state, {
    purchaseOrderDateRange: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'purchaseOrderDateRange success' }
    }
})
const purchaseOrderDateRangeError = (state, action) => update(state, {
    purchaseOrderDateRange: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const purchaseOrderDateRangeClear = (state, action) => update(state, {
    purchaseOrderDateRange: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
})

// ___________________________________ General Setting cat/Desc________________________

const getGeneralMappingRequest = (state, action) => update(state, {
    getGeneralMapping: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: '' }
    }
  });
  const getGeneralMappingSuccess = (state, action) => update(state, {
    getGeneralMapping: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: 'get General Setting Success' }
    }
  });
  const getGeneralMappingError = (state, action) => update(state, {
    getGeneralMapping: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
    }
  });
  const getGeneralMappingClear = (state, action) => update(state, {
    getGeneralMapping: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: '' }
    }
  });

  const updateGeneralMappingRequest = (state, action) => update(state, {
    updateGeneralMapping: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: 'update General Setting Request' }
    }
  });
  const updateGeneralMappingSuccess = (state, action) => update(state, {
    updateGeneralMapping: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: 'update General Setting Success' }
    }
  });
  const updateGeneralMappingError = (state, action) => update(state, {
    updateGeneralMapping: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
    }
  });
  const updateGeneralMappingClear = (state, action) => update(state, {
    updateGeneralMapping: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: '' }
    }
  });

// ____________________________________

const getAllConfigRequest = (state, action) => update(state, {
    getAllConfig: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
const getAllConfigSuccess = (state, action) => update(state, {
    getAllConfig: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getAllConfig success' }
    }
})
const getAllConfigError = (state, action) => update(state, {
    getAllConfig: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllConfigClear = (state, action) => update(state, {
    getAllConfig: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
})
const getAllOtbRequest = (state, action) => update(state, {
    getAllOtb: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
const getAllOtbSuccess = (state, action) => update(state, {
    getAllOtb: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getAllOtb success' }
    }
})
const getAllOtbError = (state, action) => update(state, {
    getAllOtb: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllOtbClear = (state, action) => update(state, {
    getAllOtb: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
})

const createSettingRequest = (state, action) => update(state, {
    createSetting: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const createSettingSuccess = (state, action) => update(state, {
    createSetting: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'createSetting success' }
    }
});

const createSettingError = (state, action) => update(state, {
    createSetting: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const createSettingClear = (state, action) => update(state, {
    createSetting: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const getSettingRequest = (state, action) => update(state, {
    getSetting: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getSettingSuccess = (state, action) => update(state, {
    getSetting: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getSetting success' }
    }
});

const getSettingError = (state, action) => update(state, {
    getSetting: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getSettingClear = (state, action) => update(state, {
    getSetting: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getModuleRequest = (state, action) => update(state, {
    getModule: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getModuleSuccess = (state, action) => update(state, {
    getModule: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getModule success' }
    }
});

const getModuleError = (state, action) => update(state, {
    getModule: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getModuleClear = (state, action) => update(state, {
    getModule: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getSubModuleRequest = (state, action) => update(state, {
    getSubModule: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getSubModuleSuccess = (state, action) => update(state, {
    getSubModule: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getSubModule success' }
    }
});

const getSubModuleError = (state, action) => update(state, {
    getSubModule: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getSubModuleClear = (state, action) => update(state, {
    getSubModule: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const getPropertyRequest = (state, action) => update(state, {
    getProperty: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getPropertySuccess = (state, action) => update(state, {
    getProperty: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getProperty success' }
    }
});

const getPropertyError = (state, action) => update(state, {
    getProperty: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getPropertyClear = (state, action) => update(state, {
    getProperty: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getEmailStatusRequest = (state, action) => update(state, {
    getEmailStatus: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getEmailStatusSuccess = (state, action) => update(state, {
    getEmailStatus: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getEmailStatus success' }
    }
});

const getEmailStatusError = (state, action) => update(state, {
    getEmailStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getEmailStatusClear = (state, action) => update(state, {
    getEmailStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const updateEmailStatusRequest = (state, action) => update(state, {
    updateEmailStatus: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const updateEmailStatusSuccess = (state, action) => update(state, {
    updateEmailStatus: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'updateEmailStatus success' }
    }
});

const updateEmailStatusError = (state, action) => update(state, {
    updateEmailStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const updateEmailStatusClear = (state, action) => update(state, {
    updateEmailStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const getToCcRequest = (state, action) => update(state, {
    getToCc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getToCcSuccess = (state, action) => update(state, {
    getToCc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getToCc success' }
    }
});

const getToCcError = (state, action) => update(state, {
    getToCc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getToCcClear = (state, action) => update(state, {
    getToCc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const saveToCcRequest = (state, action) => update(state, {
    saveToCc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const saveToCcSuccess = (state, action) => update(state, {
    saveToCc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'saveToCc success' }
    }
});

const saveToCcError = (state, action) => update(state, {
    saveToCc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const saveToCcClear = (state, action) => update(state, {
    saveToCc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const updateToCcRequest = (state, action) => update(state, {
    updateToCc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const updateToCcSuccess = (state, action) => update(state, {
    updateToCc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'success' }
    }
});

const updateToCcError = (state, action) => update(state, {
    updateToCc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const updateToCcClear = (state, action) => update(state, {
    updateToCc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const settingAssortOtbRequest = (state, action) => update(state, {
    settingAssortOtb: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const settingAssortOtbSuccess = (state, action) => update(state, {
    settingAssortOtb: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'settingAssortOtb success' }
    }
});

const settingAssortOtbError = (state, action) => update(state, {
    settingAssortOtb: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const settingAssortOtbClear = (state, action) => update(state, {
    settingAssortOtb: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const settingOtbCreateRequest = (state, action) => update(state, {
    settingOtbCreate: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const settingOtbCreateSuccess = (state, action) => update(state, {
    settingOtbCreate: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'settingOtbCreate success' }
    }
});

const settingOtbCreateError = (state, action) => update(state, {
    settingOtbCreate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const settingOtbCreateClear = (state, action) => update(state, {
    settingOtbCreate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});
const otbGetHlevelRequest = (state, action) => update(state, {
    otbGetHlevel: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const otbGetHlevelSuccess = (state, action) => update(state, {
    otbGetHlevel: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'otbGetHlevel success' }
    }
});

const otbGetHlevelError = (state, action) => update(state, {
    otbGetHlevel: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const otbGetHlevelClear = (state, action) => update(state, {
    otbGetHlevel: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const otbGetDefaultRequest = (state, action) => update(state, {
    otbGetDefault: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const otbGetDefaultSuccess = (state, action) => update(state, {
    otbGetDefault: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'otbGetDefault success' }
    }
});

const otbGetDefaultError = (state, action) => update(state, {
    otbGetDefault: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const otbGetDefaultClear = (state, action) => update(state, {
    otbGetDefault: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

// -----------------------------------ADN NUMBER FORMAT ------------------------------------


const getAsnFormatRequest = (state, action) => update(state, {
    getAsnFormat: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAsnFormatSuccess = (state, action) => update(state, {
    getAsnFormat: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getAsnFormat success' }
    }
});

const getAsnFormatError = (state, action) => update(state, {
    getAsnFormat: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getAsnFormatClear = (state, action) => update(state, {
    getAsnFormat: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const createAsnFormatRequest = (state, action) => update(state, {
    createAsnFormat: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const createAsnFormatSuccess = (state, action) => update(state, {
    createAsnFormat: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'createAsnFormat success' }
    }
});

const createAsnFormatError = (state, action) => update(state, {
    createAsnFormat: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const createAsnFormatClear = (state, action) => update(state, {
    createAsnFormat: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const invoiceApprovalBasedLRRequest = (state, action) => update(state, {
    invoiceApprovalBasedLR: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const invoiceApprovalBasedLRSuccess = (state, action) => update(state, {
    invoiceApprovalBasedLR: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'createAsnFormat success' }
    }
});

const invoiceApprovalBasedLRError = (state, action) => update(state, {
    invoiceApprovalBasedLR: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const invoiceApprovalBasedLRClear = (state, action) => update(state, {
    invoiceApprovalBasedLR: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getAvailableKeysRequest = (state, action) => update(state, {
    getAvailableKeys: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAvailableKeysSuccess = (state, action) => update(state, {
    getAvailableKeys: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getAvailableKeys success' }
    }
});
const getAvailableKeysError = (state, action) => update(state, {
    getAvailableKeys: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getAvailableKeysClear = (state, action) => update(state, {
    getAvailableKeys: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const updateAvailableKeysRequest = (state, action) => update(state, {
    updateAvailableKeys: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateAvailableKeysSuccess = (state, action) => update(state, {
    updateAvailableKeys: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'updateAvailableKeys success' }
    }
});
const updateAvailableKeysError = (state, action) => update(state, {
    updateAvailableKeys: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const updateAvailableKeysClear = (state, action) => update(state, {
    updateAvailableKeys: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


export default handleActions({
      // General Setting cat/desc request
    [constants.GET_GENERAL_MAPPING_REQUEST]: getGeneralMappingRequest,
    [constants.GET_GENERAL_MAPPING_SUCCESS]: getGeneralMappingSuccess,
    [constants.GET_GENERAL_MAPPING_ERROR]: getGeneralMappingError,
    [constants.GET_GENERAL_MAPPING_CLEAR]: getGeneralMappingClear,

    [constants.UPDATE_GENERAL_MAPPING_REQUEST]: updateGeneralMappingRequest,
    [constants.UPDATE_GENERAL_MAPPING_SUCCESS]: updateGeneralMappingSuccess,
    [constants.UPDATE_GENERAL_MAPPING_ERROR]: updateGeneralMappingError,
    [constants.UPDATE_GENERAL_MAPPING_CLEAR]: updateGeneralMappingClear,

    //---------------------------------------------------------------------//

    [constants.CREATE_SETTING_REQUEST]: createSettingRequest,
    [constants.CREATE_SETTING_SUCCESS]: createSettingSuccess,
    [constants.CREATE_SETTING_ERROR]: createSettingError,
    [constants.CREATE_SETTING_CLEAR]: createSettingClear,

    [constants.GET_SETTING_REQUEST]: getSettingRequest,
    [constants.GET_SETTING_SUCCESS]: getSettingSuccess,
    [constants.GET_SETTING_ERROR]: getSettingError,
    [constants.GET_SETTING_CLEAR]: getSettingClear,

    [constants.GET_MODULE_REQUEST]: getModuleRequest,
    [constants.GET_MODULE_SUCCESS]: getModuleSuccess,
    [constants.GET_MODULE_ERROR]: getModuleError,
    [constants.GET_MODULE_CLEAR]: getModuleClear,

    [constants.GET_SUB_MODULE_REQUEST]: getSubModuleRequest,
    [constants.GET_SUB_MODULE_SUCCESS]: getSubModuleSuccess,
    [constants.GET_SUB_MODULE_ERROR]: getSubModuleError,
    [constants.GET_SUB_MODULE_CLEAR]: getSubModuleClear,

    [constants.GET_PROPERTY_REQUEST]: getPropertyRequest,
    [constants.GET_PROPERTY_SUCCESS]: getPropertySuccess,
    [constants.GET_PROPERTY_ERROR]: getPropertyError,
    [constants.GET_PROPERTY_CLEAR]: getPropertyClear,

    [constants.GET_EMAIL_STATUS_REQUEST]: getEmailStatusRequest,
    [constants.GET_EMAIL_STATUS_SUCCESS]: getEmailStatusSuccess,
    [constants.GET_EMAIL_STATUS_ERROR]: getEmailStatusError,
    [constants.GET_EMAIL_STATUS_CLEAR]: getEmailStatusClear,

    [constants.UPDATE_EMAIL_STATUS_REQUEST]: updateEmailStatusRequest,
    [constants.UPDATE_EMAIL_STATUS_SUCCESS]: updateEmailStatusSuccess,
    [constants.UPDATE_EMAIL_STATUS_ERROR]: updateEmailStatusError,
    [constants.UPDATE_EMAIL_STATUS_CLEAR]: updateEmailStatusClear,

    [constants.GET_TO_CC_REQUEST]: getToCcRequest,
    [constants.GET_TO_CC_SUCCESS]: getToCcSuccess,
    [constants.GET_TO_CC_ERROR]: getToCcError,
    [constants.GET_TO_CC_CLEAR]: getToCcClear,

    [constants.SAVE_TO_CC_REQUEST]: saveToCcRequest,
    [constants.SAVE_TO_CC_SUCCESS]: saveToCcSuccess,
    [constants.SAVE_TO_CC_ERROR]: saveToCcError,
    [constants.SAVE_TO_CC_CLEAR]: saveToCcClear,

    [constants.UPDATE_TO_CC_REQUEST]: updateToCcRequest,
    [constants.UPDATE_TO_CC_SUCCESS]: updateToCcSuccess,
    [constants.UPDATE_TO_CC_ERROR]: updateToCcError,
    [constants.UPDATE_TO_CC_CLEAR]: updateToCcClear,


    [constants.SETTING_ASSORTOTB_REQUEST]: settingAssortOtbRequest,
    [constants.SETTING_ASSORTOTB_SUCCESS]: settingAssortOtbSuccess,
    [constants.SETTING_ASSORTOTB_ERROR]: settingAssortOtbError,
    [constants.SETTING_ASSORTOTB_CLEAR]: settingAssortOtbClear,

    [constants.SETTING_OTB_CREATE_REQUEST]: settingOtbCreateRequest,
    [constants.SETTING_OTB_CREATE_ERROR]: settingOtbCreateError,
    [constants.SETTING_OTB_CREATE_SUCCESS]: settingOtbCreateSuccess,
    [constants.SETTING_OTB_CREATE_CLEAR]: settingOtbCreateClear,

    [constants.OTB_GET_DEFAULT_REQUEST]: otbGetDefaultRequest,
    [constants.OTB_GET_DEFAULT_CLEAR]: otbGetDefaultClear,
    [constants.OTB_GET_DEFAULT_SUCCESS]: otbGetDefaultSuccess,
    [constants.OTB_GET_DEFAULT_ERROR]: otbGetDefaultError,

    [constants.OTB_GET_HLEVEL_REQUEST]: otbGetHlevelRequest,
    [constants.OTB_GET_HLEVEL_SUCCESS]: otbGetHlevelSuccess,
    [constants.OTB_GET_HLEVEL_ERROR]: otbGetHlevelError,
    [constants.OTB_GET_HLEVEL_CLEAR]: otbGetHlevelClear,

    [constants.GET_ALL_CONFIG_REQUEST]: getAllConfigRequest,
    [constants.GET_ALL_CONFIG_SUCCESS]: getAllConfigSuccess,
    [constants.GET_ALL_CONFIG_ERROR]: getAllConfigError,
    [constants.GET_ALL_CONFIG_CLEAR]: getAllConfigClear,

    [constants.GET_ALL_OTB_REQUEST]: getAllOtbRequest,
    [constants.GET_ALL_OTB_SUCCESS]:getAllOtbSuccess,
    [constants.GET_ALL_OTB_ERROR]:getAllOtbError,
    [constants.GET_ALL_OTB_CLEAR]:getAllOtbClear,
    
    [constants.PURCHASE_ORDER_DATE_RANGE_REQUEST]: purchaseOrderDateRangeRequest,
    [constants.PURCHASE_ORDER_DATE_RANGE_SUCCESS]:purchaseOrderDateRangeSuccess,
    [constants.PURCHASE_ORDER_DATE_RANGE_ERROR]:purchaseOrderDateRangeError,
    [constants.PURCHASE_ORDER_DATE_RANGE_CLEAR]:purchaseOrderDateRangeClear,

// --------------------------------ASN NUMBER FORMAT--------------------------------
    [constants.GET_ASN_FORMAT_REQUEST]: getAsnFormatRequest,
    [constants.GET_ASN_FORMAT_SUCCESS]:getAsnFormatSuccess,
    [constants.GET_ASN_FORMAT_ERROR]:getAsnFormatError,
    [constants.GET_ASN_FORMAT_CLEAR]:getAsnFormatClear,

    [constants.CREATE_ASN_FORMAT_REQUEST]: createAsnFormatRequest,
    [constants.CREATE_ASN_FORMAT_SUCCESS]:createAsnFormatSuccess,
    [constants.CREATE_ASN_FORMAT_ERROR]:createAsnFormatError,
    [constants.CREATE_ASN_FORMAT_CLEAR]:createAsnFormatClear,

    [constants.INVOICE_BASED_LR_REQUEST]: invoiceApprovalBasedLRRequest,
    [constants.INVOICE_BASED_LR_SUCCESS]:invoiceApprovalBasedLRSuccess,
    [constants.INVOICE_BASED_LR_ERROR]:invoiceApprovalBasedLRError,
    [constants.INVOICE_BASED_LR_CLEAR]:invoiceApprovalBasedLRClear,

    [constants.GET_AVAILABLE_KEYS_REQUEST]: getAvailableKeysRequest,
    [constants.GET_AVAILABLE_KEYS_SUCCESS]:getAvailableKeysSuccess,
    [constants.GET_AVAILABLE_KEYS_ERROR]:getAvailableKeysError,
    [constants.GET_AVAILABLE_KEYS_CLEAR]:getAvailableKeysClear,

    [constants.UPDATE_AVAILABLE_KEYS_REQUEST]: updateAvailableKeysRequest,
    [constants.UPDATE_AVAILABLE_KEYS_SUCCESS]:updateAvailableKeysSuccess,
    [constants.UPDATE_AVAILABLE_KEYS_ERROR]:updateAvailableKeysError,
    [constants.UPDATE_AVAILABLE_KEYS_CLEAR]:updateAvailableKeysClear,

}, initialState);