import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import { OganisationIdName } from '../../../organisationIdName';

export function* createSettingRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.createSettingClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/general/setting/create`, {
        numberFormat: action.payload.numberFormat,
        decimalPlaces: action.payload.decimalPlaces,
        useSeperator: action.payload.useSeperator,
        currency: action.payload.currency,
        useSymbol: action.payload.useSymbol,
        dateFormat: action.payload.dateFormat,
        timeFormat: action.payload.timeFormat,
        orgId: orgIdGlobal
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.createSettingSuccess(response.data.data));
        yield put(actions.getSettingRequest('data'));
      } else if (finalResponse.failure) {
        yield put(actions.createSettingError(response.data));
      }
    } catch (e) {
      yield put(actions.createSettingError("error occurs"));
      console.warn('Some error found in createSetting action\n', e);

    }
  }
}

export function* getGeneralMappingRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getGeneralMappingClear());
  } else {
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/system/config/get/mapping?isapparel=${action.payload}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getGeneralMappingSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getGeneralMappingError(response.data));
      }
    } catch (e) {
      yield put(actions.getGeneralMappingError('error occurs'));
      console.warn('Some error found in "getGeneralMappingRequest" action\n', e)
    }
  }
}

export function* updateGeneralMappingRequest(action){
  if (action.payload == undefined) {
    yield put(actions.updateGeneralMappingClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/system/config/update/mapping`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateGeneralMappingSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updateActivePlanError(response.data));
      }
    } catch (e) {
      yield put(actions.updateGeneralMappingError('error occurs'));
      console.warn('Some error found in "updateGeneralMappingRequest"')
    }
  }
}


export function* getSettingRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getSettingClear());
  } else {
    let orgId = orgIdGlobal;
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/general/setting/get/all?orgId=${orgId}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getSettingSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getSettingError(response.data));
      }
    } catch (e) {
      yield put(actions.getSettingError("error occurs"));
      console.warn('Some error found in getSetting action\n', e);

    }
  }
}

export function* getModuleRequest(action) {
  try {
    let moduleName = action.payload;
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/notification/email/get/all/module`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getModuleSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getModuleError(response.data));
    }
  } catch (e) {
    yield put(actions.getModuleError("error occurs"));
    console.warn('Some error found in getModule action\n', e);
  }
}

export function* getSubModuleRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getSubModuleClear());
  } else {
    try {
      let moduleName = action.payload;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/notification/email/get/all/subModule?moduleName=${moduleName}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getSubModuleSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getSubModuleError(response.data));
      }
    } catch (e) {
      yield put(actions.getSubModuleError("error occurs"));
      console.warn('Some error found in getSubModule action\n', e);
    }
  }
}

export function* getPropertyRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getPropertyClear());
  } else {
    try {
      let subModuleName = action.payload;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/notification/email/get/configuration/property?subModuleName=${subModuleName}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getPropertySuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getPropertyError(response.data));
      }
    } catch (e) {
      yield put(actions.getPropertyError("error occurs"));
      console.warn('Some error found in getPropertye action\n', e);
    }
  }
}

export function* getEmailStatusRequest(action) {
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/notification/email/get/status`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getEmailStatusSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getEmailStatusError(response.data));
    }
  } catch (e) {
    yield put(actions.getEmailStatusError("error occurs"));
    console.warn('Some error found in getEmailStatuse action\n', e);
  }
}

export function* updateEmailStatusRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updateEmailStatusClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/notification/email/update/status`, {
        status: action.payload
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getEmailStatusRequest());
        yield put(actions.updateEmailStatusSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updateEmailStatusError(response.data));
      }
    } catch (e) {
      yield put(actions.updateEmailStatusError("error occurs"));
      console.warn('Some error found in updateEmailStatuse action\n', e);
    }
  }
}

export function* getToCcRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getToCcClear());
  } else {
    try {
      let moduleName = action.payload.moduleName;
      let subModuleName = action.payload.subModuleName;
      let configurationProperty = action.payload.configurationProperty;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/notification/email/get/configuration?moduleName=${moduleName}&subModuleName=${subModuleName}&configurationProperty=${configurationProperty}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getToCcSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getToCcError(response.data));
      }
    } catch (e) {
      yield put(actions.getToCcError("error occurs"));
      console.warn('Some error found in updateEmailStatuse action\n', e);
    }
  }
}

export function* saveToCcRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.saveToCcClear());
  } else {
    try {
      let to = "";
      for (let i = 0; i < action.payload.to.length; i++) {
        let a = action.payload.to[i];
        if (action.payload.to.length > 1) {
          to = to == "" ? "".concat(a) : to.concat(',').concat(a);
        } else {
          to = a;
        }
      }
      let cc = "";
      for (let i = 0; i < action.payload.cc.length; i++) {
        let a = action.payload.cc[i];
        if (action.payload.cc.length > 1) {
          cc = cc == "" ? "".concat(a) : cc.concat(',').concat(a);
        } else {
          cc = a;
        }
      }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/notification/email/create`, {
        module: action.payload.selectedModule,
        subModule: action.payload.selectedSubModule,
        type: action.payload.selectedProperty,
        fromName: "",
        from: "",
        subject: "",
        to: to,
        cc: cc,
        bcc: "",
        channel: "",
        templateName: "",
        arguments: ""
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.saveToCcSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.saveToCcError(response.data));
      }
    } catch (e) {
      yield put(actions.saveToCcError("error occurs"));
      console.warn('Some error found in saveToCc action\n', e);
    }
  }
}

// update notification emails::: 
export function* updateToCcRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updateToCcClear());
  } else {
    try {
      let to = "";
      for (let i = 0; i < action.payload.to.length; i++) {
        let a = action.payload.to[i];
        if (action.payload.to.length > 1) {
          to = to == "" ? "".concat(a) : to.concat(',').concat(a);
        } else {
          to = a;
        }
      }
      let cc = "";
      for (let i = 0; i < action.payload.cc.length; i++) {
        let a = action.payload.cc[i];
        if (action.payload.cc.length > 1) {
          cc = cc == "" ? "".concat(a) : cc.concat(',').concat(a);
        } else {
          cc = a;
        }
      }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/notification/email/update`, {
        module: action.payload.selectedModule,
        subModule: action.payload.selectedSubModule,
        type: action.payload.selectedProperty,
        fromName: "",
        from: "",
        subject: "",
        to: to,
        cc: cc,
        bcc: "",
        channel: "",
        templateName: "",
        arguments: ""
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateToCcSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updateToCcError(response.data));
      }
    } catch (e) {
      yield put(actions.updateToCcError("error occurs"));
      console.warn('Some error found in saveToCc action\n', e);
    }
  }
}

export function* settingAssortOtbRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.settingAssortOtbClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/setting/otb/create/otbassort`, {
        "otbObject": action.payload
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.settingAssortOtbSuccess(response.data.data));
        yield put(actions.getAllOtbRequest())
      } else if (finalResponse.failure) {
        yield put(actions.settingAssortOtbError(response.data));
      }
    } catch (e) {
      yield put(actions.settingAssortOtbError("error occurs"));
      console.warn('Some error found in settingAssortOtb action\n', e);
    }
  }
}

export function* settingOtbCreateRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.settingOtbCreateClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/setting/otb/create`, {
        "hlevel": action.payload.hLevel,
        "udflevel": action.payload.udflevel,

      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.settingOtbCreateSuccess(response.data.data));
        yield put(actions.getAllConfigRequest())
      } else if (finalResponse.failure) {
        yield put(actions.settingOtbCreateError(response.data));
      }
    } catch (e) {
      yield put(actions.settingOtbCreateError("error occurs"));
      console.warn('Some error found in settingOtbCreate action\n', e);
    }
  }
}


export function* otbGetDefaultRequest(action) {

  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/setting/otb/get/default`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.otbGetDefaultSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.otbGetDefaultError(response.data));
    }
  } catch (e) {
    yield put(actions.otbGetDefaultError("error occurs"));
    console.warn('Some error found in otbGetDefault action\n', e);
  }
}



export function* otbGetHlevelRequest(action) {

  try {
    let HLVALUE = "HLVALUE"
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/setting/otb/get/hlevel?HLEVEL=${HLVALUE}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {

      yield put(actions.otbGetHlevelSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.otbGetHlevelError(response.data));
    }
  } catch (e) {
    yield put(actions.otbGetHlevelError("error occurs"));
    console.warn('Some error found in otbGetHlevel action\n', e);
  }
}


export function* getAllConfigRequest(action) {

  try {

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/setting/otb/get/all/config`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {

      yield put(actions.getAllConfigSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getAllConfigError(response.data));
    }
  } catch (e) {
    yield put(actions.getAllConfigError("error occurs"));
    console.warn('Some error found in getAllConfig action\n', e);
  }
}

export function* getAllOtbRequest(action) {
  // if (action.payload == undefined) {
  //   yield put(actions.getAllOtbClear());
  // } else {
    try {

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/setting/otb/get/all/otb`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.getAllOtbSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAllOtbError(response.data));
      }
    } catch (e) {
      yield put(actions.getAllOtbError("error occurs"));
      console.warn('Some error found in getAllOtb action\n', e);
    }
  }
// }

// ___________________---FESTIVAL SETTING API____________________________


// export function* defaultListRequest(action) {
//   if (action.payload == undefined) {
//     yield put(actions.defaultListClear());
//   } else {
//     try {
//       const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/setting/festival/get/all/defaultlist`, {
//       });
//       const finalResponse = script(response);
//       if (finalResponse.success) {
//         yield put(actions.defaultListSuccess(response.data.data));
//       } else if (finalResponse.failure) {
//         yield put(actions.defaultListError(response.data));
//       }
//     } catch (e) {
//       yield put(actions.defaultListError("error occurs"));
//       console.warn('Some error found in defaultList action\n', e);
//     }
//   }
// }



// export function* checkedListRequest(action) {
//   if (action.payload == undefined) {
//     yield put(actions.checkedListClear());
//   } else {
//     try {
//       const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/setting/festival/get/checkedlist`, {
//       });
//       const finalResponse = script(response);
//       if (finalResponse.success) {
//         yield put(actions.checkedListSuccess(response.data.data));
//       } else if (finalResponse.failure) {
//         yield put(actions.checkedListError(response.data));
//       }
//     } catch (e) {
//       yield put(actions.checkedListError("error occurs"));
//       console.warn('Some error found in checkedList action\n', e);
//     }
//   }
// }

export function* purchaseOrderDateRangeRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.purchaseOrderDateRangeClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/config/prodate`, {
        "startDate": action.payload.startDate,
        "endDate": action.payload.endDate,

      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.purchaseOrderDateRangeSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.purchaseOrderDateRangeError(response.data));
      }
    } catch (e) {
      yield put(actions.purchaseOrderDateRangeError("error occurs"));
      console.warn('Some error found in purchaseOrderDateRangeRequest action\n', e);
    }
  }
}

export function* getAsnFormatRequest(action) {
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/get/asn/format`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.getAsnFormatSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAsnFormatError(response.data));
      }
    } catch (e) {
      yield put(actions.getAsnFormatError("error occurs"));
      console.warn('Some error found in getAsnFormatRequest action\n', e);
    }
}

export function* createAsnFormatRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.createAsnFormatClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/create/asn`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.createAsnFormatSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.createAsnFormatError(response.data));
      }
    } catch (e) {
      yield put(actions.createAsnFormatError("error occurs"));
      console.warn('Some error found in createAsnFormatRequest action\n', e);
    }
  }
}

// api for giving check for invoice based LR creation::
export function* invoiceApprovalBasedLRRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.invoiceApprovalBasedLRClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.SYSTEM_CONFIG}/update/key`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.invoiceApprovalBasedLRSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.invoiceApprovalBasedLRError(response.data));
      }
    } catch (e) {
      yield put(actions.invoiceApprovalBasedLRError("error occurs"));
      console.warn('Some error found in invoiceApprovalBasedLRRequest action\n', e);
    }
  }
}

export function* getAvailableKeysRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getAvailableKeysClear());
  } else{
      try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/system/config/availablekeys`, {
        config: action.payload
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.getAvailableKeysSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAvailableKeysError(response.data));
      }
    } catch (e) {
      yield put(actions.getAvailableKeysError("error occurs"));
      console.warn('Some error found in getAvailableKeysRequest action\n', e);
    }
  }
}

export function* updateAvailableKeysRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updateAvailableKeysClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/system/config/update/availablekeys`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.updateAvailableKeysSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updateAvailableKeysError(response.data));
      }
    } catch (e) {
      yield put(actions.updateAvailableKeysError("error occurs"));
      console.warn('Some error found in updateAvailableKeysRequest action\n', e);
    }
  }
}