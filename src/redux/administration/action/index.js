import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import moment from 'moment';
import { resolve } from 'path';
import { OganisationIdName } from '../../../organisationIdName.js';

export function* organizationRequest(action) {

  try {

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ORGANIZATION}/all/organisation/details`, {
      pageNo: action.payload.no == undefined ? 1 : action.payload.no,
      type: action.payload.type,
      sortedIn:"",
      sortedBy:"",
      search: action.payload.search == undefined ? "" : action.payload.search,
      roleId: sessionStorage.getItem("mid"),
      filter:action.payload.filter == undefined ? {} : action.payload.filter,
      searchBy: action.payload.searchBy != undefined ? action.payload.searchBy : "contains"
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.organizationSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.organizationError(response.data));
    }

  } catch (e) {
    yield put(actions.organizationError("error occurs"));
    console.warn('Some error found in "organizationRequest" action\n', e);
  }
}

export function* addOrganizationRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.addOrganizationClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ORGANIZATION}/create`, {
        'orgName': action.payload.orgName,
        'enterpriseId': sessionStorage.getItem('partnerEnterpriseId'),
        'displayName': action.payload.displayName,
        'description': action.payload.description,
        'contPerson': action.payload.contPerson,
        'contNumber': action.payload.contNumber,
        'contEmail': action.payload.contEmail,
        'status': action.payload.status,
        // 'isDefault': a,
        // 'roleId': a,
        'isDefault': action.payload.isDefault,
        'roleId': sessionStorage.getItem("mid"),
        'billToAddress': action.payload.billToAddress,

      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.addOrganizationSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.addOrganizationError(response.data));
      }
    } catch (e) {
      yield put(actions.addOrganizationError("error occurs"));
      console.warn('Some error found in "addOrganisationRequest" action\n', e);
    }
  }
}

//edit organization

export function* editOrganizationRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.editOrganizationClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ORGANIZATION}/update`, {

        'orgId': action.payload.orgId,
        'name': action.payload.orgNameE,
        'displayName': action.payload.displayNameE,
        // 'description': action.payload.description,
        'contPerson': action.payload.contPerson,
        'contNumber': action.payload.contNumber,
        'contEmail': action.payload.contEmail,
        'status': action.payload.statusE,
        'billToAddress': action.payload.billToAddress,
        'roleId': sessionStorage.getItem("mid") == null ? "" : sessionStorage.getItem("mid"),
        'active': action.payload.statusE == "Active" ? "1" : 0,
        'cin': action.payload.cin,
        'terms': action.payload.terms,
        'logo': action.payload.logo,

        "gstIn": action.payload.billingGSTIN,
        "country": action.payload.billingCountry,
        "state": action.payload.billingState,
        "city": action.payload.billingCity,
        "fullAddress": action.payload.billingAddress,

        "shipCity": action.payload.shippingCity,
        "shipState": action.payload.shippingState,
        "shipCountry": action.payload.shippingCountry,
        "shipAddress": action.payload.shippingAddress,
        "shipGSTIN": action.payload.shippingGSTIN,

      });

      const finalResponse = script(response);

      if (finalResponse.success) {

        yield put(actions.editOrganizationSuccess(response.data.data));
        yield put(actions.organizationRequest(action.payload));

      } else if (finalResponse.failure) {
        yield put(actions.editOrganizationError(response.data));
      }
    } catch (e) {
      yield put(actions.editOrganizationError("error occurs"));
      console.warn('Some error found in "getOrganisationRequest" action\n', e);
    }
  }
}
//delete_organization

export function* deleteOrganizationRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.deleteOrganizationClear());
  }
  else {
    try {
      let id = action.payload.iid;
      let roleId = sessionStorage.getItem("mid")
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ORGANIZATION}/delete/${id}/${roleId}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.organizationRequest(action.payload));
        yield put(actions.deleteOrganizationSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.deleteOrganizationError(response.data));
      }
    } catch (e) {
      yield put(actions.deleteOrganizationError("error occurs"));
      console.warn('Some error found in "getOrganisationRequest" action\n', e);
    }
  }
}

//site

export function* siteRequest(action) {

  var { orgIdGlobal } = OganisationIdName()
  try {


    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.SITE}/all/sites/detail`, {
      pageNo: action.payload.no == undefined ? 1 : action.payload.no,
      type: action.payload.type == undefined ? 1 : action.payload.type,
      search: action.payload.search == undefined ? "" : action.payload.search,
      name: action.payload.siteName == undefined ? "" : action.payload.siteName,
      code: action.payload.code == undefined ? "" : action.payload.code,
      displayName: action.payload.displayName == undefined ? "" : action.payload.displayName,
      state: action.payload.state == undefined ? "" : action.payload.state,
      country: action.payload.country == undefined ? "" : action.payload.country,
      zipCode: action.payload.zipCode == undefined ? "" : action.payload.zipCode,
      status: action.payload.status == undefined ? "" : action.payload.status,
      orgId: orgIdGlobal,
      searchBy: action.payload.searchBy != undefined ? action.payload.searchBy : "contains",
      //sortedBy: action.payload.filterKey,
      //sortedIn: action.payload.filterType,
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.siteSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.siteError(response.data));
    }
  } catch (e) {
    yield put(actions.siteError('Error Occurs !!'));
    console.warn('Some error found in "sapmleRequest" action\n', e);
  }
}


export function* addSiteRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.addSiteClear());
  } else {
    try {
      let req = action.payload;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.SITE}/create`, {
        code: req.code,
        name: req.name,
        country: req.country,
        gstin: req.gstin,
        area: req.area,
        areaUnit: req.areaUnit,
        city: req.city,
        displayName: req.displayName,
        state: req.state,
        zipCode: req.zipCode,
        partner: req.partner,
        zone: req.zone,
        region: req.region,
        latitude: req.latitude,
        longitude: req.longitude,
        numberFloors: req.numberFloors,
        optStoreSDate: req.optStoreSDate,
        optStoreEDate: req.optStoreEDate,
        address: req.address,
        groupName: req.groupName,
        udf1: req.udf1,
        udf2: req.udf2,
        udf3: req.udf3,
        udf4: req.udf4,
        udf5: req.udf5,
        timeZone: req.timeZone,
        type: req.type,
        additional: req.additional,
        isProxyStore: req.isProxyStore,
        proxyStore: req.proxyStore
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.addSiteSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.addSiteError(response.data));
      }
    } catch (e) {
      yield put(actions.addSiteError('Error Occurs !!'));
      console.warn('Some error found in "sapmleRequest" action\n', e);
    }
  }
}

//editSitemapping
export function* editSiteRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.editSiteClear());
  } else {
    try {
      let req = action.payload;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.SITE}/update`, {
        code: req.code,
        name: req.name,
        country: req.country,
        gstin: req.gstin,
        area: req.area,
        areaUnit: req.areaUnit,
        city: req.city,
        displayName: req.displayName,
        state: req.state,
        zipCode: req.zipCode,
        partner: req.partner,
        zone: req.zone,
        region: req.region,
        latitude: req.latitude,
        longitude: req.longitude,
        numberFloors: req.numberFloors,
        optStoreSDate: req.optStoreSDate,
        optStoreEDate: req.optStoreEDate,
        address: req.address,
        groupName: req.groupName,
        udf1: req.udf1,
        udf2: req.udf2,
        udf3: req.udf3,
        udf4: req.udf4,
        udf5: req.udf5,
        timeZone: req.timeZone,
        type: req.type,
        additional: req.additional,
        isProxyStore: req.isProxyStore,
        proxyStore: req.proxyStore
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.editSiteSuccess(response.data.data));
        yield put(actions.siteRequest(action.payload));

      } else if (finalResponse.failure) {
        yield put(actions.editSiteError(response.data));
      }
    } catch (e) {
      yield put(actions.editSiteError('Error Occurs !!'));
      console.warn('Some error found in "user" action\n', e);
    }
  }
}

export function* deleteSiteRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.deleteSiteClear());
  } else {
    try {
      let id = action.payload.idd;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.SITE}/delete/${id}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.deleteSiteSuccess(response.data.data));
        yield put(actions.siteRequest(action.payload));
      } else if (finalResponse.failure) {
        yield put(actions.deleteSiteError(response.data));
      }
    } catch (e) {
      yield put(actions.deleteSiteError('Error Occurs !!'));
      console.warn('Some error found in "deletesitemapingRequest" action\n', e);
    }
  }
}

//user
export function* vendorUserRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type == undefined ? 1 : action.payload.type;
    let search = action.payload.search == undefined ? "" : action.payload.search;
    let partnerEnterpriseName = action.payload.partnerEnterpriseName;
    let firstName = action.payload.firstName;
    let middleName = action.payload.middleName;
    let lastName = action.payload.lastName;
    let userName = action.payload.userName;
    let email = action.payload.email;
    let status = action.payload.status;

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/vendor/get/all`, {
      pageNo: no,
      type,
      search,
      partnerEnterpriseName,
      firstName,
      middleName,
      lastName,
      userName,
      email,
      status
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.vendorUserSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.vendorUserError(response.data));
    }
  } catch (e) {
    yield put(actions.vendorUserError('Error Occurs !!'));
    console.warn('Some error found in "vendorUser" action\n', e);
  }
}

export function* userRequest(action) {
  try {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type == undefined ? 1 : action.payload.type;
    let search = action.payload.search == undefined ? "" : action.payload.search;
    let partnerEnterpriseName = action.payload.partnerEnterpriseName == undefined ? "" : action.payload.partnerEnterpriseName;
    let firstName = action.payload.firstName == undefined ? "" : action.payload.firstName;
    let middleName = action.payload.middleName == undefined ? "" : action.payload.middleName;
    let lastName = action.payload.lastName == undefined ? "" : action.payload.lastName;
    let userName = action.payload.userName == undefined ? "" : action.payload.userName;
    let email = action.payload.email == undefined ? "" : action.payload.email;
    let status = action.payload.status == undefined ? "" : action.payload.status;
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/user/get/all`, {
      'pageNo':no,
      'type':type,
      'search':search,
      'partnerEnterpriseName':partnerEnterpriseName,
      'firstName':firstName,
      'middleName':middleName,
      'lastName':lastName,
      'userName':userName,
      'email':email,
      'status':status
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.userSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.userError(response.data));
    }
  } catch (e) {
    yield put(actions.userError('Error Occurs !!'));
    console.warn('Some error found in "user" action\n', e);
  }
}


export function* addUserRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.addUserClear());
  }
  else {

    try {
      var { orgIdGlobal } = OganisationIdName()
      let xyz = [], idMap = [];
      let abc = "";
      let additionalUrl = action.payload.additionalUrl == undefined ? "" : action.payload.additionalUrl
      if (action.payload.selected != undefined) {

        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].id;
          if (action.payload.selected.length > 1) {
            abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
          } else {
            abc = a;
          }
        }


        for (let i = 0; i < action.payload.selected.length; i++) {
          xyz.push(action.payload.selected[i].name)
        }
        for (let i = 0; i < action.payload.selected.length; i++) {
          idMap.push((action.payload.selected[i].id).toString())
        }
      }

      if(action.payload.uType == "VENDOR" && action.payload.selected !== undefined) {

        abc="";
        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].entorgid;
          if (action.payload.selected.length > 1) {
            abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
          } else {
            abc = a;
          }
        }

          const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_VENDOR}/create/vendor/user${additionalUrl}`, {
            'firstName': action.payload.first,
            'middleName': action.payload.middle,
            'lastName': action.payload.last,
            'email': action.payload.email,
            'mobileNumber': action.payload.mobile,
            'workPhone': action.payload.phone,
            'accessMode': action.payload.access,
            'status': action.payload.active,
            'address': action.payload.address,
            'partnerEnterpriseId': sessionStorage.getItem('partnerEnterpriseId'),
            'partnerEnterpriseName': sessionStorage.getItem('partnerEnterpriseName'),
            'userName': action.payload.user,
            'userRoles': action.payload.uType == "VENDOR" ? idMap : xyz,
            'retailers': abc,
            "uType": action.payload.uType,
            "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
            "slName": action.payload.slName != undefined ? action.payload.slName : "",
            "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
            'userAsAdmin': action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

            // user: user,
          });
          const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.addUserSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.addUserError(response.data));
      }
      } else {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR_USER}/create${additionalUrl}`, {
          'firstName': action.payload.first,
          'middleName': action.payload.middle,
          'lastName': action.payload.last,
          'email': action.payload.email,
          'mobileNumber': action.payload.mobile,
          'workPhone': action.payload.phone,
          'accessMode': action.payload.access,
          'status': action.payload.active,
          'address': action.payload.address,
          'partnerEnterpriseId': sessionStorage.getItem('partnerEnterpriseId'),
          'partnerEnterpriseName': sessionStorage.getItem('partnerEnterpriseName'),
          'userName': action.payload.user,
          'userRoles': action.payload.uType == "VENDOR" ? idMap : xyz,
          'roles': abc,
          "uType": action.payload.uType,
          "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
          "slName": action.payload.slName != undefined ? action.payload.slName : "",
          "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
          'userAsAdmin': action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

          // user: user,
        });
        const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.addUserSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.addUserError(response.data));
      }
      }
    
      
    } catch (e) {
      yield put(actions.addUserError('Error Occurs !!'));
      console.warn('Some error found in "user" action\n', e);
    }
  }
}
export function* editUserRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  let roles = "";
  if (action.payload == undefined) {
    yield put(actions.editUserClear());
  }
  else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      
      if(action.payload.uType == 'ENT') {

        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].id;
          if (action.payload.selected.length > 1) {
            roles = roles == "" ? "".concat(a) : roles.concat('|').concat(a);
          } else {
            roles = a;
          }
        }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR_USER}/update`, {
        'userId': action.payload.userId,
        'firstName': action.payload.firstNameE != undefined ? action.payload.firstNameE : "",
        'middleName': action.payload.middleNameE != undefined ? action.payload.middleNameE : "",
        'lastName': action.payload.lastNameE != undefined ? action.payload.lastNameE : "",
        'email': action.payload.emailE != undefined ? action.payload.emailE : "",
        'mobileNumber': action.payload.mobileNumber != undefined ? action.payload.mobileNumber : "",
        'workPhone': action.payload.workPhone,
        'accessMode': action.payload.accessMode,
        'roles': roles == "" ? null : roles,
        'status': action.payload.active,
        'slCode': action.payload.slCode != undefined ? action.payload.slCode : "",
        'slName': action.payload.slName != undefined ? action.payload.slName : "",
        'address': action.payload.address != undefined ? action.payload.address : "",
        'partnerEnterpriseId': action.payload.partnerEnterpriseId != undefined ? action.payload.partnerEnterpriseId : "",
        'partnerEnterpriseName': action.payload.partnerEnterpriseNameE != undefined ? action.payload.partnerEnterpriseNameE : "",
        'userName': action.payload.userNameE,
        'userRoles': action.payload.selected != undefined ? action.payload.selected : [],
        "uType": action.payload.uType != undefined ? action.payload.uType : "ENT",
        "newUserName": action.payload.newUserName,
        "newPassword": action.payload.newPassword,
        "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
        "userAsAdmin": action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

        // user: user,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.editUserSuccess(response.data.data));
        // yield put(actions.userRequest(action.payload));
        //yield put(actions.getUserNameRequest(action.payload.userNameE));
      } else if (finalResponse.failure) {
        yield put(actions.editUserError(response.data));
      }
    } else {

      roles="";
      if(action.payload.selected!= undefined) {
        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].entorgid;
          if (action.payload.selected.length > 1) {
            roles = roles == "" ? "".concat(a) : roles.concat('|').concat(a);
          } else {
            roles = a;
          }
        }
      }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_VENDOR}/update/user`, {
        'userId': action.payload.userId,
        'firstName': action.payload.firstNameE != undefined ? action.payload.firstNameE : "",
        'middleName': action.payload.middleNameE != undefined ? action.payload.middleNameE : "",
        'lastName': action.payload.lastNameE != undefined ? action.payload.lastNameE : "",
        'email': action.payload.emailE != undefined ? action.payload.emailE : "",
        'mobileNumber': action.payload.mobileNumber != undefined ? action.payload.mobileNumber : "",
        'workPhone': action.payload.workPhone,
        'accessMode': action.payload.accessMode,
        'retailers': roles == "" ? null : roles,
        'status': action.payload.active,
        'slCode': action.payload.slCode != undefined ? action.payload.slCode : "",
        'slName': action.payload.slName != undefined ? action.payload.slName : "",
        'address': action.payload.address != undefined ? action.payload.address : "",
        'partnerEnterpriseId': action.payload.partnerEnterpriseId != undefined ? action.payload.partnerEnterpriseId : "",
        'partnerEnterpriseName': action.payload.partnerEnterpriseNameE != undefined ? action.payload.partnerEnterpriseNameE : "",
        'userName': action.payload.userNameE,
        'userRoles': action.payload.selected != undefined ? action.payload.selected : [],
        "uType": action.payload.uType != undefined ? action.payload.uType : "ENT",
        "newUserName": action.payload.newUserName,
        "newPassword": action.payload.newPassword,
        "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
        "userAsAdmin": action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

        // user: user,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.editUserSuccess(response.data.data));
        // yield put(actions.userRequest(action.payload));
        //yield put(actions.getUserNameRequest(action.payload.userNameE));
      } else if (finalResponse.failure) {
        yield put(actions.editUserError(response.data));
      }
    }
    } catch (e) {
      yield put(actions.editUserError('Error Occurs !!'));
      console.warn('Some error found in "user edit" action\n', e);
    }
  }

}

export function* userStatusRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.userStatusClear());
  }
  else {
    try {
      if(action.payload.uType!=undefined && action.payload.uType=='ENT') {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR_USER}/update/status`, {
          userName: action.payload.userName,
          status: action.payload.active,
          userId: action.payload.userId
          // user: user,
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
  
          yield put(actions.userStatusSuccess(response.data.data));
          yield put(actions.userRequest(action.payload));
        } else if (finalResponse.failure) {
          yield put(actions.userStatusError(response.data));
        }
      } else {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_VENDOR}/update/status`, {
        userName: action.payload.userName,
        status: action.payload.active,
        userId: action.payload.userId
        // user: user,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.userStatusSuccess(response.data.data));
        yield put(actions.userRequest(action.payload));
      } else if (finalResponse.failure) {
        yield put(actions.userStatusError(response.data));
      }
    }
    } catch (e) {
      yield put(actions.userStatusError('Error Occurs !!'));
      console.warn('Some error found in "user status" action\n', e);
    }
  }

}



export function* deleteUserRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.deleteUserClear());
  }
  else {
    try {
      let id = action.payload.idd;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.USER}/delete/${id}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.deleteUserSuccess(response.data.data));
        yield put(actions.userRequest(action.payload));
      } else if (finalResponse.failure) {
        yield put(actions.deleteUserError(response.data));
      }
    } catch (e) {
      yield put(actions.deleteUserError('Error Occurs !!'));
      console.warn('Some error found in "deletesitemapingRequest" action\n', e);
    }
  }

}


export function* rolesRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type;
    let search = action.payload.search == undefined ? "" : action.payload.search;
    let name = action.payload.name == undefined ? "" : action.payload.name;
    let userName = action.payload.userName == undefined ? "" : action.payload.userName;
    let createdBy = action.payload.createdBy == undefined ? "" : action.payload.createdBy;
    let status = action.payload.status == undefined ? "" : action.payload.status;

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ROLES}/get/all?pageNo=${no}&type=${type}&search=${search}&userName=${userName}&name=${name}&createdBy=${createdBy}&status=${status}&orgId=${orgIdGlobal}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.rolesSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.rolesError(response.data));
    }
  } catch (e) {
    yield put(actions.rolesError('Error Occurs !!'));
    console.warn('Some error found in "rolesRequest" action\n', e);
  }
}

export function* addRolesRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.addRolesClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ROLES}/create`, {
        'userName': action.payload.username,
        'name': action.payload.role,
        'userId': action.payload.userId,
        'roles': ""
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.addRolesSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.addRolesError(response.data));
      }
    } catch (e) {
      yield put(actions.addRolesError('Error Occurs !!'));
      console.warn('Some error found in "addRolesRequest" action\n', e);
    }
  }
}

export function* editRolesRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.editRolesClear());
  } else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ROLES}/update`, {
        'userId': action.payload.userId,
        'id': action.payload.id,
        'userName': action.payload.userNameE,
        'roles': action.payload.roles,
        'userRoles': action.payload.userRoles,
        'organisationId': orgIdGlobal
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.editRolesSuccess(response.data.data));
        yield put(actions.rolesRequest(action.payload));
      } else if (finalResponse.failure) {
        yield put(actions.editRolesError(response.data));
      }
    } catch (e) {
      yield put(actions.editRolesError('Error Occurs !!'));
      console.warn('Some error found in "addRolesRequest" action\n', e);
    }
  }
}


// export function* deleteRolesRequest (action) {
//   let id = action.payload;
//   try {
//     const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ROLES}/delete/${id}`, {

//     });
//     const finalResponse = script(response);  
//     if (finalResponse.success) {
//        yield put(actions.deleteRolesSuccess(response.data.data));
//        yield put(actions.rolesRequest());
//     } else if (finalResponse.failure) {
//       yield put(actions.deleteRolesError(response.data.error));
//     }
//   } catch (e) {
//     yield put(actions.deleteRolesError('Error Occurs !!'));
//     console.warn('Some error found in "deleteRolesRequest" action\n', e);
//   }
// }





export function* siteMappingRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type;
    let search = action.payload.search == undefined ? "" : action.payload.search;
    let fromSiteName = action.payload.fromSiteName == undefined ? "" : action.payload.fromSiteName;
    let toSiteName = action.payload.toSiteName == undefined ? "" : action.payload.toSiteName;
    let startDate = action.payload.startDate == undefined ? "" : action.payload.startDate;
    let tptLeadTime = action.payload.tptLeadTime == undefined ? "" : action.payload.tptLeadTime;
    let status = action.payload.status == undefined ? "" : action.payload.status;
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.SITE_MAPPING}/get/all?pageNo=${no}&type=${type}&search=${search}&fromSiteName=${fromSiteName}&toSiteName=${toSiteName}&startDate=${startDate}&tptLeadTime=${tptLeadTime}&status=${status}&orgId=${orgIdGlobal}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.siteMappingSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.siteMappingError(response.data));
    }
  } catch (e) {
    yield put(actions.siteMappingError('Error Occurs !!'));
    console.warn('Some error found in "siteMappingRequest" action\n', e);
  }
}

export function* addSiteMappingRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.addSiteMappingClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.SITE_MAPPING}/create`, {
        'fromSite': action.payload.fromId,
        'fromSiteId': action.payload.from,
        'toSite': action.payload.toId,
        'toSiteId': action.payload.to,
        'startDate': moment(action.payload.fromDate).format(),
        'endDate': moment(action.payload.toDate).format(),
        'tptLeadTime': action.payload.transportationLeadTime,
        'transportationMode': action.payload.transportationMode,
        'status': action.payload.active,
        'orgId': orgIdGlobal
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.addSiteMappingSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.addSiteMappingError(response.data));
      }
    } catch (e) {
      yield put(actions.addSiteMappingError('Error Occurs !!'));
      console.warn('Some error found in "addSiteMappingRequest" action\n', e);
    }
  }
}
export function* editSiteMappingRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.editSiteMappingClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.SITE_MAPPING}/update`, {
        'fromSite': action.payload.fromSite,
        'toSite': action.payload.toSite,
        'startDate': moment(action.payload.startDate).format(),
        'endDate': moment(action.payload.endDate).format(),
        'tptLeadTime': action.payload.tptLeadTimeE,
        'transportationMode': action.payload.transportationModeE,
        'status': action.payload.statusE,
        'toSiteId': action.payload.toSiteId,
        'fromSiteId': action.payload.fromSiteId,
        'id': action.payload.id,
      });
      const finalResponse = script(response);

      if (finalResponse.success) {

        yield put(actions.editSiteMappingSuccess(response.data.data));
        yield put(actions.siteMappingRequest(action.payload));
      } else if (finalResponse.failure) {
        yield put(actions.editSiteMappingError(response.data));
      }
    } catch (e) {
      yield put(actions.editSiteMappingError("error occurs"));
      console.warn('Some error found in "site mapping Request" action\n', e);
    }
  }
}




export function* deleteSiteMappingRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.deleteSiteMappingClear());
  } else {
    try {
      let id = action.payload.idd;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.SITE_MAPPING}/delete/${id}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.deleteSiteMappingSuccess(response.data.data));
        yield put(actions.siteMappingRequest(action.payload));
      } else if (finalResponse.failure) {
        yield put(actions.deleteSiteMappingError(response.data));
      }
    } catch (e) {
      yield put(actions.deleteSiteMappingError('Error Occurs !!'));
      console.warn('Some error found in "deletesitemapingRequest" action\n', e);
    }
  }
}

export function* fromToRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.SITE}/get/all/siteid?orgId=${orgIdGlobal}`, {

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.fromToSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.fromToError(response.data));
    }
  } catch (e) {
    yield put(actions.fromToError('Error Occurs !!'));
    console.warn('Some error found in "fromToError" action\n', e);
  }
}
export function* allRoleRequest(action) {
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/role/get/all`, {

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.allRoleSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.allRoleError(response.data));
    }
  } catch (e) {
    yield put(actions.allRoleError('Error Occurs !!'));
    console.warn('Some error found in "allRole" action\n', e);
  }
}

export function* allUserRequest(action) {
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.USER}/get/all/userid`, {

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.allUserSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.allUserError(response.data));
    }
  } catch (e) {
    yield put(actions.allUserError('Error Occurs !!'));
    console.warn('Some error found in "allUser" action\n', e);
  }
}


export function* sketchersUploadRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/upload`, {
      'bucket': sessionStorage.getItem('bucket') == null ? "NA" : sessionStorage.getItem('bucket'),
      // 'bucket': "com-supplymint-glue",
      'fileName': "/home/sahilturningcloud/Downloads/generatedBy_react-csv.csv",
    });
    const finalResponse = script(response);
    if (finalResponse.success) {

      yield put(actions.sketchersUploadSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.sketchersUploadError(response.data));
    }
  } catch (e) {
    yield put(actions.sketchersUploadError("error occurs"));
    console.warn('Some error found in "sketchersUpload" action\n', e);
  }
}

export function* saveSketchersRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.saveSketchersClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/file/upload`, {
        'bucket': sessionStorage.getItem('bucket') == null ? "NA" : sessionStorage.getItem('bucket'),
        // 'bucket': "com-supplymint-glue",
        'channel': action.payload.channel,
        'template': action.payload.template,
        'filepath': action.payload.file
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.saveSketchersSuccess(response.data.data));
        let data = {
          type: 1,
          no: 1,
          search: "",
          channel: action.payload.channel
        }
        yield put(actions.dataSyncRequest(data));
      } else if (finalResponse.failure) {
        yield put(actions.saveSketchersError(response.data));
      }
    } catch (e) {
      yield put(actions.saveSketchersError("error occurs"));
      console.warn('Some error found in "saveSketchers" action\n', e);
    }
  }
}

export function* getTemplateRequest(action) {
  try {
    let module = action.payload.module == undefined ? "" : action.payload.module;
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/get/template`, {
      'module': module
    });
    const finalResponse = script(response);
    if (finalResponse.success) {

      yield put(actions.getTemplateSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.getTemplateError(response.data));
    }
  } catch (e) {
    yield put(actions.getTemplateError("error occurs"));
    console.warn('Some error found in "getTemplate" action\n', e);
  }
}

export function* downloadRequest(action) {
  try {
    let url = ""
    if (action.payload.openUrl != undefined) {

      let template = action.payload.template;
      let oid = action.payload.oid;
      let ocode = action.payload.ocode;
      let bname = action.payload.bname;
      url = `${CONFIG.BASE_URL}/upload/open/url/download/csv/${template}/${ocode}?orgId=${oid}&orgCode=${ocode}&bucketName=${bname}`

    } else {
      let template = action.payload;
      url = `${CONFIG.BASE_URL}/aws/s3bucket/download/csv/${template}`

    }
    const response = yield call(fireAjax, 'GET', url, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {

      yield put(actions.downloadSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.downloadError(response.data));
    }
  } catch (e) {
    yield put(actions.downloadError("error occurs"));
    console.warn('Some error found in "download" action\n', e);
  }
}

export function* dataSyncRequest(action) {
  try {
    let no = action.payload.no == undefined || action.payload.no == "" ? 1 : action.payload.no;
    let type = action.payload.type;
    let module = action.payload.module == undefined ? "" : action.payload.module

    let search = action.payload.search == undefined ? "" : action.payload.search;
    let name = action.payload.name == undefined ? "" : action.payload.name;
    let status = action.payload.status == undefined ? "" : action.payload.status;
    let key = action.payload.key == undefined ? "" : action.payload.key;
    let eventStart = action.payload.eventStart == undefined ? "" : action.payload.eventStart;
    let eventEnd = action.payload.eventEnd == undefined ? "" : action.payload.eventEnd;
    //let dataCount = action.payload.dataCount == undefined ? "" : action.payload.dataCount;
    let channel = action.payload.channel == undefined ? "" : action.payload.channel
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/get/all/reference`, {
      'pageNo':no,
      'type':type,
      'search':search,
      'name':name,
      'key':key,
      'eventStart':eventStart,
      'eventEnd':eventEnd,
      'status':status,
      //'channel':channel,
      'module': module
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.dataSyncSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.dataSyncError(response.data));
    }

  } catch (e) {
    yield put(actions.dataSyncError("error occurs"));
    console.warn('Some error found in "dataSyncRequest" action\n', e);
  }
}


// ______________________________RETAILMASTER NYSAA_______________________________

export function* paramsettingRequest(action) {
  try {
    let type = action.payload.type;
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/paramsetting/${type}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.paramsettingSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.paramsettingError(response.data));
    }

  } catch (e) {
    yield put(actions.paramsettingError("error occurs"));
    console.warn('Some error found in "paramsettingRequest" action\n', e);
  }
}

// ___________--FESTIVASL SETTING PAGE API'S______________


export function* allFestivalRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.allFestivalClear());
  } else {
    try {
      let year = action.payload.year
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/setting/festival/get/all?year=${year}&orgId=${orgIdGlobal}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.allFestivalSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.allFestivalError(response.data));
      }
    } catch (e) {
      yield put(actions.allFestivalError("error occurs"));
      console.warn('Some error found in allFestival action\n', e);
    }
  }
}

export function* createFestivalRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.createFestivalClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/setting/festival/create`, {
        "updateFestival": action.payload.updateFestival,
        "lastAddFestival": action.payload.lastAddFestival,
        "customAddFestival": action.payload.customAddFestival,
        "fiscalYear": action.payload.fiscalYear,
        "orgId": orgIdGlobal
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.createFestivalSuccess(response.data.data));
        // yield put(actions.checkedListRequest('data'));
      } else if (finalResponse.failure) {
        yield put(actions.createFestivalError(response.data));
      }
    } catch (e) {
      yield put(actions.createFestivalError("error occurs"));
      console.warn('Some error found in createFestival action\n', e);
    }
  }
}

// _____________________promotionalEvents ____________________________

export function* promotionalRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.promotionalClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/promotional/event/create`, {
        name: action.payload.name,
        startDate: action.payload.startDate,
        endDate: action.payload.endDate,
        description: action.payload.description,
        orgId: orgIdGlobal
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.promotionalSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.promotionalError(response.data));
      }
    } catch (e) {
      yield put(actions.promotionalError("error occurs"));
      console.warn('Some error found in promotional action\n', e);
    }
  }
}


// _____________________PROMOTION EVENT_________________________

export function* getPromotionalEventRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getPromotionalEventClear());
  } else {
    try {
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let type = action.payload.type == "" ? 1 : action.payload.type;
      let search = action.payload.search == undefined ? "" : action.payload.search;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/promotional/event/get/all?pageNo=${no}&type=${type}&search=${search}`, {
        "festivalList": action.payload.festivalList,

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getPromotionalEventSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getPromotionalEventError(response.data));
      }
    } catch (e) {
      yield put(actions.getPromotionalEventError("error occurs"));
      console.warn('Some error found in getPromotionalEvent action\n', e);
    }
  }
}

export function* updatePromotionalEventRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updatePromotionalEventClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/promotional/event/update`, {
        id: action.payload.id,
        name: action.payload.name,
        startDate: action.payload.startDate,
        endDate: action.payload.endDate,
        description: action.payload.description
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updatePromotionalEventSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updatePromotionalEventError(response.data));
      }
    } catch (e) {
      yield put(actions.updatePromotionalEventError("error occurs"));
      console.warn('Some error found in updatePromotionalEvent action\n', e);
    }
  }
}
//------------------------------Role Master-------------------------------


export function* disablePromotionalEventRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.disablePromotionalEventClear());
  } else {
    try {
      let isEnable = action.payload.isEnable;
      let id = action.payload.id;

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/promotional/event/status?isEnable=${isEnable}&id=${id}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.disablePromotionalEventSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.disablePromotionalEventError(response.data));
      }
    } catch (e) {
      yield put(actions.disablePromotionalEventError("error occurs"));
      console.warn('Some error found in disablePromotionalEvent action\n', e);
    }
  }
}

export function* deactivePromotionalEventRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.deactivePromotionalEventClear());
  } else {
    try {
      let id = action.payload.id == undefined ? 1 : action.payload.id;
      let userName = action.payload.userName

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/promotional/event/deactivate?id=${id}&userName=${userName}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.deactivePromotionalEventSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.deactivePromotionalEventError(response.data));
      }
    } catch (e) {
      yield put(actions.deactivePromotionalEventError("error occurs"));
      console.warn('Some error found in deactivePromotionalEvent action\n', e);
    }
  }
}
export function* updateRoleRequest(action) {
  var { orgIdGlobal, orgNameGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.updateRoleClear());
  } else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.MODULE}/updaterole/data`, {
        "organisationId": orgIdGlobal,
        "organisationName": orgNameGlobal,
        "enterpriseId": sessionStorage.getItem("partnerEnterpriseId"),
        "roleId": "",
        "roleCode": "",
        "isUpdated": action.payload.isUpdated,
        "enterpriseName": sessionStorage.getItem("partnerEnterpriseName"),
        "roleName": action.payload.roleName,
        "module": action.payload.module


      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateRoleSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updateRoleError(response.data));
      }
    } catch (e) {
      yield put(actions.updateRoleError("error occurs"));
      console.warn('Some error found in updateRole action\n', e);
    }
  }
}

// addRole

export function* moduleGetDataRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.moduleGetDataClear());
  } else {
    try {
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let type = action.payload.type == undefined ? 1 : action.payload.type;
      let search = action.payload.search ==  undefined ? "" : action.payload.search;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/rolemaster/module/data`, {
         "pageNo": no,
         "type": type,
         "search": search 
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.moduleGetDataSuccess(response.data.data));
        // yield put(actions.moduleGetRoleRequest('data'))
      } else if (finalResponse.failure) {
        yield put(actions.moduleGetDataError(response.data));
      }
    } catch (e) {
      yield put(actions.moduleGetDataError("error occurs"));
      console.warn('Some error found in moduleGetData action\n', e);
    }
  }
}
export function* moduleGetRoleRequest(action) {
  var { orgIdGlobal, orgNameGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.moduleGetRoleClear());
  } else {
    try {
      let orgID = orgIdGlobal;
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let type = action.payload.type == undefined ? 1 : action.payload.type;
      let search = action.payload == "data" ? "" : action.payload.search;
      // let id = acton.payload

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.MODULE}/getrole/data`, {
        pageNo: no,
        type: type,
        search: search,

      });

      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.moduleGetRoleSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.moduleGetRoleError(response.data));
      }
    } catch (e) {
      yield put(actions.moduleGetRoleError("error occurs"));
      console.warn('Some error found in moduleGetRole action\n', e);
    }
  }
}
export function* getSubModuleDataRequest(action) {
  var { orgIdGlobal, orgNameGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getSubModuleDataClear());
  } else {
    try {
      let orgId = orgIdGlobal;
      let orgName = orgNameGlobal;
      let roleId = action.payload.roleName.id

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.MODULE}/getsubmodule/data`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getSubModuleDataSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getSubModuleDataError(response.data));
      }
    } catch (e) {
      yield put(actions.getSubModuleDataError("error occurs"));
      console.warn('Some error found in getSubModuleData action\n', e);
    }
  }
}

// _______________________________PARAMETERCUSTOMGETALL_____________________________________

export function* parameterCustomRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.parameterCustomClear());
  } else {
    try {
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/all?pageno=${no}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.parameterCustomSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.parameterCustomError(response.data));
      }
    } catch (e) {
      yield put(actions.parameterCustomError("error occurs"));
      console.warn('Some error found in parameterCustom action\n', e);
    }
  }
}
// _____________proxy store_______________________

export function* proxyStoreRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.proxyStoreClear());
  } else {
    try {
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let type = action.payload.type == "" ? 1 : action.payload.type;
      let search = action.payload.search == undefined ? "" : action.payload.search;
      let orgId = orgIdGlobal
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.SITE}/get/proxystores?pageno=${no}&type=${type}&search=${search}&orgId=${orgId}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.proxyStoreSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.proxyStoreError(response.data));
      }
    } catch (e) {
      yield put(actions.proxyStoreError("error occurs"));
      console.warn('Some error found in proxyStore action\n', e);
    }
  }
}
//-----------------subModuleby ROLE ID
export function* getSubModuleByRoleNameRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getSubModuleByRoleNameClear());
  } else {
    try {
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let type = action.payload.type == undefined ? 1 : action.payload.type;
      let search = action.payload.search == undefined ? "" : action.payload.search;
      // let roleName = action.payload.roleName == undefined ? "" : action.payload.roleName;
      let roleId = action.payload.roleId;
      // let orgId = orgIdGlobal;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/rolemaster/getsubmodule/data/${roleId}`,{
        "pageNo": no,
        "type": type,
        "search": search
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getSubModuleByRoleNameSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getSubModuleByRoleNameError(response.data));
      }
    } catch (e) {
      yield put(actions.getSubModuleByRoleNameError("error occurs"));
      console.warn('Some error found in getSubModuleByRoleName action\n', e);
    }
  }
}



export function* customMasterSkechersRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.customMasterSkechersClear());
  } else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/master/create`, {
        "channel": action.payload.channel,
        "partner": action.payload.partner,
        "zone": action.payload.zone,
        "grade": action.payload.grade,
        "storeCode": action.payload.storeCode,
        "eventName": action.payload.eventName,
        "startDate": moment(action.payload.startDate).format('YYYY-MM-DD 12:00'),
        "endDate": moment(action.payload.endDate).format('YYYY-MM-DD 12:00'),
        "id": action.payload.id,
        "stockDays": action.payload.stockDays,
        "multiRos": action.payload.multiRos,
        "isUpdate": action.payload.isUpdate,
        "isEventNameChanged": action.payload.isEventNameChanged
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.customMasterSkechersSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.customMasterSkechersError(response.data));
      }
    } catch (e) {
      yield put(actions.customMasterSkechersError("error occurs"));
      console.warn('Some error found in customMasterSkechers action\n', e);
    }
  }
}


export function* customGetEventRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.customGetEventClear());
  } else {
    try {
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let type = action.payload.type == undefined ? 1 : action.payload.type;
      let search = action.payload.search;
      let eventName = action.payload.eventName;
      let startDate = action.payload.startDate;
      let endDate = action.payload.endDate;
      let multiplierROS = action.payload.multiplierROS;
      let stockDays = action.payload.stockDays;
      let grade = action.payload.grade;
      let partner = action.payload.partner;
      let zone = action.payload.zone;
      let storeCode = action.payload.storeCode

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/all/event?pageno=${no}&type=${type}&search=${search}&eventName=${eventName}&startDate=${startDate}&endDate=${endDate}&multiplierROS=${multiplierROS}&stockDays=${stockDays}&grade=${grade}&partner=${partner}&zone=${zone}&storeCode=${storeCode}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.customGetEventSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.customGetEventError(response.data));
      }
    } catch (e) {
      yield put(actions.customGetEventError("error occurs"));
      console.warn('Some error found in customGetEventRequest action\n', e);
    }
  }
}

export function* customDeleteEventRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.customDeleteEventClear());
  } else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/delete/event`, {
        "eventName": action.payload.eventName
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.customDeleteEventSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.customDeleteEventError(response.data));
      }
    } catch (e) {
      yield put(actions.customDeleteEventError("error occurs"));
      console.warn('Some error found in customDeleteEvent action\n', e);
    }
  }
}



export function* customGetStoreCodeRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.customGetStoreCodeClear());
  } else {
    try {
      let partner = []

      action.payload.partner != "" ? partner.push(action.payload.partner) : []
      var { orgCodeGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/get/storeCode`, {
        channel: orgCodeGlobal,
        zone: action.payload.zone.length == 0 ? [] : action.payload.zone,
        grade: action.payload.grade.length == 0 ? [] : action.payload.grade,
        partner: partner


      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.customGetStoreCodeSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.customGetStoreCodeError(response.data));
      }
    } catch (e) {
      yield put(actions.customGetStoreCodeError("error occurs"));
      console.warn('Some error found in "customGetStoreCode" action\n', e);
    }
  }
}

export function* customGetGradeRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.customGetGradeClear());
  } else {
    try {
      // let type = action.payload.type == undefined ? 1 : action.payload.type;
      // let no = action.payload.no == undefined ? 1 : action.payload.no;
      // let scode = action.payload.scode == undefined ? "" : action.payload.scode;
      let channel = action.payload.channel;
      var { orgCodeGlobal } = OganisationIdName()
      let zone = action.payload.zone;
      let partner = action.payload.partner;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/get/grade`, {
        channel: orgCodeGlobal,
        partner: partner,
        zone: zone

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.customGetGradeSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.customGetGradeError(response.data));
      }
    } catch (e) {
      yield put(actions.customGetGradeError("error occurs"));
      console.warn('Some error found in "customGetGrade" action\n', e);
    }
  }
}
export function* customGetZoneRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.customGetZoneClear());
  } else {
    try {
      var { orgCodeGlobal } = OganisationIdName()
      let channel = action.payload.channel;
      let partner = action.payload.partner;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/zone?channel=${orgCodeGlobal}&partner=${partner}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.customGetZoneSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.customGetZoneError(response.data));
      }
    } catch (e) {
      yield put(actions.customGetZoneError("error occurs"));
      console.warn('Some error found in "customGetZone" action\n', e);
    }
  }
}

export function* customGetPartnerRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.customGetPartnerClear());
  } else {
    try {
      let channel = action.payload.channel
      var { orgCodeGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/partner?channel=${orgCodeGlobal}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.customGetPartnerSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.customGetPartnerError(response.data));
      }
    } catch (e) {
      yield put(actions.customGetPartnerError("error occurs"));
      console.warn('Some error found in "customGetPartner" action\n', e);
    }
  }
}



export function* customApplyFilterRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.customApplyFilterClear());
  } else {
    try {
      // let channel = action.payload.channel;
      // var { orgCodeGlobal } = OganisationIdName()
      // let partner = action.payload.partner;
      // let zone = action.payload.zone;
      // let grade = action.payload.grade;
      // let storeCode = action.payload.storeCode;
      let partner = []
      action.payload.partner != "" ? partner.push(action.payload.partner) : []

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/filter/storeCode`, {

        "channel": action.payload.channel,
        "partner": partner,
        "grade": action.payload.grade,
        "zone": action.payload.zone,
        "storeCode": action.payload.storeCode

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.customApplyFilterSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.customApplyFilterError(response.data));
      }
    } catch (e) {
      yield put(actions.customApplyFilterError("error occurs"));
      console.warn('Some error found in "customApplyFilter" action\n', e);
    }
  }
}

// get retailer request

export function* allRetailerRequest(action) {
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/vendor/get/enterprises`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.allRetailerSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.allRetailerError(response.data));
    }
  } catch (e) {
    yield put(actions.allRetailerError('Error Occurs !!'));
    console.warn('Some error found in "allRetailer" action\n', e);
  }
}

// check RoleName Request 
export function* checkRoleNameRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.checkRoleNameClear());
  } else {
    try {
      let roleName = action.payload.roleName

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/rolemaster/rolemodule/role/check`, {
        "roleName":roleName});
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.checkRoleNameSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.checkRoleNameError(response.data));
      }
    } catch (e) {
      yield put(actions.checkRoleNameError("error occurs"));
      console.warn('Some error found in CheckRoleName action\n', e);
    }
  }
}
// New Role name Submit Request
export function* roleSubmitRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.roleSubmitClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/rolemaster/rolemodule/role/create`, {

        "roleName": action.payload.roleName,
        "roleAccess": action.payload.roleAccess,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.roleSubmitSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.roleSubmitError(response.data));
      }
    } catch (e) {
      yield put(actions.roleSubmitError("error occurs"));
      console.warn('Some error found in "roleSubmit" action\n', e);
    }
  }
}
// Role name Update Request
export function* roleUpdateDataRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.roleUpdateDataClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/rolemaster/rolemodule/role/update`, {
        "roleId":action.payload.roleId,
        "roleName": action.payload.roleName,
        "roleAccess": action.payload.roleAccess,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.roleUpdateDataSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.roleUpdateDataError(response.data));
      }
    } catch (e) {
      yield put(actions.roleUpdateDataError("error occurs"));
      console.warn('Some error found in "roleUpdateData" action\n', e);
    }
  }
}
//Api logs
export function* getModuleApiLogsRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getModuleApiLogsClear());
  } else {
    try {
     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/intapi/find/log`,{
      "pageNo":action.payload.pageNo,
      "type": action.payload.type,
      "search": action.payload.search,
      "sortedBy": action.payload.sortedBy,
      "sortedIn": action.payload.sortedIn,
      "filter": action.payload.filter
     });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getModuleApiLogsSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getModuleApiLogsError(response.data));
      }
    } catch (e) {
      yield put(actions.getModuleApiLogsError("error occurs"));
      console.warn('Some error found in getModuleApiLogs action\n', e);
    }
  }
}

// delete role
export function* roleDeleteRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.roleDeleteClear());
  } else {
    try {
     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/rolemaster/rolemodule/role/delete/${action.payload.roleId}`);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.roleDeleteSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.roleDeleteError(response.data));
      }
    } catch (e) {
      yield put(actions.roleDeleteError("error occurs"));
      console.warn('Some error found in groleDelete action\n', e);
    }
  }
}

export function* getSubscriptionRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getSubscriptionClear());
  } else {
    try {
     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_VENDOR}/get/subscription`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getSubscriptionSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getSubscriptionError(response.data));
      }
    } catch (e) {
      yield put(actions.getSubscriptionError("error occurs"));
      console.warn('Some error found in getSubscriptionRequest action\n', e);
    }
  }
}

export function* getTransactionRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getTransactionClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_VENDOR}/get/plan/transactions`,{

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getTransactionSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getTransactionError(response.data));
      }
    } catch (e) {
      yield put(actions.getTransactionError("error occurs"));
      console.warn('Some error found in getTransactionRequest action\n', e);
    }
  }
}

//-------------------- GET API ACCESS DETAILS --------------------
export function* getApiAccessDetailsRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getApiAccessDetailsClear());
  } else {
    try {
     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/org/get/erp/details`,{});
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getApiAccessDetailsSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getApiAccessDetailsError(response.data));
      }
    } catch (e) {
      yield put(actions.getApiAccessDetailsError("error occurs"));
      console.warn('Some error found in getApiAccessDetails action\n', e);
    }
  }
}

//-------------------- GET EMAIL NOTIFICATION LOG --------------------
export function* getEmailNotificationLogRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getEmailNotificationLogClear());
  } else {
    try {
     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/notification/email/find/log`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getEmailNotificationLogSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getEmailNotificationLogError(response.data));
      }
    } catch (e) {
      yield put(actions.getEmailNotificationLogError("error occurs"));
      console.warn('Some error found in getEmailNotificationLog action\n', e);
    }
  }
}


//-------------------- RESEND EMAIL NOTIFICATION --------------------
export function* resendEmailNotificationRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.resendEmailNotificationClear());
  } else {
    try {
     const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/notification/email/resend/${action.payload}`, {});
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.resendEmailNotificationSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.resendEmailNotificationError(response.data));
      }
    } catch (e) {
      yield put(actions.resendEmailNotificationError("error occurs"));
      console.warn('Some error found in getEmailNotificationLog action\n', e);
    }
  }
}


//-------------------- GET EMAIL BODY --------------------
export function* getEmailBodyRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getEmailBodyClear());
  } else {
    try {
     const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/notification/email/body/${action.payload}`, {});
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getEmailBodySuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getEmailBodyError(response.data));
      }
    } catch (e) {
      yield put(actions.getEmailBodyError("error occurs"));
      console.warn('Some error found in getEmailBody action\n', e);
    }
  }
}


export function* getUserDataMappingRequest(action) {
  if (action.payload === undefined) {
    yield put(actions.getUserDataMappingClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/user/get/datamapping/info`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getUserDataMappingSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getUserDataMappingError(response.data));
      }
    } catch (e) {
      yield put(actions.getUserDataMappingError('Error Occurs !!'));
      console.warn('Some error found in "user" action\n', e);
    }
  }
}

export function* updateUserDataMappingRequest(action) {
  if (action.payload === undefined) {
    yield put(actions.updateUserDataMappingClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/user/update/datamapping/info`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateUserDataMappingSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updateUserDataMappingError(response.data));
      }
    } catch (e) {
      yield put(actions.updateUserDataMappingError('Error Occurs !!'));
      console.warn('Some error found in "user" action\n', e);
    }
  }
}

export function* getInsertRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getInsertClear());
  } else {
    try {
     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/org/update/erp/outbound`,action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getInsertSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getInsertError(response.data));
      }
    } catch (e) {
      yield put(actions.getInsertError("error occurs"));
      console.warn('Some error found in getInsert action\n', e);
    }
  }
}
export function* saveInsertRequest(action) {
    
  if (action.payload == undefined) {

      console.log("saveInsertRequest if");
      yield put(actions.saveInsertClear());
  }
  else {
      try {
          console.log("saveInsertRequest try", action.payload);
          const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/org/update/erp/outbound`,
              action.payload
          );
          const finalResponse = script(response);
          if (finalResponse.success) {
              yield put(actions.saveInsertSuccess(response.data.data));
              // yield put(actions.runOnDemandRequest(action.payload));
          }
          else if (finalResponse.failure) {
              yield put(actions.saveInsertError(response.data));
          }
      }
      catch (e) {
          console.log("saveInsertRequest catch");
          yield put(actions.saveInsertError("error occured"));
          console.warn('Some error found in "saveInsertRequest" action\n', e);
      }
  }
}

export function* allRetailerCustomerRequest(action) {
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/customer/get/enterprises`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.allRetailerCustomerSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.allRetailerCustomerError(response.data));
    }
  } catch (e) {
    yield put(actions.allRetailerCustomerError('Error Occurs !!'));
    console.warn('Some error found in "allRetailerCustomer" action\n', e);
  }
}

export function* customerUserRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type == undefined ? 1 : action.payload.type;
    let search = action.payload.search == undefined ? "" : action.payload.search;
    let partnerEnterpriseName = action.payload.partnerEnterpriseName;
    let firstName = action.payload.firstName;
    let middleName = action.payload.middleName;
    let lastName = action.payload.lastName;
    let userName = action.payload.userName;
    let email = action.payload.email;
    let status = action.payload.status;

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/customer/get/all`, {
      pageNo: no,
      type,
      search,
      partnerEnterpriseName,
      firstName,
      middleName,
      lastName,
      userName,
      email,
      status
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.customerUserSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.customerUserError(response.data));
    }
  } catch (e) {
    yield put(actions.customerUserError('Error Occurs !!'));
    console.warn('Some error found in "customerUser" action\n', e);
  }
}

export function* addUserCustomerRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.addUserCustomerClear());
  }
  else {

    try {
      var { orgIdGlobal } = OganisationIdName()
      let xyz = [], idMap = [];
      let abc = "";
      let additionalUrl = action.payload.additionalUrl == undefined ? "" : action.payload.additionalUrl
      if (action.payload.selected != undefined) {

        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].id;
          if (action.payload.selected.length > 1) {
            abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
          } else {
            abc = a;
          }
        }


        for (let i = 0; i < action.payload.selected.length; i++) {
          xyz.push(action.payload.selected[i].name)
        }
        for (let i = 0; i < action.payload.selected.length; i++) {
          idMap.push((action.payload.selected[i].id).toString())
        }
      }

      if(action.payload.uType == "CUSTOMER" && action.payload.selected !== undefined) {

        abc="";
        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].entorgid;
          if (action.payload.selected.length > 1) {
            abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
          } else {
            abc = a;
          }
        }

          const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_CUSTOMER}/create/user${additionalUrl}`, {
            'firstName': action.payload.first,
            'middleName': action.payload.middle,
            'lastName': action.payload.last,
            'email': action.payload.email,
            'mobileNumber': action.payload.mobile,
            'workPhone': action.payload.phone,
            'accessMode': action.payload.access,
            'status': action.payload.active,
            'address': action.payload.address,
            'partnerEnterpriseId': sessionStorage.getItem('partnerEnterpriseId'),
            'partnerEnterpriseName': sessionStorage.getItem('partnerEnterpriseName'),
            'userName': action.payload.user,
            'userRoles': action.payload.uType == "CUSTOMER" ? idMap : xyz,
            'retailers': abc,
            "uType": action.payload.uType,
            "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
            "slName": action.payload.slName != undefined ? action.payload.slName : "",
            "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
            'userAsAdmin': action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

            // user: user,
          });
          const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.addUserCustomerSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.addUserCustomerError(response.data));
      }
      } else {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR_USER}/create${additionalUrl}`, {
          'firstName': action.payload.first,
          'middleName': action.payload.middle,
          'lastName': action.payload.last,
          'email': action.payload.email,
          'mobileNumber': action.payload.mobile,
          'workPhone': action.payload.phone,
          'accessMode': action.payload.access,
          'status': action.payload.active,
          'address': action.payload.address,
          'partnerEnterpriseId': sessionStorage.getItem('partnerEnterpriseId'),
          'partnerEnterpriseName': sessionStorage.getItem('partnerEnterpriseName'),
          'userName': action.payload.user,
          'userRoles': action.payload.uType == "CUSTOMER" ? idMap : xyz,
          'roles': abc,
          "uType": action.payload.uType,
          "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
          "slName": action.payload.slName != undefined ? action.payload.slName : "",
          "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
          'userAsAdmin': action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

          // user: user,
        });
        const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.addUserCustomerSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.addUserCustomerError(response.data));
      }
      }
    
      
    } catch (e) {
      yield put(actions.addUserCustomerError('Error Occurs !!'));
      console.warn('Some error found in "user" action\n', e);
    }
  }
}

export function* editUserCustomerRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  let roles = "";
  if (action.payload == undefined) {
    yield put(actions.editUserCustomerClear());
  }
  else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      
      if(action.payload.uType == 'ENT') {

        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].id;
          if (action.payload.selected.length > 1) {
            roles = roles == "" ? "".concat(a) : roles.concat('|').concat(a);
          } else {
            roles = a;
          }
        }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR_USER}/update`, {
        'userId': action.payload.userId,
        'firstName': action.payload.firstNameE != undefined ? action.payload.firstNameE : "",
        'middleName': action.payload.middleNameE != undefined ? action.payload.middleNameE : "",
        'lastName': action.payload.lastNameE != undefined ? action.payload.lastNameE : "",
        'email': action.payload.emailE != undefined ? action.payload.emailE : "",
        'mobileNumber': action.payload.mobileNumber != undefined ? action.payload.mobileNumber : "",
        'workPhone': action.payload.workPhone,
        'accessMode': action.payload.accessMode,
        'roles': roles == "" ? null : roles,
        'status': action.payload.active,
        'slCode': action.payload.slCode != undefined ? action.payload.slCode : "",
        'slName': action.payload.slName != undefined ? action.payload.slName : "",
        'address': action.payload.address != undefined ? action.payload.address : "",
        'partnerEnterpriseId': action.payload.partnerEnterpriseId != undefined ? action.payload.partnerEnterpriseId : "",
        'partnerEnterpriseName': action.payload.partnerEnterpriseNameE != undefined ? action.payload.partnerEnterpriseNameE : "",
        'userName': action.payload.userNameE,
        'userRoles': action.payload.selected != undefined ? action.payload.selected : [],
        "uType": action.payload.uType != undefined ? action.payload.uType : "ENT",
        "newUserName": action.payload.newUserName,
        "newPassword": action.payload.newPassword,
        "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
        "userAsAdmin": action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

        // user: user,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.editUserCustomerSuccess(response.data.data));
        // yield put(actions.userRequest(action.payload));
        //yield put(actions.getUserNameRequest(action.payload.userNameE));
      } else if (finalResponse.failure) {
        yield put(actions.editUserCustomerError(response.data));
      }
    } else {

      roles="";
      if(action.payload.selected!= undefined) {
        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].entorgid;
          if (action.payload.selected.length > 1) {
            roles = roles == "" ? "".concat(a) : roles.concat('|').concat(a);
          } else {
            roles = a;
          }
        }
      }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_CUSTOMER}/update/user`, {
        'userId': action.payload.userId,
        'firstName': action.payload.firstNameE != undefined ? action.payload.firstNameE : "",
        'middleName': action.payload.middleNameE != undefined ? action.payload.middleNameE : "",
        'lastName': action.payload.lastNameE != undefined ? action.payload.lastNameE : "",
        'email': action.payload.emailE != undefined ? action.payload.emailE : "",
        'mobileNumber': action.payload.mobileNumber != undefined ? action.payload.mobileNumber : "",
        'workPhone': action.payload.workPhone,
        'accessMode': action.payload.accessMode,
        'retailers': roles == "" ? null : roles,
        'status': action.payload.active,
        'slCode': action.payload.slCode != undefined ? action.payload.slCode : "",
        'slName': action.payload.slName != undefined ? action.payload.slName : "",
        'address': action.payload.address != undefined ? action.payload.address : "",
        'partnerEnterpriseId': action.payload.partnerEnterpriseId != undefined ? action.payload.partnerEnterpriseId : "",
        'partnerEnterpriseName': action.payload.partnerEnterpriseNameE != undefined ? action.payload.partnerEnterpriseNameE : "",
        'userName': action.payload.userNameE,
        'userRoles': action.payload.selected != undefined ? action.payload.selected : [],
        "uType": action.payload.uType != undefined ? action.payload.uType : "ENT",
        "newUserName": action.payload.newUserName,
        "newPassword": action.payload.newPassword,
        "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
        "userAsAdmin": action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

        // user: user,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.editUserCustomerSuccess(response.data.data));
        // yield put(actions.userRequest(action.payload));
        //yield put(actions.getUserNameRequest(action.payload.userNameE));
      } else if (finalResponse.failure) {
        yield put(actions.editUserCustomerError(response.data));
      }
    }
    } catch (e) {
      yield put(actions.editUserCustomerError('Error Occurs !!'));
      console.warn('Some error found in "user edit" action\n', e);
    }
  }

}

export function* userStatusCustomerRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.userStatusCustomerClear());
  }
  else {
    try {
      if(action.payload.uType!=undefined && action.payload.uType=='ENT') {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR_USER}/update/status`, {
          userName: action.payload.userName,
          status: action.payload.active,
          userId: action.payload.userId
          // user: user,
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
  
          yield put(actions.userStatusCustomerSuccess(response.data.data));
          yield put(actions.userRequest(action.payload));
        } else if (finalResponse.failure) {
          yield put(actions.userStatusCustomerError(response.data));
        }
      } else {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_CUSTOMER}/update/status`, {
        userName: action.payload.userName,
        status: action.payload.active,
        userId: action.payload.userId
        // user: user,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.userStatusCustomerSuccess(response.data.data));
        yield put(actions.userRequest(action.payload));
      } else if (finalResponse.failure) {
        yield put(actions.userStatusCustomerError(response.data));
      }
    }
    } catch (e) {
      yield put(actions.userStatusCustomerError('Error Occurs !!'));
      console.warn('Some error found in "user status" action\n', e);
    }
  }
}

export function* getSubscriptionCustomerRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getSubscriptionCustomerClear());
  } else {
    try {
     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_CUSTOMER}/get/subscription`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getSubscriptionCustomerSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getSubscriptionCustomerError(response.data));
      }
    } catch (e) {
      yield put(actions.getSubscriptionCustomerError("error occurs"));
      console.warn('Some error found in getSubscriptionCustomerRequest action\n', e);
    }
  }
}

export function* getTransactionCustomerRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getTransactionCustomerClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.CORE_CUSTOMER}/get/plan/transactions`,{

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getTransactionCustomerSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getTransactionCustomerError(response.data));
      }
    } catch (e) {
      yield put(actions.getTransactionCustomerError("error occurs"));
      console.warn('Some error found in getTransactionCustomerRequest action\n', e);
    }
  }
}