import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
  organization: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  addOrganization: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  editOrganization: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deleteOrganization: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  site: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  addSite: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  editSite: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deleteSite: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  user: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  addUser: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  editUser: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deleteUser: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  userStatus: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  roles: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  addRoles: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  editRoles: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deleteRoles: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  siteMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  addSiteMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  editSiteMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deleteSiteMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  fromTo: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  allRole: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  allUser: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  sketchersUpload: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  saveSketchers: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getTemplate: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  download: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  dataSync: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  allFestival: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createFestival: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  paramsetting: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  promotional: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getPromotionalEvent: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updatePromotionalEvent: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deactivePromotionalEvent: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''

  },
  disablePromotionalEvent: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  updateRole: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  moduleGetData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  moduleGetRole: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getSubModuleData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  proxyStore: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  parameterCustom: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customMasterSkechers: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customGetEvent: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customDeleteEvent: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customGetPartner: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customGetGrade: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getSubModuleByRoleName: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customGetZone: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customGetStoreCode: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customApplyFilter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  allRetailer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  vendorUser: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  checkRoleNameData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  roleSubmitData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  roleUpdateData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getModuleApiLogsData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  roleDelete: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getSubscription: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getTransaction: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getApiAccessDetails: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getEmailNotificationLog: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  resendEmailNotification: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getEmailBody: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getUserDataMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
  },

  getInsertData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateUserDataMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  allRetailerCustomer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  customerUser: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  addUserCustomer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  editUserCustomer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  userStatusCustomer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getSubscriptionCustomer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getTransactionCustomer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
};

const createFestivalRequest = (state, action) => update(state, {
  createFestival: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const createFestivalSuccess = (state, action) => update(state, {
  createFestival: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'createFestival success' }
  }
})

const createFestivalError = (state, action) => update(state, {
  createFestival: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const createFestivalClear = (state, action) => update(state, {
  createFestival: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const allFestivalRequest = (state, action) => update(state, {
  allFestival: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const allFestivalSuccess = (state, action) => update(state, {
  allFestival: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'allFestival success' }
  }
});

const allFestivalError = (state, action) => update(state, {
  allFestival: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const allFestivalClear = (state, action) => update(state, {
  allFestival: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const organizationRequest = (state, action) => update(state, {
  organization: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const organizationSuccess = (state, action) => update(state, {
  organization: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Organisation success' }
  }
});

const organizationError = (state, action) => update(state, {
  organization: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const organizationClear = (state, action) => update(state, {
  organization: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const editOrganizationRequest = (state, action) => update(state, {
  editOrganization: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editOrganizationSuccess = (state, action) => update(state, {
  editOrganization: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'edit organization success' }
  }
});

const editOrganizationError = (state, action) => update(state, {
  editOrganization: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const editOrganizationClear = (state, action) => update(state, {
  editOrganization: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const addOrganizationRequest = (state, action) => update(state, {
  addOrganization: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addOrganizationSuccess = (state, action) => update(state, {
  addOrganization: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Add Organisation success' }
  }
});

const addOrganizationError = (state, action) => update(state, {
  addOrganization: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const addOrganizationClear = (state, action) => update(state, {
  addOrganization: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const deleteOrganizationRequest = (state, action) => update(state, {
  deleteOrganization: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteOrganizationSuccess = (state, action) => update(state, {
  deleteOrganization: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Add Organisation success' }
  }
});

const deleteOrganizationError = (state, action) => update(state, {
  deleteOrganization: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const deleteOrganizationClear = (state, action) => update(state, {
  deleteOrganization: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const siteRequest = (state, action) => update(state, {
  site: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const siteSuccess = (state, action) => update(state, {
  site: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Site success' }
  }
});

const siteError = (state, action) => update(state, {
  site: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const siteClear = (state, action) => update(state, {
  site: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const addSiteRequest = (state, action) => update(state, {
  addSite: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addSiteClear = (state, action) => update(state, {
  addSite: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addSiteSuccess = (state, action) => update(state, {
  addSite: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Add Site success' }
  }
});

const addSiteError = (state, action) => update(state, {
  addSite: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const editSiteRequest = (state, action) => update(state, {
  editSite: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editSiteClear = (state, action) => update(state, {
  editSite: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editSiteSuccess = (state, action) => update(state, {
  editSite: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Add Site success' }
  }
});

const editSiteError = (state, action) => update(state, {
  editSite: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const deleteSiteRequest = (state, action) => update(state, {
  deleteSite: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteSiteClear = (state, action) => update(state, {
  deleteSite: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteSiteSuccess = (state, action) => update(state, {
  deleteSite: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Add Site success' }
  }
});

const deleteSiteError = (state, action) => update(state, {
  deleteSite: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const userRequest = (state, action) => update(state, {
  user: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const userSuccess = (state, action) => update(state, {
  user: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'user success' }
  }
});

const userError = (state, action) => update(state, {
  user: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const addUserRequest = (state, action) => update(state, {
  addUser: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addUserSuccess = (state, action) => update(state, {
  addUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'user success' }
  }
});

const addUserError = (state, action) => update(state, {
  addUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const addUserClear = (state, action) => update(state, {
  addUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const editUserRequest = (state, action) => update(state, {
  editUser: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editUserSuccess = (state, action) => update(state, {
  editUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'user success' }
  }
});

const editUserError = (state, action) => update(state, {
  editUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const editUserClear = (state, action) => update(state, {
  editUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const deleteUserRequest = (state, action) => update(state, {
  deleteUser: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteUserSuccess = (state, action) => update(state, {
  deleteUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'user success' }
  }
});

const deleteUserError = (state, action) => update(state, {
  deleteUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const deleteUserClear = (state, action) => update(state, {
  deleteUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
//user status
const userStatusRequest = (state, action) => update(state, {
  userStatus: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const userStatusSuccess = (state, action) => update(state, {
  userStatus: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'user Status' }
  }
});

const userStatusError = (state, action) => update(state, {
  userStatus: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const userStatusClear = (state, action) => update(state, {
  userStatus: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});




const rolesRequest = (state, action) => update(state, {
  roles: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const rolesSuccess = (state, action) => update(state, {
  roles: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Roles success' }
  }
});

const rolesError = (state, action) => update(state, {
  roles: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const addRolesRequest = (state, action) => update(state, {
  addRoles: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addRolesClear = (state, action) => update(state, {
  addRoles: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addRolesSuccess = (state, action) => update(state, {
  addRoles: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'addRoles success' }
  }
});

const addRolesError = (state, action) => update(state, {
  addRoles: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const editRolesRequest = (state, action) => update(state, {
  editRoles: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editRolesClear = (state, action) => update(state, {
  editRoles: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editRolesSuccess = (state, action) => update(state, {
  editRoles: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'addRoles success' }
  }
});

const editRolesError = (state, action) => update(state, {
  editRoles: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const deleteRolesRequest = (state, action) => update(state, {
  deleteRoles: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteRolesSuccess = (state, action) => update(state, {
  deleteRoles: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'addRoles success' }
  }
});

const deleteRolesError = (state, action) => update(state, {
  deleteRoles: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const siteMappingRequest = (state, action) => update(state, {
  siteMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const siteMappingSuccess = (state, action) => update(state, {
  siteMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'site mapping success' }
  }
});

const siteMappingError = (state, action) => update(state, {
  siteMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const addSiteMappingRequest = (state, action) => update(state, {
  addSiteMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addSiteMappingClear = (state, action) => update(state, {
  addSiteMapping: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addSiteMappingSuccess = (state, action) => update(state, {
  addSiteMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'add site mapping success' }
  }
});

const addSiteMappingError = (state, action) => update(state, {
  addSiteMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const editSiteMappingRequest = (state, action) => update(state, {
  editSiteMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editSiteMappingClear = (state, action) => update(state, {
  editSiteMapping: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editSiteMappingSuccess = (state, action) => update(state, {
  editSiteMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'edit site mapping success' }
  }
});

const editSiteMappingError = (state, action) => update(state, {
  editSiteMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const deleteSiteMappingRequest = (state, action) => update(state, {
  deleteSiteMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteSiteMappingClear = (state, action) => update(state, {
  deleteSiteMapping: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteSiteMappingSuccess = (state, action) => update(state, {
  deleteSiteMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'edit site mapping success' }
  }
});

const deleteSiteMappingError = (state, action) => update(state, {
  deleteSiteMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const fromToRequest = (state, action) => update(state, {
  fromTo: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const fromToSuccess = (state, action) => update(state, {
  fromTo: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'fromTo success' }
  }
});

const fromToError = (state, action) => update(state, {
  fromTo: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const allRoleRequest = (state, action) => update(state, {
  allRole: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const allRoleSuccess = (state, action) => update(state, {
  allRole: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'allRole success' }
  }
});

const allRoleError = (state, action) => update(state, {
  allRole: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const allRoleClear = (state, action) => update(state, {
  allRole: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const allUserRequest = (state, action) => update(state, {
  allUser: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const allUserSuccess = (state, action) => update(state, {
  allUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'allUser success' }
  }
});

const allUserError = (state, action) => update(state, {
  allUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const sketchersUploadRequest = (state, action) => update(state, {
  sketchersUpload: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const sketchersUploadSuccess = (state, action) => update(state, {
  sketchersUpload: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'sketchersUpload success' }
  }
});

const sketchersUploadError = (state, action) => update(state, {
  sketchersUpload: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const saveSketchersRequest = (state, action) => update(state, {
  saveSketchers: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const saveSketchersSuccess = (state, action) => update(state, {
  saveSketchers: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'saveSketchers success' }
  }
});

const saveSketchersError = (state, action) => update(state, {
  saveSketchers: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const saveSketchersClear = (state, action) => update(state, {
  saveSketchers: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getTemplateRequest = (state, action) => update(state, {
  getTemplate: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getTemplateSuccess = (state, action) => update(state, {
  getTemplate: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'getTemplate success' }
  }
});

const getTemplateError = (state, action) => update(state, {
  getTemplate: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getTemplateClear = (state, action) => update(state, {
  getTemplate: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const downloadRequest = (state, action) => update(state, {
  download: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const downloadSuccess = (state, action) => update(state, {
  download: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'download success' }
  }
});

const downloadError = (state, action) => update(state, {
  download: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const downloadClear = (state, action) => update(state, {
  download: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const dataSyncRequest = (state, action) => update(state, {
  dataSync: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const dataSyncSuccess = (state, action) => update(state, {
  dataSync: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'dataSync success' }
  }
});

const dataSyncError = (state, action) => update(state, {
  dataSync: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const dataSyncClear = (state, action) => update(state, {
  dataSync: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const paramsettingRequest = (state, action) => update(state, {
  paramsetting: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const promotionalRequest = (state, action) => update(state, {
  promotional: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const promotionalSuccess = (state, action) => update(state, {
  promotional: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'promotional success' }
  }
})

const promotionalError = (state, action) => update(state, {
  promotional: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const promotionalClear = (state, action) => update(state, {
  promotional: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const paramsettingSuccess = (state, action) => update(state, {
  paramsetting: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },

    message: { $set: 'paramsetting success' }
  }
});

const paramsettingError = (state, action) => update(state, {
  paramsetting: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const paramsettingClear = (state, action) => update(state, {
  paramsetting: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


// _________________PROMOTIONAL EVENT ______________________

const getPromotionalEventRequest = (state, action) => update(state, {
  getPromotionalEvent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getPromotionalEventSuccess = (state, action) => update(state, {
  getPromotionalEvent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'getPromotionalEvent success' }
  }
})

const getPromotionalEventError = (state, action) => update(state, {
  getPromotionalEvent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getPromotionalEventClear = (state, action) => update(state, {
  getPromotionalEvent: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
// _____---update____________________
const updatePromotionalEventRequest = (state, action) => update(state, {
  updatePromotionalEvent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const updatePromotionalEventSuccess = (state, action) => update(state, {
  updatePromotionalEvent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'updatePromotionalEvent success' }
  }
})

const updatePromotionalEventError = (state, action) => update(state, {
  updatePromotionalEvent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const updatePromotionalEventClear = (state, action) => update(state, {
  updatePromotionalEvent: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
// ________________isdisable____________
const disablePromotionalEventRequest = (state, action) => update(state, {
  disablePromotionalEvent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const disablePromotionalEventSuccess = (state, action) => update(state, {
  disablePromotionalEvent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'disablePromotionalEvent success' }
  }
})

const disablePromotionalEventError = (state, action) => update(state, {
  disablePromotionalEvent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const disablePromotionalEventClear = (state, action) => update(state, {
  disablePromotionalEvent: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
// ____________deactive_____________

const deactivePromotionalEventRequest = (state, action) => update(state, {
  deactivePromotionalEvent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const deactivePromotionalEventSuccess = (state, action) => update(state, {
  deactivePromotionalEvent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'deactivePromotionalEvent success' }
  }
})

const deactivePromotionalEventError = (state, action) => update(state, {
  deactivePromotionalEvent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const deactivePromotionalEventClear = (state, action) => update(state, {
  deactivePromotionalEvent: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

//role master

const updateRoleRequest = (state, action) => update(state, {
  updateRole: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const updateRoleSuccess = (state, action) => update(state, {
  updateRole: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'updateRole success' }
  }
})

const updateRoleError = (state, action) => update(state, {
  updateRole: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const updateRoleClear = (state, action) => update(state, {
  updateRole: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: 'clear' }
  }
})

const moduleGetDataRequest = (state, action) => update(state, {
  moduleGetData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const moduleGetDataSuccess = (state, action) => update(state, {
  moduleGetData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'moduleGetData success' }
  }
})

const moduleGetDataError = (state, action) => update(state, {
  moduleGetData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const moduleGetDataClear = (state, action) => update(state, {
  moduleGetData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const moduleGetRoleRequest = (state, action) => update(state, {
  moduleGetRole: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const moduleGetRoleSuccess = (state, action) => update(state, {
  moduleGetRole: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'moduleGetRole success' }
  }
})

const moduleGetRoleError = (state, action) => update(state, {
  moduleGetRole: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const moduleGetRoleClear = (state, action) => update(state, {
  moduleGetRole: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const getSubModuleDataRequest = (state, action) => update(state, {
  getSubModuleData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getSubModuleDataSuccess = (state, action) => update(state, {
  getSubModuleData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'getSubModuleData success' }
  }
})

const getSubModuleDataError = (state, action) => update(state, {
  getSubModuleData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getSubModuleDataClear = (state, action) => update(state, {
  getSubModuleData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})



// ___________________  PROXY STORE_____________________


const proxyStoreRequest = (state, action) => update(state, {
  proxyStore: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const proxyStoreSuccess = (state, action) => update(state, {
  proxyStore: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'proxyStore success' }
  }
})

const proxyStoreError = (state, action) => update(state, {
  proxyStore: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const proxyStoreClear = (state, action) => update(state, {
  proxyStore: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const getSubModuleByRoleNameRequest = (state, action) => update(state, {
  getSubModuleByRoleName: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

})

const getSubModuleByRoleNameSuccess = (state, action) => update(state, {
  getSubModuleByRoleName: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'getSubModuleByRoleName success' }
  }
})

const getSubModuleByRoleNameError = (state, action) => update(state, {
  getSubModuleByRoleName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getSubModuleByRoleNameClear = (state, action) => update(state, {
  getSubModuleByRoleName: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


const customMasterSkechersRequest = (state, action) => update(state, {
  customMasterSkechers: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customMasterSkechersSuccess = (state, action) => update(state, {
  customMasterSkechers: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customMasterSkechers success' }
  }
})

const customMasterSkechersError = (state, action) => update(state, {
  customMasterSkechers: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customMasterSkechersClear = (state, action) => update(state, {
  customMasterSkechers: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const customGetEventRequest = (state, action) => update(state, {
  customGetEvent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customGetEventSuccess = (state, action) => update(state, {
  customGetEvent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customGetEvent success' }
  }
})

const customGetEventError = (state, action) => update(state, {
  customGetEvent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customGetEventClear = (state, action) => update(state, {
  customGetEvent: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const customDeleteEventRequest = (state, action) => update(state, {
  customDeleteEvent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customDeleteEventSuccess = (state, action) => update(state, {
  customDeleteEvent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customDeleteEvent success' }
  }
})

const customDeleteEventError = (state, action) => update(state, {
  customDeleteEvent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customDeleteEventClear = (state, action) => update(state, {
  customDeleteEvent: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


const customGetPartnerRequest = (state, action) => update(state, {
  customGetPartner: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customGetPartnerSuccess = (state, action) => update(state, {
  customGetPartner: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customGetPartner success' }
  }
})

const customGetPartnerError = (state, action) => update(state, {
  customGetPartner: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customGetPartnerClear = (state, action) => update(state, {
  customGetPartner: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


const customGetZoneRequest = (state, action) => update(state, {
  customGetZone: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customGetZoneSuccess = (state, action) => update(state, {
  customGetZone: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customGetZone success' }
  }
})

const customGetZoneError = (state, action) => update(state, {
  customGetZone: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customGetZoneClear = (state, action) => update(state, {
  customGetZone: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const customGetGradeRequest = (state, action) => update(state, {
  customGetGrade: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customGetGradeSuccess = (state, action) => update(state, {
  customGetGrade: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customGetGrade success' }
  }
})

const customGetGradeError = (state, action) => update(state, {
  customGetGrade: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customGetGradeClear = (state, action) => update(state, {
  customGetGrade: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


const customGetStoreCodeRequest = (state, action) => update(state, {
  customGetStoreCode: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customGetStoreCodeSuccess = (state, action) => update(state, {
  customGetStoreCode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customGetStoreCode success' }
  }
})

const customGetStoreCodeError = (state, action) => update(state, {
  customGetStoreCode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customGetStoreCodeClear = (state, action) => update(state, {
  customGetStoreCode: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


const customApplyFilterRequest = (state, action) => update(state, {
  customApplyFilter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customApplyFilterSuccess = (state, action) => update(state, {
  customApplyFilter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customApplyFilter success' }
  }
})

const customApplyFilterError = (state, action) => update(state, {
  customApplyFilter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customApplyFilterClear = (state, action) => update(state, {
  customApplyFilter: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const parameterCustomRequest = (state, action) => update(state, {
  parameterCustom: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const parameterCustomSuccess = (state, action) => update(state, {
  parameterCustom: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'parameterCustom success' }
  }
})

const parameterCustomError = (state, action) => update(state, {
  parameterCustom: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const parameterCustomClear = (state, action) => update(state, {
  parameterCustom: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const allRetailerRequest = (state, action) => update(state, {
  allRetailer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const allRetailerSuccess = (state, action) => update(state, {
  allRetailer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'allRetailer success' }
  }
})

const allRetailerError = (state, action) => update(state, {
  allRetailer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const allRetailerClear = (state, action) => update(state, {
  allRetailer: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

// vendor user
const vendorUserRequest = (state, action) => update(state, {
  vendorUser: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const vendorUserSuccess = (state, action) => update(state, {
  vendorUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'vendorUser success' }
  }
})

const vendorUserError = (state, action) => update(state, {
  vendorUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const vendorUserClear = (state, action) => update(state, {
  vendorUser: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

// check role name availablity
const checkRoleNameRequest = (state, action) => update(state, {
  checkRoleNameData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const checkRoleNameSuccess = (state, action) => update(state, {
  checkRoleNameData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'checkRoleNameData success' }
  }
})

const checkRoleNameError = (state, action) => update(state, {
  checkRoleNameData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const checkRoleNameClear = (state, action) => update(state, {
  checkRoleNameData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

// new role name 
const roleSubmitRequest = (state, action) => update(state, {
  roleSubmitData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const roleSubmitSuccess = (state, action) => update(state, {
  roleSubmitData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'roleSubmitData success' }
  }
})

const roleSubmitError = (state, action) => update(state, {
  roleSubmitData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const roleSubmitClear = (state, action) => update(state, {
  roleSubmitData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


// update role name 
const roleUpdateDataRequest = (state, action) => update(state, {
  roleUpdateData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const roleUpdateDataSuccess = (state, action) => update(state, {
  roleUpdateData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'roleUpdateData success' }
  }
})

const roleUpdateDataError = (state, action) => update(state, {
  roleUpdateData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const roleUpdateDataClear = (state, action) => update(state, {
  roleUpdateData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


// Api logs
const getModuleApiLogsRequest = (state, action) => update(state, {
  getModuleApiLogsData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getModuleApiLogsSuccess = (state, action) => update(state, {
  getModuleApiLogsData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'getModuleApiLogs success' }
  }
})

const getModuleApiLogsError = (state, action) => update(state, {
  getModuleApiLogsData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getModuleApiLogsClear = (state, action) => update(state, {
  getModuleApiLogsData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})



// Api logs
const roleDeleteRequest = (state, action) => update(state, {
  roleDelete: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const roleDeleteSuccess = (state, action) => update(state, {
  roleDelete: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'roleDelete success' }
  }
})

const roleDeleteError = (state, action) => update(state, {
  roleDelete: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const roleDeleteClear = (state, action) => update(state, {
  roleDelete: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const getSubscriptionRequest = (state, action) => update(state, {
  getSubscription: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getSubscriptionSuccess = (state, action) => update(state, {
  getSubscription: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})

const getSubscriptionError = (state, action) => update(state, {
  getSubscription: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getSubscriptionClear = (state, action) => update(state, {
  getSubscription: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const getTransactionRequest = (state, action) => update(state, {
  getTransaction: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getTransactionSuccess = (state, action) => update(state, {
  getTransaction: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})

const getTransactionError = (state, action) => update(state, {
  getTransaction: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getTransactionClear = (state, action) => update(state, {
  getTransaction: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

//-------------------- GET API ACCESS DETAILS --------------------
const getApiAccessDetailsRequest = (state, action) => update(state, {
  getApiAccessDetails: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getApiAccessDetailsSuccess = (state, action) => update(state, {
  getApiAccessDetails: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})

const getApiAccessDetailsError = (state, action) => update(state, {
  getApiAccessDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getApiAccessDetailsClear = (state, action) => update(state, {
  getApiAccessDetails: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


//-------------------- GET EMAIL NOTIFICATION LOG --------------------
const getEmailNotificationLogRequest = (state, action) => update(state, {
  getEmailNotificationLog: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getEmailNotificationLogSuccess = (state, action) => update(state, {
  getEmailNotificationLog: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})

const getEmailNotificationLogError = (state, action) => update(state, {
  getEmailNotificationLog: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getEmailNotificationLogClear = (state, action) => update(state, {
  getEmailNotificationLog: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

//-------------------- RESEND EMAIL NOTIFICATION --------------------
const resendEmailNotificationRequest = (state, action) => update(state, {
  resendEmailNotification: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const resendEmailNotificationSuccess = (state, action) => update(state, {
  resendEmailNotification: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})

const resendEmailNotificationError = (state, action) => update(state, {
  resendEmailNotification: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const resendEmailNotificationClear = (state, action) => update(state, {
  resendEmailNotification: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})


//-------------------- GET EMAIL BODY --------------------
const getEmailBodyRequest = (state, action) => update(state, {
  getEmailBody: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getEmailBodySuccess = (state, action) => update(state, {
  getEmailBody: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})

const getEmailBodyError = (state, action) => update(state, {
  getEmailBody: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getEmailBodyClear = (state, action) => update(state, {
  getEmailBody: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const getUserDataMappingRequest = (state, action) => update(state, {
  getUserDataMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getUserDataMappingSuccess = (state, action) => update(state, {
  getUserDataMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'User Data Mapping success' }
  }
});

const getUserDataMappingError = (state, action) => update(state, {
  getUserDataMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getUserDataMappingClear = (state, action) => update(state, {
  getUserDataMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const updateUserDataMappingRequest = (state, action) => update(state, {
  updateUserDataMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateUserDataMappingSuccess = (state, action) => update(state, {
  updateUserDataMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'User Data Mapping success' }
  }
});

const updateUserDataMappingError = (state, action) => update(state, {
  updateUserDataMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const updateUserDataMappingClear = (state, action) => update(state, {
  updateUserDataMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const getInsertRequest = (state, action) => update(state, {
  getInsertData: {
   isLoading: { $set: true },
   isError: { $set: false },
   isSuccess: { $set: false },
   message: { $set: '' },
  

 }
})
const getInsertSuccess = (state, action) => update(state, {
 getInsertData: {
   data: { $set: action.payload },
   isLoading: { $set: false },
   isError: { $set: false },
   isSuccess: { $set: true },
   message: { $set: 'getInsert success' },
  
 }
})

const getInsertError = (state, action) => update(state, {
 getInsertData: {
   isLoading: { $set: false },
   isSuccess: { $set: false },
   isError: { $set: true },
   message: { $set: action.payload },
   
 }
})
const getInsertClear = (state, action) => update(state, {
 getInsertData: {
   isLoading: { $set: false },
   isError: { $set: false },
   isSuccess: { $set: false },
   message: { $set: '' },
    }
})

const saveInsertRequest = (state, action) => update(state, {
  getInsertData: {
   isLoading: { $set: true },
   isError: { $set: false },
   isSuccess: { $set: false },
   message: { $set: '' },
  

 }
})
const saveInsertSuccess = (state, action) => update(state, {
 getInsertData: {
   data: { $set: action.payload },
   isLoading: { $set: false },
   isError: { $set: false },
   isSuccess: { $set: true },
   message: { $set: 'saveInsert success' },
  
 }
})

const saveInsertError = (state, action) => update(state, {
 getInsertData: {
   isLoading: { $set: false },
   isSuccess: { $set: false },
   isError: { $set: true },
   message: { $set: action.payload },
   
 }
})
const saveInsertClear = (state, action) => update(state, {
 getInsertData: {
   isLoading: { $set: false },
   isError: { $set: false },
   isSuccess: { $set: false },
   message: { $set: '' },
    }
})

const allRetailerCustomerRequest = (state, action) => update(state, {
  allRetailerCustomer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const allRetailerCustomerSuccess = (state, action) => update(state, {
  allRetailerCustomer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'allRetailerCustomer success' }
  }
})
const allRetailerCustomerError = (state, action) => update(state, {
  allRetailerCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const allRetailerCustomerClear = (state, action) => update(state, {
  allRetailerCustomer: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const customerUserRequest = (state, action) => update(state, {
  customerUser: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const customerUserSuccess = (state, action) => update(state, {
  customerUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'customerUser success' }
  }
})
const customerUserError = (state, action) => update(state, {
  customerUser: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const customerUserClear = (state, action) => update(state, {
  customerUser: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const addUserCustomerRequest = (state, action) => update(state, {
  addUserCustomer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addUserCustomerSuccess = (state, action) => update(state, {
  addUserCustomer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'user success' }
  }
});

const addUserCustomerError = (state, action) => update(state, {
  addUserCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const addUserCustomerClear = (state, action) => update(state, {
  addUserCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const editUserCustomerRequest = (state, action) => update(state, {
  editUserCustomer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editUserCustomerSuccess = (state, action) => update(state, {
  editUserCustomer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'user success' }
  }
});

const editUserCustomerError = (state, action) => update(state, {
  editUserCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const editUserCustomerClear = (state, action) => update(state, {
  editUserCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const userStatusCustomerRequest = (state, action) => update(state, {
  userStatusCustomer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const userStatusCustomerSuccess = (state, action) => update(state, {
  userStatusCustomer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'user Status' }
  }
});

const userStatusCustomerError = (state, action) => update(state, {
  userStatusCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const userStatusCustomerClear = (state, action) => update(state, {
  userStatusCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getSubscriptionCustomerRequest = (state, action) => update(state, {
  getSubscriptionCustomer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getSubscriptionCustomerSuccess = (state, action) => update(state, {
  getSubscriptionCustomer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})

const getSubscriptionCustomerError = (state, action) => update(state, {
  getSubscriptionCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getSubscriptionCustomerClear = (state, action) => update(state, {
  getSubscriptionCustomer: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const getTransactionCustomerRequest = (state, action) => update(state, {
  getTransactionCustomer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const getTransactionCustomerSuccess = (state, action) => update(state, {
  getTransactionCustomer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})

const getTransactionCustomerError = (state, action) => update(state, {
  getTransactionCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const getTransactionCustomerClear = (state, action) => update(state, {
  getTransactionCustomer: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

export default handleActions({
  [constants.ORGANIZATION_REQUEST]: organizationRequest,
  [constants.ORGANIZATION_SUCCESS]: organizationSuccess,
  [constants.ORGANIZATION_ERROR]: organizationError,
  [constants.ORGANIZATION_CLEAR]: organizationClear,
  [constants.ADD_ORGANIZATION_REQUEST]: addOrganizationRequest,
  [constants.ADD_ORGANIZATION_SUCCESS]: addOrganizationSuccess,
  [constants.ADD_ORGANIZATION_ERROR]: addOrganizationError,
  [constants.ADD_ORGANIZATION_CLEAR]: addOrganizationClear,
  [constants.EDIT_ORGANIZATION_REQUEST]: editOrganizationRequest,
  [constants.EDIT_ORGANIZATION_SUCCESS]: editOrganizationSuccess,
  [constants.EDIT_ORGANIZATION_ERROR]: editOrganizationError,
  [constants.EDIT_ORGANIZATION_CLEAR]: editOrganizationClear,
  [constants.DELETE_ORGANIZATION_REQUEST]: deleteOrganizationRequest,
  [constants.DELETE_ORGANIZATION_SUCCESS]: deleteOrganizationSuccess,
  [constants.DELETE_ORGANIZATION_ERROR]: deleteOrganizationError,
  [constants.DELETE_ORGANIZATION_CLEAR]: deleteOrganizationClear,
  [constants.SITE_REQUEST]: siteRequest,
  [constants.SITE_SUCCESS]: siteSuccess,
  [constants.SITE_ERROR]: siteError,
  [constants.SITE_CLEAR]: siteClear,
  [constants.ADD_SITE_CLEAR]: addSiteClear,
  [constants.ADD_SITE_REQUEST]: addSiteRequest,
  [constants.ADD_SITE_SUCCESS]: addSiteSuccess,
  [constants.ADD_SITE_ERROR]: addSiteError,
  [constants.EDIT_SITE_CLEAR]: editSiteClear,
  [constants.EDIT_SITE_REQUEST]: editSiteRequest,
  [constants.EDIT_SITE_SUCCESS]: editSiteSuccess,
  [constants.EDIT_SITE_ERROR]: editSiteError,
  [constants.DELETE_SITE_CLEAR]: deleteSiteClear,
  [constants.DELETE_SITE_REQUEST]: deleteSiteRequest,
  [constants.DELETE_SITE_SUCCESS]: deleteSiteSuccess,
  [constants.DELETE_SITE_ERROR]: deleteSiteError,

  [constants.USER_REQUEST]: userRequest,
  [constants.USER_SUCCESS]: userSuccess,
  [constants.USER_ERROR]: userError,

  [constants.ADD_USER_REQUEST]: addUserRequest,
  [constants.ADD_USER_SUCCESS]: addUserSuccess,
  [constants.ADD_USER_ERROR]: addUserError,
  [constants.ADD_USER_CLEAR]: addUserClear,

  [constants.EDIT_USER_REQUEST]: editUserRequest,
  [constants.EDIT_USER_SUCCESS]: editUserSuccess,
  [constants.EDIT_USER_ERROR]: editUserError,
  [constants.EDIT_USER_CLEAR]: editUserClear,

  [constants.DELETE_USER_REQUEST]: deleteUserRequest,
  [constants.DELETE_USER_SUCCESS]: deleteUserSuccess,
  [constants.DELETE_USER_ERROR]: deleteUserError,
  [constants.DELETE_USER_CLEAR]: deleteUserClear,

  [constants.USER_STATUS_REQUEST]: userStatusRequest,
  [constants.USER_STATUS_ERROR]: userStatusError,
  [constants.USER_STATUS_SUCCESS]: userStatusSuccess,
  [constants.USER_STATUS_CLEAR]: userStatusClear,

  [constants.ROLES_REQUEST]: rolesRequest,
  [constants.ROLES_SUCCESS]: rolesSuccess,
  [constants.ROLES_ERROR]: rolesError,
  [constants.ADD_ROLES_REQUEST]: addRolesRequest,
  [constants.ADD_ROLES_CLEAR]: addRolesClear,
  [constants.ADD_ROLES_SUCCESS]: addRolesSuccess,
  [constants.ADD_ROLES_ERROR]: addRolesError,
  [constants.EDIT_ROLES_REQUEST]: editRolesRequest,
  [constants.EDIT_ROLES_CLEAR]: editRolesClear,
  [constants.EDIT_ROLES_SUCCESS]: editRolesSuccess,
  [constants.EDIT_ROLES_ERROR]: editRolesError,
  [constants.DELETE_ROLES_REQUEST]: deleteRolesRequest,
  [constants.DELETE_ROLES_SUCCESS]: deleteRolesSuccess,
  [constants.DELETE_ROLES_ERROR]: deleteRolesError,
  [constants.SITE_MAPPING_REQUEST]: siteMappingRequest,
  [constants.SITE_MAPPING_SUCCESS]: siteMappingSuccess,
  [constants.SITE_MAPPING_ERROR]: siteMappingError,
  [constants.ADD_SITE_MAPPING_CLEAR]: addSiteMappingClear,
  [constants.ADD_SITE_MAPPING_REQUEST]: addSiteMappingRequest,
  [constants.ADD_SITE_MAPPING_SUCCESS]: addSiteMappingSuccess,
  [constants.ADD_SITE_MAPPING_ERROR]: addSiteMappingError,
  [constants.EDIT_SITE_MAPPING_CLEAR]: editSiteMappingClear,
  [constants.EDIT_SITE_MAPPING_REQUEST]: editSiteMappingRequest,
  [constants.EDIT_SITE_MAPPING_SUCCESS]: editSiteMappingSuccess,
  [constants.EDIT_SITE_MAPPING_ERROR]: editSiteMappingError,
  [constants.DELETE_SITE_MAPPING_CLEAR]: deleteSiteMappingClear,
  [constants.DELETE_SITE_MAPPING_REQUEST]: deleteSiteMappingRequest,
  [constants.DELETE_SITE_MAPPING_SUCCESS]: deleteSiteMappingSuccess,
  [constants.DELETE_SITE_MAPPING_ERROR]: deleteSiteMappingError,
  [constants.FROM_TO_REQUEST]: fromToRequest,
  [constants.FROM_TO_SUCCESS]: fromToSuccess,
  [constants.FROM_TO_ERROR]: fromToError,
  [constants.ALL_ROLE_REQUEST]: allRoleRequest,
  [constants.ALL_ROLE_SUCCESS]: allRoleSuccess,
  [constants.ALL_ROLE_ERROR]: allRoleError,
  [constants.ALL_ROLE_CLEAR]: allRoleClear,
  [constants.ALL_USER_REQUEST]: allUserRequest,
  [constants.ALL_USER_SUCCESS]: allUserSuccess,
  [constants.ALL_USER_ERROR]: allUserError,
  [constants.SKETCHERS_UPLOAD_REQUEST]: sketchersUploadRequest,
  [constants.SKETCHERS_UPLOAD_SUCCESS]: sketchersUploadSuccess,
  [constants.SKETCHERS_UPLOAD_ERROR]: sketchersUploadError,
  [constants.SAVE_SKETCHERS_REQUEST]: saveSketchersRequest,
  [constants.SAVE_SKETCHERS_SUCCESS]: saveSketchersSuccess,
  [constants.SAVE_SKETCHERS_ERROR]: saveSketchersError,
  [constants.SAVE_SKETCHERS_CLEAR]: saveSketchersClear,

  [constants.GET_TEMPLATE_REQUEST]: getTemplateRequest,
  [constants.GET_TEMPLATE_SUCCESS]: getTemplateSuccess,
  [constants.GET_TEMPLATE_ERROR]: getTemplateError,
  [constants.GET_TEMPLATE_CLEAR]: getTemplateClear,

  [constants.DOWNLOAD_REQUEST]: downloadRequest,
  [constants.DOWNLOAD_SUCCESS]: downloadSuccess,
  [constants.DOWNLOAD_ERROR]: downloadError,
  [constants.DOWNLOAD_CLEAR]: downloadClear,

  [constants.DATA_SYNC_REQUEST]: dataSyncRequest,
  [constants.DATA_SYNC_SUCCESS]: dataSyncSuccess,
  [constants.DATA_SYNC_ERROR]: dataSyncError,
  [constants.DATA_SYNC_CLEAR]: dataSyncClear,


  [constants.PARAMSETTING_REQUEST]: paramsettingRequest,
  [constants.PARAMSETTING_SUCCESS]: paramsettingSuccess,
  [constants.PARAMSETTING_ERROR]: paramsettingError,
  [constants.PARAMSETTING_CLEAR]: paramsettingClear,

  //______________________________ FESTIVAL SETTING _______________________________

  [constants.CREATE_FESTIVAL_REQUEST]: createFestivalRequest,
  [constants.CREATE_FESTIVAL_SUCCESS]: createFestivalSuccess,
  [constants.CREATE_FESTIVAL_ERROR]: createFestivalError,
  [constants.CREATE_FESTIVAL_CLEAR]: createFestivalClear,

  [constants.ALL_FESTIVAL_REQUEST]: allFestivalRequest,
  [constants.ALL_FESTIVAL_SUCCESS]: allFestivalSuccess,
  [constants.ALL_FESTIVAL_ERROR]: allFestivalError,
  [constants.ALL_FESTIVAL_CLEAR]: allFestivalClear,

  [constants.PROMOTIONAL_REQUEST]: promotionalRequest,
  [constants.PROMOTIONAL_SUCCESS]: promotionalSuccess,
  [constants.PROMOTIONAL_ERROR]: promotionalError,
  [constants.PROMOTIONAL_CLEAR]: promotionalClear,

  [constants.GET_PROMOTIONAL_EVENT_REQUEST]: getPromotionalEventRequest,
  [constants.GET_PROMOTIONAL_EVENT_SUCCESS]: getPromotionalEventSuccess,
  [constants.GET_PROMOTIONAL_EVENT_ERROR]: getPromotionalEventError,
  [constants.GET_PROMOTIONAL_EVENT_CLEAR]: getPromotionalEventClear,

  [constants.UPDATE_PROMOTIONAL_EVENT_REQUEST]: updatePromotionalEventRequest,
  [constants.UPDATE_PROMOTIONAL_EVENT_SUCCESS]: updatePromotionalEventSuccess,
  [constants.UPDATE_PROMOTIONAL_EVENT_ERROR]: updatePromotionalEventError,
  [constants.UPDATE_PROMOTIONAL_EVENT_CLEAR]: updatePromotionalEventClear,

  [constants.DISABLE_PROMOTIONAL_EVENT_REQUEST]: disablePromotionalEventRequest,
  [constants.DISABLE_PROMOTIONAL_EVENT_SUCCESS]: disablePromotionalEventSuccess,
  [constants.DISABLE_PROMOTIONAL_EVENT_ERROR]: disablePromotionalEventError,
  [constants.DISABLE_PROMOTIONAL_EVENT_CLEAR]: disablePromotionalEventClear,

  [constants.DEACTIVE_PROMOTIONAL_EVENT_REQUEST]: deactivePromotionalEventRequest,
  [constants.DEACTIVE_PROMOTIONAL_EVENT_SUCCESS]: deactivePromotionalEventSuccess,
  [constants.DEACTIVE_PROMOTIONAL_EVENT_ERROR]: deactivePromotionalEventError,
  [constants.DEACTIVE_PROMOTIONAL_EVENT_CLEAR]: deactivePromotionalEventClear,
  //role master


  [constants.UPDATE_ROLE_REQUEST]: updateRoleRequest,
  [constants.UPDATE_ROLE_SUCCESS]: updateRoleSuccess,
  [constants.UPDATE_ROLE_ERROR]: updateRoleError,
  [constants.UPDATE_ROLE_CLEAR]: updateRoleClear,

  [constants.MODULE_GET_DATA_REQUEST]: moduleGetDataRequest,
  [constants.MODULE_GET_DATA_SUCCESS]: moduleGetDataSuccess,
  [constants.MODULE_GET_DATA_ERROR]: moduleGetDataError,
  [constants.MODULE_GET_DATA_CLEAR]: moduleGetDataClear,

  [constants.MODULE_GETROLE_REQUEST]: moduleGetRoleRequest,
  [constants.MODULE_GETROLE_SUCCESS]: moduleGetRoleSuccess,
  [constants.MODULE_GETROLE_ERROR]: moduleGetRoleError,
  [constants.MODULE_GETROLE_CLEAR]: moduleGetRoleClear,

  [constants.GET_SUB_MODULE_DATA_REQUEST]: getSubModuleDataRequest,
  [constants.GET_SUB_MODULE_DATA_SUCCESS]: getSubModuleDataSuccess,
  [constants.GET_SUB_MODULE_DATA_ERROR]: getSubModuleDataError,
  [constants.GET_SUB_MODULE_DATA_CLEAR]: getSubModuleDataClear,

  [constants.PROXY_STORE_REQUEST]: proxyStoreRequest,
  [constants.PROXY_STORE_SUCCESS]: proxyStoreSuccess,
  [constants.PROXY_STORE_ERROR]: proxyStoreError,
  [constants.PROXY_STORE_CLEAR]: proxyStoreClear,

  [constants.GET_SUB_MODULE_BY_ROLENAME_REQUEST]: getSubModuleByRoleNameRequest,
  [constants.GET_SUB_MODULE_BY_ROLENAME_SUCCESS]: getSubModuleByRoleNameSuccess,
  [constants.GET_SUB_MODULE_BY_ROLENAME_ERROR]: getSubModuleByRoleNameError,
  [constants.GET_SUB_MODULE_BY_ROLENAME_CLEAR]: getSubModuleByRoleNameClear,



  [constants.PARAMETER_CUSTOM_REQUEST]: parameterCustomRequest,
  [constants.PARAMETER_CUSTOM_SUCCESS]: parameterCustomSuccess,
  [constants.PARAMETER_CUSTOM_ERROR]: parameterCustomError,
  [constants.PARAMETER_CUSTOM_CLEAR]: parameterCustomClear,

  [constants.CUSTOM_MASTER_SKECHERS_REQUEST]: customMasterSkechersRequest,
  [constants.CUSTOM_MASTER_SKECHERS_SUCCESS]: customMasterSkechersSuccess,
  [constants.CUSTOM_MASTER_SKECHERS_ERROR]: customMasterSkechersError,
  [constants.CUSTOM_MASTER_SKECHERS_CLEAR]: customMasterSkechersClear,

  [constants.CUSTOM_GET_EVENT_REQUEST]: customGetEventRequest,
  [constants.CUSTOM_GET_EVENT_SUCCESS]: customGetEventSuccess,
  [constants.CUSTOM_GET_EVENT_ERROR]: customGetEventError,
  [constants.CUSTOM_GET_EVENT_CLEAR]: customGetEventClear,


  [constants.CUSTOM_DELETE_EVENT_REQUEST]: customDeleteEventRequest,
  [constants.CUSTOM_DELETE_EVENT_SUCCESS]: customDeleteEventSuccess,
  [constants.CUSTOM_DELETE_EVENT_ERROR]: customDeleteEventError,
  [constants.CUSTOM_DELETE_EVENT_CLEAR]: customDeleteEventClear,

  [constants.CUSTOM_GET_PARTNER_REQUEST]: customGetPartnerRequest,
  [constants.CUSTOM_GET_PARTNER_SUCCESS]: customGetPartnerSuccess,
  [constants.CUSTOM_GET_PARTNER_ERROR]: customGetPartnerError,
  [constants.CUSTOM_GET_PARTNER_CLEAR]: customGetPartnerClear,

  [constants.CUSTOM_GET_ZONE_REQUEST]: customGetZoneRequest,
  [constants.CUSTOM_GET_ZONE_SUCCESS]: customGetZoneSuccess,
  [constants.CUSTOM_GET_ZONE_ERROR]: customGetZoneError,
  [constants.CUSTOM_GET_ZONE_CLEAR]: customGetZoneClear,

  [constants.CUSTOM_GET_GRADE_REQUEST]: customGetGradeRequest,
  [constants.CUSTOM_GET_GRADE_SUCCESS]: customGetGradeSuccess,
  [constants.CUSTOM_GET_GRADE_ERROR]: customGetGradeError,
  [constants.CUSTOM_GET_GRADE_CLEAR]: customGetGradeClear,

  [constants.CUSTOM_GET_STORECODE_REQUEST]: customGetStoreCodeRequest,
  [constants.CUSTOM_GET_STORECODE_SUCCESS]: customGetStoreCodeSuccess,
  [constants.CUSTOM_GET_STORECODE_ERROR]: customGetStoreCodeError,
  [constants.CUSTOM_GET_STORECODE_CLEAR]: customGetStoreCodeClear,

  [constants.CUSTOM_APPLYFILTER_REQUEST]: customApplyFilterRequest,
  [constants.CUSTOM_APPLYFILTER_SUCCESS]: customApplyFilterSuccess,
  [constants.CUSTOM_APPLYFILTER_ERROR]: customApplyFilterError,
  [constants.CUSTOM_APPLYFILTER_CLEAR]: customApplyFilterClear,

  [constants.ALL_RETAILER_REQUEST]: allRetailerRequest,
  [constants.ALL_RETAILER_SUCCESS]: allRetailerSuccess,
  [constants.ALL_RETAILER_ERROR]: allRetailerError,
  [constants.ALL_RETAILER_CLEAR]: allRetailerClear,

  [constants.VENDOR_USER_REQUEST]: vendorUserRequest,
  [constants.VENDOR_USER_SUCCESS]: vendorUserSuccess,
  [constants.VENDOR_USER_ERROR]: vendorUserError,
  [constants.VENDOR_USER_CLEAR]: vendorUserClear,

  //check role name 
  [constants.CHECK_ROLE_NAME_REQUEST]: checkRoleNameRequest,
  [constants.CHECK_ROLE_NAME_SUCCESS]: checkRoleNameSuccess,
  [constants.CHECK_ROLE_NAME_ERROR]: checkRoleNameError,
  [constants.CHECK_ROLE_NAME_CLEAR]: checkRoleNameClear,

  //submit role  
  [constants.SUBMIT_ROLE_REQUEST]: roleSubmitRequest,
  [constants.SUBMIT_ROLE_SUCCESS]: roleSubmitSuccess,
  [constants.SUBMIT_ROLE_ERROR]: roleSubmitError,
  [constants.SUBMIT_ROLE_CLEAR]: roleSubmitClear,

  //check role  
  [constants.UPDATE_ROLE_DATA_REQUEST]: roleUpdateDataRequest,
  [constants.UPDATE_ROLE_DATA_SUCCESS]: roleUpdateDataSuccess,
  [constants.UPDATE_ROLE_DATA_ERROR]: roleUpdateDataError,
  [constants.UPDATE_ROLE_DATA_CLEAR]: roleUpdateDataClear,

  //check role  
  [constants.GET_MODULE_API_LOGS_REQUEST]: getModuleApiLogsRequest,
  [constants.GET_MODULE_API_LOGS_SUCCESS]: getModuleApiLogsSuccess,
  [constants.GET_MODULE_API_LOGS_ERROR]: getModuleApiLogsError,
  [constants.GET_MODULE_API_LOGS_CLEAR]: getModuleApiLogsClear,
  //check role  
  [constants.ROLE_DELETE_REQUEST]: roleDeleteRequest,
  [constants.ROLE_DELETE_SUCCESS]: roleDeleteSuccess,
  [constants.ROLE_DELETE_ERROR]: roleDeleteError,
  [constants.ROLE_DELETE_CLEAR]: roleDeleteClear,

  [constants.GET_SUBSCRIPTION_REQUEST]: getSubscriptionRequest,
  [constants.GET_SUBSCRIPTION_SUCCESS]: getSubscriptionSuccess,
  [constants.GET_SUBSCRIPTION_ERROR]: getSubscriptionError,
  [constants.GET_SUBSCRIPTION_CLEAR]: getSubscriptionClear,

  [constants.GET_TRANSACTION_REQUEST]: getTransactionRequest,
  [constants.GET_TRANSACTION_SUCCESS]: getTransactionSuccess,
  [constants.GET_TRANSACTION_ERROR]: getTransactionError,
  [constants.GET_TRANSACTION_CLEAR]: getTransactionClear,

  //-------------------- GET API ACCESS DETAILS --------------------
  [constants.GET_API_ACCESS_DETAILS_REQUEST]: getApiAccessDetailsRequest,
  [constants.GET_API_ACCESS_DETAILS_SUCCESS]: getApiAccessDetailsSuccess,
  [constants.GET_API_ACCESS_DETAILS_ERROR]: getApiAccessDetailsError,
  [constants.GET_API_ACCESS_DETAILS_CLEAR]: getApiAccessDetailsClear,

  //-------------------- GET EMAIL NOTIFICATION LOG --------------------
  [constants.GET_EMAIL_NOTIFICATION_LOG_REQUEST]: getEmailNotificationLogRequest,
  [constants.GET_EMAIL_NOTIFICATION_LOG_SUCCESS]: getEmailNotificationLogSuccess,
  [constants.GET_EMAIL_NOTIFICATION_LOG_ERROR]: getEmailNotificationLogError,
  [constants.GET_EMAIL_NOTIFICATION_LOG_CLEAR]: getEmailNotificationLogClear,

  //-------------------- RESEND EMAIL NOTIFICATION --------------------
  [constants.RESEND_EMAIL_NOTIFICATION_REQUEST]: resendEmailNotificationRequest,
  [constants.RESEND_EMAIL_NOTIFICATION_SUCCESS]: resendEmailNotificationSuccess,
  [constants.RESEND_EMAIL_NOTIFICATION_ERROR]: resendEmailNotificationError,
  [constants.RESEND_EMAIL_NOTIFICATION_CLEAR]: resendEmailNotificationClear,

  //-------------------- GET EMAIL BODY --------------------
  [constants.GET_EMAIL_BODY_REQUEST]: getEmailBodyRequest,
  [constants.GET_EMAIL_BODY_SUCCESS]: getEmailBodySuccess,
  [constants.GET_EMAIL_BODY_ERROR]: getEmailBodyError,
  [constants.GET_EMAIL_BODY_CLEAR]: getEmailBodyClear,

  [constants.GET_USER_DATA_MAPPING_REQUEST]: getUserDataMappingRequest,
  [constants.GET_USER_DATA_MAPPING_SUCCESS]: getUserDataMappingSuccess,
  [constants.GET_USER_DATA_MAPPING_ERROR]: getUserDataMappingError,
  [constants.GET_USER_DATA_MAPPING_CLEAR]: getUserDataMappingClear,

  [constants.UPDATE_USER_DATA_MAPPING_REQUEST]: updateUserDataMappingRequest,
  [constants.UPDATE_USER_DATA_MAPPING_SUCCESS]: updateUserDataMappingSuccess,
  [constants.UPDATE_USER_DATA_MAPPING_ERROR]: updateUserDataMappingError,
  [constants.UPDATE_USER_DATA_MAPPING_CLEAR]: updateUserDataMappingClear,




  //-----------------Insert----------------------
  [constants.GET_INSERT_REQUEST]: getInsertRequest,
  [constants.GET_INSERT_SUCCESS]: getInsertSuccess,
  [constants.GET_INSERT_ERROR]: getInsertError,
  [constants.GET_INSERT_CLEAR]: getInsertClear,

  [constants.SAVE_INSERT_REQUEST]: saveInsertRequest,
  [constants.SAVE_INSERT_SUCCESS]: saveInsertSuccess,
  [constants.SAVE_INSERT_ERROR]: saveInsertError,
  [constants.SAVE_INSERT_CLEAR]: saveInsertClear,

  [constants.ALL_RETAILER_CUSTOMER_REQUEST]: allRetailerCustomerRequest,
  [constants.ALL_RETAILER_CUSTOMER_SUCCESS]: allRetailerCustomerSuccess,
  [constants.ALL_RETAILER_CUSTOMER_ERROR]: allRetailerCustomerError,
  [constants.ALL_RETAILER_CUSTOMER_CLEAR]: allRetailerCustomerClear,

  [constants.CUSTOMER_USER_REQUEST]: customerUserRequest,
  [constants.CUSTOMER_USER_SUCCESS]: customerUserSuccess,
  [constants.CUSTOMER_USER_ERROR]: customerUserError,
  [constants.CUSTOMER_USER_CLEAR]: customerUserClear,

  [constants.ADD_USER_CUSTOMER_REQUEST]: addUserCustomerRequest,
  [constants.ADD_USER_CUSTOMER_SUCCESS]: addUserCustomerSuccess,
  [constants.ADD_USER_CUSTOMER_ERROR]: addUserCustomerError,
  [constants.ADD_USER_CUSTOMER_CLEAR]: addUserCustomerClear,

  [constants.EDIT_USER_CUSTOMER_REQUEST]: editUserCustomerRequest,
  [constants.EDIT_USER_CUSTOMER_SUCCESS]: editUserCustomerSuccess,
  [constants.EDIT_USER_CUSTOMER_ERROR]: editUserCustomerError,
  [constants.EDIT_USER_CUSTOMER_CLEAR]: editUserCustomerClear,

  [constants.USER_STATUS_CUSTOMER_REQUEST]: userStatusCustomerRequest,
  [constants.USER_STATUS_CUSTOMER_ERROR]: userStatusCustomerError,
  [constants.USER_STATUS_CUSTOMER_SUCCESS]: userStatusCustomerSuccess,
  [constants.USER_STATUS_CUSTOMER_CLEAR]: userStatusCustomerClear,

  [constants.GET_SUBSCRIPTION_CUSTOMER_REQUEST]: getSubscriptionCustomerRequest,
  [constants.GET_SUBSCRIPTION_CUSTOMER_SUCCESS]: getSubscriptionCustomerSuccess,
  [constants.GET_SUBSCRIPTION_CUSTOMER_ERROR]: getSubscriptionCustomerError,
  [constants.GET_SUBSCRIPTION_CUSTOMER_CLEAR]: getSubscriptionCustomerClear,

  [constants.GET_TRANSACTION_CUSTOMER_REQUEST]: getTransactionCustomerRequest,
  [constants.GET_TRANSACTION_CUSTOMER_SUCCESS]: getTransactionCustomerSuccess,
  [constants.GET_TRANSACTION_CUSTOMER_ERROR]: getTransactionCustomerError,
  [constants.GET_TRANSACTION_CUSTOMER_CLEAR]: getTransactionCustomerClear,

}, initialState);
