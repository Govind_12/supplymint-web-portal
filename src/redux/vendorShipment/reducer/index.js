import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
    getShipment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    shipmentConfirmCancel: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getCompleteDetailShipment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    // _______________________________VENDOR SHIPMENT REDUCERS_______________________________

    getAllShipmentVendor: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    updateVendor: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getShipmentDetails: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getAllComment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    qcAddComment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    uploadComment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    deleteUploads: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    updateVendorShipment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    updateInvoice: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    // Add multiple Comment QC
    addMultipleCommentQc: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    // get user emails
    getUserEmails: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    }
}

const getShipmentRequest = (state, action) => update(state, {
    getShipment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getShipmentError = (state, action) => update(state, {
    getShipment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getShipmentSuccess = (state, action) => update(state, {
    getShipment: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getShipmentClear = (state, action) => update(state, {
    getShipment: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// ______________________________________

const shipmentConfirmCancelRequest = (state, action) => update(state, {
    shipmentConfirmCancel: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const shipmentConfirmCancelError = (state, action) => update(state, {
    shipmentConfirmCancel: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const shipmentConfirmCancelSuccess = (state, action) => update(state, {
    shipmentConfirmCancel: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const shipmentConfirmCancelClear = (state, action) => update(state, {
    shipmentConfirmCancel: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// ___________________________

const getCompleteDetailShipmentRequest = (state, action) => update(state, {
    getCompleteDetailShipment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getCompleteDetailShipmentError = (state, action) => update(state, {
    getCompleteDetailShipment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getCompleteDetailShipmentSuccess = (state, action) => update(state, {
    getCompleteDetailShipment: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getCompleteDetailShipmentClear = (state, action) => update(state, {
    getCompleteDetailShipment: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// __________________________________ VENDOR SHIPMENT ________________________________

const getAllShipmentVendorRequest = (state, action) => update(state, {
    getAllShipmentVendor: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllShipmentVendorError = (state, action) => update(state, {
    getAllShipmentVendor: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllShipmentVendorSuccess = (state, action) => update(state, {
    getAllShipmentVendor: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllShipmentVendorClear = (state, action) => update(state, {
    getAllShipmentVendor: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const updateVendorRequest = (state, action) => update(state, {
    updateVendor: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateVendorError = (state, action) => update(state, {
    updateVendor: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const updateVendorSuccess = (state, action) => update(state, {
    updateVendor: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const updateVendorClear = (state, action) => update(state, {
    updateVendor: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const getShipmentDetailsRequest = (state, action) => update(state, {
    getShipmentDetails: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getShipmentDetailsError = (state, action) => update(state, {
    getShipmentDetails: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getShipmentDetailsSuccess = (state, action) => update(state, {
    getShipmentDetails: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getShipmentDetailsClear = (state, action) => update(state, {
    getShipmentDetails: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
// ______________________________-GET ALL COMMENTS__________________________

const getAllCommentRequest = (state, action) => update(state, {
    getAllComment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllCommentError = (state, action) => update(state, {
    getAllComment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllCommentSuccess = (state, action) => update(state, {
    getAllComment: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllCommentClear = (state, action) => update(state, {
    getAllComment: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const qcAddCommentRequest = (state, action) => update(state, {
    qcAddComment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const qcAddCommentError = (state, action) => update(state, {
    qcAddComment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const qcAddCommentSuccess = (state, action) => update(state, {
    qcAddComment: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const qcAddCommentClear = (state, action) => update(state, {
    qcAddComment: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const uploadCommentRequest = (state, action) => update(state, {
    uploadComment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const uploadCommentError = (state, action) => update(state, {
    uploadComment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const uploadCommentSuccess = (state, action) => update(state, {
    uploadComment: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const uploadCommentClear = (state, action) => update(state, {
    uploadComment: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const deleteUploadsRequest = (state, action) => update(state, {
    deleteUploads: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const deleteUploadsError = (state, action) => update(state, {
    deleteUploads: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const deleteUploadsSuccess = (state, action) => update(state, {
    deleteUploads: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const deleteUploadsClear = (state, action) => update(state, {
    deleteUploads: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const updateVendorShipmentRequest = (state, action) => update(state, {
    updateVendorShipment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateVendorShipmentError = (state, action) => update(state, {
    updateVendorShipment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const updateVendorShipmentSuccess = (state, action) => update(state, {
    updateVendorShipment: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const updateVendorShipmentClear = (state, action) => update(state, {
    updateVendorShipment: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const updateInvoiceRequest = (state, action) => update(state, {
    updateInvoice: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateInvoiceError = (state, action) => update(state, {
    updateInvoice: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const updateInvoiceSuccess = (state, action) => update(state, {
    updateInvoice: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const updateInvoiceClear = (state, action) => update(state, {
    updateInvoice: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//--------Add multiple Comment QC -------------------------
const addMultipleCommentQcRequest = (state, action) => update(state, {
    addMultipleCommentQc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const addMultipleCommentQcError = (state, action) => update(state, {
    addMultipleCommentQc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const addMultipleCommentQcSuccess = (state, action) => update(state, {
    addMultipleCommentQc: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const addMultipleCommentQcClear = (state, action) => update(state, {
    addMultipleCommentQc: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
//----------------------------------------------------------


//--------Add multiple Comment QC -------------------------
const getUserEmailsRequest = (state, action) => update(state, {
    getUserEmails: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getUserEmailsError = (state, action) => update(state, {
    getUserEmails: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getUserEmailsSuccess = (state, action) => update(state, {
    getUserEmails: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getUserEmailsClear = (state, action) => update(state, {
    getUserEmails: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
//----------------------------------------------------------------------------

export default handleActions({

    [constants.GET_SHIPMENT_REQUEST]: getShipmentRequest,
    [constants.GET_SHIPMENT_SUCCESS]: getShipmentSuccess,
    [constants.GET_SHIPMENT_CLEAR]: getShipmentClear,
    [constants.GET_SHIPMENT_ERROR]: getShipmentError,

    [constants.SHIPMENT_CONFIRM_CANCEL_REQUEST]: shipmentConfirmCancelRequest,
    [constants.SHIPMENT_CONFIRM_CANCEL_SUCCESS]: shipmentConfirmCancelSuccess,
    [constants.SHIPMENT_CONFIRM_CANCEL_CLEAR]: shipmentConfirmCancelClear,
    [constants.SHIPMENT_CONFIRM_CANCEL_ERROR]: shipmentConfirmCancelError,

    [constants.GET_COMPLETE_DETAIL_SHIPMENT_REQUEST]: getCompleteDetailShipmentRequest,
    [constants.GET_COMPLETE_DETAIL_SHIPMENT_SUCCESS]: getCompleteDetailShipmentSuccess,
    [constants.GET_COMPLETE_DETAIL_SHIPMENT_CLEAR]: getCompleteDetailShipmentClear,
    [constants.GET_COMPLETE_DETAIL_SHIPMENT_ERROR]: getCompleteDetailShipmentError,

    // _________________________________VENDOR SHIPMENT __________________________________

    [constants.GET_ALL_SHIPMENT_VENDOR_REQUEST]: getAllShipmentVendorRequest,
    [constants.GET_ALL_SHIPMENT_VENDOR_SUCCESS]: getAllShipmentVendorSuccess,
    [constants.GET_ALL_SHIPMENT_VENDOR_CLEAR]: getAllShipmentVendorClear,
    [constants.GET_ALL_SHIPMENT_VENDOR_ERROR]: getAllShipmentVendorError,

    [constants.UPDATE_VENDOR_REQUEST]: updateVendorRequest,
    [constants.UPDATE_VENDOR_SUCCESS]: updateVendorSuccess,
    [constants.UPDATE_VENDOR_CLEAR]: updateVendorClear,
    [constants.UPDATE_VENDOR_ERROR]: updateVendorError,

    [constants.GET_SHIPMENT_DETAILS_REQUEST]: getShipmentDetailsRequest,
    [constants.GET_SHIPMENT_DETAILS_SUCCESS]: getShipmentDetailsSuccess,
    [constants.GET_SHIPMENT_DETAILS_CLEAR]: getShipmentDetailsClear,
    [constants.GET_SHIPMENT_DETAILS_ERROR]: getShipmentDetailsError,

    [constants.GET_ALL_COMMENT_REQUEST]: getAllCommentRequest,
    [constants.GET_ALL_COMMENT_SUCCESS]: getAllCommentSuccess,
    [constants.GET_ALL_COMMENT_CLEAR]: getAllCommentClear,
    [constants.GET_ALL_COMMENT_ERROR]: getAllCommentError,


    [constants.QC_ADD_COMMENT_REQUEST]: qcAddCommentRequest,
    [constants.QC_ADD_COMMENT_SUCCESS]: qcAddCommentSuccess,
    [constants.QC_ADD_COMMENT_CLEAR]: qcAddCommentClear,
    [constants.QC_ADD_COMMENT_ERROR]: qcAddCommentError,

    [constants.UPLOAD_COMMENT_REQUEST]: uploadCommentRequest,
    [constants.UPLOAD_COMMENT_SUCCESS]: uploadCommentSuccess,
    [constants.UPLOAD_COMMENT_CLEAR]: uploadCommentClear,
    [constants.UPLOAD_COMMENT_ERROR]: uploadCommentError,

    [constants.DELETE_UPLOADS_REQUEST]: deleteUploadsRequest,
    [constants.DELETE_UPLOADS_SUCCESS]: deleteUploadsSuccess,
    [constants.DELETE_UPLOADS_CLEAR]: deleteUploadsClear,
    [constants.DELETE_UPLOADS_ERROR]: deleteUploadsError,

    [constants.UPDATE_VENDOR_SHIPMENT_REQUEST]: updateVendorShipmentRequest,
    [constants.UPDATE_VENDOR_SHIPMENT_SUCCESS]: updateVendorShipmentSuccess,
    [constants.UPDATE_VENDOR_SHIPMENT_CLEAR]: updateVendorShipmentClear,
    [constants.UPDATE_VENDOR_SHIPMENT_ERROR]: updateVendorShipmentError,

    [constants.UPDATE_INVOICE_REQUEST]: updateInvoiceRequest,
    [constants.UPDATE_INVOICE_SUCCESS]: updateInvoiceSuccess,
    [constants.UPDATE_INVOICE_CLEAR]: updateInvoiceClear,
    [constants.UPDATE_INVOICE_ERROR]: updateInvoiceError,

    [constants.ADD_MULTIPLE_COMMENT_QC_REQUEST]: addMultipleCommentQcRequest,
    [constants.ADD_MULTIPLE_COMMENT_QC_SUCCESS]: addMultipleCommentQcSuccess,
    [constants.ADD_MULTIPLE_COMMENT_QC_CLEAR]: addMultipleCommentQcClear,
    [constants.ADD_MULTIPLE_COMMENT_QC_ERROR]: addMultipleCommentQcError,

    [constants.GET_USER_EMAILS_REQUEST]: getUserEmailsRequest,
    [constants.GET_USER_EMAILS_SUCCESS]: getUserEmailsSuccess,
    [constants.GET_USER_EMAILS_CLEAR]: getUserEmailsClear,
    [constants.GET_USER_EMAILS_ERROR]: getUserEmailsError,

}, initialState)