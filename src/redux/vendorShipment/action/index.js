import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import { OganisationIdName } from '../../../organisationIdName.js';
import moment from 'moment';
export function* getShipmentRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getShipmentClear())
    } else {
        try {
            //let vendorCode = sessionStorage.getItem('vendorCode') == undefined ? "" : sessionStorage.getItem('vendorCode');
    
            let no = action.payload.no == undefined ? 1 : action.payload.no;
            let type = action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let status = action.payload.status;
            let isDashboardComment = action.payload.isDashboardComment;
            let isReportPage = action.payload.isReportPage == undefined ? 0 : action.payload.isReportPage;

            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy;
            let sortedIn = action.payload.sortedIn == undefined ? "" : action.payload.sortedIn;
            var get= {
                    pageNo: no,
                    type: type,
                    search: search,
                    status: status,
                    sortedBy,
                    sortedIn,
                    isDashboardComment,
                    isReportPage
                }
           
            let filter = {};
            // if( action.payload.filter != undefined && action.payload.filter.vendorCode != undefined ){
                filter = { ...action.payload.filter };
            // } else {
            //     filter = {...action.payload.filter, vendorCode };
            // }   
            let final = { ...get, filter };
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/entship/find/all`, final
            );
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getShipmentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getShipmentError(response.data));
            }

        } catch (e) {
            yield put(actions.getShipmentError("error occurs"));
            console.warn('Some error found in "getShipmentRequest" action\n', e);
        }
    }
}
// ____________________________CONFIRMED AND CANCEL SHIPMENT_______________________

export function* shipmentConfirmCancelRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.shipmentConfirmCancelClear())
    } else {
        try {
            let shipmentCriteria = action.payload.shipmentCriteria;
            let shipmentStatusList = action.payload.shipmentStatusList;
            let isLRProcessingASN = action.payload.isLRProcessingASN == undefined ? 0 : 1;

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/entship/update/sr`, {
                'shipmentStatusList': shipmentStatusList,
                'shipmentCriteria': shipmentCriteria,
                'isLRProcessingASN': isLRProcessingASN
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.shipmentConfirmCancelSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.shipmentConfirmCancelError(response.data));
            }

        } catch (e) {
            yield put(actions.shipmentConfirmCancelError("error occurs"));
            console.warn('Some error found in "shipmentConfirmCancel" action\n', e);
        }
    }
}

export function* getCompleteDetailShipmentRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getCompleteDetailShipmentClear())
    } else {
        try {
            let orderId = action.payload.orderId == undefined ? "" : action.payload.orderId;
            let setHeaderId = action.payload.setHeaderId == undefined ? "" : action.payload.setHeaderId;
            let detailType = action.payload.detailType == undefined ? "" : action.payload.detailType;
            let poType = action.payload.poType == undefined ? "" : action.payload.poType;
            let shipmentStatus = action.payload.shipmentStatus == undefined ? "" : action.payload.shipmentStatus;
            let shipmentId = action.payload.shipmentId == undefined ? "" : action.payload.shipmentId;
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/entship/find/srDetail`, {
                orderId,
                setHeaderId,
                detailType,
                poType,
                shipmentStatus,
                shipmentId
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getCompleteDetailShipmentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getCompleteDetailShipmentError(response.data));
            }

        } catch (e) {
            yield put(actions.getCompleteDetailShipmentError("error occurs"));
            console.warn('Some error found in "getCompleteDetailShipment" action\n', e);
        }
    }
}


// _______________________________VENDOR SHIPMENT API'S___________________________________

export function* getAllShipmentVendorRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAllShipmentVendorClear())
    } else {
        try {
            let no = action.payload.no == undefined ? 1 : action.payload.no;
            let type = action.payload.type == undefined ? 1 : action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let status = action.payload.status == undefined ? "" : action.payload.status;
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy;
            let sortedIn = action.payload.sortedIn == undefined ? "" : action.payload.sortedIn;
            let isDashboardComment = action.payload.isDashboardComment;
            let isDashboardShipment = action.payload.isDashboardShipment == undefined ?  0 : action.payload.isDashboardShipment;
            let isReportPage = action.payload.isReportPage == undefined ?  0 : action.payload.isReportPage;

            let get = {
                pageNo: no,
                type: type,
                search: search,
                status: status,
                sortedBy,
                sortedIn,
                isDashboardComment,
                isDashboardShipment,
                isReportPage
            }
            let filter = {...action.payload.filter}
            let final = { ...get,  filter}

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/find/all/sr`, final
            );
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAllShipmentVendorSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAllShipmentVendorError(response.data));
            }

        } catch (e) {
            yield put(actions.getAllShipmentVendorError("error occurs"));
            console.warn('Some error found in "getAllShipmentVendor" action\n', e);
        }
    }
}

export function* updateVendorRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updateVendorClear())
    } else {
        try {
            let updateShipment = action.payload.updateShipment;
            let shipmentCriteria = action.payload.shipmentCriteria;
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/update/status`, {
                'updateShipment': updateShipment,
                'shipmentCriteria': shipmentCriteria
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateVendorSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.updateVendorError(response.data));
            }

        } catch (e) {
            yield put(actions.updateVendorError("error occurs"));
            console.warn('Some error found in "updateVendor" action\n', e);
        }
    }
}


export function* getShipmentDetailsRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getShipmentDetailsClear())
    } else {
        try {
            let orderId = action.payload.orderId == undefined ? "" : action.payload.orderId;
            let setHeaderId = action.payload.setHeaderId == undefined ? "" : action.payload.setHeaderId;
            let poType = action.payload.poType == undefined ? "" : action.payload.poType;
            let detailType = action.payload.detailType == undefined ? "" : action.payload.detailType;
            let shipmentStatus = action.payload.shipmentStatus == undefined ? "" : action.payload.shipmentStatus;
            let shipmentId = action.payload.shipmentId == undefined ? "" : action.payload.shipmentId;

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/find/srDetail`, {
                orderId,
                setHeaderId,
                poType,
                detailType,
                shipmentStatus,
                shipmentId
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getShipmentDetailsSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getShipmentDetailsError(response.data));
            }

        } catch (e) {
            yield put(actions.getShipmentDetailsError("error occurs"));
            console.warn('Some error found in "getShipmentDetails" action\n', e);
        }
    }
}
// ___________________________________COMMENT GET ALL___________________________

export function* getAllCommentRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAllCommentClear())
    } else {
        try {
            //let shipmentId = action.payload.shipmentId == undefined ? "" : action.payload.shipmentId
            let isAllAttachment = action.payload.isAllAttachment == undefined ? "" : action.payload.isAllAttachment
            let module = action.payload.module == undefined ? "" : action.payload.module
            let pageNo = action.payload.pageNo == undefined ? "" : action.payload.pageNo;
            //let orderCode = action.payload.orderCode == undefined ? "" : action.payload.orderCode;
            //let orderNumber = action.payload.orderNumber == undefined ? "" : action.payload.orderNumber
            let subModule = action.payload.subModule == undefined ? "" : action.payload.subModule
            //let orderId = action.payload.orderId == undefined ? "" : action.payload.orderId;
            //let documentNumber = action.payload.documentNumber == undefined ? "" : action.payload.documentNumber;
            let type = action.payload.type == undefined ? 1 : action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let commentId = action.payload.commentId == undefined ? 1 : action.payload.commentId;
            let commentCode = action.payload.commentCode == undefined ? "" : action.payload.commentCode;
          
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/find/all/comqcinvoice`, {
                //shipmentId: shipmentId,
                module: module,
                pageNo: pageNo,
                //orderCode: orderCode,
                //orderNumber: orderNumber,
                subModule,
                isAllAttachment,
                //orderId,
                //documentNumber,
                type,
                search,
                commentId,
                commentCode
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                response.data.data.refresh = action.payload.refresh
                yield put(actions.getAllCommentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAllCommentError(response.data));
            }

        } catch (e) {
            yield put(actions.getAllCommentError("error occurs"));
            console.warn('Some error found in "getAllComment" action\n', e);
        }
    }
}

export function* qcAddCommentRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.qcAddCommentClear())
    } else {
        try {

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/add/comqcinvoice`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.qcAddCommentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.qcAddCommentError(response.data));
            }

        } catch (e) {
            yield put(actions.qcAddCommentError("error occurs"));
            console.warn('Some error found in "qcAddComment" action\n', e);
        }
    }
}

export function* deleteUploadsRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteUploadsClear())
    } else {
        try {

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/delete/uploads`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.deleteUploadsSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.deleteUploadsError(response.data));
            }

        } catch (e) {
            yield put(actions.deleteUploadsError("error occurs"));
            console.warn('Some error found in "deleteUploads" action\n', e);
        }
    }
}

export function* uploadCommentRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.uploadCommentClear())
    } else {
        try {
            let shipmentId = action.payload.shipmentId
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/get/${shipmentId}/comment`, {

            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.uploadCommentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.uploadCommentError(response.data));
            }

        } catch (e) {
            yield put(actions.uploadCommentError("error occurs"));
            console.warn('Some error found in "uploadComment" action\n', e);
        }
    }
}

export function* updateVendorShipmentRequest(action) { 
    if (action.payload == undefined) {
        yield put(actions.updateVendorShipmentClear())
    } else {
        try {
            const response = action.payload.editRequestQty == undefined ? yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/update/shipment`, action.payload) :
                             yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/entlogi/update/shipment`, action.payload)
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateVendorShipmentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.updateVendorShipmentError(response.data));
            }

        } catch (e) {
            yield put(actions.updateVendorShipmentError("error occurs"));
            console.warn('Some error found in "updateVendorShipment" action\n', e);
        }
    }
}

export function* updateInvoiceRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updateInvoiceClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/update/invoice`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateInvoiceSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.updateInvoiceError(response.data));
            }

        } catch (e) {
            yield put(actions.updateInvoiceError("error occurs"));
            console.warn('Some error found in "updateInvoice" action\n', e);
        }
    }
}

// add multiple comment qc
export function* addMultipleCommentQcRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.addMultipleCommentQcClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/add/multiple/comqcinvoice`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.addMultipleCommentQcSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.addMultipleCommentQcError(response.data));
            }

        } catch (e) {
            yield put(actions.addMultipleCommentQcError("error occurs"));
            console.warn('Some error found in "addMultipleCommentQc" action\n', e);
        }
    }
}
//-------------------------

//get all user emails
export function* getUserEmailsRequest(action) {
    try {

        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/user/find/info`, action.payload);
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getUserEmailsSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getUserEmailsError(response.data));
        }

    } catch (e) {
        yield put(actions.getUserEmailsError("error occurs"));
        console.warn('Some error found in "getUserEmails" action\n', e);
    }
}