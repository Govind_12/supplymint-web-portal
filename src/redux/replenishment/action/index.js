import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import { OganisationIdName } from '../../../organisationIdName';

export function* createTriggerRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.createTriggerClear());
  }
  else {

    try {
          let jobName = action.payload.jobName
          let cronExpression = action.payload.cronExpression
          let planningAttribute = action.payload.planningAttribute
          let locationFilter = action.payload.locationFilter
          let divisionFilter = action.payload.divisionFilter
          let sectionFilter = action.payload.sectionFilter
          let departmentFilter = action.payload.departmentFilter
          let brandFilter = action.payload.brandFilter
          let mrpRangeFilter = action.payload.mrpRangeFilter

          const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/create/newTrigger`, action.payload);
          const finalResponse = script(response);
          if (finalResponse.success) {
            yield put(actions.createTriggerSuccess(response.data.data));
          // yield put(actions.nscheduleRequest(triggerName));
          } else if (finalResponse.failure) {
            yield put(actions.createTriggerError(response.data));
          }
    } catch (e) {
      yield put(actions.createTriggerError("error occurs"));
      console.warn('Some error found in "create Trigger " action\n', e);
    }
  }
}

export function* getJobByNameRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getJobByNameRequest());
  }
  else {

    try {
      let name = action.payload;
      let { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/get/jobName?jobName=${name}&orgId=${orgIdGlobal}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getJobByNameSuccess(response.data.data));
        // sessionStorage.setItem('jobId', response.data.data.resource.jobId);
      } else if (finalResponse.failure) {
        yield put(actions.getJobByNameError(response.data));
      }
    } catch (e) {
      yield put(actions.getJobByNameError("error occurs"));
      console.warn('Some error found in "getJobByNameError" action\n', e);
    }
  }
}


export function* jobRunDetailRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    console.log(action.payload)
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/get/rundetailnew`, {
      // jobName: action.payload,
      // jobId: sessionStorage.getItem('jobId') == "null" ? "NA" : sessionStorage.getItem('jobId') == null ? "NA" : sessionStorage.getItem('jobId') == undefined ? "NA" : sessionStorage.getItem('jobId'),
      // // jobId: action.payload.jobId,
      // orgId: orgIdGlobal
      manageRuleId: action.payload
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      // if (response.data.data.resource.state == "STOPPED") {
      //   yield put(actions.getJobByNameRequest(action.payload));
      //   yield put(actions.lastJobRequest(action.payload));

      // }
      // if (response.data.data.resource.state == "SUCCEEDED" || response.data.data.resource.state == "FAILED") {
      //   // var mode = "";
      //   // if (process.env.NODE_ENV == "develop") {
      //   //   mode = "-DEV-JOB"
      //   // }
      //   // if (process.env.NODE_ENV == "production") {
      //   //   mode = "-DEV-JOB"
      //   // }
      //   // else {
      //   //   mode = "-DEV-JOB"
      //   // }
      //   yield put(actions.getJobByNameRequest(action.payload));
      //   yield put(actions.lastJobRequest(action.payload));

      // }

      yield put(actions.jobRunDetailSuccess(response.data.data));
      sessionStorage.setItem('jobId', response.data.data.resource.jobId);
      if (response.data.data.resource.state == "STOPPED") {
        sessionStorage.removeItem('jobId')
      }
    } else if (finalResponse.failure) {
      yield put(actions.jobRunDetailError(response.data));
    }
  } catch (e) {
    yield put(actions.jobRunDetailError("error occurs"));
    console.warn('Some error found in "jobRunDetailError" action\n', e);
  }
}

export function* runOnDemandRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.runOnDemandClear());
  } else {
    try {
      sessionStorage.removeItem('jobId');


      // let abc = ""
      // if(action.payload.runType == "no-filter"){
      //   for(let i = 0; i<action.payload.selected.length;i++){
      //     let a = action.payload.selected[i].storeCode;
      //     if(action.payload.selected.length > 1){
      //       abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
      //     }else{
      //       abc = a;
      //     }
      //   }
      // }else{
      //   abc = action.payload.selected
      // }
      // var abc = ""
      // if (sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS") {
      //   abc = action.payload.selected
      // } else if (sessionStorage.getItem('partnerEnterpriseName') == "VMART") {
      //   abc = action.payload.storeCode
      // } else {
      //   abc = action.payload.storeCode
      // }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/run/ondemandnew`, 
      // {
      //   jobName: action.payload.jobName,
      //   store: action.payload.store,
      //   // storeCode: sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? action.payload.selected : "ALL_STORES",
      //   storeCode: action.payload.storeCode == "" ? "NA" : action.payload.storeCode,
      //   path: action.payload.path == undefined ? "NA" : action.payload.path == "" ? "NA" : action.payload.path,
      //   userName: sessionStorage.getItem('userName'),
      //   stockPoint: action.payload.stockPoint,
      //   requirement: action.payload.requirement != undefined ? action.payload.requirement : "NA",
      //   allocation: action.payload.allocation != undefined ? action.payload.allocation : "NA",
      // }
      action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.runOnDemandSuccess(response.data.data));
        sessionStorage.setItem('jobId', response.data.data.resource.jobId);
      } else if (finalResponse.failure) {
        yield put(actions.runOnDemandError(response.data));
      }
    } catch (e) {
      yield put(actions.runOnDemandError("error occurs"));
      console.warn('Some error found in "runOnDemend" action\n', e);
    }
  }
}

export function* stopOnDemandRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.stopOnDemandClear());
  } else {
    try {


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/stop/ondemand`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getJobByNameRequest(action.payload));
        yield put(actions.stopOnDemandSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.stopOnDemandError(response.data));
      }
    } catch (e) {
      yield put(actions.stopOnDemandError("error occurs"));
      console.warn('Some error found in "stopOnDemend" action\n', e);
    }
  }
}

export function* nscheduleRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/get/triggerDetail`, {
      triggerName: action.payload,
      // userName: sessionStorage.getItem('userName'),
      // jobId: sessionStorage.getItem('jobId')
      orgId: orgIdGlobal
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.nscheduleSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.nscheduleError(response.data));
    }
  } catch (e) {
    yield put(actions.nscheduleError("error occurs"));
    console.warn('Some error found in "stopOnDemend" action\n', e);
  }
}

export function* jobHistoryRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type == undefined ? 1 : action.payload.type;
    let from = action.payload.from == undefined ? "" : action.payload.from;
    let to = action.payload.to == undefined ? "" : action.payload.to;
    let id = action.payload.jobId == undefined ? "" : action.payload.jobId;
    let search = action.payload.search == undefined ? "" : action.payload.search;

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/get/all?pageno=${no}&type=${type}&from=${from}&to=${to}&id=${id}&search=${search}`, {
      filter: action.payload.filter
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.jobHistorySuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.jobHistoryError(response.data));
    }
  } catch (e) {
    yield put(actions.jobHistoryError("error occurs"));
    console.warn('Some error found in "jobHistory" action\n', e);
  }
}

export function* jobHistoryFilterRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type == undefined ? 1 : action.payload.type;
    let from = action.payload.from == undefined ? "" : action.payload.from;
    let to = action.payload.to == undefined ? "" : action.payload.to;

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/get/all?pageno=${no}&type=${type}&from=${from}&to=${to}&orgId=${orgIdGlobal}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.jobHistoryFilterSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.jobHistoryFilterError(response.data));
    }
  } catch (e) {
    yield put(actions.jobHistoryFilterError("error occurs"));
    console.warn('Some error found in "jobFilterHistory" action\n', e);
  }
}

export function* createJobRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/createJob`, {
      jobName: action.payload,
      userName: sessionStorage.getItem('userName')
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.createJobSuccess(response.data.data));
      // yield put(actions.runOnDemandRequest(action.payload));
    } else if (finalResponse.failure) {
      yield put(actions.createJobError(response.data));
    }
  } catch (e) {
    yield put(actions.createJobError("error occurs"));
    console.warn('Some error found in "createJob" action\n', e);
  }
}

export function* allStoreCodeRequest(action) {
  try {
    let jobName = action.payload.jobName
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/get/storecode/name?jobName=${jobName}`, {

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.allStoreCodeSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.allStoreCodeError(response.data));
    }
  } catch (e) {
    yield put(actions.allStoreCodeError("error occurs"));
    console.warn('Some error found in "allStoreCode" action\n', e);
  }
}

export function* lastJobRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    // let jobName = action.payload;
    // const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/get/jobName?jobName=${jobName}`, {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/getLastJob/summary`, {
      manageRuleId: action.payload,

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.lastJobSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.lastJobError(response.data));
    }
  } catch (e) {
    yield put(actions.lastJobError("error occurs"));
    console.warn('Some error found in "lastJob" action\n', e);
  }
}


export function* getAllocationRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {    
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/getAllocation/summary`, action.payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getAllocationSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getAllocationError(response.data));
    }
  } catch (e) {
    yield put(actions.getAllocationError("error occurs"));
    console.warn('Some error found in "lastJob" action\n', e);
  }
}


export function* getRequirementSummaryRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getRequirementSummaryClear());
  } else {
    try {    
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/get/all/RequirementSummary`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getRequirementSummarySuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getRequirementSummaryError(response.data));
      }
    } catch (e) {
      yield put(actions.getRequirementSummaryError("error occurs"));
      console.warn('Some error found in "lastJob" action\n', e);
    }
  }
}



export function* summaryDetailRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.summaryDetailClear());
  } else {
    try {
      let type = action.payload.type == undefined ? 1 : action.payload.type;
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let scode = action.payload.scode == undefined ? "" : action.payload.scode;
      let displayName = action.payload.displayName == undefined ? "" : action.payload.displayName;
      let jobName = action.payload.jobName == undefined ? "" : action.payload.jobName

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.REPLENISHMENT}/getLastJob/Sdetails?type=${type}&pageno=${no}&storecode=${scode}&displayName=${displayName}&jobName=${jobName}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.summaryDetailSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.summaryDetailError(response.data));
      }
    } catch (e) {
      yield put(actions.summaryDetailError("error occurs"));
      console.warn('Some error found in "summaryDetail" action\n', e);
    }
  }
}

export function* summaryCsvRequest(action) {
  try {
    // let type = action.payload.type == undefined ? 1 : action.payload.type;
    // let no = action.payload.no == undefined ? 1 : action.payload.no;
    // let scode = action.payload.scode == undefined ? "" : action.payload.scode;
    let date = action.payload
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/aws/s3bucket/presigned?${date}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.summaryCsvSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.summaryCsvError(response.data));
    }
  } catch (e) {
    yield put(actions.summaryCsvError("error occurs"));
    console.warn('Some error found in "summaryCsv" action\n', e);
  }
}

export function* getStoreCodeRequest(action) {

  try {
    var { orgIdGlobal } = OganisationIdName()
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/get/storeCode`, {
      filter: action.payload.filter,
      storeCode: action.payload.storeCode,
      orgId: orgIdGlobal
    });
    // conflict code
    // let partner = []
    // action.payload.partner != "" ? partner.push(action.payload.partner) : []


    // var { orgCodeGlobal } = OganisationIdName()
    // const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/get/storeCode`, {
    //   channel: orgCodeGlobal,
    //   zone: action.payload.zone.length == 0 ? [] : action.payload.zone,
    //   grade: action.payload.grade.length == 0 ? [] : action.payload.grade,
    //   partner: partner

    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getStoreCodeSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getStoreCodeError(response.data));
    }
  } catch (e) {
    yield put(actions.getStoreCodeError("error occurs"));
    console.warn('Some error found in "getStoreCode" action\n', e);
  }

}

export function* getGradeRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getGradeClear());
  } else {
    try {
      // let type = action.payload.type == undefined ? 1 : action.payload.type;
      // let no = action.payload.no == undefined ? 1 : action.payload.no;
      // let scode = action.payload.scode == undefined ? "" : action.payload.scode;
      let channel = action.payload.channel;
      var { orgCodeGlobal } = OganisationIdName()
      let zone = action.payload.zone;
      let partner = action.payload.partner;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/get/grade`, {
        channel: orgCodeGlobal,
        zone: zone,
        partner: partner


      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getGradeSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getGradeError(response.data));
      }
    } catch (e) {
      yield put(actions.getGradeError("error occurs"));
      console.warn('Some error found in "getGrade" action\n', e);
    }
  }
}
export function* getZoneRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getZoneClear());
  } else {
    try {
      var { orgCodeGlobal } = OganisationIdName()
      let channel = action.payload.channel;
      let partner = action.payload.partner;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/zone?channel=${orgCodeGlobal}&partner=${partner}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getZoneSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getZoneError(response.data));
      }
    } catch (e) {
      yield put(actions.getZoneError("error occurs"));
      console.warn('Some error found in "getZone" action\n', e);
    }
  }
}

export function* getPartnerRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getPartnerClear());
  } else {
    try {
      let channel = action.payload.channel
      var { orgCodeGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/partner?channel=${orgCodeGlobal}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getPartnerSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getPartnerError(response.data));
      }
    } catch (e) {
      yield put(actions.getPartnerError("error occurs"));
      console.warn('Some error found in "getPartner" action\n', e);
    }
  }
}

export function* getCSVLinkRequest(action) {

  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/aws/s3bucket/download/OUTPUT`, {
      jobId: action.payload
    });
    console.log(response)
    const finalResponse = script(response);
    console.log(finalResponse)
    if (finalResponse.success) {
      yield put(actions.getCSVLinkSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getCSVLinkError(response.data));
    }
  } catch (e) {
    yield put(actions.getCSVLinkError("error occurs"));
    console.warn('Some error found in "getCSVLink" action\n', e);
  }

}

export function* getXLSLinkRequest(action) {
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/rule/engine/download/summary/${sessionStorage.getItem('partnerEnterpriseName')}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getXLSLinkSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getXLSLinkError(response.data));
    }
  } catch (e) {
    yield put(actions.getXLSLinkError("error occurs"));
    console.warn('Some error found in "getXLSLink" action\n', e);
  }
}

export function* toStatusRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    console.log(action)
    let jobId = action.payload;
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/rule/engine/get/transferOrder?id=${jobId}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.toStatusSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.toStatusError(response.data));
    }
  } catch (e) {
    yield put(actions.toStatusError("error occurs"));
    console.warn('Some error found in "toStatus" action\n', e);
  }
}


export function* applyFilterRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.applyFilterClear());
  } else {
    try {
      // let channel = action.payload.channel;
      // let partner = action.payload.partner;
      // let zone = action.payload.zone;
      // let grade = action.payload.grade;
      // let storeCode = action.payload.storeCode;
      let partner = []
      action.payload.partner != "" ? partner.push(action.payload.partner) : []

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/filter/storeCode`, {
        "filter": action.payload.filter,
        "orgId": orgIdGlobal
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.applyFilterSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.applyFilterError(response.data));
      }
    } catch (e) {
      yield put(actions.applyFilterError("error occurs"));
      console.warn('Some error found in "applyFilter" action\n', e);
    }
  }
}

export function* getMainHeaderConfigRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getMainHeaderConfigClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      let enterpriseName = action.payload.enterpriseName;
      let attributeType = action.payload.attributeType;
      let displayName = action.payload.displayName;

      var { orgIdGlobal } = OganisationIdName()

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/headerconfig/get/header?enterpriseName=${enterpriseName}&attributeType=${attributeType}&displayName=${displayName}&orgId=${orgIdGlobal}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        yield put(actions.getMainHeaderConfigSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getMainHeaderConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.getMainHeaderConfigError("error occurs"));
      console.warn('Some error found in "getMainHeaderConfig" action\n', e);
    }
  }
}

export function* getSetHeaderConfigRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getSetHeaderConfigClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      let enterpriseName = action.payload.enterpriseName;
      let attributeType = action.payload.attributeType;
      let displayName = action.payload.displayName;

      var { orgIdGlobal } = OganisationIdName()

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/headerconfig/get/header?enterpriseName=${enterpriseName}&attributeType=${attributeType}&displayName=${displayName}&orgId=${orgIdGlobal}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        yield put(actions.getSetHeaderConfigSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getSetHeaderConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.getSetHeaderConfigError("error occurs"));
      console.warn('Some error found in "getSetHeaderConfig" action\n', e);
    }
  }
}

export function* getItemHeaderConfigRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getItemHeaderConfigClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      let enterpriseName = action.payload.enterpriseName;
      let attributeType = action.payload.attributeType;
      let displayName = action.payload.displayName;

      var { orgIdGlobal } = OganisationIdName()

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/headerconfig/get/header?enterpriseName=${enterpriseName}&attributeType=${attributeType}&displayName=${displayName}&orgId=${orgIdGlobal}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        yield put(actions.getItemHeaderConfigSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getItemHeaderConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.getItemHeaderConfigError("error occurs"));
      console.warn('Some error found in "getItemHeaderConfig" action\n', e);
    }
  }
}


export function* getHeaderConfigRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getHeaderConfigClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      let enterpriseName = action.payload.enterpriseName;
      let attributeType = action.payload.attributeType;
      let displayName = action.payload.displayName;

      var { orgIdGlobal } = OganisationIdName()

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/headerconfig/get/header?enterpriseName=${enterpriseName}&attributeType=${attributeType}&displayName=${displayName}&orgId=${orgIdGlobal}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        yield put(actions.getHeaderConfigSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getHeaderConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.getHeaderConfigError("error occurs"));
      console.warn('Some error found in "getHeaderConfig" action\n', e);
    }
  }
}

export function* getHeaderConfigExcelRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getHeaderConfigExcelClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      let enterpriseName = action.payload.enterpriseName;
      let attributeType = action.payload.attributeType;
      let displayName = action.payload.displayName;

      var { orgIdGlobal } = OganisationIdName()

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/headerconfig/get/header?enterpriseName=${enterpriseName}&attributeType=${attributeType}&displayName=${displayName}&orgId=${orgIdGlobal}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        console.log(response)
        yield put(actions.getHeaderConfigExcelSuccess(response.data.data));
        yield put(actions.getHeaderConfigExcelClear());
      } else if (finalResponse.failure) {
        yield put(actions.getHeaderConfigExcelError(response.data));
      }
    } catch (e) {
      yield put(actions.getHeaderConfigExcelError("error occurs"));
      console.warn('Some error found in "getHeaderConfig" action\n', e);
    }
  }
}

export function* getCustomHeaderRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getCustomHeaderClear());
  } else {
    try {
      let isDefault = action.payload.isDefault;
      let attributeType = action.payload.attributeType;
      let displayName = action.payload.displayName;

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/rule/engine/get/customheader?isDefault=${isDefault}&attributeType=${attributeType}&displayName=${displayName}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getCustomHeaderSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getCustomHeaderError(response.data));
      }
    } catch (e) {
      yield put(actions.getCustomHeaderError("error occurs"));
      console.warn('Some error found in "getCustomHeader" action\n', e);
    }
  }
}

export function* createHeaderConfigRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.createHeaderConfigClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      var { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/headerconfig/create`, {
        enterpriseId: sessionStorage.getItem('partnerEnterpriseId'),
        enterpriseName: sessionStorage.getItem('partnerEnterpriseName').toUpperCase(),
        module: action.payload.module,
        subModule: action.payload.subModule,
        section: action.payload.section,
        source: action.payload.source,
        typeConfig: action.payload.typeConfig,
        displayName: action.payload.displayName,
        attributeType: action.payload.attributeType,
        token: sessionStorage.getItem('token'),
        fixedHeaders: action.payload.fixedHeaders,
        defaultHeaders: action.payload.defaultHeaders,
        customHeaders: action.payload.customHeaders,

        orgId: orgIdGlobal

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        yield put(actions.createHeaderConfigSuccess(response.data.data));
        // yield put(actions.getFiveTriggersRequest());
      } else if (finalResponse.failure) {
        yield put(actions.createHeaderConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.createHeaderConfigError("error occurs"));
      console.warn('Some error found in "applyFilter" action\n', e);
    }
  }
}
//createMainHeaderConfigRequest
export function* createMainHeaderConfigRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.createMainHeaderConfigClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      var { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/headerconfig/create`, {
        enterpriseId: sessionStorage.getItem('partnerEnterpriseId'),
        enterpriseName: sessionStorage.getItem('partnerEnterpriseName').toUpperCase(),
        module: action.payload.module,
        subModule: action.payload.subModule,
        section: action.payload.section,
        source: action.payload.source,
        typeConfig: action.payload.typeConfig,
        displayName: action.payload.displayName,
        attributeType: action.payload.attributeType,
        token: sessionStorage.getItem('token'),
        fixedHeaders: action.payload.fixedHeaders,
        defaultHeaders: action.payload.defaultHeaders,
        customHeaders: action.payload.customHeaders,

        orgId: orgIdGlobal

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        yield put(actions.createMainHeaderConfigSuccess(response.data.data));
        // yield put(actions.getFiveTriggersRequest());
      } else if (finalResponse.failure) {
        yield put(actions.createMainHeaderConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.createMainHeaderConfigError("error occurs"));
      console.warn('Some error found in "applyFilter" action\n', e);
    }
  }
}
//createSetHeaderConfigRequest
export function* createSetHeaderConfigRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.createSetHeaderConfigClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      var { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/headerconfig/create`, {
        enterpriseId: sessionStorage.getItem('partnerEnterpriseId'),
        enterpriseName: sessionStorage.getItem('partnerEnterpriseName').toUpperCase(),
        module: action.payload.module,
        subModule: action.payload.subModule,
        section: action.payload.section,
        source: action.payload.source,
        typeConfig: action.payload.typeConfig,
        displayName: action.payload.displayName,
        attributeType: action.payload.attributeType,
        token: sessionStorage.getItem('token'),
        fixedHeaders: action.payload.fixedHeaders,
        defaultHeaders: action.payload.defaultHeaders,
        customHeaders: action.payload.customHeaders,

        orgId: orgIdGlobal

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        yield put(actions.createSetHeaderConfigSuccess(response.data.data));
        // yield put(actions.getFiveTriggersRequest());
      } else if (finalResponse.failure) {
        yield put(actions.createSetHeaderConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.createSetHeaderConfigError("error occurs"));
      console.warn('Some error found in "applyFilter" action\n', e);
    }
  }
}
//createItemHeaderConfigRequest
export function* createItemHeaderConfigRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.createItemHeaderConfigClear());
  } else {
    try {
      let basedOn = action.payload.basedOn != undefined ? action.payload.basedOn : ""
      var { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/headerconfig/create`, {
        enterpriseId: sessionStorage.getItem('partnerEnterpriseId'),
        enterpriseName: sessionStorage.getItem('partnerEnterpriseName').toUpperCase(),
        module: action.payload.module,
        subModule: action.payload.subModule,
        section: action.payload.section,
        source: action.payload.source,
        typeConfig: action.payload.typeConfig,
        displayName: action.payload.displayName,
        attributeType: action.payload.attributeType,
        token: sessionStorage.getItem('token'),
        fixedHeaders: action.payload.fixedHeaders,
        defaultHeaders: action.payload.defaultHeaders,
        customHeaders: action.payload.customHeaders,

        orgId: orgIdGlobal

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.basedOn = basedOn;
        yield put(actions.createItemHeaderConfigSuccess(response.data.data));
        // yield put(actions.getFiveTriggersRequest());
      } else if (finalResponse.failure) {
        yield put(actions.createItemHeaderConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.createItemHeaderConfigError("error occurs"));
      console.warn('Some error found in "applyFilter" action\n', e);
    }
  }
}



export function* updateHeaderLogsRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updateHeaderLogsClear());
  } else {
    try {
      let channel = action.payload.channel;
      let partner = action.payload.partner;
      let zone = action.payload.zone;
      let grade = action.payload.grade;
      let storeCode = action.payload.storeCode;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/filter/storeCode?channel=${channel}&partner=${partner}&zone=${zone}&grade=${grade}&storeCode=${storeCode}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateHeaderLogsSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updateHeaderLogsError(response.data));
      }
    } catch (e) {
      yield put(actions.updateHeaderLogsError("error occurs"));
      console.warn('Some error found in "applyFilter" action\n', e);
    }
  }
}
//fmcg

export function* getMotherCompRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getMotherCompClear());
  } else {
    try {
      let no = action.payload.no == undefined ? 1 : action.payload.no
      let type = action.payload.type == undefined ? 1 : action.payload.type
      let search = action.payload.search == undefined ? "" : action.payload.search

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/fmcg/mother/comp?pageno=${no}&search=${search}&type=${type}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getMotherCompSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getMotherCompError(response.data));
      }
    } catch (e) {
      yield put(actions.getMotherCompError("error occurs"));
      console.warn('Some error found in "getMotherComp" action\n', e);
    }
  }
}
export function* getArticleFmcgRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getArticleFmcgClear());
  } else {
    try {
      let motherComp = action.payload.motherComp == undefined ? "" : action.payload.motherComp
      let type = action.payload.type == undefined ? 1 : action.payload.type
      let search = action.payload.search == undefined ? "" : action.payload.search
      let no = action.payload.no == undefined ? 1 : action.payload.no
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/fmcg/article?motherComp=${motherComp}&pageno=${no}&search=${search}&type=${type}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getArticleFmcgSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getArticleFmcgError(response.data));
      }
    } catch (e) {
      yield put(actions.getArticleFmcgError("error occurs"));
      console.warn('Some error found in "getArticleFmcg" action\n', e);
    }
  }
}
export function* getVendorFmcgRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updateHeaderLogsClear());
  } else {
    try {
      let motherComp = action.payload.motherComp == undefined ? "" : action.payload.motherComp
      let articleCode = action.payload.articleCode == undefined ? "" : action.payload.articleCode
      let type = action.payload.type == undefined ? 1 : action.payload.type
      let search = action.payload.search == undefined ? "" : action.payload.search
      let no = action.payload.no == undefined ? 1 : action.payload.no

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/fmcg/vendor?motherComp=${motherComp}&pageno=${no}&articleCode=${articleCode}&search=${search}&type=${type}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getVendorFmcgSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getVendorFmcgError(response.data));
      }
    } catch (e) {
      yield put(actions.getVendorFmcgError("error occurs"));
      console.warn('Some error found in "getVendorFmcg" action\n', e);
    }
  }
}
export function* getStoreFmcgRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getStoreFmcgClear());
  } else {
    try {
      let motherComp = action.payload.motherComp == undefined ? "" : action.payload.motherComp
      let articleCode = action.payload.articleCode == undefined ? "" : action.payload.articleCode
      let vendorCode = action.payload.vendorCode == undefined ? "" : action.payload.vendorCode
      let type = action.payload.type == undefined ? 1 : action.payload.type
      let search = action.payload.search == undefined ? "" : action.payload.search
      let no = action.payload.no == undefined ? 1 : action.payload.no

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/fmcg/store?motherComp=${motherComp}&pageno=${no}&articleCode=${articleCode}&vendorCode=${vendorCode}&search=${search}&type=${type}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getStoreFmcgSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getStoreFmcgError(response.data));
      }
    } catch (e) {
      yield put(actions.getStoreFmcgError("error occurs"));
      console.warn('Some error found in "getStoreFmcgError" action\n', e);
    }
  }
}
export function* applyFilterFmcgRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.applyFilterFmcgClear());
  } else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/custom/get/fmcg/filter/store`, {
        "motherComp": action.payload.motherComp,
        "articleCode": action.payload.articleCode,
        "vendorCode": action.payload.vendorCode,
        "storeCode": action.payload.storeCode
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.applyFilterFmcgSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.applyFilterFmcgError(response.data));
      }
    } catch (e) {
      yield put(actions.applyFilterFmcgError("error occurs"));
      console.warn('Some error found in "applyFilterFmcg" action\n', e);
    }
  }
}


export function* customFilterMenuRequest(action) {

  try {

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/filter/menu`, {

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.customFilterMenuSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.customFilterMenuError(response.data));
    }
  } catch (e) {
    yield put(actions.customFilterMenuError("error occurs"));
    console.warn('Some error found in "customFilterMenu" action\n', e);
  }

}


export function* getFiveTriggersRequest(action) {
  if (action.payload == undefined) {
    yield put(action.getFiveTriggersClear());
  } else {

    try {
      let type = action.payload.type
      let pageNo = action.payload.pageNo
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/rule/engine/get/alltriggers`, {
        type,
        pageNo
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getFiveTriggersSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.getFiveTriggersError(response.data));
      }
    } catch (e) {
      yield put(actions.getFiveTriggersError("error occurs"));
      console.warn('Some error found in "getFiveTriggers" action\n', e);
    }
  }

}

export function* deleteTriggerRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.deleteTriggerClear());
  } else {
    try {
      let id = action.payload.id
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/rule/engine/job/delete/trigger?id=${id}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.deleteTriggerSuccess(response.data.data));
        //yield put(actions.getFiveTriggersRequest());
      } else if (finalResponse.failure) {
        yield put(actions.deleteTriggerError(response.data));
      }
    } catch (e) {
      yield put(actions.deleteTriggerError("error occurs"));
      console.warn('Some error found in "deleteTrigger" action\n', e);
    }
  }
}



export function* getFilterSectionRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getFilterSectionClear());
  } else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/get/filtersection?orgId=${orgIdGlobal}`, {


      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getFilterSectionSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getFilterSectionError(response.data));
      }
    } catch (e) {
      yield put(actions.getFilterSectionError("error occurs"));
      console.warn('Some error found in "getFilterSection" action\n', e);
    }
  }
}


export function* deleteConfigRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.deleteConfigClear());
  } else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/delete/config`, {
        "orgId": orgIdGlobal,
        "configName": action.payload.configName

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.deleteConfigSuccess(response.data.data));
        let invget = {
          type: 1,
          no: 1,
          search: ""
        }
        yield put(actions.invGetAllConfigRequest(invget))
      } else if (finalResponse.failure) {
        yield put(actions.deleteConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.deleteConfigError("error occurs"));
      console.warn('Some error found in "deleteConfig" action\n', e);
    }
  }
}


export function* statusChangeRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.statusChangeClear());
  } else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/status/change`, {
        "orgId": orgIdGlobal,
        "configName": action.payload.configName,
        "isEnabled": action.payload.isEnabled

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.statusChangeSuccess(response.data.data));
        let invget = {
          type: 1,
          no: 1,
          search: ""
        }
        yield put(actions.invGetAllConfigRequest(invget))
      } else if (finalResponse.failure) {
        yield put(actions.statusChangeError(response.data));
      }
    } catch (e) {
      yield put(actions.statusChangeError("error occurs"));
      console.warn('Some error found in "statusChange" action\n', e);
    }
  }
}


export function* invGetAllConfigRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.invGetAllConfigClear());
  } else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      let type = action.payload.type == "" ? 1 : action.payload.type;
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let search = action.payload.search;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/get/all/config?pageno=${no}&type=${type}&search=${search}&orgId=${orgIdGlobal}`, {


      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.invGetAllConfigSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.invGetAllConfigError(response.data));
      }
    } catch (e) {
      yield put(actions.invGetAllConfigError("error occurs"));
      console.warn('Some error found in "invGetAllConfig" action\n', e);
    }
  }
}


export function* getJobDataRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getJobDataClear());
  } else {
    try {

      let id = action.payload;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/get/config-detail?id=${id}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getJobDataSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getJobDataError(response.data));
      }
    } catch (e) {
      yield put(actions.getJobDataError("error occurs"));
      console.warn('Some error found in "getJobData" action\n', e);
    }
  }
}

export function* updateJobDataRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updateJobDataClear());
  } else {
    try {
      let updateData = action.payload;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/update/config-detail`, updateData
      );
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateJobDataSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.updateJobDataError(response.data));
      }
    } catch (e) {
      yield put(actions.getJobDataError("error occurs"));
      console.warn('Some error found in "getJobData" action\n', e);
    }
  }
}


export function* configCreateRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.configCreateClear());
  } else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      var { orgNameGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/create`, {
        "orgId": orgIdGlobal,
        "orgName": orgNameGlobal,
        "storeCode": action.payload.storeCode,
        "label": action.payload.label

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.configCreateSuccess(response.data.data));
        yield put(actions.getFilterSectionRequest('data'))
      } else if (finalResponse.failure) {
        yield put(actions.configCreateError(response.data));
      }
    } catch (e) {
      yield put(actions.configCreateError("error occurs"));
      console.warn('Some error found in "configCreate" action\n', e);
    }
  }
}
export function* getJobNameRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getJobNameClear());
  } else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/get/job/name`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getJobNameSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getJobNameError(response.data));
      }
    } catch (e) {
      yield put(actions.getJobNameError("error occurs"));
      console.warn('Some error found in "getJobName" action\n', e);
    }
  }
}


export function* glueParameterRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.glueParameterClear());
  } else {
    try {
      var { orgIdGlobal } = OganisationIdName()
      var { orgNameGlobal } = OganisationIdName()
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/glue/parameter/map`, {
        "jobName": action.payload.jobName,
        "configName": action.payload.configName,
        "orgId": orgIdGlobal,
        "orgName": orgNameGlobal,
        "isEnabled": action.payload.isEnabled,
        "isAssigned": action.payload.isAssigned

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.glueParameterSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.glueParameterError(response.data));
      }
    } catch (e) {
      yield put(actions.glueParameterError("error occurs"));
      console.warn('Some error found in "glueParameter" action\n', e);
    }
  }
}

// ____________________________________GET ALL JOB API__________________________________
export function* getAllJobRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getAllJobClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/rule/engine/get/all/job`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getAllJobSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAllJobError(response.data));
      }
    } catch (e) {
      yield put(actions.getAllJobError("error occurs"));
      console.warn('Some error found in "getAllJob" action\n', e);
    }
  }
}

// ______________________________AUTOCONFIG AND RUN ON DEMAND FILTER API ___________________________________


export function* getConfigFilterRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getConfigFilterClear());
  } else {
    try {
      let jobName = action.payload
      // const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/get/config?jobName=${jobName}`, {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV_CONFIG}/get/config-detail?configName=${action.payload}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getConfigFilterSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getConfigFilterError(response.data));
      }
    } catch (e) {
      yield put(actions.getConfigFilterError("error occurs"));
      console.warn('Some error found in "getConfigFilter" action\n', e);
    }
  }
}

//Vendor portal in vendor side Inspection ON OFF::
export function* getInspOnOffConfigRequest(action) {
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/find/qcinvoice`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getInspOnOffConfigSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getInspOnOffConfigError(response.data));
    }
  } catch (e) {
    yield put(actions.getInspOnOffConfigError("error occurs"));
    console.warn('Some error found in "getInspOnOffConfigRequest" action\n', e);
  }
}


export function* downloadRepReportRequest(action) {
  if (action.payload == undefined) {
      console.log("downloadRepReportRequest if");
      yield put(actions.downloadRepReportClear());
  }
  else
  {
      console.log("downloadRepReportRequest else");
      try {
          const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=${action.payload.module}&isAllData=false&isOnlyCurrentPage=false`, action.payload.data);

          const finalResponse = script(response);
          if (finalResponse.success) {
              yield put(actions.downloadRepReportSuccess(response.data.data));
          }
          else if (finalResponse.failure) {
              yield put(actions.downloadRepReportError(response.data));
          }
      }
      catch (e) {
          yield put(actions.downloadRepReportError("error occured"));
          console.warn('Some error found in "downloadRepReportRequest" action\n', e);
      }
  }
}
