import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';
let initialState = {
  createTrigger: {

    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  jobByName: {

    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  jobRunDetail: {

    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  runOnDemand: {

    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  stopOnDemand: {

    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  nschedule: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  jobHistory: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  jobHistoryFilter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createJob: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  allStoreCode: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  lastJob: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getAllocation: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getRequirementSummary: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  summaryDetail: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  summaryCsv: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getGrade: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getZone: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getStoreCode: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getPartner: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getCSVLink: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getXLSLink: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  toStatus: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  applyFilter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getHeaderConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getHeaderConfigExcel: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getCustomHeader: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createHeaderConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createMainHeaderConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createSetHeaderConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createItemHeaderConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  updateHeaderLogs: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getFiveTriggers: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getMotherComp: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deleteTrigger: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getFilterSection: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deleteConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  statusChange: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  invGetAllConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getJobData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateJobData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getArticleFmcg: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  configCreate: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getVendorFmcg: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getJobName: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getStoreFmcg: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  glueParameter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  applyFilterFmcg: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getAllJob: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  getConfigFilter: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false
  },
  customFilterMenu: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getExcelDownloadedPo: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getExcelDownloadedPi: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  inspectionOnOff: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getMainHeaderConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getSetHeaderConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getItemHeaderConfig: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  downloadRepReport: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
},

}
// ________________________________GET ALL JOB ______________________________________


const getAllJobRequest = (state, action) => update(state, {
  getAllJob: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllJobSuccess = (state, action) => update(state, {
  getAllJob: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get All Job success' }
  }
});

const getAllJobError = (state, action) => update(state, {
  getAllJob: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getAllJobClear = (state, action) => update(state, {
  getAllJob: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// ___________________________________

// _____________________________GET CONFIG FILTER ______________________________

const getConfigFilterRequest = (state, action) => update(state, {
  getConfigFilter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const getConfigFilterSuccess = (state, action) => update(state, {
  getConfigFilter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get Config Filter success' }
  }
});
const getConfigFilterError = (state, action) => update(state, {
  getConfigFilter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getConfigFilterClear = (state, action) => update(state, {
  getConfigFilter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// _______________________________________________________________________________

const createTriggerRequest = (state, action) => update(state, {
  createTrigger: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createTriggerSuccess = (state, action) => update(state, {
  createTrigger: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'create trigger success' }
  }
});

const createTriggerError = (state, action) => update(state, {
  createTrigger: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const createTriggerClear = (state, action) => update(state, {
  createTrigger: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getJobByNameRequest = (state, action) => update(state, {
  jobByName: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getJobByNameError = (state, action) => update(state, {
  jobByName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getJobByNameClear = (state, action) => update(state, {
  jobByName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getJobByNameSuccess = (state, action) => update(state, {
  jobByName: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get JOb by Name success' }
  }
});

const jobRunDetailRequest = (state, action) => update(state, {
  jobRunDetail: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const jobRunDetailError = (state, action) => update(state, {
  jobRunDetail: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const jobRunDetailSuccess = (state, action) => update(state, {
  jobRunDetail: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get jobRunDetail success' }
  }
});

const jobRunDetailClear = (state, action) => update(state, {
  jobRunDetail: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const runOnDemandRequest = (state, action) => update(state, {
  runOnDemand: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const runOnDemandClear = (state, action) => update(state, {
  runOnDemand: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const runOnDemandError = (state, action) => update(state, {
  runOnDemand: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const runOnDemandSuccess = (state, action) => update(state, {
  runOnDemand: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get runOnDemand success' }
  }
});

const stopOnDemandRequest = (state, action) => update(state, {
  stopOnDemand: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const stopOnDemandClear = (state, action) => update(state, {
  stopOnDemand: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const stopOnDemandError = (state, action) => update(state, {
  stopOnDemand: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const stopOnDemandSuccess = (state, action) => update(state, {
  stopOnDemand: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get stopOnDemand success' }
  }
});

const nscheduleRequest = (state, action) => update(state, {
  nschedule: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const nscheduleError = (state, action) => update(state, {
  nschedule: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const nscheduleSuccess = (state, action) => update(state, {
  nschedule: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get nschedule success' }
  }
});

const jobHistoryRequest = (state, action) => update(state, {
  jobHistory: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const jobHistoryError = (state, action) => update(state, {
  jobHistory: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const jobHistorySuccess = (state, action) => update(state, {
  jobHistory: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get jobHistory success' }
  }
});

const jobHistoryFilterRequest = (state, action) => update(state, {
  jobHistoryFilter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const jobHistoryFilterError = (state, action) => update(state, {
  jobHistoryFilter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const jobHistoryFilterSuccess = (state, action) => update(state, {
  jobHistoryFilter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get jobHistoryFilter success' }
  }
});

const createJobRequest = (state, action) => update(state, {
  createJob: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createJobError = (state, action) => update(state, {
  createJob: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const createJobSuccess = (state, action) => update(state, {
  createJob: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get createJob success' }
  }
});

const allStoreCodeRequest = (state, action) => update(state, {
  allStoreCode: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const allStoreCodeError = (state, action) => update(state, {
  allStoreCode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const allStoreCodeSuccess = (state, action) => update(state, {
  allStoreCode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get allStoreCode success' }
  }
});

const getAllocationRequest = (state, action) => update(state, {
  getAllocation: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const getAllocationError = (state, action) => update(state, {
  getAllocation: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getAllocationSuccess = (state, action) => update(state, {
  getAllocation: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get lastJob success' }
  }
});

const getAllocationClear = (state, action) => update(state, {
  getAllocation: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
})

const getRequirementSummaryRequest = (state, action) => update(state, {
  getRequirementSummary: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})

const getRequirementSummaryError = (state, action) => update(state, {
  getRequirementSummary: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getRequirementSummarySuccess = (state, action) => update(state, {
  getRequirementSummary: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get requirementSummary success' }
  }
});

const getRequirementSummaryClear = (state, action) => update(state, {
  getRequirementSummary: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
})

const lastJobRequest = (state, action) => update(state, {
  lastJob: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const lastJobError = (state, action) => update(state, {
  lastJob: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const lastJobSuccess = (state, action) => update(state, {
  lastJob: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get lastJob success' }
  }
});

const lastJobClear = (state, action) => update(state, {
  lastJob: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const summaryDetailRequest = (state, action) => update(state, {
  summaryDetail: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const summaryDetailClear = (state, action) => update(state, {
  summaryDetail: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const summaryDetailError = (state, action) => update(state, {
  summaryDetail: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const summaryDetailSuccess = (state, action) => update(state, {
  summaryDetail: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get summaryDetail success' }
  }
});

const summaryCsvRequest = (state, action) => update(state, {
  summaryCsv: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const summaryCsvError = (state, action) => update(state, {
  summaryCsv: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const summaryCsvSuccess = (state, action) => update(state, {
  summaryCsv: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get summaryCsv success' }
  }
});

// getStoreCode
const getStoreCodeRequest = (state, action) => update(state, {
  getStoreCode: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getStoreCodeError = (state, action) => update(state, {
  getStoreCode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getStoreCodeSuccess = (state, action) => update(state, {
  getStoreCode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get store code success' }
  }
});

const getStoreCodeClear = (state, action) => update(state, {
  getStoreCode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// getZone
const getZoneRequest = (state, action) => update(state, {
  getZone: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getZoneError = (state, action) => update(state, {
  getZone: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getZoneSuccess = (state, action) => update(state, {
  getZone: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get zone success' }
  }
});

const getZoneClear = (state, action) => update(state, {
  getZone: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//get partner 
const getPartnerRequest = (state, action) => update(state, {
  getPartner: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getPartnerError = (state, action) => update(state, {
  getPartner: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getPartnerSuccess = (state, action) => update(state, {
  getPartner: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get zone success' }
  }
});

const getPartnerClear = (state, action) => update(state, {
  getPartner: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// getGrade

const getGradeRequest = (state, action) => update(state, {
  getGrade: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getGradeError = (state, action) => update(state, {
  getGrade: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getGradeSuccess = (state, action) => update(state, {
  getGrade: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get grade success' }
  }
});

const getGradeClear = (state, action) => update(state, {
  getGrade: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getCSVLinkRequest = (state, action) => update(state, {
  getCSVLink: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getCSVLinkClear = (state, action) => update(state, {
  getCSVLink: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getCSVLinkError = (state, action) => update(state, {
  getCSVLink: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getCSVLinkSuccess = (state, action) => update(state, {
  getCSVLink: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get grade success' }
  }
});

const getXLSLinkRequest = (state, action) => update(state, {
  getXLSLink: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getXLSLinkError = (state, action) => update(state, {
  getXLSLink: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getXLSLinkSuccess = (state, action) => update(state, {
  getXLSLink: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get grade success' }
  }
});

const toStatusRequest = (state, action) => update(state, {
  toStatus: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const toStatusError = (state, action) => update(state, {
  toStatus: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const toStatusSuccess = (state, action) => update(state, {
  toStatus: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get toStatus' }
  }
});


const applyFilterRequest = (state, action) => update(state, {
  applyFilter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const applyFilterClear = (state, action) => update(state, {
  applyFilter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const applyFilterError = (state, action) => update(state, {
  applyFilter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const applyFilterSuccess = (state, action) => update(state, {
  applyFilter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'get applyFilter' }
  }
});

//replenishment
const getHeaderConfigRequest = (state, action) => update(state, {
  getHeaderConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getHeaderConfigClear = (state, action) => update(state, {
  getHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getHeaderConfigError = (state, action) => update(state, {
  getHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getHeaderConfigSuccess = (state, action) => update(state, {
  getHeaderConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getHeaderConfig' }
  }
});


//replenishment
const getHeaderConfigExcelRequest = (state, action) => update(state, {
  getHeaderConfigExcel: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getHeaderConfigExcelClear = (state, action) => update(state, {
  getHeaderConfigExcel: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getHeaderConfigExcelSuccess = (state, action) => update(state, {
  getHeaderConfigExcel: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: true },
    isError: { $set: false },
    message: { $set: action.payload }
  }
});


const getHeaderConfigExcelError = (state, action) => update(state, {
  getHeaderConfigExcel: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: ' getHeaderConfig' }
  }
});



const getCustomHeaderRequest = (state, action) => update(state, {
  getCustomHeader: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getCustomHeaderClear = (state, action) => update(state, {
  getCustomHeader: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getCustomHeaderError = (state, action) => update(state, {
  getCustomHeader: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getCustomHeaderSuccess = (state, action) => update(state, {
  getCustomHeader: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getCustomHeader' }
  }
});



const createHeaderConfigRequest = (state, action) => update(state, {
  createHeaderConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createHeaderConfigClear = (state, action) => update(state, {
  createHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const createHeaderConfigError = (state, action) => update(state, {
  createHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const createHeaderConfigSuccess = (state, action) => update(state, {
  createHeaderConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' createHeaderConfig' }
  }
});

//createMainHeader
const createMainHeaderConfigRequest = (state, action) => update(state, {
  createMainHeaderConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createMainHeaderConfigClear = (state, action) => update(state, {
  createMainHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const createMainHeaderConfigError = (state, action) => update(state, {
  createMainHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const createMainHeaderConfigSuccess = (state, action) => update(state, {
  createMainHeaderConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' createMainHeaderConfig' }
  }
});

//CreateSetHeader
const createSetHeaderConfigRequest = (state, action) => update(state, {
  createSetHeaderConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createSetHeaderConfigClear = (state, action) => update(state, {
  createSetHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const createSetHeaderConfigError = (state, action) => update(state, {
  createSetHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const createSetHeaderConfigSuccess = (state, action) => update(state, {
  createSetHeaderConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' createSetHeaderConfig' }
  }
});

//CreateItemHeader
const createItemHeaderConfigRequest = (state, action) => update(state, {
  createItemHeaderConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createItemHeaderConfigClear = (state, action) => update(state, {
  createItemHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const createItemHeaderConfigError = (state, action) => update(state, {
  createItemHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const createItemHeaderConfigSuccess = (state, action) => update(state, {
  createItemHeaderConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' createItemHeaderConfig' }
  }
});




const updateHeaderLogsRequest = (state, action) => update(state, {
  updateHeaderLogs: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateHeaderLogsClear = (state, action) => update(state, {
  updateHeaderLogs: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const updateHeaderLogsError = (state, action) => update(state, {
  updateHeaderLogs: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const updateHeaderLogsSuccess = (state, action) => update(state, {
  updateHeaderLogs: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' createHeaderConfig' }
  }
});



const getFiveTriggersRequest = (state, action) => update(state, {
  getFiveTriggers: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const getMotherCompRequest = (state, action) => update(state, {
  getMotherComp: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getFiveTriggersClear = (state, action) => update(state, {
  getFiveTriggers: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const getMotherCompClear = (state, action) => update(state, {
  getMotherComp: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getFiveTriggersError = (state, action) => update(state, {
  getFiveTriggers: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getMotherCompError = (state, action) => update(state, {
  getMotherComp: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getFiveTriggersSuccess = (state, action) => update(state, {
  getFiveTriggers: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getFiveTriggers' }
  }
});
const getMotherCompSuccess = (state, action) => update(state, {
  getMotherComp: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getMotherComp' }
  }
});




const deleteTriggerRequest = (state, action) => update(state, {
  deleteTrigger: {

    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const getArticleFmcgRequest = (state, action) => update(state, {
  getArticleFmcg: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getArticleFmcgClear = (state, action) => update(state, {
  getArticleFmcg: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getArticleFmcgError = (state, action) => update(state, {
  getArticleFmcg: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getArticleFmcgSuccess = (state, action) => update(state, {
  getArticleFmcg: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getArticleFmcg' }
  }
});
const getVendorFmcgRequest = (state, action) => update(state, {
  getVendorFmcg: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteTriggerClear = (state, action) => update(state, {
  deleteTrigger: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const getVendorFmcgClear = (state, action) => update(state, {
  getVendorFmcg: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const deleteTriggerError = (state, action) => update(state, {
  deleteTrigger: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getVendorFmcgError = (state, action) => update(state, {
  getVendorFmcg: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const deleteTriggerSuccess = (state, action) => update(state, {
  deleteTrigger: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' deleteTrigger' }
  }
});
const getVendorFmcgSuccess = (state, action) => update(state, {
  getVendorFmcg: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getVendorFmcg' }
  }
});

const getStoreFmcgRequest = (state, action) => update(state, {
  getStoreFmcg: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getStoreFmcgClear = (state, action) => update(state, {
  getStoreFmcg: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getStoreFmcgError = (state, action) => update(state, {
  getStoreFmcg: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getFilterSectionRequest = (state, action) => update(state, {
  getFilterSection: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getFilterSectionClear = (state, action) => update(state, {
  getFilterSection: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getFilterSectionError = (state, action) => update(state, {
  getFilterSection: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getFilterSectionSuccess = (state, action) => update(state, {
  getFilterSection: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getFilterSection' }
  }
});


const deleteConfigRequest = (state, action) => update(state, {
  deleteConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteConfigClear = (state, action) => update(state, {
  deleteConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const deleteConfigError = (state, action) => update(state, {
  deleteConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const deleteConfigSuccess = (state, action) => update(state, {
  deleteConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' deleteConfig' }
  }
});



const statusChangeRequest = (state, action) => update(state, {
  statusChange: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const statusChangeClear = (state, action) => update(state, {
  statusChange: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const statusChangeError = (state, action) => update(state, {
  statusChange: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const statusChangeSuccess = (state, action) => update(state, {
  statusChange: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' statusChange' }
  }
});
const invGetAllConfigRequest = (state, action) => update(state, {
  invGetAllConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const invGetAllConfigClear = (state, action) => update(state, {
  invGetAllConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const invGetAllConfigError = (state, action) => update(state, {
  invGetAllConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const invGetAllConfigSuccess = (state, action) => update(state, {
  invGetAllConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' invGetAllConfig' }
  }
});




const getJobDataRequest = (state, action) => update(state, {
  getJobData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getJobDataClear = (state, action) => update(state, {
  getJobData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getJobDataError = (state, action) => update(state, {
  getJobData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getJobDataSuccess = (state, action) => update(state, {
  getJobData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getJobData' }
  }
});

const updateJobDataRequest = (state, action) => update(state, {
  updateJobData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateJobDataClear = (state, action) => update(state, {
  updateJobData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const updateJobDataError = (state, action) => update(state, {
  updateJobData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const updateJobDataSuccess = (state, action) => update(state, {
  updateJobData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getJobData' }
  }
});

const configCreateRequest = (state, action) => update(state, {
  configCreate: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const configCreateClear = (state, action) => update(state, {
  configCreate: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const configCreateError = (state, action) => update(state, {
  configCreate: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const configCreateSuccess = (state, action) => update(state, {
  configCreate: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' configCreate' }
  }
});



const getJobNameRequest = (state, action) => update(state, {
  getJobName: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getJobNameClear = (state, action) => update(state, {
  getJobName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getJobNameError = (state, action) => update(state, {
  getJobName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const getJobNameSuccess = (state, action) => update(state, {
  getJobName: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getJobName' }
  }
});
const glueParameterRequest = (state, action) => update(state, {
  glueParameter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const glueParameterClear = (state, action) => update(state, {
  glueParameter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const glueParameterError = (state, action) => update(state, {
  glueParameter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const glueParameterSuccess = (state, action) => update(state, {
  glueParameter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' glueParameter' }
  }
});


const getStoreFmcgSuccess = (state, action) => update(state, {
  getStoreFmcg: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getStoreFmcg' }
  }
});



const applyFilterFmcgRequest = (state, action) => update(state, {
  applyFilterFmcg: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const applyFilterFmcgClear = (state, action) => update(state, {
  applyFilterFmcg: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const applyFilterFmcgError = (state, action) => update(state, {
  applyFilterFmcg: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const applyFilterFmcgSuccess = (state, action) => update(state, {
  applyFilterFmcg: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' applyFilterFmcg' }
  }
});


const customFilterMenuRequest = (state, action) => update(state, {
  customFilterMenu: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const customFilterMenuClear = (state, action) => update(state, {
  customFilterMenu: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const customFilterMenuError = (state, action) => update(state, {
  customFilterMenu: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const customFilterMenuSuccess = (state, action) => update(state, {
  customFilterMenu: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' customFilterMenu' }
  }
});


// Vendor portal vendor side Inspection ON OFF::
const getInspOnOffConfigRequest = (state, action) => update(state, {
  inspectionOnOff: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getInspOnOffConfigClear = (state, action) => update(state, {
  inspectionOnOff: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getInspOnOffConfigError = (state, action) => update(state, {
  inspectionOnOff: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getInspOnOffConfigSuccess = (state, action) => update(state, {
  inspectionOnOff: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'success' }
  }
});
//Header Section new Design
const getMainHeaderConfigRequest = (state, action) => update(state, {
  getMainHeaderConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getMainHeaderConfigClear = (state, action) => update(state, {
  getMainHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getMainHeaderConfigError = (state, action) => update(state, {
  getMainHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getMainHeaderConfigSuccess = (state, action) => update(state, {
  getMainHeaderConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getMainHeaderConfig' }
  }
});

const getSetHeaderConfigRequest = (state, action) => update(state, {
  getSetHeaderConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getSetHeaderConfigClear = (state, action) => update(state, {
  getSetHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getSetHeaderConfigError = (state, action) => update(state, {
  getSetHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getSetHeaderConfigSuccess = (state, action) => update(state, {
  getSetHeaderConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getSetHeaderConfig' }
  }
});

const getItemHeaderConfigRequest = (state, action) => update(state, {
  getItemHeaderConfig: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getItemHeaderConfigClear = (state, action) => update(state, {
  getItemHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getItemHeaderConfigError = (state, action) => update(state, {
  getItemHeaderConfig: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getItemHeaderConfigSuccess = (state, action) => update(state, {
  getItemHeaderConfig: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: ' getItemHeaderConfig' }
  }
});


//------------------------- DOWNLOAD REPORT -------------------------

const downloadRepReportRequest = (state, action) => update(state, {
  downloadRepReport: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false},
      message: { $set: '' }
  }
});

const downloadRepReportSuccess = (state, action) => update(state, {
  downloadRepReport: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: 'download report success' }
  }
});

const downloadRepReportError = (state, action) => update(state, {
  downloadRepReport: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
  }
});

const downloadRepReportClear = (state, action) => update(state, {
  downloadRepReport: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: '' }
  }
});


export default handleActions({
  [constants.CREATE_TRIGGER_REQUEST]: createTriggerRequest,
  [constants.CREATE_TRIGGER_SUCCESS]: createTriggerSuccess,
  [constants.CREATE_TRIGGER_ERROR]: createTriggerError,
  [constants.CREATE_TRIGGER_CLEAR]: createTriggerClear,

  [constants.GET_JOB_BY_NAME_REQUEST]: getJobByNameRequest,
  [constants.GET_JOB_BY_NAME_SUCCESS]: getJobByNameSuccess,
  [constants.GET_JOB_BY_NAME_ERROR]: getJobByNameError,
  [constants.GET_JOB_BY_NAME_CLEAR]: getJobByNameClear,

  [constants.JOB_RUN_DETAIL_REQUEST]: jobRunDetailRequest,
  [constants.JOB_RUN_DETAIL_SUCCESS]: jobRunDetailSuccess,
  [constants.JOB_RUN_DETAIL_ERROR]: jobRunDetailError,
  [constants.JOB_RUN_DETAIL_CLEAR]: jobRunDetailClear,

  [constants.RUN_ON_DEMAND_REQUEST]: runOnDemandRequest,
  [constants.RUN_ON_DEMAND_CLEAR]: runOnDemandClear,
  [constants.RUN_ON_DEMAND_SUCCESS]: runOnDemandSuccess,
  [constants.RUN_ON_DEMAND_ERROR]: runOnDemandError,

  [constants.STOP_ON_DEMAND_REQUEST]: stopOnDemandRequest,
  [constants.STOP_ON_DEMAND_CLEAR]: stopOnDemandClear,
  [constants.STOP_ON_DEMAND_SUCCESS]: stopOnDemandSuccess,
  [constants.STOP_ON_DEMAND_ERROR]: stopOnDemandError,

  [constants.NSCHEDULE_REQUEST]: nscheduleRequest,
  [constants.NSCHEDULE_SUCCESS]: nscheduleSuccess,
  [constants.NSCHEDULE_ERROR]: nscheduleError,

  [constants.JOB_HISTORY_REQUEST]: jobHistoryRequest,
  [constants.JOB_HISTORY_SUCCESS]: jobHistorySuccess,
  [constants.JOB_HISTORY_ERROR]: jobHistoryError,

  [constants.JOB_HISTORY_FILTER_REQUEST]: jobHistoryFilterRequest,
  [constants.JOB_HISTORY_FILTER_SUCCESS]: jobHistoryFilterSuccess,
  [constants.JOB_HISTORY_FILTER_ERROR]: jobHistoryFilterError,

  [constants.CREATE_JOB_REQUEST]: createJobRequest,
  [constants.CREATE_JOB_SUCCESS]: createJobSuccess,
  [constants.CREATE_JOB_ERROR]: createJobError,

  [constants.ALL_STORE_CODE_REQUEST]: allStoreCodeRequest,
  [constants.ALL_STORE_CODE_SUCCESS]: allStoreCodeSuccess,
  [constants.ALL_STORE_CODE_ERROR]: allStoreCodeError,


  [constants.LAST_JOB_REQUEST]: lastJobRequest,
  [constants.LAST_JOB_SUCCESS]: lastJobSuccess,
  [constants.LAST_JOB_ERROR]: lastJobError,
  [constants.LAST_JOB_CLEAR]: lastJobClear,

  [constants.GET_ALLOCATION_REQUEST]: getAllocationRequest,
  [constants.GET_ALLOCATION_CLEAR]: getAllocationClear,
  [constants.GET_ALLOCATION_SUCCESS]: getAllocationSuccess,
  [constants.GET_ALLOCATION_ERROR]: getAllocationError,

  [constants.GET_REQUIREMENT_SUMMARY_REQUEST]: getRequirementSummaryRequest,
  [constants.GET_REQUIREMENT_SUMMARY_CLEAR]: getRequirementSummaryClear,
  [constants.GET_REQUIREMENT_SUMMARY_SUCCESS]: getRequirementSummarySuccess,
  [constants.GET_REQUIREMENT_SUMMARY_ERROR]: getRequirementSummaryError,

  [constants.SUMMARY_DETAIL_REQUEST]: summaryDetailRequest,
  [constants.SUMMARY_DETAIL_CLEAR]: summaryDetailClear,
  [constants.SUMMARY_DETAIL_SUCCESS]: summaryDetailSuccess,
  [constants.SUMMARY_DETAIL_ERROR]: summaryDetailError,


  [constants.SUMMARY_CSV_REQUEST]: summaryCsvRequest,
  [constants.SUMMARY_CSV_SUCCESS]: summaryCsvSuccess,
  [constants.SUMMARY_CSV_ERROR]: summaryCsvError,


  [constants.GETPARTNER_REQUEST]: getPartnerRequest,
  [constants.GETPARTNER_SUCCESS]: getPartnerSuccess,
  [constants.GETPARTNER_CLEAR]: getPartnerClear,
  [constants.GETPARTNER_ERROR]: getPartnerError,

  [constants.GETZONE_REQUEST]: getZoneRequest,
  [constants.GETZONE_SUCCESS]: getZoneSuccess,
  [constants.GETZONE_CLEAR]: getZoneClear,
  [constants.GETZONE_ERROR]: getZoneError,

  [constants.GETSTORECODE_REQUEST]: getStoreCodeRequest,
  [constants.GETSTORECODE_SUCCESS]: getStoreCodeSuccess,
  [constants.GETSTORECODE_CLEAR]: getStoreCodeClear,
  [constants.GETSTORECODE_ERROR]: getStoreCodeError,

  [constants.GETGRADE_REQUEST]: getGradeRequest,
  [constants.GETGRADE_CLEAR]: getGradeClear,
  [constants.GETGRADE_SUCCESS]: getGradeSuccess,
  [constants.GETGRADE_ERROR]: getGradeError,

  [constants.GETCSVLINK_REQUEST]: getCSVLinkRequest,
  [constants.GETCSVLINK_SUCCESS]: getCSVLinkSuccess,
  [constants.GETCSVLINK_ERROR]: getCSVLinkError,
  [constants.GETCSVLINK_CLEAR]: getCSVLinkClear,

  [constants.GETXLSLINK_REQUEST]: getXLSLinkRequest,
  [constants.GETXLSLINK_SUCCESS]: getXLSLinkSuccess,
  [constants.GETXLSLINK_ERROR]: getXLSLinkError,

  [constants.TO_STATUS_REQUEST]: toStatusRequest,
  [constants.TO_STATUS_SUCCESS]: toStatusSuccess,
  [constants.TO_STATUS_ERROR]: toStatusError,

  [constants.APPLY_FILTER_REQUEST]: applyFilterRequest,
  [constants.APPLY_FILTER_CLEAR]: applyFilterClear,
  [constants.APPLY_FILTER_SUCCESS]: applyFilterSuccess,
  [constants.APPLY_FILTER_ERROR]: applyFilterError,

  [constants.GET_HEADER_CONFIG_REQUEST]: getHeaderConfigRequest,
  [constants.GET_HEADER_CONFIG_CLEAR]: getHeaderConfigClear,
  [constants.GET_HEADER_CONFIG_SUCCESS]: getHeaderConfigSuccess,
  [constants.GET_HEADER_CONFIG_ERROR]: getHeaderConfigError,

  [constants.GET_HEADER_CONFIG_EXCEL_REQUEST]: getHeaderConfigExcelRequest,
  [constants.GET_HEADER_CONFIG_EXCEL_CLEAR]: getHeaderConfigExcelClear,
  [constants.GET_HEADER_CONFIG_EXCEL_SUCCESS]: getHeaderConfigExcelSuccess,
  [constants.GET_HEADER_CONFIG_EXCEL_ERROR]: getHeaderConfigExcelError,

  [constants.GET_CUSTOM_HEADER_REQUEST]: getCustomHeaderRequest,
  [constants.GET_CUSTOM_HEADER_CLEAR]: getCustomHeaderClear,
  [constants.GET_CUSTOM_HEADER_SUCCESS]: getCustomHeaderSuccess,
  [constants.GET_CUSTOM_HEADER_ERROR]: getCustomHeaderError,

  [constants.CREATE_HEADER_CONFIG_REQUEST]: createHeaderConfigRequest,
  [constants.CREATE_HEADER_CONFIG_CLEAR]: createHeaderConfigClear,
  [constants.CREATE_HEADER_CONFIG_SUCCESS]: createHeaderConfigSuccess,
  [constants.CREATE_HEADER_CONFIG_ERROR]: createHeaderConfigError,

  //CreateMainHeader
  [constants.CREATE_MAIN_HEADER_CONFIG_REQUEST]: createMainHeaderConfigRequest,
  [constants.CREATE_MAIN_HEADER_CONFIG_CLEAR]: createMainHeaderConfigClear,
  [constants.CREATE_MAIN_HEADER_CONFIG_SUCCESS]: createMainHeaderConfigSuccess,
  [constants.CREATE_MAIN_HEADER_CONFIG_ERROR]: createMainHeaderConfigError,

  //CreateSetHeader
  [constants.CREATE_SET_HEADER_CONFIG_REQUEST]: createSetHeaderConfigRequest,
  [constants.CREATE_SET_HEADER_CONFIG_CLEAR]: createSetHeaderConfigClear,
  [constants.CREATE_SET_HEADER_CONFIG_SUCCESS]: createSetHeaderConfigSuccess,
  [constants.CREATE_SET_HEADER_CONFIG_ERROR]: createSetHeaderConfigError,

  //CreateItemHeader
  [constants.CREATE_ITEM_HEADER_CONFIG_REQUEST]: createItemHeaderConfigRequest,
  [constants.CREATE_ITEM_HEADER_CONFIG_CLEAR]: createItemHeaderConfigClear,
  [constants.CREATE_ITEM_HEADER_CONFIG_SUCCESS]: createItemHeaderConfigSuccess,
  [constants.CREATE_ITEM_HEADER_CONFIG_ERROR]: createItemHeaderConfigError,

  [constants.UPDATE_HEADER_LOGS_REQUEST]: updateHeaderLogsRequest,
  [constants.UPDATE_HEADER_LOGS_CLEAR]: updateHeaderLogsClear,
  [constants.UPDATE_HEADER_LOGS_SUCCESS]: updateHeaderLogsSuccess,
  [constants.UPDATE_HEADER_LOGS_ERROR]: updateHeaderLogsError,

  [constants.GET_FIVE_TRIGGERS_REQUEST]: getFiveTriggersRequest,
  [constants.GET_FIVE_TRIGGERS_CLEAR]: getFiveTriggersClear,
  [constants.GET_FIVE_TRIGGERS_SUCCESS]: getFiveTriggersSuccess,
  [constants.GET_FIVE_TRIGGERS_ERROR]: getFiveTriggersError,

  [constants.DELETE_TRIGGER_REQUEST]: deleteTriggerRequest,
  [constants.DELETE_TRIGGER_ERROR]: deleteTriggerError,
  [constants.DELETE_TRIGGER_SUCCESS]: deleteTriggerSuccess,
  [constants.DELETE_TRIGGER_CLEAR]: deleteTriggerClear,



  [constants.GET_FILTER_SECTION_REQUEST]: getFilterSectionRequest,
  [constants.GET_FILTER_SECTION_CLEAR]: getFilterSectionClear,
  [constants.GET_FILTER_SECTION_SUCCESS]: getFilterSectionSuccess,
  [constants.GET_FILTER_SECTION_ERROR]: getFilterSectionError,

  [constants.DELETE_CONFIG_REQUEST]: deleteConfigRequest,
  [constants.DELETE_CONFIG_CLEAR]: deleteConfigClear,
  [constants.DELETE_CONFIG_SUCCESS]: deleteConfigSuccess,
  [constants.DELETE_CONFIG_ERROR]: deleteConfigError,

  [constants.STATUS_CHANGE_REQUEST]: statusChangeRequest,
  [constants.STATUS_CHANGE_CLEAR]: statusChangeClear,
  [constants.STATUS_CHANGE_SUCCESS]: statusChangeSuccess,
  [constants.STATUS_CHANGE_ERROR]: statusChangeError,

  [constants.INV_GET_ALL_CONFIG_REQUEST]: invGetAllConfigRequest,
  [constants.INV_GET_ALL_CONFIG_CLEAR]: invGetAllConfigClear,
  [constants.INV_GET_ALL_CONFIG_ERROR]: invGetAllConfigError,
  [constants.INV_GET_ALL_CONFIG_SUCCESS]: invGetAllConfigSuccess,


  [constants.GET_JOBDATA_REQUEST]: getJobDataRequest,
  [constants.GET_JOBDATA_CLEAR]: getJobDataClear,
  [constants.GET_JOBDATA_SUCCESS]: getJobDataSuccess,
  [constants.GET_JOBDATA_ERROR]: getJobDataError,

  [constants.UPDATE_JOBDATA_REQUEST]: updateJobDataRequest,
  [constants.UPDATE_JOBDATA_CLEAR]: updateJobDataClear,
  [constants.UPDATE_JOBDATA_SUCCESS]: updateJobDataSuccess,
  [constants.UPDATE_JOBDATA_ERROR]: updateJobDataError,

  [constants.CONFIG_CREATE_REQUEST]: configCreateRequest,
  [constants.CONFIG_CREATE_CLEAR]: configCreateClear,
  [constants.CONFIG_CREATE_SUCCESS]: configCreateSuccess,
  [constants.CONFIG_CREATE_ERROR]: configCreateError,

  [constants.GET_JOBNAME_REQUEST]: getJobNameRequest,
  [constants.GET_JOBNAME_CLEAR]: getJobNameClear,
  [constants.GET_JOBNAME_SUCCESS]: getJobNameSuccess,
  [constants.GET_JOBNAME_ERROR]: getJobNameError,

  [constants.GLUE_PARAMETER_REQUEST]: glueParameterRequest,
  [constants.GLUE_PARAMETER_CLEAR]: glueParameterClear,
  [constants.GLUE_PARAMETER_ERROR]: glueParameterError,
  [constants.GLUE_PARAMETER_SUCCESS]: glueParameterSuccess,

  [constants.GET_ALL_JOB_REQUEST]: getAllJobRequest,
  [constants.GET_ALL_JOB_CLEAR]: getAllJobClear,
  [constants.GET_ALL_JOB_ERROR]: getAllJobError,
  [constants.GET_ALL_JOB_SUCCESS]: getAllJobSuccess,

  [constants.GET_CONFIG_FILTER_REQUEST]: getConfigFilterRequest,
  [constants.GET_CONFIG_FILTER_SUCCESS]: getConfigFilterSuccess,
  [constants.GET_CONFIG_FILTER_ERROR]: getConfigFilterError,
  [constants.GET_CONFIG_FILTER_CLEAR]: getConfigFilterClear,
  [constants.GET_MOTHER_COMP_REQUEST]: getMotherCompRequest,
  [constants.GET_MOTHER_COMP_SUCCESS]: getMotherCompSuccess,
  [constants.GET_MOTHER_COMP_ERROR]: getMotherCompError,
  [constants.GET_MOTHER_COMP_CLEAR]: getMotherCompClear,

  [constants.GET_ARTICLE_FMCG_REQUEST]: getArticleFmcgRequest,
  [constants.GET_ARTICLE_FMCG_SUCCESS]: getArticleFmcgSuccess,
  [constants.GET_ARTICLE_FMCG_ERROR]: getArticleFmcgError,
  [constants.GET_ARTICLE_FMCG_CLEAR]: getArticleFmcgClear,


  [constants.GET_VENDOR_FMCG_REQUEST]: getVendorFmcgRequest,
  [constants.GET_VENDOR_FMCG_SUCCESS]: getVendorFmcgSuccess,
  [constants.GET_VENDOR_FMCG_ERROR]: getVendorFmcgError,
  [constants.GET_VENDOR_FMCG_CLEAR]: getVendorFmcgClear,

  [constants.GET_STORE_FMCG_REQUEST]: getStoreFmcgRequest,
  [constants.GET_STORE_FMCG_SUCCESS]: getStoreFmcgSuccess,
  [constants.GET_STORE_FMCG_ERROR]: getStoreFmcgError,
  [constants.GET_STORE_FMCG_CLEAR]: getStoreFmcgClear,

  [constants.APPLY_FILTER_FMCG_REQUEST]: applyFilterFmcgRequest,
  [constants.APPLY_FILTER_FMCG_SUCCESS]: applyFilterFmcgSuccess,
  [constants.APPLY_FILTER_FMCG_ERROR]: applyFilterFmcgError,
  [constants.APPLY_FILTER_FMCG_CLEAR]: applyFilterFmcgClear,

  [constants.CUSTOM_FILTER_MENU_REQUEST]: customFilterMenuRequest,
  [constants.CUSTOM_FILTER_MENU_SUCCESS]: customFilterMenuSuccess,
  [constants.CUSTOM_FILTER_MENU_ERROR]: customFilterMenuError,
  [constants.CUSTOM_FILTER_MENU_CLEAR]: customFilterMenuClear,

  [constants.INSP_ON_OFF_CONFIG_REQUEST]: getInspOnOffConfigRequest,
  [constants.INSP_ON_OFF_CONFIG_CLEAR]: getInspOnOffConfigClear,
  [constants.INSP_ON_OFF_CONFIG_SUCCESS]: getInspOnOffConfigSuccess,
  [constants.INSP_ON_OFF_CONFIG_ERROR]: getInspOnOffConfigError,

  [constants.GET_MAIN_HEADER_CONFIG_REQUEST]: getMainHeaderConfigRequest,
  [constants.GET_MAIN_HEADER_CONFIG_CLEAR]: getMainHeaderConfigClear,
  [constants.GET_MAIN_HEADER_CONFIG_SUCCESS]: getMainHeaderConfigSuccess,
  [constants.GET_MAIN_HEADER_CONFIG_ERROR]: getMainHeaderConfigError,

  [constants.GET_SET_HEADER_CONFIG_REQUEST]: getSetHeaderConfigRequest,
  [constants.GET_SET_HEADER_CONFIG_CLEAR]: getSetHeaderConfigClear,
  [constants.GET_SET_HEADER_CONFIG_SUCCESS]: getSetHeaderConfigSuccess,
  [constants.GET_SET_HEADER_CONFIG_ERROR]: getSetHeaderConfigError,

  [constants.GET_ITEM_HEADER_CONFIG_REQUEST]: getItemHeaderConfigRequest,
  [constants.GET_ITEM_HEADER_CONFIG_CLEAR]: getItemHeaderConfigClear,
  [constants.GET_ITEM_HEADER_CONFIG_SUCCESS]: getItemHeaderConfigSuccess,
  [constants.GET_ITEM_HEADER_CONFIG_ERROR]: getItemHeaderConfigError,

  [constants.DOWNLOAD_REP_REPORT_REQUEST]: downloadRepReportRequest,
  [constants.DOWNLOAD_REP_REPORT_SUCCESS]: downloadRepReportSuccess,
  [constants.DOWNLOAD_REP_REPORT_ERROR]: downloadRepReportError,
  [constants.DOWNLOAD_REP_REPORT_CLEAR]: downloadRepReportClear,


}, initialState);
