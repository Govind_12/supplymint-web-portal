import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
    getAllVendorShipmentShipped: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllVendorSsdetail: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllVendorTransitNDeliver: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllVendorSiDetail: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllVendorSupplier: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllVendorTransporter: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    lrCreate: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    updateDelivery: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    lrDetailsAuto: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getGrcItem: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    createGrc: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllGrc: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getGrcDetails: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllGateEntry: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    updateExtensionDate: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getButtonActiveConfig: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllLogTransPayReport: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    },
    getAllLogWhStockReport: {
        data: {},
        isSuccess: false,
        isLoading: false,
        isError: false,
        message: ''
    }
}


const getAllVendorShipmentShippedRequest = (state, action) => update(state, {
    getAllVendorShipmentShipped: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllVendorShipmentShippedError = (state, action) => update(state, {
    getAllVendorShipmentShipped: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllVendorShipmentShippedSuccess = (state, action) => update(state, {
    getAllVendorShipmentShipped: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllVendorShipmentShippedClear = (state, action) => update(state, {
    getAllVendorShipmentShipped: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})



const getAllVendorSsdetailRequest = (state, action) => update(state, {
    getAllVendorSsdetail: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllVendorSsdetailError = (state, action) => update(state, {
    getAllVendorSsdetail: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllVendorSsdetailSuccess = (state, action) => update(state, {
    getAllVendorSsdetail: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllVendorSsdetailClear = (state, action) => update(state, {
    getAllVendorSsdetail: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const getAllVendorTransitNDeliverRequest = (state, action) => update(state, {
    getAllVendorTransitNDeliver: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllVendorTransitNDeliverError = (state, action) => update(state, {
    getAllVendorTransitNDeliver: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllVendorTransitNDeliverSuccess = (state, action) => update(state, {
    getAllVendorTransitNDeliver: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllVendorTransitNDeliverClear = (state, action) => update(state, {
    getAllVendorTransitNDeliver: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const getAllVendorSiDetailRequest = (state, action) => update(state, {
    getAllVendorSiDetail: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllVendorSiDetailError = (state, action) => update(state, {
    getAllVendorSiDetail: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllVendorSiDetailSuccess = (state, action) => update(state, {
    getAllVendorSiDetail: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllVendorSiDetailClear = (state, action) => update(state, {
    getAllVendorSiDetail: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const getAllVendorSupplierRequest = (state, action) => update(state, {
    getAllVendorSupplier: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllVendorSupplierError = (state, action) => update(state, {
    getAllVendorSupplier: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllVendorSupplierSuccess = (state, action) => update(state, {
    getAllVendorSupplier: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllVendorSupplierClear = (state, action) => update(state, {
    getAllVendorSupplier: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const getAllVendorTransporterRequest = (state, action) => update(state, {
    getAllVendorTransporter: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllVendorTransporterError = (state, action) => update(state, {
    getAllVendorTransporter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllVendorTransporterSuccess = (state, action) => update(state, {
    getAllVendorTransporter: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllVendorTransporterClear = (state, action) => update(state, {
    getAllVendorTransporter: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const lrCreateRequest = (state, action) => update(state, {
    lrCreate: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const lrCreateError = (state, action) => update(state, {
    lrCreate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const lrCreateSuccess = (state, action) => update(state, {
    lrCreate: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const lrCreateClear = (state, action) => update(state, {
    lrCreate: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const updateDeliveryRequest = (state, action) => update(state, {
    updateDelivery: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateDeliveryError = (state, action) => update(state, {
    updateDelivery: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const updateDeliverySuccess = (state, action) => update(state, {
    updateDelivery: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const updateDeliveryClear = (state, action) => update(state, {
    updateDelivery: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


const lrDetailsAutoRequest = (state, action) => update(state, {
    lrDetailsAuto: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const lrDetailsAutoError = (state, action) => update(state, {
    lrDetailsAuto: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const lrDetailsAutoSuccess = (state, action) => update(state, {
    lrDetailsAuto: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const lrDetailsAutoClear = (state, action) => update(state, {
    lrDetailsAuto: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//Update Extension Date

const updateExtensionDateRequest = (state, action) => update(state, {
    updateExtensionDate: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateExtensionDateClear = (state, action) => update(state, {
    updateExtensionDate: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateExtensionDateError = (state, action) => update(state, {
    updateExtensionDate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        data: { $set: action.payload }
    }
});
const updateExtensionDateSuccess = (state, action) => update(state, {
    updateExtensionDate: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

// get GRC Item Detail
const getGrcItemRequest = (state, action) => update(state, {
    getGrcItem: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getGrcItemError = (state, action) => update(state, {
    getGrcItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getGrcItemSuccess = (state, action) => update(state, {
    getGrcItem: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getGrcItemClear = (state, action) => update(state, {
    getGrcItem: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// create GRC
const createGrcRequest = (state, action) => update(state, {
    createGrc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const createGrcError = (state, action) => update(state, {
    createGrc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const createGrcSuccess = (state, action) => update(state, {
    createGrc: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const createGrcClear = (state, action) => update(state, {
    createGrc: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


// get all GRC
const getAllGrcRequest = (state, action) => update(state, {
    getAllGrc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllGrcError = (state, action) => update(state, {
    getAllGrc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllGrcSuccess = (state, action) => update(state, {
    getAllGrc: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllGrcClear = (state, action) => update(state, {
    getAllGrc: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// get GRC Details
const getGrcDetailsRequest = (state, action) => update(state, {
    getGrcDetails: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getGrcDetailsError = (state, action) => update(state, {
    getGrcDetails: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getGrcDetailsSuccess = (state, action) => update(state, {
    getGrcDetails: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getGrcDetailsClear = (state, action) => update(state, {
    getGrcDetails: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// get all Gate entry
const getAllGateEntryRequest = (state, action) => update(state, {
    getAllGateEntry: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllGateEntryError = (state, action) => update(state, {
    getAllGateEntry: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllGateEntrySuccess = (state, action) => update(state, {
    getAllGateEntry: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllGateEntryClear = (state, action) => update(state, {
    getAllGateEntry: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// get status button active config::
const getStatusButtonActiveRequest = (state, action) => update(state, {
    getButtonActiveConfig: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getStatusButtonActiveError = (state, action) => update(state, {
    getButtonActiveConfig: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getStatusButtonActiveSuccess = (state, action) => update(state, {
    getButtonActiveConfig: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getStatusButtonActiveClear = (state, action) => update(state, {
    getButtonActiveConfig: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const getAllLogTransPayReportRequest = (state, action) => update(state, {
    getAllLogTransPayReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllLogTransPayReportError = (state, action) => update(state, {
    getAllLogTransPayReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllLogTransPayReportSuccess = (state, action) => update(state, {
    getAllLogTransPayReport: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllLogTransPayReportClear = (state, action) => update(state, {
    getAllLogTransPayReport: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const getAllLogWhStockReportRequest = (state, action) => update(state, {
    getAllLogWhStockReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllLogWhStockReportError = (state, action) => update(state, {
    getAllLogWhStockReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllLogWhStockReportSuccess = (state, action) => update(state, {
    getAllLogWhStockReport: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllLogWhStockReportClear = (state, action) => update(state, {
    getAllLogWhStockReport: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

export default handleActions({

    [constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_REQUEST]: getAllVendorShipmentShippedRequest,
    [constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_SUCCESS]: getAllVendorShipmentShippedSuccess,
    [constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_CLEAR]: getAllVendorShipmentShippedClear,
    [constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_ERROR]: getAllVendorShipmentShippedError,

    [constants.GET_ALL_VENDOR_SSDETAIL_REQUEST]: getAllVendorSsdetailRequest,
    [constants.GET_ALL_VENDOR_SSDETAIL_SUCCESS]: getAllVendorSsdetailSuccess,
    [constants.GET_ALL_VENDOR_SSDETAIL_CLEAR]: getAllVendorSsdetailClear,
    [constants.GET_ALL_VENDOR_SSDETAIL_ERROR]: getAllVendorSsdetailError,

    [constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_REQUEST]: getAllVendorTransitNDeliverRequest,
    [constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_SUCCESS]: getAllVendorTransitNDeliverSuccess,
    [constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_CLEAR]: getAllVendorTransitNDeliverClear,
    [constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_ERROR]: getAllVendorTransitNDeliverError,

    [constants.GET_ALL_VENDOR_SIDETAIL_REQUEST]: getAllVendorSiDetailRequest,
    [constants.GET_ALL_VENDOR_SIDETAIL_SUCCESS]: getAllVendorSiDetailSuccess,
    [constants.GET_ALL_VENDOR_SIDETAIL_CLEAR]: getAllVendorSiDetailClear,
    [constants.GET_ALL_VENDOR_SIDETAIL_ERROR]: getAllVendorSiDetailError,

    [constants.GET_ALL_VENDOR_SUPPLIER_REQUEST]: getAllVendorSupplierRequest,
    [constants.GET_ALL_VENDOR_SUPPLIER_SUCCESS]: getAllVendorSupplierSuccess,
    [constants.GET_ALL_VENDOR_SUPPLIER_CLEAR]: getAllVendorSupplierClear,
    [constants.GET_ALL_VENDOR_SUPPLIER_ERROR]: getAllVendorSupplierError,

    [constants.GET_ALL_VENDOR_TRANSPORTER_REQUEST]: getAllVendorTransporterRequest,
    [constants.GET_ALL_VENDOR_TRANSPORTER_SUCCESS]: getAllVendorTransporterSuccess,
    [constants.GET_ALL_VENDOR_TRANSPORTER_CLEAR]: getAllVendorTransporterClear,
    [constants.GET_ALL_VENDOR_TRANSPORTER_ERROR]: getAllVendorTransporterError,

    [constants.LR_CREATE_REQUEST]: lrCreateRequest,
    [constants.LR_CREATE_ERROR]: lrCreateError,
    [constants.LR_CREATE_SUCCESS]: lrCreateSuccess,
    [constants.LR_CREATE_CLEAR]: lrCreateClear,

    [constants.UPDATE_DELIVERY_REQUEST]: updateDeliveryRequest,
    [constants.UPDATE_DELIVERY_ERROR]: updateDeliveryError,
    [constants.UPDATE_DELIVERY_SUCCESS]: updateDeliverySuccess,
    [constants.UPDATE_DELIVERY_CLEAR]: updateDeliveryClear,

    [constants.LR_DETAILS_AUTO_REQUEST]: lrDetailsAutoRequest,
    [constants.LR_DETAILS_AUTO_ERROR]: lrDetailsAutoError,
    [constants.LR_DETAILS_AUTO_SUCCESS]: lrDetailsAutoSuccess,
    [constants.LR_DETAILS_AUTO_CLEAR]: lrDetailsAutoClear,

    [constants.GET_GRC_ITEM_REQUEST]: getGrcItemRequest,
    [constants.GET_GRC_ITEM_ERROR]: getGrcItemError,
    [constants.GET_GRC_ITEM_SUCCESS]: getGrcItemSuccess,
    [constants.GET_GRC_ITEM_CLEAR]: getGrcItemClear,

    [constants.CREATE_GRC_REQUEST]: createGrcRequest,
    [constants.CREATE_GRC_ERROR]: createGrcError,
    [constants.CREATE_GRC_SUCCESS]: createGrcSuccess,
    [constants.CREATE_GRC_CLEAR]: createGrcClear,

    [constants.GET_ALL_GRC_REQUEST]: getAllGrcRequest,
    [constants.GET_ALL_GRC_ERROR]: getAllGrcError,
    [constants.GET_ALL_GRC_SUCCESS]: getAllGrcSuccess,
    [constants.GET_ALL_GRC_CLEAR]: getAllGrcClear,

    [constants.GET_GRC_DETAILS_REQUEST]: getGrcDetailsRequest,
    [constants.GET_GRC_DETAILS_ERROR]: getGrcDetailsError,
    [constants.GET_GRC_DETAILS_SUCCESS]: getGrcDetailsSuccess,
    [constants.GET_GRC_DETAILS_CLEAR]: getGrcDetailsClear,

    [constants.UPDATE_EXTENSION_DATE_REQUEST]: updateExtensionDateRequest,
    [constants.UPDATE_EXTENSION_DATE_ERROR]: updateExtensionDateError,
    [constants.UPDATE_EXTENSION_DATE_SUCCESS]: updateExtensionDateSuccess,
    [constants.UPDATE_EXTENSION_DATE_CLEAR]: updateExtensionDateClear,

    [constants.GET_ALL_GATE_ENTRY_REQUEST]: getAllGateEntryRequest,
    [constants.GET_ALL_GATE_ENTRY_ERROR]: getAllGateEntryError,
    [constants.GET_ALL_GATE_ENTRY_SUCCESS]: getAllGateEntrySuccess,
    [constants.GET_ALL_GATE_ENTRY_CLEAR]: getAllGateEntryClear,

    [constants.GET_STATUS_BUTTON_ACTIVE_REQUEST]: getStatusButtonActiveRequest,
    [constants.GET_STATUS_BUTTON_ACTIVE_ERROR]: getStatusButtonActiveError,
    [constants.GET_STATUS_BUTTON_ACTIVE_SUCCESS]: getStatusButtonActiveSuccess,
    [constants.GET_STATUS_BUTTON_ACTIVE_CLEAR]: getStatusButtonActiveClear,

    [constants.GET_ALL_LOG_TRANS_PAY_REPORT_REQUEST]: getAllLogTransPayReportRequest,
    [constants.GET_ALL_LOG_TRANS_PAY_REPORT_SUCCESS]: getAllLogTransPayReportSuccess,
    [constants.GET_ALL_LOG_TRANS_PAY_REPORT_CLEAR]: getAllLogTransPayReportClear,
    [constants.GET_ALL_LOG_TRANS_PAY_REPORT_ERROR]: getAllLogTransPayReportError,

    [constants.GET_ALL_LOG_WH_STOCK_REPORT_REQUEST]: getAllLogWhStockReportRequest,
    [constants.GET_ALL_LOG_WH_STOCK_REPORT_SUCCESS]: getAllLogWhStockReportSuccess,
    [constants.GET_ALL_LOG_WH_STOCK_REPORT_CLEAR]: getAllLogWhStockReportClear,
    [constants.GET_ALL_LOG_WH_STOCK_REPORT_ERROR]: getAllLogWhStockReportError,

}, initialState)