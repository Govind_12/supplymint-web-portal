import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import moment from 'moment';
import { OganisationIdName } from '../../../organisationIdName.js';

export function* getAllVendorShipmentShippedRequest(action) {
    try {
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;
        //let vendorCode = sessionStorage.getItem('vendorCode') == undefined ? "" : sessionStorage.getItem('vendorCode');
        let get = {
                pageNo: action.payload.no == undefined ? 1 : action.payload.no,
                type: action.payload.type == undefined ? 1 : action.payload.type,
                search: action.payload.search == undefined ? "" : action.payload.search,
                status: action.payload.status == undefined ? "" : action.payload.status,
                isLRCreation: action.payload.isLRCreation == undefined ? "" : action.payload.isLRCreation,
                vendorName: action.payload.vendorName == undefined ? "" : action.payload.vendorName,
                transporter: action.payload.transporter == undefined ? "" : action.payload.transporter,
                siteDetail: action.payload.siteDetail == undefined ? "" : action.payload.siteDetail,
                sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
                sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
                isDashboardComment: action.payload.isDashboardComment,
        }
       
        let filter = {};
        //if( action.payload.filter != undefined && action.payload.filter.vendorCode != undefined ){
            filter = { ...action.payload.filter }
        // }else{
        //     filter = { ...action.payload.filter, vendorCode }
        // }
        let final = { ...get, filter }

        // let invoiceNumber = action.payload.invoiceNumber == undefined ? "" : action.payload.invoiceNumber;
        // let invoiceAmount = action.payload.invoiceAmount == undefined ? "" : action.payload.invoiceAmount;
        // let unitCount = action.payload.unitCount == undefined ? "" : action.payload.unitCount;
        // let invoiceDate = action.payload.invoiceDate == undefined ? "" : action.payload.invoiceDate;

        // let url =""
        // if(userType=="vendorlogi"){
        //     url = `get/all/ss?pageNo=${no}&type=${type}&status=${status}&isLRCreation=${isLRCreation}&search=${search}&shipmentRequestDate=${shipmentRequestDate}&poNumber=${poNumber}&vendorName=${vendorName}&transporter=${transporter}&siteDetail=${siteDetail}&adviceNo=${adviceNo}&adviceDate=${adviceDate}&poDate=${poDate}&shipConfirmDate=${shipmentConfirmDate}&dueDate=${dueDate}&totalQty=${totalQty}`
        // }else if(userType=="entlogi") {
        //     url= `get/all/ss?pageNo=${no}&search=${search}&vendorName=${vendorName}&poNumber=${poNumber}&type=${type}&status=${status}&shipmentRequestDate=${shipmentRequestDate}&isLRCreation=${isLRCreation}&transporter=${transporter}&siteDetail=${siteDetail}&shipmentAdviceCode=${shipmentAdviceCode}&shipmentDate=${shipmentDate}&poDate=${poDate}&shipmentConfirmDate=${shipmentConfirmDate}&dueInDays=${dueInDays}&totalQty=${totalQty}`
        // } 
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/all/ss`, final
        );
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllVendorShipmentShippedSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllVendorShipmentShippedError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllVendorShipmentShippedError("error occurs"));
        console.warn('Some error found in "getAllVendorShipmentShipped" action\n', e);
    }
}


export function* getAllVendorSsdetailRequest(action) {
    try {
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;
        let orderId = action.payload.orderId == undefined ? "" : action.payload.orderId
        let setHeaderId = action.payload.setHeaderId == undefined ? "" : action.payload.setHeaderId
        let poType = action.payload.poType == undefined ? "" : action.payload.poType
        let detailType = action.payload.detailType == undefined ? "" : action.payload.detailType;
        let shipmentId = action.payload.shipmentId == undefined ? "" : action.payload.shipmentId;

        let basedOn = action.payload.basedOn == undefined ? "" : action.payload.basedOn;


        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/ssDetail`, {
            orderId,
            setHeaderId,
            detailType,
            poType,
            shipmentId
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            response.data.data.basedOn = basedOn
            yield put(actions.getAllVendorSsdetailSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllVendorSsdetailError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllVendorSsdetailError("error occurs"));
        console.warn('Some error found in "getAllVendorSsdetail" action\n', e);
    }
}
export function* getAllVendorTransitNDeliverRequest(action) {
    try {
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;
        //let vendorCode = sessionStorage.getItem('vendorCode') == undefined ? "" : sessionStorage.getItem('vendorCode');
        let get = {
                pageNo: action.payload.no == undefined ? 1 : action.payload.no,
                type: action.payload.type == undefined ? 1 : action.payload.type,
                search: action.payload.search == undefined ? "" : action.payload.search,
                status: action.payload.status == undefined ? "" : action.payload.status,
                sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
                sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
                isDashboardComment: action.payload.isDashboardComment,
                isReportPage : action.payload.isReportPage == undefined ? 0 : action.payload.isReportPage,
        }
     
        let filter = {};
        //if( action.payload.filter != undefined && action.payload.filter.vendorCode != undefined ){
            filter = { ...action.payload.filter }
        // }else{
        //     filter = { ...action.payload.filter, vendorCode}
        // }
        let final = { ...get, filter }
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/all/lr`, final

        );
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllVendorTransitNDeliverSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllVendorTransitNDeliverError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllVendorTransitNDeliverError("error occurs"));
        console.warn('Some error found in "getAllVendorTransitNDeliver" action\n', e);
    }
}

export function* getAllGateEntryRequest(action) {
    try {
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;
        //let vendorCode = sessionStorage.getItem('vendorCode') == undefined ? "" : sessionStorage.getItem('vendorCode');
        let get = {
            pageNo: action.payload.no == undefined ? 1 : action.payload.no,
            type: action.payload.type == undefined ? 1 : action.payload.type,
            search: action.payload.search == undefined ? "" : action.payload.search,
            status: action.payload.status == undefined ? "" : action.payload.status,
            sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
            sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
            isDashboardComment: action.payload.isDashboardComment,
        }
        let filter = {};
        //if( action.payload.filter != undefined && action.payload.filter.vendorCode != undefined ){
            filter = { ...action.payload.filter };
        // } else {
        //     filter = {...action.payload.filter, vendorCode };
        // } 
        let final = { ...get, filter }

        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/all/gate/entry`, final

        );
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllGateEntrySuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllGateEntryError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllGateEntryError("error occurs"));
        console.warn('Some error found in "getAllGateEntry" action\n', e);
    }
}

export function* getAllVendorSiDetailRequest(action) {
    try {
        let type = action.payload.type == undefined ? 1 : action.payload.type;
        let search = action.payload.search == undefined ? "" : action.payload.search;
        let status = action.payload.status == undefined ? "" : action.payload.status;
        let lgtNumber = action.payload.lgtNumber == undefined ? "" : action.payload.lgtNumber
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;
        let no = action.payload.no == undefined ? 1 : action.payload.no;

        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/siDetails`, {
            type: type,
            search: search,
            status: status,
            lgtNumber: lgtNumber,
            pageNo: no
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllVendorSiDetailSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllVendorSiDetailError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllVendorSiDetailError("error occurs"));
        console.warn('Some error found in "getAllVendorSiDetail" action\n', e);
    }
}
export function* getAllVendorSupplierRequest(action) {
    try {
        let no = action.payload.no == undefined ? 1 : action.payload.no;

        let type = action.payload.type == undefined ? 1 : action.payload.type;
        let search = action.payload.search == undefined ? "" : action.payload.search;
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;

        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/get/all/suppliers?type=${type}&pageNo=${no}&search=${search}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllVendorSupplierSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllVendorSupplierError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllVendorSupplierError("error occurs"));
        console.warn('Some error found in "getAllVendorSupplier" action\n', e);
    }
}
export function* getAllVendorTransporterRequest(action) {
    try {
        let no = action.payload.no == undefined ? 1 : action.payload.no;

        let type = action.payload.type == undefined ? 1 : action.payload.type;
        let search = action.payload.search == undefined ? "" : action.payload.search;
        let slCode = action.payload.slCode == undefined ? "" : action.payload.slCode;
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;

        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/get/all/transporters?type=${type}&pageNo=${no}&search=${search}&slCode=${slCode}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllVendorTransporterSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllVendorTransporterError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllVendorTransporterError("error occurs"));
        console.warn('Some error found in "getAllVendorTransporter" action\n', e);
    }
}

export function* lrCreateRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorlogi/lr/create`, {
            "logisticNo": action.payload.logisticNo,
            "logisticDate": moment(action.payload.logisticDate).format("YYYY-MM-DD") + "T00:00+05:30",
            "lrNo": action.payload.lrNo,
            "lrDate": moment(action.payload.lrDate).format("YYYY-MM-DD") + "T00:00+05:30",
            "transporterMode": action.payload.transporterMode,
            "stationFrom": action.payload.stationFrom,
            "stationTo": action.payload.stationTo,
            "vehicleNo": action.payload.vehicleNo,
            "policyNo": action.payload.policyNo,
            "declarationAmount": action.payload.declarationAmount,
            "permitNo": action.payload.permitNo,
            "issuedOn": moment(action.payload.issuedOn).format("YYYY-MM-DD") + "T00:00+05:30",
            "ownerSite": action.payload.ownerSite,
            "consignorName": action.payload.consignorName,
            "transporterCode": action.payload.transporterCode,
            "transporterName": action.payload.transporterName,
            "lrPcs": action.payload.lrPcs,
            "lrBales": action.payload.lrBales,
            "lrWeight": action.payload.lrWeight,
            "lrType": action.payload.lrType,
            "lrRate": action.payload.lrRate,
            "lrFreight": action.payload.lrFreight,
            "consigneeName": action.payload.consigneeName,
            "vehicleType": action.payload.vehicleType,
            "lrDetailList": action.payload.lrDetailList,
            "lrETADate": action.payload.lrETADate,
            "courier": action.payload.courier,
            "consignmentNo": action.payload.consignmentNo
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.lrCreateSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.lrCreateError(response.data));
        }

    } catch (e) {
        yield put(actions.lrCreateError("error occurs"));
        console.warn('Some error found in "lrCreate" action\n', e);
    }
}
export function* updateDeliveryRequest(action) {
    try {

        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/entlogi/update/delivery`, {
            "updateDelivery": action.payload
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.updateDeliverySuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.updateDeliveryError(response.data));
        }

    } catch (e) {
        yield put(actions.updateDeliveryError("error occurs"));
        console.warn('Some error found in "updateDelivery" action\n', e);
    }
}

export function* lrDetailsAutoRequest(action) {
    try {
        let orderId = action.payload.orderId == undefined ? "" : action.payload.orderId;
        // let orderNumber = action.payload.orderNumber == undefined ? "" : action.payload.orderNumber;
        let shipmentId = action.payload.shipmentId == undefined ? "" : action.payload.shipmentId;

        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorlogi/find/lr/auto`, {
            "orderId": orderId,
            "shipmentId": shipmentId
            // "orderNumber": orderNumber,
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.lrDetailsAutoSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.lrDetailsAutoError(response.data));
        }
    } catch (e) {
        yield put(actions.lrDetailsAutoError("error occurs"));
        console.warn('Some error found in "lrDetailsAuto" action\n', e);
    }
}
// update Entension Date
export function* updateExtensionDateRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/update/date`,
            { shipmentUpdate: action.payload });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.updateExtensionDateSuccess(response.data ));
        } else if (finalResponse.failure) {
            yield put(actions.updateExtensionDateError(response.data));
        }
    } catch (e) {
        yield put(actions.updateExtensionDateError("error occurs"));
        console.warn('Some error found in "updateExtensionDate" action\n', e);
    }
}

// get GRC item
export function* getGrcItemRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/entlogi/find/grc/item`, {
            orderId: action.payload.poId,
            poType: action.payload.poType,
            saId: action.payload.saId,
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getGrcItemSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getGrcItemError(response.data));
        }
    } catch (e) {
        yield put(actions.getGrcItemError("error occurs"));
        console.warn('Some error found in "getGrcItem" action\n', e);
    }
}

// get GRC item
export function* createGrcRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/entlogi/grc/create`, action.payload);
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.createGrcSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.createGrcError(response.data));
        }
    } catch (e) {
        yield put(actions.createGrcError("error occurs"));
        console.warn('Some error found in "createGrc" action\n', e);
    }
}

// get all grc
// export function* getAllGrcRequest(action) {
//     if (action.payload == undefined) {
//       yield put(actions.piHistoryClear());
//     } else {
//       try {
//         var filter = action.payload.filter == undefined ? {} : action.payload.filter
//         const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/all/grc`,{
//         // const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/history`, {
       
//             pageNo: action.payload.no == undefined ? 1 : action.payload.no,
//             type: action.payload.type == undefined ? 1 : action.payload.type,
//             search: action.payload.search == undefined ? "" : action.payload.search,
//             status: action.payload.status == undefined ? "" : action.payload.status,
//             sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
//             sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
//           filter: { ...filter }
//         });

//         const finalResponse = script(response);
//         if (finalResponse.success) {
//             yield put(actions.getAllGrcSuccess(response.data.data));
//         } else if (finalResponse.failure) {
//             yield put(actions.getAllGrcError(response.data));
//         }
//     } catch (e) {
//         yield put(actions.getAllGrcError("error occurs"));
//         console.warn('Some error found in "getAllGrc" action\n', e);
//     }
// }


export function* getAllGrcRequest(action) {
    try {
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;
        //let vendorCode = sessionStorage.getItem('vendorCode') == undefined ? "" : sessionStorage.getItem('vendorCode');
        let request = {
            pageNo: action.payload.no == undefined ? 1 : action.payload.no,
            type: action.payload.type == undefined ? 1 : action.payload.type,
            search: action.payload.search == undefined ? "" : action.payload.search,
            status: action.payload.status == undefined ? "" : action.payload.status,
            sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
            sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
            isDashboardComment: action.payload.isDashboardComment,
        }
        let filter = {};
        // if( action.payload.filter != undefined && action.payload.filter.vendorCode != undefined ){
            filter = { ...action.payload.filter };
        // } else {
        //     filter = {...action.payload.filter, vendorCode };
        // }
        let final = { ...request, filter }
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/all/grc`, final);
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllGrcSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllGrcError(response.data));
        }
    } catch (e) {
        yield put(actions.getAllGrcError("error occurs"));
        console.warn('Some error found in "getAllGrc" action\n', e);
    }
}


// get grc detail
export function* getGrcDetailsRequest(action) {
    try {
        let userType = action.payload.userType == undefined ? "" : action.payload.userType;
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/grc/detail`, action.payload);
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getGrcDetailsSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getGrcDetailsError(response.data));
        }
    } catch (e) {
        yield put(actions.getGrcDetailsError("error occurs"));
        console.warn('Some error found in "getGrcDetails" action\n', e);
    }
}

// Get Status Button Active Config::
export function* getStatusButtonActiveRequest(action) {
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.SYSTEM_CONFIG}/get/active/key`);
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getStatusButtonActiveSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getStatusButtonActiveError(response.data));
        }
    } catch (e) {
        yield put(actions.getStatusButtonActiveError("error occurs"));
        console.warn('Some error found in "getStatusButtonActiveRequest" action\n', e);
    }
}

export function* getAllLogTransPayReportRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ent/analytics/find/all/logistic/transporter/payment/report`, action.payload);
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllLogTransPayReportSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllLogTransPayReportError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllLogTransPayReportError("error occurs"));
        console.warn('Some error found in "getAllLogTransPayReport" action\n', e);
    }
}

export function* getAllLogWhStockReportRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ent/analytics/logistics/warehouse/stock/report`, action.payload);
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAllLogWhStockReportSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAllLogWhStockReportError(response.data));
        }

    } catch (e) {
        yield put(actions.getAllLogWhStockReportError("error occurs"));
        console.warn('Some error found in "getAllLogWhStockReport" action\n', e);
    }
}