import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';

// REFERENCED FROM getAllJobRequest AND createJobRequest

export function* getSeasonPlanningRequest(action) {
    if (action.payload == undefined) {
        console.log("getSeasonPlanningRequest if");
        yield put(actions.getSeasonPlanningClear());
    }
    else {
        console.log("getSeasonPlanningRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/get/season-planning`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getSeasonPlanningSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getSeasonPlanningError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getSeasonPlanningError("error occured"));
            console.warn('Some error found in "getSeasonPlanningRequest" action\n', e);
        }
    }
}

export function* saveSeasonPlanningRequest(action) {
    
    if (action.payload == undefined) {

        console.log("saveSeasonPlanningRequest if");
        yield put(actions.saveSeasonPlanningClear());
    }
    else {
        try {
            console.log("saveSeasonPlanningRequest try", action.payload);
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/save/season-planning`,
                action.payload
            );
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.saveSeasonPlanningSuccess(response.data.data));
                // yield put(actions.runOnDemandRequest(action.payload));
            }
            else if (finalResponse.failure) {
                yield put(actions.saveSeasonPlanningError(response.data));
            }
        }
        catch (e) {
            console.log("saveSeasonPlanningRequest catch");
            yield put(actions.saveSeasonPlanningError("error occured"));
            console.warn('Some error found in "saveSeasonPlanningRequest" action\n', e);
        }
    }
}

export function* deleteSeasonPlanningRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteSeasonPlanningClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/delete/seasonPlanning`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.deleteSeasonPlanningSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.deleteSeasonPlanningError(response.data));
            }
        }
        catch (e) {
            yield put(actions.deleteSeasonPlanningError("error occured"));
            console.warn('Some error found in "deleteSeasonPlanningRequest" action\n', e);
        }
    }
}


export function* getEventPlanningRequest(action) {
    if (action.payload == undefined) {
        console.log("getEventPlanningRequest if");
        yield put(actions.getEventPlanningClear());
    }
    else {
        console.log("getEventPlanningRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/get/event-planning`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getEventPlanningSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getEventPlanningError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getEventPlanningError("error occured"));
            console.warn('Some error found in "getEventPlanningRequest" action\n', e);
        }
    }
}

export function* saveEventPlanningRequest(action) {
    if (action.payload == undefined) {
        console.log("saveEventPlanningRequest if");
        yield put(actions.saveEventPlanningClear());
    }
    try {
        console.log("saveEventPlanningRequest try", action.payload);
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/save/event-planning`,
            action.payload
        );
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.saveEventPlanningSuccess(response.data.data));
            // yield put(actions.runOnDemandRequest(action.payload));
        }
        else if (finalResponse.failure) {
            yield put(actions.saveEventPlanningError(response.data));
        }
    }
    catch (e) {
        console.log("saveEventPlanningRequest catch");
        yield put(actions.saveEventPlanningError("error occured"));
        console.warn('Some error found in "saveEventPlanningRequest" action\n', e);
    }
}

export function* deleteEventPlanningRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteEventPlanningClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/delete/eventPlanning`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.deleteEventPlanningSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.deleteEventPlanningError(response.data));
            }
        }
        catch (e) {
            yield put(actions.deleteEventPlanningError("error occured"));
            console.warn('Some error found in "deleteEventPlanningRequest" action\n', e);
        }
    }
}


export function* getSitePlanningRequest(action) {
    if (action.payload == undefined) {
        console.log("getSitePlanningRequest if");
        yield put(actions.getSitePlanningClear());
    }
    else {
        console.log("getSitePlanningRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/get/site-planning-data`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getSitePlanningSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getSitePlanningError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getSitePlanningError("error occured"));
            console.warn('Some error found in "getSitePlanningRequest" action\n', e);
        }
    }
}

export function* getSitePlanningFiltersRequest(action) {
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/plan/settings/get/site-planning-filters`, {});

        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getSitePlanningFiltersSuccess(response.data.data));
        }
        else if (finalResponse.failure) {
            yield put(actions.getSitePlanningFiltersError(response.data));
        }
    }
    catch (e) {
        yield put(actions.getSitePlanningFiltersError("error occured"));
        console.warn('Some error found in "getSitePlanningFiltersRequest" action\n', e);
    }
}

export function* saveSitePlanningRequest(action) {
    if (action.payload == undefined) {
        console.log("saveSitePlanningRequest if");
        yield put(actions.saveSitePlanningClear());
    }
    try {
        console.log("saveSitePlanningRequest try", action.payload);
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/save/site-planning`,
            action.payload
        );
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.saveSitePlanningSuccess(response.data.data));
            // yield put(actions.runOnDemandRequest(action.payload));
        }
        else if (finalResponse.failure) {
            yield put(actions.saveSitePlanningError(response.data));
        }
    }
    catch (e) {
        console.log("saveSitePlanningRequest catch");
        yield put(actions.saveSitePlanningError("error occured"));
        console.warn('Some error found in "saveSitePlanningRequest" action\n', e);
    }
}

export function* deleteSitePlanningRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteSitePlanningClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/delete/sitePlanning`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.deleteSitePlanningSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.deleteSitePlanningError(response.data));
            }
        }
        catch (e) {
            yield put(actions.deleteSitePlanningError("error occured"));
            console.warn('Some error found in "deleteSitePlanningRequest" action\n', e);
        }
    }
}


export function* getPlanningParameterRequest(action) {
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/plan/settings/get/planning-parameter`, {});

        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getPlanningParameterSuccess(response.data.data));
        }
        else if (finalResponse.failure) {
            yield put(actions.getPlanningParameterError(response.data));
        }
    }
    catch (e) {
        yield put(actions.getPlanningParameterError("error occured"));
        console.warn('Some error found in "getPlanningParameterRequest" action\n', e);
    }
}

export function* savePlanningParameterRequest(action) {
    try {
        console.log("savePlanningParameterRequest try", action.payload);
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/save/planning-parameter`,
            action.payload
        );
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.savePlanningParameterSuccess(response.data.data));
            // yield put(actions.runOnDemandRequest(action.payload));
        }
        else if (finalResponse.failure) {
            yield put(actions.savePlanningParameterError(response.data));
        }
    }
    catch (e) {
        console.log("savePlanningParameterRequest catch");
        yield put(actions.savePlanningParameterError("error occured"));
        console.warn('Some error found in "savePlanningParameterRequest" action\n', e);
    }
}


export function* getItemConfigurationRequest(action) { //API NUMBER 5
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/plan/settings/get/siteItemConfigurationFilterData?filterType=${action.payload}`, {});

        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getItemConfigurationSuccess(response.data.data));
        }
        else if (finalResponse.failure) {
            yield put(actions.getItemConfigurationError(response.data));
        }
    }
    catch (e) {
        yield put(actions.getItemConfigurationError("error occured"));
        console.warn('Some error found in "getItemConfigurationRequest" action\n', e);
    }
}

export function* saveItemConfigurationRequest(action) { //API NUMBER 6
    try {
        console.log("saveItemConfigurationRequest try", action.payload);
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/save/siteItemConfigurationFilterData?filterType=${action.payload.filterType}`, {
            data: action.payload.data,
            userDefineNames: action.payload.userDefineNames
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.saveItemConfigurationSuccess(response.data.data));
            // yield put(actions.runOnDemandRequest(action.payload));
        }
        else if (finalResponse.failure) {
            yield put(actions.saveItemConfigurationError(response.data));
        }
    }
    catch (e) {
        console.log("saveItemConfigurationRequest catch");
        yield put(actions.saveItemConfigurationError("error occured"));
        console.warn('Some error found in "saveItemConfigurationRequest" action\n', e);
    }
}

export function* getSiteAndItemFilterRequest(action) { //API NUMBER 2
    console.log(action.payload);
    try {
        let req = action.payload;
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/plan/settings/get/siteAndItemFilter?filterType=${action.payload}`, {});

        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getSiteAndItemFilterSuccess(response.data.data));
        }
        else if (finalResponse.failure) {
            yield put(actions.getSiteAndItemFilterError(response.data));
        }
    }
    catch (e) {
        yield put(actions.getSiteAndItemFilterError("error occured"));
        console.warn('Some error found in "getSiteAndItemFilterRequest" action\n', e);
    }
}

export function* saveSiteAndItemFilterRequest(action) { //API NUMBER 3
    try {
        console.log("saveSiteAndItemFilterRequest try", action.payload);
        let payload = {};
        action.payload.data === undefined ? payload.date = action.payload.date : payload.data = action.payload.data;
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/save/arsSiteAndArsItemFilter?filterType=${action.payload.filterType}`, {...payload});
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.saveSiteAndItemFilterSuccess(response.data.data));
            // yield put(actions.runOnDemandRequest(action.payload));
        }
        else if (finalResponse.failure) {
            yield put(actions.saveSiteAndItemFilterError(response.data));
        }
    }
    catch (e) {
        console.log("saveSiteAndItemFilterRequest catch");
        yield put(actions.saveSiteAndItemFilterError("error occured"));
        console.warn('Some error found in "saveSiteAndItemFilterRequest" action\n', e);
    }
}

export function* getSiteAndItemFilterConfigurationRequest(action) { //API NUMBER 4 FOR RUN-ON-DEMAND
    console.log(action.payload);
    try {
        let req = action.payload;
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/plan/settings/get/arsItemAndArsSiteFilter?filterType=${action.payload}`, {});

        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getSiteAndItemFilterConfigurationSuccess(response.data.data));
        }
        else if (finalResponse.failure) {
            yield put(actions.getSiteAndItemFilterConfigurationError(response.data));
        }
    }
    catch (e) {
        yield put(actions.getSiteAndItemFilterConfigurationError("error occured"));
        console.warn('Some error found in "getSiteAndItemFilterConfigurationRequest" action\n', e);
    }
}

export function* getArsGenericFiltersRequest(action) { //API NUMBER 1
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/search/arsGenericFilters`, {
            entity: action.payload.entity,
            key: action.payload.key,
            code: action.payload.code == undefined ? "" : action.payload.code,
            search: action.payload.search,
            pageNo: action.payload.pageNo
        });

        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getArsGenericFiltersSuccess(response.data.data));
        }
        else if (finalResponse.failure) {
            yield put(actions.getArsGenericFiltersError(response.data));
        }
    }
    catch (e) {
        yield put(actions.getArsGenericFiltersError("error occured"));
        console.warn('Some error found in "getArsGenericFiltersRequest" action\n', e);
    }
}


export function* getAdhocRequest(action) {
    console.log(action.payload);
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/inv/plan/adhoc/get/all`, action.payload);

        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAdhocSuccess(response.data.data));
        }
        else if (finalResponse.failure) {
            yield put(actions.getAdhocError(response.data));
        }
    }
    catch (e) {
        yield put(actions.getAdhocError("error occured"));
        console.warn('Some error found in "getAdhocRequest" action\n', e);
    }
}

export function* deleteAdhocRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteAdhocClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/inv/plan/adhoc/delete/adhocRequest?workOrder=${action.payload}`, {});
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.deleteAdhocSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.deleteAdhocError(response.data));
            }
        }
        catch (e) {
            yield put(actions.deleteAdhocError("error occured"));
            console.warn('Some error found in "deleteAdhocRequest" action\n', e);
        }
    }
}

export function* updateAdhocRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updateAdhocClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/inv/plan/adhoc/update/pendingToApproveReject?statusType=${action.payload.type}`,
                action.payload.keys
            );
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateAdhocSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.updateAdhocError(response.data));
            }
        }
        catch (e) {
            yield put(actions.updateAdhocError("error occured"));
            console.warn('Some error found in "updateAdhocRequest" action\n', e);
        }
    }
}

export function* searchAdhocSiteRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.searchAdhocSiteClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/inv/plan/adhoc/search/adhocSite?search=${action.payload.search}&pageNo=${action.payload.pageNo}`, {});
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.searchAdhocSiteSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.searchAdhocSiteError(response.data));
            }
        }
        catch (e) {
            yield put(actions.searchAdhocSiteError("error occured"));
            console.warn('Some error found in "searchAdhocSiteRequest" action\n', e);
        }
    }
}

export function* searchAdhocItemRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.searchAdhocItemClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/inv/plan/adhoc/search/adhocItem?search=${action.payload.search}&pageNo=${action.payload.pageNo}`, {});
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.searchAdhocItemSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.searchAdhocItemError(response.data));
            }
        }
        catch (e) {
            yield put(actions.searchAdhocItemError("error occured"));
            console.warn('Some error found in "searchAdhocItemRequest" action\n', e);
        }
    }
}

export function* saveAdhocRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.saveAdhocClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/inv/plan/adhoc/create/adhocRequest`,
                action.payload
            );
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.saveAdhocSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.saveAdhocError(response.data));
            }
        }
        catch (e) {
            yield put(actions.saveAdhocError("error occured"));
            console.warn('Some error found in "saveAdhocRequest" action\n', e);
        }
    }
}

export function* getAdhocItemsPerWORequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAdhocItemsPerWOClear());
    }
    else {
        try {
            let req = action.payload;
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/inv/plan/adhoc/get/itemsPerWorkOrder?pageNo=${req.pageNo}&workOrder=${req.workOrder}&statusType=${req.status}`, {});
    
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAdhocItemsPerWOSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getAdhocItemsPerWOError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getAdhocItemsPerWOError("error occured"));
            console.warn('Some error found in "getAdhocItemsPerWORequest" action\n', e);
        }
    }
}

export function* updateAdhocItemsPerWORequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updateAdhocItemsPerWOClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/inv/plan/adhoc/update/itemsPerWorkOrder`,
                action.payload
            );
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateAdhocItemsPerWOSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.updateAdhocItemsPerWOError(response.data));
            }
        }
        catch (e) {
            yield put(actions.updateAdhocItemsPerWOError("error occured"));
            console.warn('Some error found in "updateAdhocItemsPerWORequest" action\n', e);
        }
    }
}


export function* getMRPRangeRequest(action) {
    if (action.payload == undefined) {
        console.log("getMRPRangeRequest if");
        yield put(actions.getMRPRangeClear());
    }
    else {
        console.log("getMRPRangeRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/get/mrp/range/icode`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getMRPRangeSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getMRPRangeError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getMRPRangeError("error occured"));
            console.warn('Some error found in "getMRPRangeRequest" action\n', e);
        }
    }
}

export function* updateMRPRangeRequest(action) {
    if (action.payload == undefined) {
        console.log("updateMRPRangeRequest if");
        yield put(actions.updateMRPRangeClear());
    }
    else {
        console.log("updateMRPRangeRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/update/mrp/range/icode`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateMRPRangeSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.updateMRPRangeError(response.data));
            }
        }
        catch (e) {
            yield put(actions.updateMRPRangeError("error occured"));
            console.warn('Some error found in "updateMRPRangeRequest" action\n', e);
        }
    }
}


export function* getAllocationBasedOnItemRequest(action) {
    // if (action.payload == undefined) {
    //     console.log("getAllocationBasedOnItemRequest if");
    //     yield put(actions.getAllocationBasedOnItemClear());
    // }
    // else 
    {
        console.log("getAllocationBasedOnItemRequest else");
        try {
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/assortment/get/allocationBasedOnItem`, {});

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAllocationBasedOnItemSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getAllocationBasedOnItemError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getAllocationBasedOnItemError("error occured"));
            console.warn('Some error found in "getAllocationBasedOnItemRequest" action\n', e);
        }
    }
}

export function* saveAllocationBasedOnItemRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.saveAllocationBasedOnItemClear());
    }
    else {
        try {
            //var { orgIdGlobal } = OganisationIdName()
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/assortment/save/allocationBasedOnItem?isEnbaleDisable=${action.payload}`, {});

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.saveAllocationBasedOnItemSuccess(response.data.data));
                //yield put(actions.getFiveTriggersRequest());
            }
            else if (finalResponse.failure) {
                yield put(actions.saveAllocationBasedOnItemError(response.data));
            }
        }
        catch (e) {
            yield put(actions.saveAllocationBasedOnItemError("error occured"));
            console.warn('Some error found in "saveAllocationBasedOnItemRequest" action\n', e);
        }
    }
}

export function* getAssortmentPlanningRequest(action) {
    if (action.payload == undefined) {
        console.log("getAssortmentPlanningRequest if");
        yield put(actions.getAssortmentPlanningClear());
    }
    else
    {
        console.log("getAssortmentPlanningRequest else");
        try {
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/assortment/get/assortmentPlanningDetail?pageNo=${action.payload.pageNo}&type=${action.payload.type}`, {});

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAssortmentPlanningSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getAssortmentPlanningError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getAssortmentPlanningError("error occured"));
            console.warn('Some error found in "getAssortmentPlanningRequest" action\n', e);
        }
    }
}

export function* findFilterAssortmentRequest(action) {
    if (action.payload == undefined) {
        console.log("findFilterAssortmentRequest if");
        yield put(actions.findFilterAssortmentClear());
    }
    else
    {
        console.log("findFilterAssortmentRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/assortment/find/filterAssortment`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.findFilterAssortmentSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.findFilterAssortmentError(response.data));
            }
        }
        catch (e) {
            yield put(actions.findFilterAssortmentError("error occured"));
            console.warn('Some error found in "findFilterAssortmentRequest" action\n', e);
        }
    }
}

export function* saveAssortmentPlanningRequest(action) {
    if (action.payload == undefined) {
        console.log("saveAssortmentPlanningRequest if");
        yield put(actions.saveAssortmentPlanningClear());
    }
    else
    {
        console.log("saveAssortmentPlanningRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/assortment/save/assortmentPlanning`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.saveAssortmentPlanningSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.saveAssortmentPlanningError(response.data));
            }
        }
        catch (e) {
            yield put(actions.saveAssortmentPlanningError("error occured"));
            console.warn('Some error found in "saveAssortmentPlanningRequest" action\n', e);
        }
    }
}


export function* getInventoryPlanningReportRequest(action) {
    if (action.payload == undefined) {
        console.log("getInventoryPlanningReportRequest if");
        yield put(actions.getInventoryPlanningReportClear());
    }
    else
    {
        console.log("getInventoryPlanningReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/get/inventoryPlanningReport`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getInventoryPlanningReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getInventoryPlanningReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getInventoryPlanningReportError("error occured"));
            console.warn('Some error found in "getInventoryPlanningReportRequest" action\n', e);
        }
    }
}

export function* getSalesInventoryReportRequest(action) {
    if (action.payload == undefined) {
        console.log("getSalesInventoryReportRequest if");
        yield put(actions.getSalesInventoryReportClear());
    }
    else
    {
        console.log("getSalesInventoryReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/get/salesInventoryReport`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getSalesInventoryReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getSalesInventoryReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getSalesInventoryReportError("error occured"));
            console.warn('Some error found in "getSalesInventoryReportRequest" action\n', e);
        }
    }
}

export function* updateInventoryPlanningReportRequest(action) {
    if (action.payload == undefined) {
        console.log("updateInventoryPlanningReportRequest if");
        yield put(actions.updateInventoryPlanningReportClear());
    }
    else
    {
        console.log("updateInventoryPlanningReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/update/inventoryPlanningReport/value`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateInventoryPlanningReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.updateInventoryPlanningReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.updateInventoryPlanningReportError("error occured"));
            console.warn('Some error found in "updateInventoryPlanningReportRequest" action\n', e);
        }
    }
}


export function* downloadReportRequest(action) {
    if (action.payload == undefined) {
        console.log("downloadReportRequest if");
        yield put(actions.downloadReportClear());
    }
    else
    {
        console.log("downloadReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=${action.payload.module}&isAllData=false&isOnlyCurrentPage=false`, action.payload.data);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.downloadReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.downloadReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.downloadReportError("error occured"));
            console.warn('Some error found in "downloadReportRequest" action\n', e);
        }
    }
}


export function* getMRPHistoryRequest(action) {
    if (action.payload == undefined) {
        console.log("getMRPHistoryRequest if");
        yield put(actions.getMRPHistoryClear());
    }
    else {
        console.log("getMRPHistoryRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/plan/settings/get/mrp/range`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getMRPHistorySuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getMRPHistoryError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getMRPHistoryError("error occured"));
            console.warn('Some error found in "getMRPHistoryRequest" action\n', e);
        }
    }
}

//-------------------- REPORTS BEGIN --------------------//

export function* getSalesContributionReportRequest(action) {
    if (action.payload == undefined) {
        console.log("getSalesContributionReportRequest if");
        yield put(actions.getSalesContributionReportClear());
    }
    else
    {
        console.log("getSalesContributionReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/get/saleContributionReport`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getSalesContributionReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getSalesContributionReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getSalesContributionReportError("error occured"));
            console.warn('Some error found in "getSalesContributionReportRequest" action\n', e);
        }
    }
}

export function* getSalesComparisonReportRequest(action) {
    if (action.payload == undefined) {
        console.log("getSalesComparisonReportRequest if");
        yield put(actions.getSalesComparisonReportClear());
    }
    else
    {
        console.log("getSalesComparisonReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/get/saleComparisionReport`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getSalesComparisonReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getSalesComparisonReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getSalesComparisonReportError("error occured"));
            console.warn('Some error found in "getSalesComparisonReportRequest" action\n', e);
        }
    }
}

export function* getSellThruPerformanceReportRequest(action) {
    if (action.payload == undefined) {
        console.log("getSellThruPerformanceReportRequest if");
        yield put(actions.getSellThruPerformanceReportClear());
    }
    else
    {
        console.log("getSellThruPerformanceReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/get/sellThruPerformance`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getSellThruPerformanceReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getSellThruPerformanceReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getSellThruPerformanceReportError("error occured"));
            console.warn('Some error found in "getSellThruPerformanceReportRequest" action\n', e);
        }
    }
}

export function* getTopMovingItemsReportRequest(action) {
    if (action.payload == undefined) {
        console.log("getTopMovingItemsReportRequest if");
        yield put(actions.getTopMovingItemsReportClear());
    }
    else
    {
        console.log("getTopMovingItemsReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/get/topMovingReport`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getTopMovingItemsReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getTopMovingItemsReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getTopMovingItemsReportError("error occured"));
            console.warn('Some error found in "getTopMovingItemsReportRequest" action\n', e);
        }
    }
}

export function* getCategorySizeWiseReportRequest(action) {
    if (action.payload == undefined) {
        console.log("getCategorySizeWiseReportRequest if");
        yield put(actions.getCategorySizeWiseReportClear());
    }
    else
    {
        console.log("getCategorySizeWiseReportRequest else");
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ars/plan/get/categorySizeWiseReport`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getCategorySizeWiseReportSuccess(response.data.data));
            }
            else if (finalResponse.failure) {
                yield put(actions.getCategorySizeWiseReportError(response.data));
            }
        }
        catch (e) {
            yield put(actions.getCategorySizeWiseReportError("error occured"));
            console.warn('Some error found in "getCategorySizeWiseReportRequest" action\n', e);
        }
    }
}

//-------------------- REPORTS END --------------------//
