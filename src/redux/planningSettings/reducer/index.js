import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
    getSeasonPlanning: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    saveSeasonPlanning: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    deleteSeasonPlanning: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    getEventPlanning: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    saveEventPlanning: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    deleteEventPlanning: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    getSitePlanning: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getSitePlanningFilters: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    saveSitePlanning: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    deleteSitePlanning: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    getPlanningParameter: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    savePlanningParameter: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getItemConfiguration: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    saveItemConfiguration: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getSiteAndItemFilter: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    saveSiteAndItemFilter: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getSiteAndItemFilterConfiguration: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getArsGenericFilters: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getAdhoc: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    deleteAdhoc: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    updateAdhoc: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    searchAdhocSite: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    searchAdhocItem: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    saveAdhoc: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    getAdhocItemsPerWO: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    updateAdhocItemsPerWO: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getMRPRange: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    updateMRPRange: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getAllocationBasedOnItem: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    saveAllocationBasedOnItem: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getAssortmentPlanning: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    findFilterAssortment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    saveAssortmentPlanning: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getInventoryPlanningReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getSalesInventoryReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    updateInventoryPlanningReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    downloadReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getMRPHistory: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getSalesContributionReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getSalesComparisonReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getSellThruPerformanceReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getTopMovingItemsReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    getCategorySizeWiseReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
}


//------------------------- GET SEASON PLANNING -------------------------

const getSeasonPlanningRequest = (state, action) => update(state, {
    getSeasonPlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSeasonPlanningSuccess = (state, action) => update(state, {
    getSeasonPlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Season Planning success' }
    }
});

const getSeasonPlanningError = (state, action) => update(state, {
    getSeasonPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSeasonPlanningClear = (state, action) => update(state, {
    getSeasonPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE SEASON PLANNING -------------------------

const saveSeasonPlanningRequest = (state, action) => update(state, {
    saveSeasonPlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const saveSeasonPlanningSuccess = (state, action) => update(state, {
    saveSeasonPlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'save Season Planning success' }
    }
});

const saveSeasonPlanningError = (state, action) => update(state, {
    saveSeasonPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const saveSeasonPlanningClear = (state, action) => update(state, {
    saveSeasonPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- DELETE SEASON PLANNING  -------------------------

const deleteSeasonPlanningRequest = (state, action) => update(state, {
    deleteSeasonPlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const deleteSeasonPlanningSuccess = (state, action) => update(state, {
    deleteSeasonPlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' deleteSeasonPlanning' }
    }
});

const deleteSeasonPlanningError = (state, action) => update(state, {
    deleteSeasonPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const deleteSeasonPlanningClear = (state, action) => update(state, {
    deleteSeasonPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET EVENT PLANNING -------------------------

const getEventPlanningRequest = (state, action) => update(state, {
    getEventPlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getEventPlanningSuccess = (state, action) => update(state, {
    getEventPlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Season Planning success' }
    }
});

const getEventPlanningError = (state, action) => update(state, {
    getEventPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getEventPlanningClear = (state, action) => update(state, {
    getEventPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE EVENT PLANNING -------------------------

const saveEventPlanningRequest = (state, action) => update(state, {
    saveEventPlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const saveEventPlanningSuccess = (state, action) => update(state, {
    saveEventPlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'save Event Planning success' }
    }
});

const saveEventPlanningError = (state, action) => update(state, {
    saveEventPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const saveEventPlanningClear = (state, action) => update(state, {
    saveEventPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- DELETE EVENT PLANNING  -------------------------

const deleteEventPlanningRequest = (state, action) => update(state, {
    deleteEventPlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const deleteEventPlanningSuccess = (state, action) => update(state, {
    deleteEventPlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' deleteEventPlanning' }
    }
});

const deleteEventPlanningError = (state, action) => update(state, {
    deleteEventPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const deleteEventPlanningClear = (state, action) => update(state, {
    deleteEventPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET SITE PLANNING -------------------------

const getSitePlanningRequest = (state, action) => update(state, {
    getSitePlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSitePlanningSuccess = (state, action) => update(state, {
    getSitePlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Site Planning success' }
    }
});

const getSitePlanningError = (state, action) => update(state, {
    getSitePlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSitePlanningClear = (state, action) => update(state, {
    getSitePlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET SITE PLANNING FILTERS -------------------------

const getSitePlanningFiltersRequest = (state, action) => update(state, {
    getSitePlanningFilters: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSitePlanningFiltersSuccess = (state, action) => update(state, {
    getSitePlanningFilters: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Site Planning Filters success' }
    }
});

const getSitePlanningFiltersError = (state, action) => update(state, {
    getSitePlanningFilters: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSitePlanningFiltersClear = (state, action) => update(state, {
    getSitePlanningFilters: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE SITE PLANNING -------------------------

const saveSitePlanningRequest = (state, action) => update(state, {
    saveSitePlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const saveSitePlanningSuccess = (state, action) => update(state, {
    saveSitePlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'save Site Planning success' }
    }
});

const saveSitePlanningError = (state, action) => update(state, {
    saveSitePlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const saveSitePlanningClear = (state, action) => update(state, {
    saveSitePlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- DELETE SITE PLANNING  -------------------------

const deleteSitePlanningRequest = (state, action) => update(state, {
    deleteSitePlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const deleteSitePlanningSuccess = (state, action) => update(state, {
    deleteSitePlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' deleteSitePlanning' }
    }
});

const deleteSitePlanningError = (state, action) => update(state, {
    deleteSitePlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const deleteSitePlanningClear = (state, action) => update(state, {
    deleteSitePlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

//------------------------- GET PLANNING PARAMETER  -------------------------

const getPlanningParameterRequest = (state, action) => update(state, {
    getPlanningParameter: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getPlanningParameterSuccess = (state, action) => update(state, {
    getPlanningParameter: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Planning Parameter success' }
    }
});

const getPlanningParameterError = (state, action) => update(state, {
    getPlanningParameter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getPlanningParameterClear = (state, action) => update(state, {
    getPlanningParameter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE PLANNING PARAMETER -------------------------

const savePlanningParameterRequest = (state, action) => update(state, {
    savePlanningParameter: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const savePlanningParameterSuccess = (state, action) => update(state, {
    savePlanningParameter: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'save Planning Parameter success' }
    }
});

const savePlanningParameterError = (state, action) => update(state, {
    savePlanningParameter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});


//------------------------- GET ITEM CONFIGURATION  -------------------------

const getItemConfigurationRequest = (state, action) => update(state, {
    getItemConfiguration: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getItemConfigurationSuccess = (state, action) => update(state, {
    getItemConfiguration: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Item Configuration success' }
    }
});

const getItemConfigurationError = (state, action) => update(state, {
    getItemConfiguration: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getItemConfigurationClear = (state, action) => update(state, {
    getItemConfiguration: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE ITEM CONFIGURATION -------------------------

const saveItemConfigurationRequest = (state, action) => update(state, {
    saveItemConfiguration: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const saveItemConfigurationSuccess = (state, action) => update(state, {
    saveItemConfiguration: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'save Item Configuration success' }
    }
});

const saveItemConfigurationError = (state, action) => update(state, {
    saveItemConfiguration: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});


//------------------------- GET SITE AND ITEM FILTER -------------------------

const getSiteAndItemFilterRequest = (state, action) => update(state, {
    getSiteAndItemFilter: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSiteAndItemFilterSuccess = (state, action) => update(state, {
    getSiteAndItemFilter: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Site and Item Filter success' }
    }
});

const getSiteAndItemFilterError = (state, action) => update(state, {
    getSiteAndItemFilter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSiteAndItemFilterClear = (state, action) => update(state, {
    getSiteAndItemFilter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE SITE AND ITEM FILTER -------------------------

const saveSiteAndItemFilterRequest = (state, action) => update(state, {
    saveSiteAndItemFilter: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const saveSiteAndItemFilterSuccess = (state, action) => update(state, {
    saveSiteAndItemFilter: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'save Site and Item Filter success' }
    }
});

const saveSiteAndItemFilterError = (state, action) => update(state, {
    saveSiteAndItemFilter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const saveSiteAndItemFilterClear = (state, action) => update(state, {
    saveSiteAndItemFilter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET SITE AND ITEM FILTER CONFIGURATION -------------------------

const getSiteAndItemFilterConfigurationRequest = (state, action) => update(state, {
    getSiteAndItemFilterConfiguration: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSiteAndItemFilterConfigurationSuccess = (state, action) => update(state, {
    getSiteAndItemFilterConfiguration: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Site and Item Filter Configuration success' }
    }
});

const getSiteAndItemFilterConfigurationError = (state, action) => update(state, {
    getSiteAndItemFilterConfiguration: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSiteAndItemFilterConfigurationClear = (state, action) => update(state, {
    getSiteAndItemFilterConfiguration: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET ARS GENERIC FILTERS  -------------------------

const getArsGenericFiltersRequest = (state, action) => update(state, {
    getArsGenericFilters: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getArsGenericFiltersSuccess = (state, action) => update(state, {
    getArsGenericFilters: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Ars Generic Filters success' }
    }
});

const getArsGenericFiltersError = (state, action) => update(state, {
    getArsGenericFilters: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getArsGenericFiltersClear = (state, action) => update(state, {
    getArsGenericFilters: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET ADHOC  -------------------------

const getAdhocRequest = (state, action) => update(state, {
    getAdhoc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getAdhocSuccess = (state, action) => update(state, {
    getAdhoc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get Adhoc success' }
    }
});

const getAdhocError = (state, action) => update(state, {
    getAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getAdhocClear = (state, action) => update(state, {
    getAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- DELETE ADHOC  -------------------------

const deleteAdhocRequest = (state, action) => update(state, {
    deleteAdhoc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const deleteAdhocSuccess = (state, action) => update(state, {
    deleteAdhoc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' deleteAdhoc' }
    }
});

const deleteAdhocError = (state, action) => update(state, {
    deleteAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const deleteAdhocClear = (state, action) => update(state, {
    deleteAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- UPDATE ADHOC  -------------------------

const updateAdhocRequest = (state, action) => update(state, {
    updateAdhoc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const updateAdhocSuccess = (state, action) => update(state, {
    updateAdhoc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' updateAdhoc' }
    }
});

const updateAdhocError = (state, action) => update(state, {
    updateAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const updateAdhocClear = (state, action) => update(state, {
    updateAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SEARCH ADHOC SITE  -------------------------

const searchAdhocSiteRequest = (state, action) => update(state, {
    searchAdhocSite: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const searchAdhocSiteSuccess = (state, action) => update(state, {
    searchAdhocSite: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' searchAdhocSite' }
    }
});

const searchAdhocSiteError = (state, action) => update(state, {
    searchAdhocSite: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const searchAdhocSiteClear = (state, action) => update(state, {
    searchAdhocSite: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SEARCH ADHOC ITEM  -------------------------

const searchAdhocItemRequest = (state, action) => update(state, {
    searchAdhocItem: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const searchAdhocItemSuccess = (state, action) => update(state, {
    searchAdhocItem: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' searchAdhocItem' }
    }
});

const searchAdhocItemError = (state, action) => update(state, {
    searchAdhocItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const searchAdhocItemClear = (state, action) => update(state, {
    searchAdhocItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE ADHOC  -------------------------

const saveAdhocRequest = (state, action) => update(state, {
    saveAdhoc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const saveAdhocSuccess = (state, action) => update(state, {
    saveAdhoc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' saveAdhoc' }
    }
});

const saveAdhocError = (state, action) => update(state, {
    saveAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const saveAdhocClear = (state, action) => update(state, {
    saveAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET ADHOC ITEMS PER WO  -------------------------

const getAdhocItemsPerWORequest = (state, action) => update(state, {
    getAdhocItemsPerWO: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAdhocItemsPerWOSuccess = (state, action) => update(state, {
    getAdhocItemsPerWO: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' getAdhocItemsPerWO' }
    }
});

const getAdhocItemsPerWOError = (state, action) => update(state, {
    getAdhocItemsPerWO: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getAdhocItemsPerWOClear = (state, action) => update(state, {
    getAdhocItemsPerWO: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- UPDATE ADHOC ITEMS PER WO  -------------------------

const updateAdhocItemsPerWORequest = (state, action) => update(state, {
    updateAdhocItemsPerWO: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const updateAdhocItemsPerWOSuccess = (state, action) => update(state, {
    updateAdhocItemsPerWO: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: ' updateAdhocItemsPerWO' }
    }
});

const updateAdhocItemsPerWOError = (state, action) => update(state, {
    updateAdhocItemsPerWO: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const updateAdhocItemsPerWOClear = (state, action) => update(state, {
    updateAdhocItemsPerWO: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET MRP RANGE  -------------------------

const getMRPRangeRequest = (state, action) => update(state, {
    getMRPRange: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getMRPRangeSuccess = (state, action) => update(state, {
    getMRPRange: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get MRPRange success' }
    }
});

const getMRPRangeError = (state, action) => update(state, {
    getMRPRange: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getMRPRangeClear = (state, action) => update(state, {
    getMRPRange: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- UPDATE MRP RANGE  -------------------------

const updateMRPRangeRequest = (state, action) => update(state, {
    updateMRPRange: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const updateMRPRangeSuccess = (state, action) => update(state, {
    updateMRPRange: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'update MRPRange success' }
    }
});

const updateMRPRangeError = (state, action) => update(state, {
    updateMRPRange: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const updateMRPRangeClear = (state, action) => update(state, {
    updateMRPRange: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET ALLOCATION BASED ON ITEM  -------------------------

const getAllocationBasedOnItemRequest = (state, action) => update(state, {
    getAllocationBasedOnItem: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getAllocationBasedOnItemSuccess = (state, action) => update(state, {
    getAllocationBasedOnItem: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get AllocationBasedOnItem success' }
    }
});

const getAllocationBasedOnItemError = (state, action) => update(state, {
    getAllocationBasedOnItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getAllocationBasedOnItemClear = (state, action) => update(state, {
    getAllocationBasedOnItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE ALLOCATION BASED ON ITEM  -------------------------

const saveAllocationBasedOnItemRequest = (state, action) => update(state, {
    saveAllocationBasedOnItem: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const saveAllocationBasedOnItemSuccess = (state, action) => update(state, {
    saveAllocationBasedOnItem: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'save AllocationBasedOnItem success' }
    }
});

const saveAllocationBasedOnItemError = (state, action) => update(state, {
    saveAllocationBasedOnItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const saveAllocationBasedOnItemClear = (state, action) => update(state, {
    saveAllocationBasedOnItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET ASSORTMENT PLANNING  -------------------------

const getAssortmentPlanningRequest = (state, action) => update(state, {
    getAssortmentPlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getAssortmentPlanningSuccess = (state, action) => update(state, {
    getAssortmentPlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get AssortmentPlanning success' }
    }
});

const getAssortmentPlanningError = (state, action) => update(state, {
    getAssortmentPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getAssortmentPlanningClear = (state, action) => update(state, {
    getAssortmentPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- FIND FILTER ASSORTMENT  -------------------------

const findFilterAssortmentRequest = (state, action) => update(state, {
    findFilterAssortment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const findFilterAssortmentSuccess = (state, action) => update(state, {
    findFilterAssortment: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'find filterAssortment success' }
    }
});

const findFilterAssortmentError = (state, action) => update(state, {
    findFilterAssortment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const findFilterAssortmentClear = (state, action) => update(state, {
    findFilterAssortment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- SAVE ASSORTMENT PLANNING  -------------------------

const saveAssortmentPlanningRequest = (state, action) => update(state, {
    saveAssortmentPlanning: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const saveAssortmentPlanningSuccess = (state, action) => update(state, {
    saveAssortmentPlanning: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'save AssortmentPlanning success' }
    }
});

const saveAssortmentPlanningError = (state, action) => update(state, {
    saveAssortmentPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const saveAssortmentPlanningClear = (state, action) => update(state, {
    saveAssortmentPlanning: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET INVENTORY PLANNING REPORT -------------------------

const getInventoryPlanningReportRequest = (state, action) => update(state, {
    getInventoryPlanningReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getInventoryPlanningReportSuccess = (state, action) => update(state, {
    getInventoryPlanningReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get InventoryPlanningReport success' }
    }
});

const getInventoryPlanningReportError = (state, action) => update(state, {
    getInventoryPlanningReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getInventoryPlanningReportClear = (state, action) => update(state, {
    getInventoryPlanningReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET SALES INVENTORY REPORT -------------------------

const getSalesInventoryReportRequest = (state, action) => update(state, {
    getSalesInventoryReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSalesInventoryReportSuccess = (state, action) => update(state, {
    getSalesInventoryReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get SalesInventoryReport success' }
    }
});

const getSalesInventoryReportError = (state, action) => update(state, {
    getSalesInventoryReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSalesInventoryReportClear = (state, action) => update(state, {
    getSalesInventoryReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- UPDATE INVENTORY PLANNING REPORT -------------------------

const updateInventoryPlanningReportRequest = (state, action) => update(state, {
    updateInventoryPlanningReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const updateInventoryPlanningReportSuccess = (state, action) => update(state, {
    updateInventoryPlanningReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'update InventoryPlanningReport success' }
    }
});

const updateInventoryPlanningReportError = (state, action) => update(state, {
    updateInventoryPlanningReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const updateInventoryPlanningReportClear = (state, action) => update(state, {
    updateInventoryPlanningReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- DOWNLOAD REPORT -------------------------

const downloadReportRequest = (state, action) => update(state, {
    downloadReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const downloadReportSuccess = (state, action) => update(state, {
    downloadReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'download report success' }
    }
});

const downloadReportError = (state, action) => update(state, {
    downloadReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const downloadReportClear = (state, action) => update(state, {
    downloadReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET MRP HISTORY  -------------------------

const getMRPHistoryRequest = (state, action) => update(state, {
    getMRPHistory: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getMRPHistorySuccess = (state, action) => update(state, {
    getMRPHistory: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get MRPHistory success' }
    }
});

const getMRPHistoryError = (state, action) => update(state, {
    getMRPHistory: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getMRPHistoryClear = (state, action) => update(state, {
    getMRPHistory: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET SALES CONTRIBUTION REPORT -------------------------

const getSalesContributionReportRequest = (state, action) => update(state, {
    getSalesContributionReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSalesContributionReportSuccess = (state, action) => update(state, {
    getSalesContributionReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get SalesContributionReport success' }
    }
});

const getSalesContributionReportError = (state, action) => update(state, {
    getSalesContributionReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSalesContributionReportClear = (state, action) => update(state, {
    getSalesContributionReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET SALES COMPARISON REPORT -------------------------

const getSalesComparisonReportRequest = (state, action) => update(state, {
    getSalesComparisonReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSalesComparisonReportSuccess = (state, action) => update(state, {
    getSalesComparisonReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get SalesComparisonReport success' }
    }
});

const getSalesComparisonReportError = (state, action) => update(state, {
    getSalesComparisonReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSalesComparisonReportClear = (state, action) => update(state, {
    getSalesComparisonReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET SELL THRU PERFORMANCE REPORT -------------------------

const getSellThruPerformanceReportRequest = (state, action) => update(state, {
    getSellThruPerformanceReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getSellThruPerformanceReportSuccess = (state, action) => update(state, {
    getSellThruPerformanceReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get SellThruPerformanceReport success' }
    }
});

const getSellThruPerformanceReportError = (state, action) => update(state, {
    getSellThruPerformanceReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSellThruPerformanceReportClear = (state, action) => update(state, {
    getSellThruPerformanceReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET TOP MOVING ITEMS REPORT -------------------------

const getTopMovingItemsReportRequest = (state, action) => update(state, {
    getTopMovingItemsReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getTopMovingItemsReportSuccess = (state, action) => update(state, {
    getTopMovingItemsReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get TopMovingItemsReport success' }
    }
});

const getTopMovingItemsReportError = (state, action) => update(state, {
    getTopMovingItemsReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getTopMovingItemsReportClear = (state, action) => update(state, {
    getTopMovingItemsReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


//------------------------- GET CATEGORY SIZE WISE REPORT -------------------------

const getCategorySizeWiseReportRequest = (state, action) => update(state, {
    getCategorySizeWiseReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false},
        message: { $set: '' }
    }
});

const getCategorySizeWiseReportSuccess = (state, action) => update(state, {
    getCategorySizeWiseReport: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'get CategorySizeWiseReport success' }
    }
});

const getCategorySizeWiseReportError = (state, action) => update(state, {
    getCategorySizeWiseReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getCategorySizeWiseReportClear = (state, action) => update(state, {
    getCategorySizeWiseReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

export default handleActions({
    [constants.GET_SEASON_PLANNING_REQUEST]: getSeasonPlanningRequest,
    [constants.GET_SEASON_PLANNING_SUCCESS]: getSeasonPlanningSuccess,
    [constants.GET_SEASON_PLANNING_ERROR]: getSeasonPlanningError,
    [constants.GET_SEASON_PLANNING_CLEAR]: getSeasonPlanningClear,

    [constants.SAVE_SEASON_PLANNING_REQUEST]: saveSeasonPlanningRequest,
    [constants.SAVE_SEASON_PLANNING_SUCCESS]: saveSeasonPlanningSuccess,
    [constants.SAVE_SEASON_PLANNING_ERROR]: saveSeasonPlanningError,
    [constants.SAVE_SEASON_PLANNING_CLEAR]: saveSeasonPlanningClear,

    [constants.DELETE_SEASON_PLANNING_REQUEST]: deleteSeasonPlanningRequest,
    [constants.DELETE_SEASON_PLANNING_SUCCESS]: deleteSeasonPlanningSuccess,
    [constants.DELETE_SEASON_PLANNING_ERROR]: deleteSeasonPlanningError,
    [constants.DELETE_SEASON_PLANNING_CLEAR]: deleteSeasonPlanningClear,

    [constants.GET_EVENT_PLANNING_REQUEST]: getEventPlanningRequest,
    [constants.GET_EVENT_PLANNING_SUCCESS]: getEventPlanningSuccess,
    [constants.GET_EVENT_PLANNING_ERROR]: getEventPlanningError,
    [constants.GET_EVENT_PLANNING_CLEAR]: getEventPlanningClear,

    [constants.SAVE_EVENT_PLANNING_REQUEST]: saveEventPlanningRequest,
    [constants.SAVE_EVENT_PLANNING_SUCCESS]: saveEventPlanningSuccess,
    [constants.SAVE_EVENT_PLANNING_ERROR]: saveEventPlanningError,
    [constants.SAVE_EVENT_PLANNING_CLEAR]: saveEventPlanningClear,

    [constants.DELETE_EVENT_PLANNING_REQUEST]: deleteEventPlanningRequest,
    [constants.DELETE_EVENT_PLANNING_SUCCESS]: deleteEventPlanningSuccess,
    [constants.DELETE_EVENT_PLANNING_ERROR]: deleteEventPlanningError,
    [constants.DELETE_EVENT_PLANNING_CLEAR]: deleteEventPlanningClear,

    [constants.GET_SITE_PLANNING_REQUEST]: getSitePlanningRequest,
    [constants.GET_SITE_PLANNING_SUCCESS]: getSitePlanningSuccess,
    [constants.GET_SITE_PLANNING_ERROR]: getSitePlanningError,
    [constants.GET_SITE_PLANNING_CLEAR]: getSitePlanningClear,

    [constants.GET_SITE_PLANNING_FILTERS_REQUEST]: getSitePlanningFiltersRequest,
    [constants.GET_SITE_PLANNING_FILTERS_SUCCESS]: getSitePlanningFiltersSuccess,
    [constants.GET_SITE_PLANNING_FILTERS_ERROR]: getSitePlanningFiltersError,
    [constants.GET_SITE_PLANNING_FILTERS_CLEAR]: getSitePlanningFiltersClear,

    [constants.SAVE_SITE_PLANNING_REQUEST]: saveSitePlanningRequest,
    [constants.SAVE_SITE_PLANNING_SUCCESS]: saveSitePlanningSuccess,
    [constants.SAVE_SITE_PLANNING_ERROR]: saveSitePlanningError,
    [constants.SAVE_SITE_PLANNING_CLEAR]: saveSitePlanningClear,

    [constants.DELETE_SITE_PLANNING_REQUEST]: deleteSitePlanningRequest,
    [constants.DELETE_SITE_PLANNING_SUCCESS]: deleteSitePlanningSuccess,
    [constants.DELETE_SITE_PLANNING_ERROR]: deleteSitePlanningError,
    [constants.DELETE_SITE_PLANNING_CLEAR]: deleteSitePlanningClear,

    [constants.GET_PLANNING_PARAMETER_REQUEST]: getPlanningParameterRequest,
    [constants.GET_PLANNING_PARAMETER_SUCCESS]: getPlanningParameterSuccess,
    [constants.GET_PLANNING_PARAMETER_ERROR]: getPlanningParameterError,
    [constants.GET_PLANNING_PARAMETER_CLEAR]: getPlanningParameterClear,

    [constants.SAVE_PLANNING_PARAMETER_REQUEST]: savePlanningParameterRequest,
    [constants.SAVE_PLANNING_PARAMETER_SUCCESS]: savePlanningParameterSuccess,
    [constants.SAVE_PLANNING_PARAMETER_ERROR]: savePlanningParameterError,

    [constants.GET_ITEM_CONFIGURATION_REQUEST]: getItemConfigurationRequest,
    [constants.GET_ITEM_CONFIGURATION_SUCCESS]: getItemConfigurationSuccess,
    [constants.GET_ITEM_CONFIGURATION_ERROR]: getItemConfigurationError,
    [constants.GET_ITEM_CONFIGURATION_CLEAR]: getItemConfigurationClear,

    [constants.SAVE_ITEM_CONFIGURATION_REQUEST]: saveItemConfigurationRequest,
    [constants.SAVE_ITEM_CONFIGURATION_SUCCESS]: saveItemConfigurationSuccess,
    [constants.SAVE_ITEM_CONFIGURATION_ERROR]: saveItemConfigurationError,

    [constants.GET_SITE_AND_ITEM_FILTER_REQUEST]: getSiteAndItemFilterRequest,
    [constants.GET_SITE_AND_ITEM_FILTER_SUCCESS]: getSiteAndItemFilterSuccess,
    [constants.GET_SITE_AND_ITEM_FILTER_ERROR]: getSiteAndItemFilterError,
    [constants.GET_SITE_AND_ITEM_FILTER_CLEAR]: getSiteAndItemFilterClear,

    [constants.SAVE_SITE_AND_ITEM_FILTER_REQUEST]: saveSiteAndItemFilterRequest,
    [constants.SAVE_SITE_AND_ITEM_FILTER_SUCCESS]: saveSiteAndItemFilterSuccess,
    [constants.SAVE_SITE_AND_ITEM_FILTER_ERROR]: saveSiteAndItemFilterError,
    [constants.SAVE_SITE_AND_ITEM_FILTER_CLEAR]: saveSiteAndItemFilterClear,

    [constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_REQUEST]: getSiteAndItemFilterConfigurationRequest,
    [constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_SUCCESS]: getSiteAndItemFilterConfigurationSuccess,
    [constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_ERROR]: getSiteAndItemFilterConfigurationError,
    [constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_CLEAR]: getSiteAndItemFilterConfigurationClear,

    [constants.GET_ARS_GENERIC_FILTERS_REQUEST]: getArsGenericFiltersRequest,
    [constants.GET_ARS_GENERIC_FILTERS_SUCCESS]: getArsGenericFiltersSuccess,
    [constants.GET_ARS_GENERIC_FILTERS_ERROR]: getArsGenericFiltersError,
    [constants.GET_ARS_GENERIC_FILTERS_CLEAR]: getArsGenericFiltersClear,

    [constants.GET_ADHOC_REQUEST]: getAdhocRequest,
    [constants.GET_ADHOC_SUCCESS]: getAdhocSuccess,
    [constants.GET_ADHOC_ERROR]: getAdhocError,
    [constants.GET_ADHOC_CLEAR]: getAdhocClear,

    [constants.DELETE_ADHOC_REQUEST]: deleteAdhocRequest,
    [constants.DELETE_ADHOC_SUCCESS]: deleteAdhocSuccess,
    [constants.DELETE_ADHOC_ERROR]: deleteAdhocError,
    [constants.DELETE_ADHOC_CLEAR]: deleteAdhocClear,

    [constants.UPDATE_ADHOC_REQUEST]: updateAdhocRequest,
    [constants.UPDATE_ADHOC_SUCCESS]: updateAdhocSuccess,
    [constants.UPDATE_ADHOC_ERROR]: updateAdhocError,
    [constants.UPDATE_ADHOC_CLEAR]: updateAdhocClear,

    [constants.SEARCH_ADHOC_SITE_REQUEST]: searchAdhocSiteRequest,
    [constants.SEARCH_ADHOC_SITE_SUCCESS]: searchAdhocSiteSuccess,
    [constants.SEARCH_ADHOC_SITE_ERROR]: searchAdhocSiteError,
    [constants.SEARCH_ADHOC_SITE_CLEAR]: searchAdhocSiteClear,

    [constants.SEARCH_ADHOC_ITEM_REQUEST]: searchAdhocItemRequest,
    [constants.SEARCH_ADHOC_ITEM_SUCCESS]: searchAdhocItemSuccess,
    [constants.SEARCH_ADHOC_ITEM_ERROR]: searchAdhocItemError,
    [constants.SEARCH_ADHOC_ITEM_CLEAR]: searchAdhocItemClear,

    [constants.SAVE_ADHOC_REQUEST]: saveAdhocRequest,
    [constants.SAVE_ADHOC_SUCCESS]: saveAdhocSuccess,
    [constants.SAVE_ADHOC_ERROR]: saveAdhocError,
    [constants.SAVE_ADHOC_CLEAR]: saveAdhocClear,

    [constants.GET_ADHOC_ITEMS_PER_WO_REQUEST]: getAdhocItemsPerWORequest,
    [constants.GET_ADHOC_ITEMS_PER_WO_SUCCESS]: getAdhocItemsPerWOSuccess,
    [constants.GET_ADHOC_ITEMS_PER_WO_ERROR]: getAdhocItemsPerWOError,
    [constants.GET_ADHOC_ITEMS_PER_WO_CLEAR]: getAdhocItemsPerWOClear,

    [constants.UPDATE_ADHOC_ITEMS_PER_WO_REQUEST]: updateAdhocItemsPerWORequest,
    [constants.UPDATE_ADHOC_ITEMS_PER_WO_SUCCESS]: updateAdhocItemsPerWOSuccess,
    [constants.UPDATE_ADHOC_ITEMS_PER_WO_ERROR]: updateAdhocItemsPerWOError,
    [constants.UPDATE_ADHOC_ITEMS_PER_WO_CLEAR]: updateAdhocItemsPerWOClear,

    [constants.GET_MRP_RANGE_REQUEST]: getMRPRangeRequest,
    [constants.GET_MRP_RANGE_SUCCESS]: getMRPRangeSuccess,
    [constants.GET_MRP_RANGE_ERROR]: getMRPRangeError,
    [constants.GET_MRP_RANGE_CLEAR]: getMRPRangeClear,

    [constants.UPDATE_MRP_RANGE_REQUEST]: updateMRPRangeRequest,
    [constants.UPDATE_MRP_RANGE_SUCCESS]: updateMRPRangeSuccess,
    [constants.UPDATE_MRP_RANGE_ERROR]: updateMRPRangeError,
    [constants.UPDATE_MRP_RANGE_CLEAR]: updateMRPRangeClear,

    [constants.GET_ALLOCATION_BASED_ON_ITEM_REQUEST]: getAllocationBasedOnItemRequest,
    [constants.GET_ALLOCATION_BASED_ON_ITEM_SUCCESS]: getAllocationBasedOnItemSuccess,
    [constants.GET_ALLOCATION_BASED_ON_ITEM_ERROR]: getAllocationBasedOnItemError,
    [constants.GET_ALLOCATION_BASED_ON_ITEM_CLEAR]: getAllocationBasedOnItemClear,

    [constants.SAVE_ALLOCATION_BASED_ON_ITEM_REQUEST]: saveAllocationBasedOnItemRequest,
    [constants.SAVE_ALLOCATION_BASED_ON_ITEM_SUCCESS]: saveAllocationBasedOnItemSuccess,
    [constants.SAVE_ALLOCATION_BASED_ON_ITEM_ERROR]: saveAllocationBasedOnItemError,
    [constants.SAVE_ALLOCATION_BASED_ON_ITEM_CLEAR]: saveAllocationBasedOnItemClear,

    [constants.GET_ASSORTMENT_PLANNING_REQUEST]: getAssortmentPlanningRequest,
    [constants.GET_ASSORTMENT_PLANNING_SUCCESS]: getAssortmentPlanningSuccess,
    [constants.GET_ASSORTMENT_PLANNING_ERROR]: getAssortmentPlanningError,
    [constants.GET_ASSORTMENT_PLANNING_CLEAR]: getAssortmentPlanningClear,

    [constants.FIND_FILTER_ASSORTMENT_REQUEST]: findFilterAssortmentRequest,
    [constants.FIND_FILTER_ASSORTMENT_SUCCESS]: findFilterAssortmentSuccess,
    [constants.FIND_FILTER_ASSORTMENT_ERROR]: findFilterAssortmentError,
    [constants.FIND_FILTER_ASSORTMENT_CLEAR]: findFilterAssortmentClear,

    [constants.SAVE_ASSORTMENT_PLANNING_REQUEST]: saveAssortmentPlanningRequest,
    [constants.SAVE_ASSORTMENT_PLANNING_SUCCESS]: saveAssortmentPlanningSuccess,
    [constants.SAVE_ASSORTMENT_PLANNING_ERROR]: saveAssortmentPlanningError,
    [constants.SAVE_ASSORTMENT_PLANNING_CLEAR]: saveAssortmentPlanningClear,

    [constants.GET_INVENTORY_PLANNING_REPORT_REQUEST]: getInventoryPlanningReportRequest,
    [constants.GET_INVENTORY_PLANNING_REPORT_SUCCESS]: getInventoryPlanningReportSuccess,
    [constants.GET_INVENTORY_PLANNING_REPORT_ERROR]: getInventoryPlanningReportError,
    [constants.GET_INVENTORY_PLANNING_REPORT_CLEAR]: getInventoryPlanningReportClear,

    [constants.GET_SALES_INVENTORY_REPORT_REQUEST]: getSalesInventoryReportRequest,
    [constants.GET_SALES_INVENTORY_REPORT_SUCCESS]: getSalesInventoryReportSuccess,
    [constants.GET_SALES_INVENTORY_REPORT_ERROR]: getSalesInventoryReportError,
    [constants.GET_SALES_INVENTORY_REPORT_CLEAR]: getSalesInventoryReportClear,

    [constants.UPDATE_INVENTORY_PLANNING_REPORT_REQUEST]: updateInventoryPlanningReportRequest,
    [constants.UPDATE_INVENTORY_PLANNING_REPORT_SUCCESS]: updateInventoryPlanningReportSuccess,
    [constants.UPDATE_INVENTORY_PLANNING_REPORT_ERROR]: updateInventoryPlanningReportError,
    [constants.UPDATE_INVENTORY_PLANNING_REPORT_CLEAR]: updateInventoryPlanningReportClear,

    [constants.DOWNLOAD_REPORT_REQUEST]: downloadReportRequest,
    [constants.DOWNLOAD_REPORT_SUCCESS]: downloadReportSuccess,
    [constants.DOWNLOAD_REPORT_ERROR]: downloadReportError,
    [constants.DOWNLOAD_REPORT_CLEAR]: downloadReportClear,

    [constants.GET_MRP_HISTORY_REQUEST]: getMRPHistoryRequest,
    [constants.GET_MRP_HISTORY_SUCCESS]: getMRPHistorySuccess,
    [constants.GET_MRP_HISTORY_ERROR]: getMRPHistoryError,
    [constants.GET_MRP_HISTORY_CLEAR]: getMRPHistoryClear,

    [constants.GET_SALES_CONTRIBUTION_REPORT_REQUEST]: getSalesContributionReportRequest,
    [constants.GET_SALES_CONTRIBUTION_REPORT_SUCCESS]: getSalesContributionReportSuccess,
    [constants.GET_SALES_CONTRIBUTION_REPORT_ERROR]: getSalesContributionReportError,
    [constants.GET_SALES_CONTRIBUTION_REPORT_CLEAR]: getSalesContributionReportClear,

    [constants.GET_SALES_COMPARISON_REPORT_REQUEST]: getSalesComparisonReportRequest,
    [constants.GET_SALES_COMPARISON_REPORT_SUCCESS]: getSalesComparisonReportSuccess,
    [constants.GET_SALES_COMPARISON_REPORT_ERROR]: getSalesComparisonReportError,
    [constants.GET_SALES_COMPARISON_REPORT_CLEAR]: getSalesComparisonReportClear,

    [constants.GET_SELL_THRU_PERFORMANCE_REPORT_REQUEST]: getSellThruPerformanceReportRequest,
    [constants.GET_SELL_THRU_PERFORMANCE_REPORT_SUCCESS]: getSellThruPerformanceReportSuccess,
    [constants.GET_SELL_THRU_PERFORMANCE_REPORT_ERROR]: getSellThruPerformanceReportError,
    [constants.GET_SELL_THRU_PERFORMANCE_REPORT_CLEAR]: getSellThruPerformanceReportClear,

    [constants.GET_TOP_MOVING_ITEMS_REPORT_REQUEST]: getTopMovingItemsReportRequest,
    [constants.GET_TOP_MOVING_ITEMS_REPORT_SUCCESS]: getTopMovingItemsReportSuccess,
    [constants.GET_TOP_MOVING_ITEMS_REPORT_ERROR]: getTopMovingItemsReportError,
    [constants.GET_TOP_MOVING_ITEMS_REPORT_CLEAR]: getTopMovingItemsReportClear,

    [constants.GET_CATEGORY_SIZE_WISE_REPORT_REQUEST]: getCategorySizeWiseReportRequest,
    [constants.GET_CATEGORY_SIZE_WISE_REPORT_SUCCESS]: getCategorySizeWiseReportSuccess,
    [constants.GET_CATEGORY_SIZE_WISE_REPORT_ERROR]: getCategorySizeWiseReportError,
    [constants.GET_CATEGORY_SIZE_WISE_REPORT_CLEAR]: getCategorySizeWiseReportClear,

}, initialState);
  