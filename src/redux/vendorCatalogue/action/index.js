import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';

export function* productCatalogueCategoryRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/catalogue/master/category`, {
            pageNo:action.payload.pageNo,
            type:action.payload.type,
            groupSearch:action.payload.groupSearch,
            departmentSearch:action.payload.departmentSearch,
            categorySearch:action.payload.categorySearch,
            searchBy: action.payload.searchBy,
            basedOn: action.payload.basedOn,
            group: action.payload.group,
            department: action.payload.department,
            category: action.payload.category
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCatalogueCategorySuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCatalogueCategoryError(response.data));
        }
    } catch (e) {
        yield put(actions.productCatalogueCategoryError("error occurs"));
        console.warn('Some error found in "productCatalogueCategoryRequest" action\n', e);
}

}


export function* productCatalogueSizeRequest(action) {
    try {
        let type = action.payload.type
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/catalogue/get/master/data?key=${type}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCatalogueSizeSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCatalogueSizeError(response.data));
        }
    } catch (e) {
        yield put(actions.productCatalogueSizeError("error occurs"));
        console.warn('Some error found in "productCatalogueSizeRequest" action\n', e);
}

}

export function* productCatalogueColourRequest(action) {
    try {
        let type = action.payload.type
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/catalogue/get/master/data?key=${type}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCatalogueColourSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCatalogueColourError(response.data));
        }
    } catch (e) {
        yield put(actions.productCatalogueColourError("error occurs"));
        console.warn('Some error found in "productCatalogueColourRequest" action\n', e);
}

}


export function* productCataloguePatternRequest(action) {
    try {
        let type = action.payload.type
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/catalogue/get/master/data?key=${type}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCataloguePatternSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCataloguePatternError(response.data));
        }
    } catch (e) {
        yield put(actions.productCataloguePatternError("error occurs"));
        console.warn('Some error found in "productCataloguePatternRequest" action\n', e);
}
}


export function* productCatalogueSeasonRequest(action) {
    try {
        let type = action.payload.type
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/catalogue/get/master/data?key=${type}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCatalogueSeasonSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCatalogueSeasonError(response.data));
        }
    } catch (e) {
        yield put(actions.productCatalogueSeasonError("error occurs"));
        console.warn('Some error found in "productCatalogueSeasonRequest" action\n', e);
}
}

export function* productCatalogueNeckRequest(action) {
    try {
        let type = action.payload.type
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/catalogue/get/master/data?key=${type}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCatalogueNeckSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCatalogueNeckError(response.data));
        }
    } catch (e) {
        yield put(actions.productCatalogueNeckError("error occurs"));
        console.warn('Some error found in "productCatalogueNeckRequest" action\n', e);
}
}

export function* productCatalogueFabricRequest(action) {
    try {
        let type = action.payload.type
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/catalogue/get/master/data?key=${type}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCatalogueFabricSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCatalogueFabricError(response.data));
        }
    } catch (e) {
        yield put(actions.productCatalogueFabricError("error occurs"));
        console.warn('Some error found in "productCatalogueFabricRequest" action\n', e);
}
}

export function* productCatalogueUsagesRequest(action) {
    try{
        let type = action.payload.type
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/catalogue/get/master/data?key=${type}`, {
        });
        const finalResponse = script(response);
        if(finalResponse.success){
            yield put(actions.productCatalogueUsagesSuccess(response.data.data));
        }
        else if(finalResponse.failure){
            yield put(actions.productCatalogueUsagesError(response.data))
        }
    }
    catch(e){
        yield put(actions.productCatalogueUsagesError('error occurs'));
        console.warn('Some error found in "productCatalogueUsagesRequest" action\n', e);
    }
}

// Manula submit Request

export function* productCatalogueManualRequest(action) {
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/catalogue/item/submission`, {
            submissionId: action.payload.submissionId,
            vendorCode: action.payload.vendorCode,
            itemId: action.payload.itemId,
            itemCode: action.payload.itemCode,
            itemName: action.payload.itemName,
            ageGroup: action.payload.ageGroup,
            department: action.payload.department,
            category: action.payload.category,
            vendorBarcode: action.payload.vendorBarcode,
            size: action.payload.size,
            description: action.payload.description,
            brand: action.payload.brand,
            availableQty: action.payload.availableQty,
            minOrderQty: action.payload.minOrderQty,
            mrp: action.payload.mrp,
            rate: action.payload.rate,
            color: action.payload.color,
            pattern: action.payload.pattern,
            material: action.payload.material,
            season: action.payload.season,
            neck: action.payload.neck,
            occasion: action.payload.occasion,
            additionalRemarks: action.payload.additionalRemarks,
            imageUrlMain: action.payload.imageUrlMain,
            imageUrlFront: action.payload.imageUrlFront,
            imageUrlBack: action.payload.imageUrlBack,
            imageUrlSide: action.payload.imageUrlSide,
            imageUrlDetailed: action.payload.imageUrlDetailed,
            isActive: action.payload.isActive,
            isStockout: action.payload.isStockout,
            catalogType: action.payload.catalogType,
            hsn: action.payload.hsn,
            udf1: action.payload.udf1,
            udf2: action.payload.udf2,
            udf3: action.payload.udf3,
            udf4: action.payload.udf4,
            udf5: action.payload.udf5,
            udf6: action.payload.udf6,
            udf7: action.payload.udf7,
            udf8: action.payload.udf8,
            udf9: action.payload.udf9,
            udf10: action.payload.udf10,
            Status: action.payload.status,
            remarks: action.payload.remarks,
            isPublished: action.payload.isPublished,
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCatalogueManualSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCatalogueManualError(response.data));
        }
    } catch (e) {
        yield put(actions.productCatalogueManualError("error occurs"));
        console.warn('Some error found in "productCatalogueManualRequest" action\n', e);
}

}

// vendor catalogue submission history
export function* productCatalogueSubmissionHistoryRequest(action){
    try{
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/catalogue/item/submission/history`, {
                ...action.payload
        });
        const finalResponse = script(response);
        if(finalResponse.success){
            yield put(actions.productCatalogueSubmissionHistorySuccess(response.data.data));
        }else if(finalResponse.failure){
            yield put(actions.productCatalogueSubmissionHistoryError(response.data));
        }
    }
    catch(e){
        yield put(actions.productCatalogueSubmissionHistoryError('error occurs'));
        console.warn('Some error found in "ProductCatalogueSubmissionHistoryRequest" action\n', e);
    }
}

export function* productCatalogueSubmissionHistoryDetailsRequest(action){
    try{
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/catalogue/item/submission/detail`, {
            type:action.payload.type,
            pageNo:action.payload.pageNo,
            productType:action.payload.productType,
            search:action.payload.search,
            searchBy:action.payload.searchBy,
            submissionId:action.payload.submissionId,
            filter:{
                ageGroup:action.payload.filter.ageGroup
            },
            sortedBy:action.payload.sortedBy,
            sortedIn:action.payload.sortedIn
        });
        const finalResponse = script(response);
        if(finalResponse.success){
            yield put(actions.productCatalogueSubmissionHistoryDetailsSuccess(response.data.data));
        }else if(finalResponse.failure){
            yield put(actions.productCatalogueSubmissionHistoryDetailsError(response.data));
        }
    }
    catch(e){
        yield put(actions.productCatalogueSubmissionHistoryDetailsError('error occurs'));
        console.warn('Some error found in "ProductCatalogueSubmissionHistoryDetailsRequest" action\n', e);
    }
}

export function* productCatalogueHistoryHeaderRequest(action) {
    try {
        let enterpriseName = action.payload.enterpriseName;
        let attributeType = action.payload.attributeType;
        let displayName = action.payload.displayName;
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/headerconfig/get/header?enterpriseName=${enterpriseName}&attributeType=${attributeType}&displayName=${displayName}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.productCatalogueHistoryHeaderSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.productCatalogueHistoryHeaderError(response.data));
        }
    } catch (e) {
        yield put(actions.productCatalogueHistoryHeaderError("error occurs"));
        console.warn('Some error found in "productCatalogueHistoryHeaderRequest" action\n', e);
}

}