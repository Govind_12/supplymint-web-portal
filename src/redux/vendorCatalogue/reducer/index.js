import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
    // productCatalogueDepartment: {
    //     isSuccess: false,
    //     message: '',
    //     type: ''
    // },
    productCatalogueCategory: {
        isSuccess: false,
        message: '',
        type: '',
    },
    productCatalogueSize:{
        isSuccess: false,
        message: '',
        type: '',
    },
    productCatalogueColour:{
        isSuccess: false,
        message: '',
        type: '',
    },
    productCataloguePattern:{
        isSuccess: false,
        message: '',
        type: '',
    },
    productCatalogueSeason:{
        isSuccess: false,
        message: '',
        type: '',
    },
    productCatalogueNeck:{
        isSuccess: false,
        message: '',
        type: '',
    },
    productCatalogueFabric:{
        isSuccess: false,
        message: '',
        type: '',
    },
    productCatalogueUsages:{
        isSuccess: false,
        message: '',
        type: '',
    },
    productCatalogueManualSubmit:{
        isSuccess: false,
        message: '',
        data: '',
    },
    productCatalogueSubmissionHistory:{
        isSuccess: false,
        message: '',
        data: '',
    },
    productCatalogueSubmissionHistoryDetails:{
        isSuccess: false,
        message: '',
        data: '',
    },
    productCatalogueHistoryHeader:{
        isSuccess: false,
        message: '',
        data: '',
    }
}

const productCatalogueManualRequest = (state, action) => update(state, {
    productCatalogueManualSubmit: {
        isSuccess: { $set: false},
        data: { $set: '' },
        message: { $set: 'request'}
    }
});

const productCatalogueManualSuccess = (state, action) => update(state, {
    productCatalogueManualSubmit: {
        isSuccess: { $set: true},
        data: { $set: action.payload},
        message: { $set: 'success'}
    }
});

const productCatalogueManualError = (state, action) => update(state, {
    productCatalogueManualSubmit: {
        isSuccess: { $set: false},
        data: { $set: action.payload },
        message: { $set: 'error'}
    }
});


const productCatalogueCategoryRequest = (state, action) => update(state, {
    productCatalogueCategory: {
        isSuccess: { $set: false},
        type: { $set: '' },
        message: { $set: 'request'}
    }
});

const productCatalogueCategorySuccess = (state, action) => update(state, {
    productCatalogueCategory: {
        isSuccess: { $set: true},
        type: { $set: action.payload },
        message: { $set: 'success'}
    }
});

const productCatalogueCategoryError = (state, action) => update(state, {
    productCatalogueCategory: {
        isSuccess: { $set: false},
        type: { $set: action.payload },
        message: { $set: 'error'}
    }
});


const productCatalogueSizeRequest = (state, action) => update(state, {
    productCatalogueSize: {
        isSuccess: { $set: false},
        type: { $set: '' },
        message: { $set: 'request'}
    }
});

const productCatalogueSizeSuccess = (state, action) => update(state, {
    productCatalogueSize: {
        isSuccess: { $set: true},
        type: { $set: action.payload.resource },
        message: { $set: 'success'}
    }
});

const productCatalogueSizeError = (state, action) => update(state, {
    productCatalogueSize: {
        isSuccess: { $set: false},
        type: { $set: action.payload },
        message: { $set: 'error'}
    }
});

const productCatalogueColourRequest = (state, action) => update(state, {
    productCatalogueColour: {
        isSuccess: { $set: false},
        type: { $set: '' },
        message: { $set: 'request'}
    }
});

const productCatalogueColourSuccess = (state, action) => update(state, {
    productCatalogueColour: {
        isSuccess: { $set: true},
        type: { $set: action.payload.resource },
        message: { $set: 'success'}
    }
});

const productCatalogueColourError = (state, action) => update(state, {
    productCatalogueColour: {
        isSuccess: { $set: false},
        type: { $set: action.payload },
        message: { $set: 'error'}
    }
});

const productCataloguePatternRequest = (state, action) => update(state, {
    productCataloguePattern: {
        isSuccess: { $set: false},
        type: { $set: '' },
        message: { $set: 'request'}
    }
});

const productCataloguePatternSuccess = (state, action) => update(state, {
    productCataloguePattern: {
        isSuccess: { $set: true},
        type: { $set: action.payload.resource },
        message: { $set: 'success'}
    }
});

const productCataloguePatternError = (state, action) => update(state, {
    productCataloguePattern: {
        isSuccess: { $set: false},
        type: { $set: action.payload },
        message: { $set: 'error'}
    }
});

const productCatalogueSeasonRequest = (state, action) => update(state, {
    productCatalogueSeason: {
        isSuccess: { $set: false},
        type: { $set: '' },
        message: { $set: 'request'}
    }
});

const productCatalogueSeasonSuccess = (state, action) => update(state, {
    productCatalogueSeason: {
        isSuccess: { $set: true},
        type: { $set: action.payload.resource },
        message: { $set: 'success'}
    }
});

const productCatalogueSeasonError = (state, action) => update(state, {
    productCatalogueSeason: {
        isSuccess: { $set: false},
        type: { $set: action.payload },
        message: { $set: 'error'}
    }
});

const productCatalogueNeckRequest = (state, action) => update(state, {
    productCatalogueNeck: {
        isSuccess: { $set: false},
        type: { $set: '' },
        message: { $set: 'request'}
    }
});

const productCatalogueNeckSuccess = (state, action) => update(state, {
    productCatalogueNeck: {
        isSuccess: { $set: true},
        type: { $set: action.payload.resource },
        message: { $set: 'success'}
    }
});

const productCatalogueNeckError = (state, action) => update(state, {
    productCatalogueNeck: {
        isSuccess: { $set: false},
        type: { $set: action.payload },
        message: { $set: 'error'}
    }
});

const productCatalogueFabricRequest = (state, action) => update(state, {
    productCatalogueFabric: {
        isSuccess: { $set: false},
        type: { $set: '' },
        message: { $set: 'request'}
    }
});

const productCatalogueFabricSuccess = (state, action) => update(state, {
    productCatalogueFabric: {
        isSuccess: { $set: true},
        type: { $set: action.payload.resource },
        message: { $set: 'success'}
    }
});

const productCatalogueFabricError = (state, action) => update(state, {
    productCatalogueFabric: {
        isSuccess: { $set: false},
        type: { $set: action.payload },
        message: { $set: 'error'}
    }
});

const productCatalogueUsagesRequest = (state, action) => update(state, {
    productCatalogueUsages: {
        isSuccess: { $set: false },
        type: {$set: ''},
        message: { $set: 'request'}
    }
});

const productCatalogueUsagesSuccess = (state, action) => update(state, {
    productCatalogueUsages: {
        isSuccess: {$set: true},
        type: { $set: action.payload.resource},
        message: { $set: 'success'}
    }
})


const productCatalogueUsagesError = (state, action) => update(state, {
    productCatalogueUsages: {
        isSuccess: { $set: false},
        type: { $set: action.payload},
        message: { $set: 'error'}
    }
})

// vendor Catalogue Submission History
const productCatalogueSubmissionHistoryRequest = (state, action) => update(state, {
    productCatalogueSubmissionHistory: {
        isSuccess: {$set: false},
        data: {$set: ''},
        message: {$set: 'Request'}
    }
})

const productCatalogueSubmissionHistorySuccess = (state, action) => update(state, {
    productCatalogueSubmissionHistory: {
        isSuccess: {$set: true},
        data: {$set: action.payload},
        message: {$set: 'Success'}
    }
})

const productCatalogueSubmissionHistoryError = (state, action) => update(state, {
    productCatalogueSubmissionHistory: {
        isSuccess: {$set: false},
        data: {$set: action.payload},
        message: {$set: 'Error'}
    }
})

const productCatalogueSubmissionHistoryDetailsRequest = (state, action) => update(state, {
    productCatalogueSubmissionHistoryDetails: {
        isSuccess: {$set: false},
        data: {$set: ''},
        message: {$set: 'Request'}
    }
})

const productCatalogueSubmissionHistoryDetailsSuccess = (state, action) => update(state, {
    productCatalogueSubmissionHistoryDetails: {
        isSuccess: {$set: true},
        data: {$set: action.payload},
        message: {$set: 'Success'}
    }
})

const productCatalogueSubmissionHistoryDetailsError = (state, action) => update(state, {
    productCatalogueSubmissionHistoryDetails: {
        isSuccess: {$set: false},
        data: {$set: action.payload},
        message: {$set: 'Error'}
    }
})

const productCatalogueHistoryHeaderRequest = (state, action) => update(state, {
    productCatalogueHistoryHeader: {
        isSuccess: {$set: false},
        data: {$set: ''},
        message: {$set: 'Request'}
    }
})

const productCatalogueHistoryHeaderSuccess = (state, action) => update(state, {
    productCatalogueHistoryHeader: {
        isSuccess: {$set: true},
        data: {$set: action.payload},
        message: {$set: 'Success'}
    }
})

const productCatalogueHistoryHeaderError = (state, action) => update(state, {
    productCatalogueHistoryHeader: {
        isSuccess: {$set: false},
        data: {$set: action.payload},
        message: {$set: 'Error'}
    }
})


export default handleActions({

    [constants.PRODUCT_CATALOGUE_MANUALSUBMIT_REQUEST]: productCatalogueManualRequest,
    [constants.PRODUCT_CATALOGUE_MANUALSUBMIT_SUCCESS]: productCatalogueManualSuccess,
    [constants.PRODUCT_CATALOGUE_MANUALSUBMIT_ERROR]: productCatalogueManualError,

    [constants.PRODUCT_CATALOGUE_CATEGORY_REQUEST]: productCatalogueCategoryRequest,
    [constants.PRODUCT_CATALOGUE_CATEGORY_SUCCESS]: productCatalogueCategorySuccess,
    [constants.PRODUCT_CATALOGUE_CATEGORY_ERROR]: productCatalogueCategoryError,

    [constants.PRODUCT_CATALOGUE_SIZE_REQUEST]: productCatalogueSizeRequest,
    [constants.PRODUCT_CATALOGUE_SIZE_SUCCESS]: productCatalogueSizeSuccess,
    [constants.PRODUCT_CATALOGUE_SIZE_ERROR]: productCatalogueSizeError,

    [constants.PRODUCT_CATALOGUE_COLOUR_REQUEST]: productCatalogueColourRequest,
    [constants.PRODUCT_CATALOGUE_COLOUR_SUCCESS]: productCatalogueColourSuccess,
    [constants.PRODUCT_CATALOGUE_COLOUR_ERROR]: productCatalogueColourError,
    
    [constants.PRODUCT_CATALOGUE_PATTERN_REQUEST]: productCataloguePatternRequest,
    [constants.PRODUCT_CATALOGUE_PATTERN_SUCCESS]: productCataloguePatternSuccess,
    [constants.PRODUCT_CATALOGUE_PATTERN_ERROR]: productCataloguePatternError,
        
    [constants.PRODUCT_CATALOGUE_SEASON_REQUEST]: productCatalogueSeasonRequest,
    [constants.PRODUCT_CATALOGUE_SEASON_SUCCESS]: productCatalogueSeasonSuccess,
    [constants.PRODUCT_CATALOGUE_SEASON_ERROR]: productCatalogueSeasonError,
            
    [constants.PRODUCT_CATALOGUE_NECK_REQUEST]: productCatalogueNeckRequest,
    [constants.PRODUCT_CATALOGUE_NECK_SUCCESS]: productCatalogueNeckSuccess,
    [constants.PRODUCT_CATALOGUE_NECK_ERROR]: productCatalogueNeckError,

    [constants.PRODUCT_CATALOGUE_FABRIC_REQUEST]: productCatalogueFabricRequest,
    [constants.PRODUCT_CATALOGUE_FABRIC_SUCCESS]: productCatalogueFabricSuccess,
    [constants.PRODUCT_CATALOGUE_FABRIC_ERROR]: productCatalogueFabricError,

    [constants.PRODUCT_CATALOGUE_USAGES_REQUEST]: productCatalogueUsagesRequest,
    [constants.PRODUCT_CATALOGUE_USAGES_SUCCESS]: productCatalogueUsagesSuccess,
    [constants.PRODUCT_CATALOGUE_USAGES_ERROR]: productCatalogueUsagesError,

    // vendor catalogue submission history
    [constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_REQUEST]: productCatalogueSubmissionHistoryRequest,
    [constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_SUCCESS]: productCatalogueSubmissionHistorySuccess,
    [constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_ERROR]: productCatalogueSubmissionHistoryError,

    [constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_DETAILS_REQUEST]: productCatalogueSubmissionHistoryDetailsRequest,
    [constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_DETAILS_SUCCESS]: productCatalogueSubmissionHistoryDetailsSuccess,
    [constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_DETAILS_ERROR]: productCatalogueSubmissionHistoryDetailsError,

    [constants.PRODUCT_CATALOGUE_HISTORY_HEADER_REQUEST]: productCatalogueHistoryHeaderRequest,
    [constants.PRODUCT_CATALOGUE_HISTORY_HEADER_SUCCESS]: productCatalogueHistoryHeaderSuccess,
    [constants.PRODUCT_CATALOGUE_HISTORY_HEADER_ERROR]: productCatalogueHistoryHeaderError,
}, initialState)

