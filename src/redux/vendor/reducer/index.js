import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';
import AddVendor from '../../../components/vendor/Managevendor/addVendor'

let initialState = {
  getVendor: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  deleteVendor: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  addVendor: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''

  },
  editVendor: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getAllManageVendor: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createVendor: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateManageVendor: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateCreatedVendor: { 
    data: {}
  },

  accessVendorPortal: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  getAllManageTransporter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createTransporter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  updateManageTransporter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  updateCreatedTransporter: {
    data: {}
  },

  getAllManageCustomer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  editCustomer: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  getAllManageItem: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  getAllManageSalesAgent: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  sendEmail: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  saveKycDetails: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getKycDetails: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
};

const vendorRequest = (state, action) => update(state, {
  getVendor: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const vendorSuccess = (state, action) => update(state, {
  getVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Vendor success' }
  }
});

const vendorError = (state, action) => update(state, {
  getVendor: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const deleteVendorRequest = (state, action) => update(state, {
  deleteVendor: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteVendorClear = (state, action) => update(state, {
  deleteVendor: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const deleteVendorSuccess = (state, action) => update(state, {
  deleteVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Delete Vendor success' }
  }
});

const deleteVendorError = (state, action) => update(state, {
  deleteVendor: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

//add_vendor
const addVendorRequest = (state, action) => update(state, {
  addVendor: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addVendorClear = (state, action) => update(state, {
  addVendor: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const addVendorSuccess = (state, action) => update(state, {
  addVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Delete Vendor success' }
  }
});

const addVendorError = (state, action) => update(state, {
  addVendor: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


//edit_vendor
const editVendorRequest = (state, action) => update(state, {
  editVendor: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editVendorClear = (state, action) => update(state, {
  editVendor: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editVendorSuccess = (state, action) => update(state, {
  editVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Delete Vendor success' }
  }
});

const editVendorError = (state, action) => update(state, {
  editVendor: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

// get all anage vendor
const getAllManageVendorRequest = (state, action) => update(state, {
  getAllManageVendor: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllManageVendorClear = (state, action) => update(state, {
  getAllManageVendor: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllManageVendorSuccess = (state, action) => update(state, {
  getAllManageVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getAllManageVendorError = (state, action) => update(state, {
  getAllManageVendor: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
// create vendor
const createVendorRequest = (state, action) => update(state, {
  createVendor: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createVendorClear = (state, action) => update(state, {
  createVendor: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createVendorSuccess = (state, action) => update(state, {
  createVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Delete Vendor success' }
  }
});

const createVendorError = (state, action) => update(state, {
  createVendor: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

// update manage vendor
const updateManageVendorRequest = (state, action) => update(state, {
  updateManageVendor: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateManageVendorClear = (state, action) => update(state, {
  updateManageVendor: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateManageVendorSuccess = (state, action) => update(state, {
  updateManageVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Delete Vendor success' }
  }
});

const updateManageVendorError = (state, action) => update(state, {
  updateManageVendor: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

// update vednor data 

const updateCreatedVendorRequest = (state, action) => update(state, {
  updateCreatedVendor: {
    data: { $set: action.payload },
  }
});

const accessVendorPortalRequest = (state, action) => update(state, {
  accessVendorPortal: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const accessVendorPortalClear = (state, action) => update(state, {
  accessVendorPortal: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const accessVendorPortalSuccess = (state, action) => update(state, {
  accessVendorPortal: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const accessVendorPortalError = (state, action) => update(state, {
  accessVendorPortal: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


//-------------------- GET ALL TRANSPORTERS --------------------
const getAllManageTransporterRequest = (state, action) => update(state, {
  getAllManageTransporter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const getAllManageTransporterClear = (state, action) => update(state, {
  getAllManageTransporter: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const getAllManageTransporterSuccess = (state, action) => update(state, {
  getAllManageTransporter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Get Transporter success' }
  }
});
const getAllManageTransporterError = (state, action) => update(state, {
  getAllManageTransporter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

//CREATE TRANSPORTERS REQUEST
const createTransporterRequest = (state, action) => update(state, {
  createTransporter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createTransporterClear = (state, action) => update(state, {
  createTransporter: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createTransporterSuccess = (state, action) => update(state, {
  createTransporter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Delete Transporter success' }
  }
});

const createTransporterError = (state, action) => update(state, {
  createTransporter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const updateManageTransporterRequest = (state, action) => update(state, {
  updateManageTransporter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
})
const updateManageTransporterClear = (state, action) => update(state, {
  updateManageTransporter: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const updateManageTransporterSuccess = (state, action) => update(state, {
  updateManageTransporter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Update Transporter success' }
  }
})

const updateManageTransporterError = (state, action) => update(state, {
  updateManageTransporter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const updateCreatedTransporterRequest = (state, action) => update(state, {
  updateCreatedTransporter: {
    data: { $set: action.payload },
  }
});


//-------------------- GET ALL CUSTOMERS --------------------
const getAllManageCustomerRequest = (state, action) => update(state, {
  getAllManageCustomer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllManageCustomerClear = (state, action) => update(state, {
  getAllManageCustomer: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllManageCustomerSuccess = (state, action) => update(state, {
  getAllManageCustomer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Get Customer success' }
  }
});

const getAllManageCustomerError = (state, action) => update(state, {
  getAllManageCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const editCustomerRequest = (state, action) => update(state, {
  editCustomer: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editCustomerClear = (state, action) => update(state, {
  editCustomer: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editCustomerSuccess = (state, action) => update(state, {
  editCustomer: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Edit Customer success' }
  }
});

const editCustomerError = (state, action) => update(state, {
  editCustomer: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

//-------------------- GET ALL ITEMS --------------------
const getAllManageItemRequest = (state, action) => update(state, {
  getAllManageItem: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllManageItemClear = (state, action) => update(state, {
  getAllManageItem: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllManageItemSuccess = (state, action) => update(state, {
  getAllManageItem: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Get Item success' }
  }
});

const getAllManageItemError = (state, action) => update(state, {
  getAllManageItem: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

// Send Email :::::
const sendEmailRequest = (state, action) => update(state, {
  sendEmail: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const sendEmailClear = (state, action) => update(state, {
  sendEmail: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const sendEmailSuccess = (state, action) => update(state, {
  sendEmail: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const sendEmailError = (state, action) => update(state, {
  sendEmail: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

//-------------------- GET ALL SALES AGENTS --------------------
const getAllManageSalesAgentRequest = (state, action) => update(state, {
  getAllManageSalesAgent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllManageSalesAgentClear = (state, action) => update(state, {
  getAllManageSalesAgent: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllManageSalesAgentSuccess = (state, action) => update(state, {
  getAllManageSalesAgent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Get SalesAgent success' }
  }
});

const getAllManageSalesAgentError = (state, action) => update(state, {
  getAllManageSalesAgent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

// KYC DETAILS::: 
const getKycDetailsRequest = (state, action) => update(state, {
  getKycDetails: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const saveKycDetailsSuccess = (state, action) => update(state, {
  saveKycDetails: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const saveKycDetailsError = (state, action) => update(state, {
  saveKycDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const saveKycDetailsClear = (state, action) => update(state, {
  saveKycDetails: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

// VENDOR KYC REQUEST::::
const saveKycDetailsRequest = (state, action) => update(state, {
  saveKycDetails: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const getKycDetailsSuccess = (state, action) => update(state, {
  getKycDetails: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getKycDetailsClear = (state, action) => update(state, {
  getKycDetails: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const getKycDetailsError = (state, action) => update(state, {
  getKycDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

export default handleActions({
  [constants.VENDOR_REQUEST]: vendorRequest,
  [constants.VENDOR_SUCCESS]: vendorSuccess,
  [constants.VENDOR_ERROR]: vendorError,
  [constants.DELETE_VENDOR_CLEAR]: deleteVendorClear,
  [constants.DELETE_VENDOR_REQUEST]: deleteVendorRequest,
  [constants.DELETE_VENDOR_SUCCESS]: deleteVendorSuccess,
  [constants.DELETE_VENDOR_ERROR]: deleteVendorError,
  [constants.ADD_VENDOR_CLEAR]: addVendorClear,
  [constants.ADD_VENDOR_REQUEST]: addVendorRequest,
  [constants.ADD_VENDOR_SUCCESS]: addVendorSuccess,
  [constants.ADD_VENDOR_ERROR]: addVendorError,
  [constants.EDIT_VENDOR_CLEAR]: editVendorClear,
  [constants.EDIT_VENDOR_REQUEST]: editVendorRequest,
  [constants.EDIT_VENDOR_SUCCESS]: editVendorSuccess,
  [constants.EDIT_VENDOR_ERROR]: editVendorError,

  [constants.GET_ALL_MANAGE_VENDOR_CLEAR]: getAllManageVendorClear,
  [constants.GET_ALL_MANAGE_VENDOR_REQUEST]: getAllManageVendorRequest,
  [constants.GET_ALL_MANAGE_VENDOR_SUCCESS]: getAllManageVendorSuccess,
  [constants.GET_ALL_MANAGE_VENDOR_ERROR]: getAllManageVendorError,

  [constants.CREATE_VENDOR_CLEAR]: createVendorClear,
  [constants.CREATE_VENDOR_REQUEST]: createVendorRequest,
  [constants.CREATE_VENDOR_SUCCESS]: createVendorSuccess,
  [constants.CREATE_VENDOR_ERROR]: createVendorError,

  [constants.UPDATE_MANAGE_VENDOR_CLEAR]: updateManageVendorClear,
  [constants.UPDATE_MANAGE_VENDOR_REQUEST]: updateManageVendorRequest,
  [constants.UPDATE_MANAGE_VENDOR_SUCCESS]: updateManageVendorSuccess,
  [constants.UPDATE_MANAGE_VENDOR_ERROR]: updateManageVendorError,

  [constants.UPDATE_CREATED_VENDOR_REQUEST]: updateCreatedVendorRequest,

  [constants.GET_ALL_MANAGE_TRANSPORTER_CLEAR]: getAllManageTransporterClear,
  [constants.GET_ALL_MANAGE_TRANSPORTER_REQUEST]: getAllManageTransporterRequest,
  [constants.GET_ALL_MANAGE_TRANSPORTER_SUCCESS]: getAllManageTransporterSuccess,
  [constants.GET_ALL_MANAGE_TRANSPORTER_ERROR]: getAllManageTransporterError,

  [constants.CREATE_TRANSPORTER_CLEAR]: createTransporterClear,
  [constants.CREATE_TRANSPORTER_REQUEST]: createTransporterRequest,
  [constants.CREATE_TRANSPORTER_SUCCESS]: createTransporterSuccess,
  [constants.CREATE_TRANSPORTER_ERROR]: createTransporterError,

  [constants.UPDATE_MANAGE_TRANSPORTER_CLEAR]: updateManageTransporterClear,
  [constants.UPDATE_MANAGE_TRANSPORTER_REQUEST]: updateManageTransporterRequest,
  [constants.UPDATE_MANAGE_TRANSPORTER_SUCCESS]: updateManageTransporterSuccess,
  [constants.UPDATE_MANAGE_TRANSPORTER_ERROR]: updateManageTransporterError,

  [constants.UPDATE_CREATED_TRANSPORTER_REQUEST]: updateCreatedTransporterRequest,

  [constants.ACCESS_VENDOR_PORTAL_CLEAR]: accessVendorPortalClear,
  [constants.ACCESS_VENDOR_PORTAL_REQUEST]: accessVendorPortalRequest,
  [constants.ACCESS_VENDOR_PORTAL_SUCCESS]: accessVendorPortalSuccess,
  [constants.ACCESS_VENDOR_PORTAL_ERROR]: accessVendorPortalError,

  [constants.GET_ALL_MANAGE_CUSTOMER_CLEAR]: getAllManageCustomerClear,
  [constants.GET_ALL_MANAGE_CUSTOMER_REQUEST]: getAllManageCustomerRequest,
  [constants.GET_ALL_MANAGE_CUSTOMER_SUCCESS]: getAllManageCustomerSuccess,
  [constants.GET_ALL_MANAGE_CUSTOMER_ERROR]: getAllManageCustomerError,

  [constants.EDIT_CUSTOMER_CLEAR]: editCustomerClear,
  [constants.EDIT_CUSTOMER_REQUEST]: editCustomerRequest,
  [constants.EDIT_CUSTOMER_SUCCESS]: editCustomerSuccess,
  [constants.EDIT_CUSTOMER_ERROR]: editCustomerError,

  [constants.GET_ALL_MANAGE_ITEM_CLEAR]: getAllManageItemClear,
  [constants.GET_ALL_MANAGE_ITEM_REQUEST]: getAllManageItemRequest,
  [constants.GET_ALL_MANAGE_ITEM_SUCCESS]: getAllManageItemSuccess,
  [constants.GET_ALL_MANAGE_ITEM_ERROR]: getAllManageItemError,

  [constants.GET_ALL_MANAGE_SALES_AGENT_CLEAR]: getAllManageSalesAgentClear,
  [constants.GET_ALL_MANAGE_SALES_AGENT_REQUEST]: getAllManageSalesAgentRequest,
  [constants.GET_ALL_MANAGE_SALES_AGENT_SUCCESS]: getAllManageSalesAgentSuccess,
  [constants.GET_ALL_MANAGE_SALES_AGENT_ERROR]: getAllManageSalesAgentError,
  
  [constants.SEND_EMAIL_CLEAR]: sendEmailClear,
  [constants.SEND_EMAIL_REQUEST]: sendEmailRequest,
  [constants.SEND_EMAIL_SUCCESS]: sendEmailSuccess,
  [constants.SEND_EMAIL_ERROR]: sendEmailError,

  [constants.SAVE_KYC_DETAILS_CLEAR]: saveKycDetailsClear,
  [constants.SAVE_KYC_DETAILS_REQUEST]: saveKycDetailsRequest,
  [constants.SAVE_KYC_DETAILS_SUCCESS]: saveKycDetailsSuccess,
  [constants.SAVE_KYC_DETAILS_ERROR]: saveKycDetailsError,

  [constants.GET_KYC_DETAILS_CLEAR]: getKycDetailsClear,
  [constants.GET_KYC_DETAILS_REQUEST]: getKycDetailsRequest,
  [constants.GET_KYC_DETAILS_SUCCESS]: getKycDetailsSuccess,
  [constants.GET_KYC_DETAILS_ERROR]: getKycDetailsError,

}, initialState);
