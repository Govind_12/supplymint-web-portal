import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import { OganisationIdName } from '../../../organisationIdName.js';
import loginAjax from '../../../services/authServices';
import { AUTH_CONFIG } from '../../../authConfig/index';

export function* vendorRequest(action) {
  try {
    let no = action.payload == undefined || action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload == undefined || action.payload.type == undefined ? 1 : action.payload.type;
    let search = action.payload == undefined || action.payload.search == undefined ? "" : action.payload.search
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/vendorportal/vendorlogi/get/all/suppliers?type=${type}&pageNo=${no}&search=${search}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.vendorSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.vendorError(response.data));
    }

  } catch (e) {
    yield put(actions.vendorError("error occurs"));
    console.warn('Some error found in "vendorRequest" action\n', e);
  }
}

export function* deleteVendorRequest(action) {
  let id = action.payload;
  if (action.payload == undefined) {
    yield put(actions.deleteVendorClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.VENDOR}/delete/${id}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.vendorRequest());
        yield put(actions.deleteVendorSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.deleteVendorError(response.data.error));
      }
    } catch (e) {
      yield put(actions.deleteVendorError("error occurs"));
      console.warn('Some error found in "getVendorRequest" action\n', e);
    }
  }
}


export function* addVendorRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.addVendorClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR}/create`, {

        'organisationName': action.payload.organisationName,
        'vendorName': action.payload.vendorName,
        'vendorCode': action.payload.vendorCode,
        'description': action.payload.description,
        'country': action.payload.country,
        'state': action.payload.state,
        'city': action.payload.city,
        'zipCode': action.payload.zipCode,
        'url': action.payload.url,
        'email': action.payload.email,
        'notes': action.payload.notes,
        'fax': action.payload.fax,
        'phone': action.payload.phone,
        'contactName': action.payload.contactName,
        'contactPhone': action.payload.contactPhone,
        'contactEmail': action.payload.contactEmail,
        'contactAddress': action.payload.contactAddress,
        'shippingCNTName': action.payload.shippingCNTName,
        'shippingPhone': action.payload.shippingPhone,
        'shippingEmail': action.payload.shippingEmail,
        'shippingCNTAddress': action.payload.shippingCNTAddress,
        'purchasingAddress': action.payload.purchasingAddress,
        'paymentTerms': action.payload.paymentTerms,
        'orderLeadTimeDays': action.payload.orderLeadTimeDays,
        'status': action.payload.status,





      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.addVendorSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.addVendorError(response.data));
      }
    } catch (e) {
      yield put(actions.addVendorError("error occurs"));
      console.warn('Some error found in "addVendorRequest" action\n', e);
    }
  }
}

export function* editVendorRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.editVendorClear());
  }
  else {
    try {

      var { orgIdGlobal } = OganisationIdName()
      let xyz = [];
      let abc = "";
      if (action.payload.selected != undefined) {

        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].id;
          if (action.payload.selected.length > 1) {
            abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
          } else {
            abc = a;
          }
        }


        for (let i = 0; i < action.payload.selected.length; i++) {
          xyz.push(action.payload.selected[i].name)
        }
      }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.USER}/vendor/enable/access`, {
        'firstName': action.payload.first,
        //'middleName': action.payload.middle,
        'lastName': action.payload.last,
        'email': action.payload.email,
        'mobileNumber': action.payload.mobile,
        'workPhone': action.payload.phone,
        'accessMode': action.payload.access,
        'status': action.payload.active,
        'address': action.payload.address,
        'partnerEnterpriseId': sessionStorage.getItem('partnerEnterpriseId'),
        'partnerEnterpriseName': sessionStorage.getItem('partnerEnterpriseName'),
        'userName': action.payload.user,
        'userRoles': xyz,
        'roles': abc,
        "uType": action.payload.uType,
        "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
        "slName": action.payload.slName != undefined ? action.payload.slName : "",
        "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
        "userAsAdmin": action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

        // "uType": action.payload.uType,
        // "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
        // "slName": action.payload.slName != undefined ? action.payload.slName : "",
        // "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal


        // const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR}/update`, {
        //   'organisationName': action.payload.organisationName,
        //   'vendorName': action.payload.vendorName,
        //   'vendorCode': action.payload.vendorCode,
        //   'description': action.payload.description,
        //   'country': action.payload.country,
        //   'state': action.payload.state,
        //   'city': action.payload.city,
        //   'zipCode': action.payload.zipCode,
        //   'url':action.payload.url,
        //   'email':action.payload.email,
        //   'notes':action.payload.notes,
        //   'fax':action.payload.fax,
        //   'phone':action.payload.phone,
        //   'contactName':action.payload.contactName,
        //   'contactPhone':action.payload.contactPhone,
        //   'contactEmail':action.payload.contactEmail,
        //   'contactAddress':action.payload.contactAddress,
        //   'shippingCNTName':action.payload.shippingCNTName,
        //   'shippingPhone':action.payload.shippingPhone,
        //   'shippingEmail':action.payload.shippingEmail,
        //   'shippingCNTAddress':action.payload.shippingCNTAddress,
        //   'purchasingAddress':action.payload.purchasingAddress,
        //   'paymentTerms':action.payload.paymentTerms,
        //   'orderLeadTimeDays':action.payload.orderLeadTimeDays,
        //   'status':action.payload.status,
        //   'id': action.payload.id

        // const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR}/update`, {
        //   'organisationName': action.payload.organisationName,
        //   'vendorName': action.payload.vendorName,
        //   'vendorCode': action.payload.vendorCode,
        //   'description': action.payload.description,
        //   'country': action.payload.country,
        //   'state': action.payload.state,
        //   'city': action.payload.city,
        //   'zipCode': action.payload.zipCode,
        //   'url':action.payload.url,
        //   'email':action.payload.email,
        //   'notes':action.payload.notes,
        //   'fax':action.payload.fax,
        //   'phone':action.payload.phone,
        //   'contactName':action.payload.contactName,
        //   'contactPhone':action.payload.contactPhone,
        //   'contactEmail':action.payload.contactEmail,
        //   'contactAddress':action.payload.contactAddress,
        //   'shippingCNTName':action.payload.shippingCNTName,
        //   'shippingPhone':action.payload.shippingPhone,
        //   'shippingEmail':action.payload.shippingEmail,
        //   'shippingCNTAddress':action.payload.shippingCNTAddress,
        //   'purchasingAddress':action.payload.purchasingAddress,
        //   'paymentTerms':action.payload.paymentTerms,
        //   'orderLeadTimeDays':action.payload.orderLeadTimeDays,
        //   'status':action.payload.status,
        //   'id': action.payload.id

      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.editVendorSuccess(response.data.data));
        yield put(actions.vendorRequest());

      } else if (finalResponse.failure) {
        yield put(actions.editVendorError(response.data));
      }
    } catch (e) {
      yield put(actions.editVendorError("error occurs"));
      console.warn('Some error found in "editVendorRequest" action\n', e);
    }
  }
}


//get vedor request new

export function* getAllManageVendorRequest(action) {
  try {
    //this is for getting vendor code for ledger report.
    let key =  action.payload.key !== undefined ? "" : "/all"
    let fixKeys = {
      pageNo: action.payload.pageNo == undefined ? 1 : action.payload.pageNo,
      type: action.payload.type == undefined ? 1 : action.payload.type,
      search: action.payload.search == undefined ? "" : action.payload.search,
      sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
      sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
      isActive: action.payload.isActive == undefined ? "" : action.payload.isActive,
    }
    let filter = { ...action.payload.filter }
    let payload = { ...fixKeys, filter }

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/find${key}/vendor`, payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getAllManageVendorSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.getAllManageVendorError(response.data));
    }
  } catch (e) {
    yield put(actions.getAllManageVendorError("error occurs"));
    console.warn('Some error found in "getAllManageVendorRequest" action\n', e);
  }
}

// // create vendor
export function* createVendorRequest(action) {
  try {
    // let payload = {
    //   slCode: action.payload.slCode == undefined ? "" : action.payload.slCode,
    //   slName: action.payload.slName == undefined ? "" : action.payload.slName,
    //   slId: action.payload.slId == undefined ? "" : action.payload.slId,
    //   firstName: action.payload.firstName == undefined ? "" : action.payload.firstName,
    //   lastName: action.payload.lastName == undefined ? "" : action.payload.lastName,
    //   email: action.payload.email == undefined ? "" : action.payload.email,
    //   alternateemail: action.payload.alternateemail == undefined ? "" : action.payload.alternateemail,
    //   contactNumber: action.payload.contactNumber == undefined ? "" : action.payload.contactNumber,
    //   slState: action.payload.state == undefined ? "" : action.payload.state,
    //   slCityName: action.payload.city == undefined ? "" : action.payload.city,
    //   slAddress: action.payload.slAddress == undefined ? "" : action.payload.slAddress,
    //   shippingName: action.payload.shippingName == undefined ? "" : action.payload.shippingName,
    //   shippedAddress: action.payload.shippedAddress == undefined ? "" : action.payload.shippedAddress,
    //   shippedGSTNo: action.payload.shippedGSTNo == undefined ? "" : action.payload.shippedGSTNo,
    //   shippedStateCode: action.payload.shippedStateCode == undefined ? "" : action.payload.shippedStateCode,
    //   shippedStateName: action.payload.shippedStateName == undefined ? "" : action.payload.shippedStateName,
    //   isDistinctShipAddress: action.payload.isDistinctShipAddress == undefined ? "" : action.payload.isDistinctShipAddress,
    //   purchaseTermCodeCode: action.payload.purchaseTermCodeCode == undefined ? "" : action.payload.purchaseTermCodeCode,
    //   purchaseTermName: action.payload.purchaseTermName == undefined ? "" : action.payload.purchaseTermName,
    //   gstInNo: action.payload.gstInNo == undefined ? "" : action.payload.gstInNo,
    //   gstInStateCode: action.payload.gstInStateCode == undefined ? "" : action.payload.gstInStateCode,
    //   tradeGrpCode: action.payload.tradeGrpCode == undefined ? "" : action.payload.tradeGrpCode,
    //   deliveryBuffDays: action.payload.deliveryBuffDays == undefined ? "" : action.payload.deliveryBuffDays,
    //   dueDateBasis: action.payload.dueDateBasis == undefined ? "" : action.payload.dueDateBasis,
    //   dueDays: action.payload.dueDays == undefined ? "" : action.payload.dueDays,
    //   shippedStateName: action.payload.shippedStateName == undefined ? "" : action.payload.shippedStateName,
    // }
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/create/vendor`, action.payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.createVendorSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.createVendorError(response.data));
    }
  } catch (e) {
    yield put(actions.createVendorError("error occurs"));
    console.warn('Some error found in "getAllManageVendorRequest" action\n', e);
  }
}

//passing data to other component
export function* updateCreatedVendorRequest(action) {
}

//update manage vendor
export function* updateManageVendorRequest(action) {
  console.log("in manage vendor api")
  try {

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/update/vendor`, action.payload);
    console.log("after api")
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.updateManageVendorSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.updateManageVendorError(response.data));
    }
  } catch (e) {
    yield put(actions.updateManageVendorError("error occurs"));
    console.warn('Some error found in "updateManageVendor" action\n', e);
  }
}


//GET TRANSPORTERS REQUEST
export function* getAllManageTransporterRequest(action) {
  try {
    let fixKeys = {
      pageNo: action.payload.no == undefined ? 1 : action.payload.no,
      type: action.payload.type == undefined ? 1 : action.payload.type,
      search: action.payload.search == undefined ? "" : action.payload.search,
      sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
      sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
    }
    let filter = { ...action.payload.filter }
    let payload = { ...fixKeys, filter }

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/find/all/transporters`, payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getAllManageTransporterSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.getAllManageTransporterError(response.data));
    }
  } catch (e) {
    yield put(actions.getAllManageTransporterError("error occurs"));
    console.warn('Some error found in "getAllManageTransporterRequest" action\n', e);
  }
}

export function* createTransporterRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.createTransporterClear());
  }
  try {
    // let payload = {
    //   slCode: action.payload.slCode == undefined ? "" : action.payload.slCode,
    //   slName: action.payload.slName == undefined ? "" : action.payload.slName,
    //   slId: action.payload.slId == undefined ? "" : action.payload.slId,
    //   firstName: action.payload.firstName == undefined ? "" : action.payload.firstName,
    //   lastName: action.payload.lastName == undefined ? "" : action.payload.lastName,
    //   email: action.payload.email == undefined ? "" : action.payload.email,
    //   alternateemail: action.payload.alternateemail == undefined ? "" : action.payload.alternateemail,
    //   contactNumber: action.payload.contactNumber == undefined ? "" : action.payload.contactNumber,
    //   slState: action.payload.state == undefined ? "" : action.payload.state,
    //   slCityName: action.payload.city == undefined ? "" : action.payload.city,
    //   slAddress: action.payload.slAddress == undefined ? "" : action.payload.slAddress,
    //   shippingName: action.payload.shippingName == undefined ? "" : action.payload.shippingName,
    //   shippedAddress: action.payload.shippedAddress == undefined ? "" : action.payload.shippedAddress,
    //   shippedGSTNo: action.payload.shippedGSTNo == undefined ? "" : action.payload.shippedGSTNo,
    //   shippedStateCode: action.payload.shippedStateCode == undefined ? "" : action.payload.shippedStateCode,
    //   shippedStateName: action.payload.shippedStateName == undefined ? "" : action.payload.shippedStateName,
    //   isDistinctShipAddress: action.payload.isDistinctShipAddress == undefined ? "" : action.payload.isDistinctShipAddress,
    //   purchaseTermCodeCode: action.payload.purchaseTermCodeCode == undefined ? "" : action.payload.purchaseTermCodeCode,
    //   purchaseTermName: action.payload.purchaseTermName == undefined ? "" : action.payload.purchaseTermName,
    //   gstInNo: action.payload.gstInNo == undefined ? "" : action.payload.gstInNo,
    //   gstInStateCode: action.payload.gstInStateCode == undefined ? "" : action.payload.gstInStateCode,
    //   tradeGrpCode: action.payload.tradeGrpCode == undefined ? "" : action.payload.tradeGrpCode,
    //   deliveryBuffDays: action.payload.deliveryBuffDays == undefined ? "" : action.payload.deliveryBuffDays,
    //   dueDateBasis: action.payload.dueDateBasis == undefined ? "" : action.payload.dueDateBasis,
    //   dueDays: action.payload.dueDays == undefined ? "" : action.payload.dueDays,
    //   shippedStateName: action.payload.shippedStateName == undefined ? "" : action.payload.shippedStateName,
    // }
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/create/transporter`, action.payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.createTransporterSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.createTransporterError(response.data));
    }
  } catch (e) {
    yield put(actions.createTransporterError("error occurs"));
    console.warn('Some error found in "createTransporterRequest" action\n', e);
  }
}

//passing data to other component
export function* updateCreatedTransporterRequest(action) {
}

export function* updateManageTransporterRequest(action) {
  console.log("in manage vendor api")
  try {

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/update/transporters`, action.payload);
    console.log("after api")
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.updateManageTransporterSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.updateManageTransporterError(response.data));
    }
  } catch (e) {
    yield put(actions.updateManageTransporterError("error occurs"));
    console.warn('Some error found in "updateManageTransporter" action\n', e);
  }
}

// ACCESS VENDOR PORTAL::
export function* accessVendorPortalRequest(action) {
    if (action.payload == undefined) {
      yield put(actions.accessVendorPortalClear());
    }
    else {
      try {
          let username = action.payload.userName == undefined || action.payload.userName == null ? "" : action.payload.userName
          let token = action.payload.token == undefined || action.payload.token == null ? "" : action.payload.token

          const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}${AUTH_CONFIG.LOGIN}/retailer/vendor`, {
            username,
            token
          });
          const finalResponse = script(response);
          if (finalResponse.success) {
            yield put(actions.accessVendorPortalSuccess(response.data.data));
            yield put(actions.vendorRequest());
          } else if (finalResponse.failure) {
            yield put(actions.accessVendorPortalError(response.data));
          }
      } catch (e) {
      yield put(actions.accessVendorPortalError("error occurs"));
      console.warn('Some error found in "accessVendorPortal" action\n', e);
    }
  }
}


//-------------------- GET ALL CUSTOMERS --------------------
export function* getAllManageCustomerRequest(action) {
  try {
    let fixKeys = {
      pageNo: action.payload.no == undefined ? 1 : action.payload.no,
      type: action.payload.type == undefined ? 1 : action.payload.type,
      search: action.payload.search == undefined ? "" : action.payload.search,
      sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
      sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
    }
    let filter = { ...action.payload.filter }
    let payload = { ...fixKeys, filter }

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/find/all/customer`, payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getAllManageCustomerSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.getAllManageCustomerError(response.data));
    }
  } catch (e) {
    yield put(actions.getAllManageCustomerError("error occurs"));
    console.warn('Some error found in "getAllManageCustomerRequest" action\n', e);
  }
}

export function* editCustomerRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.editCustomerClear());
  }
  else {
    try {

      var { orgIdGlobal } = OganisationIdName()
      let xyz = [];
      let abc = "";
      if (action.payload.selected != undefined) {

        for (let i = 0; i < action.payload.selected.length; i++) {
          let a = action.payload.selected[i].id;
          if (action.payload.selected.length > 1) {
            abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
          } else {
            abc = a;
          }
        }


        for (let i = 0; i < action.payload.selected.length; i++) {
          xyz.push(action.payload.selected[i].name)
        }
      }

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.USER}/customer/enable/access`, {
        'firstName': action.payload.first,
        //'middleName': action.payload.middle,
        'lastName': action.payload.last,
        'email': action.payload.email,
        'mobileNumber': action.payload.mobile,
        'workPhone': action.payload.phone,
        'accessMode': action.payload.access,
        'status': action.payload.active,
        'address': action.payload.address,
        'partnerEnterpriseId': sessionStorage.getItem('partnerEnterpriseId'),
        'partnerEnterpriseName': sessionStorage.getItem('partnerEnterpriseName'),
        'userName': action.payload.user,
        'userRoles': xyz,
        'roles': abc,
        "uType": action.payload.uType,
        "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
        "slName": action.payload.slName != undefined ? action.payload.slName : "",
        "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal,
        "userAsAdmin": action.payload.userAsAdmin != undefined && action.payload.userAsAdmin

        // "uType": action.payload.uType,
        // "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
        // "slName": action.payload.slName != undefined ? action.payload.slName : "",
        // "organisationId": action.payload.orgId != undefined ? action.payload.orgId : orgIdGlobal


        // const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR}/update`, {
        //   'organisationName': action.payload.organisationName,
        //   'vendorName': action.payload.vendorName,
        //   'vendorCode': action.payload.vendorCode,
        //   'description': action.payload.description,
        //   'country': action.payload.country,
        //   'state': action.payload.state,
        //   'city': action.payload.city,
        //   'zipCode': action.payload.zipCode,
        //   'url':action.payload.url,
        //   'email':action.payload.email,
        //   'notes':action.payload.notes,
        //   'fax':action.payload.fax,
        //   'phone':action.payload.phone,
        //   'contactName':action.payload.contactName,
        //   'contactPhone':action.payload.contactPhone,
        //   'contactEmail':action.payload.contactEmail,
        //   'contactAddress':action.payload.contactAddress,
        //   'shippingCNTName':action.payload.shippingCNTName,
        //   'shippingPhone':action.payload.shippingPhone,
        //   'shippingEmail':action.payload.shippingEmail,
        //   'shippingCNTAddress':action.payload.shippingCNTAddress,
        //   'purchasingAddress':action.payload.purchasingAddress,
        //   'paymentTerms':action.payload.paymentTerms,
        //   'orderLeadTimeDays':action.payload.orderLeadTimeDays,
        //   'status':action.payload.status,
        //   'id': action.payload.id

        // const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR}/update`, {
        //   'organisationName': action.payload.organisationName,
        //   'vendorName': action.payload.vendorName,
        //   'vendorCode': action.payload.vendorCode,
        //   'description': action.payload.description,
        //   'country': action.payload.country,
        //   'state': action.payload.state,
        //   'city': action.payload.city,
        //   'zipCode': action.payload.zipCode,
        //   'url':action.payload.url,
        //   'email':action.payload.email,
        //   'notes':action.payload.notes,
        //   'fax':action.payload.fax,
        //   'phone':action.payload.phone,
        //   'contactName':action.payload.contactName,
        //   'contactPhone':action.payload.contactPhone,
        //   'contactEmail':action.payload.contactEmail,
        //   'contactAddress':action.payload.contactAddress,
        //   'shippingCNTName':action.payload.shippingCNTName,
        //   'shippingPhone':action.payload.shippingPhone,
        //   'shippingEmail':action.payload.shippingEmail,
        //   'shippingCNTAddress':action.payload.shippingCNTAddress,
        //   'purchasingAddress':action.payload.purchasingAddress,
        //   'paymentTerms':action.payload.paymentTerms,
        //   'orderLeadTimeDays':action.payload.orderLeadTimeDays,
        //   'status':action.payload.status,
        //   'id': action.payload.id

      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.editCustomerSuccess(response.data.data));
        yield put(actions.vendorRequest());

      } else if (finalResponse.failure) {
        yield put(actions.editCustomerError(response.data));
      }
    } catch (e) {
      yield put(actions.editCustomerError("error occurs"));
      console.warn('Some error found in "editCustomerRequest" action\n', e);
    }
  }
}


//-------------------- GET ALL ITEMS --------------------
export function* getAllManageItemRequest(action) {
  try {
    let fixKeys = {
      pageNo: action.payload.no == undefined ? 1 : action.payload.no,
      type: action.payload.type == undefined ? 1 : action.payload.type,
      search: action.payload.search == undefined ? "" : action.payload.search,
      sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
      sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
    }
    let filter = { ...action.payload.filter }
    let payload = { ...fixKeys, filter }

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/get/genericItemMaster`, payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getAllManageItemSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.getAllManageItemError(response.data));
    }
  } catch (e) {
    yield put(actions.getAllManageItemError("error occurs"));
    console.warn('Some error found in "getAllManageItemRequest" action\n', e);
  }
}

//-------------------- GET ALL SALES AGENTS --------------------
export function* getAllManageSalesAgentRequest(action) {
  try {
    let fixKeys = {
      pageNo: action.payload.no == undefined ? 1 : action.payload.no,
      type: action.payload.type == undefined ? 1 : action.payload.type,
      search: action.payload.search == undefined ? "" : action.payload.search,
      sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
      sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
    }
    let filter = { ...action.payload.filter }
    let payload = { ...fixKeys, filter }

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/managevendor/get/all/salesAgent`, payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getAllManageSalesAgentSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.getAllManageSalesAgentError(response.data));
    }
  } catch (e) {
    yield put(actions.getAllManageSalesAgentError("error occurs"));
    console.warn('Some error found in "getAllManageSalesAgentRequest" action\n', e);
  }
}

export function* sendEmailRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.sendEmailClear());
  }
  else {
      try {
          let username = action.payload.userName == undefined || action.payload.userName == null ? "" : action.payload.userName
          let token = action.payload.token == undefined || action.payload.token == null ? "" : action.payload.token

          const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.USER}/vendor/enable/access/send/email`, action.payload);
          const finalResponse = script(response);
          if (finalResponse.success) {
            yield put(actions.sendEmailSuccess(response.data.data));
            yield put(actions.vendorRequest());
          } else if (finalResponse.failure) {
            yield put(actions.sendEmailError(response.data));
          }
      } catch (e) {
          yield put(actions.sendEmailError("error occurs"));
          console.warn('Some error found in "sendEmailRequest" action\n', e);
      }
    }
}
// SAVE KYC DETAILS::
export function* saveKycDetailsRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.saveKycDetailsClear());
  }
  else {
        try {
            
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/save/kyc`, action.payload)
            const finalResponse = script(response);
            if (finalResponse.success) {
              yield put(actions.saveKycDetailsSuccess(response.data.data));
            } else if (finalResponse.failure) {
              yield put(actions.saveKycDetailsError(response.data));
            }
        } catch (e) {
              yield put(actions.saveKycDetailsError("error occurs"));
              console.warn('Some error found in "saveKycDetailsRequest" action\n', e);
        }
  }
}

// GET KYC DETAILS::
export function* getKycDetailsRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getKycDetailsClear());
  }
  else {
        try {
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/get/kyc`)
            const finalResponse = script(response);
            if (finalResponse.success) {
              yield put(actions.getKycDetailsSuccess(response.data.data));
            } else if (finalResponse.failure) {
              yield put(actions.getKycDetailsError(response.data));
            }
        } catch (e) {
              yield put(actions.getKycDetailsError("error occurs"));
              console.warn('Some error found in "getKycDetailsRequest" action\n', e);
        }
  }
}