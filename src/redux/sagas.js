import { takeEvery, takeLatest } from "redux-saga/effects";

import * as constants from "./constants";

// import {sampleRequest} from './sample/action/'

import { loginRequest, changePasswordRequest, forgotPasswordRequest, resetPasswordRequest, forgotUserRequest, activateUserRequest, getUserNameRequest, updateProfileRequest, getProfileDetailsRequest, updateUserDetailsRequest, getSupportAccessRequest, createSupportAccessRequest } from "./login/action/";

import { customApplyFilterRequest, customGetPartnerRequest, customGetGradeRequest, customGetStoreCodeRequest, customGetZoneRequest, customMasterSkechersRequest, customGetEventRequest, customDeleteEventRequest, updateRoleRequest, getSubModuleDataRequest, checkRoleNameRequest, roleSubmitRequest, roleUpdateDataRequest, moduleGetDataRequest, moduleGetRoleRequest, dataSyncRequest, downloadRequest, getTemplateRequest, saveSketchersRequest, sketchersUploadRequest, allUserRequest, allRoleRequest, fromToRequest, addUserRequest, addRolesRequest, deleteSiteRequest, deleteRolesRequest, deleteUserRequest, addOrganizationRequest, editOrganizationRequest, deleteOrganizationRequest, deleteSiteMappingRequest, addSiteMappingRequest, editSiteMappingRequest, addSiteRequest, editSiteRequest, rolesRequest, editRolesRequest, siteMappingRequest, organizationRequest, userRequest, editUserRequest, userStatusRequest, siteRequest, allFestivalRequest, createFestivalRequest, promotionalRequest, getPromotionalEventRequest, updatePromotionalEventRequest, disablePromotionalEventRequest, deactivePromotionalEventRequest, paramsettingRequest, parameterCustomRequest, proxyStoreRequest, getSubModuleByRoleNameRequest,
  allRetailerRequest, vendorUserRequest, getModuleApiLogsRequest, roleDeleteRequest, getSubscriptionRequest, getTransactionRequest, getApiAccessDetailsRequest, getEmailNotificationLogRequest, resendEmailNotificationRequest, getEmailBodyRequest, getUserDataMappingRequest, updateUserDataMappingRequest,getInsertRequest,saveInsertRequest, allRetailerCustomerRequest, customerUserRequest, addUserCustomerRequest, editUserCustomerRequest, userStatusCustomerRequest, getSubscriptionCustomerRequest, getTransactionCustomerRequest } from "./administration/action/";
// import { customApplyFilterRequest, customGetPartnerRequest, customGetGradeRequest, customGetStoreCodeRequest, customGetZoneRequest, customMasterSkechersRequest, customGetEventRequest, customDeleteEventRequest, upDateRoleRequest, getSubModuleDataRequest,checkRoleNameRequest,roleSubmitRequest,roleUpdateDataRequest, moduleGetDataRequest, moduleGetRoleRequest, dataSyncRequest, downloadRequest, getTemplateRequest, saveSketchersRequest, sketchersUploadRequest, allUserRequest, allRoleRequest, fromToRequest, addUserRequest, addRolesRequest, deleteSiteRequest, deleteRolesRequest, deleteUserRequest, addOrganizationRequest, editOrganizationRequest, deleteOrganizationRequest, deleteSiteMappingRequest, addSiteMappingRequest, editSiteMappingRequest, addSiteRequest, editSiteRequest, rolesRequest, editRolesRequest, siteMappingRequest, organizationRequest, userRequest, editUserRequest, userStatusRequest, siteRequest, allFestivalRequest, createFestivalRequest, promotionalRequest, getPromotionalEventRequest, updatePromotionalEventRequest, disablePromotionalEventRequest, deactivePromotionalEventRequest, paramsettingRequest, parameterCustomRequest } from "./administration/action/";

import { vendorRequest, deleteVendorRequest, addVendorRequest, editVendorRequest, getAllManageVendorRequest, createVendorRequest, updateCreatedVendorRequest, updateManageVendorRequest, getAllManageTransporterRequest, createTransporterRequest, updateManageTransporterRequest, updateCreatedTransporterRequest, accessVendorPortalRequest, getAllManageCustomerRequest, getAllManageItemRequest, getAllManageSalesAgentRequest, sendEmailRequest, saveKycDetailsRequest, getKycDetailsRequest, editCustomerRequest } from "./vendor/action/";
import {
  po_approve_reject_Request, gen_IndentBasedRequest, gen_SetBasedRequest, gen_PurchaseIndentRequest, gen_PurchaseOrderRequest, poArticleRequest, discountRequest, departmentSetBasedRequest, getItemDetailsValueRequest, getItemDetailsRequest, marginRuleRequest, piAddNewRequest, articleNameRequest, cnameRequest, getItemUdfRequest, updateSizeDeptRequest, updateItemUdfRequest, updateItemCatDescRequest, createItemUdfRequest, getCatDescUdfRequest, updateUdfMappingRequest, itemCatDescUdfRequest, itemUdfMappingRequest, activeUdfRequest, activeCatDescRequest, createUdfSettingRequest, poSizeRequest, poCreateRequest, getUdfMappingRequest, poHistoryRequest, udfTypeRequest, getPoDataRequest, piUpdateRequest, piCreateRequest, markUpRequest, otbRequest, quantityRequest, totalRequest, getPurchaseTermRequest, purchaseTermRequest, colorRequest, supplierRequest, getTransporterRequest, transporterRequest, divisionSectionDepartmentRequest, leadTimeRequest, vendorMrpRequest, sizeRequest, lineItemRequest, piHistoryRequest, loadIndentRequest, articlePoRequest, poItemcodeRequest, poUdfMappingRequest, imageRequest,
  selectVendorRequest, selectOrderNumberRequest, setBasedRequest, hsnCodeRequest, multipleLineItemRequest, procurementSiteRequest, getPoItemCodeRequest, poItemBarcodeRequest, poRadioValidationRequest, fmcgGetHistoryRequest, getMultipleMarginRequest, getAllCityRequest, piArticleRequest, getLineItemRequest, getPiLineItemRequest, piEditRequest, editedPiSaveRequest, poSaveDraftRequest, piSaveDraftRequest, getDraftRequest, poEditRequest, editedPoSaveRequest, discardPoPiDataRequest, viewImagesRequest, piDashBottomRequest, piDashHierarchyRequest, emailDashboardSendRequest, piPoDashSiteRequest, piImageUrlRequest, getSlRequest, analyticsOtbRequest, catDescDropdownRequest  ,updateItemUdfMappingRequest, exportDashboardExcelRequest, mappingExcelUploadRequest, mappingExcelStatusRequest, po_approve_retry_Request, mappingExcelExportRequest, getAgentNameRequest
} from "./purchaseIndent/action/";

import {
  getMainHeaderConfigRequest, getSetHeaderConfigRequest, getItemHeaderConfigRequest,
  getHeaderConfigExcelRequest, updateHeaderLogsRequest, getHeaderConfigRequest, getCustomHeaderRequest, createHeaderConfigRequest,
  createMainHeaderConfigRequest, createItemHeaderConfigRequest, createSetHeaderConfigRequest,
  applyFilterRequest, toStatusRequest, getXLSLinkRequest, getCSVLinkRequest, getPartnerRequest, getZoneRequest, getGradeRequest, summaryCsvRequest, summaryDetailRequest, lastJobRequest, allStoreCodeRequest, createJobRequest, jobHistoryFilterRequest, getAllocationRequest, getRequirementSummaryRequest,
  jobHistoryRequest, nscheduleRequest, createTriggerRequest, getJobByNameRequest, jobRunDetailRequest, runOnDemandRequest, stopOnDemandRequest, getStoreCodeRequest, getFiveTriggersRequest, deleteTriggerRequest, getFilterSectionRequest, deleteConfigRequest, statusChangeRequest, invGetAllConfigRequest, getJobDataRequest, configCreateRequest, getJobNameRequest
  , glueParameterRequest, getAllJobRequest, getConfigFilterRequest, getMotherCompRequest, getArticleFmcgRequest, getVendorFmcgRequest, getStoreFmcgRequest, applyFilterFmcgRequest, customFilterMenuRequest, getInspOnOffConfigRequest, updateJobDataRequest, downloadRepReportRequest
} from "./replenishment/action";
// import { getMotherCompRequest, getArticleFmcgRequest, getVendorFmcgRequest, getStoreFmcgRequest, applyFilterFmcgRequest, updateHeaderLogsRequest, getHeaderConfigRequest, getCustomHeaderRequest, createHeaderConfigRequest, applyFilterRequest, toStatusRequest, getXLSLinkRequest, getCSVLinkRequest, getPartnerRequest, getZoneRequest, getGradeRequest, summaryCsvRequest, summaryDetailRequest, lastJobRequest, allStoreCodeRequest, createJobRequest, jobHistoryFilterRequest, jobHistoryRequest, nscheduleRequest, createTriggerRequest, getJobByNameRequest, jobRunDetailRequest, runOnDemandRequest, stopOnDemandRequest, getStoreCodeRequest, customFilterMenuRequest } from "./replenishment/action";

import { getAssortmentDropdownRequest, cancelAdhocRequest, getActivityRequest, editAdhocRequest, createAdhocRequest, getItemAdHockRequest, getSiteAdHockRequest, getAdHockRequest, hl1Request, hl2Request, hl3Request, hl4Request, assetCodeRequest, createAssortmentRequest, getAssortmentStatusRequest, viewMessageRequest, getAssortmentRequest, deleteItemCodeRequest, getAssortmentPlanningFiltersRequest, getAssortmentPlannigCountRequest, getAssortmentPlanningDetailRequest, saveAssortmentPlannigDetailRequest, getExcelDownloadedPoRequest, getExcelDownloadedPiRequest } from "./inventory management/action";

// import { supplierPoRequest, leadTimePoRequest, vendorMrpPoRequest } from "./purchaseOrder/action"

import { slowFastArticleRequest, salesTrendGraphRequest, storesArticlesRequest, dashboardTilesRequest, profileImageRequest, userSessionCreateRequest, switchOrganisationRequest, switchEntRequest, userActivityRequest, searchDataRequest } from "./home/action"
// import { slowFastArticleRequest, salesTrendGraphRequest, storesArticlesRequest, dashboardTilesRequest, profileImageRequest, userSessionCreateRequest, switchEntRequest } from "./home/action"

import { downloadPreviousForcastRequest, getAssortmentHistoryRequest, getOtbStatusRequest, getOtbPlanRequest, planSaleUpdateRequest, totalOtbRequest, removePlanRequest, getMakeCopyRequest, updateActivePlanRequest, createPlanRequest, updatePlanRequest, getActivePlanRequest, lastForecastRequest, getForecastStatusRequest, mwHistoryRequest, bsHistoryRequest, bsStatusRequest, dpProgressRequest, successBSRequest, verifyBSRequest, processingBSRequest, tableRequest, weeklyForRequest, monthlyForRequest, weeklyDPRequest, getForcastRequest, assortmentDPRequest, storeDPRequest, forcastRequest, getPreviousAssortmentRequest } from "./demandPlanning/action"

import { getAllConfigRequest, getAllOtbRequest, saveToCcRequest, getToCcRequest, updateEmailStatusRequest, getEmailStatusRequest, getPropertyRequest, getSubModuleRequest, getModuleRequest, getSettingRequest, createSettingRequest, settingOtbCreateRequest, settingAssortOtbRequest, otbGetHlevelRequest, otbGetDefaultRequest, purchaseOrderDateRangeRequest, getAsnFormatRequest, createAsnFormatRequest, updateToCcRequest, invoiceApprovalBasedLRRequest, getGeneralMappingRequest, updateGeneralMappingRequest, getAvailableKeysRequest, updateAvailableKeysRequest } from './changeSetting/action';

import { stockHandRequest, getSalesTrendRequest, getTopArticleRequest, generateTotalSalesRequest, generateUnitSoldRequest, getStoreProfileCodeRequest, getStoreDataRequest, getFestivalImpactRequest, getSellThruRequest, generateSalesPerSquareFootRequest, generateTotalSalesVsProfitRequest, getReviewReasonRequest, getDataWithoutFilterRequest, getfiltersArsDataRequest, getAssortmentDetailsRequest, getAssortmentTimePhasedRequest, getAssortmentSalesHistoryRequest, getAssortmentItemsRequest, getAllocationDataRequest, getAllStoreAnalysisRequest, createStoreAnalysisRequest, getDataStoreAnalysisRequest } from "./analytics/action";
import { EnterpriseApprovedPoRequest, EnterpriseGetPoDetailsRequest, EnterpriseCancelPoRequest, VendorCreateShipmentDetailsRequest, vendorCreateSrRequest, poCloserRequest, shipmentTrackingRequest, findShipmentAdviceRequest, commentNotificationRequest, dashboardUpperRequest, dashboardBottomRequest, getAllPlansRequest, getPaymentSummaryRequest, getOrderIdRequest, multipleDocumentDownloadRequest, getCapturePaymentRequest, emailTranscriptRequest, vendorDashboardFilterRequest, dashboardUnreadCommentRequest,updateHeaderRequest,getAddressRequest,getLocationRequest, getPaymentStatusRequest, approvePOConfirmRequest, getCancelAndRejectReasonRequest,
         getAllCommentsRequest, getAllLedgerReportRequest,getAllSaleReportRequest, getAllStockReportRequest, getAllOutStandingReportRequest, getLedgerFilterReportRequest, getVendorActivityRequest, getVendorLogsRequest, getPoStockReportRequest, getPoColorMapRequest, createPoArticleRequest } from "./vendorPortal/action";
import { getShipmentRequest, shipmentConfirmCancelRequest, getCompleteDetailShipmentRequest, getAllShipmentVendorRequest, updateVendorRequest, getShipmentDetailsRequest, getAllCommentRequest, qcAddCommentRequest, uploadCommentRequest, deleteUploadsRequest, updateVendorShipmentRequest, updateInvoiceRequest, addMultipleCommentQcRequest, getUserEmailsRequest } from "./vendorShipment/action";

import { getAllVendorShipmentShippedRequest, getAllVendorSsdetailRequest, getAllVendorTransitNDeliverRequest, getAllVendorSiDetailRequest, getAllVendorSupplierRequest, getAllVendorTransporterRequest, lrCreateRequest, updateDeliveryRequest, lrDetailsAutoRequest, getGrcItemRequest, createGrcRequest, getAllGrcRequest, getGrcDetailsRequest, updateExtensionDateRequest, getAllGateEntryRequest, getStatusButtonActiveRequest, getAllLogTransPayReportRequest, getAllLogWhStockReportRequest } from "./vendorLogistic/action"
import { createDataSyncTemplateRequest, downlaodTemplateRequest, uploadTemplateRequest, getAllDataSyncRequest, dataSyncErpRequest } from "./dataSync/action";
import { productCatalogueDepartmentRequest, productCatalogueCategoryRequest, productCatalogueSizeRequest, productCatalogueColourRequest, productCataloguePatternRequest, productCatalogueSeasonRequest, productCatalogueNeckRequest, productCatalogueFabricRequest, productCatalogueUsagesRequest, productCatalogueManualRequest, productCatalogueSubmissionHistoryRequest, productCatalogueSubmissionHistoryDetailsRequest, productCatalogueHistoryHeaderRequest } from "./vendorCatalogue/action";
// import { getAllVendorShipmentShippedRequest, getAllVendorSsdetailRequest, getAllVendorTransitNDeliverRequest, getAllVendorSiDetailRequest, getAllVendorSupplierRequest, getAllVendorTransporterRequest, lrCreateRequest, updateDeliveryRequest, lrDetailsAutoRequest } from "./vendorLogistic/action"

import { getSeasonPlanningRequest, saveSeasonPlanningRequest, deleteSeasonPlanningRequest, getEventPlanningRequest, saveEventPlanningRequest, deleteEventPlanningRequest, getSitePlanningRequest, getSitePlanningFiltersRequest, saveSitePlanningRequest, getPlanningParameterRequest, savePlanningParameterRequest, getItemConfigurationRequest, saveItemConfigurationRequest, getAdhocRequest, deleteAdhocRequest, updateAdhocRequest, searchAdhocSiteRequest, searchAdhocItemRequest, saveAdhocRequest, getSiteAndItemFilterRequest, saveSiteAndItemFilterRequest, getSiteAndItemFilterConfigurationRequest, getArsGenericFiltersRequest, getAdhocItemsPerWORequest, updateAdhocItemsPerWORequest, getMRPRangeRequest, updateMRPRangeRequest, getAllocationBasedOnItemRequest, saveAllocationBasedOnItemRequest, getAssortmentPlanningRequest, findFilterAssortmentRequest, saveAssortmentPlanningRequest, getInventoryPlanningReportRequest, getSalesInventoryReportRequest, updateInventoryPlanningReportRequest, downloadReportRequest, getMRPHistoryRequest, deleteSitePlanningRequest, getSalesContributionReportRequest, getSalesComparisonReportRequest, getSellThruPerformanceReportRequest, getTopMovingItemsReportRequest, getCategorySizeWiseReportRequest } from './planningSettings/action';
import { excelSearchRequest, handleValidateRequest, excelHeadersRequest, excelSubmitRequest, downloadTemplateRequest } from "./excelUpload/action";

import { getInvoiceManagementItemRequest, saveInvoiceManagementItemRequest, deleteInvoiceManagementItemRequest, updateInvoiceManagementItemRequest, getInvoiceManagementCustomerRequest, saveInvoiceManagementCustomerRequest, deleteInvoiceManagementCustomerRequest, updateInvoiceManagementCustomerRequest, getInvoiceManagementGenericRequest,deleteInvoiceManagementGenericRequest,expandInvoiceManagementGenericRequest, getCoreDropdownRequest, searchInvoiceDataRequest, createInvoiceRequest } from "./invoiceManagement/action"
// import { assortmentDPRequest } from "./actions";

export function* watchActions() {
  //auth Workx
  yield takeLatest(constants.LOGIN_REQUEST, loginRequest);
  yield takeLatest(constants.FORGOT_PASSWORD_REQUEST, forgotPasswordRequest);
  yield takeLatest(constants.RESET_PASSWORD_REQUEST, resetPasswordRequest);
  yield takeLatest(constants.FORGOT_USER_REQUEST, forgotUserRequest);
  yield takeLatest(constants.ACTIVATE_USER_REQUEST, activateUserRequest);
  yield takeLatest(constants.CHANGE_PASSWORD_REQUEST, changePasswordRequest);
  yield takeLatest(constants.PROFILE_IMAGE_REQUEST, profileImageRequest);
  yield takeLatest(constants.DASHBOARD_TILES_REQUEST, dashboardTilesRequest);
  yield takeLatest(constants.STORES_ARTICLES_REQUEST, storesArticlesRequest);
  yield takeLatest(constants.SALES_TREND_GRAPH_REQUEST, salesTrendGraphRequest);
  yield takeLatest(constants.SLOW_FAST_ARTICLE_REQUEST, slowFastArticleRequest);
  yield takeLatest(constants.SWITCH_ENT_REQUEST, switchEntRequest)
  //auth Work Ends

  // yield takeLatest(constants.SAMPLE_REQUEST, sampleRequest);
  yield takeLatest(constants.ROLES_REQUEST, rolesRequest);
  yield takeLatest(constants.ADD_ROLES_REQUEST, addRolesRequest);
  yield takeLatest(constants.EDIT_ROLES_REQUEST, editRolesRequest);
  yield takeLatest(constants.SITE_MAPPING_REQUEST, siteMappingRequest);
  yield takeLatest(constants.ADD_SITE_MAPPING_REQUEST, addSiteMappingRequest);
  yield takeLatest(constants.EDIT_SITE_MAPPING_REQUEST, editSiteMappingRequest);
  yield takeLatest(constants.DELETE_SITE_MAPPING_REQUEST, deleteSiteMappingRequest);
  yield takeLatest(constants.ORGANIZATION_REQUEST, organizationRequest);
  yield takeLatest(constants.ADD_ORGANIZATION_REQUEST, addOrganizationRequest);
  yield takeLatest(constants.EDIT_ORGANIZATION_REQUEST, editOrganizationRequest);
  yield takeLatest(constants.DELETE_ORGANIZATION_REQUEST, deleteOrganizationRequest);
  yield takeLatest(constants.USER_REQUEST, userRequest);
  yield takeLatest(constants.ADD_USER_REQUEST, addUserRequest);
  yield takeLatest(constants.EDIT_USER_REQUEST, editUserRequest)
  yield takeLatest(constants.USER_STATUS_REQUEST, userStatusRequest)
  yield takeLatest(constants.DELETE_USER_REQUEST, deleteUserRequest);
  yield takeLatest(constants.GET_USERNAME_REQUEST, getUserNameRequest);
  yield takeLatest(constants.UPDATE_PROFILE_REQUEST, updateProfileRequest);
  yield takeLatest(constants.GET_FIVE_TRIGGERS_REQUEST, getFiveTriggersRequest)

  yield takeLatest(constants.SITE_REQUEST, siteRequest);
  yield takeLatest(constants.ADD_SITE_REQUEST, addSiteRequest);
  yield takeLatest(constants.EDIT_SITE_REQUEST, editSiteRequest);
  yield takeLatest(constants.DELETE_SITE_REQUEST, deleteSiteRequest);
  yield takeLatest(constants.FROM_TO_REQUEST, fromToRequest);
  yield takeLatest(constants.ALL_ROLE_REQUEST, allRoleRequest);
  yield takeLatest(constants.ALL_USER_REQUEST, allUserRequest);
  yield takeLatest(constants.SKETCHERS_UPLOAD_REQUEST, sketchersUploadRequest);
  yield takeLatest(constants.SAVE_SKETCHERS_REQUEST, saveSketchersRequest);
  yield takeLatest(constants.GET_TEMPLATE_REQUEST, getTemplateRequest);
  yield takeLatest(constants.DOWNLOAD_REQUEST, downloadRequest);
  yield takeLatest(constants.DATA_SYNC_REQUEST, dataSyncRequest);
  yield takeLatest(constants.PO_ARTICLE_REQUEST, poArticleRequest)

  //vendor begins
  yield takeLatest(constants.VENDOR_REQUEST, vendorRequest);
  yield takeLatest(constants.DELETE_VENDOR_REQUEST, deleteVendorRequest);
  yield takeLatest(constants.ADD_VENDOR_REQUEST, addVendorRequest);
  yield takeLatest(constants.EDIT_VENDOR_REQUEST, editVendorRequest);


  //purchase Indent begins
  yield takeLatest(constants.DIVISION_SECTION_DEPARTMENT_REQUEST, divisionSectionDepartmentRequest);
  yield takeLatest(constants.TRANSPORTER_REQUEST, transporterRequest);
  yield takeLatest(constants.GET_TRANSPORTER_REQUEST, getTransporterRequest);

  // ________________purchase order __________----
  // yield takeLatest(constants.SUPPLIERPO_REQUEST, supplierPoRequest);
  // yield takeLatest(constants.LEAD_TIME_PO_REQUEST, leadTimePoRequest);
  // yield takeLatest(constants.VENDOR_MRP_PO_REQUEST, vendorMrpPoRequest);
  yield takeLatest(constants.LOAD_INDENT_REQUEST, loadIndentRequest);
  yield takeLatest(constants.MULTIPLE_LINEITEM_REQUEST, multipleLineItemRequest)
  yield takeLatest(constants.SUPPLIER_REQUEST, supplierRequest);
  yield takeLatest(constants.GET_AGENT_NAME_REQUEST, getAgentNameRequest);
  yield takeLatest(constants.LEAD_TIME_REQUEST, leadTimeRequest);
  yield takeLatest(constants.VENDOR_MRP_REQUEST, vendorMrpRequest);
  yield takeLatest(constants.SIZE_REQUEST, sizeRequest);
  yield takeLatest(constants.COLOR_REQUEST, colorRequest);
  yield takeLatest(constants.PURCHASETERM_REQUEST, purchaseTermRequest);
  yield takeLatest(constants.GET_PURCHASETERM_REQUEST, getPurchaseTermRequest);
  yield takeLatest(constants.QUANTITY_REQUEST, quantityRequest);
  yield takeLatest(constants.TOTAL_REQUEST, totalRequest);
  yield takeLatest(constants.IMAGE_REQUEST, imageRequest);
  yield takeLatest(constants.OTB_REQUEST, otbRequest);
  yield takeLatest(constants.LINEITEM_REQUEST, lineItemRequest);
  yield takeLatest(constants.MARKUP_REQUEST, markUpRequest);
  yield takeLatest(constants.PICREATE_REQUEST, piCreateRequest);

  yield takeLatest(constants.PIUPDATE_REQUEST, piUpdateRequest);
  yield takeLatest(constants.POITEMCODE_REQUEST, poItemcodeRequest);
  yield takeLatest(constants.GET_PODATA_REQUEST, getPoDataRequest);

  yield takeLatest(constants.UPDATE_ITEM_UDF_REQUEST, updateItemUdfRequest);
  yield takeLatest(constants.UPDATE_ITEM_CAT_DESC_REQUEST, updateItemCatDescRequest);
  yield takeLatest(constants.GET_ITEM_UDF_REQUEST, getItemUdfRequest);

  yield takeLatest(constants.UDF_TYPE_REQUEST, udfTypeRequest);
  yield takeLatest(constants.GET_UDF_MAPPING_REQUEST, getUdfMappingRequest);
  yield takeLatest(constants.PO_HISTORY_REQUEST, poHistoryRequest);
  yield takeLatest(constants.PO_CREATE_REQUEST, poCreateRequest);
  yield takeLatest(constants.PO_UDF_MAPPING_REQUEST, poUdfMappingRequest);
  yield takeLatest(constants.UPDATE_UDF_MAPPING_REQUEST, updateUdfMappingRequest);

  yield takeLatest(constants.PO_SIZE_REQUEST, poSizeRequest);
  yield takeLatest(constants.CREATE_UDF_SETTING_REQUEST, createUdfSettingRequest);

  yield takeLatest(constants.ACTIVE_CAT_DESC_REQUEST, activeCatDescRequest);
  yield takeLatest(constants.ACTIVE_UDF_REQUEST, activeUdfRequest);
  yield takeLatest(constants.GET_CATDESC_UDF_REQUEST, getCatDescUdfRequest);

  yield takeLatest(constants.ITEM_CATDESC_UDF_REQUEST, itemCatDescUdfRequest);
  yield takeLatest(constants.ITEM_UDF_MAPPING_REQUEST, itemUdfMappingRequest);
  yield takeLatest(constants.CREATE_ITEM_UDF_REQUEST, createItemUdfRequest);

  yield takeLatest(constants.UPDATE_SIZE_DEPT_REQUEST, updateSizeDeptRequest);

  yield takeLatest(constants.ARTICLE_NAME_REQUEST, articleNameRequest);
  yield takeLatest(constants.CNAME_REQUEST, cnameRequest)
  yield takeLatest(constants.HSN_CODE_REQUEST, hsnCodeRequest)
  yield takeLatest(constants.GET_PO_ITEMCODE_REQUEST, getPoItemCodeRequest)

  //pi_itemDetails
  yield takeLatest(constants.GET_ITEM_DETAILS_REQUEST, getItemDetailsRequest);
  yield takeLatest(constants.GET_ITEM_DETAILS_VALUE_REQUEST, getItemDetailsValueRequest)
  yield takeLatest(constants.PI_ADD_NEW_REQUEST, piAddNewRequest);
  yield takeLatest(constants.PROCUREMENT_SITE_REQUEST, procurementSiteRequest)
  yield takeLatest(constants.PO_ITEM_BARCODE_REQUEST, poItemBarcodeRequest)
  yield takeLatest(constants.PO_RADIO_VALIDATION_REQUEST, poRadioValidationRequest)
  yield takeLatest(constants.GET_MULTIPLE_MARGIN_REQUEST, getMultipleMarginRequest)



  //replenishment begins
  yield takeLatest(constants.CREATE_TRIGGER_REQUEST, createTriggerRequest);
  yield takeLatest(constants.GET_JOB_BY_NAME_REQUEST, getJobByNameRequest);
  yield takeLatest(constants.JOB_RUN_DETAIL_REQUEST, jobRunDetailRequest);;
  yield takeLatest(constants.RUN_ON_DEMAND_REQUEST, runOnDemandRequest);
  yield takeLatest(constants.STOP_ON_DEMAND_REQUEST, stopOnDemandRequest);
  yield takeLatest(constants.NSCHEDULE_REQUEST, nscheduleRequest);
  yield takeLatest(constants.JOB_HISTORY_REQUEST, jobHistoryRequest);
  yield takeLatest(constants.JOB_HISTORY_FILTER_REQUEST, jobHistoryFilterRequest);
  yield takeLatest(constants.CREATE_JOB_REQUEST, createJobRequest);
  yield takeLatest(constants.ALL_STORE_CODE_REQUEST, allStoreCodeRequest);
  yield takeLatest(constants.LAST_JOB_REQUEST, lastJobRequest);
  yield takeLatest(constants.GET_ALLOCATION_REQUEST, getAllocationRequest);
  yield takeLatest(constants.GET_REQUIREMENT_SUMMARY_REQUEST, getRequirementSummaryRequest);
  yield takeLatest(constants.SUMMARY_DETAIL_REQUEST, summaryDetailRequest);
  yield takeLatest(constants.SUMMARY_CSV_REQUEST, summaryCsvRequest);
  yield takeLatest(constants.GETGRADE_REQUEST, getGradeRequest);
  yield takeLatest(constants.GETZONE_REQUEST, getZoneRequest);
  yield takeLatest(constants.GETPARTNER_REQUEST, getPartnerRequest);
  yield takeLatest(constants.GETSTORECODE_REQUEST, getStoreCodeRequest);
  yield takeLatest(constants.GETCSVLINK_REQUEST, getCSVLinkRequest);
  yield takeLatest(constants.TO_STATUS_REQUEST, toStatusRequest);
  yield takeLatest(constants.APPLY_FILTER_REQUEST, applyFilterRequest);
  yield takeLatest(constants.GETXLSLINK_REQUEST, getXLSLinkRequest);
  // PIHISTORY

  yield takeLatest(constants.PIHISTORY_REQUEST, piHistoryRequest);
  yield takeLatest(constants.ARTICLEPO_REQUEST, articlePoRequest);
  // yield takeLatest(constants.PIDOWNLOAD_REQUEST,piDownloadRequest)

  //assortment
  yield takeLatest(constants.HL1_REQUEST, hl1Request);
  yield takeLatest(constants.HL2_REQUEST, hl2Request);
  yield takeLatest(constants.HL3_REQUEST, hl3Request);
  yield takeLatest(constants.HL4_REQUEST, hl4Request);
  yield takeLatest(constants.ASSET_CODE_REQUEST, assetCodeRequest);
  yield takeLatest(constants.CREATE_ASSORTMENT_REQUEST, createAssortmentRequest);
  yield takeLatest(constants.GET_ASSORTMENT_REQUEST, getAssortmentRequest);
  yield takeLatest(constants.VIEW_MESSAGE_REQUEST, viewMessageRequest);
  yield takeLatest(constants.GET_ASSORTMENT_STATUS_REQUEST, getAssortmentStatusRequest);
  yield takeLatest(constants.GET_AD_HOCK_REQUEST, getAdHockRequest);
  yield takeLatest(constants.GET_SITE_AD_HOCK_REQUEST, getSiteAdHockRequest);
  yield takeLatest(constants.GET_ITEM_AD_HOCK_REQUEST, getItemAdHockRequest);
  yield takeLatest(constants.CREATE_ADHOCK_REQUEST, createAdhocRequest);
  yield takeLatest(constants.EDIT_ADHOCK_REQUEST, editAdhocRequest);
  yield takeLatest(constants.DELETE_ITEM_CODE_REQUEST, deleteItemCodeRequest);
  yield takeLatest(constants.GET_ACTIVITY_REQUEST, getActivityRequest);
  yield takeLatest(constants.CANCEL_ADHOCK_REQUEST, cancelAdhocRequest)


  yield takeLatest(constants.FORCAST_REQUEST, forcastRequest);
  yield takeLatest(constants.STOREDP_REQUEST, storeDPRequest);
  yield takeLatest(constants.ASSORTMENTDP_REQUEST, assortmentDPRequest);
  yield takeLatest(constants.GET_FORCAST_REQUEST, getForcastRequest);
  yield takeLatest(constants.WEEKLYDP_REQUEST, weeklyDPRequest);
  yield takeLatest(constants.MONTHLYFOR_REQUEST, monthlyForRequest);
  yield takeLatest(constants.WEEKLYFOR_REQUEST, weeklyForRequest);
  yield takeLatest(constants.TABLE_REQUEST, tableRequest);
  yield takeLatest(constants.PROCESSINGBS_REQUEST, processingBSRequest);
  yield takeLatest(constants.VERIFYBS_REQUEST, verifyBSRequest);
  yield takeLatest(constants.SUCCESSBS_REQUEST, successBSRequest);
  yield takeLatest(constants.DPPROGRESS_REQUEST, dpProgressRequest);
  yield takeLatest(constants.BSSTATUS_REQUEST, bsStatusRequest);
  yield takeLatest(constants.BSHISTORY_REQUEST, bsHistoryRequest);
  yield takeLatest(constants.MWHISTORY_REQUEST, mwHistoryRequest);
  yield takeLatest(constants.GET_FORECAST_STATUS_REQUEST, getForecastStatusRequest);
  yield takeLatest(constants.LAST_FORECAST_REQUEST, lastForecastRequest);

  yield takeLatest(constants.GET_ASSORTMENT_DROPDOWN_REQUEST, getAssortmentDropdownRequest)
  yield takeLatest(constants.GET_ASSORTMENT_HISTORY_REQUEST, getAssortmentHistoryRequest)
  //change setting
  yield takeLatest(constants.CREATE_SETTING_REQUEST, createSettingRequest);
  yield takeLatest(constants.GET_SETTING_REQUEST, getSettingRequest);
  yield takeLatest(constants.GET_MODULE_REQUEST, getModuleRequest);
  yield takeLatest(constants.GET_SUB_MODULE_REQUEST, getSubModuleRequest);
  yield takeLatest(constants.GET_SUB_MODULE_BY_ROLENAME_REQUEST, getSubModuleByRoleNameRequest)
  yield takeLatest(constants.GET_PROPERTY_REQUEST, getPropertyRequest);
  yield takeLatest(constants.GET_EMAIL_STATUS_REQUEST, getEmailStatusRequest);
  yield takeLatest(constants.UPDATE_EMAIL_STATUS_REQUEST, updateEmailStatusRequest);
  yield takeLatest(constants.GET_TO_CC_REQUEST, getToCcRequest);
  yield takeLatest(constants.SAVE_TO_CC_REQUEST, saveToCcRequest);
  yield takeLatest(constants.UPDATE_TO_CC_REQUEST, updateToCcRequest);
  // ______________________
  // yield takeLatest(constants.CHECKED_LIST_REQUEST, checkedListRequest);
  yield takeLatest(constants.CREATE_FESTIVAL_REQUEST, createFestivalRequest);
  yield takeLatest(constants.ALL_FESTIVAL_REQUEST, allFestivalRequest);
  // yield takeLatest(constants.DEFAULT_LIST_REQUEST, defaultListRequest)

  //_________________________________________OTB_____________________

  yield takeLatest(constants.CREATE_PLAN_REQUEST, createPlanRequest);
  yield takeLatest(constants.GET_ACTIVE_PLAN_REQUEST, getActivePlanRequest);
  yield takeLatest(constants.UPDATE_PLAN_REQUEST, updatePlanRequest);
  yield takeLatest(constants.REMOVE_PLAN_REQUEST, removePlanRequest);
  yield takeLatest(constants.UPDATE_ACTIVE_PLAN_REQUEST, updateActivePlanRequest);
  yield takeLatest(constants.GET_MAKE_COPY_REQUEST, getMakeCopyRequest);
  yield takeLatest(constants.GET_OTB_PLAN_REQUEST, getOtbPlanRequest);
  yield takeLatest(constants.TOTAL_OTB_REQUEST, totalOtbRequest);
  yield takeLatest(constants.PLAN_SALE_UPDATE_REQUEST, planSaleUpdateRequest);
  yield takeLatest(constants.GET_OTB_STATUS_REQUEST, getOtbStatusRequest);


  //____________________________________SESSION_______________________________

  yield takeLatest(constants.USER_SESSION_CREATE_REQUEST, userSessionCreateRequest);


  //_______________________STORE_PROFILE__________________________

  yield takeLatest(constants.GENERATE_TOTALSALES_VS_PROFIT_REQUEST, generateTotalSalesVsProfitRequest);
  yield takeLatest(constants.GENERATE_SALES_PER_SQUARE_FOOT_REQUEST, generateSalesPerSquareFootRequest);
  yield takeLatest(constants.GET_SELL_THRU_REQUEST, getSellThruRequest);
  yield takeLatest(constants.GET_FESTIVAL_IMPACT_REQUEST, getFestivalImpactRequest);


  yield takeLatest(constants.GENERATE_TOTAL_SALES_REQUEST, generateTotalSalesRequest);
  yield takeLatest(constants.GENERATE_UNIT_SOLD_REQUEST, generateUnitSoldRequest);

  yield takeLatest(constants.GET_STORE_PROFILE_CODE_REQUEST, getStoreProfileCodeRequest);
  yield takeLatest(constants.GET_STORE_DATA_REQUEST, getStoreDataRequest);

  yield takeLatest(constants.GET_TOP_ARTICLE_REQUEST, getTopArticleRequest);
  yield takeLatest(constants.GET_SALES_TREND_REQUEST, getSalesTrendRequest);

  //changeSetting

  yield takeLatest(constants.SETTING_ASSORTOTB_REQUEST, settingAssortOtbRequest);
  yield takeLatest(constants.SETTING_OTB_CREATE_REQUEST, settingOtbCreateRequest);
  yield takeLatest(constants.OTB_GET_DEFAULT_REQUEST, otbGetDefaultRequest);
  yield takeLatest(constants.OTB_GET_HLEVEL_REQUEST, otbGetHlevelRequest);

  yield takeLatest(constants.MARGIN_RULE_REQUEST, marginRuleRequest);

  yield takeLatest(constants.STOCK_HAND_REQUEST, stockHandRequest);

  yield takeLatest(constants.GET_ALL_CONFIG_REQUEST, getAllConfigRequest);
  yield takeLatest(constants.GET_ALL_OTB_REQUEST, getAllOtbRequest)

  yield takeLatest(constants.SELECT_VENDOR_REQUEST, selectVendorRequest);
  yield takeLatest(constants.DEPARTMENT_SET_BASED_REQUEST, departmentSetBasedRequest)

  yield takeLatest(constants.SELECT_ORDERNUMBER_REQUEST, selectOrderNumberRequest);
  yield takeLatest(constants.SET_BASED_REQUEST, setBasedRequest);

  yield takeLatest(constants.GET_PREVIOUS_ASSORTMENT_REQUEST, getPreviousAssortmentRequest);
  //replenishment

  yield takeLatest(constants.GET_HEADER_CONFIG_REQUEST, getHeaderConfigRequest);
  yield takeLatest(constants.GET_HEADER_CONFIG_EXCEL_REQUEST, getHeaderConfigExcelRequest);
  yield takeLatest(constants.GET_CUSTOM_HEADER_REQUEST, getCustomHeaderRequest);
  yield takeLatest(constants.CREATE_HEADER_CONFIG_REQUEST, createHeaderConfigRequest)

    ;

  yield takeLatest(constants.CREATE_MAIN_HEADER_CONFIG_REQUEST, createMainHeaderConfigRequest);
  yield takeLatest(constants.CREATE_SET_HEADER_CONFIG_REQUEST, createSetHeaderConfigRequest);
  yield takeLatest(constants.CREATE_ITEM_HEADER_CONFIG_REQUEST, createItemHeaderConfigRequest);

  yield takeLatest(constants.UPDATE_HEADER_LOGS_REQUEST, updateHeaderLogsRequest);
  yield takeLatest(constants.GET_MAIN_HEADER_CONFIG_REQUEST, getMainHeaderConfigRequest);
  yield takeLatest(constants.GET_SET_HEADER_CONFIG_REQUEST, getSetHeaderConfigRequest);
  yield takeLatest(constants.GET_ITEM_HEADER_CONFIG_REQUEST, getItemHeaderConfigRequest);

  //demandplanning

  yield takeLatest(constants.DOWNLOAD_PREVIOUS_FORCAST_REQUEST, downloadPreviousForcastRequest)

  yield takeLatest(constants.PARAMSETTING_REQUEST, paramsettingRequest)

  yield takeLatest(constants.PROMOTIONAL_REQUEST, promotionalRequest)
  yield takeLatest(constants.GET_PROMOTIONAL_EVENT_REQUEST, getPromotionalEventRequest)
  yield takeLatest(constants.UPDATE_PROMOTIONAL_EVENT_REQUEST, updatePromotionalEventRequest)
  yield takeLatest(constants.DISABLE_PROMOTIONAL_EVENT_REQUEST, disablePromotionalEventRequest),
    yield takeLatest(constants.DEACTIVE_PROMOTIONAL_EVENT_REQUEST, deactivePromotionalEventRequest)

  // __________PROXY STORE_____________________

  yield takeLatest(constants.PROXY_STORE_REQUEST, proxyStoreRequest)


  // _____________________parameter get all____________________________

  yield takeLatest(constants.PARAMETER_CUSTOM_REQUEST, parameterCustomRequest)
  // ______________________DATA_SYNC________________________

  yield takeLatest(constants.CREATE_DATA_SYNC_TEMPLATE_REQUEST, createDataSyncTemplateRequest)
  yield takeLatest(constants.DOWNLOAD_TEMPLATE_REQUEST, downlaodTemplateRequest)
  yield takeLatest(constants.UPLOAD_TEMPLATE_REQUEST, uploadTemplateRequest)
  yield takeLatest(constants.GET_ALL_DATA_SYNC_REQUEST, getAllDataSyncRequest)
  yield takeLatest(constants.DATA_SYNC_ERP_REQUEST, dataSyncErpRequest)

  //__________________inventory configuration && MANAGE RULE_________________
  yield takeLatest(constants.GET_FILTER_SECTION_REQUEST, getFilterSectionRequest)
  yield takeLatest(constants.DELETE_CONFIG_REQUEST, deleteConfigRequest)
  yield takeLatest(constants.STATUS_CHANGE_REQUEST, statusChangeRequest)
  yield takeLatest(constants.INV_GET_ALL_CONFIG_REQUEST, invGetAllConfigRequest)
  yield takeLatest(constants.GET_JOBDATA_REQUEST, getJobDataRequest)
  yield takeLatest(constants.UPDATE_JOBDATA_REQUEST, updateJobDataRequest)
  yield takeLatest(constants.CONFIG_CREATE_REQUEST, configCreateRequest)
  yield takeLatest(constants.GET_JOBNAME_REQUEST, getJobNameRequest)
  yield takeLatest(constants.GLUE_PARAMETER_REQUEST, glueParameterRequest)
  yield takeLatest(constants.DELETE_TRIGGER_REQUEST, deleteTriggerRequest)

  // ________________________SWITCHORGANISATION______________________

  yield takeLatest(constants.SWITCH_ORGANISATION_REQUEST, switchOrganisationRequest);

  yield takeLatest(constants.GET_ALL_JOB_REQUEST, getAllJobRequest)


  // _______________________________FMCG HISTORY___________________________


  // _--------_________________________________ VENDOR PORTAL ENTERPRISE SAGAS_____________________________
  yield takeLatest(constants.ENTERPRISE_APPROVED_PO_REQUEST, EnterpriseApprovedPoRequest)
  yield takeLatest(constants.ENTERPRISE_GET_PO_DETAILS_REQUEST, EnterpriseGetPoDetailsRequest)
  yield takeLatest(constants.ENTERPRISE_CANCEL_PO_REQUEST, EnterpriseCancelPoRequest)
  yield takeLatest(constants.UPDATE_HEADER_REQUEST, updateHeaderRequest)
  yield takeLatest(constants.GET_ADDRESS_REQUEST, getAddressRequest)
  yield takeLatest(constants.GET_LOCATION_REQUEST, getLocationRequest)
  yield takeLatest(constants.APPROVE_PO_CONFIRM_REQUEST, approvePOConfirmRequest)

  // -----------------------------------------VENDOR CREATE SHIPMENT DETAILS--------------------
  yield takeLatest(constants.VENDOR_CREATE_SHIPMENT_DETAILS_REQUEST, VendorCreateShipmentDetailsRequest)

  // -----------------------------------------------Get Shipment Request-------------------------------
  yield takeLatest(constants.GET_SHIPMENT_REQUEST, getShipmentRequest)


  // --------------------------------------------------CENDOR CREATE SR------------------------
  yield takeLatest(constants.VENDOR_CREATE_SR_REQUEST, vendorCreateSrRequest)

  // _________________________________SHIPMENT SAGA'S_____________________________

  yield takeLatest(constants.SHIPMENT_CONFIRM_CANCEL_REQUEST, shipmentConfirmCancelRequest)
  yield takeLatest(constants.GET_COMPLETE_DETAIL_SHIPMENT_REQUEST, getCompleteDetailShipmentRequest)

  // ___________________________________ VENDOR SHIPMENT SAGA_____________________

  yield takeLatest(constants.GET_ALL_SHIPMENT_VENDOR_REQUEST, getAllShipmentVendorRequest)
  yield takeLatest(constants.UPDATE_VENDOR_REQUEST, updateVendorRequest)
  yield takeLatest(constants.GET_SHIPMENT_DETAILS_REQUEST, getShipmentDetailsRequest)
  yield takeLatest(constants.GET_ALL_COMMENT_REQUEST, getAllCommentRequest)

  //______________________________________LR SAGA________________________________________

  yield takeLatest(constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_REQUEST, getAllVendorShipmentShippedRequest)
  yield takeLatest(constants.GET_ALL_VENDOR_SSDETAIL_REQUEST, getAllVendorSsdetailRequest)
  yield takeLatest(constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_REQUEST, getAllVendorTransitNDeliverRequest)

  yield takeLatest(constants.GET_ALL_VENDOR_SIDETAIL_REQUEST, getAllVendorSiDetailRequest)
  yield takeLatest(constants.GET_ALL_VENDOR_SUPPLIER_REQUEST, getAllVendorSupplierRequest)

  yield takeLatest(constants.GET_ALL_VENDOR_TRANSPORTER_REQUEST, getAllVendorTransporterRequest)
  yield takeLatest(constants.FMCG_GET_HISTORY_REQUEST, fmcgGetHistoryRequest)
  yield takeLatest(constants.GET_CONFIG_FILTER_REQUEST, getConfigFilterRequest)
  // yield takeLatest(constants.GET_ALL_VENDOR_TRANSPORTER_REQUEST, getAllVendorTransporterRequest)
  // yield takeLatest(constants.FMCG_GET_HISTORY_REQUEST, fmcgGetHistoryRequest)



  yield takeLatest(constants.LR_CREATE_REQUEST, lrCreateRequest)
  yield takeLatest(constants.UPDATE_DELIVERY_REQUEST, updateDeliveryRequest)
  yield takeLatest(constants.QC_ADD_COMMENT_REQUEST, qcAddCommentRequest)
  // yield takeLatest(constants.QC_GET_COMMENT_REQUEST,qcGetCommentRequest)
  yield takeLatest(constants.UPLOAD_COMMENT_REQUEST, uploadCommentRequest)




  //_____________________________EVENT MASTER___________________________________

  yield takeLatest(constants.CUSTOM_MASTER_SKECHERS_REQUEST, customMasterSkechersRequest);
  yield takeLatest(constants.CUSTOM_GET_EVENT_REQUEST, customGetEventRequest);
  yield takeLatest(constants.CUSTOM_DELETE_EVENT_REQUEST, customDeleteEventRequest);
  yield takeLatest(constants.CUSTOM_GET_PARTNER_REQUEST, customGetPartnerRequest);
  yield takeLatest(constants.CUSTOM_GET_ZONE_REQUEST, customGetZoneRequest);
  yield takeLatest(constants.CUSTOM_GET_GRADE_REQUEST, customGetGradeRequest);
  yield takeLatest(constants.CUSTOM_GET_STORECODE_REQUEST, customGetStoreCodeRequest);
  yield takeLatest(constants.CUSTOM_APPLYFILTER_REQUEST, customApplyFilterRequest);

  //procurement
  yield takeLatest(constants.GET_ALL_CITY_REQUEST, getAllCityRequest);
  yield takeLatest(constants.PURCHASE_ORDER_DATE_RANGE_REQUEST, purchaseOrderDateRangeRequest);
  yield takeLatest(constants.DISCOUNT_REQUEST, discountRequest);
  yield takeLatest(constants.PI_ARTICLE_REQUEST, piArticleRequest);
  yield takeLatest(constants.GET_LINE_ITEM_REQUEST, getLineItemRequest);
  yield takeLatest(constants.GET_AVAILABLE_KEYS_REQUEST, getAvailableKeysRequest);
  yield takeLatest(constants.UPDATE_AVAILABLE_KEYS_REQUEST, updateAvailableKeysRequest);

  //fmcg replenishment

  yield takeLatest(constants.GET_MOTHER_COMP_REQUEST, getMotherCompRequest)
  yield takeLatest(constants.GET_ARTICLE_FMCG_REQUEST, getArticleFmcgRequest);
  yield takeLatest(constants.GET_VENDOR_FMCG_REQUEST, getVendorFmcgRequest);
  yield takeLatest(constants.GET_STORE_FMCG_REQUEST, getStoreFmcgRequest);
  yield takeLatest(constants.APPLY_FILTER_FMCG_REQUEST, applyFilterFmcgRequest);

  yield takeLatest(constants.DELETE_UPLOADS_REQUEST, deleteUploadsRequest);

  yield takeLatest(constants.CUSTOM_FILTER_MENU_REQUEST, customFilterMenuRequest)
  yield takeLatest(constants.UPDATE_VENDOR_SHIPMENT_REQUEST, updateVendorShipmentRequest);

  //ASN NUMBER FORMAT CHANGE SETTING

  yield takeLatest(constants.GET_ASN_FORMAT_REQUEST, getAsnFormatRequest);
  yield takeLatest(constants.CREATE_ASN_FORMAT_REQUEST, createAsnFormatRequest);

  //Logistics form details
  yield takeLatest(constants.LR_DETAILS_AUTO_REQUEST, lrDetailsAutoRequest);

  yield takeLatest(constants.GET_PI_LINE_ITEM_REQUEST, getPiLineItemRequest)

  //update Invoice 
  yield takeLatest(constants.UPDATE_INVOICE_REQUEST, updateInvoiceRequest);

  //Po Closer
  yield takeLatest(constants.PO_CLOSER_REQUEST, poCloserRequest);
  // yield takeLatest(constants.MULTIPLE_OTB_REQUEST, multipleOtbRequest);

  yield takeLatest(constants.GEN_PURCHASE_ORDER_REQUEST, gen_PurchaseOrderRequest);
  yield takeLatest(constants.GEN_PURCHASE_INDENT_REQUEST, gen_PurchaseIndentRequest);
  yield takeLatest(constants.GEN_SET_BASED_REQUEST, gen_SetBasedRequest);
  yield takeLatest(constants.GEN_INDENT_BASED_REQUEST, gen_IndentBasedRequest);
  yield takeLatest(constants.PO_APPROVE_REJECT_REQUEST, po_approve_reject_Request)
  yield takeLatest(constants.PO_APPROVE_RETRY_REQUEST, po_approve_retry_Request)


  //All Advice No
  yield takeLatest(constants.FIND_SHIPMENT_ADVICE_REQUEST, findShipmentAdviceRequest);

  yield takeLatest(constants.SHIPMENT_TRACKING_REQUEST, shipmentTrackingRequest)
  // add multiple comment Qc
  yield takeLatest(constants.ADD_MULTIPLE_COMMENT_QC_REQUEST, addMultipleCommentQcRequest)

  // comment notification
  yield takeLatest(constants.COMMENT_NOTIFICATION_REQUEST, commentNotificationRequest)

  // dashboard upper
  yield takeLatest(constants.DASHBOARD_UPPER_REQUEST, dashboardUpperRequest)

  // dashboard bottom
  yield takeLatest(constants.DASHBOARD_BOTTOM_REQUEST, dashboardBottomRequest)

  // dashboard unread comment
  yield takeLatest(constants.DASHBOARD_UNREAD_COMMENT_REQUEST, dashboardUnreadCommentRequest)

  // vendor dashboard filter
  yield takeLatest(constants.VENDOR_DASHBOARD_FILTER_REQUEST, vendorDashboardFilterRequest)

  // get all plans
  yield takeLatest(constants.GET_ALL_PLANS_REQUEST, getAllPlansRequest)

  // get payment Summary
  yield takeLatest(constants.GET_PAYMENT_SUMMARY_REQUEST, getPaymentSummaryRequest)
  // edit pi
  yield takeLatest(constants.PI_EDIT_REQUEST, piEditRequest)

  // get order Id
  yield takeLatest(constants.GET_ORDER_ID_REQUEST, getOrderIdRequest)

  // get capture payment
  yield takeLatest(constants.GET_CAPTURE_PAYMENT_REQUEST, getCapturePaymentRequest)

  // Multiple Document Download
  yield takeLatest(constants.MULTIPLE_DOCUMENT_DOWNLOAD_REQUEST, multipleDocumentDownloadRequest)

  // get all vedor request new
  yield takeLatest(constants.GET_ALL_MANAGE_VENDOR_REQUEST, getAllManageVendorRequest)
  // get all vedor request new
  yield takeLatest(constants.CREATE_VENDOR_REQUEST, createVendorRequest)
  // updated vendor Data
  yield takeLatest(constants.UPDATE_CREATED_VENDOR_REQUEST, updateCreatedVendorRequest)

  // Manage Vendor Upodate
  yield takeLatest(constants.UPDATE_MANAGE_VENDOR_REQUEST, updateManageVendorRequest)

  // GET USER PROFILE DETAILE
  yield takeLatest(constants.GET_PROFILE_DETAILS_REQUEST, getProfileDetailsRequest)

  // UPDATE_USER_DETAILS
  yield takeLatest(constants.UPDATE_USER_DETAILS_REQUEST, updateUserDetailsRequest)

  // GET USER EMAILS
  yield takeLatest(constants.GET_USER_EMAILS_REQUEST, getUserEmailsRequest)

  // GET ALL RETAILER
  yield takeLatest(constants.ALL_RETAILER_REQUEST, allRetailerRequest)



  // ____________________________________ ASR dashboard ____________________________________

  yield takeLatest(constants.GET_REVIEW_REASON_REQUEST, getReviewReasonRequest);
  yield takeLatest(constants.GET_DATA_WITHOUT_FILTER_REQUEST, getDataWithoutFilterRequest);
  yield takeLatest(constants.GET_FILLTERS_ASR_DATA_REQUEST, getfiltersArsDataRequest);
  yield takeLatest(constants.GET_ASSORTMENT_DETAILS_REQUEST, getAssortmentDetailsRequest);
  yield takeLatest(constants.GET_ASSORTMENT_TIME_PHASED_REQUEST, getAssortmentTimePhasedRequest);
  yield takeLatest(constants.GET_ASSORTMENT_ITEMS_REQUEST, getAssortmentItemsRequest);
  yield takeLatest(constants.GET_ASSORTMENT_SALES_HISTORY_REQUEST, getAssortmentSalesHistoryRequest);
  // yield takeLatest(constants.GET_ASSORTMENT_SALES_HISTORY_REQUEST, getAssortmentSalesHistoryRequest);
  yield takeLatest(constants.GET_ALLOCATION_DATA_REQUEST, getAllocationDataRequest);
  // save edited pi

  yield takeLatest(constants.EDITED_PI_SAVE_REQUEST, editedPiSaveRequest)

  // save po draft
  yield takeLatest(constants.PO_SAVE_DRAFT_REQUEST, poSaveDraftRequest);

  // save pi draft
  yield takeLatest(constants.PI_SAVE_DRAFT_REQUEST, piSaveDraftRequest);
  //  pi/po draft data
  yield takeLatest(constants.GET_DRAFT_REQUEST, getDraftRequest);

  // edit po
  yield takeLatest(constants.PO_EDIT_REQUEST, poEditRequest);

  //saveedited po
  yield takeLatest(constants.EDITED_PO_SAVE_REQUEST, editedPoSaveRequest)
  //email Transcript
  yield takeLatest(constants.EMAIL_TRANSCRIPT_REQUEST, emailTranscriptRequest)
  //Payment Status
  yield takeLatest(constants.GET_PAYMENT_STATUS_REQUEST, getPaymentStatusRequest)

  //discard pi po data
  yield takeLatest(constants.DISCARD_PO_PI_DATA_REQUEST, discardPoPiDataRequest)

  //discard pi po data
  yield takeLatest(constants.UPDATE_EXTENSION_DATE_REQUEST, updateExtensionDateRequest)
  yield takeLatest(constants.VENDOR_USER_REQUEST, vendorUserRequest)
  //view image
  yield takeLatest(constants.VIEW_IMAGES_REQUEST, viewImagesRequest)
  //get grc detail
  yield takeLatest(constants.GET_GRC_ITEM_REQUEST, getGrcItemRequest)
  //create grc
  yield takeLatest(constants.CREATE_GRC_REQUEST, createGrcRequest)
  //get all grc
  yield takeLatest(constants.GET_ALL_GRC_REQUEST, getAllGrcRequest)
  //get all grc
  yield takeLatest(constants.GET_GRC_DETAILS_REQUEST, getGrcDetailsRequest)
  //get all gate entry
  yield takeLatest(constants.GET_ALL_GATE_ENTRY_REQUEST, getAllGateEntryRequest)
  //proc dashboard bottom
  yield takeLatest(constants.PI_DASH_BOTTOM_REQUEST, piDashBottomRequest)
  //po dashboard hierarchy
  yield takeLatest(constants.PI_DASH_HIERARCHY_REQUEST, piDashHierarchyRequest)
  //PO DASHBOARD EMAIL
  yield takeLatest(constants.EMAIL_DASHBOARD_SEND_REQUEST,  emailDashboardSendRequest)


  //pi po dashboard Get Site
  yield takeLatest(constants.PI_PO_DASH_SITE_REQUEST, piPoDashSiteRequest)

  //assortment planning filters
  yield takeLatest(constants.GET_ASSORTMENT_PLANNING_FILTERS_REQUEST, getAssortmentPlanningFiltersRequest)
  //assortment planning count
  yield takeLatest(constants.GET_ASSORTMENT_PLANNING_COUNT_REQUEST, getAssortmentPlannigCountRequest)
  //get assortment planning
  yield takeLatest(constants.GET_ASSORTMENT_PLANNING_DETAIL_REQUEST, getAssortmentPlanningDetailRequest)
  //save assortment planning
  yield takeLatest(constants.SAVE_ASSORTMENT_PLANNING_DETAIL_REQUEST, saveAssortmentPlannigDetailRequest)
  //PO EXCEL download
  yield takeLatest(constants.GET_EXCEL_DOWNLOADED_REQUEST, getExcelDownloadedPoRequest)
  //PI EXCEL download
  yield takeLatest(constants.GET_EXCEL_DOWNLOADED_REQUEST_PI, getExcelDownloadedPiRequest)
  //Status Button Active Config::
  yield takeLatest(constants.GET_STATUS_BUTTON_ACTIVE_REQUEST, getStatusButtonActiveRequest)

  //rolemaster
  yield takeLatest(constants.UPDATE_ROLE_REQUEST, updateRoleRequest)
  yield takeLatest(constants.MODULE_GET_DATA_REQUEST, moduleGetDataRequest)
  yield takeLatest(constants.MODULE_GETROLE_REQUEST, moduleGetRoleRequest)
  yield takeLatest(constants.GET_SUB_MODULE_DATA_REQUEST, getSubModuleDataRequest)
  yield takeLatest(constants.CHECK_ROLE_NAME_REQUEST, checkRoleNameRequest)
  yield takeLatest(constants.SUBMIT_ROLE_REQUEST, roleSubmitRequest)
  yield takeLatest(constants.UPDATE_ROLE_DATA_REQUEST, roleUpdateDataRequest)
  //Api logs
  yield takeLatest(constants.GET_MODULE_API_LOGS_REQUEST, getModuleApiLogsRequest)
  // vender catalogue add product manual dropdown value
  // yield takeLatest(constants.PRODUCT_CATALOGUE_DEPARTMENT_REQUEST, productCatalogueDepartmentRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_CATEGORY_REQUEST, productCatalogueCategoryRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_SIZE_REQUEST, productCatalogueSizeRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_COLOUR_REQUEST, productCatalogueColourRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_PATTERN_REQUEST, productCataloguePatternRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_SEASON_REQUEST, productCatalogueSeasonRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_NECK_REQUEST, productCatalogueNeckRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_FABRIC_REQUEST, productCatalogueFabricRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_USAGES_REQUEST, productCatalogueUsagesRequest)
  yield takeLatest(constants.PRODUCT_CATALOGUE_MANUALSUBMIT_REQUEST, productCatalogueManualRequest);

  // vendor catalogue submission History 
  yield takeLatest(constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_REQUEST, productCatalogueSubmissionHistoryRequest);
  yield takeLatest(constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_DETAILS_REQUEST, productCatalogueSubmissionHistoryDetailsRequest);
  yield takeLatest(constants.PRODUCT_CATALOGUE_HISTORY_HEADER_REQUEST, productCatalogueHistoryHeaderRequest)

  yield takeLatest(constants.INSP_ON_OFF_CONFIG_REQUEST, getInspOnOffConfigRequest);

  yield takeLatest(constants.INVOICE_BASED_LR_REQUEST, invoiceApprovalBasedLRRequest);
  yield takeLatest(constants.GET_SL_REQUEST, getSlRequest);

  yield takeLatest(constants.GET_GENERAL_MAPPING_REQUEST, getGeneralMappingRequest);
  yield takeLatest(constants.UPDATE_GENERAL_MAPPING_REQUEST, updateGeneralMappingRequest);

  yield takeLatest(constants.ANALYTICS_OTB_REQUEST, analyticsOtbRequest);

  // pi Image url request
  yield takeLatest(constants.PI_IMAGE_URL_REQUEST, piImageUrlRequest);
  // role delete
  yield takeLatest(constants.ROLE_DELETE_REQUEST, roleDeleteRequest);
  // get activity
  yield takeLatest(constants.USER_ACTIVITY_REQUEST, userActivityRequest);
  yield takeLatest(constants.SEARCH_DATA_REQUEST, searchDataRequest);

  // Proc mapping
  yield takeLatest(constants.CAT_DESC_DROPDOWN_REQUEST, catDescDropdownRequest);
  yield takeLatest(constants.UPDATE_ITEM_UDF_MAPPING_REQUEST, updateItemUdfMappingRequest);
  yield takeLatest(constants.MAPPING_EXCEL_UPLOAD_REQUEST, mappingExcelUploadRequest);
  yield takeLatest(constants.MAPPING_EXCEL_STATUS_REQUEST, mappingExcelStatusRequest);
  yield takeLatest(constants.MAPPING_EXCEL_EXPORT_REQUEST, mappingExcelExportRequest);

  // dashboard Excel download
  yield takeLatest(constants.EXPORT_DASHBOARD_EXCEL_REQUEST, exportDashboardExcelRequest)

  //Cancellation And Rejection Reasons::
  yield takeLatest(constants.CANCEL_AND_REJECT_REASON_REQUEST, getCancelAndRejectReasonRequest);

  //---------------------------- SEASON MAPPING PLANNING ----------------------------
  yield takeLatest(constants.GET_SEASON_PLANNING_REQUEST, getSeasonPlanningRequest);
  yield takeLatest(constants.SAVE_SEASON_PLANNING_REQUEST, saveSeasonPlanningRequest);
  yield takeLatest(constants.DELETE_SEASON_PLANNING_REQUEST, deleteSeasonPlanningRequest);
  yield takeLatest(constants.GET_EVENT_PLANNING_REQUEST, getEventPlanningRequest);
  yield takeLatest(constants.SAVE_EVENT_PLANNING_REQUEST, saveEventPlanningRequest);
  yield takeLatest(constants.DELETE_EVENT_PLANNING_REQUEST, deleteEventPlanningRequest);
  yield takeLatest(constants.GET_SITE_PLANNING_REQUEST, getSitePlanningRequest);
  yield takeLatest(constants.GET_SITE_PLANNING_FILTERS_REQUEST, getSitePlanningFiltersRequest);
  yield takeLatest(constants.SAVE_SITE_PLANNING_REQUEST, saveSitePlanningRequest);
  yield takeLatest(constants.DELETE_SITE_PLANNING_REQUEST, deleteSitePlanningRequest);
  yield takeLatest(constants.GET_PLANNING_PARAMETER_REQUEST, getPlanningParameterRequest);
  yield takeLatest(constants.SAVE_PLANNING_PARAMETER_REQUEST, savePlanningParameterRequest);
  yield takeLatest(constants.GET_ITEM_CONFIGURATION_REQUEST, getItemConfigurationRequest);
  yield takeLatest(constants.SAVE_ITEM_CONFIGURATION_REQUEST, saveItemConfigurationRequest);
  yield takeLatest(constants.GET_SITE_AND_ITEM_FILTER_REQUEST, getSiteAndItemFilterRequest);
  yield takeLatest(constants.SAVE_SITE_AND_ITEM_FILTER_REQUEST, saveSiteAndItemFilterRequest);
  yield takeLatest(constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_REQUEST, getSiteAndItemFilterConfigurationRequest);
  yield takeLatest(constants.GET_ARS_GENERIC_FILTERS_REQUEST, getArsGenericFiltersRequest);
  yield takeLatest(constants.GET_ADHOC_REQUEST, getAdhocRequest);
  yield takeLatest(constants.DELETE_ADHOC_REQUEST, deleteAdhocRequest);
  yield takeLatest(constants.UPDATE_ADHOC_REQUEST, updateAdhocRequest);
  yield takeLatest(constants.SEARCH_ADHOC_SITE_REQUEST, searchAdhocSiteRequest);
  yield takeLatest(constants.SEARCH_ADHOC_ITEM_REQUEST, searchAdhocItemRequest);
  yield takeLatest(constants.SAVE_ADHOC_REQUEST, saveAdhocRequest);
  yield takeLatest(constants.GET_ADHOC_ITEMS_PER_WO_REQUEST, getAdhocItemsPerWORequest);
  yield takeLatest(constants.UPDATE_ADHOC_ITEMS_PER_WO_REQUEST, updateAdhocItemsPerWORequest);
  yield takeLatest(constants.GET_MRP_RANGE_REQUEST, getMRPRangeRequest);
  yield takeLatest(constants.UPDATE_MRP_RANGE_REQUEST, updateMRPRangeRequest);
  yield takeLatest(constants.GET_ALLOCATION_BASED_ON_ITEM_REQUEST, getAllocationBasedOnItemRequest);
  yield takeLatest(constants.SAVE_ALLOCATION_BASED_ON_ITEM_REQUEST, saveAllocationBasedOnItemRequest);
  yield takeLatest(constants.GET_ASSORTMENT_PLANNING_REQUEST, getAssortmentPlanningRequest);
  yield takeLatest(constants.FIND_FILTER_ASSORTMENT_REQUEST, findFilterAssortmentRequest);
  yield takeLatest(constants.SAVE_ASSORTMENT_PLANNING_REQUEST, saveAssortmentPlanningRequest);
  yield takeLatest(constants.GET_INVENTORY_PLANNING_REPORT_REQUEST, getInventoryPlanningReportRequest);
  yield takeLatest(constants.GET_SALES_INVENTORY_REPORT_REQUEST, getSalesInventoryReportRequest);
  yield takeLatest(constants.UPDATE_INVENTORY_PLANNING_REPORT_REQUEST, updateInventoryPlanningReportRequest);
  yield takeLatest(constants.DOWNLOAD_REPORT_REQUEST, downloadReportRequest);
  yield takeLatest(constants.GET_MRP_HISTORY_REQUEST, getMRPHistoryRequest);

  yield takeLatest(constants.GET_SALES_CONTRIBUTION_REPORT_REQUEST, getSalesContributionReportRequest);
  yield takeLatest(constants.GET_SALES_COMPARISON_REPORT_REQUEST, getSalesComparisonReportRequest);
  yield takeLatest(constants.GET_SELL_THRU_PERFORMANCE_REPORT_REQUEST, getSellThruPerformanceReportRequest);
  yield takeLatest(constants.GET_TOP_MOVING_ITEMS_REPORT_REQUEST, getTopMovingItemsReportRequest);
  yield takeLatest(constants.GET_CATEGORY_SIZE_WISE_REPORT_REQUEST, getCategorySizeWiseReportRequest);
  
  yield takeLatest(constants.GET_ALL_COMMENTS_REQUEST, getAllCommentsRequest);

  //-------------------- GET ALL TRANSPORTERS --------------------
  yield takeLatest(constants.GET_ALL_MANAGE_TRANSPORTER_REQUEST, getAllManageTransporterRequest);
  yield takeLatest(constants.CREATE_TRANSPORTER_REQUEST, createTransporterRequest);
  yield takeLatest(constants.UPDATE_MANAGE_TRANSPORTER_REQUEST, updateManageTransporterRequest);
  yield takeLatest(constants.UPDATE_CREATED_TRANSPORTER_REQUEST, updateCreatedTransporterRequest);
  yield takeLatest(constants.ACCESS_VENDOR_PORTAL_REQUEST, accessVendorPortalRequest);

  yield takeLatest(constants.GET_SUBSCRIPTION_REQUEST, getSubscriptionRequest);
  yield takeLatest(constants.GET_TRANSACTION_REQUEST, getTransactionRequest);
  
  yield takeLatest(constants.EXCEL_SEARCH_REQUEST, excelSearchRequest)
  yield takeLatest(constants.HANDLE_VALIDATE_REQUEST, handleValidateRequest)
  yield takeLatest(constants.EXCEL_HEADERS_REQUEST, excelHeadersRequest)
  yield takeLatest(constants.EXCEL_SUBMIT_REQUEST, excelSubmitRequest)
  yield takeLatest(constants.DOWNLOAD_TEMPLATE_REQUEST, downloadTemplateRequest)

  
  

  //-------------------- GET ALL CUSTOMERS --------------------
  yield takeLatest(constants.GET_ALL_MANAGE_CUSTOMER_REQUEST, getAllManageCustomerRequest);
  yield takeLatest(constants.EDIT_CUSTOMER_REQUEST, editCustomerRequest);

  //-------------------- GET ALL ITEMS --------------------
  yield takeLatest(constants.GET_ALL_MANAGE_ITEM_REQUEST, getAllManageItemRequest);
  
  yield takeLatest(constants.GET_ALL_LEDGER_REPORT_REQUEST, getAllLedgerReportRequest);
  yield takeLatest(constants.GET_ALL_SALE_REPORT_REQUEST, getAllSaleReportRequest);
  yield takeLatest(constants.GET_ALL_STOCK_REPORT_REQUEST, getAllStockReportRequest);
  yield takeLatest(constants.GET_ALL_OUTSTANDING_REPORT_REQUEST, getAllOutStandingReportRequest);
  yield takeLatest(constants.GET_LEDGER_FILTER_REPORT_REQUEST, getLedgerFilterReportRequest);

  yield takeLatest(constants.SEND_EMAIL_REQUEST, sendEmailRequest);

  //-------------------- GET ALL SALES AGENTS --------------------
  yield takeLatest(constants.GET_ALL_MANAGE_SALES_AGENT_REQUEST, getAllManageSalesAgentRequest);

  //-------------------- GET ALL STORE ANALYSIS --------------------
  yield takeLatest(constants.GET_ALL_STORE_ANALYSIS_REQUEST, getAllStoreAnalysisRequest);

  //-------------------- CREATE STORE ANALYSIS --------------------
  yield takeLatest(constants.CREATE_STORE_ANALYSIS_REQUEST, createStoreAnalysisRequest);

  //-------------------- GET DATA STORE ANALYSIS --------------------
  yield takeEvery(constants.GET_DATA_STORE_ANALYSIS_REQUEST, getDataStoreAnalysisRequest);

  

  //-------------------- GET SUPPORT ACCESS --------------------
  yield takeLatest(constants.GET_SUPPORT_ACCESS_REQUEST, getSupportAccessRequest);

  //-------------------- CREATE SUPPORT ACCESS --------------------
  yield takeLatest(constants.CREATE_SUPPORT_ACCESS_REQUEST, createSupportAccessRequest);
  //-------------------- GET API ACCESS DETAILS --------------------
  yield takeLatest(constants.GET_API_ACCESS_DETAILS_REQUEST, getApiAccessDetailsRequest);

  //-------------------- GET EMAIL NOTIFICATION LOG --------------------
  yield takeLatest(constants.GET_EMAIL_NOTIFICATION_LOG_REQUEST, getEmailNotificationLogRequest);

  //-------------------- RESEND EMAIL NOTIFICATION --------------------
  yield takeLatest(constants.RESEND_EMAIL_NOTIFICATION_REQUEST, resendEmailNotificationRequest);

  //-------------------- GET EMAIL BODY --------------------
  yield takeLatest(constants.GET_EMAIL_BODY_REQUEST, getEmailBodyRequest);

  yield takeLatest(constants.DOWNLOAD_REP_REPORT_REQUEST, downloadRepReportRequest);
  yield takeLatest(constants.GET_KYC_DETAILS_REQUEST, getKycDetailsRequest);
  yield takeLatest(constants.SAVE_KYC_DETAILS_REQUEST, saveKycDetailsRequest);

  yield takeLatest(constants.GET_INVOICE_MANAGEMENT_ITEM_REQUEST, getInvoiceManagementItemRequest);
  yield takeLatest(constants.SAVE_INVOICE_MANAGEMENT_ITEM_REQUEST, saveInvoiceManagementItemRequest);
  yield takeLatest(constants.DELETE_INVOICE_MANAGEMENT_ITEM_REQUEST, deleteInvoiceManagementItemRequest);
  yield takeLatest(constants.UPDATE_INVOICE_MANAGEMENT_ITEM_REQUEST, updateInvoiceManagementItemRequest);
  yield takeLatest(constants.GET_INVOICE_MANAGEMENT_CUSTOMER_REQUEST, getInvoiceManagementCustomerRequest);
  yield takeLatest(constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST, saveInvoiceManagementCustomerRequest);
  yield takeLatest(constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST, deleteInvoiceManagementCustomerRequest);
  yield takeLatest(constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST, updateInvoiceManagementCustomerRequest);

  yield takeLatest(constants.GET_INVOICE_MANAGEMENT_GENERIC_REQUEST, getInvoiceManagementGenericRequest);
  yield takeLatest(constants.DELETE_INVOICE_MANAGEMENT_GENERIC_REQUEST, deleteInvoiceManagementGenericRequest);
  yield takeLatest(constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_REQUEST, expandInvoiceManagementGenericRequest);


  yield takeLatest(constants.GET_USER_DATA_MAPPING_REQUEST, getUserDataMappingRequest);
  yield takeLatest(constants.UPDATE_USER_DATA_MAPPING_REQUEST, updateUserDataMappingRequest);


  yield takeLatest(constants.GET_INSERT_REQUEST, getInsertRequest);

  yield takeLatest(constants.GET_INSERT_REQUEST, saveInsertRequest);

  yield takeLatest(constants.GET_VENDOR_ACTIVITY_REQUEST, getVendorActivityRequest);
  yield takeLatest(constants.GET_VENDOR_LOGS_REQUEST, getVendorLogsRequest);
  yield takeEvery(constants.GET_CORE_DROPDOWN_REQUEST, getCoreDropdownRequest);
  yield takeLatest(constants.SEARCH_INVOICE_DATA_REQUEST, searchInvoiceDataRequest);
  yield takeLatest(constants.CREATE_INVOICE_REQUEST, createInvoiceRequest);
  yield takeLatest(constants.GET_ALL_LOG_TRANS_PAY_REPORT_REQUEST, getAllLogTransPayReportRequest);
  yield takeLatest(constants.GET_ALL_LOG_WH_STOCK_REPORT_REQUEST, getAllLogWhStockReportRequest);
  yield takeLatest(constants.GET_PO_STOCK_REPORT_REQUEST, getPoStockReportRequest);
  yield takeLatest(constants.GET_PO_COLOR_MAP_REQUEST, getPoColorMapRequest);
  yield takeLatest(constants.CREATE_PO_ARTICLE_REQUEST, createPoArticleRequest);
  yield takeLatest(constants.ALL_RETAILER_CUSTOMER_REQUEST, allRetailerCustomerRequest);
  yield takeLatest(constants.CUSTOMER_USER_REQUEST, customerUserRequest);
  yield takeLatest(constants.ADD_USER_CUSTOMER_REQUEST, addUserCustomerRequest);
  yield takeLatest(constants.EDIT_USER_CUSTOMER_REQUEST, editUserCustomerRequest);
  yield takeLatest(constants.USER_STATUS_CUSTOMER_REQUEST, userStatusCustomerRequest);
  yield takeLatest(constants.GET_SUBSCRIPTION_CUSTOMER_REQUEST, getSubscriptionCustomerRequest);
  yield takeLatest(constants.GET_TRANSACTION_CUSTOMER_REQUEST, getTransactionCustomerRequest);
}
export default function* rootSaga() {
  yield [watchActions()];
}