import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
  generateTotalSales: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  generateUnitSold: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getStoreProfileCode: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getStoreData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getSellThru: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getFestivalImpact: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  generateTotalSalesVsProfit: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  generateSalesPerSquareFoot: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getTopArticle: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getSalesTrend: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  stockHand: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  reviewReason: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  arsPwqData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  arsPwqFilters: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  assortmentDetails: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  assortmentItems: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  assortmentTimePhased: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  assortmentSalesHistory: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  allocationData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getAllStoreAnalysis: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createStoreAnalysis: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getDataStoreAnalysis: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
};

const generateTotalSalesRequest = (state, action) => update(state, {
  generateTotalSales: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const generateTotalSalesSuccess = (state, action) => update(state, {
  generateTotalSales: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const generateTotalSalesError = (state, action) => update(state, {
  generateTotalSales: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const generateTotalSalesClear = (state, action) => update(state, {
  generateTotalSales: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// __________________________SOLD UNITE _______________________________


const generateUnitSoldRequest = (state, action) => update(state, {
  generateUnitSold: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const generateUnitSoldSuccess = (state, action) => update(state, {
  generateUnitSold: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const generateUnitSoldError = (state, action) => update(state, {
  generateUnitSold: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const generateUnitSoldClear = (state, action) => update(state, {
  generateUnitSold: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


// ___________________--GET STORE DATA__________________________-


const getStoreDataRequest = (state, action) => update(state, {
  getStoreData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getStoreDataSuccess = (state, action) => update(state, {
  getStoreData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getStoreDataError = (state, action) => update(state, {
  getStoreData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getStoreDataClear = (state, action) => update(state, {
  getStoreData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// __________________________SOLD UNITE _______________________________


const getStoreProfileCodeRequest = (state, action) => update(state, {
  getStoreProfileCode: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getStoreProfileCodeSuccess = (state, action) => update(state, {
  getStoreProfileCode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getStoreProfileCodeError = (state, action) => update(state, {
  getStoreProfileCode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getStoreProfileCodeClear = (state, action) => update(state, {
  getStoreProfileCode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getFestivalImpactRequest = (state, action) => update(state, {
  getFestivalImpact: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getFestivalImpactSuccess = (state, action) => update(state, {
  getFestivalImpact: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getFestivalImpactError = (state, action) => update(state, {
  getFestivalImpact: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getFestivalImpactClear = (state, action) => update(state, {
  getFestivalImpact: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const generateTotalSalesVsProfitRequest = (state, action) => update(state, {
  generateTotalSalesVsProfit: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const generateTotalSalesVsProfitSuccess = (state, action) => update(state, {
  generateTotalSalesVsProfit: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const generateTotalSalesVsProfitError = (state, action) => update(state, {
  generateTotalSalesVsProfit: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const generateTotalSalesVsProfitClear = (state, action) => update(state, {
  generateTotalSalesVsProfit: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const generateSalesPerSquareFootRequest = (state, action) => update(state, {
  generateSalesPerSquareFoot: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const generateSalesPerSquareFootSuccess = (state, action) => update(state, {
  generateSalesPerSquareFoot: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const generateSalesPerSquareFootError = (state, action) => update(state, {
  generateSalesPerSquareFoot: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const generateSalesPerSquareFootClear = (state, action) => update(state, {
  generateSalesPerSquareFoot: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getSellThruRequest = (state, action) => update(state, {
  getSellThru: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getSellThruSuccess = (state, action) => update(state, {
  getSellThru: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getSellThruError = (state, action) => update(state, {
  getSellThru: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getSellThruClear = (state, action) => update(state, {
  getSellThru: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});



const getTopArticleRequest = (state, action) => update(state, {
  getTopArticle: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getTopArticleSuccess = (state, action) => update(state, {
  getTopArticle: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getTopArticleError = (state, action) => update(state, {
  getTopArticle: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getTopArticleClear = (state, action) => update(state, {
  getTopArticle: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getSalesTrendRequest = (state, action) => update(state, {
  getSalesTrend: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getSalesTrendSuccess = (state, action) => update(state, {
  getSalesTrend: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getSalesTrendError = (state, action) => update(state, {
  getSalesTrend: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getSalesTrendClear = (state, action) => update(state, {
  getSalesTrend: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


// _________________ STOCK IN HAND________________________-

const stockHandRequest = (state, action) => update(state, {
  stockHand: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const stockHandSuccess = (state, action) => update(state, {
  stockHand: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const stockHandError = (state, action) => update(state, {
  stockHand: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const stockHandClear = (state, action) => update(state, {
  stockHand: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//ars dashboard

const getReviewReasonRequest = (state, action) => update(state, {
  reviewReason : {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getReviewReasonSuccess = (state, action) => update(state, {
  reviewReason: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getReviewReasonFailure = (state, action) => update(state, {
  reviewReason: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
}); 
const getReviewReasonClear = (state, action) => update(state, {
  reviewReason: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getDataWithoutFilterRequest = (state, action) => update(state, {
  arsPwqData : {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getDataWithoutFilterSuccess = (state, action) => update(state, {
  arsPwqData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getDataWithoutFilterFailure = (state, action) => update(state, {
  arsPwqData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
}); 
const getDataWithoutFilterClear = (state, action) => update(state, {
  arsPwqData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getfiltersArsDataRequest = (state, action) => update(state, {
  arsPwqFilters : {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getfiltersArsDataSuccess = (state, action) => update(state, {
  arsPwqFilters: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getfiltersArsDataError = (state, action) => update(state, {
  arsPwqFilters: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
}); 
const getfiltersArsDataClear = (state, action) => update(state, {
  arsPwqFilters: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getAssortmentDetailsRequest = (state, action) => update(state, {
  assortmentDetails: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAssortmentDetailsSuccess = (state, action) => update(state, {
  assortmentDetails: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getAssortmentDetailsError = (state, action) => update(state, {
  assortmentDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getAssortmentDetailsClear = (state, action) => update(state, {
  assortmentDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const getAssortmentItemsRequest = (state, action) => update(state, {
  assortmentItems: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAssortmentItemsSuccess = (state, action) => update(state, {
  assortmentItems: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getAssortmentItemsError = (state, action) => update(state, {
  assortmentItems: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getAssortmentItemsClear = (state, action) => update(state, {
  assortmentItems: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const getAssortmentTimePhasedRequest = (state, action) => update(state, {
  assortmentTimePhased: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAssortmentTimePhasedSuccess = (state, action) => update(state, {
  assortmentTimePhased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getAssortmentTimePhasedError = (state, action) => update(state, {
  assortmentTimePhased: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getAssortmentTimePhasedClear = (state, action) => update(state, {
  assortmentTimePhased: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const getAssortmentSalesHistoryRequest = (state, action) => update(state, {
  assortmentSalesHistory: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAssortmentSalesHistorySuccess = (state, action) => update(state, {
  assortmentSalesHistory: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getAssortmentSalesHistoryError = (state, action) => update(state, {
  assortmentSalesHistory: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getAssortmentSalesHistoryClear = (state, action) => update(state, {
  assortmentSalesHistory: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getAllocationDataRequest = (state, action) => update(state, {
  allocationData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllocationDataSuccess = (state, action) => update(state, {
  allocationData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getAllocationDataError = (state, action) => update(state, {
  allocationData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getAllocationDataClear = (state, action) => update(state, {
  allocationData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//-------------------- GET ALL STORE ANALYSIS --------------------
const getAllStoreAnalysisRequest = (state, action) => update(state, {
  getAllStoreAnalysis: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllStoreAnalysisClear = (state, action) => update(state, {
  getAllStoreAnalysis: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAllStoreAnalysisSuccess = (state, action) => update(state, {
  getAllStoreAnalysis: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Get Store Analysis success' }
  }
});

const getAllStoreAnalysisError = (state, action) => update(state, {
  getAllStoreAnalysis: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

//-------------------- CREATE STORE ANALYSIS --------------------
const createStoreAnalysisRequest = (state, action) => update(state, {
  createStoreAnalysis: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createStoreAnalysisClear = (state, action) => update(state, {
  createStoreAnalysis: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createStoreAnalysisSuccess = (state, action) => update(state, {
  createStoreAnalysis: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Create Store Analysis success' }
  }
});

const createStoreAnalysisError = (state, action) => update(state, {
  createStoreAnalysis: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

//-------------------- GET DATA STORE ANALYSIS --------------------
const getDataStoreAnalysisRequest = (state, action) => update(state, {
  getDataStoreAnalysis: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getDataStoreAnalysisClear = (state, action) => update(state, {
  getDataStoreAnalysis: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getDataStoreAnalysisSuccess = (state, action) => {
  console.log(state.getDataStoreAnalysis, action);
  return update(state, {
  getDataStoreAnalysis: {
    data: { $set: {...state.getDataStoreAnalysis.data, ["type" + action.payload.type]: action.payload.data} },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Get Data Store Analysis success' }
  }
})};

// const getDataStoreAnalysisSuccess = (state, action) => update(state, {
//   getDataStoreAnalysis: {
//     data: { $set: action.payload },
//     isLoading: { $set: false },
//     isError: { $set: false },
//     isSuccess: { $set: true },
//     message: { $set: 'Get Data Store Analysis success' }
//   }
// });

const getDataStoreAnalysisError = (state, action) => update(state, {
  getDataStoreAnalysis: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


export default handleActions({
  [constants.GENERATE_TOTAL_SALES_REQUEST]: generateTotalSalesRequest,
  [constants.GENERATE_TOTAL_SALES_SUCCESS]: generateTotalSalesSuccess,
  [constants.GENERATE_TOTAL_SALES_ERROR]: generateTotalSalesError,
  [constants.GENERATE_TOTAL_SALES_CLEAR]: generateTotalSalesClear,

  [constants.GENERATE_UNIT_SOLD_REQUEST]: generateUnitSoldRequest,
  [constants.GENERATE_UNIT_SOLD_SUCCESS]: generateUnitSoldSuccess,
  [constants.GENERATE_UNIT_SOLD_ERROR]: generateUnitSoldError,
  [constants.GENERATE_UNIT_SOLD_CLEAR]: generateUnitSoldClear,


  [constants.GET_STORE_PROFILE_CODE_REQUEST]: getStoreProfileCodeRequest,
  [constants.GET_STORE_PROFILE_CODE_SUCCESS]: getStoreProfileCodeSuccess,
  [constants.GET_STORE_PROFILE_CODE_ERROR]: getStoreProfileCodeError,
  [constants.GET_STORE_PROFILE_CODE_CLEAR]: getStoreProfileCodeClear,

  [constants.GET_STORE_DATA_REQUEST]: getStoreDataRequest,
  [constants.GET_STORE_DATA_SUCCESS]: getStoreDataSuccess,
  [constants.GET_STORE_DATA_ERROR]: getStoreDataError,
  [constants.GET_STORE_DATA_CLEAR]: getStoreDataClear,



  [constants.GENERATE_SALES_PER_SQUARE_FOOT_REQUEST]: generateSalesPerSquareFootRequest,
  [constants.GENERATE_SALES_PER_SQUARE_FOOT_SUCCESS]: generateSalesPerSquareFootSuccess,
  [constants.GENERATE_SALES_PER_SQUARE_FOOT_ERROR]: generateSalesPerSquareFootError,
  [constants.GENERATE_SALES_PER_SQUARE_FOOT_CLEAR]: generateSalesPerSquareFootClear,

  [constants.GENERATE_TOTALSALES_VS_PROFIT_REQUEST]: generateTotalSalesVsProfitRequest,
  [constants.GENERATE_TOTALSALES_VS_PROFIT_SUCCESS]: generateTotalSalesVsProfitSuccess,
  [constants.GENERATE_TOTALSALES_VS_PROFIT_ERROR]: generateTotalSalesVsProfitError,
  [constants.GENERATE_TOTALSALES_VS_PROFIT_CLEAR]: generateTotalSalesVsProfitClear,


  [constants.GET_SELL_THRU_REQUEST]: getSellThruRequest,
  [constants.GET_SELL_THRU_SUCCESS]: getSellThruSuccess,
  [constants.GET_SELL_THRU_ERROR]: getSellThruError,
  [constants.GET_SELL_THRU_CLEAR]: getSellThruClear,

  [constants.GET_FESTIVAL_IMPACT_REQUEST]: getFestivalImpactRequest,
  [constants.GET_FESTIVAL_IMPACT_SUCCESS]: getFestivalImpactSuccess,
  [constants.GET_FESTIVAL_IMPACT_ERROR]: getFestivalImpactError,
  [constants.GET_FESTIVAL_IMPACT_CLEAR]: getFestivalImpactClear,


  [constants.GET_TOP_ARTICLE_REQUEST]: getTopArticleRequest,
  [constants.GET_TOP_ARTICLE_SUCCESS]: getTopArticleSuccess,
  [constants.GET_TOP_ARTICLE_ERROR]: getTopArticleError,
  [constants.GET_TOP_ARTICLE_CLEAR]: getTopArticleClear,


  [constants.GET_SALES_TREND_REQUEST]: getSalesTrendRequest,
  [constants.GET_SALES_TREND_SUCCESS]: getSalesTrendSuccess,
  [constants.GET_SALES_TREND_ERROR]: getSalesTrendError,
  [constants.GET_SALES_TREND_CLEAR]: getSalesTrendClear,

  [constants.STOCK_HAND_REQUEST]: stockHandRequest,
  [constants.STOCK_HAND_SUCCESS]: stockHandSuccess,
  [constants.STOCK_HAND_ERROR]: stockHandError,
  [constants.STOCK_HAND_CLEAR]: stockHandClear,

  [constants.GET_REVIEW_REASON_REQUEST]: getReviewReasonRequest,
  [constants.GET_REVIEW_REASON_SUCCESS]: getReviewReasonSuccess,
  [constants.GET_REVIEW_REASON_FAILURE]: getReviewReasonFailure,
  [constants.GET_REVIEW_REASON_CLEAR]: getReviewReasonClear,

  [constants.GET_DATA_WITHOUT_FILTER_REQUEST]: getDataWithoutFilterRequest,
  [constants.GET_DATA_WITHOUT_FILTER_SUCCESS]: getDataWithoutFilterSuccess,
  [constants.GET_DATA_WITH_FILTER_FAILURE]: getDataWithoutFilterFailure,
  [constants.GET_DATA_WITH_FILTER_CLEAR]: getDataWithoutFilterClear,

  [constants.GET_FILLTERS_ASR_DATA_REQUEST]: getfiltersArsDataRequest,
  [constants.GET_FILLTERS_ASR_DATA_SUCCESS]: getfiltersArsDataSuccess,
  [constants.GET_FILLTERS_ASR_DATA_ERROR]: getfiltersArsDataError,
  [constants.GET_FILLTERS_ASR_DATA_CLEAR]: getfiltersArsDataClear,


  [constants.GET_ASSORTMENT_DETAILS_REQUEST]: getAssortmentDetailsRequest,
  [constants.GET_ASSORTMENT_DETAILS_SUCCESS]: getAssortmentDetailsSuccess,
  [constants.GET_ASSORTMENT_DROPDOWN_ERROR]: getAssortmentDetailsError,
  [constants.GET_ASSORTMENT_DETAILS_CLEAR]: getAssortmentDetailsClear,

  [constants.GET_ASSORTMENT_TIME_PHASED_REQUEST]: getAssortmentTimePhasedRequest,
  [constants.GET_ASSORTMENT_TIME_PHASED_SUCCESS]: getAssortmentTimePhasedSuccess,
  [constants.GET_ASSORTMENT_TIME_PHASED_ERROR]: getAssortmentTimePhasedError,
  [constants.GET_ASSORTMENT_TIME_PHASED_CLEAR]: getAssortmentTimePhasedClear,

  [constants.GET_ASSORTMENT_ITEMS_REQUEST]: getAssortmentItemsRequest,
  [constants.GET_ASSORTMENT_ITEMS_SUCCESS]: getAssortmentItemsSuccess,
  [constants.GET_ASSORTMENT_ITEMS_ERROR]: getAssortmentItemsError,
  [constants.GET_ASSORTMENT_ITEMS_CLEAR]: getAssortmentItemsClear,

  [constants.GET_ASSORTMENT_SALES_HISTORY_REQUEST]: getAssortmentSalesHistoryRequest,
  [constants.GET_ASSORTMENT_SALES_HISTORY_SUCCESS]: getAssortmentSalesHistorySuccess,
  [constants.GET_ASSORTMENT_SALES_HISTORY_ERROR]: getAssortmentSalesHistoryError,
  [constants.GET_ASSORTMENT_SALES_HISTORY_CLEAR]: getAssortmentSalesHistoryClear,

  [constants.GET_ALLOCATION_DATA_REQUEST]: getAllocationDataRequest,
  [constants.GET_ALLOCATION_DATA_SUCCESS]: getAllocationDataSuccess,
  [constants.GET_ALLOCATION_DATA_ERROR]: getAllocationDataError,
  [constants.GET_ALLOCATION_DATA_CLEAR]: getAllocationDataClear,

  [constants.GET_ALL_STORE_ANALYSIS_CLEAR]: getAllStoreAnalysisClear,
  [constants.GET_ALL_STORE_ANALYSIS_REQUEST]: getAllStoreAnalysisRequest,
  [constants.GET_ALL_STORE_ANALYSIS_SUCCESS]: getAllStoreAnalysisSuccess,
  [constants.GET_ALL_STORE_ANALYSIS_ERROR]: getAllStoreAnalysisError,

  [constants.CREATE_STORE_ANALYSIS_CLEAR]: createStoreAnalysisClear,
  [constants.CREATE_STORE_ANALYSIS_REQUEST]: createStoreAnalysisRequest,
  [constants.CREATE_STORE_ANALYSIS_SUCCESS]: createStoreAnalysisSuccess,
  [constants.CREATE_STORE_ANALYSIS_ERROR]: createStoreAnalysisError,

  [constants.GET_DATA_STORE_ANALYSIS_CLEAR]: getDataStoreAnalysisClear,
  [constants.GET_DATA_STORE_ANALYSIS_REQUEST]: getDataStoreAnalysisRequest,
  [constants.GET_DATA_STORE_ANALYSIS_SUCCESS]: getDataStoreAnalysisSuccess,
  [constants.GET_DATA_STORE_ANALYSIS_ERROR]: getDataStoreAnalysisError,
  
}, initialState);
