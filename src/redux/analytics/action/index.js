import { call, put, takeEvery } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';

export function* generateTotalSalesRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.generateTotalSalesClear());
  }
  else {
    try {
      let storeCode = action.payload.storeCode
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}/get/sell/qty/graph?storeCode=${storeCode}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.generateTotalSalesSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.generateTotalSalesError(response.data));
      }

    } catch (e) {
      yield put(actions.generateTotalSalesError("error occurs"));
      console.warn('Some error found in "generateTotalSales" action\n', e);
    }
  }
}

// ________________________UNIT SOLD API___________________________-

export function* generateUnitSoldRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.generateUnitSoldClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.generateUnitSoldSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.generateUnitSoldError(response.data));
      }

    } catch (e) {
      yield put(actions.generateUnitSoldError("error occurs"));
      console.warn('Some error found in "generateUnitSold" action\n', e);
    }
  }
}

// ________________________STORE DATA___________________________-

export function* getStoreDataRequest(action) {

  try {

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}/get/store/code`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getStoreDataSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getStoreDataError(response.data));
    }

  } catch (e) {
    yield put(actions.getStoreDataError("error occurs"));
    console.warn('Some error found in "getStoreData" action\n', e);
  }
}



// ________________________STORE CODE________________________-

export function* generateSalesPerSquareFootRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.generateSalesPerSquareFootClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.generateSalesPerSquareFootSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.generateSalesPerSquareFootError(response.data));
      }

    } catch (e) {
      yield put(actions.generateSalesPerSquareFootError("error occurs"));
      console.warn('Some error found in "generateSalesPerSquareFoot" action\n', e);
    }
  }
}



export function* generateTotalSalesVsProfitRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.generateTotalSalesVsProfitClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.generateTotalSalesVsProfitSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.generateTotalSalesVsProfitError(response.data));
      }

    } catch (e) {
      yield put(actions.generateTotalSalesVsProfitError("error occurs"));
      console.warn('Some error found in "generateTotalSalesVsProfit" action\n', e);
    }
  }
}




export function* getSellThruRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getSellThruClear());
  }
  else {
    try {
      let sellThruType = action.payload.sellThruType
      let storeCode = action.payload.storeCode
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}/get/sell/thru?sellThroghType=${sellThruType}&storeCode=${storeCode}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getSellThruSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getSellThruError(response.data));
      }

    } catch (e) {
      yield put(actions.getSellThruError("error occurs"));
      console.warn('Some error found in "getSellThru" action\n', e);
    }
  }
}



export function* getFestivalImpactRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getStoreDataClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getFestivalImpactSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getFestivalImpactError(response.data));
      }

    } catch (e) {
      yield put(actions.getFestivalImpactError("error occurs"));
      console.warn('Some error found in "getFestivalImpact" action\n', e);
    }
  }
}



export function* getStoreProfileCodeRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getStoreProfileCodeClear());
  }
  else {
    try {
      let storeCode = action.payload.storeCode;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}/get/store/info?storeCode=${storeCode}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getStoreProfileCodeSuccess(response.data.data));


      } else if (finalResponse.failure) {
        yield put(actions.getStoreProfileCodeError(response.data));
      }

    } catch (e) {
      yield put(actions.getStoreProfileCodeError("error occurs"));
      // console.warn('Some error found in "getStoreProfileCode" action\n', e);
    }
  }
}

export function* getTopArticleRequest(action) {

  try {

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}/get/top/article`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getTopArticleSuccess(response.data.data));


    } else if (finalResponse.failure) {
      yield put(actions.getTopArticleError(response.data));
    }

  } catch (e) {
    yield put(actions.getTopArticleError("error occurs"));
    console.warn('Some error found in "getTopArticle" action\n', e);
  }
}


export function* getSalesTrendRequest(action) {

  try {

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}/get/sell/profit/trends`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getSalesTrendSuccess(response.data.data));


    } else if (finalResponse.failure) {
      yield put(actions.getSalesTrendError(response.data));
    }

  } catch (e) {
    yield put(actions.getSalesTrendError("error occurs"));
    console.warn('Some error found in "getSalesTrend" action\n', e);
  }
}

// ___________________ STOCK IN HAND ______________________

export function* stockHandRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.stockHandClear());
  }
  try {
    let storeCode = action.payload.storeCode;
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.STORE_PROFILE}/get/stock/hand?storeCode=${storeCode}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.stockHandSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.stockHandError(response.data));
    }

  } catch (e) {
    
    yield put(actions.stockHandError("error occurs"));
    console.warn('Some error found in "stockHand" action\n', e);
  }
}

// _______________________ ARS _______________________________
export function* getReviewReasonRequest(action) {
  if(action.payload == undefined) {
    yield put(actions.getReviewReasonClear());
    console.warn("undefined action payload");
  }else{ 
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/review-reason`);
      const finalResponse = script(response);
      if(finalResponse.success) {
        yield put(actions.getReviewReasonSuccess(response.data))
      } else if(finalResponse.failure) {
        yield put(actions.getReviewReasonFailure(response.data))
      }
    }
    catch (e) {
      yield put(actions.getReviewReasonFailure("error occurred"));
      // console.warn('Some error found in "ReviewReason" action\n', e);
    }
  }
}

export function* getDataWithoutFilterRequest(action) {
  if(action.payload == undefined) {
    yield put(actions.getDataWithoutFilterClear());
    console.warn("undefined action payload");
  }else{ 
    try {
      let storeCode= action.payload.filterOption.storeCode;
      let storeName = action.payload.filterOption.storeName;
      let division = action.payload.filterOption.division;
      let section = action.payload.filterOption.section;
      let department = action.payload.filterOption.department;
      let priceBand = action.payload.filterOption.priceBand;
      let reason = action.payload.selectOption;
      let type = action.payload.type;
      let pageNo = action.payload.page;
      let searchText = action.payload.searchText;
      let category = action.payload.filterOption.category;
      let sortedIn = action.payload.sortedIn;
      let sortedBy = action.payload.sortedBy;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/all?pageNo=${pageNo}&type=${type}&reason=${reason}&storeCode=${storeCode}&storeName=${storeName}&division=${division}&section=${section}&department=${department}&priceBand=${priceBand}&searchText=${searchText}&category=${category}&sortedIn=${sortedIn}&sortedBy=${sortedBy}`);
      const finalResponse = script(response);
      if(finalResponse.success) {
        yield put(actions.getDataWithoutFilterSuccess(response.data.data))
      } else if(finalResponse.failure) {
        yield put(actions.getDataWithoutFilterFailure(response.data))
      }
    }
    catch (e) {
      yield put(actions.getDataWithoutFilterFailure("error occurs"));
      console.warn('Some error found in "ReviewReason" action\n', e);
    }
  }
}

export function* getfiltersArsDataRequest(action) {
  if(action.payload == undefined) {
    yield put(actions.getfiltersArsDataClear());
    console.warn("undefined action payload");
  }else{
    try {
      let reviewReason = action.payload.reason;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/filters?reviewReason=${reviewReason}`);
      const finalResponse = script(response);
      if(finalResponse.success) {
        yield put(actions.getfiltersArsDataSuccess(response.data.data))
      } else if(finalResponse.failure) {
        yield put(actions.getfiltersArsDataError(response.data))
      }
    }
    catch(e) {
      yield put(actions.getfiltersArsDataError("error occurred"))
      console.warn('Some error found in "error in filter" action\n', e);
    }
  }
}

export function* getAssortmentDetailsRequest(action){
  if(action.payload == undefined) {
    yield put(actions.getAssortmentDetailsClear());
    console.warn("undefined action payload");
  }else{
    try {
      let assortmentNo = action.payload.assortmentNo == undefined ? 0 : action.payload.assortmentNo;
      let reason = action.payload.reason;
      let assortmentName = action.payload.assortmentName == undefined ? "" : action.payload.assortmentName;
      let storeCode = action.payload.storeCode;
      let storeName = action.payload.storeName;
      let division = action.payload.division;
      let section = action.payload.section;
      let department = action.payload.department;
      let priceBand = action.payload.priceBand;

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/assortment-details?reason=${reason}&assortmentNo=${assortmentNo}&assortmentName=${assortmentName}`);
      const finalResponse = script(response);
      if(finalResponse.success) {
        yield put(actions.getAssortmentDetailsSuccess(response.data))
      } else if(finalResponse.failure) {
        yield put(actions.getAssortmentDropdownError(response.data))
      }
    }
    catch(e) {
      yield put(actions.getAssortmentDropdownError("error occurred"))
      console.warn('Some error found in "error in filter" action\n', e);
    }
  }
}

export function* getAssortmentItemsRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getAssortmentItemsClear());
  } else {
    try {
      let storeCode = action.payload.storeCode;
      let pageNo = action.payload.pageNo;
      let type = action.payload.type;
      let division =action.payload.division;
      let department = action.payload.department;
      let reason = action.payload.reason;
      let section = action.payload.section;
      let category = action.payload.category;
      let searchText = action.payload.searchText;
      let priceBand = action.payload.priceBand;
      let sortedIn = action.payload.sortedIn;
      let sortedBy = action.payload.sortedBy;
      let assortment = action.payload.assortment == undefined ? "" : action.payload.assortment;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/assortment-items?storeCode=${storeCode}&pageNo=${pageNo}&type=${type}&division=${division}&section=${section}&department=${department}&category=${category}&priceBand=${priceBand}&reason=${reason}&searchText=${searchText}&sortedIn=${sortedIn}&sortedBy=${sortedBy}&assortment=${assortment}`);
      const finalResponse = script(response);

      if (finalResponse.success) {

        yield put(actions.getAssortmentItemsSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAssortmentItemsError(response.data));
      }
    } catch (e) {
      yield put(actions.getAssortmentItemsError("error occurs"));
      console.warn('Some error found in assortment items error action\n', e);
    }
  } 
}

export function* getAssortmentSalesHistoryRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getAssortmentSalesHistoryClear());
  } else {
    try {
      const type = action.payload.type;
      const assortment = action.payload.assortment;
      const storeCode = action.payload.storeCode;
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ARS}/post/sales-history`, {"type": type,"assortment":assortment,"storeCode":storeCode});
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getAssortmentSalesHistorySuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAssortmentSalesHistoryError(response.data));
      }
    } catch (e) {
      yield put(actions.getAssortmentSalesHistoryError("error occurs"));
      console.warn('Some error found in assortment sales history error action\n', e);
    }
  } 
}

export function* getAssortmentTimePhasedRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getAssortmentTimePhasedClear());
  } else {
    try {
      let pageNo = action.payload.pageNo;
      let type = action.payload.type;
      let storeCode = action.payload.storeCode;
      let searchText = action.payload.searchText;
      let sortedIn = action.payload.sortedIn;
      let sortedBy = action.payload.sortedBy;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/time-phased-view?storeCode=${storeCode}&pageNo=${pageNo}&type=${type}&searchText=${searchText}&sortedIn=${sortedIn}&sortedBy=${sortedBy}`);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getAssortmentTimePhasedSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAssortmentTimePhasedError(response.data));
      }
    } catch (e) {
      yield put(actions.getAssortmentTimePhasedError("error occurs"));
      console.warn('Some error found in assortment time phased error action\n', e);
    }
  } 
}

export function* getAllocationDataRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getAllocationDataClear());
  } else {
    try {
      let pageNo = action.payload.pageNo;
      let type = action.payload.type;
      let storeCode = action.payload.storeCode;
      let searchText = action.payload.searchText;
      let sortedIn = action.payload.sortedIn;
      let sortedBy = action.payload.sortedBy;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/allocation?storeCode=${storeCode}&pageNo=${pageNo}&type=${type}&searchText=${searchText}&sortedIn=${sortedIn}&sortedBy=${sortedBy}`);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getAllocationDataSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAllocationDataError(response.data));
      }
    } catch (e) {
      yield put(actions.getAllocationDataError("error occurs"));
      console.warn('Some error found in allocation action\n', e);
    }
  } 
}

//-------------------- GET ALL STORE ANALYSIS -------------------- (gives top 5 data, called upon Site change)
export function* getAllStoreAnalysisRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getAllStoreAnalysisClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/all/storeAnalysis`, {
        siteCode: action.payload
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getAllStoreAnalysisSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAllStoreAnalysisError(response.data));
      }
    } catch (e) {
      yield put(actions.getAllStoreAnalysisError("error occurs"));
      console.warn('Some error found in getAllStoreAnalysisRequest action\n', e);
    }
  } 
}

//-------------------- CREATE STORE ANALYSIS -------------------- (called upon Days change)
export function* createStoreAnalysisRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.createStoreAnalysisClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ARS}/create/storeAnalysis`, {
        noOfDays: action.payload
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.createStoreAnalysisSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.createStoreAnalysisError(response.data));
      }
    } catch (e) {
      yield put(actions.createStoreAnalysisError("error occurs"));
      console.warn('Some error found in createStoreAnalysisRequest action\n', e);
    }
  } 
}

//-------------------- GET DATA STORE ANALYSIS -------------------- (called for individual table)
// export function* getDataStoreAnalysisRequest(action) {
//   if (action.payload == undefined) {
//     yield put(actions.getDataStoreAnalysisClear());
//   } else {
//     try {
//       const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/data/storeAnalysis`, action.payload);
//       const finalResponse = script(response);
//       if (finalResponse.success) {
//         yield put(actions.getDataStoreAnalysisSuccess(response.data.data));
//       } else if (finalResponse.failure) {
//         yield put(actions.getDataStoreAnalysisError(response.data));
//       }
//     } catch (e) {
//       yield put(actions.getDataStoreAnalysisError("error occurs"));
//       console.warn('Some error found in getDataStoreAnalysisRequest action\n', e);
//     }
//   } 
// }

export function* getDataStoreAnalysisRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getDataStoreAnalysisClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ARS}/get/data/storeAnalysis`, action.payload);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getDataStoreAnalysisSuccess({data: response.data.data, type: action.payload.type}));
      } else if (finalResponse.failure) {
        yield put(actions.getDataStoreAnalysisError(response.data));
      }
    } catch (e) {
      yield put(actions.getDataStoreAnalysisError("error occurs"));
      console.warn('Some error found in getDataStoreAnalysisRequest action\n', e);
    }
  } 
}

// export function* getDataStoreAnalysisRequest() {
//   console.log("okkkkkkkkkkkkkkkkkk");
//   yield takeEvery("GET_DATA_STORE_ANALYSIS_REQUEST", getDataStoreAnalysisRequestA);
// }