import { combineReducers } from "redux";
import sample from "./sample/reducer/";
import administration from "./administration/reducer/";
import vendor from "./vendor/reducer/";
import auth from "./login/reducer";
import home from "./home/reducer";
import purchaseIndent from "./purchaseIndent/reducer";
import analytics from "./analytics/reducer";
import replenishment from "./replenishment/reducer";
import inventoryManagement from "./inventory management/reducer";
import dataSync from"./dataSync/reducer";
import demandPlanning from "./demandPlanning/reducer";
import orders from "./vendorPortal/reducer";
import shipment from "./vendorShipment/reducer";
import logistic from "./vendorLogistic/reducer";
import changeSetting from "./changeSetting/reducer";
import vendorProductTypes from './vendorCatalogue/reducer';
import seasonPlanning from './planningSettings/reducer';
import invoiceManagement from './invoiceManagement/reducer';
//import transporters from './transporters/reducer'
import excelUpload from './excelUpload/reducer';

export const makeRootReducer = asyncReducers => {
  return combineReducers({
    sample: sample,
    administration: administration,
    auth: auth,
    vendor: vendor,
    home:home,
    purchaseIndent:purchaseIndent,
    replenishment: replenishment,
    analytics:analytics,
    inventoryManagement: inventoryManagement,
    demandPlanning: demandPlanning,
    changeSetting: changeSetting,
    dataSync: dataSync,
    orders: orders,
    shipment : shipment,
    logistic : logistic,
    vendorProductTypes : vendorProductTypes,
    seasonPlanning: seasonPlanning,
    invoiceManagement: invoiceManagement,
    //transporters: transporters,
    excelUpload : excelUpload,
    ...asyncReducers
  });
};

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return;

  store.asyncReducers[key] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
