import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import moment from 'moment';
import encryption from '../../../encryption';
import { OganisationIdName } from '../../../organisationIdName.js';



export function* divisionSectionDepartmentRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.divisionSectionDepartmentClear());
  }
  else {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type == "" ? 1 : action.payload.type;
    let hl1name = action.payload.hl1name == undefined ? "" : action.payload.hl1name;
    let hl2name = action.payload.hl2name == undefined ? "" : action.payload.hl2name;
    let hl3name = action.payload.hl3name == undefined ? "" : action.payload.hl3name;
    let search = action.payload.search == undefined ? "" : action.payload.search;

    var { orgIdGlobal } = OganisationIdName()
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/hierarachylevels/`, {
        pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        type: action.payload.type == "" ? 1 : action.payload.type,
        hl1name: action.payload.hl1name == undefined ? "" : action.payload.hl1name,
        hl2name: action.payload.hl2name == undefined ? "" : action.payload.hl2name,
        hl3name: action.payload.hl3name == undefined ? "" : action.payload.hl3name,
        search: action.payload.search == undefined ? "" : action.payload.search,
        orgId: orgIdGlobal,
        searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy,

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.divisionSectionDepartmentSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.divisionSectionDepartmentError(response.data));
      }

    } catch (e) {
      yield put(actions.divisionSectionDepartmentError("error occurs"));
      console.warn('Some error found in "piRequest" action\n', e);
    }
  }
}

export function* transporterRequest(action) {
  let id = action.payload;
  try {

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.PI}/get/transporter/supplier/${id}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.transporterSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.transporterError(response.data));
    }

  } catch (e) {
    yield put(actions.transporterError("error occurs"));
    console.warn('Some error found in "transporter" action\n', e);
  }
}

export function* getAgentNameRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/agent`,{
      type: action.payload.type,
      pageNo: action.payload.pageNo,
      search: action.payload.search,
      cityName: action.payload.cityName
    })
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getAgentNameSuccess(response.data.data))
    } else if (finalResponse.failure) {
      yield put(actions.getAgentNameError(response.data))
    }
  } catch(e) {
    yield put(action.getAgentNameError('error occurs'))
    console.warn('Some error found in Agent name action')
  }
}

export function* supplierRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.supplierClear());
  }
  else {

    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/supplierdata/`, {
        pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        type: action.payload.type == "" ? 1 : action.payload.type,
        name: action.payload.name == undefined ? "" : action.payload.name,
        code: action.payload.slCode == undefined ? "" : action.payload.slCode,
        address: action.payload.address == undefined ? "" : action.payload.address,
        search: action.payload.search == undefined ? "" : action.payload.search,
        departmentName: action.payload.department == undefined ? "" : action.payload.department,
        departmentCode: action.payload.departmentCode == undefined ? "" : action.payload.departmentCode,
        siteCode: action.payload.siteCode == undefined ? "" : action.payload.siteCode,
        cityName: action.payload.city == undefined ? "" : action.payload.city,
        searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy

      });

      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.supplierSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.supplierError(response.data));
      }

    } catch (e) {
      yield put(actions.supplierError("error occurs"));
      console.warn('Some error found in "supplier" action\n', e);
    }
  }
}

export function* purchaseTermRequest(action) {
  let code = action.payload
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.PI}/get/purtermdata/${code}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.purchaseTermSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.purchaseTermError(response.data));
    }

  } catch (e) {
    yield put(actions.purchaseTermError("error occurs"));
    console.warn('Some error found in "purchaseTerm" action\n', e);
  }
}



export function* leadTimeRequest(action) {


  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/leadtime/supplier/`, {
      slCode: action.payload,
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.leadTimeSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.leadTimeError(response.data));
    }

  } catch (e) {
    yield put(actions.leadTimeError("error occurs"));
    console.warn('Some error found in "leadTime" action\n', e);
  }
}


export function* quantityRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.quantityClear());
  }
  else {
    let rId = action.payload.id;
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/calculate/qty/article/data`, {

        'ratio': action.payload.ratioo,
        'numOfColors': action.payload.numOfColor,
        'numOfSets': action.payload.set,

      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        response.data.data.rowId = rId;
        yield put(actions.quantitySuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.quantityError(response.data));
      }

    } catch (e) {
      yield put(actions.quantityError("error occurs"));
      console.warn('Some error found in "quantity" action\n', e);
    }
  }
}


export function* totalRequest(action) {


  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/total/article/data`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.totalSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.totalError(response.data));
    }

  } catch (e) {
    yield put(actions.totalError("error occurs"));
    console.warn('Some error found in "total " action\n', e);
  }
}

export function* imageRequest(action) {

  try {

    let rId = action.payload.id;
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/file/upload`, {

      'file': action.payload.file,
      'fileName': action.payload.fileName,
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      response.data.data.rowId = rId;
      yield put(actions.imageSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.imageError(response.data));
    }

  } catch (e) {
    yield put(actions.imageError("error occurs"));
    console.warn('Some error found in "image " action\n', e);
  }
}
export function* otbRequest(action) {
  let code = action.payload.articleCode;
  let rId = action.payload.rowId;
  let mrp = action.payload.mrp
  let month = action.payload.month != undefined && action.payload.month != '' ? action.payload.month.toUpperCase() : action.payload.month;
  let year = action.payload.year
  var { orgIdGlobal } = OganisationIdName()
  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.PI}/calculate/otb/articlecode/${code}?mrp=${mrp}&month=${month}&year=${year}&orgId=${orgIdGlobal}`, {

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      response.data.data.rowId = rId;
      yield put(actions.otbSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.otbError(response.data));
    }

  } catch (e) {
    yield put(actions.otbError("error occurs"));
    console.warn('Some error found in "otb " action\n', e);
  }
}

// procurment analytics otb report
export function* analyticsOtbRequest(action) {
  
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/otb/reports`, {
      data: action.payload.data,
      monthYearList: action.payload.monthYearList,
      isExport: action.payload.isExport,
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.analyticsOtbSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.analyticsOtbError(response.data));
    }

  } catch (e) {
    yield put(actions.analyticsOtbError("error occurs"));
    console.warn('Some error found in "otb report" action\n', e);
  }
}


export function* vendorMrpRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.vendorMrpClear());
  }
  else {


    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/all/pricepoint/`, {
        slCode: action.payload.code,
        pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        type: action.payload.type == "" ? 1 : action.payload.type,
        // let articleCode = action.payload.articleCode  == undefined ? "" :action.payload.articleCode ;
        articleName: action.payload.articleName == undefined ? "" : action.payload.articleName,
        costPrice: action.payload.mrp == undefined ? "" : action.payload.mrp,
        sellPrice: action.payload.rsp == undefined ? "" : action.payload.rsp,
        search: action.payload.search == undefined ? "" : action.payload.search,
        department: action.payload.department == undefined ? "" : action.payload.department,
        searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy


      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.vendorMrpSuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.vendorMrpError(response.data));
      }

    } catch (e) {
      yield put(actions.vendorMrpError("error occurs"));
      console.warn('Some error found in "leadTime" action\n', e);
    }
  }
}

export function* lineItemRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.lineItemClear());
  }
  else {
    let code = action.payload.articleCode;
    let rId = action.payload.rowId;
    let mrp = action.payload.mrp;
    let designRow = action.payload.designRow
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/lineitem/charges/data`, {
        hsnSacCode: action.payload.hsnSacCode,
        purtermMainCode: Number(action.payload.purtermMainCode),
        qty: action.payload.qty,
        rate: Number(action.payload.rate),
        basic: action.payload.qty * Number(action.payload.rate),
        clientGstIn: sessionStorage.getItem('gstin'),
        piDate: moment(action.payload.piDate).format("DD-MMM-YYYY HH:mm:ss"),
        supplierGstinStateCode: action.payload.supplierGstinStateCode,
        siteCode: action.payload.siteCode


      });
      const finalResponse = script(response);
      console.log(finalResponse)
      if (finalResponse.success) {
        response.data.data.rowId = rId;
        response.data.data.rowId = rId;
        response.data.data.articleCode = code
        response.data.data.mrp = mrp
        response.data.data.designRow = designRow
        yield put(actions.lineItemSuccess(response.data.data));
      } else if (finalResponse.failure) {
        response.data.rowId = rId;
        response.data.articleCode = code
        response.data.mrp = mrp
        response.data.designRow = designRow

        yield put(actions.lineItemError(response.data));
      }

    } catch (e) {
      yield put(actions.lineItemError("error occurs"));
      console.warn('Some error found in "lineItem" action\n', e);
    }
  }
}


export function* markUpRequest(action) {
  console.log(action)
  if (action.payload == undefined) {
    yield put(actions.markUpClear());
  }
  else {
    let code = action.payload.articleCode;
    let rId = action.payload.rowId;
    let designRow = action.payload.designRow

    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/calculate/actual/markup/article/{data}`, {
        // qty:action.payload.qty,
        rate: Number(action.payload.rate),

        rsp: Number(action.payload.rsp),
        piDate: action.payload.piDate,
        hsnSacCode: action.payload.hsnSacCode


      });
      const finalResponse = script(response);
      console.log(finalResponse)
      if (finalResponse.success) {
        response.data.data.rowId = rId;
        response.data.data.articleCode = code
        response.data.data.designRow = designRow
        yield put(actions.markUpSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.markUpError(response.data));
      }

    } catch (e) {
      yield put(actions.markUpError("error occurs"));
      console.warn('Some error found in "markup" action\n', e);
    }
  }
}



export function* sizeRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.sizeClear());
  }
  else {
    let rId = action.payload.id;


    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/get/itemcatdesc`,
        {
          "type": action.payload.type == "" ? 1 : action.payload.type,
          "pageNo": action.payload.no == undefined ? 1 : action.payload.no,
          "search": action.payload.search != undefined ? action.payload.search : "",
          "hl3Code": action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
          "hl3Name": action.payload.hl3Name == undefined ? "" : action.payload.hl3Name,
          "catDescType": "size",
          "hl1Name": action.payload.hl1Name == undefined ? "" : action.payload.hl1Name,
          "hl2Name": action.payload.hl2Name == undefined ? "" : action.payload.hl2Name,
          "hl4Name": action.payload.hl4Name == undefined ? "" : action.payload.hl4Name,
          "searchBy": action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy,
          "isToggled": action.payload.isToggled,
          "ispo": window.location.hash == "#/purchase/purchaseIndents" ? false : true,
          "itemUdfArray": action.payload.itemUdfObj != undefined ? action.payload.itemUdfObj : {}
        });
      const finalResponse = script(response);

      if (finalResponse.success) {
        // if(response.data.data.resource!=null){
        response.data.data.rowId = rId;
        // response.data.data.hl3Name = department;
        // }else{
        // response.data.data = {rowId : rId}
        // }

        yield put(actions.sizeSuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.sizeError(response.data));
      }

    } catch (e) {
      yield put(actions.sizeError("error occurs"));
      console.warn('Some error found in "size request" action\n', e);
    }
  }
}

export function* colorRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.colorClear());
  }
  else {
    let rId = action.payload.id;

    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/get/itemcatdesc`,
        {
          "type": action.payload.type == "" ? 1 : action.payload.type,
          "pageNo": action.payload.no == undefined ? 1 : action.payload.no,
          "search": action.payload.search,
          "hl3Code": action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
          "hl3Name": action.payload.hl3Name == undefined ? "" : action.payload.hl3Name,
          "catDescType": "color",
          "hl1Name": action.payload.hl1Name == undefined ? "" : action.payload.hl1Name,
          "hl2Name": action.payload.hl2Name == undefined ? "" : action.payload.hl2Name,
          "hl4Name": action.payload.hl4Name == undefined ? "" : action.payload.hl4Name,
          "searchBy": action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy,
          "isToggled": action.payload.isToggled ? action.payload.isToggled : "",
          "ispo": window.location.hash == "#/purchase/purchaseIndents" ? false : true
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.rowId = rId;
        // response.data.data.articleCode= code
        yield put(actions.colorSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.colorError(response.data));
      }

    } catch (e) {
      yield put(actions.colorError("error occurs"));
      console.warn('Some error found in color request" action\n', e);
    }
  }
}



// PIHISTORY

export function* piHistoryRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.piHistoryClear());
  } else {
    try {
      var filter = action.payload.filter == undefined ? {} : action.payload.filter;
      let filterItems = action.payload.filterItems == undefined ? {} : action.payload.filterItems;

      // const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/history`, {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/history`, {
        pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        type: action.payload.type == undefined ? 1 : action.payload.type,
        search: action.payload.search == undefined ? "" : action.payload.search,
        sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
        sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
        status: action.payload.status,
        filter: { ...filter },
        filterItems: { ...filterItems }
        //  supplierName : action.payload.supplierName == undefined ? "" : action.payload.supplierName,
        //  articleCode : action.payload.articleCode == undefined ? "" : action.payload.articleCode,
        //  pattern : action.payload.indentId == undefined ? "" : action.payload.indentId,
        //  division :action.payload.division == undefined ? "" : action.payload.division,
        //  generatedDate : action.payload.generatedDate == undefined || action.payload.generatedDate == "" ? "" : moment(action.payload.generatedDate).format("YYYY-MM-DD"),
        //  status : action.payload.status == undefined ? "" : action.payload.status,
        //  department :action.payload.department == undefined ? "" : action.payload.department,
        //  section : action.payload.section == undefined ? "" : action.payload.section,
        //  slCityName : action.payload.slCityName,
        //  deliveryDateFrom :action.payload.deliveryDateFrom == undefined || action.payload.deliveryDateFrom == "" ? "" : moment(action.payload.deliveryDateFrom).format("DD-MMM-YYYY"),
        //  deliveryDateTo : action.payload.deliveryDateTo == undefined || action.payload.deliveryDateTo == "" ? "" : moment(action.payload.deliveryDateTo).format("DD-MMM-YYYY"),
        //  desc2Code : action.payload.desc2Code,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.piHistorySuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.piHistoryError(response.data));
      }

    } catch (e) {
      yield put(actions.piHistoryError("error occurs"));
      console.warn('Some error found in "piHistory" action\n', e);
    }
  }
}


export function* piCreateRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.piCreateClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/create`, {
        piKey: {

          hl1Name: action.payload.hl1Name,
          hl1Code: action.payload.hl1Code,
          hl2Name: action.payload.hl2Name,
          hl2Code: action.payload.hl2Code,
          hl3Name: action.payload.hl3Name,
          hl3Code: action.payload.hl3Code,
          slCode: action.payload.slCode,
          slName: action.payload.slName,
          slCityName: action.payload.slCityName,
          slAddr: action.payload.slAddr,
          leadTime: action.payload.leadTime,
          termCode: action.payload.termCode,
          termName: action.payload.termName,
          transporterCode: action.payload.transporterCode,
          transporterName: action.payload.transporterName,
          poQuantity: action.payload.poQuantity,
          poAmount: action.payload.poAmount,
          hsnSacCode: action.payload.hsnSacCode,
          stateCode: action.payload.stateCode,
          isUDFExist: action.payload.isUDFExist,


          piDetails: action.payload.piDetails,
          siteCode: action.payload.siteCode,
          siteName: action.payload.siteName,
          isSiteExist: action.payload.isSiteExist,
          itemUdfExist: action.payload.itemUdfExist
        }
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.piCreateSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.piCreateError(response.data));
      }
    } catch (e) {
      yield put(actions.piCreateError("error occurs"));
      console.warn('Some error found in "piCreateRequest" action\n', e);
    }
  }
}





export function* loadIndentRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.loadIndentClear());
  }
  else {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type == undefined || action.payload.type == "" ? 1 : action.payload.type;
    let search = action.payload.search == undefined ? "" : action.payload.search;
    let orderId = action.payload.orderId;
    let supplier = action.payload.supplier;
    let cityName = action.payload.cityName;
    let piFromDate = action.payload.piFromDate != "" ? moment(action.payload.piFromDate).format("DD-MMM-YYYY") : "";
    let piToDate = action.payload.piToDate != "" ? moment(action.payload.piToDate).format("DD-MMM-YYYY") : "";
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PO}/get/load/indent`, {

        pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        type: action.payload.type == undefined || action.payload.type == "" ? 1 : action.payload.type,
        search: action.payload.search == undefined ? "" : action.payload.search,
        orderId: action.payload.orderId,
        supplier: action.payload.supplier,
        cityName: action.payload.cityName,
        piFromDate: action.payload.piFromDate != "" ? moment(action.payload.piFromDate).format("DD-MMM-YYYY") : "",
        piToDate: action.payload.piToDate != "" ? moment(action.payload.piToDate).format("DD-MMM-YYYY") : "",
        searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy

      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.loadIndentSuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.loadIndentError(response.data));
      }

    } catch (e) {
      yield put(actions.loadIndentError("error occurs"));
      console.warn('Some error found in "loadIndent request" action\n', e);
    }
  }
}

export function* articlePoRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.articlePoClear());
  }
  else {

    var { orgIdGlobal } = OganisationIdName()
    // let encodeUrl = encryption("type="+type+"&pageNo="+no+"&hl4Code="+articleCode+"&hl4Name="+articleName+"&hl1Name="+division+"&hl2Name="+section+"&hl3Name="+department+"&search="+search)
    //  console.log(encodeUrl)
    // console.log(code.decryptMessage(encodeUrl,'abc'))
    //  let u = type=${type}&pageNo=${no}&hl4Code=${articleCode}&hl4Name=${articleName}&hl1Name=${division}&hl2Name=${section}&hl3Name=${department}&search=${search}
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PO}/get/article/hierarchy/data`,
        {
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          type: action.payload.type == "" ? 1 : action.payload.type,
          hl4Code: action.payload.articleCode == undefined ? "" : action.payload.articleCode,
          hl4Name: action.payload.articleName == undefined ? "" : action.payload.articleName,
          hl1Name: action.payload.division == undefined ? "" : action.payload.division,
          hl2Name: action.payload.section == undefined ? "" : action.payload.section,
          hl3Name: action.payload.department == undefined ? "" : action.payload.department,
          search: action.payload.search == undefined ? "" : action.payload.search,
          orgId: orgIdGlobal,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.articlePoSuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.articlePoError(response.data));
      }

    } catch (e) {
      yield put(actions.articlePoError("error occurs"));
      console.warn('Some error found in "articlePo request" action\n', e);
    }
  }
}



export function* piUpdateRequest(action) {
  console.log(action)

  if (action.payload == undefined) {
    yield put(actions.piUpdateClear());
  }
  else {
    try {


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/update/status`,
        {
          status: action.payload.payloadForStatus.status,
          patterns: action.payload.payloadForStatus.patterns,
          remark: action.payload.payloadForStatus.remark != undefined ? action.payload.payloadForStatus.remark : '',
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.piUpdateSuccess(response.data.data));
        yield put(actions.piHistoryRequest(action.payload.payloadForHistory));
      } else if (finalResponse.failure) {

        yield put(actions.piUpdateError(response.data));
      }

    } catch (e) {
      yield put(actions.piUpdateError("error occurs"));
      console.warn('Some error found in "piUpdate request" action\n', e);
    }
  }
}



export function* poItemcodeRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.poItemcodeClear());
  }
  else {




    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PO}/get/desc6`, {

        pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        type: action.payload.type == "" ? 1 : action.payload.type,
        articleCode: action.payload.code == undefined ? "" : action.payload.code,
        search: action.payload.search == undefined ? "" : action.payload.search,
        mrpRangeFrom: action.payload.mrpRangeFrom == undefined ? "" : action.payload.mrpRangeFrom,
        mrpRangeTo: action.payload.mrpRangeTo == undefined ? "" : action.payload.mrpRangeTo,
        searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.poItemcodeSuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.poItemcodeError(response.data));
      }

    } catch (e) {
      yield put(actions.poItemcodeError("error occurs"));
      console.warn('Some error found in "poItemcode request" action\n', e);
    }
  }
}


export function* getTransporterRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getTransporterClear());
  }
  else {

    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/transporter`, {
        pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        type: action.payload.type == "" ? 1 : action.payload.type,
        transporterCode: action.payload.transporterCode == "" ? "" : action.payload.transporterCode,
        search: action.payload.search == undefined ? "" : action.payload.search,
        transporterName: action.payload.transporterName == "" ? "" : action.payload.transporterName,
        cityName: action.payload.city == undefined ? "" : action.payload.city,
        searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getTransporterSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getTransporterError(response.data));
      }

    } catch (e) {
      yield put(actions.getTransporterError("error occurs"));
      console.warn('Some error found in "getTransporter " action\n', e);
    }
  }
}
export function* getPurchaseTermRequest(action) {


  try {
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.PI}/get/all/purchaseterms`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getPurchaseTermSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getPurchaseTermError(response.data));
    }

  } catch (e) {
    yield put(actions.getPurchaseTermError("error occurs"));
    console.warn('Some error found in "getPurchaseTerm " action\n', e);
  }
}



export function* getPoDataRequest(action) {


  if (action.payload == undefined) {
    yield put(actions.getPoDataClear());
  }
  else {
    try {

      let orderId = action.payload.orderId;
      let orderDetailId = action.payload.orderDetailId;
      var { orgIdGlobal } = OganisationIdName()

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/get?orderId=${orderId}&orderDetailId=${orderDetailId}&orgId=${orgIdGlobal}`,
        {
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getPoDataSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.getPoDataError(response.data));
      }

    } catch (e) {
      yield put(actions.getPoDataError("error occurs"));
      console.warn('Some error found in "getPoData request" action\n', e);
    }
  }
}


export function* udfTypeRequest(action) {


  if (action.payload == undefined) {
    yield put(actions.udfTypeClear());
  }
  else {
    try {



      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/all/setudfmapping`,
        {
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          type: action.payload.type == "" ? 1 : action.payload.type,
          udfType: action.payload.udfType,
          code: action.payload.code,
          name: action.payload.name,
          search: action.payload.search == undefined ? "" : action.payload.search,
          ispo: action.payload.ispo == undefined ? "" : action.payload.ispo,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.udfTypeSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.udfTypeError(response.data));
      }

    } catch (e) {
      yield put(actions.udfTypeError("error occurs"));
      console.warn('Some error found in "udfType request" action\n', e);
    }
  }
}


//getUdf mapping

export function* getUdfMappingRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.getUdfMappingClear());
  }
  else {
    try {



      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/all/setudfsetting`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          udfType: action.payload.udfType,
          isCompulsary: action.payload.isCompulsary,
          displayName: action.payload.displayName,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy,
          search: action.payload.search == undefined ? "" : action.payload.search,
          ispo: action.payload.ispo,
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getUdfMappingSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.getUdfMappingError(response.data));
      }

    } catch (e) {
      yield put(actions.getUdfMappingError("error occurs"));
      console.warn('Some error found in "getUdfMapping request" action\n', e);
    }
  }
}

// Mapping Excel upload
export function* mappingExcelUploadRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.mappingExcelUploadClear());
  }
  else {
    try {



      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/upload/mapping/data`, action.payload
        // {
        //   type: action.payload.type,
        //   fileData: action.payload.fileData
        // }
        );
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.mappingExcelUploadSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.mappingExcelUploadError(response.data));
      }

    } catch (e) {
      yield put(actions.mappingExcelUploadError("error occurs"));
      console.warn('Some error found in "mappingExcelUploadRequest request" action\n', e);
    }
  }
}

// mapping status

export function* mappingExcelStatusRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.mappingExcelStatusClear());
  }
  else {
    try {
      let template = action.payload.template;
      let type = action.payload.type;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/upload/mapping/status?isDownloadTemplate=${template}&type=${type}`);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.mappingExcelStatusSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.mappingExcelStatusError(response.data));
      }

    } catch (e) {
      yield put(actions.mappingExcelStatusError("error occurs"));
      console.warn('Some error found in "mappingExcelStatusRequest request" action\n', e);
    }
  }
}

export function* mappingExcelExportRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.mappingExcelExportClear());
  }
  else {
    try {
      let catDescUdfType = action.payload.catDescUdfType;
      let hl1Code = action.payload.hl1Code;
      let hl2Code = action.payload.hl2Code;
      let hl3Code = action.payload.hl3Code;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/export/mappings/data?catDescUdfType=${catDescUdfType}&hl1Code=${hl1Code}&hl2Code=${hl2Code}&hl3Code=${hl3Code}`);
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.mappingExcelExportSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.mappingExcelExportError(response.data));
      }

    } catch (e) {
      yield put(actions.mappingExcelStatusError("error occurs"));
      console.warn('Some error found in "mappingExcelExportRequest request" action\n', e);
    }
  }
}

//pohistory

export function* poHistoryRequest(action) {


  if (action.payload == undefined) {
    yield put(actions.poHistoryClear());
  }
  else {
    try {
      let type = action.payload.type == "" ? 1 : action.payload.type;
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let orderNo = action.payload.orderNo;
      let orderDate = action.payload.orderDate == undefined || action.payload.orderDate == "" ? "" : moment(action.payload.orderDate).format("DD-MMM-YYYY");
      let validFrom = action.payload.validFrom == undefined || action.payload.validFrom == "" ? "" : moment(action.payload.validFrom).format("DD-MMM-YYYY");
      let validTo = action.payload.validTo == undefined || action.payload.validTo == "" ? "" : moment(action.payload.validTo).format("DD-MMM-YYYY");;
      let slCode = action.payload.slCode;
      let slCityName = action.payload.slCityName == undefined ? "" : action.payload.slCityName;
      let slName = action.payload.slName == undefined ? "" : action.payload.slName;
      let search = action.payload.search == undefined ? "" : action.payload.search;
      let hl1Name = action.payload.hl1Name == undefined ? "" : action.payload.hl1Name;
      let hl2Name = action.payload.hl2Name == undefined ? "" : action.payload.hl2Name;
      let hl3Name = action.payload.hl3Name == undefined ? "" : action.payload.hl3Name;
      var filter = action.payload.filter == undefined ? {} : action.payload.filter;
      let filterItems = action.payload.filterItems == undefined ? {} : action.payload.filterItems;
      // const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/history?type=${type}&pageNo=${no}&orderNo=${orderNo}&orderDate=${orderDate}&validFrom=${validFrom}&validTo=${validTo}&slCode=${slCode}&slName=${slName}&slCityName=${slCityName}&hl1Name=${hl1Name}&hl2Name=${hl2Name}&hl3Name=${hl3Name}&search=${search}`,
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/history`,

        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search == undefined ? "" : action.payload.search,
          sortedBy: action.payload.sortedBy == undefined ? "" : action.payload.sortedBy,
          sortedIn: action.payload.sortedIn == undefined ? "" : action.payload.sortedIn,
          filter: { ...filter },
          filterItems: { ...filterItems }
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.poHistorySuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.poHistoryError(response.data));
      }

    } catch (e) {
      yield put(actions.poHistoryError("error occurs"));
      console.warn('Some error found in "poHistory request" action\n', e);
    }
  }
}

//poCreate

export function* poCreateRequest(action) {


  if (action.payload == undefined) {
    yield put(actions.poCreateClear());
  }
  else {
    try {




      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/create`,
        {
          prevOrderNo: action.payload.prevOrderNo,
          poType: action.payload.poType,
          orderDate: moment(action.payload.orderDate).format(),
          validFrom: moment(action.payload.validFrom).format(),
          validTo: moment(action.payload.validTo).format(),
          hl1Name: action.payload.hl1Name,
          hl1Code: action.payload.hl1Code,
          hl2Name: action.payload.hl2Name,
          hl2Code: action.payload.hl2Code,
          hl3Name: action.payload.hl3Name,
          hl3Code: action.payload.hl3Code,
          hl4Name: action.payload.hl4Name,
          hl4Code: action.payload.hl4Code,
          slCode: action.payload.slCode,
          slName: action.payload.slName,
          slCityName: action.payload.slCityName,
          slAddr: action.payload.slAddr,
          leadTime: action.payload.leadTime,
          termCode: action.payload.termCode,
          termName: action.payload.termName,
          transporterCode: action.payload.transporterCode,
          transporterName: action.payload.transporterName,
          mrpStart: action.payload.mrpStart,
          mrpEnd: action.payload.mrpEnd,
          poQuantity: action.payload.poQuantity,
          poAmount: action.payload.poAmount,
          stateCode: action.payload.stateCode,
          pol: action.payload.pol,
          hsnSacCode: action.payload.hsnSacCode,
          hsnCode: action.payload.hsnCode,
          isHoldPO: action.payload.isHoldPO,
          isUDFExist: action.payload.isUDFExist,
          siteCode: action.payload.siteCode,
          siteName: action.payload.siteName,
          isSiteExist: action.payload.isSiteExist,
          itemUdfExist: action.payload.itemUdfExist


        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.poCreateSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.poCreateError(response.data));
      }

    } catch (e) {
      yield put(actions.poCreateError("error occurs"));
      console.warn('Some error found in "poCreate request" action\n', e);
    }
  }
}





export function* poSizeRequest(action) {


  if (action.payload == undefined) {


    yield put(actions.poSizeClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/all/sizemapping`,
        {

          'deptName': action.payload.deptName,
          'deptCode': action.payload.deptCode,
          'hl1Name': action.payload.hl1Name == undefined ? "" : action.payload.hl1Name,
          'hl2Name': action.payload.hl2Name == undefined ? "" : action.payload.hl2Name,
          'hl4Name': action.payload.hl4Name == undefined ? "" : action.payload.hl4Name


        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.poSizeSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.poSizeError(response.data));
      }

    } catch (e) {
      yield put(actions.poSizeError("error occurs"));
      console.warn('Some error found in "poSize request" action\n', e);
    }
  }
}


export function* createUdfSettingRequest(action) {


  if (action.payload == undefined) {
    yield put(actions.createUdfSettingClear());
  }
  else {
    try {
      let udfdata = {
        type: 1,
        udfType: "",
        isCompulsary: "",
        displayName: "",
        search: "",
      }
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/update/setudfsetting`,
        {

          "udfKey": action.payload
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.createUdfSettingSuccess(response.data.data));
        yield put(actions.getUdfMappingRequest(udfdata));
      } else if (finalResponse.failure) {

        yield put(actions.createUdfSettingError(response.data));
      }

    } catch (e) {
      yield put(actions.createUdfSettingError("error occurs"));
      console.warn('Some error found in "createUdfSetting request" action\n', e);
    }
  }
}


//active UDF
export function* activeUdfRequest(action) {

  try {

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/get/active/itemudfmapping`,
      {

      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.activeUdfSuccess(response.data.data));

    } else if (finalResponse.failure) {

      yield put(actions.activeUdfError(response.data));
    }

  } catch (e) {
    yield put(actions.activeUdfError("error occurs"));
    console.warn('Some error found in "activeUdf request" action\n', e);
  }
}


export function* activeCatDescRequest(action) {


  try {

    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/get/active/itemcatdescmapping`,
      {

      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.activeCatDescSuccess(response.data.data));

    } else if (finalResponse.failure) {

      yield put(actions.activeCatDescError(response.data));
    }

  } catch (e) {
    yield put(actions.activeCatDescError("error occurs"));
    console.warn('Some error found in "activeCatDesc request" action\n', e);
  }
}




export function* getCatDescUdfRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.getCatDescUdfClear());
  }
  else {
    try {
      // let department = action.payload.department

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/itemudfsetting`,
        {
          hl3Code: action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
          ispo: action.payload.ispo == undefined ? "false" : action.payload.ispo,
          hl3Name: action.payload.hl3Name == undefined ? "" : action.payload.hl3Name

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getCatDescUdfSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.getCatDescUdfError(response.data));
      }

    } catch (e) {
      yield put(actions.getCatDescUdfError("error occurs"));
      console.warn('Some error found in "getCatDescUdf request" action\n', e);
    }
  }
}

// _____________________________proc mapping_________________________
export function* catDescDropdownRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.catDescDropdownClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/get/active/mapping?hl3Code=${action.payload.hl3Code}`,{ });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.catDescDropdownSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.catDescDropdownError(response.data));
      }

    } catch (e) {
      yield put(actions.catDescDropdownError("error occurs"));
      console.warn('Some error found in "catDescDropdown request" action\n', e);
    }
  }
}


// _____________________________ITEM UDF______________

export function* itemCatDescUdfRequest(action) {
  console.log(action.payload)


  if (action.payload == undefined) {
    yield put(actions.itemCatDescUdfClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/itemudfmapping`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          description: action.payload.description,
          ispo: action.payload.ispo == undefined ? "" : action.payload.ispo,
          search: action.payload.search,
          cat_desc_udf: action.payload.itemUdfType,
          hl3code: action.payload.hl3Code,
          hl4code: action.payload.hl4Code,
          hl4Name: action.payload.hl4Name,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.itemCatDescUdfSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.itemCatDescUdfError(response.data));
      }

    } catch (e) {
      yield put(actions.itemCatDescUdfError("error occurs"));
      console.warn('Some error found in "itemCatDescUdf request" action\n', e);
    }
  }
}


export function* itemUdfMappingRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.itemUdfMappingClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/itemudfmapping`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          description: action.payload.description,
          ispo: action.payload.ispo == undefined ? "" : action.payload.ispo,
          search: action.payload.search,
          cat_desc_udf: action.payload.itemUdfType,
          hl3code: action.payload.hl3Code,
          hl4code: action.payload.hl4Code,
          hl4Name: action.payload.hl4Name,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.itemUdfMappingSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.itemUdfMappingError(response.data));
      }

    } catch (e) {
      yield put(actions.itemUdfMappingError("error occurs"));
      console.warn('Some error found in "itemUdfMapping request" action\n', e);
    }
  }
}



export function* createItemUdfRequest(action) {
  let payload = {
    hl3Code: action.payload.hl3Code
  }
  if (action.payload == undefined) {
    yield put(actions.createItemUdfClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/update/itemudfsetting`,
        {
          "catDescKey": action.payload

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.createItemUdfSuccess(response.data.data));
        if(window.location.hash != '#/purchase/catDescUdfSetting'){
          yield put(actions.getCatDescUdfRequest(payload));
        }
      } else if (finalResponse.failure) {

        yield put(actions.createItemUdfError(response.data));
      }

    } catch (e) {
      yield put(actions.createItemUdfError("error occurs"));
      console.warn('Some error found in "createItemUdf request" action\n', e);
    }
  }
}


// _______________________________UDF MAPPIING API___________________________________


export function* poUdfMappingRequest(action) {



  if (action.payload == undefined) {
    yield put(actions.poUdfMappingClear());
  }
  else {
    try {



      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/setudfmapping`,
        {
          pageNo: action.payload.no,
          type: action.payload.type == "" ? 1 : action.payload.type,
          search: action.payload.search,
          udfType: action.payload.udfType,
          code: action.payload.code,
          name: action.payload.name,
          ext: action.payload.ext,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.poUdfMappingSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.poUdfMappingError(response.data));
      }

    } catch (e) {
      yield put(actions.poUdfMappingError("error occurs"));
      console.warn('Some error found in "poUdfMapping request" action\n', e);
    }
  }
}


export function* updateUdfMappingRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.updateUdfMappingClear());
  }
  else {
    try {

      let udfMap = {
        no: 1,
        type: 1,
        search: "",
        udfType: action.payload.udfMappingData[0].udfType,
        code: "",
        name: "",
        ext: "",
      }


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/insertupdate/setudfmapping`,
        {
          "isInsert": action.payload.isInsert,
          "resultUDFType": action.payload.udfMappingData,

        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.updateUdfMappingSuccess(response.data.data));
        yield put(actions.poUdfMappingRequest(udfMap));


      } else if (finalResponse.failure) {

        yield put(actions.updateUdfMappingError(response.data));
      }

    } catch (e) {
      yield put(actions.updateUdfMappingError("error occurs"));
      console.warn('Some error found in "updateUdfMapping request" action\n', e);
    }
  }
}





export function* getItemUdfRequest(action) {


  if (action.payload == undefined) {
    yield put(actions.getItemUdfClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/insertupdate/setudf`,
        {

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getItemUdfSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.getItemUdfError(response.data));
      }

    } catch (e) {
      yield put(actions.getItemUdfError("error occurs"));
      console.warn('Some error found in "getItemUdf request" action\n', e);
    }
  }
}

// ____________ITEM MAPPING UDF REDUX SAVE API__________________________

export function* updateItemUdfRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.updateItemUdfClear());
  }
  else {
    try {
      let payload = {
        type: 1,
        no: action.payload.no == undefined ? 1 : action.payload.no,
        description: "",
        ispo: action.payload.ispo == undefined ? "" : true,
        search: "",
        itemUdfType: action.payload.itemUdfMappingData[0].cat_desc_udf,
        hl3Code: action.payload.itemUdfMappingData[0].hl3code,
        hl4Code: action.payload.itemUdfMappingData[0].hl4code,
        hl4Namen: action.payload.itemUdfMappingData[0].hl4Name,
      }
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/insertupdate/itemudfmapping`,
        {
          "isInsert": action.payload.isInsert,
          "resultUDFType": action.payload.itemUdfMappingData,
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateItemUdfSuccess(response.data.data));
        yield put(actions.itemUdfMappingRequest(payload))
      } else if (finalResponse.failure) {

        yield put(actions.updateItemUdfError(response.data));
      }

    } catch (e) {
      yield put(actions.updateItemUdfError("error occurs"));
      console.warn('Some error found in "updateItemUdf request" action\n', e);
    }
  }
}
// udfNameudfName_________________________--ITEM MAPPING CAT/DESC REDUX API__________________________


export function* updateItemCatDescRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updateItemCatDescClear());
  }
  else {
    try {
      let payload = {
        type: 1,
        no: action.payload.no == undefined ? 1 : action.payload.no,
        description: "",
        ispo: action.payload.ispo == undefined ? "" : action.payload.ispo,
        search: "",
        itemUdfType: action.payload.itemUdfMappingData[0].cat_desc_udf,
        hl3Code: action.payload.itemUdfMappingData[0].hl3code,
        hl4Code: action.payload.itemUdfMappingData[0].hl4code,
        hl4Name: action.payload.itemUdfMappingData[0].hl4Name
      }
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/insertupdate/itemudfmapping`,
        {
          "isInsert": action.payload.isInsert,
          "resultUDFType": action.payload.itemUdfMappingData,
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateItemCatDescSuccess(response.data.data));
        yield put(actions.itemCatDescUdfRequest(payload))
      } else if (finalResponse.failure) {

        yield put(actions.updateItemCatDescError(response.data));
      }

    } catch (e) {
      yield put(actions.updateItemCatDescError("error occurs"));
      console.warn('Some error found in "updateItemCatDesc request" action\n', e);
    }
  }
}

// configuration

export function* updateItemUdfMappingRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.updateItemUdfMappingClear());
  }
  else {
    try {   
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/insertupdate/itemudfmapping`,
        {
          isInsert: action.payload.isInsert,
          hl1code: action.payload.hl1code,
          hl2code: action.payload.hl2code,
          hl3code: action.payload.hl3code,
          hl3Name: action.payload.hl3Name,
          hl4code: action.payload.hl4code,
          hl4Name: action.payload.hl4Name,
          cat_desc_udf: action.payload.cat_desc_udf,
          resultUDFType: action.payload.resultUDFType,
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateItemUdfMappingSuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.updateItemUdfMappingError(response.data));
      }

    } catch (e) {
      yield put(actions.updateItemUdfMappingError("error occurs"));
      console.warn('Some error found in "updateItemUdf request" action\n', e);
    }
  }
}


export function* updateSizeDeptRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.updateSizeDeptClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/udpate/sizemapping`,
        {
          "sizes": action.payload,
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateSizeDeptSuccess(response.data.data));
        let data = {
          deptName: response.data.data.resource.sizes[0].hl3Name,
          deptCode: response.data.data.resource.sizes[0].hl3Code
        }

        yield put(actions.poSizeRequest(data))
      } else if (finalResponse.failure) {
        yield put(actions.updateSizeDeptError(response.data));
      }
    } catch (e) {
      yield put(actions.updateSizeDeptError("error occurs"));
      console.warn('Some error found in "updateSizeDept request" action\n', e);
    }
  }
}




export function* cnameRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.cnameClear());
  }
  else {
    try {
      let no = action.payload.no;
      let type = action.payload.type == "" ? 1 : action.payload.type;
      let search = action.payload.search;
      let header = action.payload.udfType;

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/catdescudfname`,
        {
          pageNo: action.payload.no,
          type: action.payload.type == "" ? 1 : action.payload.type,
          search: action.payload.search,
          header: action.payload.udfType,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.cnameSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.cnameError(response.data));
      }
    } catch (e) {
      yield put(actions.cnameError("error occurs"));
      console.warn('Some error found in "cnameRequest request" action\n', e);
    }
  }
}

export function* articleNameRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.articleNameClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/article`,
        {
          pageNo: action.payload.no,
          type: action.payload.type == "" ? 1 : action.payload.type,
          search: action.payload.search,
          hl3Code: action.payload.hl3Code,
          catDescUDF: action.payload.catDescUDF,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.articleNameSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.articleNameError(response.data));
      }
    } catch (e) {
      yield put(actions.articleNameError("error occurs"));
      console.warn('Some error found in "articleNameRequest request" action\n', e);
    }
  }
}



export function* piAddNewRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.piAddNewClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/validation`,
        {
          isExist: action.payload.isExist == undefined ? "" : action.payload.isExist,
          hl3Code: action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
          code: action.payload.code == undefined ? "" : action.payload.code,
          categoryName: action.payload.categoryName == undefined ? "" : action.payload.categoryName,

          hl3name: action.payload.hl3Name == undefined ? "" : action.payload.hl3Name,
          hl1name: action.payload.hl1Name == undefined ? "" : action.payload.hl1Name,
          hl2name: action.payload.hl2Name == undefined ? "" : action.payload.hl2Name,

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.piAddNewSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.piAddNewError(response.data));
      }
    } catch (e) {
      yield put(actions.piAddNewError("error occurs"));
      console.warn('Some error found in "piAddNewRequest request" action\n', e);
    }
  }
}

// ______________________________MARGINRULE_______________________-



export function* marginRuleRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.marginRuleClear());
  }
  else {
    try {
      let rId = action.payload.rId

      let slCode = action.payload.slCode == undefined ? "" : action.payload.slCode;
      let articleCode = action.payload.articleCode == undefined ? "" : action.payload.articleCode
      let siteCode = action.payload.siteCode == undefined ? "" : action.payload.siteCode
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/pi/get/margin/rule/data/${slCode}?articleCode=${articleCode}&siteCode=${siteCode}`,
        {

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.rowId = rId;
        yield put(actions.marginRuleSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.marginRuleError(response.data));
      }
    } catch (e) {

      yield put(actions.marginRuleError("error occurs"));
      console.warn('Some error found in "marginRuleRequest request" action\n', e);
    }
  }
}



export function* getItemDetailsRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.getItemDetailsClear());
  }
  else {
    try {


      let hl3Code = action.payload.hl3Code == undefined ? "" : action.payload.hl3Code;
      let hl3Name = action.payload.hl3Name == undefined ? "" : action.payload.hl3Name
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/pi/get/catdesc/caption?hl3Code=${hl3Code}&hl3Name=${hl3Name}`,
        {
        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.getItemDetailsSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.getItemDetailsError(response.data));
      }
    } catch (e) {
      yield put(actions.getItemDetailsError("error occurs"));
      console.warn('Some error found in "getItemDetails request" action\n', e);
    }
  }
}


export function* getItemDetailsValueRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.getItemDetailsValueClear());
  }
  else {
    try {


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/get/itemcatdesc`,
        {
          "type": action.payload.type == "" ? 1 : action.payload.type,
          "pageNo": action.payload.no == undefined ? 1 : action.payload.no,
          "search": action.payload.search,
          "hl3Code": action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
          "hl3Name": action.payload.hl3Name == undefined ? "" : action.payload.hl3Name,
          "catDescType": action.payload.itemType,
          "hl1Name": action.payload.hl1Name == undefined ? "" : action.payload.hl1Name,
          "hl2Name": action.payload.hl2Name == undefined ? "" : action.payload.hl2Name,
          "hl4Name": action.payload.hl4Name == undefined ? "" : action.payload.hl4Name,
          "searchBy": action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy,
          "isToggled": action.payload.isToggled ? action.payload.isToggled : "",
          "ispo": window.location.hash == "#/purchase/purchaseIndents" ? false : true
        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.getItemDetailsValueSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.getItemDetailsValueError(response.data));
      }
    } catch (e) {

      yield put(actions.getItemDetailsValueError("error occurs"));
      console.warn('Some error found in "getItemDetailsValue request" action\n', e);
    }
  }



}


// _______-SET BASED, PO, AND SETS_______________________




export function* selectVendorRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.selectVendorClear());
  }
  else {
    try {


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/supplier`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search,
          hl3code: action.payload.hl3code == undefined ? "" : action.payload.hl3code,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy



        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.selectVendorSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.selectVendorError(response.data));
      }
    } catch (e) {
      yield put(actions.selectVendorError("error occurs"));
      console.warn('Some error found in "selectVendor request" action\n', e);
    }
  }
}


export function* selectOrderNumberRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.selectOrderNumberClear());
  }
  else {
    try {



      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/orderno`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search,
          hl3code: action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
          supplierCode: action.payload.supplierCode == undefined ? "" : action.payload.supplierCode,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy


        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.selectOrderNumberSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.selectOrderNumberError(response.data));
      }
    } catch (e) {
      yield put(actions.selectOrderNumberError("error occurs"));
      console.warn('Some error found in "selectOrderNumber request" action\n', e);
    }
  }
}




export function* setBasedRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.setBasedClear());
  }
  else {
    try {
      let orderNo = action.payload.orderNo
      let validTo = action.payload.validTo
      var { orgIdGlobal } = OganisationIdName()
      let poType = action.payload.poType

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/get/setbased?orderNo=${orderNo}&validTo=${validTo}&orgId=${orgIdGlobal}&poType=${poType}`,
        {

        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.setBasedSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.setBasedError(response.data));
      }
    } catch (e) {
      yield put(actions.setBasedError("error occurs"));
      console.warn('Some error found in "setBased request" action\n', e);
    }
  }
}

export function* departmentSetBasedRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.departmentSetBasedClear());
  }
  else {
    try {


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/department`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy

        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.departmentSetBasedSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.departmentSetBasedError(response.data));
      }
    } catch (e) {
      yield put(actions.departmentSetBasedError("error occurs"));
      console.warn('Some error found in "departmentSetBased request" action\n', e);
    }
  }
}

export function* hsnCodeRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.hsnCodeClear());
  }
  else {
    try {
      let rId = action.payload.rowId

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/get/hsncode`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search,
          departmentCode: action.payload.code,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.rowId = rId;
        yield put(actions.hsnCodeSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.hsnCodeError(response.data));
      }
    } catch (e) {
      yield put(actions.hsnCodeError("error occurs"));
      console.warn('Some error found in "hsnCode request" action\n', e);
    }
  }
}


export function* multipleLineItemRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.multipleLineItemClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/get/multiplelineitem/charges/data`,
        {
          'multipleLineItem': action.payload,
        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.multipleLineItemSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.multipleLineItemError(response.data));
      }
    } catch (e) {
      yield put(actions.multipleLineItemError("error occurs"));
      console.warn('Some error found in "multipleLineItem request" action\n', e);
    }
  }
}


export function* procurementSiteRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.procurementSiteClear());
  }
  else {
    try {


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/site/po/get/all`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageno: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy,
          module: "PROCUREMENT"
        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.procurementSiteSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.procurementSiteError(response.data));
      }
    } catch (e) {
      yield put(actions.procurementSiteError("error occurs"));
      console.warn('Some error found in "procurementSite request" action\n', e);
    }
  }
}

export function* getPoItemCodeRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.getPoItemCodeClear());
  }
  else {

    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/itembarcode`,
        // const response = yield call(fireAjax, 'GET', `http://localhost:5000/proxy/10/1`,

        // {
        //   type: action.payload.type == "" ? 1 : action.payload.type,
        //   pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        //   search: action.payload.search,
        //   articleCode: action.payload.articleCode,
        //   supplierCode: action.payload.supplierCode,
        //   siteId: action.payload.siteCode,
        //   searchBy: "startwith",
        //   mrp: action.payload.mrp
        // });
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search,
          articleCode: "",
          supplierCode: action.payload.supplierCode,
          siteId: action.payload.siteCode,
          searchBy: "contains",
          version: "V2"
        });

      const finalResponse = script(response);
      if (finalResponse.success) {
        console.log(response)
        yield put(actions.getPoItemCodeSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.getPoItemCodeError(response.data));
      }
    } catch (e) {
      yield put(actions.getPoItemCodeError("error occurs"));
      console.warn('Some error found in "getPoItemCode request" action\n', e);
    }
  }
}

export function* poItemBarcodeRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.poItemBarcodeClear());
  }
  else {
    try {
      let itemId = action.payload.itemId

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/find/itembarcode/detail`,
        {
          "validTo": action.payload.validTo,
          "articleCode": action.payload.articleCode,
          "items": action.payload.items,
          "version": action.payload.version
        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.poItemBarcodeSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.poItemBarcodeError(response.data));
      }
    } catch (e) {
      yield put(actions.poItemBarcodeError("error occurs"));
      console.warn('Some error found in "poItemBarcode request" action\n', e);
    }
  }
}



export function* poRadioValidationRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.poRadioValidationClear());
  }
  else {
    try {
      var { orgIdGlobal } = OganisationIdName()


      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/get/availablekeys?orgId=${orgIdGlobal}`,
        {
          // "type": action.payload.type,
          // "search": action.payload.search,
          // "siteId": action.payload.siteId,
          // "articleId": action.payload.articleId,
          // "desc6": action.payload.desc6,
          // "cat5Code": action.payload.cat5Code,
          // "cat5Name": action.payload.cat5Name,
          // "cat6Code": action.payload.cat6Code,
          // "cat6Name":action.payload.cat6Name  
        });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.poRadioValidationSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.poRadioValidationError(response.data));
      }
    } catch (e) {
      yield put(actions.poItemBarcodeError("error occurs"));
      console.warn('Some error found in "poItemBarcode request" action\n', e);
    }
  }
}

// ___________________________FMCG GET HISTORY_____________________________

export function* fmcgGetHistoryRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.fmcgGetHistoryClear());
  }
  else {
    try {

      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/fmcg/get/history`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search,
          uploadedDate: action.payload.uploadedDate,
          fileName: action.payload.fileName,
          fileRecord: action.payload.fileRecord,
          status: action.payload.status,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.fmcgGetHistorySuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.fmcgGetHistoryError(response.data));
      }

    } catch (e) {
      yield put(actions.fmcgGetHistoryError("error occurs"));
      console.warn('Some error found in "fmcgGetHistory request" action\n', e);
    }
  }
}

//multiple Calculated margin


export function* getMultipleMarginRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.getMultipleMarginClear());
  }
  else {
    try {


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/calculate/multipleactual/markup/article/{data}`,
        {
          "multipleMarkup": action.payload
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getMultipleMarginSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.getMultipleMarginError(response.data));
      }

    } catch (e) {
      yield put(actions.getMultipleMarginError("error occurs"));
      console.warn('Some error found in "getMultipleMargin request" action\n', e);
    }
  }
}


//getAllcity
export function* getAllCityRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.getAllCityClear());
  }
  else {
    try {


      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/cities`,
        {
          type: action.payload.type == "" ? 1 : action.payload.type,
          pageNo: action.payload.no == undefined ? 1 : action.payload.no,
          search: action.payload.search,
          searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getAllCitySuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.getAllCityError(response.data));
      }

    } catch (e) {
      yield put(actions.getAllCityError("error occurs"));
      console.warn('Some error found in "getAllCity request" action\n', e);
    }
  }
}


export function* discountRequest(action) {

  if (action.payload == undefined) {
    yield put(actions.discountClear());
  }
  else {
    try {
      let articleCode = action.payload.articleCode
      let mrp = action.payload.mrp;
      let discountType = action.payload.discountType;

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/get/discount?articleCode=${articleCode}&mrp=${mrp}&discountType=${discountType}`,
        {

        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.discountSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.discountError(response.data));
      }

    } catch (e) {
      yield put(actions.discountError("error occurs"));
      console.warn('Some error found in "discount request" action\n', e);
    }
  }
}

// _____________________________PURCHASE INDENT ARTICLE NEW API____________________________


export function* piArticleRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.piArticleClear());
  }
  else {


    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.PI}/get/all`, {

        pageNo: action.payload.no == undefined ? 1 : action.payload.no,
        type: action.payload.type == "" ? 1 : action.payload.type,
        hl3Code: action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
        search: action.payload.search == undefined ? "" : action.payload.search,
        searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.piArticleSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.piArticleError(response.data));
      }
    } catch (e) {
      yield put(actions.piArticleError("error occurs"));
      console.warn('Some error found in "piArticle request" action\n', e);
    }
  }
}



export function* getLineItemRequest(action) {



  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/detail/v2`, {

      orderNo: action.payload.orderNo,
      isDraft: action.payload.status

    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getLineItemSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getLineItemError(response.data));
    }
  } catch (e) {
    yield put(actions.getLineItemError("error occurs"));
    console.warn('Some error found in "getLineItem request" action\n', e);
  }

}



export function* getPiLineItemRequest(action) {



  try {
    let orderNo = action.payload.orderNo == undefined ? 1 : action.payload.orderNo;
    let isDraft = action.payload.status == undefined ? true : action.payload.status;

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/get/detail/v2`, {
        "orderId": orderNo,
        "isDraft": isDraft
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getPiLineItemSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.getPiLineItemError(response.data));
    }
  } catch (e) {
    yield put(actions.getPiLineItemError("error occurs"));
    console.warn('Some error found in "getPiLineItem request" action\n', e);
  }

}



export function* poArticleRequest(action) {

  let isArticleWithMrp = action.payload.isArticleWithMrp == undefined ? '' : {isArticleWithMrp : action.payload.isArticleWithMrp};
  try {

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/custom/article/hierarchy/data`, {
      type: action.payload.type == "" ? 1 : action.payload.type,
      pageNo: action.payload.no == undefined ? 1 : action.payload.no,
      supplier: action.payload.supplier == undefined ? "" : action.payload.supplier,
      basedOn: action.payload.basedOn == undefined ? "" : action.payload.basedOn,

      hl4Code: action.payload.hl4Code == undefined ? "" : action.payload.hl4Code,
      hl4Name: action.payload.hl4Name == undefined ? "" : action.payload.hl4Name,
      hl1Name: action.payload.hl1Name == undefined ? "" : action.payload.hl1Name,
      hl2Name: action.payload.hl2Name == undefined ? "" : action.payload.hl2Name,
      hl3Name: action.payload.hl3Name == undefined ? "" : action.payload.hl3Name,
      hl3Code: action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
      hl2Code: action.payload.hl2Code == undefined ? "" : action.payload.hl2Code,
      hl1Code: action.payload.hl1Code == undefined ? "" : action.payload.hl1Code,

      divisionSearch: action.payload.divisionSearch == undefined ? "" : action.payload.divisionSearch,
      sectionSearch: action.payload.sectionSearch == undefined ? "" : action.payload.sectionSearch,
      departmentSearch: action.payload.departmentSearch == undefined ? "" : action.payload.departmentSearch,
      articleSearch: action.payload.articleSearch == undefined ? "" : action.payload.articleSearch,
      searchBy: action.payload.searchBy == undefined ? "startWith" : action.payload.searchBy,
      mrpSearch: action.payload.mrpSearch == undefined ? "" : action.payload.mrpSearch,
      isMrp: action.payload.isMrp,
      ...isArticleWithMrp
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.poArticleSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.poArticleError(response.data));
    }
  } catch (e) {
    yield put(actions.poArticleError("error occurs"));
    console.warn('Some error found in "poArticle request" action\n', e);
  }

}





// export function* multipleOtbRequest(action) {



//   try {

//     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/calculate/multiple/otb`, {

//       month: action.payload.month,
//       year: action.payload.year,
//       articleList: action.payload.articleList


//     });
//     const finalResponse = script(response);
//     if (finalResponse.success) {
//       yield put(actions.multipleOtbSuccess(response.data.data));
//     } else if (finalResponse.failure) {
//       yield put(actions.multipleOtbError(response.data));
//     }
//   } catch (e) {
//     yield put(actions.multipleOtbError("error occurs"));
//     console.warn('Some error found in "multipleOtb request" action\n', e);
//   }

// }





export function* gen_PurchaseOrderRequest(action) {

  try {




    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/multiple/article/create`,
      {
        isSet: action.payload.isSet,
        prevOrderNo: action.payload.prevOrderNo,
        orderNo: action.payload.orderNo, 
        poType: action.payload.poType,
        orderDate: moment(action.payload.orderDate).format(),
        validFrom: moment(action.payload.validFrom).format(),
        validTo: moment(action.payload.validTo).format(),
        typeOfBuying: action.payload.typeOfBuying,
        slCode: action.payload.slCode,
        slName: action.payload.slName,
        slCityName: action.payload.slCityName,
        slAddr: action.payload.slAddr,
        leadTime: action.payload.leadTime,
        termCode: action.payload.termCode,
        termName: action.payload.termName,
        transporterCode: action.payload.transporterCode,
        transporterName: action.payload.transporterName,
        poQuantity: action.payload.poQuantity,
        poAmount: action.payload.poAmount,
        stateCode: action.payload.stateCode,
        enterpriseGstin: action.payload.enterpriseGstin,
        poDetails: action.payload.poDetails,
        isHoldPO: action.payload.isHoldPO,
        isUDFExist: action.payload.isUDFExist,
        siteCode: action.payload.siteCode,
        siteName: action.payload.siteName,
        itemUdfExist: action.payload.itemUdfExist,
        poudf1: action.payload.poudf1,
        poudf2: action.payload.poudf2,
        poudf3: action.payload.poudf3,
        poudf4: action.payload.poudf4,
        poudf5: action.payload.poudf5,
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.gen_PurchaseOrderSuccess(response.data.data));

    } else if (finalResponse.failure) {

      yield put(actions.gen_PurchaseOrderError(response.data));
    }

  } catch (e) {
    yield put(actions.gen_PurchaseOrderError("error occurs"));
    console.warn('Some error found in "gen_PurchaseOrder request" action\n', e);
  }

}




export function* gen_PurchaseIndentRequest(action) {

  try {




    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/multiple/article/create`,
      {
        isSet: action.payload.isSet,
        typeOfBuying: action.payload.typeOfBuying,
        slCode: action.payload.slCode,
        slName: action.payload.slName,
        slCityName: action.payload.slCityName,
        slAddr: action.payload.slAddr,
        leadTime: action.payload.leadTime,
        termCode: action.payload.termCode,
        termName: action.payload.termName,
        transporterCode: action.payload.transporterCode,
        transporterName: action.payload.transporterName,
        poQuantity: action.payload.poQuantity,
        poAmount: action.payload.poAmount,
        stateCode: action.payload.stateCode,
        enterpriseGstin: action.payload.enterpriseGstin,
        piDetails: action.payload.piDetails,

        isUDFExist: action.payload.isUDFExist,
        siteCode: action.payload.siteCode,
        siteName: action.payload.siteName,
        itemUdfExist: action.payload.itemUdfExist,
        indentNo: action.payload.indentNo,
        validFrom: moment(action.payload.validFrom).format(),
        validTo: moment(action.payload.validTo).format(),
        // validFrom: action.payload.validFrom,
        // validTo: action.payload.validTo,
        poudf1: action.payload.poudf1,
        poudf2: action.payload.poudf2,
        poudf3: action.payload.poudf3,
        poudf4: action.payload.poudf4,
        poudf5: action.payload.poudf5,

      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.gen_PurchaseIndentSuccess(response.data.data));

    } else if (finalResponse.failure) {

      yield put(actions.gen_PurchaseIndentError(response.data));
    }

  } catch (e) {
    yield put(actions.gen_PurchaseIndentError("error occurs"));
    console.warn('Some error found in "gen_PurchaseIndent request" action\n', e);
  }

}


export function* gen_SetBasedRequest(action) {


  try {
    let orderNo = action.payload.orderNo
    let validTo = action.payload.validTo
    // var { orgIdGlobal } = OganisationIdName(
    let poType = action.payload.poType

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/setbase/data`,
      {
        orderNo: orderNo,
        validTo: validTo,
        poType: poType


      });
    const finalResponse = script(response);
    if (finalResponse.success) {

      yield put(actions.gen_SetBasedSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.gen_SetBasedError(response.data));
    }
  } catch (e) {
    yield put(actions.gen_SetBasedError("error occurs"));
    console.warn('Some error found in "gen_SetBased request" action\n', e);
  }

}


export function* gen_IndentBasedRequest(action) {


  try {

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/indent/base`,
      {
        indentNo: [action.payload.indentNo]
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.gen_IndentBasedSuccess(response.data.data));

    } else if (finalResponse.failure) {

      yield put(actions.gen_IndentBasedError(response.data));
    }

  } catch (e) {
    yield put(actions.gen_IndentBasedError("error occurs"));
    console.warn('Some error found in "gen_IndentBased request" action\n', e);
  }

}


export function* po_approve_reject_Request(action) {

  try {



    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/update/status`,
      {
        documentNumbers: action.payload.payloadForStatus.patterns,
        status: action.payload.payloadForStatus.status,
        remark: action.payload.payloadForStatus.remark != undefined ? action.payload.payloadForStatus.remark : ''
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.po_approve_reject_Success(response.data.data));
      yield put(actions.poHistoryRequest(action.payload.payloadForHistory));
    } else if (finalResponse.failure) {

      yield put(actions.po_approve_reject_Error(response.data));
    }

  } catch (e) {
    yield put(actions.po_approve_reject_Error("error occurs"));
    console.warn('Some error found in "po_approve_reject_ request" action\n', e);
  }

}

export function* po_approve_retry_Request(action) {

  try {



    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/erp/retry`,
      {
        documentNumbers: action.payload.payloadForStatus.patterns,
        status: action.payload.payloadForStatus.status,
        remark: action.payload.payloadForStatus.remark != undefined ? action.payload.payloadForStatus.remark : ''
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.po_approve_retry_Success(response.data.data));
      yield put(actions.poHistoryRequest(action.payload.payloadForHistory));
    } else if (finalResponse.failure) {

      yield put(actions.po_approve_retry_Error(response.data));
    }

  } catch (e) {
    yield put(actions.po_approve_retry_Error("error occurs"));
    console.warn('Some error found in "po_approve_reject_ request" action\n', e);
  }

}

export function* piEditRequest(action) {
  console.log(action, "piEditRequest");
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/piedit`,
      {
        // indentId:"12"
        indentId: action.payload.indentId
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.piEditSuccess(response.data.data));

    } else if (finalResponse.failure) {

      yield put(actions.piEditError(response.data));
    }
  }
  catch (e) {
    yield put(actions.piEditError("error occurs"));
    console.warn('Some error found in "piEditRequest" action\n', e);
  }
}

export function* editedPiSaveRequest(action) {


  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/update`,
      {
        isSet: action.payload.isSet,
        typeOfBuying: action.payload.typeOfBuying,
        slCode: action.payload.slCode,
        slName: action.payload.slName,
        slCityName: action.payload.slCityName,
        slAddr: action.payload.slAddr,
        leadTime: action.payload.leadTime,
        termCode: action.payload.termCode,
        termName: action.payload.termName,
        transporterCode: action.payload.transporterCode,
        transporterName: action.payload.transporterName,
        poQuantity: action.payload.poQuantity,
        poAmount: action.payload.poAmount,
        stateCode: action.payload.stateCode,
        enterpriseGstin: action.payload.enterpriseGstin,
        piDetails: action.payload.piDetails,

        isUDFExist: action.payload.isUDFExist,
        siteCode: action.payload.siteCode,
        siteName: action.payload.siteName,
        itemUdfExist: action.payload.itemUdfExist,
        indentNo: action.payload.indentNo,
        id: action.payload.indentId,
        status: action.payload.status,
        validFrom: moment(action.payload.validFrom).format(),
        validTo: moment(action.payload.validTo).format(),
        poudf1: action.payload.poudf1,
        poudf2: action.payload.poudf2,
        poudf3: action.payload.poudf3,
        poudf4: action.payload.poudf4,
        poudf5: action.payload.poudf5,
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.editedPiSaveSuccess(response.data.data));

    } else if (finalResponse.failure) {

      yield put(actions.editedPiSaveError(response.data));
    }
  } catch (e) {
    yield put(actions.editedPiSaveError("error occurs"));
    console.warn('Some error found in "gen_PurchaseIndent request" action\n', e);
  }


}

//save po draft

export function* poSaveDraftRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/saved/draft`,
      {
        orderNo: action.payload.orderNo,
        status: action.payload.status,
        isSet: action.payload.isSet,
        prevOrderNo: action.payload.prevOrderNo,
        poType: action.payload.poType,
        orderDate: moment(action.payload.orderDate).format(),
        validFrom: moment(action.payload.validFrom).format(),
        validTo: moment(action.payload.validTo).format(),
        typeOfBuying: action.payload.typeOfBuying,
        slCode: action.payload.slCode,
        slName: action.payload.slName,
        slCityName: action.payload.slCityName,
        slAddr: action.payload.slAddr,
        leadTime: action.payload.leadTime,
        termCode: action.payload.termCode,
        termName: action.payload.termName,
        transporterCode: action.payload.transporterCode,
        transporterName: action.payload.transporterName,
        poQuantity: action.payload.poQuantity,
        poAmount: action.payload.poAmount,
        stateCode: action.payload.stateCode,
        enterpriseGstin: action.payload.enterpriseGstin,
        poDetails: action.payload.poDetails,
        isHoldPO: action.payload.isHoldPO,
        isUDFExist: action.payload.isUDFExist,
        siteCode: action.payload.siteCode,
        siteName: action.payload.siteName,
        itemUdfExist: action.payload.itemUdfExist,
        poudf1: action.payload.poudf1,
        poudf2: action.payload.poudf2,
        poudf3: action.payload.poudf3,
        poudf4: action.payload.poudf4,
        poudf5: action.payload.poudf5,
      });
    const finalResponse = script(response);
    // if (finalResponse.success) {
    //   yield put(actions.poSaveDraftSuccess(response.data.data));

    // }
    if (finalResponse.success) {
      response.data.data.modal = action.payload.showModal
      yield put(actions.poSaveDraftSuccess(response.data.data));
    }
    else if (finalResponse.failure) {

      yield put(actions.poSaveDraftError(response.data));
    }
  } catch (e) {
    yield put(actions.poSaveDraftError("error occurs"));
    console.warn('Some error found in "poSaveDraft request" action\n', e);
  }
}

//save pi draft

export function* piSaveDraftRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/saved/draft`,
      {
        indentNo: action.payload.indentNo,
        status: action.payload.status,
        isSet: action.payload.isSet,
        typeOfBuying: action.payload.typeOfBuying,
        slCode: action.payload.slCode,
        slName: action.payload.slName,
        slCityName: action.payload.slCityName,
        slAddr: action.payload.slAddr,
        leadTime: action.payload.leadTime,
        termCode: action.payload.termCode,
        termName: action.payload.termName,
        transporterCode: action.payload.transporterCode,
        transporterName: action.payload.transporterName,
        poQuantity: action.payload.poQuantity,
        poAmount: action.payload.poAmount,
        stateCode: action.payload.stateCode,
        enterpriseGstin: action.payload.enterpriseGstin,
        piDetails: action.payload.piDetails,

        isUDFExist: action.payload.isUDFExist,
        siteCode: action.payload.siteCode,
        siteName: action.payload.siteName,
        itemUdfExist: action.payload.itemUdfExist,
        validFrom: moment(action.payload.validFrom).format(),
        validTo: moment(action.payload.validTo).format(),
        poudf1: action.payload.poudf1,
        poudf2: action.payload.poudf2,
        poudf3: action.payload.poudf3,
        poudf4: action.payload.poudf4,
        poudf5: action.payload.poudf5,
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      response.data.data.modal = action.payload.showModal
      yield put(actions.piSaveDraftSuccess(response.data.data));

    } else if (finalResponse.failure) {

      yield put(actions.piSaveDraftError(response.data));
    }
  } catch (e) {
    yield put(actions.piSaveDraftError("error occurs"));
    console.warn('Some error found in "piSaveDraft request" action\n', e);
  }
}

export function* getDraftRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/draft`,
      {
        orderNo: action.payload.orderNo,
        draftType: action.payload.draftType
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getDraftSuccess(response.data.data))
      console.log(response)
    } else if (finalResponse.failure) {
      yield put(actions.getDraftError(response.data))
    }
  }
  catch (e) {
    yield put(action.getDraftError("error occoured"));
    console.warn('Some error found in "getDraft request" action\n', e);
  }
}

export function* poEditRequest(action) {
  console.log("PO edit request")
  console.log(action)
  try {
    if (action.payload.version == "V1") {
      var { orgIdGlobal } = OganisationIdName();
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/po/get/poedit/v1?orderNo=${action.payload.orderNo}&validTo=${action.payload.validTo}&orgId=${orgIdGlobal}&poType=${action.payload.poType}`);
      const finalResponse = script(response);
      if (finalResponse.success) {
        console.log("po edit success")

        yield put(actions.poEditSuccess(response.data.data));

      } else if (finalResponse.failure) {
        console.log("po edit fail")
        yield put(actions.poEditError(response.data));
      }

    } else {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/poedit`,
        {
          orderNo: action.payload.orderNo,
          poType: action.payload.poType
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.poEditSuccess(response.data.data));

      } else if (finalResponse.failure) {

        yield put(actions.poEditError(response.data));
      }

    }


  } catch (e) {
    console.warn(e, "error with poedit");
    yield put(actions.poEditError(response.data));

  }
}

export function* editedPoSaveRequest(action) {
  try {

    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/update`,
      {
        isSet: action.payload.isSet,
        prevOrderNo: action.payload.prevOrderNo,
        poType: action.payload.poType,
        orderDate: moment(action.payload.orderDate).format(),
        validFrom: moment(action.payload.validFrom).format(),
        validTo: moment(action.payload.validTo).format(),
        typeOfBuying: action.payload.typeOfBuying,
        slCode: action.payload.slCode,
        slName: action.payload.slName,
        slCityName: action.payload.slCityName,
        slAddr: action.payload.slAddr,
        leadTime: action.payload.leadTime,
        termCode: action.payload.termCode,
        termName: action.payload.termName,
        transporterCode: action.payload.transporterCode,
        transporterName: action.payload.transporterName,
        poQuantity: action.payload.poQuantity,
        poAmount: action.payload.poAmount,
        stateCode: action.payload.stateCode,
        enterpriseGstin: action.payload.enterpriseGstin,
        poDetails: action.payload.poDetails,
        isHoldPO: action.payload.isHoldPO,
        isUDFExist: action.payload.isUDFExist,
        siteCode: action.payload.siteCode,
        siteName: action.payload.siteName,
        itemUdfExist: action.payload.itemUdfExist,
        orderNo: action.payload.orderNo,
        poudf1: action.payload.poudf1,
        poudf2: action.payload.poudf2,
        poudf3: action.payload.poudf3,
        poudf4: action.payload.poudf4,
        poudf5: action.payload.poudf5,
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.editedPoSaveSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.editedPoSaveError(response.data));
    }
  } catch (e) {
    yield put(actions.editedPoSaveError(response.data));
  }
}

export function* discardPoPiDataRequest(action) {
  console.log(action)
  try {
    if (action.payload.discardData != undefined && action.payload.value === 'pi') {
      let value = action.payload.value
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/${value}/discard`,
        {
          discardData: action.payload.discardData
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.discardPoPiDataSuccess(response.data.data))
      } else if (finalResponse.failure) {
        yield put(actions.discardPoPiDataError(response.data))
      }

    } else if (action.payload.discardData != undefined && action.payload.value === 'po') {
      let value = action.payload.value
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/${value}/discard`,
        {
          discardData: action.payload.discardData
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.discardPoPiDataSuccess(response.data.data))
      } else if (finalResponse.failure) {
        yield put(actions.discardPoPiDataError(response.data))
      }

    }



  } catch (e) {
    console.warn(e, "discardPoPiDataRequest")
    yield put(actions.discardPoPiDataError(response.data));
  }
}

export function* viewImagesRequest(action) {
  console.log(action)
  try {
    if (action.payload.filePath != undefined) {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/view/images`,
        {
          filePath: action.payload.filePath,
        });
      const finalResponse = script(response);
      if (finalResponse.success) {
        response.data.data.gridTwoId = action.payload.gridTwoId
        yield put(actions.viewImagesSuccess(response.data.data))
      } else if (finalResponse.failure) {
        yield put(actions.viewImagesError(response.data))
      }

    }
  } catch (e) {
    console.warn(e, "viewImagesRequest")
    yield put(actions.viewImagesError(response.data));
  }
}

// procurement Dashboard Bottom APi
export function* piDashBottomRequest(action) {
  try {
    console.log(action)
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/procurement/dashboard/get/actionable/data`,
      {
        "isPO": action.payload.isPO == undefined ? "" : action.payload.isPO,
        "fromDate": action.payload.fromDate == undefined ? "" : action.payload.fromDate,
        "toDate": action.payload.toDate == undefined ? "" : action.payload.toDate,
        "expireDays": action.payload.expireDays == undefined ? "" : action.payload.expireDays,
        "siteCode": action.payload.siteCode == undefined ? "" : action.payload.siteCode,
        "hl1Code": action.payload.hl1Code == undefined ? "" : action.payload.hl1Code,
        "hl2Code": action.payload.hl2Code == undefined ? "" : action.payload.hl2Code,
        "hl3Code": action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
        "slCode": action.payload.slCode == undefined ? "" : action.payload.slCode,
        "hl4Code": action.payload.hl4Code == undefined ? "" : action.payload.hl4Code,
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.piDashBottomSuccess(response.data.data))
    } else if (finalResponse.failure) {
      yield put(actions.piDashBottomError(response.data))
    }
  } catch (e) {
    console.warn(e, "piDashBottomRequest")
    yield put(actions.piDashBottomError(response.data));
  }
}
// poDashboard bottom
// export function* poDashBottomRequest(action) {  
//   try {
//     const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/procurement/dashboard/get/actionable/data`,
//       {
//         "isPO": action.payload.isPO == undefined ? "" : action.payload.isPO,
//         "fromDate": action.payload.fromDate == undefined ? "" : action.payload.fromDate,
//         "toDate": action.payload.toDate == undefined ? "" : action.payload.toDate,
//         "expireDays": action.payload.expireDays == undefined ? "" : action.payload.expireDays,
//         "siteCode": action.payload.siteCode == undefined ? "" : action.payload.siteCode,
//         "hl1Code": action.payload.hl1Code == undefined ? "" : action.payload.hl1Code,
//         "hl2Code": action.payload.hl2Code == undefined ? "" : action.payload.hl2Code,
//         "hl3Code": action.payload.hl3Code == undefined ? "" : action.payload.hl3Code,
//         "hl4Code": action.payload.hl4Code == undefined ? "" : action.payload.hl4Code,
//         "slCode": ""
//       });
//     const finalResponse = script(response);
//     if (finalResponse.success) {
//       yield put(actions.poDashBottomSuccess(response.data.data))
//     } else if (finalResponse.failure) {
//       yield put(actions.poDashBottomError(response.data))
//     }
//   } catch (e) {
//     console.warn(e, "poDashBottomRequest")
//     yield put(actions.poDashBottomError(response.data));
//   }
// }

// pi hierarchy dashboard bottom
export function* piDashHierarchyRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/procurement/dashboard/get/hierarachylevels`,
      {
        "type": action.payload.type == undefined ? "" : action.payload.type,
        "pageNo": action.payload.pageNo == undefined ? "" : action.payload.pageNo,
        "search": action.payload.search == undefined ? "" : action.payload.search,
        "isPO": action.payload.isPO == undefined ? "" : action.payload.isPO,
        "fromDate": action.payload.fromDate == undefined ? "" : action.payload.fromDate,
        "toDate": action.payload.toDate == undefined ? "" : action.payload.toDate,
        "siteCode": action.payload.siteCode == undefined ? "" : action.payload.siteCode,
        "hl1Name": action.payload.hl1Name == undefined ? "" : action.payload.hl1Name,
        "hl2Name": action.payload.hl2Name == undefined ? "" : action.payload.hl2Name,
        "hl3Name": action.payload.hl3Name == undefined ? "" : action.payload.hl3Name,
        "hl4Name": action.payload.hl4Name == undefined ? "" : action.payload.hl4Name,
        "getBy": action.payload.getBy == undefined ? "" : action.payload.getBy,
        "slCode": action.payload.slCode == undefined ? "" : action.payload.slCode,
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.piDashHierarchySuccess(response.data.data))
    } else if (finalResponse.failure) {
      yield put(actions.piDashHierarchyError(response.data))
    }
  } catch (e) {
    console.warn(e, "piDashHierarchyRequest")
    yield put(actions.piDashHierarchyError(response.data));
  }
}
 //PO DASHBOARD EMAIL
export function* emailDashboardSendRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/procurement/dashboard/send/mail`,
      action.payload);
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.emailDashboardSendSuccess(response.data.data))
    } else if (finalResponse.failure) {
      yield put(actions.emailDashboardSendError(response.data))
    }
  } catch (e) {
    console.warn(e, "emailDashboardSendRequest")
    yield put(actions.emailDashboardSendError(response.data));
  }
}

export function* piPoDashSiteRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/procurement/dashboard/get/site`,
      {
        "isPO": action.payload.isPO == undefined ? "" : action.payload.isPO,
        "fromDate": action.payload.fromDate == undefined ? "" : action.payload.fromDate,
        "toDate": action.payload.toDate == undefined ? "" : action.payload.toDate,
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.piPoDashSiteSuccess(response.data.data))
    } else if (finalResponse.failure) {
      yield put(actions.piPoDashSiteError(response.data))
    }
  } catch (e) {
    console.warn(e, "piPoDashSiteRequest")
    yield put(actions.piPoDashSiteError(response.data));
  }
}

// pi image url request
export function* piImageUrlRequest(action) {
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/view/images`,
      {
        filePath: action.payload.path
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.piImageUrlSuccess(response.data.data))
    } else if (finalResponse.failure) {
      yield put(actions.piImageUrlError(response.data))
    }
  } catch (e) {
    yield put(actions.piImageUrlError("error occurs"));
    console.warn('Some error found in "piImageUrlRequest" action\n', e);
  }
}

export function* getSlRequest(action) {
  console.log("in action", action)
  try {
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/procurement/dashboard/get/sl`,
      {
        "type": action.payload.type,
        "pageNo": action.payload.pageNo,
        "search": action.payload.search,
        "siteCode": action.payload.siteCode,
        "isPO": action.payload.isPO == undefined ? "" : action.payload.isPO,
        "fromDate": action.payload.fromDate == undefined ? "" : action.payload.fromDate,
        "toDate": action.payload.toDate == undefined ? "" : action.payload.toDate,
      });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.getSlSuccess(response.data.data))
    } else if (finalResponse.failure) {
      yield put(actions.getSlError(response.data))
    }
  } catch (e) {
    console.warn(e, "getSlRequest")
    yield put(actions.getSlError(response.data));
  }
}

// Dashboard excell download

export function* exportDashboardExcelRequest(action){

  try{
    const response = yield call(fireAjax, 'POST' , `${CONFIG.BASE_URL}/procurement/dashboard/export/filtered/data`, {
      "isPO": action.payload.isPO != undefined ? action.payload.isPO : false,
      "isMainHeader": action.payload.isMainHeader != undefined ? action.payload.isMainHeader : false,
      "fromDate": action.payload.fromDate != undefined ? action.payload.fromDate : "",
      "toDate": action.payload.toDate != undefined ? action.payload.toDate : "",
      "status": action.payload.status != undefined ? action.payload.status : "",
      "expireDays": action.payload.expireDays != undefined ? action.payload.expireDays : "",
      "siteCode": action.payload.siteCode != undefined ? action.payload.siteCode : "",
      "hl1Code": action.payload.hl1Code != undefined ? action.payload.hl1Code : "",
      "hl2Code": action.payload.hl2Code != undefined ? action.payload.hl2Code : "",
      "hl3Code": action.payload.hl3Code != undefined ? action.payload.hl3Code : "",
      "hl4Code": action.payload.hl4Code != undefined ? action.payload.hl4Code : "",
      "slCode": action.payload.slCode != undefined ? action.payload.slCode : "",
      "headers": action.payload.headers != undefined ? action.payload.headers : "",
    });
    const finalResponse = script(response);
    if(finalResponse.success){
      yield put(actions.exportDashboardExcelSuccess(response.data.data))
    } else if(finalResponse.failure){
      yield put(actions.exportDashboardExcelError(response.data))
    }
  } catch (e) {
    yield put(actions.exportDashboardExcelError('error occured'))
    console.warn('error find in exportDashboarExcelError')
  }
}

