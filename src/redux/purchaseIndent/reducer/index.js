import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
  divisionData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  transporter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  supplier: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getAgentName: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  leadTime: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  vendorMrp: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  size: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  color: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  image: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  otb: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  lineItem: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  markUp: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  purchaseTerm: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  quantity: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  total: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  piCreate: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  piHistory: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: '',
    search: '',
    filter: {}
  },

  loadIndent: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  articlePo: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  piUpdate: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  piDownload: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  poItemcode: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getTransporter: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getPurchaseTerm: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getPoData: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  udfType: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  getUdfMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  poHistory: {
    data: {},
    search: "",
    filter: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  poCreate: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  poSize: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  udfSetting: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  activeUdf: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  activeCatDesc: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getCatDescUdf: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  itemCatDescUdf: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  itemUdfMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createItemUdf: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  poUdfMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateUdfMapping: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getItemUdf: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateItemUdf: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateItemCatDesc: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateSizeDept: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  articleName: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''

  },
  cname: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''

  },
  piAddNew: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  marginRule: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getItemDetails: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getItemDetailsValue: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },

  selectVendor: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  selectOrderNumber: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  setBased: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  departmentSetBased: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  hsnCode: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  multipleLineItem: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  procurementSite: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  getPoItemCode: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  poItemBarcode: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  poRadioValidation: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  fmcgGetHistory: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  getMultipleMargin: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  getAllCity: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  discount: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  piArticle: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  getLineItem: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  getPiLineItem: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  poArticle: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  multipleOtb: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  analyticsOtb: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  gen_PurchaseIndent: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  gen_PurchaseOrder: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  gen_SetBased: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  gen_IndentBased: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  po_approve_reject: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  po_approve_retry: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  pi_edit_data: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  save_edited_pi: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  save_draft_po: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  save_draft_pi: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  get_draft_data: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: '',
    indentNo: ''
  },
  po_edit_data: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  save_edited_po: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  discardPoPiData: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  viewImages: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  viewImages: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  piDashBottom: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  piDashHierarchy: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  emailDashboardSend: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  piPoDashSite: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  displayModal: {
    data: false,
  },
  piImageUrl: {
    data: {},
    isSuccess: false,
    isError: false,
    message: ''
  },
  slData: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  catDescDropdown: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  updateItemUdfMapping: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },  
  exportDashboardExcel: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  mappingExcelUpload: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  mappingExcelStatus: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  },
  mappingExcelExport: {
    data: {},
    isLoading: false,
    isSuccess: false,
    isError: false,
    message: ''
  }
};

// Dashboard Excel Download

const exportDashboardExcelRequest = (state, action) => update(state, {
  exportDashboardExcel: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' } 
  }
})

const exportDashboardExcelSuccess = (state, action) => update(state, {
  exportDashboardExcel: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'success' } 
  }
})

const exportDashboardExcelClear = (state, action) => update(state, {
  exportDashboardExcel: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: 'Clear' } 
  }
})

const exportDashboardExcelError = (state, action) => update(state, {
  exportDashboardExcel: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: 'Error' } 
  }
})

// _______________________-FMCG HISTORY_________________________

const mappingExcelUploadRequest = (state, action) => update(state, {
  mappingExcelUpload: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const mappingExcelUploadSuccess = (state, action) => update(state, {
  mappingExcelUpload: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})
const mappingExcelUploadError = (state, action) => update(state, {
  mappingExcelUpload: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const mappingExcelUploadClear = (state, action) => update(state, {
  mappingExcelUpload: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
})

const mappingExcelStatusRequest = (state, action) => update(state, {
  mappingExcelStatus: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const mappingExcelStatusSuccess = (state, action) => update(state, {
  mappingExcelStatus: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})
const mappingExcelStatusError = (state, action) => update(state, {
  mappingExcelStatus: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const mappingExcelStatusClear = (state, action) => update(state, {
  mappingExcelStatus: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
})

const mappingExcelExportRequest = (state, action) => update(state, {
  mappingExcelExport: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const mappingExcelExportSuccess = (state, action) => update(state, {
  mappingExcelExport: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})
const mappingExcelExportError = (state, action) => update(state, {
  mappingExcelExport: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const mappingExcelExportClear = (state, action) => update(state, {
  mappingExcelExport: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
})

// _______________________-FMCG HISTORY_________________________

const fmcgGetHistoryRequest = (state, action) => update(state, {
  fmcgGetHistory: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const fmcgGetHistorySuccess = (state, action) => update(state, {
  fmcgGetHistory: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})
const fmcgGetHistoryError = (state, action) => update(state, {
  fmcgGetHistory: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const fmcgGetHistoryClear = (state, action) => update(state, {
  fmcgGetHistory: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
})

// ______________________________ARTICLE NEW_________________________

const piArticleRequest = (state, action) => update(state, {
  piArticle: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const piArticleSuccess = (state, action) => update(state, {
  piArticle: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})
const piArticleError = (state, action) => update(state, {
  piArticle: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const piArticleClear = (state, action) => update(state, {
  piArticle: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
})

// ______________________________________

const marginRuleRequest = (state, action) => update(state, {
  marginRule: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const marginRuleSuccess = (state, action) => update(state, {
  marginRule: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
})
const marginRuleError = (state, action) => update(state, {
  marginRule: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
})
const marginRuleClear = (state, action) => update(state, {
  marginRule: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
})

const articlePoRequest = (state, action) => update(state, {
  articlePo: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const articlePoSuccess = (state, action) => update(state, {
  articlePo: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const articlePoError = (state, action) => update(state, {
  articlePo: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const articlePoClear = (state, action) => update(state, {
  articlePo: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const divisionSectionDepartmentRequest = (state, action) => update(state, {
  divisionData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const divisionSectionDepartmentSuccess = (state, action) => update(state, {
  divisionData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const divisionSectionDepartmentError = (state, action) => update(state, {
  divisionData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const divisionSectionDepartmentClear = (state, action) => update(state, {
  divisionData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const transporterRequest = (state, action) => update(state, {
  transporter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const transporterSuccess = (state, action) => update(state, {
  transporter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const transporterError = (state, action) => update(state, {
  transporter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getTransporterRequest = (state, action) => update(state, {
  getTransporter: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getTransporterSuccess = (state, action) => update(state, {
  getTransporter: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getTransporterError = (state, action) => update(state, {
  getTransporter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getTransporterClear = (state, action) => update(state, {
  getTransporter: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const supplierRequest = (state, action) => update(state, {
  supplier: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const supplierSuccess = (state, action) => update(state, {
  supplier: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const supplierError = (state, action) => update(state, {
  supplier: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const supplierClear = (state, action) => update(state, {
  supplier: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getAgentNameRequest = (state, action) => update(state, {
  getAgentName: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getAgentNameSuccess = (state, action) => update(state, {
  getAgentName: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getAgentNameError = (state, action) => update(state, {
  getAgentName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getAgentNameClear = (state, action) => update(state, {
  getAgentName: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const leadTimeRequest = (state, action) => update(state, {
  leadTime: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const leadTimeSuccess = (state, action) => update(state, {
  leadTime: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const leadTimeError = (state, action) => update(state, {
  leadTime: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const leadTimeClear = (state, action) => update(state, {
  leadTime: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});




const vendorMrpRequest = (state, action) => update(state, {
  vendorMrp: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const vendorMrpSuccess = (state, action) => update(state, {
  vendorMrp: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const vendorMrpError = (state, action) => update(state, {
  vendorMrp: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const vendorMrpClear = (state, action) => update(state, {
  vendorMrp: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const sizeRequest = (state, action) => update(state, {
  size: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const sizeSuccess = (state, action) => update(state, {
  size: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const sizeError = (state, action) => update(state, {
  size: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const sizeClear = (state, action) => update(state, {
  size: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const colorRequest = (state, action) => update(state, {
  color: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const colorSuccess = (state, action) => update(state, {
  color: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const colorError = (state, action) => update(state, {
  color: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const colorClear = (state, action) => update(state, {
  color: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
//quantity


const quantityRequest = (state, action) => update(state, {
  quantity: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const quantitySuccess = (state, action) => update(state, {
  quantity: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const quantityError = (state, action) => update(state, {
  quantity: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const quantityClear = (state, action) => update(state, {
  quantity: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


//total
const totalRequest = (state, action) => update(state, {
  total: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const totalSuccess = (state, action) => update(state, {
  total: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const totalError = (state, action) => update(state, {
  total: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

// image 

const imageRequest = (state, action) => update(state, {
  image: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const imageSuccess = (state, action) => update(state, {
  image: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'image success' }
  }
});

const imageError = (state, action) => update(state, {
  image: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: '' }
  }
});

const imageClear = (state, action) => update(state, {
  image: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// otb

const otbRequest = (state, action) => update(state, {
  otb: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const otbSuccess = (state, action) => update(state, {
  otb: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const otbError = (state, action) => update(state, {
  otb: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const otbClear = (state, action) => update(state, {
  otb: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// proc analytics otb report

const analyticsOtbRequest = (state, action) => update(state, {
  analyticsOtb: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const analyticsOtbSuccess = (state, action) => update(state, {
  analyticsOtb: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const analyticsOtbError = (state, action) => update(state, {
  analyticsOtb: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const analyticsOtbClear = (state, action) => update(state, {
  analyticsOtb: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// lineitem
const lineItemRequest = (state, action) => update(state, {
  lineItem: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const lineItemSuccess = (state, action) => update(state, {
  lineItem: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const lineItemError = (state, action) => update(state, {
  lineItem: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const lineItemClear = (state, action) => update(state, {
  lineItem: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const markUpRequest = (state, action) => update(state, {
  markUp: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const markUpSuccess = (state, action) => update(state, {
  markUp: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const markUpError = (state, action) => update(state, {
  markUp: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const markUpClear = (state, action) => update(state, {
  markUp: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});





// LOADINDENT______________________________---

const loadIndentRequest = (state, action) => update(state, {
  loadIndent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const loadIndentSuccess = (state, action) => update(state, {
  loadIndent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const loadIndentError = (state, action) => update(state, {
  loadIndent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const loadIndentClear = (state, action) => update(state, {
  loadIndent: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


//purchaseTerm

const purchaseTermRequest = (state, action) => update(state, {
  purchaseTerm: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const purchaseTermSuccess = (state, action) => update(state, {
  purchaseTerm: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const purchaseTermError = (state, action) => update(state, {
  purchaseTerm: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const purchaseTermClear = (state, action) => update(state, {
  purchaseTerm: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getPurchaseTermRequest = (state, action) => update(state, {
  getPurchaseTerm: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getPurchaseTermSuccess = (state, action) => update(state, {
  getPurchaseTerm: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getPurchaseTermError = (state, action) => update(state, {
  getPurchaseTerm: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getPurchaseTermClear = (state, action) => update(state, {
  getPurchaseTerm: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
//
const piCreateRequest = (state, action) => update(state, {
  piCreate: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piCreateSuccess = (state, action) => update(state, {
  piCreate: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const piCreateError = (state, action) => update(state, {
  piCreate: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


// PIHISTORY
const piHistoryRequest = (state, action) => update(state, {
  piHistory: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piHistorySuccess = (state, action) => update(state, {
  piHistory: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const piHistoryError = (state, action) => update(state, {
  piHistory: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const piHistoryClear = (state, action) => update(state, {
  piHistory: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const piHistoryDataUpdate = (state, action) => update(state, {
  piHistory: {
    search: { $set: action.payload.search },
    filter: { $set: action.payload.filter }
  }
});

const piCreateClear = (state, action) => update(state, {
  piCreate: {
    // data: { $set: '' },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});




const piMatchRequest = (state, action) => update(state, {
  piMatch: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piMatchSuccess = (state, action) => update(state, {
  piMatch: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const piMatchError = (state, action) => update(state, {
  piMatch: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const piMatchClear = (state, action) => update(state, {
  piMatch: {
    // data: { $set: '' },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});



const piNotMatchRequest = (state, action) => update(state, {
  piNotMatch: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: action.payload }
  }
});

const piNotMatchSuccess = (state, action) => update(state, {
  piNotMatch: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const piNotMatchError = (state, action) => update(state, {
  piNotMatch: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const piNotMatchClear = (state, action) => update(state, {
  piNotMatch: {
    // data: { $set: '' },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});



const piUpdateRequest = (state, action) => update(state, {
  piUpdate: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piUpdateSuccess = (state, action) => update(state, {
  piUpdate: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const piUpdateError = (state, action) => update(state, {
  piUpdate: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const piUpdateClear = (state, action) => update(state, {
  piUpdate: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});




const poItemcodeRequest = (state, action) => update(state, {
  poItemcode: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const poItemcodeSuccess = (state, action) => update(state, {
  poItemcode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const poItemcodeError = (state, action) => update(state, {
  poItemcode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const poItemcodeClear = (state, action) => update(state, {
  poItemcode: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const displayModalBool = (state, action) => update(state, {
  displayModal: {
    data: { $set: action.payload }
  },
})



//  const piDownloadRequest = (state,action)=> update(state, {
//   piDownload: {
//     isLoading: { $set: true },
//     isError: { $set: false },
//     isSuccess: { $set: false },
//     message: { $set: '' }
//   }
// });

// const piDownloadSuccess = (state, action) => update(state, {
//   piDownload: {
//     data: { $set: action.payload },
//     isLoading: { $set: false },
//     isError: { $set: false },
//     isSuccess: { $set: true },
//     message: { $set: 'piDownload success' }
//   }
// });

// const  piDownloadError = (state, action) => update(state, {
//   piDownload: {
//     isLoading: { $set: false },
//     isSuccess: { $set: false },
//     isError: { $set: true },
//     message: { $set: action.payload }
//   }
// });


// const  piDownloadClear = (state, action) => update(state, {
//   piDownload: {
//     data: { $set: '' },
//     isLoading: { $set: false },
//     isSuccess: { $set: false }, 
//     isError: { $set: false },
//     message: { $set: '' }
//   }
//  });

const getPoDataRequest = (state, action) => update(state, {
  getPoData: {

    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getPoDataSuccess = (state, action) => update(state, {
  getPoData: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getPoDataError = (state, action) => update(state, {
  getPoData: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getPoDataClear = (state, action) => update(state, {
  getPoData: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const udfTypeRequest = (state, action) => update(state, {
  udfType: {

    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const udfTypeSuccess = (state, action) => update(state, {
  udfType: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const udfTypeError = (state, action) => update(state, {
  udfType: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const udfTypeClear = (state, action) => update(state, {
  udfType: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});



//get udf mapping

const getUdfMappingRequest = (state, action) => update(state, {
  getUdfMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getUdfMappingSuccess = (state, action) => update(state, {
  getUdfMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getUdfMappingError = (state, action) => update(state, {
  getUdfMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getUdfMappingClear = (state, action) => update(state, {
  getUdfMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//pohistory
const poHistoryDataUpdate = (state, action) => update(state, {
  poHistory: {
    search: { $set: action.payload.search },
    filter: { $set: action.payload.filter }
  }
});




const poHistoryRequest = (state, action) => update(state, {
  poHistory: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const poHistorySuccess = (state, action) => update(state, {
  poHistory: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const poHistoryError = (state, action) => update(state, {
  poHistory: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const poHistoryClear = (state, action) => update(state, {
  poHistory: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//poCreate
const poCreateRequest = (state, action) => update(state, {
  poCreate: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const poCreateSuccess = (state, action) => update(state, {
  poCreate: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const poCreateError = (state, action) => update(state, {
  poCreate: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const poCreateClear = (state, action) => update(state, {
  poCreate: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//poSize request
const poSizeRequest = (state, action) => update(state, {
  poSize: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const poSizeSuccess = (state, action) => update(state, {
  poSize: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const poSizeError = (state, action) => update(state, {
  poSize: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const poSizeClear = (state, action) => update(state, {
  poSize: {

    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const createUdfSettingRequest = (state, action) => update(state, {
  udfSetting: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createUdfSettingSuccess = (state, action) => update(state, {
  udfSetting: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const createUdfSettingError = (state, action) => update(state, {
  udfSetting: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const createUdfSettingClear = (state, action) => update(state, {
  udfSetting: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const activeCatDescRequest = (state, action) => update(state, {
  activeCatDesc: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const activeCatDescSuccess = (state, action) => update(state, {
  activeCatDesc: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const activeCatDescError = (state, action) => update(state, {
  activeCatDesc: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const activeCatDescClear = (state, action) => update(state, {
  activeCatDesc: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const activeUdfRequest = (state, action) => update(state, {
  activeUdf: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const activeUdfSuccess = (state, action) => update(state, {
  activeUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const activeUdfError = (state, action) => update(state, {
  activeUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const activeUdfClear = (state, action) => update(state, {
  activeUdf: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getCatDescUdfRequest = (state, action) => update(state, {
  getCatDescUdf: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getCatDescUdfSuccess = (state, action) => update(state, {
  getCatDescUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getCatDescUdfError = (state, action) => update(state, {
  getCatDescUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getCatDescUdfClear = (state, action) => update(state, {
  getCatDescUdf: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
// __________________ITEM UDF MAPPING _________________

const itemCatDescUdfRequest = (state, action) => update(state, {
  itemCatDescUdf: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const itemCatDescUdfSuccess = (state, action) => update(state, {
  itemCatDescUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const itemCatDescUdfError = (state, action) => update(state, {
  itemCatDescUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const itemCatDescUdfClear = (state, action) => update(state, {
  itemCatDescUdf: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const itemUdfMappingRequest = (state, action) => update(state, {
  itemUdfMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const itemUdfMappingSuccess = (state, action) => update(state, {
  itemUdfMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const itemUdfMappingError = (state, action) => update(state, {
  itemUdfMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const itemUdfMappingClear = (state, action) => update(state, {
  itemUdfMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const createItemUdfRequest = (state, action) => update(state, {
  createItemUdf: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createItemUdfSuccess = (state, action) => update(state, {
  createItemUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const createItemUdfError = (state, action) => update(state, {
  createItemUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const createItemUdfClear = (state, action) => update(state, {
  createItemUdf: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// ______________________________________UDF MAPPING GET DATA________________

const poUdfMappingRequest = (state, action) => update(state, {
  poUdfMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const poUdfMappingSuccess = (state, action) => update(state, {
  poUdfMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const poUdfMappingError = (state, action) => update(state, {
  poUdfMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const poUdfMappingClear = (state, action) => update(state, {
  poUdfMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// ________________proc mapping___________
const catDescDropdownRequest = (state, action) => update(state, {
  catDescDropdown: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const catDescDropdownSuccess = (state, action) => update(state, {
  catDescDropdown: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const catDescDropdownError = (state, action) => update(state, {
  catDescDropdown: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const catDescDropdownClear = (state, action) => update(state, {
  catDescDropdown: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


// ________________________UPDATE UDF MAPPING________________
const updateUdfMappingRequest = (state, action) => update(state, {
  updateUdfMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateUdfMappingSuccess = (state, action) => update(state, {
  updateUdfMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const updateUdfMappingError = (state, action) => update(state, {
  updateUdfMapping: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const updateUdfMappingClear = (state, action) => update(state, {
  updateUdfMapping: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//get item udf
const getItemUdfRequest = (state, action) => update(state, {
  getItemUdf: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getItemUdfSuccess = (state, action) => update(state, {
  getItemUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getItemUdfError = (state, action) => update(state, {
  getItemUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getItemUdfClear = (state, action) => update(state, {
  getItemUdf: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//update Item udf name
const updateItemUdfRequest = (state, action) => update(state, {
  updateItemUdf: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateItemUdfSuccess = (state, action) => update(state, {
  updateItemUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const updateItemUdfError = (state, action) => update(state, {
  updateItemUdf: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const updateItemUdfClear = (state, action) => update(state, {
  updateItemUdf: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// _______________UPDATE ITEM CAT/DESC___________________

const updateItemCatDescRequest = (state, action) => update(state, {
  updateItemCatDesc: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateItemCatDescSuccess = (state, action) => update(state, {
  updateItemCatDesc: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const updateItemCatDescError = (state, action) => update(state, {
  updateItemCatDesc: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const updateItemCatDescClear = (state, action) => update(state, {
  updateItemCatDesc: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});




const updateSizeDeptRequest = (state, action) => update(state, {
  updateSizeDept: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateSizeDeptSuccess = (state, action) => update(state, {
  updateSizeDept: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const updateSizeDeptError = (state, action) => update(state, {
  updateSizeDept: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const updateSizeDeptClear = (state, action) => update(state, {
  updateSizeDept: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//articleName

const articleNameRequest = (state, action) => update(state, {
  articleName: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const articleNameSuccess = (state, action) => update(state, {
  articleName: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const articleNameError = (state, action) => update(state, {
  articleName: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const articleNameClear = (state, action) => update(state, {
  articleName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

//cname
const cnameRequest = (state, action) => update(state, {
  cname: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const cnameSuccess = (state, action) => update(state, {
  cname: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const cnameError = (state, action) => update(state, {
  cname: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const cnameClear = (state, action) => update(state, {
  cname: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const piAddNewRequest = (state, action) => update(state, {
  piAddNew: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piAddNewSuccess = (state, action) => update(state, {
  piAddNew: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const piAddNewError = (state, action) => update(state, {
  piAddNew: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const piAddNewClear = (state, action) => update(state, {
  piAddNew: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const getItemDetailsRequest = (state, action) => update(state, {
  getItemDetails: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getItemDetailsSuccess = (state, action) => update(state, {
  getItemDetails: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getItemDetailsError = (state, action) => update(state, {
  getItemDetails: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getItemDetailsClear = (state, action) => update(state, {
  getItemDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});



const getItemDetailsValueRequest = (state, action) => update(state, {
  getItemDetailsValue: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getItemDetailsValueSuccess = (state, action) => update(state, {
  getItemDetailsValue: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getItemDetailsValueError = (state, action) => update(state, {
  getItemDetailsValue: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getItemDetailsValueClear = (state, action) => update(state, {
  getItemDetailsValue: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// _________________________PO ___________________________

const selectVendorRequest = (state, action) => update(state, {
  selectVendor: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const selectVendorSuccess = (state, action) => update(state, {
  selectVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const selectVendorError = (state, action) => update(state, {
  selectVendor: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const selectVendorClear = (state, action) => update(state, {
  selectVendor: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// _____________________DEPARTMENT SET BASED_____________________


const departmentSetBasedRequest = (state, action) => update(state, {
  departmentSetBased: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const departmentSetBasedSuccess = (state, action) => update(state, {
  departmentSetBased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const departmentSetBasedError = (state, action) => update(state, {
  departmentSetBased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const departmentSetBasedClear = (state, action) => update(state, {
  departmentSetBased: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const selectOrderNumberRequest = (state, action) => update(state, {
  selectOrderNumber: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const selectOrderNumberSuccess = (state, action) => update(state, {
  selectOrderNumber: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const selectOrderNumberError = (state, action) => update(state, {
  selectOrderNumber: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const selectOrderNumberClear = (state, action) => update(state, {
  selectOrderNumber: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const setBasedRequest = (state, action) => update(state, {
  setBased: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const setBasedSuccess = (state, action) => update(state, {
  setBased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const setBasedError = (state, action) => update(state, {
  setBased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const setBasedClear = (state, action) => update(state, {
  setBased: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const hsnCodeRequest = (state, action) => update(state, {
  hsnCode: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const hsnCodeSuccess = (state, action) => update(state, {
  hsnCode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const hsnCodeError = (state, action) => update(state, {
  hsnCode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const hsnCodeClear = (state, action) => update(state, {
  hsnCode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const multipleLineItemRequest = (state, action) => update(state, {
  multipleLineItem: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const multipleLineItemSuccess = (state, action) => update(state, {
  multipleLineItem: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const multipleLineItemError = (state, action) => update(state, {
  multipleLineItem: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const multipleLineItemClear = (state, action) => update(state, {
  multipleLineItem: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const procurementSiteRequest = (state, action) => update(state, {
  procurementSite: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const procurementSiteSuccess = (state, action) => update(state, {
  procurementSite: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const procurementSiteError = (state, action) => update(state, {
  procurementSite: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const procurementSiteClear = (state, action) => update(state, {
  procurementSite: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getPoItemCodeRequest = (state, action) => update(state, {
  getPoItemCode: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const getPoItemCodeSuccess = (state, action) => update(state, {
  getPoItemCode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getPoItemCodeError = (state, action) => update(state, {
  getPoItemCode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getPoItemCodeClear = (state, action) => update(state, {
  getPoItemCode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const poItemBarcodeRequest = (state, action) => update(state, {
  poItemBarcode: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const poItemBarcodeSuccess = (state, action) => update(state, {
  poItemBarcode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const poItemBarcodeError = (state, action) => update(state, {
  poItemBarcode: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const poItemBarcodeClear = (state, action) => update(state, {
  poItemBarcode: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const poRadioValidationRequest = (state, action) => update(state, {
  poRadioValidation: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const poRadioValidationSuccess = (state, action) => update(state, {
  poRadioValidation: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const poRadioValidationError = (state, action) => update(state, {
  poRadioValidation: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const poRadioValidationClear = (state, action) => update(state, {
  poRadioValidation: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getMultipleMarginRequest = (state, action) => update(state, {
  getMultipleMargin: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const getMultipleMarginSuccess = (state, action) => update(state, {
  getMultipleMargin: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getMultipleMarginError = (state, action) => update(state, {
  getMultipleMargin: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getMultipleMarginClear = (state, action) => update(state, {
  getMultipleMargin: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getAllCityRequest = (state, action) => update(state, {
  getAllCity: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


const getAllCitySuccess = (state, action) => update(state, {
  getAllCity: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getAllCityError = (state, action) => update(state, {
  getAllCity: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getAllCityClear = (state, action) => update(state, {
  getAllCity: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const discountRequest = (state, action) => update(state, {
  discount: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const discountSuccess = (state, action) => update(state, {
  discount: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const discountError = (state, action) => update(state, {
  discount: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const discountClear = (state, action) => update(state, {
  discount: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getLineItemRequest = (state, action) => update(state, {
  getLineItem: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const getLineItemSuccess = (state, action) => update(state, {
  getLineItem: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getLineItemError = (state, action) => update(state, {
  getLineItem: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getLineItemClear = (state, action) => update(state, {
  getLineItem: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const getPiLineItemRequest = (state, action) => update(state, {
  getPiLineItem: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const getPiLineItemSuccess = (state, action) => update(state, {
  getPiLineItem: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const getPiLineItemError = (state, action) => update(state, {
  getPiLineItem: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getPiLineItemClear = (state, action) => update(state, {
  getPiLineItem: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const poArticleRequest = (state, action) => update(state, {
  poArticle: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const poArticleSuccess = (state, action) => update(state, {
  poArticle: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const poArticleError = (state, action) => update(state, {
  poArticle: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const poArticleClear = (state, action) => update(state, {
  poArticle: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});




const multipleOtbRequest = (state, action) => update(state, {
  multipleOtb: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const multipleOtbSuccess = (state, action) => update(state, {
  multipleOtb: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const multipleOtbError = (state, action) => update(state, {
  multipleOtb: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const multipleOtbClear = (state, action) => update(state, {
  multipleOtb: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});



const gen_PurchaseIndentRequest = (state, action) => update(state, {
  gen_PurchaseIndent: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const gen_PurchaseIndentSuccess = (state, action) => update(state, {
  gen_PurchaseIndent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const gen_PurchaseIndentError = (state, action) => update(state, {
  gen_PurchaseIndent: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const gen_PurchaseIndentClear = (state, action) => update(state, {
  gen_PurchaseIndent: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const gen_PurchaseOrderRequest = (state, action) => update(state, {
  gen_PurchaseOrder: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const gen_PurchaseOrderSuccess = (state, action) => update(state, {
  gen_PurchaseOrder: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const gen_PurchaseOrderError = (state, action) => update(state, {
  gen_PurchaseOrder: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const gen_PurchaseOrderClear = (state, action) => update(state, {
  gen_PurchaseOrder: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const gen_SetBasedRequest = (state, action) => update(state, {
  gen_SetBased: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const gen_SetBasedSuccess = (state, action) => update(state, {
  gen_SetBased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const gen_SetBasedError = (state, action) => update(state, {
  gen_SetBased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const gen_SetBasedClear = (state, action) => update(state, {
  gen_SetBased: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const gen_IndentBasedRequest = (state, action) => update(state, {
  gen_IndentBased: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const gen_IndentBasedSuccess = (state, action) => update(state, {
  gen_IndentBased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const gen_IndentBasedError = (state, action) => update(state, {
  gen_IndentBased: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const gen_IndentBasedClear = (state, action) => update(state, {
  gen_IndentBased: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});
const po_approve_reject_Request = (state, action) => update(state, {
  po_approve_reject: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const po_approve_reject_Success = (state, action) => update(state, {
  po_approve_reject: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const po_approve_reject_Error = (state, action) => update(state, {
  po_approve_reject: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const po_approve_reject_Clear = (state, action) => update(state, {
  po_approve_reject: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const po_approve_retry_Request = (state, action) => update(state, {
  po_approve_retry: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }

});

const po_approve_retry_Success = (state, action) => update(state, {
  po_approve_retry: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});
const po_approve_retry_Error = (state, action) => update(state, {
  po_approve_retry: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const po_approve_retry_Clear = (state, action) => update(state, {
  po_approve_retry: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const pi_edit_data_Request = (state, action) => update(state, {
  pi_edit_data: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const pi_edit_data_Success = (state, action) => update(state, {
  pi_edit_data: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const pi_edit_data_Error = (state, action) => update(state, {
  pi_edit_data: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload }
  }
});

const pi_edit_data_Clear = (state, action) => update(state, {
  pi_edit_data: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editedPiSaveRequest = (state, action) => update(state, {
  save_edited_pi: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const editedPiSaveSuccess = (state, action) => update(state, {
  save_edited_pi: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const editedPiSaveError = (state, action) => update(state, {
  save_edited_pi: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload }
  }
});

const editedPiSaveClear = (state, action) => update(state, {
  save_edited_pi: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const poSaveDraftRequest = (state, action) => update(state, {
  save_draft_po: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const poSaveDraftSuccess = (state, action) => update(state, {
  save_draft_po: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});

const poSaveDraftError = (state, action) => update(state, {
  save_draft_po: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});

const poSaveDraftClear = (state, action) => update(state, {
  save_draft_po: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piSaveDraftRequest = (state, action) => update(state, {
  save_draft_pi: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piSaveDraftSuccess = (state, action) => update(state, {
  save_draft_pi: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});

const piSaveDraftError = (state, action) => update(state, {
  save_draft_pi: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});

const piSaveDraftClear = (state, action) => update(state, {
  save_draft_pi: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' },
    data: { $set: '' }
  }
});
// function setOrderOrIndent(action) {
//   if(draftType == "PO_DRAFT"){
//     return 
//   }
//   console.log(action)
// }
const getDraftRequest = (state, action) => update(state, {
  get_draft_data: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' },
    indentNo: { $set: action.payload.orderNo },
    // orderNo: setOrderOrIndent(action.payload)
  }
});

const getDraftSuccess = (state, action) => update(state, {
  get_draft_data: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});

const getDraftError = (state, action) => update(state, {
  get_draft_data: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});

const getDraftClear = (state, action) => update(state, {
  get_draft_data: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' },
    data: { $set: '' }

  }
});

const poEditRequest = (state, action) => update(state, {
  po_edit_data: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const poEditSuccess = (state, action) => update(state, {
  po_edit_data: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const poEditError = (state, action) => update(state, {
  po_edit_data: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload }
  }
});

const poEditClear = (state, action) => update(state, {
  po_edit_data: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const save_edited_po_Request = (state, action) => update(state, {
  save_edited_po: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const save_edited_po_Success = (state, action) => update(state, {
  save_edited_po: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const save_edited_po_Error = (state, action) => update(state, {
  save_edited_po: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload }
  }
});

const save_edited_po_Clear = (state, action) => update(state, {
  save_edited_po: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' },
    data: { $set: '' },
  }
});

const discardPoPiDataRequest = (state, action) => update(state, {
  discardPoPiData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const discardPoPiDataSuccess = (state, action) => update(state, {
  discardPoPiData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const discardPoPiDataError = (state, action) => update(state, {
  discardPoPiData: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const discardPoPiDataClear = (state, action) => update(state, {
  discardPoPiData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const view_Images_Request = (state, action) => update(state, {
  viewImages: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const view_Images_Success = (state, action) => update(state, {
  viewImages: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const view_Images_Error = (state, action) => update(state, {
  viewImages: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const view_Images_Clear = (state, action) => update(state, {
  viewImages: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

//proc dash bottom

const piDashBottomRequest = (state, action) => update(state, {
  piDashBottom: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piDashBottomSuccess = (state, action) => update(state, {
  piDashBottom: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const piDashBottomError = (state, action) => update(state, {
  piDashBottom: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const piDashBottomClear = (state, action) => update(state, {
  piDashBottom: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

// PO DASHBOARD EMAIL

const emailDashboardSendRequest = (state, action) => update(state, {
  emailDashboardSend: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const emailDashboardSendSuccess = (state, action) => update(state, {
  emailDashboardSend: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const emailDashboardSendError = (state, action) => update(state, {
  emailDashboardSend: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const emailDashboardSendClear = (state, action) => update(state, {
  emailDashboardSend: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

// pi dash hierarchy 

const piDashHierarchyRequest = (state, action) => update(state, {
  piDashHierarchy: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piDashHierarchySuccess = (state, action) => update(state, {
  piDashHierarchy: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const piDashHierarchyError = (state, action) => update(state, {
  piDashHierarchy: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const piDashHierarchyClear = (state, action) => update(state, {
  piDashHierarchy: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

//pi po dash site 

const piPoDashSiteRequest = (state, action) => update(state, {
  piPoDashSite: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const piPoDashSiteSuccess = (state, action) => update(state, {
  piPoDashSite: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const piPoDashSiteError = (state, action) => update(state, {
  piPoDashSite: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const piPoDashSiteClear = (state, action) => update(state, {
  piPoDashSite: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

//pi Image url request

const piImageUrlRequest = (state, action) => update(state, {
  piImageUrl: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: 'Request' }
  }
});

const piImageUrlSuccess = (state, action) => update(state, {
  piImageUrl: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'success' },
    data: { $set: action.payload }
  }
});
const piImageUrlError = (state, action) => update(state, {
  piImageUrl: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: 'error' },
    data: { $set: action.payload }
  }
});
const piImageUrlClear = (state, action) => update(state, {
  piImageUrl: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});


//Sl Data 

const getSlRequest = (state, action) => update(state, {
  slData: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getSlSuccess = (state, action) => update(state, {
  slData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const getSlError = (state, action) => update(state, {
  slData: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const getSlClear = (state, action) => update(state, {
  slData: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateItemUdfMappingRequest = (state, action) => update(state, {
  updateItemUdfMapping: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateItemUdfMappingSuccess = (state, action) => update(state, {
  updateItemUdfMapping: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const updateItemUdfMappingError = (state, action) => update(state, {
  updateItemUdfMapping: {
    isLoading: { $set: false },
    isError: { $set: true },
    isSuccess: { $set: false },
    message: { $set: action.payload },
    data: { $set: action.payload }
  }
});
const updateItemUdfMappingClear = (state, action) => update(state, {
  updateItemUdfMapping: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

export default handleActions({
  
  // Dashboard Excel download
  [constants.EXPORT_DASHBOARD_EXCEL_REQUEST]: exportDashboardExcelRequest,
  [constants.EXPORT_DASHBOARD_EXCEL_SUCCESS]: exportDashboardExcelSuccess,
  [constants.EXPORT_DASHBOARD_EXCEL_CLEAR]: exportDashboardExcelClear,
  [constants.EXPORT_DASHBOARD_EXCEL_ERROR]: exportDashboardExcelError,
  
  [constants.MAPPING_EXCEL_UPLOAD_REQUEST]: mappingExcelUploadRequest,
  [constants.MAPPING_EXCEL_UPLOAD_SUCCESS]: mappingExcelUploadSuccess,
  [constants.MAPPING_EXCEL_UPLOAD_ERROR]: mappingExcelUploadError,
  [constants.MAPPING_EXCEL_UPLOAD_CLEAR]: mappingExcelUploadClear,

  [constants.MAPPING_EXCEL_STATUS_REQUEST]: mappingExcelStatusRequest,
  [constants.MAPPING_EXCEL_STATUS_SUCCESS]: mappingExcelStatusSuccess,
  [constants.MAPPING_EXCEL_STATUS_ERROR]: mappingExcelStatusError,
  [constants.MAPPING_EXCEL_STATUS_CLEAR]: mappingExcelStatusClear,

  [constants.MAPPING_EXCEL_EXPORT_REQUEST]: mappingExcelExportRequest,
  [constants.MAPPING_EXCEL_EXPORT_SUCCESS]: mappingExcelExportSuccess,
  [constants.MAPPING_EXCEL_EXPORT_ERROR]: mappingExcelExportError,
  [constants.MAPPING_EXCEL_EXPORT_CLEAR]: mappingExcelExportClear,

  [constants.DIVISION_SECTION_DEPARTMENT_REQUEST]: divisionSectionDepartmentRequest,
  [constants.DIVISION_SECTION_DEPARTMENT_SUCCESS]: divisionSectionDepartmentSuccess,
  [constants.DIVISION_SECTION_DEPARTMENT_ERROR]: divisionSectionDepartmentError,
  [constants.DIVISION_SECTION_DEPARTMENT_CLEAR]: divisionSectionDepartmentClear,
  [constants.TRANSPORTER_REQUEST]: transporterRequest,
  [constants.TRANSPORTER_SUCCESS]: transporterSuccess,
  [constants.TRANSPORTER_ERROR]: transporterError,

  [constants.GET_TRANSPORTER_REQUEST]: getTransporterRequest,
  [constants.GET_TRANSPORTER_SUCCESS]: getTransporterSuccess,
  [constants.GET_TRANSPORTER_ERROR]: getTransporterError,
  [constants.GET_TRANSPORTER_CLEAR]: getTransporterClear,
  [constants.SUPPLIER_REQUEST]: supplierRequest,
  [constants.SUPPLIER_SUCCESS]: supplierSuccess,
  [constants.SUPPLIER_ERROR]: supplierError,
  [constants.SUPPLIER_CLEAR]: supplierClear,
  [constants.GET_AGENT_NAME_REQUEST]: getAgentNameRequest,
  [constants.GET_AGENT_NAME_SUCCESS]: getAgentNameSuccess,
  [constants.GET_AGENT_NAME_ERROR]: getAgentNameError,
  [constants.GET_AGENT_NAME_CLEAR]: getAgentNameClear,
  [constants.LEAD_TIME_REQUEST]: leadTimeRequest,
  [constants.LEAD_TIME_SUCCESS]: leadTimeSuccess,
  [constants.LEAD_TIME_ERROR]: leadTimeError,
  [constants.LEAD_TIME_CLEAR]: leadTimeClear,

  [constants.PURCHASETERM_REQUEST]: purchaseTermRequest,
  [constants.PURCHASETERM_SUCCESS]: purchaseTermSuccess,
  [constants.PURCHASETERM_ERROR]: purchaseTermError,
  [constants.PURCHASETERM_CLEAR]: purchaseTermClear,


  [constants.GET_PURCHASETERM_REQUEST]: getPurchaseTermRequest,
  [constants.GET_PURCHASETERM_SUCCESS]: getPurchaseTermSuccess,

  [constants.GET_PURCHASETERM_ERROR]: getPurchaseTermError,
  [constants.GET_PURCHASETERM_CLEAR]: getPurchaseTermClear,

  [constants.QUANTITY_REQUEST]: quantityRequest,
  [constants.QUANTITY_SUCCESS]: quantitySuccess,
  [constants.QUANTITY_ERROR]: quantityError,
  [constants.QUANTITY_CLEAR]: quantityClear,


  [constants.TOTAL_REQUEST]: totalRequest,
  [constants.TOTAL_SUCCESS]: totalSuccess,
  [constants.TOTAL_ERROR]: totalError,

  [constants.VENDOR_MRP_REQUEST]: vendorMrpRequest,
  [constants.VENDOR_MRP_SUCCESS]: vendorMrpSuccess,
  [constants.VENDOR_MRP_ERROR]: vendorMrpError,
  [constants.VENDOR_MRP_CLEAR]: vendorMrpClear,

  [constants.SIZE_REQUEST]: sizeRequest,
  [constants.SIZE_SUCCESS]: sizeSuccess,
  [constants.SIZE_ERROR]: sizeError,
  [constants.SIZE_CLEAR]: sizeClear,

  [constants.COLOR_REQUEST]: colorRequest,
  [constants.COLOR_SUCCESS]: colorSuccess,
  [constants.COLOR_ERROR]: colorError,
  [constants.COLOR_CLEAR]: colorClear,

  [constants.IMAGE_REQUEST]: imageRequest,
  [constants.IMAGE_SUCCESS]: imageSuccess,
  [constants.IMAGE_ERROR]: imageError,
  [constants.IMAGE_CLEAR]: imageClear,

  [constants.OTB_REQUEST]: otbRequest,
  [constants.OTB_SUCCESS]: otbSuccess,
  [constants.OTB_ERROR]: otbError,
  [constants.OTB_CLEAR]: otbClear,

  [constants.ANALYTICS_OTB_REQUEST]: analyticsOtbRequest,
  [constants.ANALYTICS_OTB_SUCCESS]: analyticsOtbSuccess,
  [constants.ANALYTICS_OTB_ERROR]: analyticsOtbError,
  [constants.ANALYTICS_OTB_CLEAR]: analyticsOtbClear,

  [constants.LINEITEM_REQUEST]: lineItemRequest,
  [constants.LINEITEM_SUCCESS]: lineItemSuccess,
  [constants.LINEITEM_ERROR]: lineItemError,
  [constants.LINEITEM_CLEAR]: lineItemClear,

  [constants.MARKUP_REQUEST]: markUpRequest,
  [constants.MARKUP_SUCCESS]: markUpSuccess,
  [constants.MARKUP_ERROR]: markUpError,
  [constants.MARKUP_CLEAR]: markUpClear,



  [constants.PICREATE_REQUEST]: piCreateRequest,
  [constants.PICREATE_SUCCESS]: piCreateSuccess,
  [constants.PICREATE_ERROR]: piCreateError,
  [constants.PICREATE_CLEAR]: piCreateClear,



  // PIHistory
  [constants.PIHISTORY_REQUEST]: piHistoryRequest,
  [constants.PIHISTORY_SUCCESS]: piHistorySuccess,
  [constants.PIHISTORY_ERROR]: piHistoryError,
  [constants.PIHISTORY_CLEAR]: piHistoryClear,
  [constants.PIHISTORY_DATA_UPDATE]: piHistoryDataUpdate,

  [constants.LOAD_INDENT_REQUEST]: loadIndentRequest,
  [constants.LOAD_INDENT_SUCCESS]: loadIndentSuccess,
  [constants.LOAD_INDENT_ERROR]: loadIndentError,
  [constants.LOAD_INDENT_CLEAR]: loadIndentClear,

  [constants.ARTICLEPO_REQUEST]: articlePoRequest,
  [constants.ARTICLEPO_SUCCESS]: articlePoSuccess,
  [constants.ARTICLEPO_ERROR]: articlePoError,
  [constants.ARTICLEPO_CLEAR]: articlePoClear,

  [constants.PIUPDATE_REQUEST]: piUpdateRequest,
  [constants.PIUPDATE_SUCCESS]: piUpdateSuccess,
  [constants.PIUPDATE_ERROR]: piUpdateError,
  [constants.PIUPDATE_CLEAR]: piUpdateClear,


  // [constants.PIDOWNLOAD_REQUEST]:  piDownloadRequest,
  // [constants.PIDOWNLOAD_SUCCESS]:  piDownloadSuccess,
  // [constants.PIDOWNLOAD_ERROR]:  piDownLoadError,
  // [constants.PIDOWNLOAD_CLEAR]:  piDownloadClear,

  [constants.POITEMCODE_REQUEST]: poItemcodeRequest,
  [constants.POITEMCODE_SUCCESS]: poItemcodeSuccess,
  [constants.POITEMCODE_ERROR]: poItemcodeError,
  [constants.POITEMCODE_CLEAR]: poItemcodeClear,

  [constants.GET_PODATA_REQUEST]: getPoDataRequest,
  [constants.GET_PODATA_SUCCESS]: getPoDataSuccess,
  [constants.GET_PODATA_ERROR]: getPoDataError,
  [constants.GET_PODATA_CLEAR]: getPoDataClear,
  [constants.UDF_TYPE_REQUEST]: udfTypeRequest,
  [constants.UDF_TYPE_SUCCESS]: udfTypeSuccess,
  [constants.UDF_TYPE_ERROR]: udfTypeError,
  [constants.UDF_TYPE_CLEAR]: udfTypeClear,

  [constants.GET_UDF_MAPPING_REQUEST]: getUdfMappingRequest,
  [constants.GET_UDF_MAPPING_SUCCESS]: getUdfMappingSuccess,
  [constants.GET_UDF_MAPPING_ERROR]: getUdfMappingError,
  [constants.GET_UDF_MAPPING_CLEAR]: getUdfMappingClear,

  [constants.PO_HISTORY_REQUEST]: poHistoryRequest,
  [constants.PO_HISTORY_SUCCESS]: poHistorySuccess,
  [constants.PO_HISTORY_ERROR]: poHistoryError,
  [constants.PO_HISTORY_CLEAR]: poHistoryClear,
  [constants.PO_HISTORY_DATA_UPDATE]: poHistoryDataUpdate,

  [constants.PO_CREATE_REQUEST]: poCreateRequest,
  [constants.PO_CREATE_SUCCESS]: poCreateSuccess,
  [constants.PO_CREATE_ERROR]: poCreateError,
  [constants.PO_CREATE_CLEAR]: poCreateClear,


  [constants.PO_SIZE_REQUEST]: poSizeRequest,
  [constants.PO_SIZE_SUCCESS]: poSizeSuccess,
  [constants.PO_SIZE_ERROR]: poSizeError,
  [constants.PO_SIZE_CLEAR]: poSizeClear,

  [constants.CREATE_UDF_SETTING_REQUEST]: createUdfSettingRequest,
  [constants.CREATE_UDF_SETTING_SUCCESS]: createUdfSettingSuccess,
  [constants.CREATE_UDF_SETTING_ERROR]: createUdfSettingError,
  [constants.CREATE_UDF_SETTING_CLEAR]: createUdfSettingClear,


  [constants.ACTIVE_CAT_DESC_REQUEST]: activeCatDescRequest,
  [constants.ACTIVE_CAT_DESC_SUCCESS]: activeCatDescSuccess,
  [constants.ACTIVE_CAT_DESC_ERROR]: activeCatDescError,
  [constants.ACTIVE_CAT_DESC_CLEAR]: activeCatDescClear,

  [constants.ACTIVE_UDF_REQUEST]: activeUdfRequest,
  [constants.ACTIVE_UDF_SUCCESS]: activeUdfSuccess,
  [constants.ACTIVE_UDF_ERROR]: activeUdfError,
  [constants.ACTIVE_UDF_CLEAR]: activeUdfClear,


  [constants.GET_CATDESC_UDF_REQUEST]: getCatDescUdfRequest,
  [constants.GET_CATDESC_UDF_SUCCESS]: getCatDescUdfSuccess,
  [constants.GET_CATDESC_UDF_ERROR]: getCatDescUdfError,
  [constants.GET_CATDESC_UDF_CLEAR]: getCatDescUdfClear,


  [constants.ITEM_CATDESC_UDF_REQUEST]: itemCatDescUdfRequest,
  [constants.ITEM_CATDESC_UDF_SUCCESS]: itemCatDescUdfSuccess,
  [constants.ITEM_CATDESC_UDF_ERROR]: itemCatDescUdfError,
  [constants.ITEM_CATDESC_UDF_CLEAR]: itemCatDescUdfClear,

  [constants.ITEM_UDF_MAPPING_REQUEST]: itemUdfMappingRequest,
  [constants.ITEM_UDF_MAPPING_SUCCESS]: itemUdfMappingSuccess,
  [constants.ITEM_UDF_MAPPING_ERROR]: itemUdfMappingError,
  [constants.ITEM_UDF_MAPPING_CLEAR]: itemUdfMappingClear,


  [constants.CREATE_ITEM_UDF_REQUEST]: createItemUdfRequest,
  [constants.CREATE_ITEM_UDF_SUCCESS]: createItemUdfSuccess,
  [constants.CREATE_ITEM_UDF_ERROR]: createItemUdfError,
  [constants.CREATE_ITEM_UDF_CLEAR]: createItemUdfClear,

  [constants.PO_UDF_MAPPING_REQUEST]: poUdfMappingRequest,
  [constants.PO_UDF_MAPPING_SUCCESS]: poUdfMappingSuccess,
  [constants.PO_UDF_MAPPING_ERROR]: poUdfMappingError,
  [constants.PO_UDF_MAPPING_CLEAR]: poUdfMappingClear,

  [constants.UPDATE_UDF_MAPPING_REQUEST]: updateUdfMappingRequest,
  [constants.UPDATE_UDF_MAPPING_SUCCESS]: updateUdfMappingSuccess,
  [constants.UPDATE_UDF_MAPPING_ERROR]: updateUdfMappingError,
  [constants.UPDATE_UDF_MAPPING_CLEAR]: updateUdfMappingClear,

  [constants.UPDATE_ITEM_UDF_REQUEST]: updateItemUdfRequest,
  [constants.UPDATE_ITEM_UDF_SUCCESS]: updateItemUdfSuccess,
  [constants.UPDATE_ITEM_UDF_ERROR]: updateItemUdfError,
  [constants.UPDATE_ITEM_UDF_CLEAR]: updateItemUdfClear,

  // ________________________UPDATE ITEM CAT/DESC__________________________

  [constants.UPDATE_ITEM_CAT_DESC_REQUEST]: updateItemCatDescRequest,
  [constants.UPDATE_ITEM_CAT_DESC_SUCCESS]: updateItemCatDescSuccess,
  [constants.UPDATE_ITEM_CAT_DESC_ERROR]: updateItemCatDescError,
  [constants.UPDATE_ITEM_CAT_DESC_CLEAR]: updateItemCatDescClear,

  [constants.GET_ITEM_UDF_REQUEST]: getItemUdfRequest,
  [constants.GET_ITEM_UDF_SUCCESS]: getItemUdfSuccess,
  [constants.GET_ITEM_UDF_ERROR]: getItemUdfError,
  [constants.GET_ITEM_UDF_CLEAR]: getItemUdfClear,

  [constants.UPDATE_SIZE_DEPT_REQUEST]: updateSizeDeptRequest,
  [constants.UPDATE_SIZE_DEPT_SUCCESS]: updateSizeDeptSuccess,
  [constants.UPDATE_SIZE_DEPT_ERROR]: updateSizeDeptError,
  [constants.UPDATE_SIZE_DEPT_CLEAR]: updateSizeDeptClear,

  [constants.ARTICLE_NAME_REQUEST]: articleNameRequest,
  [constants.ARTICLE_NAME_SUCCESS]: articleNameSuccess,
  [constants.ARTICLE_NAME_ERROR]: articleNameError,
  [constants.ARTICLE_NAME_CLEAR]: articleNameClear,

  [constants.CNAME_REQUEST]: cnameRequest,
  [constants.CNAME_SUCCESS]: cnameSuccess,
  [constants.CNAME_ERROR]: cnameError,
  [constants.CNAME_CLEAR]: cnameClear,

  [constants.PI_ADD_NEW_REQUEST]: piAddNewRequest,
  [constants.PI_ADD_NEW_SUCCESS]: piAddNewSuccess,
  [constants.PI_ADD_NEW_ERROR]: piAddNewError,
  [constants.PI_ADD_NEW_CLEAR]: piAddNewClear,

  [constants.MARGIN_RULE_REQUEST]: marginRuleRequest,
  [constants.MARGIN_RULE_SUCCESS]: marginRuleSuccess,
  [constants.MARGIN_RULE_ERROR]: marginRuleError,
  [constants.MARGIN_RULE_CLEAR]: marginRuleClear,

  [constants.GET_ITEM_DETAILS_REQUEST]: getItemDetailsRequest,
  [constants.GET_ITEM_DETAILS_SUCCESS]: getItemDetailsSuccess,
  [constants.GET_ITEM_DETAILS_ERROR]: getItemDetailsError,
  [constants.GET_ITEM_DETAILS_CLEAR]: getItemDetailsClear,

  [constants.GET_ITEM_DETAILS_VALUE_REQUEST]: getItemDetailsValueRequest,
  [constants.GET_ITEM_DETAILS_VALUE_SUCCESS]: getItemDetailsValueSuccess,
  [constants.GET_ITEM_DETAILS_VALUE_ERROR]: getItemDetailsValueError,
  [constants.GET_ITEM_DETAILS_VALUE_CLEAR]: getItemDetailsValueClear,

  [constants.SELECT_VENDOR_REQUEST]: selectVendorRequest,
  [constants.SELECT_VENDOR_SUCCESS]: selectVendorSuccess,
  [constants.SELECT_VENDOR_ERROR]: selectVendorError,
  [constants.SELECT_VENDOR_CLEAR]: selectVendorClear,

  [constants.DEPARTMENT_SET_BASED_REQUEST]: departmentSetBasedRequest,
  [constants.DEPARTMENT_SET_BASED_SUCCESS]: departmentSetBasedSuccess,
  [constants.DEPARTMENT_SET_BASED_ERROR]: departmentSetBasedError,
  [constants.DEPARTMENT_SET_BASED_CLEAR]: departmentSetBasedClear,

  [constants.SELECT_ORDERNUMBER_REQUEST]: selectOrderNumberRequest,
  [constants.SELECT_ORDERNUMBER_SUCCESS]: selectOrderNumberSuccess,
  [constants.SELECT_ORDERNUMBER_ERROR]: selectOrderNumberError,
  [constants.SELECT_ORDERNUMBER_CLEAR]: selectOrderNumberClear,

  [constants.SET_BASED_REQUEST]: setBasedRequest,
  [constants.SET_BASED_SUCCESS]: setBasedSuccess,
  [constants.SET_BASED_ERROR]: setBasedError,
  [constants.SET_BASED_CLEAR]: setBasedClear,

  [constants.HSN_CODE_REQUEST]: hsnCodeRequest,
  [constants.HSN_CODE_SUCCESS]: hsnCodeSuccess,
  [constants.HSN_CODE_ERROR]: hsnCodeError,
  [constants.HSN_CODE_CLEAR]: hsnCodeClear,

  [constants.MULTIPLE_LINEITEM_REQUEST]: multipleLineItemRequest,
  [constants.MULTIPLE_LINEITEM_SUCCESS]: multipleLineItemSuccess,
  [constants.MULTIPLE_LINEITEM_ERROR]: multipleLineItemError,
  [constants.MULTIPLE_LINEITEM_CLEAR]: multipleLineItemClear,

  [constants.PROCUREMENT_SITE_REQUEST]: procurementSiteRequest,
  [constants.PROCUREMENT_SITE_SUCCESS]: procurementSiteSuccess,
  [constants.PROCUREMENT_SITE_ERROR]: procurementSiteError,
  [constants.PROCUREMENT_SITE_CLEAR]: procurementSiteClear,


  [constants.GET_PO_ITEMCODE_REQUEST]: getPoItemCodeRequest,
  [constants.GET_PO_ITEMCODE_SUCCESS]: getPoItemCodeSuccess,
  [constants.GET_PO_ITEMCODE_ERROR]: getPoItemCodeError,
  [constants.GET_PO_ITEMCODE_CLEAR]: getPoItemCodeClear,

  [constants.PO_ITEM_BARCODE_REQUEST]: poItemBarcodeRequest,
  [constants.PO_ITEM_BARCODE_SUCCESS]: poItemBarcodeSuccess,
  [constants.PO_ITEM_BARCODE_ERROR]: poItemBarcodeError,
  [constants.PO_ITEM_BARCODE_CLEAR]: poItemBarcodeClear,

  [constants.PO_RADIO_VALIDATION_REQUEST]: poRadioValidationRequest,
  [constants.PO_RADIO_VALIDATION_SUCCESS]: poRadioValidationSuccess,
  [constants.PO_RADIO_VALIDATION_ERROR]: poRadioValidationError,
  [constants.PO_RADIO_VALIDATION_CLEAR]: poRadioValidationClear,

  [constants.FMCG_GET_HISTORY_REQUEST]: fmcgGetHistoryRequest,
  [constants.FMCG_GET_HISTORY_SUCCESS]: fmcgGetHistorySuccess,
  [constants.FMCG_GET_HISTORY_ERROR]: fmcgGetHistoryError,
  [constants.FMCG_GET_HISTORY_CLEAR]: fmcgGetHistoryClear,

  [constants.GET_MULTIPLE_MARGIN_REQUEST]: getMultipleMarginRequest,
  [constants.GET_MULTIPLE_MARGIN_SUCCESS]: getMultipleMarginSuccess,
  [constants.GET_MULTIPLE_MARGIN_ERROR]: getMultipleMarginError,
  [constants.GET_MULTIPLE_MARGIN_CLEAR]: getMultipleMarginClear,

  [constants.GET_ALL_CITY_REQUEST]: getAllCityRequest,
  [constants.GET_ALL_CITY_SUCCESS]: getAllCitySuccess,
  [constants.GET_ALL_CITY_ERROR]: getAllCityError,
  [constants.GET_ALL_CITY_CLEAR]: getAllCityClear,

  [constants.PI_ARTICLE_REQUEST]: piArticleRequest,
  [constants.PI_ARTICLE_SUCCESS]: piArticleSuccess,
  [constants.PI_ARTICLE_ERROR]: piArticleError,
  [constants.PI_ARTICLE_CLEAR]: piArticleClear,

  [constants.DISCOUNT_REQUEST]: discountRequest,
  [constants.DISCOUNT_SUCCESS]: discountSuccess,
  [constants.DISCOUNT_ERROR]: discountError,
  [constants.DISCOUNT_CLEAR]: discountClear,

  [constants.GET_LINE_ITEM_REQUEST]: getLineItemRequest,
  [constants.GET_LINE_ITEM_SUCCESS]: getLineItemSuccess,
  [constants.GET_LINE_ITEM_ERROR]: getLineItemError,
  [constants.GET_LINE_ITEM_CLEAR]: getLineItemClear,

  [constants.GET_PI_LINE_ITEM_REQUEST]: getPiLineItemRequest,
  [constants.GET_PI_LINE_ITEM_SUCCESS]: getPiLineItemSuccess,
  [constants.GET_PI_LINE_ITEM_ERROR]: getPiLineItemError,
  [constants.GET_PI_LINE_ITEM_CLEAR]: getPiLineItemClear,

  [constants.PO_ARTICLE_REQUEST]: poArticleRequest,
  [constants.PO_ARTICLE_SUCCESS]: poArticleSuccess,
  [constants.PO_ARTICLE_ERROR]: poArticleError,
  [constants.PO_ARTICLE_CLEAR]: poArticleClear,

  [constants.MULTIPLE_OTB_REQUEST]: multipleOtbRequest,
  [constants.MULTIPLE_OTB_SUCCESS]: multipleOtbSuccess,
  [constants.MULTIPLE_OTB_ERROR]: multipleOtbError,
  [constants.MULTIPLE_OTB_CLEAR]: multipleOtbClear,

  [constants.GEN_PURCHASE_ORDER_REQUEST]: gen_PurchaseOrderRequest,
  [constants.GEN_PURCHASE_ORDER_SUCCESS]: gen_PurchaseOrderSuccess,
  [constants.GEN_PURCHASE_ORDER_ERROR]: gen_PurchaseOrderError,
  [constants.GEN_PURCHASE_ORDER_CLEAR]: gen_PurchaseOrderClear,

  [constants.GEN_PURCHASE_INDENT_REQUEST]: gen_PurchaseIndentRequest,
  [constants.GEN_PURCHASE_INDENT_SUCCESS]: gen_PurchaseIndentSuccess,
  [constants.GEN_PURCHASE_INDENT_ERROR]: gen_PurchaseIndentError,
  [constants.GEN_PURCHASE_INDENT_CLEAR]: gen_PurchaseIndentClear,

  [constants.GEN_INDENT_BASED_REQUEST]: gen_IndentBasedRequest,
  [constants.GEN_INDENT_BASED_SUCCESS]: gen_IndentBasedSuccess,
  [constants.GEN_INDENT_BASED_ERROR]: gen_IndentBasedError,
  [constants.GEN_INDENT_BASED_CLEAR]: gen_IndentBasedClear,


  [constants.GEN_SET_BASED_REQUEST]: gen_SetBasedRequest,
  [constants.GEN_SET_BASED_SUCCESS]: gen_SetBasedSuccess,
  [constants.GEN_SET_BASED_ERROR]: gen_SetBasedError,
  [constants.GEN_SET_BASED_CLEAR]: gen_SetBasedClear,

  [constants.PO_APPROVE_REJECT_REQUEST]: po_approve_reject_Request,
  [constants.PO_APPROVE_REJECT_SUCCESS]: po_approve_reject_Success,
  [constants.PO_APPROVE_REJECT_ERROR]: po_approve_reject_Error,
  [constants.PO_APPROVE_REJECT_CLEAR]: po_approve_reject_Clear,

  [constants.PO_APPROVE_RETRY_REQUEST]: po_approve_retry_Request,
  [constants.PO_APPROVE_RETRY_SUCCESS]: po_approve_retry_Success,
  [constants.PO_APPROVE_RETRY_ERROR]: po_approve_retry_Error,
  [constants.PO_APPROVE_RETRY_CLEAR]: po_approve_retry_Clear,


  // edit pi
  [constants.PI_EDIT_REQUEST]: pi_edit_data_Request,
  [constants.PI_EDIT_SUCCESS]: pi_edit_data_Success,
  [constants.PI_EDIT_ERROR]: pi_edit_data_Error,
  [constants.PI_EDIT_CLEAR]: pi_edit_data_Clear,
  // save edit pi
  [constants.EDITED_PI_SAVE_REQUEST]: editedPiSaveRequest,
  [constants.EDITED_PI_SAVE_SUCCESS]: editedPiSaveSuccess,
  [constants.EDITED_PI_SAVE_ERROR]: editedPiSaveError,
  [constants.EDITED_PI_SAVE_CLEAR]: editedPiSaveClear,

  //save draft po
  [constants.PO_SAVE_DRAFT_REQUEST]: poSaveDraftRequest,
  [constants.PO_SAVE_DRAFT_SUCCESS]: poSaveDraftSuccess,
  [constants.PO_SAVE_DRAFT_ERROR]: poSaveDraftError,
  [constants.PO_SAVE_DRAFT_CLEAR]: poSaveDraftClear,

  //save draft pi
  [constants.PI_SAVE_DRAFT_REQUEST]: piSaveDraftRequest,
  [constants.PI_SAVE_DRAFT_SUCCESS]: piSaveDraftSuccess,
  [constants.PI_SAVE_DRAFT_ERROR]: piSaveDraftError,
  [constants.PI_SAVE_DRAFT_CLEAR]: piSaveDraftClear,

  //saved draft data
  [constants.GET_DRAFT_REQUEST]: getDraftRequest,
  [constants.GET_DRAFT_SUCCESS]: getDraftSuccess,
  [constants.GET_DRAFT_ERROR]: getDraftError,
  [constants.GET_DRAFT_CLEAR]: getDraftClear,

  // edit pi
  [constants.PO_EDIT_REQUEST]: poEditRequest,
  [constants.PO_EDIT_SUCCESS]: poEditSuccess,
  [constants.PO_EDIT_ERROR]: poEditError,
  [constants.PO_EDIT_CLEAR]: poEditClear,
  // save edit pi
  [constants.EDITED_PO_SAVE_REQUEST]: save_edited_po_Request,
  [constants.EDITED_PO_SAVE_SUCCESS]: save_edited_po_Success,
  [constants.EDITED_PO_SAVE_ERROR]: save_edited_po_Error,
  [constants.EDITED_PO_SAVE_CLEAR]: save_edited_po_Clear,
  // discard po pi data

  [constants.DISCARD_PO_PI_DATA_REQUEST]: discardPoPiDataRequest,
  [constants.DISCARD_PO_PI_DATA_SUCCESS]: discardPoPiDataSuccess,
  [constants.DISCARD_PO_PI_DATA_ERROR]: discardPoPiDataError,
  [constants.DISCARD_PO_PI_DATA_CLEAR]: discardPoPiDataClear,

  // view image
  [constants.VIEW_IMAGES_REQUEST]: view_Images_Request,
  [constants.VIEW_IMAGES_SUCCESS]: view_Images_Success,
  [constants.VIEW_IMAGES_ERROR]: view_Images_Error,
  [constants.VIEW_IMAGES_CLEAR]: view_Images_Clear,

  // proc dash bottom
  [constants.PI_DASH_BOTTOM_REQUEST]: piDashBottomRequest,
  [constants.PI_DASH_BOTTOM_SUCCESS]: piDashBottomSuccess,
  [constants.PI_DASH_BOTTOM_ERROR]: piDashBottomError,
  [constants.PI_DASH_BOTTOM_CLEAR]: piDashBottomClear,

  // pi dash hierarchy
  [constants.PI_DASH_HIERARCHY_REQUEST]: piDashHierarchyRequest,
  [constants.PI_DASH_HIERARCHY_SUCCESS]: piDashHierarchySuccess,
  [constants.PI_DASH_HIERARCHY_ERROR]: piDashHierarchyError,
  [constants.PI_DASH_HIERARCHY_CLEAR]: piDashHierarchyClear,

  // EMAIL DASHBOARD PO
  [constants.EMAIL_DASHBOARD_SEND_REQUEST]: emailDashboardSendRequest,
  [constants.EMAIL_DASHBOARD_SEND_SUCCESS]: emailDashboardSendSuccess,
  [constants.EMAIL_DASHBOARD_SEND_ERROR]: emailDashboardSendError,
  [constants.EMAIL_DASHBOARD_SEND_CLEAR]: emailDashboardSendClear,

  // pi po dash get site
  [constants.PI_PO_DASH_SITE_REQUEST]: piPoDashSiteRequest,
  [constants.PI_PO_DASH_SITE_SUCCESS]: piPoDashSiteSuccess,
  [constants.PI_PO_DASH_SITE_ERROR]: piPoDashSiteError,
  [constants.PI_PO_DASH_SITE_CLEAR]: piPoDashSiteClear,

  // sl Data
  [constants.GET_SL_REQUEST]: getSlRequest,
  [constants.GET_SL_SUCCESS]: getSlSuccess,
  [constants.GET_SL_ERROR]: getSlError,
  [constants.GET_SL_CLEAR]: getSlClear,


  //alertPopup modal
  [constants.PI_PO_MODAL]: displayModalBool,

  // pi Image URl Request
  [constants.PI_IMAGE_URL_REQUEST]: piImageUrlRequest,
  [constants.PI_IMAGE_URL_SUCCESS]: piImageUrlSuccess,
  [constants.PI_IMAGE_URL_ERROR]: piImageUrlError,
  [constants.PI_IMAGE_URL_CLEAR]: piImageUrlClear,

  // proc mapping
  [constants.CAT_DESC_DROPDOWN_REQUEST]: catDescDropdownRequest,
  [constants.CAT_DESC_DROPDOWN_SUCCESS]: catDescDropdownSuccess,
  [constants.CAT_DESC_DROPDOWN_ERROR]: catDescDropdownError,
  [constants.CAT_DESC_DROPDOWN_CLEAR]: catDescDropdownClear,

  [constants.UPDATE_ITEM_UDF_MAPPING_REQUEST]: updateItemUdfMappingRequest,
  [constants.UPDATE_ITEM_UDF_MAPPING_SUCCESS]: updateItemUdfMappingSuccess,
  [constants.UPDATE_ITEM_UDF_MAPPING_ERROR]: updateItemUdfMappingError,
  [constants.UPDATE_ITEM_UDF_MAPPING_CLEAR]: updateItemUdfMappingClear,

}, initialState);
