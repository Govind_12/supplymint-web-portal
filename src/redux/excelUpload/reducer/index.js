import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';
let initialState = {
    excelSearch: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
      }, 
      handleValidate: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
      }, 
      excelHeaders: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
      }, 
      excelSubmit: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
      }, 
      downloadTemplate: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
      }, 
      
}

const handleValidateRequest = (state, action) => update(state, {
    handleValidate: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: '' }
    }
  });
  const handleValidateSuccess = (state, action) => update(state, {
    handleValidate: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: action.payload },
      data: { $set: action.payload }
    }
  });

  const handleValidateClear = (state, action) => update(state, {
    handleValidate: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: ''}
    }
  });
  const handleValidateError = (state, action) => update(state, {
    handleValidate: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
    }
  });

  const excelSearchRequest = (state, action) => update(state, {
    excelSearch: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: '' }
    }
  });
  const excelSearchSuccess = (state, action) => update(state, {
    excelSearch: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: action.payload },
      data: { $set: action.payload }
    }
  });

  const excelSearchClear = (state, action) => update(state, {
    excelSearch: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: ''}
    }
  });
  const excelSearchError = (state, action) => update(state, {
    excelSearch: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
    }
  });

  const excelHeadersRequest = (state, action) => update(state, {
    excelHeaders: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: '' }
    }
  });
  const excelHeadersSuccess = (state, action) => update(state, {
    excelHeaders: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: action.payload },
      data: { $set: action.payload }
    }
  });

  const excelHeadersClear = (state, action) => update(state, {
    excelHeaders: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: ''}
    }
  });
  const excelHeadersError = (state, action) => update(state, {
    excelHeaders: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
    }
  });

  
  const excelSubmitRequest = (state, action) => update(state, {
    excelSubmit: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: '' }
    }
  });
  const excelSubmitSuccess = (state, action) => update(state, {
    excelSubmit: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: action.payload },
      data: { $set: action.payload }
    }
  });

  const excelSubmitClear = (state, action) => update(state, {
    excelSubmit: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: ''},
    }
  });
  const excelSubmitError = (state, action) => update(state, {
    excelSubmit: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
    }
  });

  const downloadTemplateRequest = (state, action) => update(state, {
    downloadTemplate: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: '' }
    }
  });
  const downloadTemplateSuccess = (state, action) => update(state, {
    downloadTemplate: {
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: action.payload },
      data: { $set: action.payload }
    }
  });

  const downloadTemplateClear = (state, action) => update(state, {
    downloadTemplate: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: ''},
    }
  });
  const downloadTemplateError = (state, action) => update(state, {
    downloadTemplate: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
    }
  });

  export default handleActions({
    // Excel upload
  [constants.EXCEL_SEARCH_REQUEST]: excelSearchRequest,
  [constants.EXCEL_SEARCH_SUCCESS]: excelSearchSuccess,
  [constants.EXCEL_SEARCH_CLEAR]: excelSearchClear,
  [constants.EXCEL_SEARCH_ERROR]: excelSearchError,

  [constants.HANDLE_VALIDATE_REQUEST]: handleValidateRequest,
  [constants.HANDLE_VALIDATE_SUCCESS]: handleValidateSuccess,
  [constants.HANDLE_VALIDATE_CLEAR]: handleValidateClear,
  [constants.HANDLE_VALIDATE_ERROR]: handleValidateError,

  [constants.EXCEL_HEADERS_REQUEST]: excelHeadersRequest,
  [constants.EXCEL_HEADERS_SUCCESS]: excelHeadersSuccess,
  [constants.EXCEL_HEADERS_CLEAR]: excelHeadersClear,
  [constants.EXCEL_HEADERS_ERROR]: excelHeadersError,

  [constants.EXCEL_SUBMIT_REQUEST]: excelSubmitRequest,
  [constants.EXCEL_SUBMIT_SUCCESS]: excelSubmitSuccess,
  [constants.EXCEL_SUBMIT_CLEAR]: excelSubmitClear,
  [constants.EXCEL_SUBMIT_ERROR]: excelSubmitError,

  [constants.DOWNLOAD_TEMPLATE_REQUEST]: downloadTemplateRequest,
  [constants.DOWNLOAD_TEMPLATE_SUCCESS]: downloadTemplateSuccess,
  [constants.DOWNLOAD_TEMPLATE_CLEAR]: downloadTemplateClear,
  [constants.DOWNLOAD_TEMPLATE_ERROR]: downloadTemplateError,
  
}, initialState);
