import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import moment from 'moment';
import encryption from '../../../encryption';
import { OganisationIdName } from '../../../organisationIdName.js';

export function* excelSearchRequest(action) {
    console.log("in action", action)
    try {
      const response = yield call(fireAjax, 'POST',  `${CONFIG.BASE_URL}/admin/po/excel/search`,
       action.payload);
      const finalResponse = script(response);
      console.log(response)
      if (finalResponse.success) {
        yield put(actions.excelSearchSuccess(response.data.data))
      } else if (finalResponse.failure) {
        yield put(actions.excelSearchError(response.data))
      }
    } catch (e) {
      console.warn(e, "getSlRequest")
      yield put(actions.excelSearchError(response.data));
    }
  }

  export function* handleValidateRequest(action) {
    console.log("in action", action)
    try {
      const response = yield call(fireAjax, 'POST',  `${CONFIG.BASE_URL}/admin/po/excel/data/validation`,
       action.payload);
      const finalResponse = script(response);
      console.log(response)
      if (finalResponse.success) {
        yield put(actions.handleValidateSuccess(response.data.data))
      } else if (finalResponse.failure) {
        yield put(actions.handleValidateError(response.data))
      }
    } catch (e) {
      console.warn(e, "getSlRequest")
      yield put(actions.handleValidateError(response.data));
    }
  }

  export function* excelHeadersRequest(action) {
    console.log("in action", action)
    try {
      let type = "DIGIPROC_EXCEL_HEADER"
      const response = yield call(fireAjax, 'GET',  `${CONFIG.BASE_URL}/core/dropdown/get?type=${type}`);
      const finalResponse = script(response);
      console.log(response)
      if (finalResponse.success) {
        yield put(actions.excelHeadersSuccess(response.data.data))
      } else if (finalResponse.failure) {
        yield put(actions.excelHeadersError(response.data))
      }
    } catch (e) {
      console.warn(e, "getSlRequest")
      yield put(actions.excelHeadersError(response.data));
    }
  }

  export  function*  excelSubmitRequest(action) {
    console.log("in action", action)
    try {
      const response = yield call(fireAjax, 'POST',  `${CONFIG.BASE_URL}/admin/po/excel/submit/data`, action.payload);
      const finalResponse = script(response);
      console.log(response)
      if (finalResponse.success) {
        yield put(actions.excelSubmitSuccess(response.data.data))
      } else if (finalResponse.failure) {
        yield put(actions.excelSubmitError(response.data))
      }
    } catch (e) {
      console.warn(e, "getSlRequest")
      yield put(actions.excelSubmitError(response.data));
    }
  }

  export  function*  downloadTemplateRequest(action) {
    console.log("in action", action)
    try {
      const response = yield call(fireAjax, 'POST',  `${CONFIG.BASE_URL}/download/excel/template`, action.payload);
      const finalResponse = script(response);
      console.log(response)
      if (finalResponse.success) {
        yield put(actions.downloadTemplateSuccess(response.data.data))
      } else if (finalResponse.failure) {
        yield put(actions.downloadTemplateError(response.data))
      }
    } catch (e) {
      console.warn(e, "downloadTemplate")
      yield put(actions.downloadTemplateError(response.data));
    }
  }