import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
    EnterpriseApprovedPo: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    EnterpriseGetPoDetails: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    EnterpriseCancelPo: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    VendorCreateShipmentDetails: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    vendorCreateSr: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    poCloser: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    findShipmentAdvice: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    shipmentTracking: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },

    commentNotification: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    dashboardUpper: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    dashboardBottom: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    dashboardUnreadComment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    vendorDashboardFilter: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getAllPlans: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getPaymentSummary: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getOrderId: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getCapturePayment: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    multipleDocumentDownload: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    emailTranscript: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    updateHeaderData: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
   getAddressData: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getLocationData: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    approvePOConfirm: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    cancelAndRejectReason: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getPaymentStatus: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getAllComments: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getAllLedgerReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getAllSaleReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getAllStockReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getAllOutStandingReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getLedgerFilterReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getVendorActivity: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getPoStockReport: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getVendorLogs: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getPoColorMap: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    createPoArticle: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    }
}
const EnterpriseApprovedPoRequest = (state, action) => update(state, {
    EnterpriseApprovedPo: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const EnterpriseApprovedPoError = (state, action) => update(state, {
    EnterpriseApprovedPo: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const EnterpriseApprovedPoSuccess = (state, action) => update(state, {
    EnterpriseApprovedPo: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const EnterpriseApprovedPoClear = (state, action) => update(state, {
    EnterpriseApprovedPo: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
// _________________________________DETAILS PO______________________________
const EnterpriseGetPoDetailsRequest = (state, action) => update(state, {
    EnterpriseGetPoDetails: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const EnterpriseGetPoDetailsError = (state, action) => update(state, {
    EnterpriseGetPoDetails: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload },

    }
})
const EnterpriseGetPoDetailsSuccess = (state, action) => update(state, {
    EnterpriseGetPoDetails: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const EnterpriseGetPoDetailsClear = (state, action) => update(state, {
    EnterpriseGetPoDetails: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' },
        // data : {$set : ''}
    }
})
// ___________________________CANCEL PO______________________________
const EnterpriseCancelPoRequest = (state, action) => update(state, {
    EnterpriseCancelPo: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const EnterpriseCancelPoError = (state, action) => update(state, {
    EnterpriseCancelPo: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const EnterpriseCancelPoSuccess = (state, action) => update(state, {
    EnterpriseCancelPo: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const EnterpriseCancelPoClear = (state, action) => update(state, {
    EnterpriseCancelPo: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// -----------------------------------------VENDOR CREATE SHIPMENT DETAILS PO -----------------------------
const VendorCreateShipmentDetailsRequest = (state, action) => update(state, {
    VendorCreateShipmentDetails: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const VendorCreateShipmentDetailsError = (state, action) => update(state, {
    VendorCreateShipmentDetails: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const VendorCreateShipmentDetailsSuccess = (state, action) => update(state, {
    VendorCreateShipmentDetails: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const VendorCreateShipmentDetailsClear = (state, action) => update(state, {
    VendorCreateShipmentDetails: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// ---------------------------------------VENDOR CREATE SR -----------------------------

const vendorCreateSrRequest = (state, action) => update(state, {
    vendorCreateSr: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const vendorCreateSrError = (state, action) => update(state, {
    vendorCreateSr: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const vendorCreateSrSuccess = (state, action) => update(state, {
    vendorCreateSr: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const vendorCreateSrClear = (state, action) => update(state, {
    vendorCreateSr: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//--------------------------------Approve PO Confirmation--------------
const approvePOConfirmRequest = (state, action) => update(state, {
    approvePOConfirm: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const approvePOConfirmError = (state, action) => update(state, {
    approvePOConfirm: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const approvePOConfirmSuccess = (state, action) => update(state, {
    approvePOConfirm: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const approvePOConfirmClear = (state, action) => update(state, {
    approvePOConfirm: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// -------------------------------Po Closer----------------------------
const poCloserRequest = (state, action) => update(state, {
    poCloser: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const poCloserError = (state, action) => update(state, {
    poCloser: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const poCloserSuccess = (state, action) => update(state, {
    poCloser: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const poCloserClear = (state, action) => update(state, {
    poCloser: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// -------------------------------Shipment Advice Get ----------------------------
const findShipmentAdviceRequest = (state, action) => update(state, {
    findShipmentAdvice: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
//processed 
const findShipmentAdviceSuccess = (state, action) => update(state, {
    findShipmentAdvice: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
})
const findShipmentAdviceClear = (state, action) => update(state, {
    findShipmentAdvice: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const findShipmentAdviceError = (state, action) => update(state, {
    findShipmentAdvice: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});


const shipmentTrackingRequest = (state, action) => update(state, {
    shipmentTracking: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const shipmentTrackingError = (state, action) => update(state, {
    shipmentTracking: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const shipmentTrackingSuccess = (state, action) => update(state, {
    shipmentTracking: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const shipmentTrackingClear = (state, action) => update(state, {
    shipmentTracking: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//Comment Notification 
const commentNotificationRequest = (state, action) => update(state, {
    commentNotification: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const commentNotificationError = (state, action) => update(state, {
    commentNotification: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const commentNotificationSuccess = (state, action) => update(state, {
    commentNotification: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const commentNotificationClear = (state, action) => update(state, {
    commentNotification: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})


// dashboard 
const dashboardUpperRequest = (state, action) => update(state, {
    dashboardUpper: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const dashboardUpperError = (state, action) => update(state, {
    dashboardUpper: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const dashboardUpperSuccess = (state, action) => update(state, {
    dashboardUpper: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const dashboardUpperClear = (state, action) => update(state, {
    dashboardUpper: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const dashboardBottomRequest = (state, action) => update(state, {
    dashboardBottom: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const dashboardBottomError = (state, action) => update(state, {
    dashboardBottom: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const dashboardBottomSuccess = (state, action) => update(state, {
    dashboardBottom: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const dashboardBottomClear = (state, action) => update(state, {
    dashboardBottom: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// dashboard unread comments ::
const dashboardUnreadCommentRequest = (state, action) => update(state, {
    dashboardUnreadComment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const dashboardUnreadCommentError = (state, action) => update(state, {
    dashboardUnreadComment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const dashboardUnreadCommentSuccess = (state, action) => update(state, {
    dashboardUnreadComment: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const dashboardUnreadCommentClear = (state, action) => update(state, {
    dashboardUnreadComment: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// get vendor dashboard filters::::
const vendorDashboardFilterRequest = (state, action) => update(state, {
    vendorDashboardFilter: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const vendorDashboardFilterError = (state, action) => update(state, {
    vendorDashboardFilter: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const vendorDashboardFilterSuccess = (state, action) => update(state, {
    vendorDashboardFilter: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const vendorDashboardFilterClear = (state, action) => update(state, {
    vendorDashboardFilter: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
// get all Plans

const getAllPlansRequest = (state, action) => update(state, {
    getAllPlans: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllPlansError = (state, action) => update(state, {
    getAllPlans: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const getAllPlansSuccess = (state, action) => update(state, {
    getAllPlans: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const getAllPlansClear = (state, action) => update(state, {
    getAllPlans: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
// get payment summary
const getPaymentSummaryRequest = (state, action) => update(state, {
    getPaymentSummary: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getPaymentSummaryError = (state, action) => update(state, {
    getPaymentSummary: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const getPaymentSummarySuccess = (state, action) => update(state, {
    getPaymentSummary: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const getPaymentSummaryClear = (state, action) => update(state, {
    getPaymentSummary: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
// get order id
const getOrderIdRequest = (state, action) => update(state, {
    getOrderId: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getOrderIdError = (state, action) => update(state, {
    getOrderId: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const getOrderIdSuccess = (state, action) => update(state, {
    getOrderId: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const getOrderIdClear = (state, action) => update(state, {
    getOrderId: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// get capture payment
const getCapturePaymentRequest = (state, action) => update(state, {
    getCapturePayment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getCapturePaymentError = (state, action) => update(state, {
    getCapturePayment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const getCapturePaymentSuccess = (state, action) => update(state, {
    getCapturePayment: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const getCapturePaymentClear = (state, action) => update(state, {
    getCapturePayment: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// Multiple Document Download

const multipleDocumentDownloadRequest = (state, action) => update(state, {
    multipleDocumentDownload: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const multipleDocumentDownloadError = (state, action) => update(state, {
    multipleDocumentDownload: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const multipleDocumentDownloadSuccess = (state, action) => update(state, {
    multipleDocumentDownload: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const multipleDocumentDownloadClear = (state, action) => update(state, {
    multipleDocumentDownload: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// Email Transcript

const emailTranscriptRequest = (state, action) => update(state, {
    emailTranscript: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const emailTranscriptError = (state, action) => update(state, {
    emailTranscript: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const emailTranscriptSuccess = (state, action) => update(state, {
    emailTranscript: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});

const emailTranscriptClear = (state, action) => update(state, {
    emailTranscript: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// ___________________________Update Header______________________________
const updateHeaderRequest = (state, action) => update(state, {
    updateHeaderData: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateHeaderError = (state, action) => update(state, {
    updateHeaderData: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const updateHeaderSuccess = (state, action) => update(state, {
    updateHeaderData: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const updateHeaderClear = (state, action) => update(state, {
    updateHeaderData: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// ___________________________Get Address______________________________
const getAddressRequest = (state, action) => update(state, {
    getAddressData: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

// Payment Status

const getPaymentStatusRequest = (state, action) => update(state, {
    getPaymentStatus: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAddressError = (state, action) => update(state, {
    getAddressData: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const getPaymentStatusError = (state, action) => update(state, {
    getPaymentStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAddressSuccess = (state, action) => update(state, {
    getAddressData: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAddressClear = (state, action) => update(state, {
    getAddressData: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})
// ___________________________Get Location______________________________
const getLocationRequest = (state, action) => update(state, {
    getLocationData: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getLocationError = (state, action) => update(state, {
    getLocationData: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getLocationSuccess = (state, action) => update(state, {
    getLocationData: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
})

const getPaymentStatusSuccess = (state, action) => update(state, {
    getPaymentStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getLocationClear = (state, action) => update(state, {
    getLocationData: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// Cancellation and Rejection Reason on cancel/reject action::
const getCancelAndRejectReasonRequest = (state, action) => update(state, {
    cancelAndRejectReason: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getCancelAndRejectReasonError = (state, action) => update(state, {
    cancelAndRejectReason: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getCancelAndRejectReasonSuccess = (state, action) => update(state, {
    cancelAndRejectReason: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getCancelAndRejectReasonClear = (state, action) => update(state, {
    cancelAndRejectReason: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getPaymentStatusClear = (state, action) => update(state, {
    getPaymentStatus: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//Get All Comments::
const getAllCommentsRequest = (state, action) => update(state, {
    getAllComments: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllCommentsError = (state, action) => update(state, {
    getAllComments: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllCommentsSuccess = (state, action) => update(state, {
    getAllComments: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllCommentsClear = (state, action) => update(state, {
    getAllComments: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
//Get All Ledger Report::
const getAllLedgerReportRequest = (state, action) => update(state, {
    getAllLedgerReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllLedgerReportError = (state, action) => update(state, {
    getAllLedgerReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllLedgerReportSuccess = (state, action) => update(state, {
    getAllLedgerReport: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllLedgerReportClear = (state, action) => update(state, {
    getAllLedgerReport: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
//Get All Sale Report::
const getAllSaleReportRequest = (state, action) => update(state, {
    getAllSaleReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllSaleReportError = (state, action) => update(state, {
    getAllSaleReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllSaleReportSuccess = (state, action) => update(state, {
    getAllSaleReport: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllSaleReportClear = (state, action) => update(state, {
    getAllSaleReport: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
//Get All Stock Report::
const getAllStockReportRequest = (state, action) => update(state, {
    getAllStockReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllStockReportError = (state, action) => update(state, {
    getAllStockReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllStockReportSuccess = (state, action) => update(state, {
    getAllStockReport: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllStockReportClear = (state, action) => update(state, {
    getAllStockReport: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
//Get All OutStanding Report::
const getAllOutStandingReportRequest = (state, action) => update(state, {
    getAllOutStandingReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getAllOutStandingReportError = (state, action) => update(state, {
    getAllOutStandingReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getAllOutStandingReportSuccess = (state, action) => update(state, {
    getAllOutStandingReport: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getAllOutStandingReportClear = (state, action) => update(state, {
    getAllOutStandingReport: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
//Submit Ledger Filter Report::
const getLedgerFilterReportRequest = (state, action) => update(state, {
    getLedgerFilterReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getLedgerFilterReportError = (state, action) => update(state, {
    getLedgerFilterReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getLedgerFilterReportSuccess = (state, action) => update(state, {
    getLedgerFilterReport: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getLedgerFilterReportClear = (state, action) => update(state, {
    getLedgerFilterReport: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getVendorActivityRequest = (state, action) => update(state, {
    getVendorActivity: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getVendorActivityError = (state, action) => update(state, {
    getVendorActivity: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getVendorActivitySuccess = (state, action) => update(state, {
    getVendorActivity: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getVendorActivityClear = (state, action) => update(state, {
    getVendorActivity: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getPoStockReportRequest = (state, action) => update(state, {
    getPoStockReport: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getPoStockReportError = (state, action) => update(state, {
    getPoStockReport: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getPoStockReportSuccess = (state, action) => update(state, {
    getPoStockReport: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getPoStockReportClear = (state, action) => update(state, {
    getPoStockReport: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getVendorLogsRequest = (state, action) => update(state, {
    getVendorLogs: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getVendorLogsError = (state, action) => update(state, {
    getVendorLogs: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getVendorLogsSuccess = (state, action) => update(state, {
    getVendorLogs: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getVendorLogsClear = (state, action) => update(state, {
    getVendorLogs: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getPoColorMapRequest = (state, action) => update(state, {
    getPoColorMap: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getPoColorMapError = (state, action) => update(state, {
    getPoColorMap: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getPoColorMapSuccess = (state, action) => update(state, {
    getPoColorMap: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getPoColorMapClear = (state, action) => update(state, {
    getPoColorMap: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const createPoArticleRequest = (state, action) => update(state, {
    createPoArticle: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const createPoArticleError = (state, action) => update(state, {
    createPoArticle: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const createPoArticleSuccess = (state, action) => update(state, {
    createPoArticle: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const createPoArticleClear = (state, action) => update(state, {
    createPoArticle: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

export default handleActions({

    [constants.ENTERPRISE_APPROVED_PO_REQUEST]: EnterpriseApprovedPoRequest,
    [constants.ENTERPRISE_APPROVED_PO_SUCCESS]: EnterpriseApprovedPoSuccess,
    [constants.ENTERPRISE_APPROVED_PO_CLEAR]: EnterpriseApprovedPoClear,
    [constants.ENTERPRISE_APPROVED_PO_ERROR]: EnterpriseApprovedPoError,

    [constants.ENTERPRISE_GET_PO_DETAILS_REQUEST]: EnterpriseGetPoDetailsRequest,
    [constants.ENTERPRISE_GET_PO_DETAILS_SUCCESS]: EnterpriseGetPoDetailsSuccess,
    [constants.ENTERPRISE_GET_PO_DETAILS_ERROR]: EnterpriseGetPoDetailsError,
    [constants.ENTERPRISE_GET_PO_DETAILS_CLEAR]: EnterpriseGetPoDetailsClear,

    [constants.ENTERPRISE_CANCEL_PO_REQUEST]: EnterpriseCancelPoRequest,
    [constants.ENTERPRISE_CANCEL_PO_SUCCESS]: EnterpriseCancelPoSuccess,
    [constants.ENTERPRISE_CANCEL_PO_ERROR]: EnterpriseCancelPoError,
    [constants.ENTERPRISE_CANCEL_PO_CLEAR]: EnterpriseCancelPoClear,

    [constants.VENDOR_CREATE_SHIPMENT_DETAILS_REQUEST]: VendorCreateShipmentDetailsRequest,
    [constants.VENDOR_CREATE_SHIPMENT_DETAILS_SUCCESS]: VendorCreateShipmentDetailsSuccess,
    [constants.VENDOR_CREATE_SHIPMENT_DETAILS_ERROR]: VendorCreateShipmentDetailsError,
    [constants.VENDOR_CREATE_SHIPMENT_DETAILS_CLEAR]: VendorCreateShipmentDetailsClear,

    [constants.VENDOR_CREATE_SR_REQUEST]: vendorCreateSrRequest,
    [constants.VENDOR_CREATE_SR_SUCCESS]: vendorCreateSrSuccess,
    [constants.VENDOR_CREATE_SR_ERROR]: vendorCreateSrError,
    [constants.VENDOR_CREATE_SR_CLEAR]: vendorCreateSrClear,

    [constants.PO_CLOSER_REQUEST]: poCloserRequest,
    [constants.PO_CLOSER_SUCCESS]: poCloserSuccess,
    [constants.PO_CLOSER_ERROR]: poCloserError,
    [constants.PO_CLOSER_CLEAR]: poCloserClear,

    [constants.FIND_SHIPMENT_ADVICE_REQUEST]: findShipmentAdviceRequest,
    [constants.FIND_SHIPMENT_ADVICE_SUCCESS]: findShipmentAdviceSuccess,
    [constants.FIND_SHIPMENT_ADVICE_ERROR]: findShipmentAdviceError,
    [constants.FIND_SHIPMENT_ADVICE_CLEAR]: findShipmentAdviceClear,

    [constants.SHIPMENT_TRACKING_REQUEST]: shipmentTrackingRequest,
    [constants.SHIPMENT_TRACKING_SUCCESS]: shipmentTrackingSuccess,
    [constants.SHIPMENT_TRACKING_ERROR]: shipmentTrackingError,
    [constants.SHIPMENT_TRACKING_CLEAR]: shipmentTrackingClear,

    [constants.COMMENT_NOTIFICATION_REQUEST]: commentNotificationRequest,
    [constants.COMMENT_NOTIFICATION_SUCCESS]: commentNotificationSuccess,
    [constants.COMMENT_NOTIFICATION_ERROR]: commentNotificationError,
    [constants.COMMENT_NOTIFICATION_CLEAR]: commentNotificationClear,

    [constants.DASHBOARD_UPPER_REQUEST]: dashboardUpperRequest,
    [constants.DASHBOARD_UPPER_SUCCESS]: dashboardUpperSuccess,
    [constants.DASHBOARD_UPPER_ERROR]: dashboardUpperError,
    [constants.DASHBOARD_UPPER_CLEAR]: dashboardUpperClear,

    [constants.DASHBOARD_BOTTOM_REQUEST]: dashboardBottomRequest,
    [constants.DASHBOARD_BOTTOM_SUCCESS]: dashboardBottomSuccess,
    [constants.DASHBOARD_BOTTOM_ERROR]: dashboardBottomError,
    [constants.DASHBOARD_BOTTOM_CLEAR]: dashboardBottomClear,

    [constants.DASHBOARD_UNREAD_COMMENT_REQUEST]: dashboardUnreadCommentRequest,
    [constants.DASHBOARD_UNREAD_COMMENT_SUCCESS]:dashboardUnreadCommentSuccess,
    [constants.DASHBOARD_UNREAD_COMMENT_ERROR]:dashboardUnreadCommentError,
    [constants.DASHBOARD_UNREAD_COMMENT_CLEAR]:dashboardUnreadCommentClear,


    [constants.VENDOR_DASHBOARD_FILTER_REQUEST]: vendorDashboardFilterRequest,
    [constants.VENDOR_DASHBOARD_FILTER_SUCCESS]: vendorDashboardFilterSuccess,
    [constants.VENDOR_DASHBOARD_FILTER_ERROR]: vendorDashboardFilterError,
    [constants.VENDOR_DASHBOARD_FILTER_CLEAR]: vendorDashboardFilterClear,

    [constants.GET_ALL_PLANS_REQUEST]: getAllPlansRequest,
    [constants.GET_ALL_PLANS_SUCCESS]: getAllPlansSuccess,
    [constants.GET_ALL_PLANS_ERROR]: getAllPlansError,
    [constants.GET_ALL_PLANS_CLEAR]: getAllPlansClear,

    [constants.GET_PAYMENT_SUMMARY_REQUEST]: getPaymentSummaryRequest,
    [constants.GET_PAYMENT_SUMMARY_SUCCESS]: getPaymentSummarySuccess,
    [constants.GET_PAYMENT_SUMMARY_ERROR]: getPaymentSummaryError,
    [constants.GET_PAYMENT_SUMMARY_CLEAR]: getPaymentSummaryClear,

    [constants.GET_ORDER_ID_REQUEST]: getOrderIdRequest,
    [constants.GET_ORDER_ID_SUCCESS]: getOrderIdSuccess,
    [constants.GET_ORDER_ID_ERROR]: getOrderIdError,
    [constants.GET_ORDER_ID_CLEAR]: getOrderIdClear,

    [constants.GET_CAPTURE_PAYMENT_REQUEST]: getCapturePaymentRequest,
    [constants.GET_CAPTURE_PAYMENT_SUCCESS]: getCapturePaymentSuccess,
    [constants.GET_CAPTURE_PAYMENT_ERROR]: getCapturePaymentError,
    [constants.GET_CAPTURE_PAYMENT_CLEAR]: getCapturePaymentClear,


    [constants.MULTIPLE_DOCUMENT_DOWNLOAD_REQUEST]: multipleDocumentDownloadRequest,
    [constants.MULTIPLE_DOCUMENT_DOWNLOAD_SUCCESS]: multipleDocumentDownloadSuccess,
    [constants.MULTIPLE_DOCUMENT_DOWNLOAD_ERROR]: multipleDocumentDownloadError,
    [constants.MULTIPLE_DOCUMENT_DOWNLOAD_CLEAR]: multipleDocumentDownloadClear,


    [constants.EMAIL_TRANSCRIPT_REQUEST]: emailTranscriptRequest,
    [constants.EMAIL_TRANSCRIPT_SUCCESS]: emailTranscriptSuccess,
    [constants.EMAIL_TRANSCRIPT_ERROR]: emailTranscriptError,
    [constants.EMAIL_TRANSCRIPT_CLEAR]: emailTranscriptClear,

    [constants.UPDATE_HEADER_REQUEST]: updateHeaderRequest,
    [constants.UPDATE_HEADER_SUCCESS]: updateHeaderSuccess,
    [constants.UPDATE_HEADER_ERROR]: updateHeaderError,
    [constants.UPDATE_HEADER_CLEAR]: updateHeaderClear,

    [constants.GET_ADDRESS_REQUEST]: getAddressRequest,
    [constants.GET_ADDRESS_SUCCESS]: getAddressSuccess,
    [constants.GET_ADDRESS_ERROR]: getAddressError,
    [constants.GET_ADDRESS_CLEAR]: getAddressClear,

    [constants.GET_LOCATION_REQUEST]: getLocationRequest,
    [constants.GET_LOCATION_SUCCESS]: getLocationSuccess,
    [constants.GET_LOCATION_ERROR]: getLocationError,
    [constants.GET_LOCATION_CLEAR]: getLocationClear,
    
    [constants.GET_PAYMENT_STATUS_REQUEST]: getPaymentStatusRequest,
    [constants.GET_PAYMENT_STATUS_SUCCESS]: getPaymentStatusSuccess,
    [constants.GET_PAYMENT_STATUS_ERROR]: getPaymentStatusError,
    [constants.GET_PAYMENT_STATUS_CLEAR]: getPaymentStatusClear,

    [constants.APPROVE_PO_CONFIRM_REQUEST]: approvePOConfirmRequest,
    [constants.APPROVE_PO_CONFIRM_SUCCESS]: approvePOConfirmSuccess,
    [constants.APPROVE_PO_CONFIRM_CLEAR]: approvePOConfirmClear,
    [constants.APPROVE_PO_CONFIRM_ERROR]: approvePOConfirmError,

    [constants.CANCEL_AND_REJECT_REASON_REQUEST]: getCancelAndRejectReasonRequest,
    [constants.CANCEL_AND_REJECT_REASON_SUCCESS]: getCancelAndRejectReasonSuccess,
    [constants.CANCEL_AND_REJECT_REASON_CLEAR]: getCancelAndRejectReasonClear,
    [constants.CANCEL_AND_REJECT_REASON_ERROR]: getCancelAndRejectReasonError,

    [constants.GET_ALL_COMMENTS_REQUEST]: getAllCommentsRequest,
    [constants.GET_ALL_COMMENTS_SUCCESS]: getAllCommentsSuccess,
    [constants.GET_ALL_COMMENTS_CLEAR]: getAllCommentsClear,
    [constants.GET_ALL_COMMENTS_ERROR]: getAllCommentsError,

    [constants.GET_ALL_LEDGER_REPORT_REQUEST]: getAllLedgerReportRequest,
    [constants.GET_ALL_LEDGER_REPORT_SUCCESS]: getAllLedgerReportSuccess,
    [constants.GET_ALL_LEDGER_REPORT_CLEAR]: getAllLedgerReportClear,
    [constants.GET_ALL_LEDGER_REPORT_ERROR]: getAllLedgerReportError,

    [constants.GET_ALL_SALE_REPORT_REQUEST]: getAllSaleReportRequest,
    [constants.GET_ALL_SALE_REPORT_SUCCESS]: getAllSaleReportSuccess,
    [constants.GET_ALL_SALE_REPORT_CLEAR]: getAllSaleReportClear,
    [constants.GET_ALL_SALE_REPORT_ERROR]: getAllSaleReportError,
    
    [constants.GET_ALL_STOCK_REPORT_REQUEST]: getAllStockReportRequest,
    [constants.GET_ALL_STOCK_REPORT_SUCCESS]: getAllStockReportSuccess,
    [constants.GET_ALL_STOCK_REPORT_CLEAR]: getAllStockReportClear,
    [constants.GET_ALL_STOCK_REPORT_ERROR]: getAllStockReportError,

    [constants.GET_ALL_OUTSTANDING_REPORT_REQUEST]: getAllOutStandingReportRequest,
    [constants.GET_ALL_OUTSTANDING_REPORT_SUCCESS]: getAllOutStandingReportSuccess,
    [constants.GET_ALL_OUTSTANDING_REPORT_CLEAR]: getAllOutStandingReportClear,
    [constants.GET_ALL_OUTSTANDING_REPORT_ERROR]: getAllOutStandingReportError,

    [constants.GET_LEDGER_FILTER_REPORT_REQUEST]: getLedgerFilterReportRequest,
    [constants.GET_LEDGER_FILTER_REPORT_SUCCESS]: getLedgerFilterReportSuccess,
    [constants.GET_LEDGER_FILTER_REPORT_CLEAR]: getLedgerFilterReportClear,
    [constants.GET_LEDGER_FILTER_REPORT_ERROR]: getLedgerFilterReportError,

    [constants.GET_VENDOR_ACTIVITY_REQUEST]: getVendorActivityRequest,
    [constants.GET_VENDOR_ACTIVITY_SUCCESS]: getVendorActivitySuccess,
    [constants.GET_VENDOR_ACTIVITY_CLEAR]: getVendorActivityClear,
    [constants.GET_VENDOR_ACTIVITY_ERROR]: getVendorActivityError,

    [constants.GET_VENDOR_LOGS_REQUEST]: getVendorLogsRequest,
    [constants.GET_VENDOR_LOGS_SUCCESS]: getVendorLogsSuccess,
    [constants.GET_VENDOR_LOGS_CLEAR]: getVendorLogsClear,
    [constants.GET_VENDOR_LOGS_ERROR]: getVendorLogsError,
    
    [constants.GET_PO_STOCK_REPORT_REQUEST]: getPoStockReportRequest,
    [constants.GET_PO_STOCK_REPORT_SUCCESS]: getPoStockReportSuccess,
    [constants.GET_PO_STOCK_REPORT_CLEAR]: getPoStockReportClear,
    [constants.GET_PO_STOCK_REPORT_ERROR]: getPoStockReportError,

    [constants.GET_PO_COLOR_MAP_REQUEST]: getPoColorMapRequest,
    [constants.GET_PO_COLOR_MAP_SUCCESS]: getPoColorMapSuccess,
    [constants.GET_PO_COLOR_MAP_CLEAR]: getPoColorMapClear,
    [constants.GET_PO_COLOR_MAP_ERROR]: getPoColorMapError,

    [constants.CREATE_PO_ARTICLE_REQUEST]: createPoArticleRequest,
    [constants.CREATE_PO_ARTICLE_SUCCESS]: createPoArticleSuccess,
    [constants.CREATE_PO_ARTICLE_CLEAR]: createPoArticleClear,
    [constants.CREATE_PO_ARTICLE_ERROR]: createPoArticleError,

}, initialState)
