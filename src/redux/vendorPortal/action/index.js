import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import { AUTH_CONFIG } from '../../../authConfig/index';
import script from '../../script';
import loginAjax from '../../../services/authServices';

// ______________________________-ENTERPRISE ORDER API________________________________

export function* EnterpriseApprovedPoRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.EnterpriseApprovedPoClear())
    } else {
        try {
            let no = action.payload.no == undefined ? 1 : action.payload.no;
            let type = action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let status = action.payload.status;
            let userType = action.payload.userType;
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy;
            let sortedIn = action.payload.sortedIn == undefined ? "" : action.payload.sortedIn;
            let isDashboardComment = action.payload.isDashboardComment;
            let isReportPage = action.payload.isReportPage == undefined ? 0 : action.payload.isReportPage;
            let get = {
                pageNo: no,
                type: type,
                search: search,
                status: status,
                sortedBy,
                sortedIn,
                isDashboardComment,
                isReportPage,
            }
            let filter = { ...action.payload.filter }
            let final = { ...get, filter}
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/all`, final
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.EnterpriseApprovedPoSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.EnterpriseApprovedPoError(response.data));
            }

        } catch (e) {
            yield put(actions.EnterpriseApprovedPoError("error occurs"));
            console.warn('Some error found in "EnterpriseApprovedPoRequest" action\n', e);
        }
    }
}

export function* EnterpriseGetPoDetailsRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.EnterpriseGetPoDetailsClear())
    } else {
        try {
            let userType = action.payload.userType;
            let detailType = action.payload.detailType == undefined ? "" : action.payload.detailType;
            let orderId = action.payload.id == undefined ? "" : action.payload.id;
            let setHeaderId = action.payload.setHeaderId == undefined ? "" : action.payload.setHeaderId;
            let poType = action.payload.poType == undefined ? "" : action.payload.poType;

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/get/poDetails`, {
                detailType,
                orderId,
                setHeaderId,
                poType
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                response.data.data.detailType = detailType
                yield put(actions.EnterpriseGetPoDetailsSuccess(response.data.data));

            } else if (finalResponse.failure) {
                yield put(actions.EnterpriseGetPoDetailsError(response.data));
            }

        } catch (e) {
            yield put(actions.EnterpriseGetPoDetailsError("error occurs"));
            console.warn('Some error found in "EnterpriseGetPoDetailsRequest" action\n', e);
        }
    }
}


export function* EnterpriseCancelPoRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.EnterpriseCancelPoClear())
    } else {
        try {
            let poStatusList = action.payload.cancelList;
            let poCriteria = action.payload.poCriteria;
            let userType = action.payload.userType;

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/update`, {
                'poStatusList': poStatusList,
                'poCriteria': poCriteria
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.EnterpriseCancelPoSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.EnterpriseCancelPoError(response.data));
            }

        } catch (e) {
            yield put(actions.EnterpriseCancelPoError("error occurs"));
            console.warn('Some error found in "EnterpriseCancelPoRequest" action\n', e);
        }
    }
}

// ----------------------------------_Vendor Pending Po Request--------------------
export function* VendorCreateShipmentDetailsRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.VendorCreateShipmentDetailsClear())
    } else {
        try {
            let userType = action.payload.userType;
            let detailType = action.payload.detailType == undefined ? "" : action.payload.detailType;
            let orderId = action.payload.id == undefined ? "" : action.payload.id;
            let setHeaderId = action.payload.setHeaderId == undefined ? "" : action.payload.setHeaderId;
            let poType = action.payload.poType == undefined ? "" : action.payload.poType;

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorpo/get/poDetail`, {
                detailType,
                orderId,
                setHeaderId,
                poType
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                response.data.data.detailType = detailType
                yield put(actions.VendorCreateShipmentDetailsSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.VendorCreateShipmentDetailsError(response.data));
            }

        } catch (e) {
            yield put(actions.VendorCreateShipmentDetailsError("error occurs"));
            console.warn('Some error found in "VendorCreateShipmentDetailsRequest" action\n', e);
        }
    }
}

// ------------------------------------------------------Save create Shipment Api------------------------------
export function* vendorCreateSrRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.vendorCreateSrClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/create/sr`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.vendorCreateSrSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.vendorCreateSrError(response.data));
            }

        } catch (e) {
            yield put(actions.vendorCreateSrError("error occurs"));
            console.warn('Some error found in "VedorCreateSrRequest" action\n', e);
        }
    }
}

export function* shipmentTrackingRequest(action) {
    console.log(action.payload)
    try {
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/vendorportal/vendorship/track/status`, action.payload);

        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.shipmentTrackingSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.shipmentTrackingError(response.data));
        }
    } catch (e) {
        yield put(actions.shipmentTrackingError("error occurs"));
        console.warn('Some error found in "shipmentTrackingRequest" action\n', e);
    }

}

//Approve PO Confirmation:::
export function* approvePOConfirmRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.approvePOConfirmClear())
    }else{
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorpo/confirm`, action.payload);

            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.approvePOConfirmSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.approvePOConfirmError(response.data));
            }
        } catch (e) {
            yield put(actions.approvePOConfirmError("error occurs"));
            console.warn('Some error found in "approvePOConfirmRequest" action\n', e);
        }
    }

}

// -----------------------------PO Closer Orders Api---------------------
export function* poCloserRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.poCloserClear())
    } else {
        try {
            let no = action.payload.no == undefined ? 1 : action.payload.no;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let type = action.payload.type;
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy;
            let sortedIn = action.payload.sortedIn == undefined ? "" : action.payload.sortedIn;
            let isDashboardComment = action.payload.isDashboardComment;
            let get = {
                pageNo: no,
                type: type,
                search: search,
                sortedBy,
                sortedIn,
                isDashboardComment,
            }
            let filter = {...action.payload.filter}
            let final = { ...get,  filter}

            let userType = action.payload.userType;

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/closer`, final);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.poCloserSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.poCloserError(response.data));
            }
        } catch (e) {
            yield put(actions.poCloserError("error occurs"));
            console.warn('Some error found in "poCloserRequest" action\n', e);
        }
    }
}

// -------------------------get all asn -------------------------------------
export function* findShipmentAdviceRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.findShipmentAdviceClear())
    } else {
        try {
            let id = action.payload.id == undefined ? "" : action.payload.id;
            let type = action.payload.type == undefined ? "" : action.payload.type;
            let userType = action.payload.userType;

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/${userType}/find/shipmentadvice/sr`, {
                type,
                orderId: id
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.findShipmentAdviceSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.findShipmentAdviceError(response.data));
            }
        } catch (e) {
            yield put(actions.findShipmentAdviceError("error occurs"));
            console.warn('Some error found in "findShipmentAdviceRequest" action\n', e);
        }
    }
}

//-------------------------------------Chat Notificatio API-----------------------------
export function* commentNotificationRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.commentNotificationClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/comments/notification`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.commentNotificationSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.commentNotificationError(response.data));
            }
        } catch (e) {
            yield put(actions.commentNotificationError("error occurs"));
            console.warn('Some error found in "commentNotificationRequest" action\n', e);
        }
    }
}
//--------------------------------------------------------------------------------------

//-------------------------------------Dashboard Upper Window API-----------------------------
export function* dashboardUpperRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.dashboardUpperClear())
    } else {
        try {
            let dateFrom = action.payload.dateFrom == undefined ? "" : action.payload.dateFrom
            let dateTo = action.payload.dateTo == undefined ? "" : action.payload.dateTo
            let vendorCode = action.payload.vendorCode == undefined ? "" : action.payload.vendorCode

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/dashboard/upr/wdw`, {
                dateFrom,
                dateTo,
                vendorCode
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.dashboardUpperSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.dashboardUpperError(response.data));
            }
        } catch (e) {
            yield put(actions.dashboardUpperError("error occurs"));
            console.warn('Some error found in "dashboardUpperRequest" action\n', e);
        }
    }
}
//--------------------------------------------------------------------------------------

// /-------------------------------------Dashboard Upper Window API-----------------------------
export function* dashboardBottomRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.dashboardBottomClear())
    } else {
        try {
            let dateFrom = action.payload.dateFrom == undefined ? "" : action.payload.dateFrom
            let dateTo = action.payload.dateTo == undefined ? "" : action.payload.dateTo
            let vendorCode = action.payload.vendorCode == undefined ? "" : action.payload.vendorCode

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/dashboard/lwr/wdw`, {
                dateFrom,
                dateTo,
                vendorCode
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.dashboardBottomSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.dashboardBottomError(response.data));
            }
        } catch (e) {
            yield put(actions.dashboardBottomError("error occurs"));
            console.warn('Some error found in "dashboardBottomRequest" action\n', e);
        }
    }
}

// /-------------------------------------Dashboard Unread Comments API-----------------------------
export function* dashboardUnreadCommentRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.dashboardUnreadCommentClear())
    } else {
        try {
            
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/dashboard/get/module/comments`);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.dashboardUnreadCommentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.dashboardUnreadCommentError(response.data));
            }
        } catch (e) {
            yield put(actions.dashboardUnreadCommentError("error occurs"));
            console.warn('Some error found in "dashboardUnreadCommentsRequest" action\n', e);
        }
    }
}

// /-------------------------------------vendor dashboard filter -----------------------------
export function* vendorDashboardFilterRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.vendorDashboardFilterClear())
    } else {
        try {
            let payload = action.payload;
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDOR_USER}/find/info`, payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.vendorDashboardFilterSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.vendorDashboardFilterError(response.data));
            }
        } catch (e) {
            yield put(actions.vendorDashboardFilterError("error occurs"));
            console.warn('Some error found in "vendorDashboardFilterRequest" action\n', e);
        }
    }
}

//--------------------------------------------------------------------------------------
export function* getAllPlansRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAllPlansClear());
    }
    else {
        try {
            const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}/get/pricing/plans`, {
                enterpriseId: action.payload.enterpriseId,
                organisationId: action.payload.organisationId
            });
            const finalResponse = script(response);
            if (finalResponse.success) {

                yield put(actions.getAllPlansSuccess(response.data.data));
            } else if (finalResponse.failure) {

                yield put(actions.getAllPlansError(response.data));
            }
        } catch (e) {
            yield put(actions.getAllPlansError("error occurs"));
            console.warn('Some error found in "getAllPlansRequest" action\n', e);
        }
    }
}

// Get payment summary

export function* getPaymentSummaryRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getPaymentSummaryClear())
    } else {
        try {
            let type = action.payload.type == undefined ? "" : action.payload.type
            let planId = action.payload.planId == undefined ? "" : action.payload.planId
            let noOfUsersPaid = action.payload.noOfUsersPaid == undefined ? "" : action.payload.noOfUsersPaid
            let couponCode = action.payload.couponCode == undefined ? "" : action.payload.couponCode

            const response = yield call(fireAjax, 'POST', `${AUTH_CONFIG.BASE_URL}/getPaymentSummary`, {
                couponCode: couponCode,
                noOfUsersPaid,
                planId,
                type,
                "productName": "DIGI_VEND"
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getPaymentSummarySuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getPaymentSummaryError(response.data));
            }
        } catch (e) {
            yield put(actions.getPaymentSummaryError("error occurs"));
            console.warn('Some error found in "getPaymentSummaryRequest" action\n', e);
        }
    }
}


// Get payment summary

export function* getOrderIdRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getOrderIdClear())
    } else {
        try {
            let payload = action.payload
            const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}/create/subscription`, payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getOrderIdSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getOrderIdError(response.data));
            }
        } catch (e) {
            yield put(actions.getOrderIdError("error occurs"));
            console.warn('Some error found in "getOrderIdRequest" action\n', e);
        }
    }
}

// Get capture payment 

export function* getCapturePaymentRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getCapturePaymentClear())
    } else {
        try {
            let payload = action.payload
            const response = yield call(fireAjax, 'POST', `${AUTH_CONFIG.BASE_URL}/getCapturePayment`, payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getCapturePaymentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getCapturePaymentError(response.data));
            }
        } catch (e) {
            yield put(actions.getCapturePaymentError("error occurs"));
            console.warn('Some error found in "getCapturePaymentRequest" action\n', e);
        }
    }
}

// Multiple Document Download 

export function* multipleDocumentDownloadRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.multipleDocumentDownloadClear())
    } else {
        try {
            let payload = action.payload
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/download/document`, payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.multipleDocumentDownloadSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.multipleDocumentDownloadError(response.data));
            }
        } catch (e) {
            yield put(actions.multipleDocumentDownloadError("error occurs"));
            console.warn('Some error found in "multipleDocumentDownloadRequest" action\n', e);
        }
    }
}
// Email Transcript 

export function* emailTranscriptRequest(action) {
    try {
        let payload = action.payload
        const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/notification/email/send/transcript`, payload);
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.emailTranscriptSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.emailTranscriptError(response.data));
        }
    } catch (e) {
        yield put(actions.emailTranscriptError("error occurs"));
        console.warn('Some error found in "emailTranscriptRequest" action\n', e);
    }
}

// Get capture payment 

export function* updateHeaderRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updateHeaderClear())
    } else {
        try {
            let payload = action.payload
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/headerconfig/update`, payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateHeaderSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.updateHeaderError(response.data));
            }
        } catch (e) {
            yield put(actions.updateHeaderError("error occurs"));
            console.warn('Some error found in "updateHeader" action\n', e);
        }
    }
}


// get Address data API

export function* getAddressRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAddressClear())
    } else {
        try {
            let payload = action.payload
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/find/all/address`,payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAddressSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAddressError(response.data));
            }
        } catch (e) {
            yield put(actions.getAddressError("error occurs"));
            console.warn('Some error found in "getAddress" action\n', e);
        }
    }
}

// get location data API 

export function* getLocationRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getLocationClear())
    } else {
        try {
            let payload = action.payload
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/vendorship/find/all/location`,payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getLocationSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getLocationError(response.data));
            }
        } catch (e) {
            yield put(actions.getLocationError("error occurs"));
            console.warn('Some error found in "getLocation" action\n', e);
        }
    }
}

// get cancellation and rejection reasons ::
export function* getCancelAndRejectReasonRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getCancelAndRejectReasonClear())
    } else {
        try {
            let type = action.payload.type

            // var MASTER_DB_DROPDOWN = localforage.createInstance({
            //     name: "Supplymint Localforage",
            //     storeName: "MASTER_DB_DROPDOWN"
            // });
            // const lfResponse = yield MASTER_DB_DROPDOWN.getItem(type);
            // if (lfResponse !== null) {
            //     yield put(actions.getCancelAndRejectReasonSuccess({
            //         message: "Request completed successfully",
            //         resource: lfResponse
            //     }));
            // }
            // else {
                const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/dropdown/get?type=${type}`);
                const finalResponse = script(response);
                if (finalResponse.success) {
                    yield put(actions.getCancelAndRejectReasonSuccess(response.data.data));
                } else if (finalResponse.failure) {
                    yield put(actions.getCancelAndRejectReasonError(response.data));
                }
            // }
        } catch (e) {
            yield put(actions.getCancelAndRejectReasonError("error occurs"));
            console.warn('Some error found in "getCancelAndRejectReasonRequest" action\n', e);
        }
    }
}
export function* getPaymentStatusRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getPaymentStatusClear())
    } else {
        try {
            let razorpayPaymentId = action.payload.razorpay_payment_id == undefined ? "" : action.payload.razorpay_payment_id
            let razorpaySubscriptionId = action.payload.razorpaySubscriptionId == undefined ? "" : action.payload.razorpaySubscriptionId
            let razorpaySignature = action.payload.razorpaySignature == undefined ? "" : action.payload.razorpaySignature
            
            const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}/verify/transaction`, {
                razorpayPaymentId,
                razorpaySubscriptionId,
                razorpaySignature,
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getPaymentStatusSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getPaymentStatusError(response.data));
            }
        } catch (e) {
            yield put(actions.getPaymentStatusError("error occurs"));
            console.warn('Some error found in "getPaymentStatusRequest" action\n', e);
        }
    }
}

// Get All Comments::
export function* getAllCommentsRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAllCommentsClear())
    } else {
        try {
            let type = action.payload.type == undefined ? "" : action.payload.type
            let search = action.payload.search == undefined ? "" : action.payload.search
            let isUnreadComment = action.payload.isUnreadComment == undefined ? "" : action.payload.isUnreadComment
            let module = action.payload.module == undefined ? "" : action.payload.module
            
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDORPROTAL}/comqc/find/comments`, {
                type,
                search,
                isUnreadComment,
                module
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAllCommentsSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAllCommentsError(response.data));
            }
        } catch (e) {
            yield put(actions.getAllCommentsError("error occurs"));
            console.warn('Some error found in "getAllCommentsRequest" action\n', e);
        }
    }
}

// Get All Ledger Report::
export function* getAllLedgerReportRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAllLedgerReportClear())
    } else {
        try {
            let type = action.payload.type == undefined ? 1 : action.payload.type
            let search = action.payload.search == undefined ? "" : action.payload.search
            let pageNo = action.payload.pageNo == undefined ? 1 : action.payload.pageNo
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy
            let sortedIn = action.payload.sortedIn == undefined ? 1 : action.payload.sortedIn
            let uType = sessionStorage.getItem('uType') == 'VENDOR' ? '/ven' : '/ent' 

            let get = {
                pageNo: pageNo,
                type: type,
                search: search,
                sortedBy,
                sortedIn
            }

            let filter = { ...action.payload.filter }
            let final = { ...get, filter}
            
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${uType}/analytics/find/all/vledger`, final);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAllLedgerReportSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAllLedgerReportError(response.data));
            }
        } catch (e) {
            yield put(actions.getAllLedgerReportError("error occurs"));
            console.warn('Some error found in "getAllLedgerReportRequest" action\n', e);
        }
    }
}

// Get All Sale Report::
export function* getAllSaleReportRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAllSaleReportClear())
    } else {
        try {
            let type = action.payload.type == undefined ? 1 : action.payload.type
            let search = action.payload.search == undefined ? "" : action.payload.search
            let pageNo = action.payload.pageNo == undefined ? 1 : action.payload.pageNo
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy
            let sortedIn = action.payload.sortedIn == undefined ? 1 : action.payload.sortedIn
            let uType = sessionStorage.getItem('uType') == 'VENDOR' ? '/ven' : '/ent' 

            let get = {
                pageNo: pageNo,
                type: type,
                search: search,
                sortedBy,
                sortedIn
            }

            let filter = { ...action.payload.filter }
            let final = { ...get, filter}
            
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${uType}/analytics/find/all/salesreport`, final);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAllSaleReportSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAllSaleReportError(response.data));
            }
        } catch (e) {
            yield put(actions.getAllSaleReportError("error occurs"));
            console.warn('Some error found in "getAllSaleReportRequest" action\n', e);
        }
    }
}

// Get All Stock Report::
export function* getAllStockReportRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAllStockReportClear())
    } else {
        try {
            let type = action.payload.type == undefined ? 1 : action.payload.type
            let search = action.payload.search == undefined ? "" : action.payload.search
            let pageNo = action.payload.pageNo == undefined ? 1 : action.payload.pageNo
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy
            let sortedIn = action.payload.sortedIn == undefined ? 1 : action.payload.sortedIn
            let uType = sessionStorage.getItem('uType') == 'VENDOR' ? '/ven' : '/ent' 

            let get = {
                pageNo: pageNo,
                type: type,
                search: search,
                sortedBy,
                sortedIn
            }

            let filter = { ...action.payload.filter }
            let final = { ...get, filter}
            
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${uType}/analytics/find/all/stockreport`, final);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAllStockReportSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAllStockReportError(response.data));
            }
        } catch (e) {
            yield put(actions.getAllStockReportError("error occurs"));
            console.warn('Some error found in "getAllStockReportRequest" action\n', e);
        }
    }
}

// Get All OutStanding Report::
export function* getAllOutStandingReportRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAllOutStandingReportClear())
    } else {
        try {
            let type = action.payload.type == undefined ? 1 : action.payload.type
            let search = action.payload.search == undefined ? "" : action.payload.search
            let pageNo = action.payload.pageNo == undefined ? 1 : action.payload.pageNo
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy
            let sortedIn = action.payload.sortedIn == undefined ? 1 : action.payload.sortedIn
            let uType = sessionStorage.getItem('uType') == 'VENDOR' ? '/ven' : '/ent' 

            let get = {
                pageNo: pageNo,
                type: type,
                search: search,
                sortedBy,
                sortedIn
            }

            let filter = { ...action.payload.filter }
            let final = { ...get, filter}
            
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${uType}/analytics/find/all/voutstanding`, final);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAllOutStandingReportSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAllOutStandingReportError(response.data));
            }
        } catch (e) {
            yield put(actions.getAllOutStandingReportError("error occurs"));
            console.warn('Some error found in "getAllOutStandingReportRequest" action\n', e);
        }
    }
}
// Get Ledger Report after upper filter applied::
export function* getLedgerFilterReportRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getLedgerFilterReportClear())
    } else {
        try {
            let vendorcode = action.payload.vendorCode == undefined ? 1 : action.payload.vendorCode
            let datefrom = action.payload.dateFrom == undefined ? "" : action.payload.dateFrom
            let dateto = action.payload.dateTo == undefined ? "" : action.payload.dateTo

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ent/analytics/get/ven/fin/tran`, {
                vendorcode,
                datefrom,
                dateto
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getLedgerFilterReportSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getLedgerFilterReportError(response.data));
            }
        } catch (e) {
            yield put(actions.getLedgerFilterReportError("error occurs"));
            console.warn('Some error found in "getLedgerFilterReportRequest" action\n', e);
        }
    }
}

export function* getVendorActivityRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getVendorActivityClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ent/analytics/find/all/vendor/activity`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getVendorActivitySuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getVendorActivityError(response.data));
            }
        } catch (e) {
            yield put(actions.getVendorActivityError("error occurs"));
            console.warn('Some error found in "getVendorActivityRequest" action\n', e);
        }
    }
}

export function* getPoStockReportRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getPoStockReportClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/vstockreport`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getPoStockReportSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getPoStockReportError(response.data));
            }
        } catch (e) {
            yield put(actions.getPoStockReportError("error occurs"));
            console.warn('Some error found in "getPoStockReportRequest" action\n', e);
        }
    }
}

export function* getVendorLogsRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getVendorLogsClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/ent/analytics/find/all/vendor/logs`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getVendorLogsSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getVendorLogsError(response.data));
            }
        } catch (e) {
            yield put(actions.getVendorLogsError("error occurs"));
            console.warn('Some error found in "getVendorLogsRequest" action\n', e);
        }
    }
}

export function* getPoColorMapRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getPoColorMapClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/get/icodecolmap`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getPoColorMapSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getPoColorMapError(response.data));
            }
        } catch (e) {
            yield put(actions.getPoColorMapError("error occurs"));
            console.warn('Some error found in "getPoColorMapRequest" action\n', e);
        }
    }
}

export function* createPoArticleRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.createPoArticleClear())
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/multiple/article/create`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.createPoArticleSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.createPoArticleError(response.data));
            }
        } catch (e) {
            yield put(actions.createPoArticleError("error occurs"));
            console.warn('Some error found in "createPoArticleRequest" action\n', e);
        }
    }
}