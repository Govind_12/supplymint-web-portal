import { createAction } from "redux-actions";
import * as constants from "./constants";
import { create } from "domain";

// login
export const loginRequest = createAction(constants.LOGIN_REQUEST);
export const loginSuccess = createAction(constants.LOGIN_SUCCESS);
export const loginError = createAction(constants.LOGIN_ERROR);
export const loginClear = createAction(constants.LOGIN_CLEAR);

export const switchEntRequest = createAction(constants.SWITCH_ENT_REQUEST);
export const switchEntSuccess = createAction(constants.SWITCH_ENT_SUCCESSS);
export const switchEntError = createAction(constants.SWITCH_ENT_ERROR);
export const switchEntClear = createAction(constants.SWITCH_ENT_CLEAR)

export const forgotPasswordRequest = createAction(constants.FORGOT_PASSWORD_REQUEST);
export const forgotPasswordSuccess = createAction(constants.FORGOT_PASSWORD_SUCCESS);
export const forgotPasswordError = createAction(constants.FORGOT_PASSWORD_ERROR);
export const forgotPasswordClear = createAction(constants.FORGOT_PASSWORD_CLEAR);

export const changePasswordRequest = createAction(constants.CHANGE_PASSWORD_REQUEST);
export const changePasswordSuccess = createAction(constants.CHANGE_PASSWORD_SUCCESS);
export const changePasswordError = createAction(constants.CHANGE_PASSWORD_ERROR);
export const changePasswordClear = createAction(constants.CHANGE_PASSWORD_CLEAR);

export const resetPasswordRequest = createAction(constants.RESET_PASSWORD_REQUEST);
export const resetPasswordSuccess = createAction(constants.RESET_PASSWORD_SUCCESS);
export const resetPasswordError = createAction(constants.RESET_PASSWORD_ERROR);
export const resetPasswordClear = createAction(constants.RESET_PASSWORD_CLEAR)

export const forgotUserRequest = createAction(constants.FORGOT_USER_REQUEST);
export const forgotUserSuccess = createAction(constants.FORGOT_USER_SUCCESS);
export const forgotUserError = createAction(constants.FORGOT_USER_ERROR);
export const forgotUserClear = createAction(constants.FORGOT_PASSWORD_CLEAR);

export const activateUserRequest = createAction(constants.ACTIVATE_USER_REQUEST);
export const activateUserSuccess = createAction(constants.ACTIVATE_USER_SUCCESS);
export const activateUserError = createAction(constants.ACTIVATE_USER_ERROR);
export const activateUserClear = createAction(constants.ACTIVATE_USER_CLEAR);


export const profileImageRequest = createAction(constants.PROFILE_IMAGE_REQUEST);
export const profileImageSuccess = createAction(constants.PROFILE_IMAGE_SUCCESS);
export const profileImageError = createAction(constants.PROFILE_IMAGE_ERROR);
export const profileImageClear = createAction(constants.PROFILE_IMAGE_CLEAR);

export const dashboardTilesRequest = createAction(constants.DASHBOARD_TILES_REQUEST);
export const dashboardTilesSuccess = createAction(constants.DASHBOARD_TILES_SUCCESS);
export const dashboardTilesError = createAction(constants.DASHBOARD_TILES_ERROR);
export const dashboardTilesClear = createAction(constants.DASHBOARD_TILES_CLEAR);

export const storesArticlesRequest = createAction(constants.STORES_ARTICLES_REQUEST);
export const storesArticlesSuccess = createAction(constants.STORES_ARTICLES_SUCCESS);
export const storesArticlesError = createAction(constants.STORES_ARTICLES_ERROR);
export const storesArticlesClear = createAction(constants.STORES_ARTICLES_CLEAR);

export const salesTrendGraphRequest = createAction(constants.SALES_TREND_GRAPH_REQUEST);
export const salesTrendGraphSuccess = createAction(constants.SALES_TREND_GRAPH_SUCCESS);
export const salesTrendGraphError = createAction(constants.SALES_TREND_GRAPH_ERROR);
export const salesTrendGraphClear = createAction(constants.SALES_TREND_GRAPH_CLEAR);

export const slowFastArticleRequest = createAction(constants.SLOW_FAST_ARTICLE_REQUEST);
export const slowFastArticleSuccess = createAction(constants.SLOW_FAST_ARTICLE_SUCCESS);
export const slowFastArticleError = createAction(constants.SLOW_FAST_ARTICLE_ERROR);
export const slowFastArticleClear = createAction(constants.SLOW_FAST_ARTICLE_CLEAR);
//login ends

//sample
export const sampleRequest = createAction(constants.SAMPLE_REQUEST);
export const sampleSuccess = createAction(constants.SAMPLE_SUCCESS);
export const sampleError = createAction(constants.SAMPLE_ERROR);
export const sampleClear = createAction(constants.SAMPLE_CLEAR)
//sample ends

//administration
export const rolesRequest = createAction(constants.ROLES_REQUEST);
export const rolesSuccess = createAction(constants.ROLES_SUCCESS);
export const rolesError = createAction(constants.ROLES_ERROR);
export const rolesClear = createAction(constants.ROLES_CLEAR)

export const addRolesClear = createAction(constants.ADD_ROLES_CLEAR);
export const addRolesRequest = createAction(constants.ADD_ROLES_REQUEST);
export const addRolesSuccess = createAction(constants.ADD_ROLES_SUCCESS);
export const addRolesError = createAction(constants.ADD_ROLES_ERROR);


export const editRolesClear = createAction(constants.EDIT_ROLES_CLEAR);
export const editRolesRequest = createAction(constants.EDIT_ROLES_REQUEST);
export const editRolesSuccess = createAction(constants.EDIT_ROLES_SUCCESS);
export const editRolesError = createAction(constants.EDIT_ROLES_ERROR);

export const deleteRolesRequest = createAction(constants.DELETE_ROLES_REQUEST);
export const deleteRolesSuccess = createAction(constants.DELETE_ROLES_SUCCESS);
export const deleteRolesError = createAction(constants.DELETE_ROLES_ERROR);
export const deleteRolesClear = createAction(constants.DELETE_ROLES_CLEAR)

export const siteMappingRequest = createAction(constants.SITE_MAPPING_REQUEST);
export const siteMappingSuccess = createAction(constants.SITE_MAPPING_SUCCESS);
export const siteMappingError = createAction(constants.SITE_MAPPING_ERROR);
export const siteMappingClear = createAction(constants.SITE_MAPPING_CLEAR);

export const addSiteMappingRequest = createAction(constants.ADD_SITE_MAPPING_REQUEST);
export const addSiteMappingClear = createAction(constants.ADD_SITE_MAPPING_CLEAR);
export const addSiteMappingSuccess = createAction(constants.ADD_SITE_MAPPING_SUCCESS);
export const addSiteMappingError = createAction(constants.ADD_SITE_MAPPING_ERROR);

export const editSiteMappingRequest = createAction(constants.EDIT_SITE_MAPPING_REQUEST);
export const editSiteMappingClear = createAction(constants.EDIT_SITE_MAPPING_CLEAR);
export const editSiteMappingSuccess = createAction(constants.EDIT_SITE_MAPPING_SUCCESS);
export const editSiteMappingError = createAction(constants.EDIT_SITE_MAPPING_ERROR);

export const deleteSiteMappingRequest = createAction(constants.DELETE_SITE_MAPPING_REQUEST);
export const deleteSiteMappingClear = createAction(constants.DELETE_SITE_MAPPING_CLEAR);
export const deleteSiteMappingSuccess = createAction(constants.DELETE_SITE_MAPPING_SUCCESS);
export const deleteSiteMappingError = createAction(constants.DELETE_SITE_MAPPING_ERROR);

export const organizationRequest = createAction(constants.ORGANIZATION_REQUEST);
export const organizationSuccess = createAction(constants.ORGANIZATION_SUCCESS);
export const organizationError = createAction(constants.ORGANIZATION_ERROR);
export const organizationClear = createAction(constants.ORGANIZATION_CLEAR);

export const addOrganizationClear = createAction(constants.ADD_ORGANIZATION_CLEAR);
export const addOrganizationRequest = createAction(constants.ADD_ORGANIZATION_REQUEST);
export const addOrganizationSuccess = createAction(constants.ADD_ORGANIZATION_SUCCESS);
export const addOrganizationError = createAction(constants.ADD_ORGANIZATION_ERROR);

export const editOrganizationClear = createAction(constants.EDIT_ORGANIZATION_CLEAR);
export const editOrganizationRequest = createAction(constants.EDIT_ORGANIZATION_REQUEST);
export const editOrganizationSuccess = createAction(constants.EDIT_ORGANIZATION_SUCCESS);
export const editOrganizationError = createAction(constants.EDIT_ORGANIZATION_ERROR);

export const deleteOrganizationClear = createAction(constants.DELETE_ORGANIZATION_CLEAR);
export const deleteOrganizationRequest = createAction(constants.DELETE_ORGANIZATION_REQUEST);
export const deleteOrganizationSuccess = createAction(constants.DELETE_ORGANIZATION_SUCCESS);
export const deleteOrganizationError = createAction(constants.DELETE_ORGANIZATION_ERROR);

export const userRequest = createAction(constants.USER_REQUEST);
export const userSuccess = createAction(constants.USER_SUCCESS);
export const userError = createAction(constants.USER_ERROR);
export const userClear = createAction(constants.USER_CLEAR);

export const addUserClear = createAction(constants.ADD_USER_CLEAR);
export const addUserRequest = createAction(constants.ADD_USER_REQUEST);
export const addUserSuccess = createAction(constants.ADD_USER_SUCCESS);
export const addUserError = createAction(constants.ADD_USER_ERROR);

export const editUserClear = createAction(constants.EDIT_USER_CLEAR);
export const editUserRequest = createAction(constants.EDIT_USER_REQUEST);
export const editUserSuccess = createAction(constants.EDIT_USER_SUCCESS);
export const editUserError = createAction(constants.EDIT_USER_ERROR);

export const deleteUserClear = createAction(constants.DELETE_USER_CLEAR);
export const deleteUserRequest = createAction(constants.DELETE_USER_REQUEST);
export const deleteUserSuccess = createAction(constants.DELETE_USER_SUCCESS);
export const deleteUserError = createAction(constants.DELETE_USER_ERROR);


export const getUserNameRequest = createAction(constants.GET_USERNAME_REQUEST);
export const getUserNameSuccess = createAction(constants.GET_USERNAME_SUCCESS);
export const getUserNameError = createAction(constants.GET_USERNAME_ERROR);
export const getUserNameClear = createAction(constants.GET_USERNAME_CLEAR);


export const updateProfileRequest = createAction(constants.UPDATE_PROFILE_REQUEST);
export const updateProfileSuccess = createAction(constants.UPDATE_PROFILE_SUCCESS);
export const updateProfileError = createAction(constants.UPDATE_PROFILE_ERROR);
export const updateProfileClear = createAction(constants.UPDATE_PROFILE_CLEAR)

export const siteRequest = createAction(constants.SITE_REQUEST);
export const siteSuccess = createAction(constants.SITE_SUCCESS);
export const siteError = createAction(constants.SITE_ERROR);
export const siteClear = createAction(constants.SITE_CLEAR)

export const addSiteRequest = createAction(constants.ADD_SITE_REQUEST);
export const addSiteClear = createAction(constants.ADD_SITE_CLEAR);
export const addSiteSuccess = createAction(constants.ADD_SITE_SUCCESS);
export const addSiteError = createAction(constants.ADD_SITE_ERROR);

export const editSiteRequest = createAction(constants.EDIT_SITE_REQUEST);
export const editSiteClear = createAction(constants.EDIT_SITE_CLEAR);
export const editSiteSuccess = createAction(constants.EDIT_SITE_SUCCESS);
export const editSiteError = createAction(constants.EDIT_SITE_ERROR);

export const deleteSiteRequest = createAction(constants.DELETE_SITE_REQUEST);
export const deleteSiteClear = createAction(constants.DELETE_SITE_CLEAR);
export const deleteSiteSuccess = createAction(constants.DELETE_SITE_SUCCESS);
export const deleteSiteError = createAction(constants.DELETE_SITE_ERROR);

export const fromToRequest = createAction(constants.FROM_TO_REQUEST);
export const fromToSuccess = createAction(constants.FROM_TO_SUCCESS);
export const fromToError = createAction(constants.FROM_TO_ERROR);
export const fromToClear = createAction(constants.FROM_TO_CLEAR);

export const allRoleRequest = createAction(constants.ALL_ROLE_REQUEST);
export const allRoleSuccess = createAction(constants.ALL_ROLE_SUCCESS);
export const allRoleError = createAction(constants.ALL_ROLE_ERROR);
export const allRoleClear = createAction(constants.ALL_ROLE_CLEAR);

export const allUserRequest = createAction(constants.ALL_USER_REQUEST);
export const allUserSuccess = createAction(constants.ALL_USER_SUCCESS);
export const allUserError = createAction(constants.ALL_USER_ERROR);
export const allUserClear = createAction(constants.ALL_USER_CLEAR)

export const userStatusRequest = createAction(constants.USER_STATUS_REQUEST);
export const userStatusSuccess = createAction(constants.USER_STATUS_SUCCESS);
export const userStatusError = createAction(constants.USER_STATUS_ERROR);
export const userStatusClear = createAction(constants.USER_STATUS_CLEAR);


export const downloadRequest = createAction(constants.DOWNLOAD_REQUEST);
export const downloadSuccess = createAction(constants.DOWNLOAD_SUCCESS);
export const downloadError = createAction(constants.DOWNLOAD_ERROR);
export const downloadClear = createAction(constants.DOWNLOAD_CLEAR)

export const dataSyncRequest = createAction(constants.DATA_SYNC_REQUEST);
export const dataSyncSuccess = createAction(constants.DATA_SYNC_SUCCESS);
export const dataSyncError = createAction(constants.DATA_SYNC_ERROR);
export const dataSyncClear = createAction(constants.DATA_SYNC_CLEAR)
//administration ends


export const sketchersUploadRequest = createAction(constants.SKETCHERS_UPLOAD_REQUEST);
export const sketchersUploadSuccess = createAction(constants.SKETCHERS_UPLOAD_SUCCESS);
export const sketchersUploadError = createAction(constants.SKETCHERS_UPLOAD_ERROR);
export const sketchersUploadClear = createAction(constants.SKETCHERS_UPLOAD_CLEAR);

export const saveSketchersRequest = createAction(constants.SAVE_SKETCHERS_REQUEST);
export const saveSketchersSuccess = createAction(constants.SAVE_SKETCHERS_SUCCESS);
export const saveSketchersError = createAction(constants.SAVE_SKETCHERS_ERROR);
export const saveSketchersClear = createAction(constants.SAVE_SKETCHERS_CLEAR);

export const getTemplateRequest = createAction(constants.GET_TEMPLATE_REQUEST);
export const getTemplateSuccess = createAction(constants.GET_TEMPLATE_SUCCESS);
export const getTemplateError = createAction(constants.GET_TEMPLATE_ERROR);
export const getTemplateClear = createAction(constants.GET_TEMPLATE_CLEAR);

//vendor Begins

export const vendorRequest = createAction(constants.VENDOR_REQUEST);
export const vendorSuccess = createAction(constants.VENDOR_SUCCESS);
export const vendorError = createAction(constants.VENDOR_ERROR);
export const vendorClear = createAction(constants.VENDOR_CLEAR)

export const deleteVendorRequest = createAction(constants.DELETE_VENDOR_REQUEST);
export const deleteVendorClear = createAction(constants.DELETE_VENDOR_CLEAR);
export const deleteVendorSuccess = createAction(constants.DELETE_VENDOR_SUCCESS);
export const deleteVendorError = createAction(constants.DELETE_VENDOR_ERROR);

//add vendors
export const addVendorRequest = createAction(constants.ADD_VENDOR_REQUEST);
export const addVendorClear = createAction(constants.ADD_VENDOR_CLEAR);
export const addVendorSuccess = createAction(constants.ADD_VENDOR_SUCCESS);
export const addVendorError = createAction(constants.ADD_VENDOR_ERROR);
//edit vendors
export const editVendorRequest = createAction(constants.EDIT_VENDOR_REQUEST);
export const editVendorClear = createAction(constants.EDIT_VENDOR_CLEAR);
export const editVendorSuccess = createAction(constants.EDIT_VENDOR_SUCCESS);
export const editVendorError = createAction(constants.EDIT_VENDOR_ERROR);


//purchase Indent begins

//division-section-department
export const divisionSectionDepartmentRequest = createAction(constants.DIVISION_SECTION_DEPARTMENT_REQUEST);
export const divisionSectionDepartmentSuccess = createAction(constants.DIVISION_SECTION_DEPARTMENT_SUCCESS);
export const divisionSectionDepartmentError = createAction(constants.DIVISION_SECTION_DEPARTMENT_ERROR);
export const divisionSectionDepartmentClear = createAction(constants.DIVISION_SECTION_DEPARTMENT_CLEAR);

export const transporterRequest = createAction(constants.TRANSPORTER_REQUEST);
export const transporterSuccess = createAction(constants.TRANSPORTER_SUCCESS);
export const transporterError = createAction(constants.TRANSPORTER_ERROR);

export const getTransporterRequest = createAction(constants.GET_TRANSPORTER_REQUEST);
export const getTransporterSuccess = createAction(constants.GET_TRANSPORTER_SUCCESS);
export const getTransporterError = createAction(constants.GET_TRANSPORTER_ERROR);
export const getTransporterClear = createAction(constants.GET_TRANSPORTER_CLEAR);


export const supplierRequest = createAction(constants.SUPPLIER_REQUEST);
export const supplierSuccess = createAction(constants.SUPPLIER_SUCCESS);
export const supplierError = createAction(constants.SUPPLIER_ERROR);
export const supplierClear = createAction(constants.SUPPLIER_CLEAR);

export const purchaseTermRequest = createAction(constants.PURCHASETERM_REQUEST);
export const purchaseTermSuccess = createAction(constants.PURCHASETERM_SUCCESS);
export const purchaseTermError = createAction(constants.PURCHASETERM_ERROR);
export const purchaseTermClear = createAction(constants.PURCHASETERM_CLEAR);


export const getPurchaseTermRequest = createAction(constants.GET_PURCHASETERM_REQUEST);
export const getPurchaseTermSuccess = createAction(constants.GET_PURCHASETERM_SUCCESS);
export const getPurchaseTermError = createAction(constants.GET_PURCHASETERM_ERROR);
export const getPurchaseTermClear = createAction(constants.GET_PURCHASETERM_CLEAR);

//lead time
export const leadTimeRequest = createAction(constants.LEAD_TIME_REQUEST);
export const leadTimeSuccess = createAction(constants.LEAD_TIME_SUCCESS);
export const leadTimeError = createAction(constants.LEAD_TIME_ERROR);
export const leadTimeClear = createAction(constants.LEAD_TIME_CLEAR);

export const markUpRequest = createAction(constants.MARKUP_REQUEST);
export const markUpSuccess = createAction(constants.MARKUP_SUCCESS);
export const markUpError = createAction(constants.MARKUP_ERROR)
export const markUpClear = createAction(constants.MARKUP_CLEAR)


//vendor_mrp
export const vendorMrpClear = createAction(constants.VENDOR_MRP_CLEAR);
export const vendorMrpRequest = createAction(constants.VENDOR_MRP_REQUEST);
export const vendorMrpSuccess = createAction(constants.VENDOR_MRP_SUCCESS);
export const vendorMrpError = createAction(constants.VENDOR_MRP_ERROR);

//size
export const sizeClear = createAction(constants.SIZE_CLEAR);
export const sizeRequest = createAction(constants.SIZE_REQUEST);
export const sizeSuccess = createAction(constants.SIZE_SUCCESS);
export const sizeError = createAction(constants.SIZE_ERROR);

//color
export const colorClear = createAction(constants.COLOR_CLEAR);
export const colorRequest = createAction(constants.COLOR_REQUEST);
export const colorSuccess = createAction(constants.COLOR_SUCCESS);
export const colorError = createAction(constants.COLOR_ERROR);

//quantity
export const quantityRequest = createAction(constants.QUANTITY_REQUEST);
export const quantitySuccess = createAction(constants.QUANTITY_SUCCESS);
export const quantityError = createAction(constants.QUANTITY_ERROR);
export const quantityClear = createAction(constants.QUANTITY_CLEAR);


//total
export const totalRequest = createAction(constants.TOTAL_REQUEST);
export const totalSuccess = createAction(constants.TOTAL_SUCCESS);
export const totalError = createAction(constants.TOTAL_ERROR);
export const totalClear = createAction(constants.TOTAL_CLEAR);

// image
export const imageRequest = createAction(constants.IMAGE_REQUEST);
export const imageSuccess = createAction(constants.IMAGE_SUCCESS);
export const imageError = createAction(constants.IMAGE_ERROR);
export const imageClear = createAction(constants.IMAGE_CLEAR);

// otb
export const otbRequest = createAction(constants.OTB_REQUEST);
export const otbSuccess = createAction(constants.OTB_SUCCESS);
export const otbError = createAction(constants.OTB_ERROR);
export const otbClear = createAction(constants.OTB_CLEAR);

// PROC analytics otb report
export const analyticsOtbRequest = createAction(constants.ANALYTICS_OTB_REQUEST);
export const analyticsOtbSuccess = createAction(constants.ANALYTICS_OTB_SUCCESS);
export const analyticsOtbError = createAction(constants.ANALYTICS_OTB_ERROR);
export const analyticsOtbClear = createAction(constants.ANALYTICS_OTB_CLEAR);


// lineitems

export const lineItemRequest = createAction(constants.LINEITEM_REQUEST);
export const lineItemSuccess = createAction(constants.LINEITEM_SUCCESS);
export const lineItemError = createAction(constants.LINEITEM_ERROR);
export const lineItemClear = createAction(constants.LINEITEM_CLEAR)

// __________________PO Action___________

export const supplierPoRequest = createAction(constants.SUPPLIERPO_REQUEST);
export const supplierPoSuccess = createAction(constants.SUPPLIERPO_SUCCESS);
export const supplierPoError = createAction(constants.SUPPLIERPO_ERROR);
export const supplierPoClear = createAction(constants.SUPPLIERPO_CLEAR);

export const leadTimePoRequest = createAction(constants.LEAD_TIME_PO_REQUEST);
export const leadTimePoSuccess = createAction(constants.LEAD_TIME_PO_SUCCESS);
export const leadTimePoError = createAction(constants.LEAD_TIME_PO_ERROR);
export const leadTimePoClear = createAction(constants.LEAD_TIME_PO_CLEAR);

export const vendorMrpPoClear = createAction(constants.VENDOR_MRP_PO_CLEAR);
export const vendorMrpPoRequest = createAction(constants.VENDOR_MRP_PO_REQUEST);
export const vendorMrpPoSuccess = createAction(constants.VENDOR_MRP_PO_SUCCESS);
export const vendorMrpPoError = createAction(constants.VENDOR_MRP_PO_ERROR);

export const loadIndentClear = createAction(constants.LOAD_INDENT_CLEAR);
export const loadIndentRequest = createAction(constants.LOAD_INDENT_REQUEST);
export const loadIndentSuccess = createAction(constants.LOAD_INDENT_SUCCESS);
export const loadIndentError = createAction(constants.LOAD_INDENT_ERROR);
//pi_itemDetails


export const piCreateRequest = createAction(constants.PICREATE_REQUEST);
export const piCreateSuccess = createAction(constants.PICREATE_SUCCESS);
export const piCreateError = createAction(constants.PICREATE_ERROR);
export const piCreateClear = createAction(constants.PICREATE_CLEAR);

export const mappingExcelUploadRequest = createAction(constants.MAPPING_EXCEL_UPLOAD_REQUEST);
export const mappingExcelUploadSuccess = createAction(constants.MAPPING_EXCEL_UPLOAD_SUCCESS);
export const mappingExcelUploadError = createAction(constants.MAPPING_EXCEL_UPLOAD_ERROR);
export const mappingExcelUploadClear = createAction(constants.MAPPING_EXCEL_UPLOAD_CLEAR);

export const mappingExcelStatusRequest = createAction(constants.MAPPING_EXCEL_STATUS_REQUEST);
export const mappingExcelStatusSuccess = createAction(constants.MAPPING_EXCEL_STATUS_SUCCESS);
export const mappingExcelStatusError = createAction(constants.MAPPING_EXCEL_STATUS_ERROR);
export const mappingExcelStatusClear = createAction(constants.MAPPING_EXCEL_STATUS_CLEAR);

export const mappingExcelExportRequest = createAction(constants.MAPPING_EXCEL_EXPORT_REQUEST);
export const mappingExcelExportSuccess = createAction(constants.MAPPING_EXCEL_EXPORT_SUCCESS);
export const mappingExcelExportError = createAction(constants.MAPPING_EXCEL_EXPORT_ERROR);
export const mappingExcelExportClear = createAction(constants.MAPPING_EXCEL_EXPORT_CLEAR);

export const piUpdateRequest = createAction(constants.PIUPDATE_REQUEST);
export const piUpdateSuccess = createAction(constants.PIUPDATE_SUCCESS);
export const piUpdateError = createAction(constants.PIUPDATE_ERROR);
export const piUpdateClear = createAction(constants.PIUPDATE_CLEAR);

export const poItemcodeRequest = createAction(constants.POITEMCODE_REQUEST);
export const poItemcodeSuccess = createAction(constants.POITEMCODE_SUCCESS);
export const poItemcodeError = createAction(constants.POITEMCODE_ERROR);
export const poItemcodeClear = createAction(constants.POITEMCODE_CLEAR);

export const udfTypeRequest = createAction(constants.UDF_TYPE_REQUEST);
export const udfTypeSuccess = createAction(constants.UDF_TYPE_SUCCESS);
export const udfTypeError = createAction(constants.UDF_TYPE_ERROR);
export const udfTypeClear = createAction(constants.UDF_TYPE_CLEAR);

export const getUdfMappingRequest = createAction(constants.GET_UDF_MAPPING_REQUEST);
export const getUdfMappingSuccess = createAction(constants.GET_UDF_MAPPING_SUCCESS);
export const getUdfMappingError = createAction(constants.GET_UDF_MAPPING_ERROR);
export const getUdfMappingClear = createAction(constants.GET_UDF_MAPPING_CLEAR);

export const poUdfMappingRequest = createAction(constants.PO_UDF_MAPPING_REQUEST);
export const poUdfMappingSuccess = createAction(constants.PO_UDF_MAPPING_SUCCESS);
export const poUdfMappingError = createAction(constants.PO_UDF_MAPPING_ERROR);
export const poUdfMappingClear = createAction(constants.PO_UDF_MAPPING_CLEAR);

export const updateUdfMappingRequest = createAction(constants.UPDATE_UDF_MAPPING_REQUEST);
export const updateUdfMappingSuccess = createAction(constants.UPDATE_UDF_MAPPING_SUCCESS);
export const updateUdfMappingError = createAction(constants.UPDATE_UDF_MAPPING_ERROR);
export const updateUdfMappingClear = createAction(constants.UPDATE_UDF_MAPPING_CLEAR);


export const updateItemUdfRequest = createAction(constants.UPDATE_ITEM_UDF_REQUEST);
export const updateItemUdfSuccess = createAction(constants.UPDATE_ITEM_UDF_SUCCESS);
export const updateItemUdfError = createAction(constants.UPDATE_ITEM_UDF_ERROR);
export const updateItemUdfClear = createAction(constants.UPDATE_ITEM_UDF_CLEAR);

export const updateItemCatDescRequest = createAction(constants.UPDATE_ITEM_CAT_DESC_REQUEST);
export const updateItemCatDescSuccess = createAction(constants.UPDATE_ITEM_CAT_DESC_SUCCESS);
export const updateItemCatDescError = createAction(constants.UPDATE_ITEM_CAT_DESC_ERROR);
export const updateItemCatDescClear = createAction(constants.UPDATE_ITEM_CAT_DESC_CLEAR);



export const getItemUdfRequest = createAction(constants.GET_ITEM_UDF_REQUEST);
export const getItemUdfSuccess = createAction(constants.GET_ITEM_UDF_SUCCESS);
export const getItemUdfError = createAction(constants.GET_ITEM_UDF_ERROR);
export const getItemUdfClear = createAction(constants.GET_ITEM_UDF_CLEAR);


export const poHistoryRequest = createAction(constants.PO_HISTORY_REQUEST);
export const poHistorySuccess = createAction(constants.PO_HISTORY_SUCCESS);
export const poHistoryError = createAction(constants.PO_HISTORY_ERROR);
export const poHistoryClear = createAction(constants.PO_HISTORY_CLEAR);
export const poHistoryDataUpdate = createAction(constants.PO_HISTORY_DATA_UPDATE);

export const poCreateRequest = createAction(constants.PO_CREATE_REQUEST);
export const poCreateSuccess = createAction(constants.PO_CREATE_SUCCESS);
export const poCreateError = createAction(constants.PO_CREATE_ERROR);
export const poCreateClear = createAction(constants.PO_CREATE_CLEAR);

export const poSizeRequest = createAction(constants.PO_SIZE_REQUEST);
export const poSizeSuccess = createAction(constants.PO_SIZE_SUCCESS);
export const poSizeError = createAction(constants.PO_SIZE_ERROR);
export const poSizeClear = createAction(constants.PO_SIZE_CLEAR);

export const piAddNewRequest = createAction(constants.PI_ADD_NEW_REQUEST);
export const piAddNewSuccess = createAction(constants.PI_ADD_NEW_SUCCESS);
export const piAddNewError = createAction(constants.PI_ADD_NEW_ERROR);
export const piAddNewClear = createAction(constants.PI_ADD_NEW_CLEAR);




export const hsnCodeRequest = createAction(constants.HSN_CODE_REQUEST);
export const hsnCodeSuccess = createAction(constants.HSN_CODE_SUCCESS);
export const hsnCodeError = createAction(constants.HSN_CODE_ERROR);
export const hsnCodeClear = createAction(constants.HSN_CODE_CLEAR)

export const createUdfSettingRequest = createAction(constants.CREATE_UDF_SETTING_REQUEST);
export const createUdfSettingSuccess = createAction(constants.CREATE_UDF_SETTING_SUCCESS);
export const createUdfSettingError = createAction(constants.CREATE_UDF_SETTING_ERROR);
export const createUdfSettingClear = createAction(constants.CREATE_UDF_SETTING_CLEAR);
// export const piDownloadRequest = createAction(constants.PIDOWNLOAD_REQUEST);
// export const piDownloadSuccess = createAction(constants.PIDOWNLOAD_SUCCESS);
// export const piDownloadError = createAction(constants.PIDOWNLOAD_ERROR);
// export const piDownloadClear = createAction(constants.PIDOWNLOAD_CLEAR)

//pobegins

export const catDescDropdownRequest = createAction(constants.CAT_DESC_DROPDOWN_REQUEST);
export const catDescDropdownSuccess = createAction(constants.CAT_DESC_DROPDOWN_SUCCESS);
export const catDescDropdownError = createAction(constants.CAT_DESC_DROPDOWN_ERROR);
export const catDescDropdownClear = createAction(constants.CAT_DESC_DROPDOWN_CLEAR);

export const activeCatDescRequest = createAction(constants.ACTIVE_CAT_DESC_REQUEST);
export const activeCatDescSuccess = createAction(constants.ACTIVE_CAT_DESC_SUCCESS);
export const activeCatDescError = createAction(constants.ACTIVE_CAT_DESC_ERROR);
export const activeCatDescClear = createAction(constants.ACTIVE_CAT_DESC_CLEAR);

export const activeUdfRequest = createAction(constants.ACTIVE_UDF_REQUEST);
export const activeUdfSuccess = createAction(constants.ACTIVE_UDF_SUCCESS);
export const activeUdfError = createAction(constants.ACTIVE_UDF_ERROR);
export const activeUdfClear = createAction(constants.ACTIVE_UDF_CLEAR);

export const getCatDescUdfRequest = createAction(constants.GET_CATDESC_UDF_REQUEST);
export const getCatDescUdfSuccess = createAction(constants.GET_CATDESC_UDF_SUCCESS);
export const getCatDescUdfError = createAction(constants.GET_CATDESC_UDF_ERROR);
export const getCatDescUdfClear = createAction(constants.GET_CATDESC_UDF_CLEAR);

export const itemCatDescUdfRequest = createAction(constants.ITEM_CATDESC_UDF_REQUEST);
export const itemCatDescUdfSuccess = createAction(constants.ITEM_CATDESC_UDF_SUCCESS);
export const itemCatDescUdfError = createAction(constants.ITEM_CATDESC_UDF_ERROR);
export const itemCatDescUdfClear = createAction(constants.ITEM_CATDESC_UDF_CLEAR);

export const itemUdfMappingRequest = createAction(constants.ITEM_UDF_MAPPING_REQUEST);
export const itemUdfMappingSuccess = createAction(constants.ITEM_UDF_MAPPING_SUCCESS);
export const itemUdfMappingError = createAction(constants.ITEM_UDF_MAPPING_ERROR);
export const itemUdfMappingClear = createAction(constants.ITEM_UDF_MAPPING_CLEAR);

export const createItemUdfRequest = createAction(constants.CREATE_ITEM_UDF_REQUEST);
export const createItemUdfSuccess = createAction(constants.CREATE_ITEM_UDF_SUCCESS);
export const createItemUdfError = createAction(constants.CREATE_ITEM_UDF_ERROR);
export const createItemUdfClear = createAction(constants.CREATE_ITEM_UDF_CLEAR);

export const getPoItemCodeRequest = createAction(constants.GET_PO_ITEMCODE_REQUEST);
export const getPoItemCodeSuccess = createAction(constants.GET_PO_ITEMCODE_SUCCESS);
export const getPoItemCodeError = createAction(constants.GET_PO_ITEMCODE_ERROR);
export const getPoItemCodeClear = createAction(constants.GET_PO_ITEMCODE_CLEAR);


export const updateSizeDeptRequest = createAction(constants.UPDATE_SIZE_DEPT_REQUEST);
export const updateSizeDeptSuccess = createAction(constants.UPDATE_SIZE_DEPT_SUCCESS);
export const updateSizeDeptError = createAction(constants.UPDATE_SIZE_DEPT_ERROR);
export const updateSizeDeptClear = createAction(constants.UPDATE_SIZE_DEPT_CLEAR)


export const cnameRequest = createAction(constants.CNAME_REQUEST);
export const cnameSuccess = createAction(constants.CNAME_SUCCESS);
export const cnameError = createAction(constants.CNAME_ERROR);
export const cnameClear = createAction(constants.CNAME_CLEAR);


export const articleNameRequest = createAction(constants.ARTICLE_NAME_REQUEST);
export const articleNameSuccess = createAction(constants.ARTICLE_NAME_SUCCESS);
export const articleNameError = createAction(constants.ARTICLE_NAME_ERROR);
export const articleNameClear = createAction(constants.ARTICLE_NAME_CLEAR);

// Dashboard Excel Download

export const exportDashboardExcelRequest = createAction(constants.EXPORT_DASHBOARD_EXCEL_REQUEST);
export const exportDashboardExcelSuccess = createAction(constants.EXPORT_DASHBOARD_EXCEL_SUCCESS);
export const exportDashboardExcelClear = createAction(constants.EXPORT_DASHBOARD_EXCEL_CLEAR);
export const exportDashboardExcelError = createAction(constants.EXPORT_DASHBOARD_EXCEL_ERROR)

//replenishment begins

//auto-config(createTrigger)

export const createTriggerRequest = createAction(constants.CREATE_TRIGGER_REQUEST);
export const createTriggerSuccess = createAction(constants.CREATE_TRIGGER_SUCCESS);
export const createTriggerError = createAction(constants.CREATE_TRIGGER_ERROR);
export const createTriggerClear = createAction(constants.CREATE_TRIGGER_CLEAR);

export const getJobByNameRequest = createAction(constants.GET_JOB_BY_NAME_REQUEST);
export const getJobByNameSuccess = createAction(constants.GET_JOB_BY_NAME_SUCCESS);
export const getJobByNameError = createAction(constants.GET_JOB_BY_NAME_ERROR);
export const getjobByNameClear = createAction(constants.GET_JOB_BY_NAME_CLEAR)

export const jobRunDetailRequest = createAction(constants.JOB_RUN_DETAIL_REQUEST);
export const jobRunDetailSuccess = createAction(constants.JOB_RUN_DETAIL_SUCCESS);
export const jobRunDetailError = createAction(constants.JOB_RUN_DETAIL_ERROR);
export const jobRunDetailClear = createAction(constants.JOB_RUN_DETAIL_CLEAR);

export const runOnDemandRequest = createAction(constants.RUN_ON_DEMAND_REQUEST);
export const runOnDemandClear = createAction(constants.RUN_ON_DEMAND_CLEAR);
export const runOnDemandSuccess = createAction(constants.RUN_ON_DEMAND_SUCCESS);
export const runOnDemandError = createAction(constants.RUN_ON_DEMAND_ERROR);

export const stopOnDemandRequest = createAction(constants.STOP_ON_DEMAND_REQUEST);
export const stopOnDemandClear = createAction(constants.STOP_ON_DEMAND_CLEAR);
export const stopOnDemandSuccess = createAction(constants.STOP_ON_DEMAND_SUCCESS);
export const stopOnDemandError = createAction(constants.STOP_ON_DEMAND_ERROR);

export const nscheduleRequest = createAction(constants.NSCHEDULE_REQUEST);
export const nscheduleSuccess = createAction(constants.NSCHEDULE_SUCCESS);
export const nscheduleError = createAction(constants.NSCHEDULE_ERROR);
export const nscheduleClear = createAction(constants.NSCHEDULE_CLEAR);

export const jobHistoryRequest = createAction(constants.JOB_HISTORY_REQUEST);
export const jobHistorySuccess = createAction(constants.JOB_HISTORY_SUCCESS);
export const jobHistoryError = createAction(constants.JOB_HISTORY_ERROR);
export const jobHistoryClear = createAction(constants.JOB_HISTORY_CLEAR);

export const jobHistoryFilterRequest = createAction(constants.JOB_HISTORY_FILTER_REQUEST);
export const jobHistoryFilterSuccess = createAction(constants.JOB_HISTORY_FILTER_SUCCESS);
export const jobHistoryFilterError = createAction(constants.JOB_HISTORY_FILTER_ERROR);
export const jobHistoryFilterClear = createAction(constants.JOB_HISTORY_FILTER_CLEAR)

export const createJobRequest = createAction(constants.CREATE_JOB_REQUEST);
export const createJobSuccess = createAction(constants.CREATE_JOB_SUCCESS);
export const createJobError = createAction(constants.CREATE_JOB_ERROR);
export const creatJobClear = createAction(constants.CREATE_JOB_CLEAR);


export const allStoreCodeRequest = createAction(constants.ALL_STORE_CODE_REQUEST);
export const allStoreCodeSuccess = createAction(constants.ALL_STORE_CODE_SUCCESS);
export const allStoreCodeError = createAction(constants.ALL_STORE_CODE_ERROR);
export const allStoreCodeClear = createAction(constants.ALL_STORE_CODE_CLEAR);

export const lastJobRequest = createAction(constants.LAST_JOB_REQUEST);
export const lastJobSuccess = createAction(constants.LAST_JOB_SUCCESS);
export const lastJobError = createAction(constants.LAST_JOB_ERROR);
export const lastJobClear = createAction(constants.LAST_JOB_CLEAR)

export const getAllocationRequest = createAction(constants.GET_ALLOCATION_REQUEST);
export const getAllocationSuccess = createAction(constants.GET_ALLOCATION_SUCCESS);
export const getAllocationError = createAction(constants.GET_ALLOCATION_ERROR);
export const getAllocationClear = createAction(constants.GET_ALLOCATION_CLEAR)

export const getRequirementSummaryRequest = createAction(constants.GET_REQUIREMENT_SUMMARY_REQUEST);
export const getRequirementSummarySuccess = createAction(constants.GET_REQUIREMENT_SUMMARY_SUCCESS);
export const getRequirementSummaryError = createAction(constants.GET_REQUIREMENT_SUMMARY_ERROR);
export const getRequirementSummaryClear = createAction(constants.GET_REQUIREMENT_SUMMARY_CLEAR)

export const summaryDetailRequest = createAction(constants.SUMMARY_DETAIL_REQUEST);
export const summaryDetailClear = createAction(constants.SUMMARY_DETAIL_CLEAR);
export const summaryDetailSuccess = createAction(constants.SUMMARY_DETAIL_SUCCESS);
export const summaryDetailError = createAction(constants.SUMMARY_DETAIL_ERROR);

export const summaryCsvRequest = createAction(constants.SUMMARY_CSV_REQUEST);
export const summaryCsvSuccess = createAction(constants.SUMMARY_CSV_SUCCESS);
export const summaryCsvError = createAction(constants.SUMMARY_CSV_ERROR);
export const summaryCsvClear = createAction(constants.SUMMARY_CSV_CLEAR)


export const getStoreCodeRequest = createAction(constants.GETSTORECODE_REQUEST);
export const getStoreCodeClear = createAction(constants.GETSTORECODE_CLEAR);
export const getStoreCodeSuccess = createAction(constants.GETSTORECODE_SUCCESS);
export const getStoreCodeError = createAction(constants.GETSTORECODE_ERROR);

export const getZoneRequest = createAction(constants.GETZONE_REQUEST);
export const getZoneClear = createAction(constants.GETZONE_CLEAR);
export const getZoneSuccess = createAction(constants.GETZONE_SUCCESS);
export const getZoneError = createAction(constants.GETZONE_ERROR);

export const getPartnerRequest = createAction(constants.GETPARTNER_REQUEST);
export const getPartnerSuccess = createAction(constants.GETPARTNER_SUCCESS);
export const getPartnerClear = createAction(constants.GETPARTNER_CLEAR);
export const getPartnerError = createAction(constants.GETPARTNER_ERROR);

export const getGradeRequest = createAction(constants.GETGRADE_REQUEST);
export const getGradeClear = createAction(constants.GETGRADE_CLEAR);
export const getGradeSuccess = createAction(constants.GETGRADE_SUCCESS);
export const getGradeError = createAction(constants.GETGRADE_ERROR);

export const getCSVLinkRequest = createAction(constants.GETCSVLINK_REQUEST);
export const getCSVLinkSuccess = createAction(constants.GETCSVLINK_SUCCESS);
export const getCSVLinkError = createAction(constants.GETCSVLINK_ERROR);
export const getCSVLinkClear = createAction(constants.GETCSVLINK_CLEAR);

export const getXLSLinkRequest = createAction(constants.GETXLSLINK_REQUEST);
export const getXLSLinkSuccess = createAction(constants.GETXLSLINK_SUCCESS);
export const getXLSLinkError = createAction(constants.GETXLSLINK_ERROR);
export const getXLSLinkClear = createAction(constants.GETXLSLINK_CLEAR)

export const toStatusRequest = createAction(constants.TO_STATUS_REQUEST);
export const toStatusSuccess = createAction(constants.TO_STATUS_SUCCESS);
export const toStatusError = createAction(constants.TO_STATUS_ERROR);
export const toStatusClear = createAction(constants.TO_STATUS_CLEAR)

export const applyFilterRequest = createAction(constants.APPLY_FILTER_REQUEST);
export const applyFilterClear = createAction(constants.APPLY_FILTER_CLEAR);
export const applyFilterSuccess = createAction(constants.APPLY_FILTER_SUCCESS);
export const applyFilterError = createAction(constants.APPLY_FILTER_ERROR);

export const getFiveTriggersRequest = createAction(constants.GET_FIVE_TRIGGERS_REQUEST);
export const getFiveTriggersSuccess = createAction(constants.GET_FIVE_TRIGGERS_SUCCESS);
export const getFiveTriggersClear = createAction(constants.GET_FIVE_TRIGGERS_CLEAR);
export const getFiveTriggersError = createAction(constants.GET_FIVE_TRIGGERS_ERROR);

export const deleteTriggerRequest = createAction(constants.DELETE_TRIGGER_REQUEST);
export const deleteTriggerSuccess = createAction(constants.DELETE_TRIGGER_SUCCESS);
export const deleteTriggerClear = createAction(constants.DELETE_TRIGGER_CLEAR);
export const deleteTriggerError = createAction(constants.DELETE_TRIGGER_ERROR);

// PIHISTORY
export const piHistoryRequest = createAction(constants.PIHISTORY_REQUEST);
export const piHistorySuccess = createAction(constants.PIHISTORY_SUCCESS);
export const piHistoryError = createAction(constants.PIHISTORY_ERROR);
export const piHistoryClear = createAction(constants.PIHISTORY_CLEAR);
export const piHistoryDataUpdate = createAction(constants.PIHISTORY_DATA_UPDATE);


//assortement
export const hl1Request = createAction(constants.HL1_REQUEST);
export const hl1Success = createAction(constants.HL1_SUCCESS);
export const hl1Error = createAction(constants.HL1_ERROR);
export const hl1Clear = createAction(constants.HL1_CLEAR);

export const hl2Request = createAction(constants.HL2_REQUEST);
export const hl2Success = createAction(constants.HL2_SUCCESS);
export const hl2Error = createAction(constants.HL2_ERROR);
export const hl2Clear = createAction(constants.HL2_CLEAR);

export const hl3Request = createAction(constants.HL3_REQUEST);
export const hl3Success = createAction(constants.HL3_SUCCESS);
export const hl3Error = createAction(constants.HL3_ERROR);
export const hl3Clear = createAction(constants.HL3_CLEAR);

export const hl4Request = createAction(constants.HL4_REQUEST);
export const hl4Success = createAction(constants.HL4_SUCCESS);
export const hl4Error = createAction(constants.HL4_ERROR);
export const hl4Clear = createAction(constants.HL4_CLEAR);

export const assetCodeRequest = createAction(constants.ASSET_CODE_REQUEST);
export const assetCodeClear = createAction(constants.ASSET_CODE_CLEAR);
export const assetCodeSuccess = createAction(constants.ASSET_CODE_SUCCESS);
export const assetCodeError = createAction(constants.ASSET_CODE_ERROR);

export const createAssortmentRequest = createAction(constants.CREATE_ASSORTMENT_REQUEST);
export const createAssortmentClear = createAction(constants.CREATE_ASSORTMENT_CLEAR);
export const createAssortmentSuccess = createAction(constants.CREATE_ASSORTMENT_SUCCESS);
export const createAssortmentError = createAction(constants.CREATE_ASSORTMENT_ERROR);

export const getAssortmentRequest = createAction(constants.GET_ASSORTMENT_REQUEST);
export const getAssortmentClear = createAction(constants.GET_ASSORTMENT_CLEAR);
export const getAssortmentSuccess = createAction(constants.GET_ASSORTMENT_SUCCESS);
export const getAssortmentError = createAction(constants.GET_ASSORTMENT_ERROR);

export const viewMessageRequest = createAction(constants.VIEW_MESSAGE_REQUEST);
export const viewMessageClear = createAction(constants.VIEW_MESSAGE_CLEAR);
export const viewMessageSuccess = createAction(constants.VIEW_MESSAGE_SUCCESS);
export const viewMessageError = createAction(constants.VIEW_MESSAGE_ERROR);

export const getAssortmentStatusRequest = createAction(constants.GET_ASSORTMENT_STATUS_REQUEST);
export const getAssortmentStatusClear = createAction(constants.GET_ASSORTMENT_STATUS_CLEAR);
export const getAssortmentStatusSuccess = createAction(constants.GET_ASSORTMENT_STATUS_SUCCESS);
export const getAssortmentStatusError = createAction(constants.GET_ASSORTMENT_STATUS_ERROR);

export const articlePoClear = createAction(constants.ARTICLEPO_CLEAR);
export const articlePoRequest = createAction(constants.ARTICLEPO_REQUEST);
export const articlePoSuccess = createAction(constants.ARTICLEPO_SUCCESS);
export const articlePoError = createAction(constants.ARTICLEPO_ERROR);


export const getPoDataRequest = createAction(constants.GET_PODATA_REQUEST);
export const getPoDataSuccess = createAction(constants.GET_PODATA_SUCCESS);
export const getPoDataError = createAction(constants.GET_PODATA_ERROR);
export const getPoDataClear = createAction(constants.GET_PODATA_CLEAR);

//inv plan ad hock

export const getAdHockRequest = createAction(constants.GET_AD_HOCK_REQUEST);
export const getAdHockClear = createAction(constants.GET_AD_HOCK_CLEAR);
export const getAdHockSuccess = createAction(constants.GET_AD_HOCK_SUCCESS);
export const getAdHockError = createAction(constants.GET_AD_HOCK_ERROR);

export const getSiteAdHockRequest = createAction(constants.GET_SITE_AD_HOCK_REQUEST);
export const getSiteAdHockSuccess = createAction(constants.GET_SITE_AD_HOCK_SUCCESS);
export const getSiteAdHockError = createAction(constants.GET_SITE_AD_HOCK_ERROR);
export const getSiteAdHockClear = createAction(constants.GET_SITE_AD_HOCK_CLEAR);


export const getItemAdHockRequest = createAction(constants.GET_ITEM_AD_HOCK_REQUEST);
export const getItemAdHockSuccess = createAction(constants.GET_ITEM_AD_HOCK_SUCCESS);
export const getItemAdHockError = createAction(constants.GET_ITEM_AD_HOCK_ERROR);
export const getItemAdHockClear = createAction(constants.GET_ITEM_AD_HOCK_CLEAR)

export const createAdhocRequest = createAction(constants.CREATE_ADHOCK_REQUEST);
export const createAdhocSuccess = createAction(constants.CREATE_ADHOCK_SUCCESS);
export const createAdhocError = createAction(constants.CREATE_ADHOCK_CLEAR);
export const createAdhocClear = createAction(constants.CREATE_ADHOCK_CLEAR);

export const editAdhocRequest = createAction(constants.EDIT_ADHOCK_REQUEST);
export const editAdhocSuccess = createAction(constants.EDIT_ADHOCK_SUCCESS);
export const editAdhocError = createAction(constants.EDIT_ADHOCK_ERROR);
export const editAdhocClear = createAction(constants.EDIT_ADHOCK_CLEAR);


export const cancelAdhocRequest = createAction(constants.CANCEL_ADHOCK_REQUEST);
export const cancelAdhocSuccess = createAction(constants.CANCEL_ADHOCK_SUCCESS);
export const cancelAdhocError = createAction(constants.CANCEL_ADHOCK_ERROR);
export const cancelAdhocClear = createAction(constants.CANCEL_ADHOCK_CLEAR);

export const deleteItemCodeRequest = createAction(constants.DELETE_ITEM_CODE_REQUEST);
export const deleteItemCodeSuccess = createAction(constants.DELETE_ITEM_CODE_SUCCESS);
export const deleteItemCodeError = createAction(constants.DELETE_ITEM_CODE_ERROR);
export const deleteItemCodeClear = createAction(constants.DELETE_ITEM_CODE_CLEAR);

export const getActivityRequest = createAction(constants.GET_ACTIVITY_REQUEST);
export const getActivitySuccess = createAction(constants.GET_ACTIVITY_SUCCESS);
export const getActivityError = createAction(constants.GET_ACTIVITY_ERROR);
export const getActivityClear = createAction(constants.GET_ACTIVITY_CLEAR);



//demandplanning

export const forcastRequest = createAction(constants.FORCAST_REQUEST);
export const forcastClear = createAction(constants.FORCAST_CLEAR);
export const forcastSuccess = createAction(constants.FORCAST_SUCCESS);
export const forcastError = createAction(constants.FORCAST_ERROR);

export const storeDPClear = createAction(constants.STOREDP_CLEAR);
export const storeDPRequest = createAction(constants.STOREDP_REQUEST);
export const storeDPSuccess = createAction(constants.STOREDP_SUCCESS);
export const storeDPError = createAction(constants.STOREDP_ERROR);

export const assortmentDPClear = createAction(constants.ASSORTMENTDP_CLEAR);
export const assortmentDPRequest = createAction(constants.ASSORTMENTDP_REQUEST);
export const assortmentDPSuccess = createAction(constants.ASSORTMENTDP_SUCCESS);
export const assortmentDPError = createAction(constants.ASSORTMENTDP_ERROR);

export const getForcastRequest = createAction(constants.GET_FORCAST_REQUEST);
export const getForcastSuccess = createAction(constants.GET_FORCAST_SUCCESS);
export const getForcastError = createAction(constants.GET_FORCAST_ERROR);
export const getForcastClear = createAction(constants.GET_FORCAST_CLEAR);

export const weeklyDPRequest = createAction(constants.WEEKLYDP_REQUEST);
export const weeklyDPSuccess = createAction(constants.WEEKLYDP_SUCCESS);
export const weeklyDPError = createAction(constants.WEEKLYDP_ERROR);
export const weeklyDPClear = createAction(constants.WEEKLYDP_CLEAR)

export const monthlyForRequest = createAction(constants.MONTHLYFOR_REQUEST);
export const monthlyForSuccess = createAction(constants.MONTHLYFOR_SUCCESS);
export const monthlyForError = createAction(constants.MONTHLYFOR_ERROR);
export const monthlyForClear = createAction(constants.MONTHLYFOR_CLEAR);

export const weeklyForRequest = createAction(constants.WEEKLYFOR_REQUEST);
export const weeklyForSuccess = createAction(constants.WEEKLYFOR_SUCCESS);
export const weeklyForError = createAction(constants.WEEKLYFOR_ERROR);
export const weeklyForClear = createAction(constants.WEEKLYFOR_CLEAR);

export const tableRequest = createAction(constants.TABLE_REQUEST);
export const tableSuccess = createAction(constants.TABLE_SUCCESS);
export const tableError = createAction(constants.TABLE_ERROR);
export const tableClear = createAction(constants.TABLE_CLEAR);

export const processingBSRequest = createAction(constants.PROCESSINGBS_REQUEST);
export const processingBSSuccess = createAction(constants.PROCESSINGBS_SUCCESS);
export const processingBSError = createAction(constants.PROCESSINGBS_ERROR);
export const processingBSClear = createAction(constants.PROCESSINGBS_CLEAR);

export const verifyBSRequest = createAction(constants.VERIFYBS_REQUEST);
export const verifyBSSuccess = createAction(constants.VERIFYBS_SUCCESS);
export const verifyBSError = createAction(constants.VERIFYBS_ERROR);
export const verifyBSClear = createAction(constants.VERIFYBS_CLEAR);

export const successBSRequest = createAction(constants.SUCCESSBS_REQUEST);
export const successBSSuccess = createAction(constants.SUCCESSBS_SUCCESS);
export const successBSError = createAction(constants.SUCCESSBS_ERROR);
export const successBSClear = createAction(constants.SUCCESSBS_CLEAR);

export const dpProgressRequest = createAction(constants.DPPROGRESS_REQUEST);
export const dpProgressSuccess = createAction(constants.DPPROGRESS_SUCCESS);
export const dpProgressError = createAction(constants.DPPROGRESS_ERROR);
export const dpProgressClear = createAction(constants.DPPROGRESS_CLEAR);

export const bsStatusRequest = createAction(constants.BSSTATUS_REQUEST);
export const bsStatusSuccess = createAction(constants.BSSTATUS_SUCCESS);
export const bsStatusError = createAction(constants.BSSTATUS_ERROR);
export const bsStatusClear = createAction(constants.BSSTATUS_CLEAR);

export const bsHistoryRequest = createAction(constants.BSHISTORY_REQUEST);
export const bsHistorySuccess = createAction(constants.BSHISTORY_SUCCESS);
export const bsHistoryError = createAction(constants.BSHISTORY_ERROR);
export const bsHistoryClear = createAction(constants.BSHISTORY_CLEAR);

export const mwHistoryRequest = createAction(constants.MWHISTORY_REQUEST);
export const mwHistorySuccess = createAction(constants.MWHISTORY_SUCCESS);
export const mwHistoryError = createAction(constants.MWHISTORY_ERROR);
export const mwHistoryClear = createAction(constants.MWHISTORY_CLEAR);

export const getForecastStatusRequest = createAction(constants.GET_FORECAST_STATUS_REQUEST);
export const getForecastStatusSuccess = createAction(constants.GET_FORECAST_STATUS_SUCCESS);
export const getForecastStatusError = createAction(constants.GET_FORECAST_STATUS_ERROR);
export const getForecastStatusClear = createAction(constants.GET_FORECAST_STATUS_CLEAR);

export const lastForecastRequest = createAction(constants.LAST_FORECAST_REQUEST);
export const lastForecastSuccess = createAction(constants.LAST_FORECAST_SUCCESS);
export const lastForecastError = createAction(constants.LAST_FORECAST_ERROR);
export const lastForecastClear = createAction(constants.LAST_FORECAST_CLEAR);

//change setting

export const createSettingRequest = createAction(constants.CREATE_SETTING_REQUEST);
export const createSettingSuccess = createAction(constants.CREATE_SETTING_SUCCESS);
export const createSettingError = createAction(constants.CREATE_SETTING_ERROR);
export const createSettingClear = createAction(constants.CREATE_SETTING_CLEAR);

export const getSettingRequest = createAction(constants.GET_SETTING_REQUEST);
export const getSettingSuccess = createAction(constants.GET_SETTING_SUCCESS);
export const getSettingError = createAction(constants.GET_SETTING_ERROR);
export const getSettingClear = createAction(constants.GET_SETTING_CLEAR);

export const getModuleRequest = createAction(constants.GET_MODULE_REQUEST);
export const getModuleSuccess = createAction(constants.GET_MODULE_SUCCESS);
export const getModuleError = createAction(constants.GET_MODULE_ERROR);
export const getModuleClear = createAction(constants.GET_MODULE_CLEAR);

export const getSubModuleRequest = createAction(constants.GET_SUB_MODULE_REQUEST);
export const getSubModuleSuccess = createAction(constants.GET_SUB_MODULE_SUCCESS);
export const getSubModuleError = createAction(constants.GET_SUB_MODULE_ERROR);
export const getSubModuleClear = createAction(constants.GET_SUB_MODULE_CLEAR);

export const getPropertyRequest = createAction(constants.GET_PROPERTY_REQUEST);
export const getPropertySuccess = createAction(constants.GET_PROPERTY_SUCCESS);
export const getPropertyError = createAction(constants.GET_PROPERTY_ERROR);
export const getPropertyClear = createAction(constants.GET_PROPERTY_CLEAR);

export const getEmailStatusRequest = createAction(constants.GET_EMAIL_STATUS_REQUEST);
export const getEmailStatusSuccess = createAction(constants.GET_EMAIL_STATUS_SUCCESS);
export const getEmailStatusError = createAction(constants.GET_EMAIL_STATUS_ERROR);
export const getEmailStatusClear = createAction(constants.GET_EMAIL_STATUS_CLEAR);

export const updateEmailStatusRequest = createAction(constants.UPDATE_EMAIL_STATUS_REQUEST);
export const updateEmailStatusSuccess = createAction(constants.UPDATE_EMAIL_STATUS_SUCCESS);
export const updateEmailStatusError = createAction(constants.UPDATE_EMAIL_STATUS_ERROR);
export const updateEmailStatusClear = createAction(constants.UPDATE_EMAIL_STATUS_CLEAR);

export const getToCcRequest = createAction(constants.GET_TO_CC_REQUEST);
export const getToCcSuccess = createAction(constants.GET_TO_CC_SUCCESS);
export const getToCcError = createAction(constants.GET_TO_CC_ERROR);
export const getToCcClear = createAction(constants.GET_TO_CC_CLEAR);

export const saveToCcRequest = createAction(constants.SAVE_TO_CC_REQUEST);
export const saveToCcSuccess = createAction(constants.SAVE_TO_CC_SUCCESS);
export const saveToCcError = createAction(constants.SAVE_TO_CC_ERROR);
export const saveToCcClear = createAction(constants.SAVE_TO_CC_CLEAR);

export const updateToCcRequest = createAction(constants.UPDATE_TO_CC_REQUEST);
export const updateToCcSuccess = createAction(constants.UPDATE_TO_CC_SUCCESS);
export const updateToCcError = createAction(constants.UPDATE_TO_CC_ERROR);
export const updateToCcClear = createAction(constants.UPDATE_TO_CC_CLEAR);

// ____________________FESTIVAL SETTING ____________________

// export const defaultListRequest = createAction(constants.DEFAULT_LIST_REQUEST);
// export const defaultListSuccess = createAction(constants.DEFAULT_LIST_SUCCESS);
// export const defaultListError = createAction(constants.DEFAULT_LIST_ERROR);
// export const defaultListClear = createAction(constants.DEFAULT_LIST_CLEAR);

export const allFestivalRequest = createAction(constants.ALL_FESTIVAL_REQUEST);
export const allFestivalSuccess = createAction(constants.ALL_FESTIVAL_SUCCESS);
export const allFestivalError = createAction(constants.ALL_FESTIVAL_ERROR);
export const allFestivalClear = createAction(constants.ALL_FESTIVAL_CLEAR);

// export const checkedListRequest = createAction(constants.CHECKED_LIST_REQUEST);
// export const checkedListSuccess = createAction(constants.CHECKED_LIST_SUCCESS);
// export const checkedListError = createAction(constants.CHECKED_LIST_ERROR);
// export const checkedListClear = createAction(constants.CHECKED_LIST_CLEAR);

export const createFestivalRequest = createAction(constants.CREATE_FESTIVAL_REQUEST);
export const createFestivalSuccess = createAction(constants.CREATE_FESTIVAL_SUCCESS);
export const createFestivalError = createAction(constants.CREATE_FESTIVAL_ERROR);
export const createFestivalClear = createAction(constants.CREATE_FESTIVAL_CLEAR);

//otb

export const createPlanRequest = createAction(constants.CREATE_PLAN_REQUEST);
export const createPlanSuccess = createAction(constants.CREATE_PLAN_SUCCESS);
export const createPlanError = createAction(constants.CREATE_PLAN_ERROR);
export const createPlanClear = createAction(constants.CREATE_PLAN_CLEAR);


export const getActivePlanRequest = createAction(constants.GET_ACTIVE_PLAN_REQUEST);
export const getActivePlanSuccess = createAction(constants.GET_ACTIVE_PLAN_SUCCESS);
export const getActivePlanError = createAction(constants.GET_ACTIVE_PLAN_ERROR);
export const getActivePlanClear = createAction(constants.GET_ACTIVE_PLAN_CLEAR);

export const updatePlanRequest = createAction(constants.UPDATE_PLAN_REQUEST);
export const updatePlanSuccess = createAction(constants.UPDATE_PLAN_SUCCESS);
export const updatePlanError = createAction(constants.UPDATE_PLAN_ERROR);
export const updatePlanClear = createAction(constants.UPDATE_PLAN_CLEAR);


export const updateActivePlanRequest = createAction(constants.UPDATE_ACTIVE_PLAN_REQUEST);
export const updateActivePlanSuccess = createAction(constants.UPDATE_ACTIVE_PLAN_SUCCESS);
export const updateActivePlanError = createAction(constants.UPDATE_ACTIVE_PLAN_ERROR);
export const updateActivePlanClear = createAction(constants.UPDATE_ACTIVE_PLAN_CLEAR);


export const removePlanRequest = createAction(constants.REMOVE_PLAN_REQUEST);
export const removePlanSuccess = createAction(constants.REMOVE_PLAN_SUCCESS);
export const removePlanError = createAction(constants.REMOVE_PLAN_ERROR);
export const removePlanClear = createAction(constants.REMOVE_PLAN_CLEAR)

export const getMakeCopyRequest = createAction(constants.GET_MAKE_COPY_REQUEST);
export const getMakeCopySuccess = createAction(constants.GET_MAKE_COPY_SUCCESS);
export const getMakeCopyError = createAction(constants.GET_MAKE_COPY_ERROR);
export const getMakeCopyClear = createAction(constants.GET_MAKE_COPY_CLEAR);


export const getOtbPlanRequest = createAction(constants.GET_OTB_PLAN_REQUEST);
export const getOtbPlanSuccess = createAction(constants.GET_OTB_PLAN_SUCCESS);
export const getOtbPlanError = createAction(constants.GET_OTB_PLAN_ERROR);
export const getOtbPlanClear = createAction(constants.GET_OTB_PLAN_CLEAR);


// ________________________________PLANSALEUPDATE AND TOTAL________________________

export const planSaleUpdateRequest = createAction(constants.PLAN_SALE_UPDATE_REQUEST);
export const planSaleUpdateSuccess = createAction(constants.PLAN_SALE_UPDATE_SUCCESS);
export const planSaleUpdateError = createAction(constants.PLAN_SALE_UPDATE_ERROR);
export const planSaleUpdateClear = createAction(constants.PLAN_SALE_UPDATE_CLEAR);


export const totalOtbRequest = createAction(constants.TOTAL_OTB_REQUEST);
export const totalOtbSuccess = createAction(constants.TOTAL_OTB_SUCCESS);
export const totalOtbError = createAction(constants.TOTAL_OTB_ERROR);
export const totalOtbClear = createAction(constants.TOTAL_OTB_CLEAR);

// get otb status

export const getOtbStatusRequest = createAction(constants.GET_OTB_STATUS_REQUEST);
export const getOtbStatusSuccess = createAction(constants.GET_OTB_STATUS_SUCCESS);
export const getOtbStatusError = createAction(constants.GET_OTB_STATUS_ERROR);
export const getOtbStatusClear = createAction(constants.GET_OTB_STATUS_CLEAR);

//After Login Api
export const userSessionCreateRequest = createAction(constants.USER_SESSION_CREATE_REQUEST);
export const userSessionCreateSuccess = createAction(constants.USER_SESSION_CREATE_SUCCESS);
export const userSessionCreateError = createAction(constants.USER_SESSION_CREATE_ERROR);
export const userSessionCreateClear = createAction(constants.USER_SESSION_CREATE_CLEAR);



// Store Profile

export const getFestivalImpactRequest = createAction(constants.GET_FESTIVAL_IMPACT_REQUEST);
export const getFestivalImpactSuccess = createAction(constants.GET_FESTIVAL_IMPACT_SUCCESS);
export const getFestivalImpactError = createAction(constants.GET_FESTIVAL_IMPACT_ERROR);
export const getFestivalImpactClear = createAction(constants.GET_FESTIVAL_IMPACT_CLEAR);

export const generateTotalSalesVsProfitRequest = createAction(constants.GENERATE_TOTALSALES_VS_PROFIT_REQUEST);
export const generateTotalSalesVsProfitSuccess = createAction(constants.GENERATE_TOTALSALES_VS_PROFIT_SUCCESS);
export const generateTotalSalesVsProfitError = createAction(constants.GENERATE_TOTALSALES_VS_PROFIT_ERROR);
export const generateTotalSalesVsProfitClear = createAction(constants.GENERATE_TOTALSALES_VS_PROFIT_CLEAR);

export const generateSalesPerSquareFootRequest = createAction(constants.GENERATE_SALES_PER_SQUARE_FOOT_REQUEST);
export const generateSalesPerSquareFootSuccess = createAction(constants.GENERATE_SALES_PER_SQUARE_FOOT_SUCCESS);
export const generateSalesPerSquareFootError = createAction(constants.GENERATE_SALES_PER_SQUARE_FOOT_ERROR);
export const generateSalesPerSquareFootClear = createAction(constants.GENERATE_SALES_PER_SQUARE_FOOT_CLEAR);

export const getSellThruRequest = createAction(constants.GET_SELL_THRU_REQUEST);
export const getSellThruSuccess = createAction(constants.GET_SELL_THRU_SUCCESS);
export const getSellThruError = createAction(constants.GET_SELL_THRU_ERROR);
export const getSellThruClear = createAction(constants.GET_SELL_THRU_CLEAR);



export const generateTotalSalesRequest = createAction(constants.GENERATE_TOTAL_SALES_REQUEST);
export const generateTotalSalesSuccess = createAction(constants.GENERATE_TOTAL_SALES_SUCCESS);
export const generateTotalSalesError = createAction(constants.GENERATE_TOTAL_SALES_ERROR);
export const generateTotalSalesClear = createAction(constants.GENERATE_TOTAL_SALES_CLEAR);

// ________UNIT SOLD___________-

export const generateUnitSoldRequest = createAction(constants.GENERATE_UNIT_SOLD_REQUEST);
export const generateUnitSoldSuccess = createAction(constants.GENERATE_UNIT_SOLD_SUCCESS);
export const generateUnitSoldError = createAction(constants.GENERATE_UNIT_SOLD_ERROR);
export const generateUnitSoldClear = createAction(constants.GENERATE_UNIT_SOLD_CLEAR);

// _________---get data store______________________


export const getStoreDataRequest = createAction(constants.GET_STORE_DATA_REQUEST);
export const getStoreDataSuccess = createAction(constants.GET_STORE_DATA_SUCCESS);
export const getStoreDataError = createAction(constants.GET_STORE_DATA_ERROR);
export const getStoreDataClear = createAction(constants.GET_STORE_DATA_CLEAR);

// ________UNIT SOLD___________-

export const getStoreProfileCodeRequest = createAction(constants.GET_STORE_PROFILE_CODE_REQUEST);
export const getStoreProfileCodeSuccess = createAction(constants.GET_STORE_PROFILE_CODE_SUCCESS);
export const getStoreProfileCodeError = createAction(constants.GET_STORE_PROFILE_CODE_ERROR);
export const getStoreProfileCodeClear = createAction(constants.GET_STORE_PROFILE_CODE_CLEAR);

//get top article
export const getTopArticleRequest = createAction(constants.GET_TOP_ARTICLE_REQUEST);
export const getTopArticleSuccess = createAction(constants.GET_TOP_ARTICLE_SUCCESS);
export const getTopArticleError = createAction(constants.GET_TOP_ARTICLE_ERROR);
export const getTopArticleClear = createAction(constants.GET_TOP_ARTICLE_CLEAR);

//get sales trend
export const getSalesTrendRequest = createAction(constants.GET_SALES_TREND_REQUEST);
export const getSalesTrendSuccess = createAction(constants.GET_SALES_TREND_SUCCESS);
export const getSalesTrendError = createAction(constants.GET_SALES_TREND_ERROR);
export const getSalesTrendClear = createAction(constants.GET_SALES_TREND_CLEAR);


//changeSetting
export const settingOtbCreateRequest = createAction(constants.SETTING_OTB_CREATE_REQUEST);
export const settingOtbCreateSuccess = createAction(constants.SETTING_OTB_CREATE_SUCCESS);
export const settingOtbCreateError = createAction(constants.SETTING_OTB_CREATE_ERROR);
export const settingOtbCreateClear = createAction(constants.SETTING_OTB_CREATE_CLEAR);

export const getGeneralMappingRequest = createAction(constants.GET_GENERAL_MAPPING_REQUEST);
export const getGeneralMappingSuccess = createAction(constants.GET_GENERAL_MAPPING_SUCCESS);
export const getGeneralMappingError = createAction(constants.GET_GENERAL_MAPPING_ERROR);
export const getGeneralMappingClear = createAction(constants.GET_GENERAL_MAPPING_CLEAR);

export const updateGeneralMappingRequest = createAction(constants.UPDATE_GENERAL_MAPPING_REQUEST);
export const updateGeneralMappingSuccess = createAction(constants.UPDATE_GENERAL_MAPPING_SUCCESS);
export const updateGeneralMappingError = createAction(constants.UPDATE_GENERAL_MAPPING_ERROR);
export const updateGeneralMappingClear = createAction(constants.UPDATE_GENERAL_MAPPING_CLEAR);


export const settingAssortOtbRequest = createAction(constants.SETTING_ASSORTOTB_REQUEST);
export const settingAssortOtbSuccess = createAction(constants.SETTING_ASSORTOTB_SUCCESS);
export const settingAssortOtbError = createAction(constants.SETTING_ASSORTOTB_ERROR);
export const settingAssortOtbClear = createAction(constants.SETTING_ASSORTOTB_CLEAR);

export const otbGetHlevelRequest = createAction(constants.OTB_GET_HLEVEL_REQUEST);
export const otbGetHlevelSuccess = createAction(constants.OTB_GET_HLEVEL_SUCCESS);
export const otbGetHlevelError = createAction(constants.OTB_GET_HLEVEL_ERROR);
export const otbGetHlevelClear = createAction(constants.OTB_GET_HLEVEL_CLEAR);


export const otbGetDefaultRequest = createAction(constants.OTB_GET_DEFAULT_REQUEST);
export const otbGetDefaultSuccess = createAction(constants.OTB_GET_DEFAULT_SUCCESS);
export const otbGetDefaultError = createAction(constants.OTB_GET_DEFAULT_ERROR);
export const otbGetDefaultClear = createAction(constants.OTB_GET_DEFAULT_CLEAR);

export const marginRuleRequest = createAction(constants.MARGIN_RULE_REQUEST);
export const marginRuleSuccess = createAction(constants.MARGIN_RULE_SUCCESS);
export const marginRuleError = createAction(constants.MARGIN_RULE_ERROR);
export const marginRuleClear = createAction(constants.MARGIN_RULE_CLEAR);

export const stockHandRequest = createAction(constants.STOCK_HAND_REQUEST);
export const stockHandSuccess = createAction(constants.STOCK_HAND_SUCCESS);
export const stockHandError = createAction(constants.STOCK_HAND_ERROR);
export const stockHandClear = createAction(constants.STOCK_HAND_CLEAR);


export const getAllConfigRequest = createAction(constants.GET_ALL_CONFIG_REQUEST);
export const getAllConfigSuccess = createAction(constants.GET_ALL_CONFIG_SUCCESS);
export const getAllConfigError = createAction(constants.GET_ALL_CONFIG_ERROR);
export const getAllConfigClear = createAction(constants.GET_ALL_CONFIG_CLEAR);

export const getAllOtbRequest = createAction(constants.GET_ALL_OTB_REQUEST);
export const getAllOtbSuccess = createAction(constants.GET_ALL_OTB_SUCCESS);
export const getAllOtbError = createAction(constants.GET_ALL_OTB_ERROR);
export const getAllOtbClear = createAction(constants.GET_ALL_OTB_CLEAR);

export const getItemDetailsRequest = createAction(constants.GET_ITEM_DETAILS_REQUEST);
export const getItemDetailsSuccess = createAction(constants.GET_ITEM_DETAILS_SUCCESS);
export const getItemDetailsError = createAction(constants.GET_ITEM_DETAILS_ERROR);
export const getItemDetailsClear = createAction(constants.GET_ITEM_DETAILS_CLEAR);

export const getItemDetailsValueRequest = createAction(constants.GET_ITEM_DETAILS_VALUE_REQUEST);
export const getItemDetailsValueSuccess = createAction(constants.GET_ITEM_DETAILS_VALUE_SUCCESS);
export const getItemDetailsValueError = createAction(constants.GET_ITEM_DETAILS_VALUE_ERROR);
export const getItemDetailsValueClear = createAction(constants.GET_ITEM_DETAILS_VALUE_CLEAR);

//ASSORTMENT

export const getAssortmentDropdownRequest = createAction(constants.GET_ASSORTMENT_DROPDOWN_REQUEST);
export const getAssortmentDropdownSuccess = createAction(constants.GET_ASSORTMENT_DROPDOWN_SUCCESS);
export const getAssortmentDropdownError = createAction(constants.GET_ASSORTMENT_DROPDOWN_ERROR);
export const getAssortmentDropdownClear = createAction(constants.GET_ASSORTMENT_DROPDOWN_CLEAR);

//demand planning
export const getAssortmentHistoryRequest = createAction(constants.GET_ASSORTMENT_HISTORY_REQUEST);
export const getAssortmentHistorySuccess = createAction(constants.GET_ASSORTMENT_HISTORY_SUCCESS);
export const getAssortmentHistoryError = createAction(constants.GET_ASSORTMENT_HISTORY_ERROR);
export const getAssortmentHistoryClear = createAction(constants.GET_ASSORTMENT_HISTORY_CLEAR);

// _____________po_________________


export const selectVendorRequest = createAction(constants.SELECT_VENDOR_REQUEST);
export const selectVendorSuccess = createAction(constants.SELECT_VENDOR_SUCCESS);
export const selectVendorError = createAction(constants.SELECT_VENDOR_ERROR);
export const selectVendorClear = createAction(constants.SELECT_VENDOR_CLEAR);

export const departmentSetBasedRequest = createAction(constants.DEPARTMENT_SET_BASED_REQUEST);
export const departmentSetBasedSuccess = createAction(constants.DEPARTMENT_SET_BASED_SUCCESS);
export const departmentSetBasedError = createAction(constants.DEPARTMENT_SET_BASED_ERROR);
export const departmentSetBasedClear = createAction(constants.DEPARTMENT_SET_BASED_CLEAR);

export const selectOrderNumberRequest = createAction(constants.SELECT_ORDERNUMBER_REQUEST);
export const selectOrderNumberSuccess = createAction(constants.SELECT_ORDERNUMBER_SUCCESS);
export const selectOrderNumberError = createAction(constants.SELECT_ORDERNUMBER_ERROR);
export const selectOrderNumberClear = createAction(constants.SELECT_ORDERNUMBER_CLEAR);

export const setBasedRequest = createAction(constants.SET_BASED_REQUEST);
export const setBasedSuccess = createAction(constants.SET_BASED_SUCCESS);
export const setBasedError = createAction(constants.SET_BASED_ERROR);
export const setBasedClear = createAction(constants.SET_BASED_CLEAR);

//Previous Assortment weekly/monthly

export const getPreviousAssortmentRequest = createAction(constants.GET_PREVIOUS_ASSORTMENT_REQUEST);
export const getPreviousAssortmentSuccess = createAction(constants.GET_PREVIOUS_ASSORTMENT_SUCCESS);
export const getPreviousAssortmentError = createAction(constants.GET_PREVIOUS_ASSORTMENT_ERROR);
export const getPreviousAssortmentClear = createAction(constants.GET_PREVIOUS_ASSORTMENT_CLEAR);


//replenishment
export const getHeaderConfigRequest = createAction(constants.GET_HEADER_CONFIG_REQUEST);
export const getHeaderConfigSuccess = createAction(constants.GET_HEADER_CONFIG_SUCCESS);
export const getHeaderConfigError = createAction(constants.GET_HEADER_CONFIG_ERROR);
export const getHeaderConfigClear = createAction(constants.GET_HEADER_CONFIG_CLEAR);

// excel download
export const getHeaderConfigExcelRequest = createAction(constants.GET_HEADER_CONFIG_EXCEL_REQUEST);
export const getHeaderConfigExcelSuccess = createAction(constants.GET_HEADER_CONFIG_EXCEL_SUCCESS);
export const getHeaderConfigExcelError = createAction(constants.GET_HEADER_CONFIG_EXCEL_ERROR);
export const getHeaderConfigExcelClear = createAction(constants.GET_HEADER_CONFIG_EXCEL_CLEAR);

export const getCustomHeaderRequest = createAction(constants.GET_CUSTOM_HEADER_REQUEST);
export const getCustomHeaderSuccess = createAction(constants.GET_CUSTOM_HEADER_SUCCESS);
export const getCustomHeaderError = createAction(constants.GET_CUSTOM_HEADER_ERROR);
export const getCustomHeaderClear = createAction(constants.GET_CUSTOM_HEADER_CLEAR);

export const createHeaderConfigRequest = createAction(constants.CREATE_HEADER_CONFIG_REQUEST);
export const createHeaderConfigSuccess = createAction(constants.CREATE_HEADER_CONFIG_SUCCESS);
export const createHeaderConfigError = createAction(constants.CREATE_HEADER_CONFIG_ERROR);
export const createHeaderConfigClear = createAction(constants.CREATE_HEADER_CONFIG_CLEAR);

export const createMainHeaderConfigRequest = createAction(constants.CREATE_MAIN_HEADER_CONFIG_REQUEST);
export const createMainHeaderConfigSuccess = createAction(constants.CREATE_MAIN_HEADER_CONFIG_SUCCESS);
export const createMainHeaderConfigError = createAction(constants.CREATE_MAIN_HEADER_CONFIG_ERROR);
export const createMainHeaderConfigClear = createAction(constants.CREATE_MAIN_HEADER_CONFIG_CLEAR);

export const createSetHeaderConfigRequest = createAction(constants.CREATE_SET_HEADER_CONFIG_REQUEST);
export const createSetHeaderConfigSuccess = createAction(constants.CREATE_SET_HEADER_CONFIG_SUCCESS);
export const createSetHeaderConfigError = createAction(constants.CREATE_SET_HEADER_CONFIG_ERROR);
export const createSetHeaderConfigClear = createAction(constants.CREATE_SET_HEADER_CONFIG_CLEAR);

export const createItemHeaderConfigRequest = createAction(constants.CREATE_ITEM_HEADER_CONFIG_REQUEST);
export const createItemHeaderConfigSuccess = createAction(constants.CREATE_ITEM_HEADER_CONFIG_SUCCESS);
export const createItemHeaderConfigError = createAction(constants.CREATE_ITEM_HEADER_CONFIG_ERROR);
export const createItemHeaderConfigClear = createAction(constants.CREATE_ITEM_HEADER_CONFIG_CLEAR);

export const updateHeaderLogsRequest = createAction(constants.UPDATE_HEADER_LOGS_REQUEST);
export const updateHeaderLogsSuccess = createAction(constants.UPDATE_HEADER_LOGS_SUCCESS);
export const updateHeaderLogsError = createAction(constants.UPDATE_HEADER_LOGS_ERROR);
export const updateHeaderLogsClear = createAction(constants.UPDATE_HEADER_LOGS_CLEAR);

//demandplanning
export const downloadPreviousForcastRequest = createAction(constants.DOWNLOAD_PREVIOUS_FORCAST_REQUEST);
export const downloadPreviousForcastSuccess = createAction(constants.DOWNLOAD_PREVIOUS_FORCAST_SUCCESS);
export const downloadPreviousForcastError = createAction(constants.DOWNLOAD_PREVIOUS_FORCAST_ERROR);
export const downloadPreviousForcastClear = createAction(constants.DOWNLOAD_PREVIOUS_FORCAST_CLEAR);

// _____________________________masterretail naysaa_____________________

export const paramsettingRequest = createAction(constants.PARAMSETTING_REQUEST);
export const paramsettingSuccess = createAction(constants.PARAMSETTING_SUCCESS);
export const paramsettingError = createAction(constants.PARAMSETTING_ERROR);
export const paramsettingClear = createAction(constants.PARAMSETTING_CLEAR);

export const promotionalRequest = createAction(constants.PROMOTIONAL_REQUEST)
export const promotionalSuccess = createAction(constants.PROMOTIONAL_SUCCESS)
export const promotionalError = createAction(constants.PROMOTIONAL_ERROR)
export const promotionalClear = createAction(constants.PROMOTIONAL_CLEAR)

export const getPromotionalEventRequest = createAction(constants.GET_PROMOTIONAL_EVENT_REQUEST)
export const getPromotionalEventSuccess = createAction(constants.GET_PROMOTIONAL_EVENT_SUCCESS)
export const getPromotionalEventError = createAction(constants.GET_PROMOTIONAL_EVENT_ERROR)
export const getPromotionalEventClear = createAction(constants.GET_PROMOTIONAL_EVENT_CLEAR)

export const updatePromotionalEventRequest = createAction(constants.UPDATE_PROMOTIONAL_EVENT_REQUEST)
export const updatePromotionalEventSuccess = createAction(constants.UPDATE_PROMOTIONAL_EVENT_SUCCESS)
export const updatePromotionalEventError = createAction(constants.UPDATE_PROMOTIONAL_EVENT_ERROR)
export const updatePromotionalEventClear = createAction(constants.UPDATE_PROMOTIONAL_EVENT_CLEAR)

export const disablePromotionalEventRequest = createAction(constants.DISABLE_PROMOTIONAL_EVENT_REQUEST)
export const disablePromotionalEventSuccess = createAction(constants.DISABLE_PROMOTIONAL_EVENT_SUCCESS)
export const disablePromotionalEventError = createAction(constants.DISABLE_PROMOTIONAL_EVENT_ERROR)
export const disablePromotionalEventClear = createAction(constants.DISABLE_PROMOTIONAL_EVENT_CLEAR)

export const deactivePromotionalEventRequest = createAction(constants.DEACTIVE_PROMOTIONAL_EVENT_REQUEST)
export const deactivePromotionalEventSuccess = createAction(constants.DEACTIVE_PROMOTIONAL_EVENT_SUCCESS)
export const deactivePromotionalEventError = createAction(constants.DEACTIVE_PROMOTIONAL_EVENT_ERROR)
export const deactivePromotionalEventClear = createAction(constants.DEACTIVE_PROMOTIONAL_EVENT_CLEAR)
//role master


export const updateRoleRequest = createAction(constants.UPDATE_ROLE_REQUEST);
export const updateRoleSuccess = createAction(constants.UPDATE_ROLE_SUCCESS);
export const updateRoleError = createAction(constants.UPDATE_ROLE_ERROR);
export const updateRoleClear = createAction(constants.UPDATE_ROLE_CLEAR);

export const moduleGetDataRequest = createAction(constants.MODULE_GET_DATA_REQUEST);
export const moduleGetDataSuccess = createAction(constants.MODULE_GET_DATA_SUCCESS);
export const moduleGetDataError = createAction(constants.MODULE_GET_DATA_ERROR);
export const moduleGetDataClear = createAction(constants.MODULE_GET_DATA_CLEAR);

export const moduleGetRoleRequest = createAction(constants.MODULE_GETROLE_REQUEST);
export const moduleGetRoleSuccess = createAction(constants.MODULE_GETROLE_SUCCESS);
export const moduleGetRoleError = createAction(constants.MODULE_GETROLE_ERROR);
export const moduleGetRoleClear = createAction(constants.MODULE_GETROLE_CLEAR);


export const getSubModuleDataRequest = createAction(constants.GET_SUB_MODULE_DATA_REQUEST);
export const getSubModuleDataSuccess = createAction(constants.GET_SUB_MODULE_DATA_SUCCESS);
export const getSubModuleDataError = createAction(constants.GET_SUB_MODULE_DATA_ERROR);
export const getSubModuleDataClear = createAction(constants.GET_SUB_MODULE_DATA_CLEAR);


export const getSubModuleByRoleNameRequest = createAction(constants.GET_SUB_MODULE_BY_ROLENAME_REQUEST);
export const getSubModuleByRoleNameSuccess = createAction(constants.GET_SUB_MODULE_BY_ROLENAME_SUCCESS);
export const getSubModuleByRoleNameError = createAction(constants.GET_SUB_MODULE_BY_ROLENAME_ERROR);
export const getSubModuleByRoleNameClear = createAction(constants.GET_SUB_MODULE_BY_ROLENAME_CLEAR);


// ________________________________parameter custom get all____________________________

export const parameterCustomRequest = createAction(constants.PARAMETER_CUSTOM_REQUEST);
export const parameterCustomSuccess = createAction(constants.PARAMETER_CUSTOM_SUCCESS);
export const parameterCustomError = createAction(constants.PARAMETER_CUSTOM_ERROR);
export const parameterCustomClear = createAction(constants.PARAMETER_CUSTOM_CLEAR);
//multiple line item
export const multipleLineItemRequest = createAction(constants.MULTIPLE_LINEITEM_REQUEST);
export const multipleLineItemSuccess = createAction(constants.MULTIPLE_LINEITEM_SUCCESS);
export const multipleLineItemError = createAction(constants.MULTIPLE_LINEITEM_ERROR);
export const multipleLineItemClear = createAction(constants.MULTIPLE_LINEITEM_CLEAR);


export const procurementSiteRequest = createAction(constants.PROCUREMENT_SITE_REQUEST);
export const procurementSiteSuccess = createAction(constants.PROCUREMENT_SITE_SUCCESS);
export const procurementSiteError = createAction(constants.PROCUREMENT_SITE_ERROR);
export const procurementSiteClear = createAction(constants.PROCUREMENT_SITE_CLEAR);


export const poItemBarcodeRequest = createAction(constants.PO_ITEM_BARCODE_REQUEST);
export const poItemBarcodeSuccess = createAction(constants.PO_ITEM_BARCODE_SUCCESS);
export const poItemBarcodeError = createAction(constants.PO_ITEM_BARCODE_ERROR);
export const poItemBarcodeClear = createAction(constants.PO_ITEM_BARCODE_CLEAR);

export const poRadioValidationRequest = createAction(constants.PO_RADIO_VALIDATION_REQUEST);
export const poRadioValidationSuccess = createAction(constants.PO_RADIO_VALIDATION_SUCCESS);
export const poRadioValidationError = createAction(constants.PO_RADIO_VALIDATION_ERROR);
export const poRadioValidationClear = createAction(constants.PO_RADIO_VALIDATION_CLEAR);

// __________________________-FMCG HISTORY_______________________________

export const fmcgGetHistoryRequest = createAction(constants.FMCG_GET_HISTORY_REQUEST);
export const fmcgGetHistorySuccess = createAction(constants.FMCG_GET_HISTORY_SUCCESS);
export const fmcgGetHistoryError = createAction(constants.FMCG_GET_HISTORY_ERROR);
export const fmcgGetHistoryClear = createAction(constants.FMCG_GET_HISTORY_CLEAR);

// _--------_________________________________ VENDOR PORTAL ENTERPRISE ACTIONS_____________________________
//____________________________MASTER EVENT_____________________________
export const customMasterSkechersRequest = createAction(constants.CUSTOM_MASTER_SKECHERS_REQUEST);
export const customMasterSkechersSuccess = createAction(constants.CUSTOM_MASTER_SKECHERS_SUCCESS);
export const customMasterSkechersError = createAction(constants.CUSTOM_MASTER_SKECHERS_ERROR);
export const customMasterSkechersClear = createAction(constants.CUSTOM_MASTER_SKECHERS_CLEAR);

export const customGetEventRequest = createAction(constants.CUSTOM_GET_EVENT_REQUEST);
export const customGetEventSuccess = createAction(constants.CUSTOM_GET_EVENT_SUCCESS);
export const customGetEventError = createAction(constants.CUSTOM_GET_EVENT_ERROR);
export const customGetEventClear = createAction(constants.CUSTOM_GET_EVENT_CLEAR);

export const customDeleteEventRequest = createAction(constants.CUSTOM_DELETE_EVENT_REQUEST);
export const customDeleteEventSuccess = createAction(constants.CUSTOM_DELETE_EVENT_SUCCESS);
export const customDeleteEventError = createAction(constants.CUSTOM_DELETE_EVENT_ERROR);
export const customDeleteEventClear = createAction(constants.CUSTOM_DELETE_EVENT_CLEAR);


export const customGetPartnerRequest = createAction(constants.CUSTOM_GET_PARTNER_REQUEST);
export const customGetPartnerSuccess = createAction(constants.CUSTOM_GET_PARTNER_SUCCESS);
export const customGetPartnerError = createAction(constants.CUSTOM_GET_PARTNER_ERROR);
export const customGetPartnerClear = createAction(constants.CUSTOM_GET_PARTNER_CLEAR);

export const customGetZoneRequest = createAction(constants.CUSTOM_GET_ZONE_REQUEST);
export const customGetZoneSuccess = createAction(constants.CUSTOM_GET_ZONE_SUCCESS);
export const customGetZoneError = createAction(constants.CUSTOM_GET_ZONE_ERROR);
export const customGetZoneClear = createAction(constants.CUSTOM_GET_ZONE_CLEAR);

export const customGetGradeRequest = createAction(constants.CUSTOM_GET_GRADE_REQUEST);
export const customGetGradeSuccess = createAction(constants.CUSTOM_GET_GRADE_SUCCESS);
export const customGetGradeError = createAction(constants.CUSTOM_GET_GRADE_ERROR);
export const customGetGradeClear = createAction(constants.CUSTOM_GET_GRADE_CLEAR);


export const customGetStoreCodeRequest = createAction(constants.CUSTOM_GET_STORECODE_REQUEST);
export const customGetStoreCodeSuccess = createAction(constants.CUSTOM_GET_STORECODE_SUCCESS);
export const customGetStoreCodeError = createAction(constants.CUSTOM_GET_STORECODE_ERROR);
export const customGetStoreCodeClear = createAction(constants.CUSTOM_GET_STORECODE_CLEAR);

export const customApplyFilterRequest = createAction(constants.CUSTOM_APPLYFILTER_REQUEST);
export const customApplyFilterSuccess = createAction(constants.CUSTOM_APPLYFILTER_SUCCESS);
export const customApplyFilterError = createAction(constants.CUSTOM_APPLYFILTER_ERROR);
export const customApplyFilterClear = createAction(constants.CUSTOM_APPLYFILTER_CLEAR);

export const getMultipleMarginRequest = createAction(constants.GET_MULTIPLE_MARGIN_REQUEST);
export const getMultipleMarginSuccess = createAction(constants.GET_MULTIPLE_MARGIN_SUCCESS);
export const getMultipleMarginError = createAction(constants.GET_MULTIPLE_MARGIN_ERROR);
export const getMultipleMarginClear = createAction(constants.GET_MULTIPLE_MARGIN_CLEAR);


export const getAllCityRequest = createAction(constants.GET_ALL_CITY_REQUEST);
export const getAllCitySuccess = createAction(constants.GET_ALL_CITY_SUCCESS);
export const getAllCityError = createAction(constants.GET_ALL_CITY_ERROR);
export const getAllCityClear = createAction(constants.GET_ALL_CITY_CLEAR);

export const purchaseOrderDateRangeRequest = createAction(constants.PURCHASE_ORDER_DATE_RANGE_REQUEST);
export const purchaseOrderDateRangeSuccess = createAction(constants.PURCHASE_ORDER_DATE_RANGE_SUCCESS);
export const purchaseOrderDateRangeError = createAction(constants.PURCHASE_ORDER_DATE_RANGE_ERROR);
export const purchaseOrderDateRangeClear = createAction(constants.PURCHASE_ORDER_DATE_RANGE_CLEAR)

export const discountRequest = createAction(constants.DISCOUNT_REQUEST);
export const discountSuccess = createAction(constants.DISCOUNT_SUCCESS);
export const discountError = createAction(constants.DISCOUNT_ERROR);
export const discountClear = createAction(constants.DISCOUNT_CLEAR);

export const piArticleRequest = createAction(constants.PI_ARTICLE_REQUEST);
export const piArticleSuccess = createAction(constants.PI_ARTICLE_SUCCESS);
export const piArticleError = createAction(constants.PI_ARTICLE_ERROR);
export const piArticleClear = createAction(constants.PI_ARTICLE_CLEAR);

export const getMotherCompRequest = createAction(constants.GET_MOTHER_COMP_REQUEST);
export const getMotherCompSuccess = createAction(constants.GET_MOTHER_COMP_SUCCESS);
export const getMotherCompError = createAction(constants.GET_MOTHER_COMP_ERROR);
export const getMotherCompClear = createAction(constants.GET_MOTHER_COMP_CLEAR);

export const getArticleFmcgRequest = createAction(constants.GET_ARTICLE_FMCG_REQUEST);
export const getArticleFmcgSuccess = createAction(constants.GET_ARTICLE_FMCG_SUCCESS);
export const getArticleFmcgError = createAction(constants.GET_ARTICLE_FMCG_ERROR);
export const getArticleFmcgClear = createAction(constants.GET_ARTICLE_FMCG_CLEAR);

export const getVendorFmcgRequest = createAction(constants.GET_VENDOR_FMCG_REQUEST);
export const getVendorFmcgSuccess = createAction(constants.GET_VENDOR_FMCG_SUCCESS);
export const getVendorFmcgError = createAction(constants.GET_VENDOR_FMCG_ERROR);
export const getVendorFmcgClear = createAction(constants.GET_VENDOR_FMCG_CLEAR);

export const getStoreFmcgRequest = createAction(constants.GET_STORE_FMCG_REQUEST);
export const getStoreFmcgSuccess = createAction(constants.GET_STORE_FMCG_SUCCESS);
export const getStoreFmcgError = createAction(constants.GET_STORE_FMCG_ERROR);
export const getStoreFmcgClear = createAction(constants.GET_STORE_FMCG_CLEAR);

export const applyFilterFmcgRequest = createAction(constants.APPLY_FILTER_FMCG_REQUEST);
export const applyFilterFmcgSuccess = createAction(constants.APPLY_FILTER_FMCG_SUCCESS);
export const applyFilterFmcgError = createAction(constants.APPLY_FILTER_FMCG_ERROR);
export const applyFilterFmcgClear = createAction(constants.APPLY_FILTER_FMCG_CLEAR);




export const EnterpriseApprovedPoRequest = createAction(constants.ENTERPRISE_APPROVED_PO_REQUEST);
export const EnterpriseApprovedPoSuccess = createAction(constants.ENTERPRISE_APPROVED_PO_SUCCESS);
export const EnterpriseApprovedPoError = createAction(constants.ENTERPRISE_APPROVED_PO_ERROR);
export const EnterpriseApprovedPoClear = createAction(constants.ENTERPRISE_APPROVED_PO_CLEAR);

// _________________PROXY STORE_____________________________

export const proxyStoreRequest = createAction(constants.PROXY_STORE_REQUEST);
export const proxyStoreSuccess = createAction(constants.PROXY_STORE_SUCCESS);
export const proxyStoreError = createAction(constants.PROXY_STORE_ERROR);
export const proxyStoreClear = createAction(constants.PROXY_STORE_CLEAR);

export const createDataSyncTemplateRequest = createAction(constants.CREATE_DATA_SYNC_TEMPLATE_REQUEST);
export const createDataSyncTemplateSuccess = createAction(constants.CREATE_DATA_SYNC_TEMPLATE_SUCCESS);
export const createDataSyncTemplateError = createAction(constants.CREATE_DATA_SYNC_TEMPLATE_ERROR);
export const createDataSyncTemplateClear = createAction(constants.CREATE_DATA_SYNC_TEMPLATE_CLEAR);

export const downlaodTemplateRequest = createAction(constants.DOWNLOAD_TEMPLATE_REQUEST);
export const downlaodTemplateSuccess = createAction(constants.DOWNLOAD_TEMPLATE_SUCCESS);
export const downlaodTemplateError = createAction(constants.DOWNLOAD_TEMPLATE_ERROR);
export const downlaodTemplateClear = createAction(constants.DOWNLOAD_TEMPLATE_CLEAR);

// _______________________________UPLAOD TEMPLATE________________________

export const uploadTemplateRequest = createAction(constants.UPLOAD_TEMPLATE_REQUEST);
export const uploadTemplateSuccess = createAction(constants.UPLOAD_TEMPLATE_SUCCESS);
export const uploadTemplateError = createAction(constants.UPLOAD_TEMPLATE_ERROR);
export const uploadTemplateClear = createAction(constants.UPLOAD_TEMPLATE_CLEAR);

// ____________________________GET ALL ______________________________________

export const getAllDataSyncRequest = createAction(constants.GET_ALL_DATA_SYNC_REQUEST);
export const getAllDataSyncSuccess = createAction(constants.GET_ALL_DATA_SYNC_SUCCESS);
export const getAllDataSyncError = createAction(constants.GET_ALL_DATA_SYNC_ERROR);
export const getAllDataSyncClear = createAction(constants.GET_ALL_DATA_SYNC_CLEAR);

// ____________________________ERP DATA GET ALL________________________

export const dataSyncErpRequest = createAction(constants.DATA_SYNC_ERP_REQUEST);
export const dataSyncErpSuccess = createAction(constants.DATA_SYNC_ERP_SUCCESS);
export const dataSyncErpError = createAction(constants.DATA_SYNC_ERP_ERROR);
export const dataSyncErpClear = createAction(constants.DATA_SYNC_ERP_CLEAR);




//inventory configuration && MANAGE RULE

export const getFilterSectionRequest = createAction(constants.GET_FILTER_SECTION_REQUEST);
export const getFilterSectionSuccess = createAction(constants.GET_FILTER_SECTION_SUCCESS);
export const getFilterSectionError = createAction(constants.GET_FILTER_SECTION_ERROR);
export const getFilterSectionClear = createAction(constants.GET_FILTER_SECTION_CLEAR);

export const deleteConfigRequest = createAction(constants.DELETE_CONFIG_REQUEST);
export const deleteConfigSuccess = createAction(constants.DELETE_CONFIG_SUCCESS);
export const deleteConfigError = createAction(constants.DELETE_CONFIG_ERROR);
export const deleteConfigClear = createAction(constants.DELETE_CONFIG_CLEAR);

export const statusChangeRequest = createAction(constants.STATUS_CHANGE_REQUEST);
export const statusChangeSuccess = createAction(constants.STATUS_CHANGE_SUCCESS);
export const statusChangeError = createAction(constants.STATUS_CHANGE_ERROR);
export const statusChangeClear = createAction(constants.STATUS_CHANGE_CLEAR);

export const invGetAllConfigRequest = createAction(constants.INV_GET_ALL_CONFIG_REQUEST);
export const invGetAllConfigSuccess = createAction(constants.INV_GET_ALL_CONFIG_SUCCESS);
export const invGetAllConfigError = createAction(constants.INV_GET_ALL_CONFIG_ERROR);
export const invGetAllConfigClear = createAction(constants.INV_GET_ALL_CONFIG_CLEAR);

export const getJobDataRequest = createAction(constants.GET_JOBDATA_REQUEST);
export const getJobDataSuccess = createAction(constants.GET_JOBDATA_SUCCESS);
export const getJobDataError = createAction(constants.GET_JOBDATA_ERROR);
export const getJobDataClear = createAction(constants.GET_JOBDATA_CLEAR);

export const updateJobDataRequest = createAction(constants.UPDATE_JOBDATA_REQUEST);
export const updateJobDataSuccess = createAction(constants.UPDATE_JOBDATA_SUCCESS);
export const updateJobDataError = createAction(constants.UPDATE_JOBDATA_ERROR);
export const updateJobDataClear = createAction(constants.UPDATE_JOBDATA_CLEAR);

export const configCreateRequest = createAction(constants.CONFIG_CREATE_REQUEST);
export const configCreateSuccess = createAction(constants.CONFIG_CREATE_SUCCESS);
export const configCreateError = createAction(constants.CONFIG_CREATE_ERROR);
export const configCreateClear = createAction(constants.CONFIG_CREATE_CLEAR);

export const getJobNameRequest = createAction(constants.GET_JOBNAME_REQUEST);
export const getJobNameSuccess = createAction(constants.GET_JOBNAME_SUCCESS);
export const getJobNameError = createAction(constants.GET_JOBNAME_ERROR);
export const getJobNameClear = createAction(constants.GET_JOBNAME_CLEAR);

export const glueParameterRequest = createAction(constants.GLUE_PARAMETER_REQUEST);
export const glueParameterSuccess = createAction(constants.GLUE_PARAMETER_SUCCESS);
export const glueParameterError = createAction(constants.GLUE_PARAMETER_ERROR);
export const glueParameterClear = createAction(constants.GLUE_PARAMETER_CLEAR);

// __________________________SWITCH ORGANISATION ______________________________

export const switchOrganisationRequest = createAction(constants.SWITCH_ORGANISATION_REQUEST);
export const switchOrganisationSuccess = createAction(constants.SWITCH_ORGANISATION_SUCCESS);
export const switchOrganisationError = createAction(constants.SWITCH_ORGANISATION_ERROR);
export const switchOrganisationClear = createAction(constants.SWITCH_ORGANISATION_CLEAR);

// ______________________________________GET ALL JOB__________________________

export const getAllJobRequest = createAction(constants.GET_ALL_JOB_REQUEST);
export const getAllJobSuccess = createAction(constants.GET_ALL_JOB_SUCCESS);
export const getAllJobError = createAction(constants.GET_ALL_JOB_ERROR);
export const getAllJobClear = createAction(constants.GET_ALL_JOB_CLEAR)

// _________________________-GET CONFIG FILTER_________________________________________
export const EnterpriseGetPoDetailsRequest = createAction(constants.ENTERPRISE_GET_PO_DETAILS_REQUEST);
export const EnterpriseGetPoDetailsSuccess = createAction(constants.ENTERPRISE_GET_PO_DETAILS_SUCCESS);
export const EnterpriseGetPoDetailsError = createAction(constants.ENTERPRISE_GET_PO_DETAILS_ERROR);
export const EnterpriseGetPoDetailsClear = createAction(constants.ENTERPRISE_GET_PO_DETAILS_CLEAR);

export const EnterpriseCancelPoRequest = createAction(constants.ENTERPRISE_CANCEL_PO_REQUEST);
export const EnterpriseCancelPoSuccess = createAction(constants.ENTERPRISE_CANCEL_PO_SUCCESS);
export const EnterpriseCancelPoError = createAction(constants.ENTERPRISE_CANCEL_PO_ERROR);
export const EnterpriseCancelPoClear = createAction(constants.ENTERPRISE_CANCEL_PO_CLEAR);

// ------------------------------------VENDOR CREATE SHIPMENT -------------------------------

export const VendorCreateShipmentDetailsRequest = createAction(constants.VENDOR_CREATE_SHIPMENT_DETAILS_REQUEST);
export const VendorCreateShipmentDetailsSuccess = createAction(constants.VENDOR_CREATE_SHIPMENT_DETAILS_SUCCESS);
export const VendorCreateShipmentDetailsError = createAction(constants.VENDOR_CREATE_SHIPMENT_DETAILS_ERROR);
export const VendorCreateShipmentDetailsClear = createAction(constants.VENDOR_CREATE_SHIPMENT_DETAILS_CLEAR);

// ---------------------------------------------get Shipment request----------------------------
export const getShipmentRequest = createAction(constants.GET_SHIPMENT_REQUEST);
export const getShipmentSuccess = createAction(constants.GET_SHIPMENT_SUCCESS);
export const getShipmentError = createAction(constants.GET_SHIPMENT_ERROR);
export const getShipmentClear = createAction(constants.GET_SHIPMENT_CLEAR);

// ---------------------------------------------VENDOR CREATE SR ---------------------------

export const vendorCreateSrRequest = createAction(constants.VENDOR_CREATE_SR_REQUEST);
export const vendorCreateSrSuccess = createAction(constants.VENDOR_CREATE_SR_SUCCESS);
export const vendorCreateSrError = createAction(constants.VENDOR_CREATE_SR_ERROR);
export const vendorCreateSrClear = createAction(constants.VENDOR_CREATE_SR_CLEAR);

// ____________________________SHIPMENT CANCEL AND CONFIRM ACTION___________________

export const shipmentConfirmCancelRequest = createAction(constants.SHIPMENT_CONFIRM_CANCEL_REQUEST);
export const shipmentConfirmCancelSuccess = createAction(constants.SHIPMENT_CONFIRM_CANCEL_SUCCESS);
export const shipmentConfirmCancelError = createAction(constants.SHIPMENT_CONFIRM_CANCEL_ERROR);
export const shipmentConfirmCancelClear = createAction(constants.SHIPMENT_CONFIRM_CANCEL_CLEAR);

// ________________________________-GET DETAILS SHIMPMET __________________________

export const getCompleteDetailShipmentRequest = createAction(constants.GET_COMPLETE_DETAIL_SHIPMENT_REQUEST);
export const getCompleteDetailShipmentSuccess = createAction(constants.GET_COMPLETE_DETAIL_SHIPMENT_SUCCESS);
export const getCompleteDetailShipmentError = createAction(constants.GET_COMPLETE_DETAIL_SHIPMENT_ERROR);
export const getCompleteDetailShipmentClear = createAction(constants.GET_COMPLETE_DETAIL_SHIPMENT_CLEAR);

// ___________________________VENDOR SHIPMENT ACTION'S__________________________________

export const getAllShipmentVendorRequest = createAction(constants.GET_ALL_SHIPMENT_VENDOR_REQUEST);
export const getAllShipmentVendorSuccess = createAction(constants.GET_ALL_SHIPMENT_VENDOR_SUCCESS);
export const getAllShipmentVendorError = createAction(constants.GET_ALL_SHIPMENT_VENDOR_ERROR);
export const getAllShipmentVendorClear = createAction(constants.GET_ALL_SHIPMENT_VENDOR_CLEAR);

export const updateVendorRequest = createAction(constants.UPDATE_VENDOR_REQUEST);
export const updateVendorSuccess = createAction(constants.UPDATE_VENDOR_SUCCESS);
export const updateVendorError = createAction(constants.UPDATE_VENDOR_ERROR);
export const updateVendorClear = createAction(constants.UPDATE_VENDOR_CLEAR);

export const getShipmentDetailsRequest = createAction(constants.GET_SHIPMENT_DETAILS_REQUEST);
export const getShipmentDetailsSuccess = createAction(constants.GET_SHIPMENT_DETAILS_SUCCESS);
export const getShipmentDetailsError = createAction(constants.GET_SHIPMENT_DETAILS_ERROR);
export const getShipmentDetailsClear = createAction(constants.GET_SHIPMENT_DETAILS_CLEAR);

export const getAllCommentRequest = createAction(constants.GET_ALL_COMMENT_REQUEST);
export const getAllCommentSuccess = createAction(constants.GET_ALL_COMMENT_SUCCESS);
export const getAllCommentError = createAction(constants.GET_ALL_COMMENT_ERROR);
export const getAllCommentClear = createAction(constants.GET_ALL_COMMENT_CLEAR);


//LR

export const getAllVendorShipmentShippedRequest = createAction(constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_REQUEST);
export const getAllVendorShipmentShippedSuccess = createAction(constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_SUCCESS);
export const getAllVendorShipmentShippedError = createAction(constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_ERROR);
export const getAllVendorShipmentShippedClear = createAction(constants.GET_ALL_VENDOR_SHIPMENT_SHIPPED_CLEAR);

export const getAllVendorSsdetailRequest = createAction(constants.GET_ALL_VENDOR_SSDETAIL_REQUEST);
export const getAllVendorSsdetailSuccess = createAction(constants.GET_ALL_VENDOR_SSDETAIL_SUCCESS);
export const getAllVendorSsdetailError = createAction(constants.GET_ALL_VENDOR_SSDETAIL_ERROR);
export const getAllVendorSsdetailClear = createAction(constants.GET_ALL_VENDOR_SSDETAIL_CLEAR);

export const getAllVendorTransitNDeliverRequest = createAction(constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_REQUEST);
export const getAllVendorTransitNDeliverSuccess = createAction(constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_SUCCESS);
export const getAllVendorTransitNDeliverError = createAction(constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_ERROR);
export const getAllVendorTransitNDeliverClear = createAction(constants.GET_ALL_VENDOR_TRANSIT_N_DELIVER_CLEAR);

export const getAllVendorSiDetailRequest = createAction(constants.GET_ALL_VENDOR_SIDETAIL_REQUEST);
export const getAllVendorSiDetailSuccess = createAction(constants.GET_ALL_VENDOR_SIDETAIL_SUCCESS);
export const getAllVendorSiDetailError = createAction(constants.GET_ALL_VENDOR_SIDETAIL_ERROR);
export const getAllVendorSiDetailClear = createAction(constants.GET_ALL_VENDOR_SIDETAIL_CLEAR);

export const getAllVendorSupplierRequest = createAction(constants.GET_ALL_VENDOR_SUPPLIER_REQUEST);
export const getAllVendorSupplierError = createAction(constants.GET_ALL_VENDOR_SUPPLIER_ERROR);
export const getAllVendorSupplierSuccess = createAction(constants.GET_ALL_VENDOR_SUPPLIER_SUCCESS);
export const getAllVendorSupplierClear = createAction(constants.GET_ALL_VENDOR_SUPPLIER_CLEAR);

export const getAllVendorTransporterRequest = createAction(constants.GET_ALL_VENDOR_TRANSPORTER_REQUEST);
export const getAllVendorTransporterError = createAction(constants.GET_ALL_VENDOR_TRANSPORTER_ERROR);
export const getAllVendorTransporterSuccess = createAction(constants.GET_ALL_VENDOR_TRANSPORTER_SUCCESS);
export const getAllVendorTransporterClear = createAction(constants.GET_ALL_VENDOR_TRANSPORTER_CLEAR)

export const getAgentNameRequest = createAction(constants.GET_AGENT_NAME_REQUEST);
export const getAgentNameSuccess = createAction(constants.GET_AGENT_NAME_SUCCESS);
export const getAgentNameError = createAction(constants.GET_AGENT_NAME_ERROR);
export const getAgentNameClear = createAction(constants.GET_AGENT_NAME_CLEAR);

export const lrCreateRequest = createAction(constants.LR_CREATE_REQUEST);
export const lrCreateSuccess = createAction(constants.LR_CREATE_SUCCESS);
export const lrCreateError = createAction(constants.LR_CREATE_ERROR);
export const lrCreateClear = createAction(constants.LR_CREATE_CLEAR);

export const updateDeliveryRequest = createAction(constants.UPDATE_DELIVERY_REQUEST);
export const updateDeliverySuccess = createAction(constants.UPDATE_DELIVERY_SUCCESS);
export const updateDeliveryError = createAction(constants.UPDATE_DELIVERY_ERROR);
export const updateDeliveryClear = createAction(constants.UPDATE_DELIVERY_CLEAR);

export const qcAddCommentRequest = createAction(constants.QC_ADD_COMMENT_REQUEST);
export const qcAddCommentSuccess = createAction(constants.QC_ADD_COMMENT_SUCCESS);
export const qcAddCommentError = createAction(constants.QC_ADD_COMMENT_ERROR);
export const qcAddCommentClear = createAction(constants.QC_ADD_COMMENT_CLEAR);

export const qcGetCommentRequest = createAction(constants.QC_GET_COMMENT_REQUEST);
export const qcGetCommentSuccess = createAction(constants.QC_GET_COMMENT_SUCCESS);
export const qcGetCommentError = createAction(constants.QC_GET_COMMENT_ERROR);
export const qcGetCommentClear = createAction(constants.QC_GET_COMMENT_CLEAR);

export const getConfigFilterRequest = createAction(constants.GET_CONFIG_FILTER_REQUEST);
export const getConfigFilterSuccess = createAction(constants.GET_CONFIG_FILTER_SUCCESS);
export const getConfigFilterError = createAction(constants.GET_CONFIG_FILTER_ERROR);
export const getConfigFilterClear = createAction(constants.GET_CONFIG_FILTER_CLEAR)


export const uploadCommentRequest = createAction(constants.UPLOAD_COMMENT_REQUEST);
export const uploadCommentSuccess = createAction(constants.UPLOAD_COMMENT_SUCCESS);
export const uploadCommentError = createAction(constants.UPLOAD_COMMENT_ERROR);
export const uploadCommentClear = createAction(constants.UPLOAD_COMMENT_CLEAR)

export const deleteUploadsRequest = createAction(constants.DELETE_UPLOADS_REQUEST);
export const deleteUploadsSuccess = createAction(constants.DELETE_UPLOADS_SUCCESS);
export const deleteUploadsError = createAction(constants.DELETE_UPLOADS_ERROR);
export const deleteUploadsClear = createAction(constants.DELETE_UPLOADS_CLEAR);

//replenishment

export const customFilterMenuRequest = createAction(constants.CUSTOM_FILTER_MENU_REQUEST);
export const customFilterMenuSuccess = createAction(constants.CUSTOM_FILTER_MENU_SUCCESS);
export const customFilterMenuError = createAction(constants.CUSTOM_FILTER_MENU_ERROR);
export const customFilterMenuClear = createAction(constants.CUSTOM_FILTER_MENU_CLEAR);

export const updateVendorShipmentRequest = createAction(constants.UPDATE_VENDOR_SHIPMENT_REQUEST);
export const updateVendorShipmentSuccess = createAction(constants.UPDATE_VENDOR_SHIPMENT_SUCCESS);
export const updateVendorShipmentError = createAction(constants.UPDATE_VENDOR_SHIPMENT_ERROR);
export const updateVendorShipmentClear = createAction(constants.UPDATE_VENDOR_SHIPMENT_CLEAR)

// ---------------------------------------------ASN NUMBER FORAMT API -----------------------
export const getAsnFormatRequest = createAction(constants.GET_ASN_FORMAT_REQUEST);
export const getAsnFormatSuccess = createAction(constants.GET_ASN_FORMAT_SUCCESS);
export const getAsnFormatError = createAction(constants.GET_ASN_FORMAT_ERROR);
export const getAsnFormatClear = createAction(constants.GET_ASN_FORMAT_CLEAR)

export const createAsnFormatRequest = createAction(constants.CREATE_ASN_FORMAT_REQUEST);
export const createAsnFormatSuccess = createAction(constants.CREATE_ASN_FORMAT_SUCCESS);
export const createAsnFormatError = createAction(constants.CREATE_ASN_FORMAT_ERROR);
export const createAsnFormatClear = createAction(constants.CREATE_ASN_FORMAT_CLEAR)

export const lrDetailsAutoRequest = createAction(constants.LR_DETAILS_AUTO_REQUEST);
export const lrDetailsAutoSuccess = createAction(constants.LR_DETAILS_AUTO_SUCCESS);
export const lrDetailsAutoError = createAction(constants.LR_DETAILS_AUTO_ERROR);
export const lrDetailsAutoClear = createAction(constants.LR_DETAILS_AUTO_CLEAR);


export const getLineItemRequest = createAction(constants.GET_LINE_ITEM_REQUEST);
export const getLineItemSuccess = createAction(constants.GET_LINE_ITEM_SUCCESS);
export const getLineItemError = createAction(constants.GET_LINE_ITEM_ERROR);
export const getLineItemClear = createAction(constants.GET_LINE_ITEM_CLEAR);


export const getPiLineItemRequest = createAction(constants.GET_PI_LINE_ITEM_REQUEST);
export const getPiLineItemSuccess = createAction(constants.GET_PI_LINE_ITEM_SUCCESS);
export const getPiLineItemError = createAction(constants.GET_PI_LINE_ITEM_ERROR);
export const getPiLineItemClear = createAction(constants.GET_PI_LINE_ITEM_CLEAR);


export const updateInvoiceRequest = createAction(constants.UPDATE_INVOICE_REQUEST);
export const updateInvoiceSuccess = createAction(constants.UPDATE_INVOICE_SUCCESS);
export const updateInvoiceError = createAction(constants.UPDATE_INVOICE_ERROR);
export const updateInvoiceClear = createAction(constants.UPDATE_INVOICE_CLEAR)

// ----------------------------------Po Closer Actions-----------------------------------

export const poCloserRequest = createAction(constants.PO_CLOSER_REQUEST);
export const poCloserSuccess = createAction(constants.PO_CLOSER_SUCCESS);
export const poCloserError = createAction(constants.PO_CLOSER_ERROR);
export const poCloserClear = createAction(constants.PO_CLOSER_CLEAR)

export const poArticleRequest = createAction(constants.PO_ARTICLE_REQUEST);
export const poArticleSuccess = createAction(constants.PO_ARTICLE_SUCCESS);
export const poArticleError = createAction(constants.PO_ARTICLE_ERROR);
export const poArticleClear = createAction(constants.PO_ARTICLE_CLEAR);

export const multipleOtbRequest = createAction(constants.MULTIPLE_OTB_REQUEST);
export const multipleOtbSuccess = createAction(constants.MULTIPLE_OTB_SUCCESS);
export const multipleOtbError = createAction(constants.MULTIPLE_OTB_ERROR);
export const multipleOtbClear = createAction(constants.MULTIPLE_OTB_CLEAR);

export const gen_PurchaseOrderRequest = createAction(constants.GEN_PURCHASE_ORDER_REQUEST);
export const gen_PurchaseOrderSuccess = createAction(constants.GEN_PURCHASE_ORDER_SUCCESS);
export const gen_PurchaseOrderError = createAction(constants.GEN_PURCHASE_ORDER_ERROR);
export const gen_PurchaseOrderClear = createAction(constants.GEN_PURCHASE_ORDER_CLEAR);

export const gen_PurchaseIndentRequest = createAction(constants.GEN_PURCHASE_INDENT_REQUEST);
export const gen_PurchaseIndentSuccess = createAction(constants.GEN_PURCHASE_INDENT_SUCCESS);
export const gen_PurchaseIndentError = createAction(constants.GEN_PURCHASE_INDENT_ERROR);
export const gen_PurchaseIndentClear = createAction(constants.GEN_PURCHASE_INDENT_CLEAR);

export const gen_SetBasedRequest = createAction(constants.GEN_SET_BASED_REQUEST);
export const gen_SetBasedSuccess = createAction(constants.GEN_SET_BASED_SUCCESS);
export const gen_SetBasedError = createAction(constants.GEN_SET_BASED_ERROR);
export const gen_SetBasedClear = createAction(constants.GEN_SET_BASED_CLEAR);

export const gen_IndentBasedRequest = createAction(constants.GEN_INDENT_BASED_REQUEST);
export const gen_IndentBasedSuccess = createAction(constants.GEN_INDENT_BASED_SUCCESS);
export const gen_IndentBasedError = createAction(constants.GEN_INDENT_BASED_ERROR);
export const gen_IndentBasedClear = createAction(constants.GEN_INDENT_BASED_CLEAR);

export const po_approve_reject_Request = createAction(constants.PO_APPROVE_REJECT_REQUEST);
export const po_approve_reject_Success = createAction(constants.PO_APPROVE_REJECT_SUCCESS);
export const po_approve_reject_Error = createAction(constants.PO_APPROVE_REJECT_ERROR);
export const po_approve_reject_Clear = createAction(constants.PO_APPROVE_REJECT_CLEAR)

export const po_approve_retry_Request = createAction(constants.PO_APPROVE_RETRY_REQUEST);
export const po_approve_retry_Success = createAction(constants.PO_APPROVE_RETRY_SUCCESS);
export const po_approve_retry_Error = createAction(constants.PO_APPROVE_RETRY_ERROR);
export const po_approve_retry_Clear = createAction(constants.PO_APPROVE_RETRY_CLEAR)
// ----------------------------------Get all Advice-----------------------------------

export const findShipmentAdviceRequest = createAction(constants.FIND_SHIPMENT_ADVICE_REQUEST);
export const findShipmentAdviceSuccess = createAction(constants.FIND_SHIPMENT_ADVICE_SUCCESS);
export const findShipmentAdviceError = createAction(constants.FIND_SHIPMENT_ADVICE_ERROR);
export const findShipmentAdviceClear = createAction(constants.FIND_SHIPMENT_ADVICE_CLEAR)


export const shipmentTrackingRequest = createAction(constants.SHIPMENT_TRACKING_REQUEST);
export const shipmentTrackingSuccess = createAction(constants.SHIPMENT_TRACKING_SUCCESS);
export const shipmentTrackingError = createAction(constants.SHIPMENT_TRACKING_ERROR);
export const shipmentTrackingClear = createAction(constants.SHIPMENT_TRACKING_CLEAR);


// ------------------------------------------ Add Multiple Comment QC ------------------------------

export const addMultipleCommentQcRequest = createAction(constants.ADD_MULTIPLE_COMMENT_QC_REQUEST);
export const addMultipleCommentQcSuccess = createAction(constants.ADD_MULTIPLE_COMMENT_QC_SUCCESS);
export const addMultipleCommentQcError = createAction(constants.ADD_MULTIPLE_COMMENT_QC_ERROR);
export const addMultipleCommentQcClear = createAction(constants.ADD_MULTIPLE_COMMENT_QC_CLEAR);
//-------------------------------------------------------------------------------------------------

// ------------------------------------------ COMMENT NOTIFICATION  ------------------------------

export const commentNotificationRequest = createAction(constants.COMMENT_NOTIFICATION_REQUEST);
export const commentNotificationSuccess = createAction(constants.COMMENT_NOTIFICATION_SUCCESS);
export const commentNotificationError = createAction(constants.COMMENT_NOTIFICATION_ERROR);
export const commentNotificationClear = createAction(constants.COMMENT_NOTIFICATION_CLEAR);
//-------------------------------------------------------------------------------------------------

// ------------------------------------------ DASHBOARD UPPER  ------------------------------

export const dashboardUpperRequest = createAction(constants.DASHBOARD_UPPER_REQUEST);
export const dashboardUpperSuccess = createAction(constants.DASHBOARD_UPPER_SUCCESS);
export const dashboardUpperError = createAction(constants.DASHBOARD_UPPER_ERROR);
export const dashboardUpperClear = createAction(constants.DASHBOARD_UPPER_CLEAR);
//--------------------------------------------------------------`----------------------------`-------
// ------------------------------------------ DASHBOARD BOTTOM  ------------------------------

export const dashboardBottomRequest = createAction(constants.DASHBOARD_BOTTOM_REQUEST);
export const dashboardBottomSuccess = createAction(constants.DASHBOARD_BOTTOM_SUCCESS);
export const dashboardBottomError = createAction(constants.DASHBOARD_BOTTOM_ERROR);
export const dashboardBottomClear = createAction(constants.DASHBOARD_BOTTOM_CLEAR);
//-------------------------------------------------------------------------------------------------

// ------------------------------------------ DASHBOARD UNREAD COMMENTS  ------------------------------

export const dashboardUnreadCommentRequest = createAction(constants.DASHBOARD_UNREAD_COMMENT_REQUEST);
export const dashboardUnreadCommentSuccess = createAction(constants.DASHBOARD_UNREAD_COMMENT_SUCCESS);
export const dashboardUnreadCommentError = createAction(constants.DASHBOARD_UNREAD_COMMENT_ERROR);
export const dashboardUnreadCommentClear = createAction(constants.DASHBOARD_UNREAD_COMMENT_CLEAR);

// ------------------------------------------ VENDOR DASHBOARD FILTER  ------------------------------

export const vendorDashboardFilterRequest = createAction(constants.VENDOR_DASHBOARD_FILTER_REQUEST);
export const vendorDashboardFilterSuccess = createAction(constants.VENDOR_DASHBOARD_FILTER_SUCCESS);
export const vendorDashboardFilterError = createAction(constants.VENDOR_DASHBOARD_FILTER_ERROR);
export const vendorDashboardFilterClear = createAction(constants.VENDOR_DASHBOARD_FILTER_CLEAR);
//-------------------------------------------------------------------------------------------------

// ------------------------------------------ Get all plans  ------------------------------

export const getAllPlansRequest = createAction(constants.GET_ALL_PLANS_REQUEST);
export const getAllPlansSuccess = createAction(constants.GET_ALL_PLANS_SUCCESS);
export const getAllPlansError = createAction(constants.GET_ALL_PLANS_ERROR);
export const getAllPlansClear = createAction(constants.GET_ALL_PLANS_CLEAR);
//-------------------------------------------------------------------------------------------------

// ------------------------------------------ Get payment summary   ------------------------------

export const getPaymentSummaryRequest = createAction(constants.GET_PAYMENT_SUMMARY_REQUEST);
export const getPaymentSummarySuccess = createAction(constants.GET_PAYMENT_SUMMARY_SUCCESS);
export const getPaymentSummaryError = createAction(constants.GET_PAYMENT_SUMMARY_ERROR);
export const getPaymentSummaryClear = createAction(constants.GET_PAYMENT_SUMMARY_CLEAR);
//-------------------------------------------------------------------------------------------------
// ------------------------------- Get order id   ------------------------------

export const getOrderIdRequest = createAction(constants.GET_ORDER_ID_REQUEST);
export const getOrderIdSuccess = createAction(constants.GET_ORDER_ID_SUCCESS);
export const getOrderIdError = createAction(constants.GET_ORDER_ID_ERROR);
export const getOrderIdClear = createAction(constants.GET_ORDER_ID_CLEAR);
//-------------------------------------------------------------------------------------------------
// ------------------------------- Get order id   ------------------------------

export const getCapturePaymentRequest = createAction(constants.GET_CAPTURE_PAYMENT_REQUEST);
export const getCapturePaymentSuccess = createAction(constants.GET_CAPTURE_PAYMENT_SUCCESS);
export const getCapturePaymentError = createAction(constants.GET_CAPTURE_PAYMENT_ERROR);
export const getCapturePaymentClear = createAction(constants.GET_CAPTURE_PAYMENT_CLEAR);
//-------------------------------------------------------------------------------------------------

// ------------------------------- Multiple document download ------------------------------

export const multipleDocumentDownloadRequest = createAction(constants.MULTIPLE_DOCUMENT_DOWNLOAD_REQUEST);
export const multipleDocumentDownloadSuccess = createAction(constants.MULTIPLE_DOCUMENT_DOWNLOAD_SUCCESS);
export const multipleDocumentDownloadError = createAction(constants.MULTIPLE_DOCUMENT_DOWNLOAD_ERROR);
export const multipleDocumentDownloadClear = createAction(constants.MULTIPLE_DOCUMENT_DOWNLOAD_CLEAR);
//-------------------------------------------------------------------------------------------------
//ars analytics

export const getReviewReasonRequest = createAction(constants.GET_REVIEW_REASON_REQUEST);
export const getReviewReasonSuccess = createAction(constants.GET_REVIEW_REASON_SUCCESS);
export const getReviewReasonFailure = createAction(constants.GET_REVIEW_REASON_FAILURE);
export const getReviewReasonClear = createAction(constants.GET_REVIEW_REASON_CLEAR);

export const getDataWithoutFilterRequest = createAction(constants.GET_DATA_WITHOUT_FILTER_REQUEST);
export const getDataWithoutFilterSuccess = createAction(constants.GET_DATA_WITHOUT_FILTER_SUCCESS);
export const getDataWithoutFilterFailure = createAction(constants.GET_DATA_WITHOUT_FILTER_FAILURE);
export const getDataWithoutFilterClear = createAction(constants.GET_DATA_WITHOUT_FILTER_CLEAR);

// export const getDataWithFilterRequest = createAction(constants.GET_DATA_WITH_FILTER_REQUEST); 
// export const getDataWithFilterSuccess = createAction(constants.GET_DATA_WITH_FILTER_SUCCESS); 
// export const getDataWithFilterFailure = createAction(constants.GET_DATA_WITH_FILTER_FAILURE); 
// export const getDataWithFilterClear = createAction(constants.GET_DATA_WITH_FILTER_CLEAR);


export const getfiltersArsDataRequest = createAction(constants.GET_FILLTERS_ASR_DATA_REQUEST)
export const getfiltersArsDataSuccess = createAction(constants.GET_FILLTERS_ASR_DATA_SUCCESS)
export const getfiltersArsDataError = createAction(constants.GET_FILLTERS_ASR_DATA_ERROR)
export const getfiltersArsDataClear = createAction(constants.GET_FILLTERS_ASR_DATA_CLEAR)

export const getAssortmentDetailsRequest = createAction(constants.GET_ASSORTMENT_DETAILS_REQUEST);
export const getAssortmentDetailsSuccess = createAction(constants.GET_ASSORTMENT_DETAILS_SUCCESS);
export const getAssortmentDetailsError = createAction(constants.GET_ASSORTMENT_DETAILS_ERROR);
export const getAssortmentDetailsClear = createAction(constants.GET_ASSORTMENT_DETAILS_CLEAR);

export const getAssortmentItemsRequest = createAction(constants.GET_ASSORTMENT_ITEMS_REQUEST);
export const getAssortmentItemsSuccess = createAction(constants.GET_ASSORTMENT_ITEMS_SUCCESS);
export const getAssortmentItemsError = createAction(constants.GET_ASSORTMENT_ITEMS_ERROR);
export const getAssortmentItemsClear = createAction(constants.GET_ASSORTMENT_ITEMS_CLEAR);

export const getAssortmentSalesHistoryRequest = createAction(constants.GET_ASSORTMENT_SALES_HISTORY_REQUEST);
export const getAssortmentSalesHistorySuccess = createAction(constants.GET_ASSORTMENT_SALES_HISTORY_SUCCESS);
export const getAssortmentSalesHistoryError = createAction(constants.GET_ASSORTMENT_SALES_HISTORY_ERROR);
export const getAssortmentSalesHistoryClear = createAction(constants.GET_ASSORTMENT_SALES_HISTORY_CLEAR);

export const getAssortmentTimePhasedRequest = createAction(constants.GET_ASSORTMENT_TIME_PHASED_REQUEST);
export const getAssortmentTimePhasedSuccess = createAction(constants.GET_ASSORTMENT_TIME_PHASED_SUCCESS);
export const getAssortmentTimePhasedError = createAction(constants.GET_ASSORTMENT_TIME_PHASED_ERROR);
export const getAssortmentTimePhasedClear = createAction(constants.GET_ASSORTMENT_TIME_PHASED_CLEAR);

export const getAllocationDataRequest = createAction(constants.GET_ALLOCATION_DATA_REQUEST);
export const getAllocationDataSuccess = createAction(constants.GET_ALLOCATION_DATA_SUCCESS);
export const getAllocationDataError = createAction(constants.GET_ALLOCATION_DATA_ERROR);
export const getAllocationDataClear = createAction(constants.GET_ALLOCATION_DATA_CLEAR);


//---------------------------------------  pi edit ---------------------------------------------

export const piEditRequest = createAction(constants.PI_EDIT_REQUEST);
export const piEditSuccess = createAction(constants.PI_EDIT_SUCCESS);
export const piEditError = createAction(constants.PI_EDIT_ERROR);
export const piEditClear = createAction(constants.PI_EDIT_CLEAR);
//---------------------------------------  save edited pi ---------------------------------------------

export const editedPiSaveRequest = createAction(constants.EDITED_PI_SAVE_REQUEST);
export const editedPiSaveSuccess = createAction(constants.EDITED_PI_SAVE_SUCCESS);
export const editedPiSaveError = createAction(constants.EDITED_PI_SAVE_ERROR);
export const editedPiSaveClear = createAction(constants.EDITED_PI_SAVE_CLEAR);


// ------------------------------- Find All Vendor new ------------------------------

export const getAllManageVendorRequest = createAction(constants.GET_ALL_MANAGE_VENDOR_REQUEST);
export const getAllManageVendorSuccess = createAction(constants.GET_ALL_MANAGE_VENDOR_SUCCESS);
export const getAllManageVendorError = createAction(constants.GET_ALL_MANAGE_VENDOR_ERROR);
export const getAllManageVendorClear = createAction(constants.GET_ALL_MANAGE_VENDOR_CLEAR);
//-------------------------------------------------------------------------------------------------


// ------------------------------- Find All Vendor new ------------------------------

export const createVendorRequest = createAction(constants.CREATE_VENDOR_REQUEST);
export const createVendorSuccess = createAction(constants.CREATE_VENDOR_SUCCESS);
export const createVendorError = createAction(constants.CREATE_VENDOR_ERROR);
export const createVendorClear = createAction(constants.CREATE_VENDOR_CLEAR);
//-------------------------------------------------------------------------------------------------
// / ------------------------------- Update manage vendor  ------------------------------

export const updateManageVendorRequest = createAction(constants.UPDATE_MANAGE_VENDOR_REQUEST);
export const updateManageVendorSuccess = createAction(constants.UPDATE_MANAGE_VENDOR_SUCCESS);
export const updateManageVendorError = createAction(constants.UPDATE_MANAGE_VENDOR_ERROR);
export const updateManageVendorClear = createAction(constants.UPDATE_MANAGE_VENDOR_CLEAR);
//-------------------------------------------------------------------------------------------------


// / ------------------------------- Get profile detaails  ------------------------------

export const getProfileDetailsRequest = createAction(constants.GET_PROFILE_DETAILS_REQUEST);
export const getProfileDetailsSuccess = createAction(constants.GET_PROFILE_DETAILS_SUCCESS);
export const getProfileDetailsError = createAction(constants.GET_PROFILE_DETAILS_ERROR);
export const getProfileDetailsClear = createAction(constants.GET_PROFILE_DETAILS_CLEAR);
//-------------------------------------------------------------------------------------------------

// / ------------------------------- update profile detaails  ------------------------------

export const updateUserDetailsRequest = createAction(constants.UPDATE_USER_DETAILS_REQUEST);
export const updateUserDetailsSuccess = createAction(constants.UPDATE_USER_DETAILS_SUCCESS);
export const updateUserDetailsError = createAction(constants.UPDATE_USER_DETAILS_ERROR);
export const updateUserDetailsClear = createAction(constants.UPDATE_USER_DETAILS_CLEAR);
//-------------------------------------------------------------------------------------------------
// / ------------------------------- Get User Emails  ------------------------------

export const getUserEmailsRequest = createAction(constants.GET_USER_EMAILS_REQUEST);
export const getUserEmailsSuccess = createAction(constants.GET_USER_EMAILS_SUCCESS);
export const getUserEmailsError = createAction(constants.GET_USER_EMAILS_ERROR);
export const getUserEmailsClear = createAction(constants.GET_USER_EMAILS_CLEAR);
//-------------------------------------------------------------------------------------------------

// / ------------------------------- Get User Emails  ------------------------------

export const allRetailerRequest = createAction(constants.ALL_RETAILER_REQUEST);
export const allRetailerSuccess = createAction(constants.ALL_RETAILER_SUCCESS);
export const allRetailerError = createAction(constants.ALL_RETAILER_ERROR);
export const allRetailerClear = createAction(constants.ALL_RETAILER_CLEAR);
//-------------------------------------------------------------------------------------------------


// updateCreatedVendor

export const updateCreatedVendorRequest = createAction(constants.UPDATE_CREATED_VENDOR_REQUEST);

// save po draft

export const poSaveDraftRequest = createAction(constants.PO_SAVE_DRAFT_REQUEST);
export const poSaveDraftSuccess = createAction(constants.PO_SAVE_DRAFT_SUCCESS);
export const poSaveDraftError = createAction(constants.PO_SAVE_DRAFT_ERROR);
export const poSaveDraftClear = createAction(constants.PO_SAVE_DRAFT_CLEAR);

// save pi draft

export const piSaveDraftRequest = createAction(constants.PI_SAVE_DRAFT_REQUEST);
export const piSaveDraftSuccess = createAction(constants.PI_SAVE_DRAFT_SUCCESS);
export const piSaveDraftError = createAction(constants.PI_SAVE_DRAFT_ERROR);
export const piSaveDraftClear = createAction(constants.PI_SAVE_DRAFT_CLEAR);

// get po/pi draft data

export const getDraftRequest = createAction(constants.GET_DRAFT_REQUEST);
export const getDraftSuccess = createAction(constants.GET_DRAFT_SUCCESS);
export const getDraftError = createAction(constants.GET_DRAFT_ERROR);
export const getDraftClear = createAction(constants.GET_DRAFT_CLEAR);
//---------------------------------------  p0 edit ---------------------------------------------

export const poEditRequest = createAction(constants.PO_EDIT_REQUEST);
export const poEditSuccess = createAction(constants.PO_EDIT_SUCCESS);
export const poEditError = createAction(constants.PO_EDIT_ERROR);
export const poEditClear = createAction(constants.PO_EDIT_CLEAR);
//---------------------------------------  save edited po ---------------------------------------------

export const editedPoSaveRequest = createAction(constants.EDITED_PO_SAVE_REQUEST);
export const editedPoSaveSuccess = createAction(constants.EDITED_PO_SAVE_SUCCESS);
export const editedPoSaveError = createAction(constants.EDITED_PO_SAVE_ERROR);
export const editedPoSaveClear = createAction(constants.EDITED_PO_SAVE_CLEAR);

//---------------------------------------  Email Transcriot ---------------------------------------------

export const emailTranscriptRequest = createAction(constants.EMAIL_TRANSCRIPT_REQUEST);
export const emailTranscriptSuccess = createAction(constants.EMAIL_TRANSCRIPT_SUCCESS);
export const emailTranscriptError = createAction(constants.EMAIL_TRANSCRIPT_ERROR);
export const emailTranscriptClear = createAction(constants.EMAIL_TRANSCRIPT_CLEAR);
//---------------------------------------  discard po/pi data ---------------------------------------------

export const discardPoPiDataRequest = createAction(constants.DISCARD_PO_PI_DATA_REQUEST);
export const discardPoPiDataSuccess = createAction(constants.DISCARD_PO_PI_DATA_SUCCESS);
export const discardPoPiDataError = createAction(constants.DISCARD_PO_PI_DATA_ERROR);
export const discardPoPiDataClear = createAction(constants.DISCARD_PO_PI_DATA_CLEAR);


export const updateExtensionDateRequest = createAction(constants.UPDATE_EXTENSION_DATE_REQUEST);
export const updateExtensionDateSuccess = createAction(constants.UPDATE_EXTENSION_DATE_SUCCESS);
export const updateExtensionDateError = createAction(constants.UPDATE_EXTENSION_DATE_ERROR);
export const updateExtensionDateClear = createAction(constants.UPDATE_EXTENSION_DATE_CLEAR);

export const vendorUserRequest = createAction(constants.VENDOR_USER_REQUEST);
export const vendorUserSuccess = createAction(constants.VENDOR_USER_SUCCESS);
export const vendorUserError = createAction(constants.VENDOR_USER_ERROR);
export const vendorUserClear = createAction(constants.VENDOR_USER_CLEAR);


export const viewImagesRequest = createAction(constants.VIEW_IMAGES_REQUEST);
export const viewImagesSuccess = createAction(constants.VIEW_IMAGES_SUCCESS);
export const viewImagesError = createAction(constants.VIEW_IMAGES_ERROR);
export const viewImagesClear = createAction(constants.VIEW_IMAGES_CLEAR);
//---------------------------------------  get GRC Item ---------------------------------------------
export const getGrcItemRequest = createAction(constants.GET_GRC_ITEM_REQUEST);
export const getGrcItemSuccess = createAction(constants.GET_GRC_ITEM_SUCCESS);
export const getGrcItemError = createAction(constants.GET_GRC_ITEM_ERROR);
export const getGrcItemClear = createAction(constants.GET_GRC_ITEM_CLEAR);

//---------------------------------------  create GRC  ---------------------------------------------
export const createGrcRequest = createAction(constants.CREATE_GRC_REQUEST);
export const createGrcSuccess = createAction(constants.CREATE_GRC_SUCCESS);
export const createGrcError = createAction(constants.CREATE_GRC_ERROR);
export const createGrcClear = createAction(constants.CREATE_GRC_CLEAR);

//---------------------------------------  get all GRC  ---------------------------------------------
export const getAllGrcRequest = createAction(constants.GET_ALL_GRC_REQUEST);
export const getAllGrcSuccess = createAction(constants.GET_ALL_GRC_SUCCESS);
export const getAllGrcError = createAction(constants.GET_ALL_GRC_ERROR);
export const getAllGrcClear = createAction(constants.GET_ALL_GRC_CLEAR);

//---------------------------------------  get GRC Details  ---------------------------------------------
export const getGrcDetailsRequest = createAction(constants.GET_GRC_DETAILS_REQUEST);
export const getGrcDetailsSuccess = createAction(constants.GET_GRC_DETAILS_SUCCESS);
export const getGrcDetailsError = createAction(constants.GET_GRC_DETAILS_ERROR);
export const getGrcDetailsClear = createAction(constants.GET_GRC_DETAILS_CLEAR);

export const getAllGateEntryRequest = createAction(constants.GET_ALL_GATE_ENTRY_REQUEST);
export const getAllGateEntrySuccess = createAction(constants.GET_ALL_GATE_ENTRY_SUCCESS);
export const getAllGateEntryError = createAction(constants.GET_ALL_GATE_ENTRY_ERROR);
export const getAllGateEntryClear = createAction(constants.GET_ALL_GATE_ENTRY_CLEAR);

// ---------------------------------------procurement Dashboard Bottom APi --------------------
export const piDashBottomRequest = createAction(constants.PI_DASH_BOTTOM_REQUEST);
export const piDashBottomSuccess = createAction(constants.PI_DASH_BOTTOM_SUCCESS);
export const piDashBottomError = createAction(constants.PI_DASH_BOTTOM_ERROR);
export const piDashBottomClear = createAction(constants.PI_DASH_BOTTOM_CLEAR);

// ---------------------------------------procurement Dashboard hierarchy --------------------
export const piDashHierarchyRequest = createAction(constants.PI_DASH_HIERARCHY_REQUEST);
export const piDashHierarchySuccess = createAction(constants.PI_DASH_HIERARCHY_SUCCESS);
export const piDashHierarchyError = createAction(constants.PI_DASH_HIERARCHY_ERROR);
export const piDashHierarchyClear = createAction(constants.PI_DASH_HIERARCHY_CLEAR);

// ---------------------------------------Email Dashboard--------------------

export const emailDashboardSendRequest = createAction(constants.EMAIL_DASHBOARD_SEND_REQUEST);
export const emailDashboardSendSuccess = createAction(constants.EMAIL_DASHBOARD_SEND_SUCCESS);
export const emailDashboardSendError = createAction(constants.EMAIL_DASHBOARD_SEND_ERROR);
export const emailDashboardSendClear = createAction(constants.EMAIL_DASHBOARD_SEND_CLEAR);


// ---------------------------------------pi po Dashboard get site --------------------
export const piPoDashSiteRequest = createAction(constants.PI_PO_DASH_SITE_REQUEST);
export const piPoDashSiteSuccess = createAction(constants.PI_PO_DASH_SITE_SUCCESS);
export const piPoDashSiteError = createAction(constants.PI_PO_DASH_SITE_ERROR);
export const piPoDashSiteClear = createAction(constants.PI_PO_DASH_SITE_CLEAR);

// ---------------------------------------assortment planning filters --------------------
export const getAssortmentPlanningFiltersRequest = createAction(constants.GET_ASSORTMENT_PLANNING_FILTERS_REQUEST);
export const getAssortmentPlanningFiltersSuccess = createAction(constants.GET_ASSORTMENT_PLANNING_FILTERS_SUCCESS);
export const getAssortmentPlanningFiltersError = createAction(constants.GET_ASSORTMENT_PLANNING_FILTERS_ERROR);
export const getAssortmentPlanningFiltersClear = createAction(constants.GET_ASSORTMENT_PLANNING_FILTERS_CLEAR);

// ---------------------------------------assortment planning count --------------------
export const getAssortmentPlannigCountRequest = createAction(constants.GET_ASSORTMENT_PLANNING_COUNT_REQUEST);
export const getAssortmentPlannigCountSuccess = createAction(constants.GET_ASSORTMENT_PLANNING_COUNT_SUCCESS);
export const getAssortmentPlannigCountError = createAction(constants.GET_ASSORTMENT_PLANNING_COUNT_ERROR);
export const getAssortmentPlannigCountClear = createAction(constants.GET_ASSORTMENT_PLANNING_COUNT_CLEAR);

// ---------------------------------------assortment planning detail get --------------------
export const getAssortmentPlanningDetailRequest = createAction(constants.GET_ASSORTMENT_PLANNING_DETAIL_REQUEST);
export const getAssortmentPlanningDetailSuccess = createAction(constants.GET_ASSORTMENT_PLANNING_DETAIL_SUCCESS);
export const getAssortmentPlanningDetailError = createAction(constants.GET_ASSORTMENT_PLANNING_DETAIL_ERROR);
export const getAssortmentPlanningDetailClear = createAction(constants.GET_ASSORTMENT_PLANNING_DETAIL_CLEAR);

// ---------------------------------------assortment planning detail save --------------------
export const saveAssortmentPlannigDetailRequest = createAction(constants.SAVE_ASSORTMENT_PLANNING_DETAIL_REQUEST);
export const saveAssortmentPlannigDetailSuccess = createAction(constants.SAVE_ASSORTMENT_PLANNING_DETAIL_SUCCESS);
export const saveAssortmentPlannigDetailError = createAction(constants.SAVE_ASSORTMENT_PLANNING_DETAIL_ERROR);
export const saveAssortmentPlannigDetailClear = createAction(constants.SAVE_ASSORTMENT_PLANNING_DETAIL_CLEAR);

// Role Master
export const checkRoleNameRequest = createAction(constants.CHECK_ROLE_NAME_REQUEST);
export const checkRoleNameSuccess = createAction(constants.CHECK_ROLE_NAME_SUCCESS);
export const checkRoleNameError = createAction(constants.CHECK_ROLE_NAME_ERROR);
export const checkRoleNameClear = createAction(constants.CHECK_ROLE_NAME_CLEAR);


export const roleSubmitRequest = createAction(constants.SUBMIT_ROLE_REQUEST);
export const roleSubmitSuccess = createAction(constants.SUBMIT_ROLE_SUCCESS);
export const roleSubmitError = createAction(constants.SUBMIT_ROLE_ERROR);
export const roleSubmitClear = createAction(constants.SUBMIT_ROLE_CLEAR);

export const roleUpdateDataRequest = createAction(constants.UPDATE_ROLE_DATA_REQUEST);
export const roleUpdateDataSuccess = createAction(constants.UPDATE_ROLE_DATA_SUCCESS);
export const roleUpdateDataError = createAction(constants.UPDATE_ROLE_DATA_ERROR);
export const roleUpdateDataClear = createAction(constants.UPDATE_ROLE_DATA_CLEAR);


// ---------------------------------------PI PO alertPopUp modalBoolean --------------------

export const displayModalBool = createAction(constants.PI_PO_MODAL);

// ----------------------------------------Excel Download PO ----------------------------------

export const getExcelDownloadedPoRequest = createAction(constants.GET_EXCEL_DOWNLOADED_REQUEST);
export const getExcelDownloadedPoSuccess = createAction(constants.GET_EXCEL_DOWNLOADED_SUCCESS);
export const getExcelDownloadedPoError = createAction(constants.GET_EXCEL_DOWNLOADED_ERROR);
export const getExcelDownloadedPoClear = createAction(constants.GET_EXCEL_DOWNLOADED_CLEAR);

// ----------------------------------------Excel Download PI ----------------------------------

export const getExcelDownloadedPiRequest = createAction(constants.GET_EXCEL_DOWNLOADED_REQUEST_PI);
export const getExcelDownloadedPiSuccess = createAction(constants.GET_EXCEL_DOWNLOADED_SUCCESS_PI);
export const getExcelDownloadedPiError = createAction(constants.GET_EXCEL_DOWNLOADED_ERROR_PI);
export const getExcelDownloadedPiClear = createAction(constants.GET_EXCEL_DOWNLOADED_CLEAR_PI);

//Api Logs
export const getModuleApiLogsRequest = createAction(constants.GET_MODULE_API_LOGS_REQUEST);
export const getModuleApiLogsSuccess = createAction(constants.GET_MODULE_API_LOGS_SUCCESS);
export const getModuleApiLogsError = createAction(constants.GET_MODULE_API_LOGS_ERROR);
export const getModuleApiLogsClear = createAction(constants.GET_MODULE_API_LOGS_CLEAR);

// ----------------------------------------Status Button Enable Disable Configuration ----------------------------------

export const getStatusButtonActiveRequest = createAction(constants.GET_STATUS_BUTTON_ACTIVE_REQUEST);
export const getStatusButtonActiveSuccess = createAction(constants.GET_STATUS_BUTTON_ACTIVE_SUCCESS);
export const getStatusButtonActiveError = createAction(constants.GET_STATUS_BUTTON_ACTIVE_ERROR);
export const getStatusButtonActiveClear = createAction(constants.GET_STATUS_BUTTON_ACTIVE_CLEAR);
// ---------------------------------------- vendor product catalogue add product manual request -----------------------------------------
// export const productCatalogueDepartmentRequest = createAction(constants.PRODUCT_CATALOGUE_DEPARTMENT_REQUEST)
// export const productCatalogueDepartmentSuccess = createAction(constants.PRODUCT_CATALOGUE_DEPARTMENT_SUCCESS);
// export const productCatalogueDepartmentError = createAction(constants.PRODUCT_CATALOGUE_DEPARTMENT_ERROR);

export const productCatalogueCategoryRequest = createAction(constants.PRODUCT_CATALOGUE_CATEGORY_REQUEST);
export const productCatalogueCategorySuccess = createAction(constants.PRODUCT_CATALOGUE_CATEGORY_SUCCESS);
export const productCatalogueCategoryError = createAction(constants.PRODUCT_CATALOGUE_CATEGORY_ERROR);

export const productCatalogueSizeRequest = createAction(constants.PRODUCT_CATALOGUE_SIZE_REQUEST);
export const productCatalogueSizeSuccess = createAction(constants.PRODUCT_CATALOGUE_SIZE_SUCCESS);
export const productCatalogueSizeError = createAction(constants.PRODUCT_CATALOGUE_SIZE_ERROR);

export const productCatalogueColourRequest = createAction(constants.PRODUCT_CATALOGUE_COLOUR_REQUEST);
export const productCatalogueColourSuccess = createAction(constants.PRODUCT_CATALOGUE_COLOUR_SUCCESS);
export const productCatalogueColourError = createAction(constants.PRODUCT_CATALOGUE_COLOUR_ERROR);

export const productCataloguePatternRequest = createAction(constants.PRODUCT_CATALOGUE_PATTERN_REQUEST);
export const productCataloguePatternSuccess = createAction(constants.PRODUCT_CATALOGUE_PATTERN_SUCCESS);
export const productCataloguePatternError = createAction(constants.PRODUCT_CATALOGUE_PATTERN_ERROR);

export const productCatalogueSeasonRequest = createAction(constants.PRODUCT_CATALOGUE_SEASON_REQUEST);
export const productCatalogueSeasonSuccess = createAction(constants.PRODUCT_CATALOGUE_SEASON_SUCCESS);
export const productCatalogueSeasonError = createAction(constants.PRODUCT_CATALOGUE_SEASON_ERROR);

export const productCatalogueNeckRequest = createAction(constants.PRODUCT_CATALOGUE_NECK_REQUEST);
export const productCatalogueNeckSuccess = createAction(constants.PRODUCT_CATALOGUE_NECK_SUCCESS);
export const productCatalogueNeckError = createAction(constants.PRODUCT_CATALOGUE_NECK_ERROR);

//New custom header rearrange
export const getMainHeaderConfigRequest = createAction(constants.GET_MAIN_HEADER_CONFIG_REQUEST);
export const getMainHeaderConfigSuccess = createAction(constants.GET_MAIN_HEADER_CONFIG_SUCCESS);
export const getMainHeaderConfigError = createAction(constants.GET_MAIN_HEADER_CONFIG_ERROR);
export const getMainHeaderConfigClear = createAction(constants.GET_MAIN_HEADER_CONFIG_CLEAR);

export const getSetHeaderConfigRequest = createAction(constants.GET_SET_HEADER_CONFIG_REQUEST);
export const getSetHeaderConfigSuccess = createAction(constants.GET_SET_HEADER_CONFIG_SUCCESS);
export const getSetHeaderConfigError = createAction(constants.GET_SET_HEADER_CONFIG_ERROR);
export const getSetHeaderConfigClear = createAction(constants.GET_SET_HEADER_CONFIG_CLEAR);

export const getItemHeaderConfigRequest = createAction(constants.GET_ITEM_HEADER_CONFIG_REQUEST);
export const getItemHeaderConfigSuccess = createAction(constants.GET_ITEM_HEADER_CONFIG_SUCCESS);
export const getItemHeaderConfigError = createAction(constants.GET_ITEM_HEADER_CONFIG_ERROR);
export const getItemHeaderConfigClear = createAction(constants.GET_ITEM_HEADER_CONFIG_CLEAR);



export const productCatalogueFabricRequest = createAction(constants.PRODUCT_CATALOGUE_FABRIC_REQUEST);
export const productCatalogueFabricSuccess = createAction(constants.PRODUCT_CATALOGUE_FABRIC_SUCCESS);
export const productCatalogueFabricError = createAction(constants.PRODUCT_CATALOGUE_FABRIC_ERROR);

export const productCatalogueUsagesRequest = createAction(constants.PRODUCT_CATALOGUE_USAGES_REQUEST);
export const productCatalogueUsagesSuccess = createAction(constants.PRODUCT_CATALOGUE_USAGES_SUCCESS);
export const productCatalogueUsagesError = createAction(constants.PRODUCT_CATALOGUE_USAGES_ERROR);

export const productCatalogueManualRequest = createAction(constants.PRODUCT_CATALOGUE_MANUALSUBMIT_REQUEST);
export const productCatalogueManualSuccess = createAction(constants.PRODUCT_CATALOGUE_MANUALSUBMIT_SUCCESS);
export const productCatalogueManualError = createAction(constants.PRODUCT_CATALOGUE_MANUALSUBMIT_ERROR);

// vendor catalogue submission history
export const productCatalogueSubmissionHistoryRequest = createAction(constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_REQUEST);
export const productCatalogueSubmissionHistorySuccess = createAction(constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_SUCCESS);
export const productCatalogueSubmissionHistoryError = createAction(constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_ERROR);

export const productCatalogueSubmissionHistoryDetailsRequest = createAction(constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_DETAILS_REQUEST);
export const productCatalogueSubmissionHistoryDetailsSuccess = createAction(constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_DETAILS_SUCCESS);
export const productCatalogueSubmissionHistoryDetailsError = createAction(constants.PRODUCT_CATALOGUE_SUBMISSION_HISTORY_DETAILS_ERROR);

export const productCatalogueHistoryHeaderRequest = createAction(constants.PRODUCT_CATALOGUE_HISTORY_HEADER_REQUEST);
export const productCatalogueHistoryHeaderSuccess = createAction(constants.PRODUCT_CATALOGUE_HISTORY_HEADER_SUCCESS);
export const productCatalogueHistoryHeaderError = createAction(constants.PRODUCT_CATALOGUE_HISTORY_HEADER_ERROR);
//vendor portal in vendor side inspection ON OFF:: 
export const getInspOnOffConfigRequest = createAction(constants.INSP_ON_OFF_CONFIG_REQUEST);
export const getInspOnOffConfigSuccess = createAction(constants.INSP_ON_OFF_CONFIG_SUCCESS);
export const getInspOnOffConfigError = createAction(constants.INSP_ON_OFF_CONFIG_ERROR);
export const getInspOnOffConfigClear = createAction(constants.INSP_ON_OFF_CONFIG_CLEAR);

//approval based LR creation::
export const invoiceApprovalBasedLRRequest = createAction(constants.INVOICE_BASED_LR_REQUEST);
export const invoiceApprovalBasedLRSuccess = createAction(constants.INVOICE_BASED_LR_SUCCESS);
export const invoiceApprovalBasedLRError = createAction(constants.INVOICE_BASED_LR_ERROR);
export const invoiceApprovalBasedLRClear = createAction(constants.INVOICE_BASED_LR_CLEAR);

// Purchase indent image url request
export const piImageUrlRequest = createAction(constants.PI_IMAGE_URL_REQUEST);
export const piImageUrlSuccess = createAction(constants.PI_IMAGE_URL_SUCCESS);
export const piImageUrlError = createAction(constants.PI_IMAGE_URL_ERROR);
export const piImageUrlClear = createAction(constants.PI_IMAGE_URL_CLEAR);
//approval based LR creation::
export const getSlRequest = createAction(constants.GET_SL_REQUEST);
export const getSlSuccess = createAction(constants.GET_SL_SUCCESS);
export const getSlError = createAction(constants.GET_SL_ERROR);
export const getSlClear = createAction(constants.GET_SL_CLEAR);


// update Header
export const updateHeaderRequest = createAction(constants.UPDATE_HEADER_REQUEST);
export const updateHeaderSuccess = createAction(constants.UPDATE_HEADER_SUCCESS);
export const updateHeaderError = createAction(constants.UPDATE_HEADER_ERROR);
export const updateHeaderClear = createAction(constants.UPDATE_HEADER_CLEAR);

// Get Address
export const getAddressRequest = createAction(constants.GET_ADDRESS_REQUEST);
export const getAddressSuccess = createAction(constants.GET_ADDRESS_SUCCESS);
export const getAddressError = createAction(constants.GET_ADDRESS_ERROR);
export const getAddressClear = createAction(constants.GET_ADDRESS_CLEAR);

// Get Location
export const getLocationRequest = createAction(constants.GET_LOCATION_REQUEST);
export const getLocationSuccess = createAction(constants.GET_LOCATION_SUCCESS);
export const getLocationError = createAction(constants.GET_LOCATION_ERROR);
export const getLocationClear = createAction(constants.GET_LOCATION_CLEAR);
//role delte request
export const roleDeleteRequest = createAction(constants.ROLE_DELETE_REQUEST);
export const roleDeleteSuccess = createAction(constants.ROLE_DELETE_SUCCESS);
export const roleDeleteError = createAction(constants.ROLE_DELETE_ERROR);
export const roleDeleteClear = createAction(constants.ROLE_DELETE_CLEAR);
//get activity::
export const userActivityRequest = createAction(constants.USER_ACTIVITY_REQUEST);
export const userActivitySuccess = createAction(constants.USER_ACTIVITY_SUCCESS);
export const userActivityError = createAction(constants.USER_ACTIVITY_ERROR);
export const userActivityClear = createAction(constants.USER_ACTIVITY_CLEAR);

export const approvePOConfirmRequest = createAction(constants.APPROVE_PO_CONFIRM_REQUEST);
export const approvePOConfirmSuccess = createAction(constants.APPROVE_PO_CONFIRM_SUCCESS);
export const approvePOConfirmError = createAction(constants.APPROVE_PO_CONFIRM_ERROR);
export const approvePOConfirmClear = createAction(constants.APPROVE_PO_CONFIRM_CLEAR);

export const getCancelAndRejectReasonRequest = createAction(constants.CANCEL_AND_REJECT_REASON_REQUEST);
export const getCancelAndRejectReasonSuccess = createAction(constants.CANCEL_AND_REJECT_REASON_SUCCESS);
export const getCancelAndRejectReasonError = createAction(constants.CANCEL_AND_REJECT_REASON_ERROR);
export const getCancelAndRejectReasonClear = createAction(constants.CANCEL_AND_REJECT_REASON_CLEAR);
//Payment Status::
export const getPaymentStatusRequest = createAction(constants.GET_PAYMENT_STATUS_REQUEST);
export const getPaymentStatusSuccess = createAction(constants.GET_PAYMENT_STATUS_SUCCESS);
export const getPaymentStatusError = createAction(constants.GET_PAYMENT_STATUS_ERROR);
export const getPaymentStatusClear = createAction(constants.GET_PAYMENT_STATUS_CLEAR);

//------------------------ GET SEASON PLANNING ------------------------

export const getSeasonPlanningRequest = createAction(constants.GET_SEASON_PLANNING_REQUEST);
export const getSeasonPlanningSuccess = createAction(constants.GET_SEASON_PLANNING_SUCCESS);
export const getSeasonPlanningError = createAction(constants.GET_SEASON_PLANNING_ERROR);
export const getSeasonPlanningClear = createAction(constants.GET_SEASON_PLANNING_CLEAR);

//------------------------ DELETE SEASON PLANNING ------------------------

export const deleteSeasonPlanningRequest = createAction(constants.DELETE_SEASON_PLANNING_REQUEST);
export const deleteSeasonPlanningSuccess = createAction(constants.DELETE_SEASON_PLANNING_SUCCESS);
export const deleteSeasonPlanningError = createAction(constants.DELETE_SEASON_PLANNING_ERROR);
export const deleteSeasonPlanningClear = createAction(constants.DELETE_SEASON_PLANNING_CLEAR);

//------------------------ SAVE SEASON PLANNING ------------------------

export const saveSeasonPlanningRequest = createAction(constants.SAVE_SEASON_PLANNING_REQUEST);
export const saveSeasonPlanningSuccess = createAction(constants.SAVE_SEASON_PLANNING_SUCCESS);
export const saveSeasonPlanningError = createAction(constants.SAVE_SEASON_PLANNING_ERROR);
export const saveSeasonPlanningClear = createAction(constants.SAVE_SEASON_PLANNING_CLEAR);

//------------------------ GET EVENT PLANNING ------------------------

export const getEventPlanningRequest = createAction(constants.GET_EVENT_PLANNING_REQUEST);
export const getEventPlanningSuccess = createAction(constants.GET_EVENT_PLANNING_SUCCESS);
export const getEventPlanningError = createAction(constants.GET_EVENT_PLANNING_ERROR);
export const getEventPlanningClear = createAction(constants.GET_EVENT_PLANNING_CLEAR);

//------------------------ SAVE EVENT PLANNING ------------------------

export const saveEventPlanningRequest = createAction(constants.SAVE_EVENT_PLANNING_REQUEST);
export const saveEventPlanningSuccess = createAction(constants.SAVE_EVENT_PLANNING_SUCCESS);
export const saveEventPlanningError = createAction(constants.SAVE_EVENT_PLANNING_ERROR);
export const saveEventPlanningClear = createAction(constants.SAVE_EVENT_PLANNING_CLEAR);

//------------------------ DELETE EVENT PLANNING ------------------------

export const deleteEventPlanningRequest = createAction(constants.DELETE_EVENT_PLANNING_REQUEST);
export const deleteEventPlanningSuccess = createAction(constants.DELETE_EVENT_PLANNING_SUCCESS);
export const deleteEventPlanningError = createAction(constants.DELETE_EVENT_PLANNING_ERROR);
export const deleteEventPlanningClear = createAction(constants.DELETE_EVENT_PLANNING_CLEAR);

//------------------------ GET SITE PLANNING ------------------------

export const getSitePlanningRequest = createAction(constants.GET_SITE_PLANNING_REQUEST);
export const getSitePlanningSuccess = createAction(constants.GET_SITE_PLANNING_SUCCESS);
export const getSitePlanningError = createAction(constants.GET_SITE_PLANNING_ERROR);
export const getSitePlanningClear = createAction(constants.GET_SITE_PLANNING_CLEAR);

//------------------------ GET SITE PLANNING FILTERS ------------------------

export const getSitePlanningFiltersRequest = createAction(constants.GET_SITE_PLANNING_FILTERS_REQUEST);
export const getSitePlanningFiltersSuccess = createAction(constants.GET_SITE_PLANNING_FILTERS_SUCCESS);
export const getSitePlanningFiltersError = createAction(constants.GET_SITE_PLANNING_FILTERS_ERROR);
export const getSitePlanningFiltersClear = createAction(constants.GET_SITE_PLANNING_FILTERS_CLEAR);

//------------------------ SAVE SITE PLANNING ------------------------

export const saveSitePlanningRequest = createAction(constants.SAVE_SITE_PLANNING_REQUEST);
export const saveSitePlanningSuccess = createAction(constants.SAVE_SITE_PLANNING_SUCCESS);
export const saveSitePlanningError = createAction(constants.SAVE_SITE_PLANNING_ERROR);
export const saveSitePlanningClear = createAction(constants.SAVE_SITE_PLANNING_CLEAR);

//------------------------ DELETE SITE PLANNING ------------------------

export const deleteSitePlanningRequest = createAction(constants.DELETE_SITE_PLANNING_REQUEST);
export const deleteSitePlanningSuccess = createAction(constants.DELETE_SITE_PLANNING_SUCCESS);
export const deleteSitePlanningError = createAction(constants.DELETE_SITE_PLANNING_ERROR);
export const deleteSitePlanningClear = createAction(constants.DELETE_SITE_PLANNING_CLEAR);

//------------------------ GET PLANNING PARAMETER ------------------------

export const getPlanningParameterRequest = createAction(constants.GET_PLANNING_PARAMETER_REQUEST);
export const getPlanningParameterSuccess = createAction(constants.GET_PLANNING_PARAMETER_SUCCESS);
export const getPlanningParameterError = createAction(constants.GET_PLANNING_PARAMETER_ERROR);
export const getPlanningParameterClear = createAction(constants.GET_PLANNING_PARAMETER_CLEAR);

//------------------------ SAVE PLANNING PARAMETER ------------------------

export const savePlanningParameterRequest = createAction(constants.SAVE_PLANNING_PARAMETER_REQUEST);
export const savePlanningParameterSuccess = createAction(constants.SAVE_PLANNING_PARAMETER_SUCCESS);
export const savePlanningParameterError = createAction(constants.SAVE_PLANNING_PARAMETER_ERROR);

//------------------------ GET ITEM CONFIGURATION ------------------------

export const getItemConfigurationRequest = createAction(constants.GET_ITEM_CONFIGURATION_REQUEST);
export const getItemConfigurationSuccess = createAction(constants.GET_ITEM_CONFIGURATION_SUCCESS);
export const getItemConfigurationError = createAction(constants.GET_ITEM_CONFIGURATION_ERROR);
export const getItemConfigurationClear = createAction(constants.GET_ITEM_CONFIGURATION_CLEAR);

//------------------------ SAVE ITEM CONFIGURATION ------------------------

export const saveItemConfigurationRequest = createAction(constants.SAVE_ITEM_CONFIGURATION_REQUEST);
export const saveItemConfigurationSuccess = createAction(constants.SAVE_ITEM_CONFIGURATION_SUCCESS);
export const saveItemConfigurationError = createAction(constants.SAVE_ITEM_CONFIGURATION_ERROR);

//------------------------ GET SITE AND ITEM FILTER ------------------------

export const getSiteAndItemFilterRequest = createAction(constants.GET_SITE_AND_ITEM_FILTER_REQUEST);
export const getSiteAndItemFilterSuccess = createAction(constants.GET_SITE_AND_ITEM_FILTER_SUCCESS);
export const getSiteAndItemFilterError = createAction(constants.GET_SITE_AND_ITEM_FILTER_ERROR);
export const getSiteAndItemFilterClear = createAction(constants.GET_SITE_AND_ITEM_FILTER_CLEAR);

//------------------------ SAVE SITE AND ITEM FILTER ------------------------

export const saveSiteAndItemFilterRequest = createAction(constants.SAVE_SITE_AND_ITEM_FILTER_REQUEST);
export const saveSiteAndItemFilterSuccess = createAction(constants.SAVE_SITE_AND_ITEM_FILTER_SUCCESS);
export const saveSiteAndItemFilterError = createAction(constants.SAVE_SITE_AND_ITEM_FILTER_ERROR);
export const saveSiteAndItemFilterClear = createAction(constants.SAVE_SITE_AND_ITEM_FILTER_CLEAR);

//------------------------ GET SITE AND ITEM FILTER CONFIGURATION ------------------------

export const getSiteAndItemFilterConfigurationRequest = createAction(constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_REQUEST);
export const getSiteAndItemFilterConfigurationSuccess = createAction(constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_SUCCESS);
export const getSiteAndItemFilterConfigurationError = createAction(constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_ERROR);
export const getSiteAndItemFilterConfigurationClear = createAction(constants.GET_SITE_AND_ITEM_FILTER_CONFIGURATION_CLEAR);

//------------------------ GET ARS GENERIC FILTERS ------------------------

export const getArsGenericFiltersRequest = createAction(constants.GET_ARS_GENERIC_FILTERS_REQUEST);
export const getArsGenericFiltersSuccess = createAction(constants.GET_ARS_GENERIC_FILTERS_SUCCESS);
export const getArsGenericFiltersError = createAction(constants.GET_ARS_GENERIC_FILTERS_ERROR);
export const getArsGenericFiltersClear = createAction(constants.GET_ARS_GENERIC_FILTERS_CLEAR);

//------------------------ GET ADHOC ------------------------

export const getAdhocRequest = createAction(constants.GET_ADHOC_REQUEST);
export const getAdhocSuccess = createAction(constants.GET_ADHOC_SUCCESS);
export const getAdhocError = createAction(constants.GET_ADHOC_ERROR);
export const getAdhocClear = createAction(constants.GET_ADHOC_CLEAR);

//------------------------ DELETE ADHOC ------------------------

export const deleteAdhocRequest = createAction(constants.DELETE_ADHOC_REQUEST);
export const deleteAdhocSuccess = createAction(constants.DELETE_ADHOC_SUCCESS);
export const deleteAdhocError = createAction(constants.DELETE_ADHOC_ERROR);
export const deleteAdhocClear = createAction(constants.DELETE_ADHOC_CLEAR);

//------------------------ UPDATE ADHOC ------------------------

export const updateAdhocRequest = createAction(constants.UPDATE_ADHOC_REQUEST);
export const updateAdhocSuccess = createAction(constants.UPDATE_ADHOC_SUCCESS);
export const updateAdhocError = createAction(constants.UPDATE_ADHOC_ERROR);
export const updateAdhocClear = createAction(constants.UPDATE_ADHOC_CLEAR);

//------------------------ SEARCH ADHOC SITE ------------------------

export const searchAdhocSiteRequest = createAction(constants.SEARCH_ADHOC_SITE_REQUEST);
export const searchAdhocSiteSuccess = createAction(constants.SEARCH_ADHOC_SITE_SUCCESS);
export const searchAdhocSiteError = createAction(constants.SEARCH_ADHOC_SITE_ERROR);
export const searchAdhocSiteClear = createAction(constants.SEARCH_ADHOC_SITE_CLEAR);

//------------------------ SEARCH ADHOC ITEM ------------------------

export const searchAdhocItemRequest = createAction(constants.SEARCH_ADHOC_ITEM_REQUEST);
export const searchAdhocItemSuccess = createAction(constants.SEARCH_ADHOC_ITEM_SUCCESS);
export const searchAdhocItemError = createAction(constants.SEARCH_ADHOC_ITEM_ERROR);
export const searchAdhocItemClear = createAction(constants.SEARCH_ADHOC_ITEM_CLEAR);

//------------------------ SAVE ADHOC ------------------------

export const saveAdhocRequest = createAction(constants.SAVE_ADHOC_REQUEST);
export const saveAdhocSuccess = createAction(constants.SAVE_ADHOC_SUCCESS);
export const saveAdhocError = createAction(constants.SAVE_ADHOC_ERROR);
export const saveAdhocClear = createAction(constants.SAVE_ADHOC_CLEAR);

//------------------------ GET ADHOC ITEMS PER WO ------------------------

export const getAdhocItemsPerWORequest = createAction(constants.GET_ADHOC_ITEMS_PER_WO_REQUEST);
export const getAdhocItemsPerWOSuccess = createAction(constants.GET_ADHOC_ITEMS_PER_WO_SUCCESS);
export const getAdhocItemsPerWOError = createAction(constants.GET_ADHOC_ITEMS_PER_WO_ERROR);
export const getAdhocItemsPerWOClear = createAction(constants.GET_ADHOC_ITEMS_PER_WO_CLEAR);

//------------------------ UPDATE ADHOC ITEMS PER WO ------------------------

export const updateAdhocItemsPerWORequest = createAction(constants.UPDATE_ADHOC_ITEMS_PER_WO_REQUEST);
export const updateAdhocItemsPerWOSuccess = createAction(constants.UPDATE_ADHOC_ITEMS_PER_WO_SUCCESS);
export const updateAdhocItemsPerWOError = createAction(constants.UPDATE_ADHOC_ITEMS_PER_WO_ERROR);
export const updateAdhocItemsPerWOClear = createAction(constants.UPDATE_ADHOC_ITEMS_PER_WO_CLEAR);

//------------------------ GET MRP RANGE ------------------------

export const getMRPRangeRequest = createAction(constants.GET_MRP_RANGE_REQUEST);
export const getMRPRangeSuccess = createAction(constants.GET_MRP_RANGE_SUCCESS);
export const getMRPRangeError = createAction(constants.GET_MRP_RANGE_ERROR);
export const getMRPRangeClear = createAction(constants.GET_MRP_RANGE_CLEAR);

//------------------------ UPDATE MRP RANGE ------------------------

export const updateMRPRangeRequest = createAction(constants.UPDATE_MRP_RANGE_REQUEST);
export const updateMRPRangeSuccess = createAction(constants.UPDATE_MRP_RANGE_SUCCESS);
export const updateMRPRangeError = createAction(constants.UPDATE_MRP_RANGE_ERROR);
export const updateMRPRangeClear = createAction(constants.UPDATE_MRP_RANGE_CLEAR);

//------------------------ GET ALLOCATION BASED ON ITEM ------------------------

export const getAllocationBasedOnItemRequest = createAction(constants.GET_ALLOCATION_BASED_ON_ITEM_REQUEST);
export const getAllocationBasedOnItemSuccess = createAction(constants.GET_ALLOCATION_BASED_ON_ITEM_SUCCESS);
export const getAllocationBasedOnItemError = createAction(constants.GET_ALLOCATION_BASED_ON_ITEM_ERROR);
export const getAllocationBasedOnItemClear = createAction(constants.GET_ALLOCATION_BASED_ON_ITEM_CLEAR);

//------------------------ SAVE ALLOCATION BASED ON ITEM ------------------------

export const saveAllocationBasedOnItemRequest = createAction(constants.SAVE_ALLOCATION_BASED_ON_ITEM_REQUEST);
export const saveAllocationBasedOnItemSuccess = createAction(constants.SAVE_ALLOCATION_BASED_ON_ITEM_SUCCESS);
export const saveAllocationBasedOnItemError = createAction(constants.SAVE_ALLOCATION_BASED_ON_ITEM_ERROR);
export const saveAllocationBasedOnItemClear = createAction(constants.SAVE_ALLOCATION_BASED_ON_ITEM_CLEAR);

//------------------------ GET ASSORTMENT PLANNING ------------------------

export const getAssortmentPlanningRequest = createAction(constants.GET_ASSORTMENT_PLANNING_REQUEST);
export const getAssortmentPlanningSuccess = createAction(constants.GET_ASSORTMENT_PLANNING_SUCCESS);
export const getAssortmentPlanningError = createAction(constants.GET_ASSORTMENT_PLANNING_ERROR);
export const getAssortmentPlanningClear = createAction(constants.GET_ASSORTMENT_PLANNING_CLEAR);

//------------------------ FIND FILTER ASSORTMENT ------------------------

export const findFilterAssortmentRequest = createAction(constants.FIND_FILTER_ASSORTMENT_REQUEST);
export const findFilterAssortmentSuccess = createAction(constants.FIND_FILTER_ASSORTMENT_SUCCESS);
export const findFilterAssortmentError = createAction(constants.FIND_FILTER_ASSORTMENT_ERROR);
export const findFilterAssortmentClear = createAction(constants.FIND_FILTER_ASSORTMENT_CLEAR);

//------------------------ SAVE ASSORTMENT PLANNING ------------------------

export const saveAssortmentPlanningRequest = createAction(constants.SAVE_ASSORTMENT_PLANNING_REQUEST);
export const saveAssortmentPlanningSuccess = createAction(constants.SAVE_ASSORTMENT_PLANNING_SUCCESS);
export const saveAssortmentPlanningError = createAction(constants.SAVE_ASSORTMENT_PLANNING_ERROR);
export const saveAssortmentPlanningClear = createAction(constants.SAVE_ASSORTMENT_PLANNING_CLEAR);

//------------------------ GET INVENTORY PLANNING REPORT ------------------------

export const getInventoryPlanningReportRequest = createAction(constants.GET_INVENTORY_PLANNING_REPORT_REQUEST);
export const getInventoryPlanningReportSuccess = createAction(constants.GET_INVENTORY_PLANNING_REPORT_SUCCESS);
export const getInventoryPlanningReportError = createAction(constants.GET_INVENTORY_PLANNING_REPORT_ERROR);
export const getInventoryPlanningReportClear = createAction(constants.GET_INVENTORY_PLANNING_REPORT_CLEAR);

//------------------------ GET SALES INVENTORY REPORT ------------------------

export const getSalesInventoryReportRequest = createAction(constants.GET_SALES_INVENTORY_REPORT_REQUEST);
export const getSalesInventoryReportSuccess = createAction(constants.GET_SALES_INVENTORY_REPORT_SUCCESS);
export const getSalesInventoryReportError = createAction(constants.GET_SALES_INVENTORY_REPORT_ERROR);
export const getSalesInventoryReportClear = createAction(constants.GET_SALES_INVENTORY_REPORT_CLEAR);

//------------------------ UPDATE INVENTORY PLANNING REPORT ------------------------

export const updateInventoryPlanningReportRequest = createAction(constants.UPDATE_INVENTORY_PLANNING_REPORT_REQUEST);
export const updateInventoryPlanningReportSuccess = createAction(constants.UPDATE_INVENTORY_PLANNING_REPORT_SUCCESS);
export const updateInventoryPlanningReportError = createAction(constants.UPDATE_INVENTORY_PLANNING_REPORT_ERROR);
export const updateInventoryPlanningReportClear = createAction(constants.UPDATE_INVENTORY_PLANNING_REPORT_CLEAR);

//------------------------ DOWNLOAD REPORT ------------------------

export const downloadReportRequest = createAction(constants.DOWNLOAD_REPORT_REQUEST);
export const downloadReportSuccess = createAction(constants.DOWNLOAD_REPORT_SUCCESS);
export const downloadReportError = createAction(constants.DOWNLOAD_REPORT_ERROR);
export const downloadReportClear = createAction(constants.DOWNLOAD_REPORT_CLEAR);


//-------------------- GET ALL TRANSPORTERS --------------------
export const getAllManageTransporterRequest = createAction(constants.GET_ALL_MANAGE_TRANSPORTER_REQUEST);
export const getAllManageTransporterSuccess = createAction(constants.GET_ALL_MANAGE_TRANSPORTER_SUCCESS);
export const getAllManageTransporterError = createAction(constants.GET_ALL_MANAGE_TRANSPORTER_ERROR);
export const getAllManageTransporterClear = createAction(constants.GET_ALL_MANAGE_TRANSPORTER_CLEAR);

//CREATE TRANSPORTER
export const createTransporterRequest = createAction(constants.CREATE_TRANSPORTER_REQUEST);
export const createTransporterSuccess = createAction(constants.CREATE_TRANSPORTER_SUCCESS);
export const createTransporterError = createAction(constants.CREATE_TRANSPORTER_ERROR);
export const createTransporterClear = createAction(constants.CREATE_TRANSPORTER_CLEAR);

//UPDATE TRANSPORTER
export const updateManageTransporterRequest = createAction(constants.UPDATE_MANAGE_TRANSPORTER_REQUEST);
export const updateManageTransporterSuccess = createAction(constants.UPDATE_MANAGE_TRANSPORTER_SUCCESS);
export const updateManageTransporterError = createAction(constants.UPDATE_MANAGE_TRANSPORTER_ERROR);
export const updateManageTransporterClear = createAction(constants.UPDATE_MANAGE_TRANSPORTER_CLEAR);

export const updateCreatedTransporterRequest = createAction(constants.UPDATE_CREATED_TRANSPORTER_REQUEST);
// Procurment configuration

export const accessVendorPortalRequest = createAction(constants.ACCESS_VENDOR_PORTAL_REQUEST);
export const accessVendorPortalSuccess = createAction(constants.ACCESS_VENDOR_PORTAL_SUCCESS);
export const accessVendorPortalClear = createAction(constants.ACCESS_VENDOR_PORTAL_CLEAR);
export const accessVendorPortalError = createAction(constants.ACCESS_VENDOR_PORTAL_ERROR);

export const getSubscriptionRequest = createAction(constants.GET_SUBSCRIPTION_REQUEST);
export const getSubscriptionSuccess = createAction(constants.GET_SUBSCRIPTION_SUCCESS);
export const getSubscriptionError = createAction(constants.GET_SUBSCRIPTION_ERROR);
export const getSubscriptionClear = createAction(constants.GET_SUBSCRIPTION_CLEAR);

export const getTransactionRequest = createAction(constants.GET_TRANSACTION_REQUEST);
export const getTransactionSuccess = createAction(constants.GET_TRANSACTION_SUCCESS);
export const getTransactionClear = createAction(constants.GET_TRANSACTION_CLEAR);
export const getTransactionError = createAction(constants.GET_TRANSACTION_ERROR);

export const getAllCommentsRequest = createAction(constants.GET_ALL_COMMENTS_REQUEST);
export const getAllCommentsSuccess = createAction(constants.GET_ALL_COMMENTS_SUCCESS);
export const getAllCommentsError = createAction(constants.GET_ALL_COMMENTS_ERROR);
export const getAllCommentsClear = createAction(constants.GET_ALL_COMMENTS_CLEAR);
// Procurment configuration
export const updateItemUdfMappingRequest = createAction(constants.UPDATE_ITEM_UDF_MAPPING_REQUEST);
export const updateItemUdfMappingSuccess = createAction(constants.UPDATE_ITEM_UDF_MAPPING_SUCCESS);
export const updateItemUdfMappingError = createAction(constants.UPDATE_ITEM_UDF_MAPPING_ERROR);
export const updateItemUdfMappingClear = createAction(constants.UPDATE_ITEM_UDF_MAPPING_CLEAR);
//-------------------- GET ALL CUSTOMERS --------------------
export const getAllManageCustomerRequest = createAction(constants.GET_ALL_MANAGE_CUSTOMER_REQUEST);
export const getAllManageCustomerSuccess = createAction(constants.GET_ALL_MANAGE_CUSTOMER_SUCCESS);
export const getAllManageCustomerError = createAction(constants.GET_ALL_MANAGE_CUSTOMER_ERROR);
export const getAllManageCustomerClear = createAction(constants.GET_ALL_MANAGE_CUSTOMER_CLEAR);

//-------------------- GET ALL ITEMS --------------------
export const getAllManageItemRequest = createAction(constants.GET_ALL_MANAGE_ITEM_REQUEST);
export const getAllManageItemSuccess = createAction(constants.GET_ALL_MANAGE_ITEM_SUCCESS);
export const getAllManageItemError = createAction(constants.GET_ALL_MANAGE_ITEM_ERROR);
export const getAllManageItemClear = createAction(constants.GET_ALL_MANAGE_ITEM_CLEAR);

//-------------------- GET ALL SALES AGENTS --------------------
export const getAllManageSalesAgentRequest = createAction(constants.GET_ALL_MANAGE_SALES_AGENT_REQUEST);
export const getAllManageSalesAgentSuccess = createAction(constants.GET_ALL_MANAGE_SALES_AGENT_SUCCESS);
export const getAllManageSalesAgentError = createAction(constants.GET_ALL_MANAGE_SALES_AGENT_ERROR);
export const getAllManageSalesAgentClear = createAction(constants.GET_ALL_MANAGE_SALES_AGENT_CLEAR);

//-------------------- GET ALL STORE ANALYSIS --------------------
export const getAllStoreAnalysisRequest = createAction(constants.GET_ALL_STORE_ANALYSIS_REQUEST);
export const getAllStoreAnalysisSuccess = createAction(constants.GET_ALL_STORE_ANALYSIS_SUCCESS);
export const getAllStoreAnalysisError = createAction(constants.GET_ALL_STORE_ANALYSIS_ERROR);
export const getAllStoreAnalysisClear = createAction(constants.GET_ALL_STORE_ANALYSIS_CLEAR);

//-------------------- CREATE STORE ANALYSIS --------------------
export const createStoreAnalysisRequest = createAction(constants.CREATE_STORE_ANALYSIS_REQUEST);
export const createStoreAnalysisSuccess = createAction(constants.CREATE_STORE_ANALYSIS_SUCCESS);
export const createStoreAnalysisError = createAction(constants.CREATE_STORE_ANALYSIS_ERROR);
export const createStoreAnalysisClear = createAction(constants.CREATE_STORE_ANALYSIS_CLEAR);

//-------------------- GET DATA STORE ANALYSIS --------------------
export const getDataStoreAnalysisRequest = createAction(constants.GET_DATA_STORE_ANALYSIS_REQUEST);
export const getDataStoreAnalysisSuccess = createAction(constants.GET_DATA_STORE_ANALYSIS_SUCCESS);
export const getDataStoreAnalysisError = createAction(constants.GET_DATA_STORE_ANALYSIS_ERROR);
export const getDataStoreAnalysisClear = createAction(constants.GET_DATA_STORE_ANALYSIS_CLEAR);
export const excelSearchRequest = createAction(constants.EXCEL_SEARCH_REQUEST);
export const excelSearchSuccess = createAction(constants.EXCEL_SEARCH_SUCCESS);
export const excelSearchClear = createAction(constants.EXCEL_SEARCH_CLEAR);
export const excelSearchError = createAction(constants.EXCEL_SEARCH_ERROR);

//-------------------- GET SUPPORT ACCESS --------------------
export const getSupportAccessRequest = createAction(constants.GET_SUPPORT_ACCESS_REQUEST);
export const getSupportAccessSuccess = createAction(constants.GET_SUPPORT_ACCESS_SUCCESS);
export const getSupportAccessError = createAction(constants.GET_SUPPORT_ACCESS_ERROR);
export const getSupportAccessClear = createAction(constants.GET_SUPPORT_ACCESS_CLEAR);

//-------------------- CREATE SUPPORT ACCESS --------------------
export const createSupportAccessRequest = createAction(constants.CREATE_SUPPORT_ACCESS_REQUEST);
export const createSupportAccessSuccess = createAction(constants.CREATE_SUPPORT_ACCESS_SUCCESS);
export const createSupportAccessError = createAction(constants.CREATE_SUPPORT_ACCESS_ERROR);
export const createSupportAccessClear = createAction(constants.CREATE_SUPPORT_ACCESS_CLEAR);

export const getAllLedgerReportRequest = createAction(constants.GET_ALL_LEDGER_REPORT_REQUEST);
export const getAllLedgerReportSuccess = createAction(constants.GET_ALL_LEDGER_REPORT_SUCCESS);
export const getAllLedgerReportClear = createAction(constants.GET_ALL_LEDGER_REPORT_CLEAR);
export const getAllLedgerReportError = createAction(constants.GET_ALL_LEDGER_REPORT_ERROR);

export const getAllSaleReportRequest = createAction(constants.GET_ALL_SALE_REPORT_REQUEST);
export const getAllSaleReportSuccess = createAction(constants.GET_ALL_SALE_REPORT_SUCCESS);
export const getAllSaleReportClear = createAction(constants.GET_ALL_SALE_REPORT_CLEAR);
export const getAllSaleReportError = createAction(constants.GET_ALL_SALE_REPORT_ERROR);

export const getAllStockReportRequest = createAction(constants.GET_ALL_STOCK_REPORT_REQUEST);
export const getAllStockReportSuccess = createAction(constants.GET_ALL_STOCK_REPORT_SUCCESS);
export const getAllStockReportClear = createAction(constants.GET_ALL_STOCK_REPORT_CLEAR);
export const getAllStockReportError = createAction(constants.GET_ALL_STOCK_REPORT_ERROR);

export const getAllOutStandingReportRequest = createAction(constants.GET_ALL_OUTSTANDING_REPORT_REQUEST);
export const getAllOutStandingReportSuccess = createAction(constants.GET_ALL_OUTSTANDING_REPORT_SUCCESS);
export const getAllOutStandingReportClear = createAction(constants.GET_ALL_OUTSTANDING_REPORT_CLEAR);
export const getAllOutStandingReportError = createAction(constants.GET_ALL_OUTSTANDING_REPORT_ERROR);

export const getLedgerFilterReportRequest = createAction(constants.GET_LEDGER_FILTER_REPORT_REQUEST);
export const getLedgerFilterReportSuccess = createAction(constants.GET_LEDGER_FILTER_REPORT_SUCCESS);
export const getLedgerFilterReportClear = createAction(constants.GET_LEDGER_FILTER_REPORT_CLEAR);
export const getLedgerFilterReportError = createAction(constants.GET_LEDGER_FILTER_REPORT_ERROR);

export const sendEmailRequest = createAction(constants.SEND_EMAIL_REQUEST);
export const sendEmailSuccess = createAction(constants.SEND_EMAIL_SUCCESS);
export const sendEmailError = createAction(constants.SEND_EMAIL_ERROR);
export const sendEmailClear = createAction(constants.SEND_EMAIL_CLEAR);

//------------------------ GET MRP RANGE ------------------------

export const getMRPHistoryRequest = createAction(constants.GET_MRP_HISTORY_REQUEST);
export const getMRPHistorySuccess = createAction(constants.GET_MRP_HISTORY_SUCCESS);
export const getMRPHistoryError = createAction(constants.GET_MRP_HISTORY_ERROR);
export const getMRPHistoryClear = createAction(constants.GET_MRP_HISTORY_CLEAR);

//-------------------- GET API ACCESS DETAILS --------------------
export const getApiAccessDetailsRequest = createAction(constants.GET_API_ACCESS_DETAILS_REQUEST);
export const getApiAccessDetailsSuccess = createAction(constants.GET_API_ACCESS_DETAILS_SUCCESS);
export const getApiAccessDetailsError = createAction(constants.GET_API_ACCESS_DETAILS_ERROR);
export const getApiAccessDetailsClear = createAction(constants.GET_API_ACCESS_DETAILS_CLEAR);

//-------------------- GET EMAIL NOTIFICATION LOG --------------------
export const getEmailNotificationLogRequest = createAction(constants.GET_EMAIL_NOTIFICATION_LOG_REQUEST);
export const getEmailNotificationLogSuccess = createAction(constants.GET_EMAIL_NOTIFICATION_LOG_SUCCESS);
export const getEmailNotificationLogError = createAction(constants.GET_EMAIL_NOTIFICATION_LOG_ERROR);
export const getEmailNotificationLogClear = createAction(constants.GET_EMAIL_NOTIFICATION_LOG_CLEAR);

//-------------------- RESEND EMAIL NOTIFICATION --------------------
export const resendEmailNotificationRequest = createAction(constants.RESEND_EMAIL_NOTIFICATION_REQUEST);
export const resendEmailNotificationSuccess = createAction(constants.RESEND_EMAIL_NOTIFICATION_SUCCESS);
export const resendEmailNotificationError = createAction(constants.RESEND_EMAIL_NOTIFICATION_ERROR);
export const resendEmailNotificationClear = createAction(constants.RESEND_EMAIL_NOTIFICATION_CLEAR);

//-------------------- GET EMAIL BODY --------------------
export const getEmailBodyRequest = createAction(constants.GET_EMAIL_BODY_REQUEST);
export const getEmailBodySuccess = createAction(constants.GET_EMAIL_BODY_SUCCESS);
export const getEmailBodyError = createAction(constants.GET_EMAIL_BODY_ERROR);
export const getEmailBodyClear = createAction(constants.GET_EMAIL_BODY_CLEAR);

export const downloadRepReportRequest = createAction(constants.DOWNLOAD_REP_REPORT_REQUEST);
export const downloadRepReportSuccess = createAction(constants.DOWNLOAD_REP_REPORT_SUCCESS);
export const downloadRepReportError = createAction(constants.DOWNLOAD_REP_REPORT_ERROR);
export const downloadRepReportClear = createAction(constants.DOWNLOAD_REP_REPORT_CLEAR);

export const handleValidateRequest = createAction(constants.HANDLE_VALIDATE_REQUEST);
export const handleValidateSuccess = createAction(constants.HANDLE_VALIDATE_SUCCESS);
export const handleValidateClear = createAction(constants.HANDLE_VALIDATE_CLEAR);
export const handleValidateError = createAction(constants.HANDLE_VALIDATE_ERROR);

export const excelHeadersRequest = createAction(constants.EXCEL_HEADERS_REQUEST);
export const excelHeadersSuccess = createAction(constants.EXCEL_HEADERS_SUCCESS);
export const excelHeadersClear = createAction(constants.EXCEL_HEADERS_CLEAR);
export const excelHeadersError = createAction(constants.EXCEL_HEADERS_ERROR);

export const excelSubmitRequest = createAction(constants.EXCEL_SUBMIT_REQUEST);
export const excelSubmitSuccess = createAction(constants.EXCEL_SUBMIT_SUCCESS);
export const excelSubmitClear = createAction(constants.EXCEL_SUBMIT_CLEAR);
export const excelSubmitError = createAction(constants.EXCEL_SUBMIT_ERROR);

export const downloadTemplateRequest = createAction(constants.DOWNLOAD_TEMPLATE_REQUEST);
export const downloadTemplateSuccess = createAction(constants.DOWNLOAD_TEMPLATE_SUCCESS);
export const downloadTemplateClear = createAction(constants.DOWNLOAD_TEMPLATE_CLEAR);
export const downloadTemplateError = createAction(constants.DOWNLOAD_TEMPLATE_ERROR);

export const saveKycDetailsRequest = createAction(constants.SAVE_KYC_DETAILS_REQUEST);
export const saveKycDetailsSuccess = createAction(constants.SAVE_KYC_DETAILS_SUCCESS);
export const saveKycDetailsError = createAction(constants.SAVE_KYC_DETAILS_ERROR);
export const saveKycDetailsClear = createAction(constants.SAVE_KYC_DETAILS_CLEAR);

export const getKycDetailsRequest = createAction(constants.GET_KYC_DETAILS_REQUEST);
export const getKycDetailsSuccess = createAction(constants.GET_KYC_DETAILS_SUCCESS);
export const getKycDetailsError = createAction(constants.GET_KYC_DETAILS_ERROR);
export const getKycDetailsClear = createAction(constants.GET_KYC_DETAILS_CLEAR);

export const getInvoiceManagementItemRequest = createAction(constants.GET_INVOICE_MANAGEMENT_ITEM_REQUEST);
export const getInvoiceManagementItemSuccess = createAction(constants.GET_INVOICE_MANAGEMENT_ITEM_SUCCESS);
export const getInvoiceManagementItemClear = createAction(constants.GET_INVOICE_MANAGEMENT_ITEM_CLEAR);
export const getInvoiceManagementItemError = createAction(constants.GET_INVOICE_MANAGEMENT_ITEM_ERROR);

export const saveInvoiceManagementItemRequest = createAction(constants.SAVE_INVOICE_MANAGEMENT_ITEM_REQUEST);
export const saveInvoiceManagementItemSuccess = createAction(constants.SAVE_INVOICE_MANAGEMENT_ITEM_SUCCESS);
export const saveInvoiceManagementItemClear = createAction(constants.SAVE_INVOICE_MANAGEMENT_ITEM_CLEAR);
export const saveInvoiceManagementItemError = createAction(constants.SAVE_INVOICE_MANAGEMENT_ITEM_ERROR);

export const deleteInvoiceManagementItemRequest = createAction(constants.DELETE_INVOICE_MANAGEMENT_ITEM_REQUEST);
export const deleteInvoiceManagementItemSuccess = createAction(constants.DELETE_INVOICE_MANAGEMENT_ITEM_SUCCESS);
export const deleteInvoiceManagementItemClear = createAction(constants.DELETE_INVOICE_MANAGEMENT_ITEM_CLEAR);
export const deleteInvoiceManagementItemError = createAction(constants.DELETE_INVOICE_MANAGEMENT_ITEM_ERROR);

export const updateInvoiceManagementItemRequest = createAction(constants.UPDATE_INVOICE_MANAGEMENT_ITEM_REQUEST);
export const updateInvoiceManagementItemSuccess = createAction(constants.UPDATE_INVOICE_MANAGEMENT_ITEM_SUCCESS);
export const updateInvoiceManagementItemClear = createAction(constants.UPDATE_INVOICE_MANAGEMENT_ITEM_CLEAR);
export const updateInvoiceManagementItemError = createAction(constants.UPDATE_INVOICE_MANAGEMENT_ITEM_ERROR);

export const getInvoiceManagementCustomerRequest = createAction(constants.GET_INVOICE_MANAGEMENT_CUSTOMER_REQUEST);
export const getInvoiceManagementCustomerSuccess = createAction(constants.GET_INVOICE_MANAGEMENT_CUSTOMER_SUCCESS);
export const getInvoiceManagementCustomerClear = createAction(constants.GET_INVOICE_MANAGEMENT_CUSTOMER_CLEAR);
export const getInvoiceManagementCustomerError = createAction(constants.GET_INVOICE_MANAGEMENT_CUSTOMER_ERROR);

export const saveInvoiceManagementCustomerRequest = createAction(constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST);
export const saveInvoiceManagementCustomerSuccess = createAction(constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_SUCCESS);
export const saveInvoiceManagementCustomerClear = createAction(constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_CLEAR);
export const saveInvoiceManagementCustomerError = createAction(constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_ERROR);

export const deleteInvoiceManagementCustomerRequest = createAction(constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST);
export const deleteInvoiceManagementCustomerSuccess = createAction(constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_SUCCESS);
export const deleteInvoiceManagementCustomerClear = createAction(constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_CLEAR);
export const deleteInvoiceManagementCustomerError = createAction(constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_ERROR);

export const updateInvoiceManagementCustomerRequest = createAction(constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST);
export const updateInvoiceManagementCustomerSuccess = createAction(constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_SUCCESS);
export const updateInvoiceManagementCustomerClear = createAction(constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_CLEAR);
export const updateInvoiceManagementCustomerError = createAction(constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_ERROR);

export const getInvoiceManagementGenericRequest = createAction(constants.GET_INVOICE_MANAGEMENT_GENERIC_REQUEST);
export const getInvoiceManagementGenericSuccess = createAction(constants.GET_INVOICE_MANAGEMENT_GENERIC_SUCCESS);
export const getInvoiceManagementGenericClear = createAction(constants.GET_INVOICE_MANAGEMENT_GENERIC_CLEAR);
export const getInvoiceManagementGenericError = createAction(constants.GET_INVOICE_MANAGEMENT_GENERIC_ERROR);

export const deleteInvoiceManagementGenericRequest = createAction(constants.DELETE_INVOICE_MANAGEMENT_GENERIC_REQUEST);
export const deleteInvoiceManagementGenericSuccess = createAction(constants.DELETE_INVOICE_MANAGEMENT_GENERIC_SUCCESS);
export const deleteInvoiceManagementGenericClear = createAction(constants.DELETE_INVOICE_MANAGEMENT_GENERIC_CLEAR);
export const deleteInvoiceManagementGenericError = createAction(constants.DELETE_INVOICE_MANAGEMENT_GENERIC_ERROR);

export const expandInvoiceManagementGenericRequest = createAction(constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_REQUEST);
export const expandInvoiceManagementGenericSuccess = createAction(constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_SUCCESS);
export const expandInvoiceManagementGenericClear = createAction(constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_CLEAR);
export const expandInvoiceManagementGenericError = createAction(constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_ERROR);

export const getUserDataMappingClear = createAction(constants.GET_USER_DATA_MAPPING_CLEAR);
export const getUserDataMappingRequest = createAction(constants.GET_USER_DATA_MAPPING_REQUEST);
export const getUserDataMappingSuccess = createAction(constants.GET_USER_DATA_MAPPING_SUCCESS);
export const getUserDataMappingError = createAction(constants.GET_USER_DATA_MAPPING_ERROR);

export const updateUserDataMappingClear = createAction(constants.UPDATE_USER_DATA_MAPPING_CLEAR);
export const updateUserDataMappingRequest = createAction(constants.UPDATE_USER_DATA_MAPPING_REQUEST);
export const updateUserDataMappingSuccess = createAction(constants.UPDATE_USER_DATA_MAPPING_SUCCESS);
export const updateUserDataMappingError = createAction(constants.UPDATE_USER_DATA_MAPPING_ERROR);

//------------------------ GET SALES CONTRIBUTION REPORT ------------------------

export const getSalesContributionReportRequest = createAction(constants.GET_SALES_CONTRIBUTION_REPORT_REQUEST);
export const getSalesContributionReportSuccess = createAction(constants.GET_SALES_CONTRIBUTION_REPORT_SUCCESS);
export const getSalesContributionReportError = createAction(constants.GET_SALES_CONTRIBUTION_REPORT_ERROR);
export const getSalesContributionReportClear = createAction(constants.GET_SALES_CONTRIBUTION_REPORT_CLEAR);

//------------------------ GET SALES COMPARISON REPORT ------------------------

export const getSalesComparisonReportRequest = createAction(constants.GET_SALES_COMPARISON_REPORT_REQUEST);
export const getSalesComparisonReportSuccess = createAction(constants.GET_SALES_COMPARISON_REPORT_SUCCESS);
export const getSalesComparisonReportError = createAction(constants.GET_SALES_COMPARISON_REPORT_ERROR);
export const getSalesComparisonReportClear = createAction(constants.GET_SALES_COMPARISON_REPORT_CLEAR);

//------------------------ GET SELL THRU PERFORMANCE REPORT ------------------------

export const getSellThruPerformanceReportRequest = createAction(constants.GET_SELL_THRU_PERFORMANCE_REPORT_REQUEST);
export const getSellThruPerformanceReportSuccess = createAction(constants.GET_SELL_THRU_PERFORMANCE_REPORT_SUCCESS);
export const getSellThruPerformanceReportError = createAction(constants.GET_SELL_THRU_PERFORMANCE_REPORT_ERROR);
export const getSellThruPerformanceReportClear = createAction(constants.GET_SELL_THRU_PERFORMANCE_REPORT_CLEAR);

//------------------------ GET TOP MOVING ITEMS REPORT ------------------------

export const getTopMovingItemsReportRequest = createAction(constants.GET_TOP_MOVING_ITEMS_REPORT_REQUEST);
export const getTopMovingItemsReportSuccess = createAction(constants.GET_TOP_MOVING_ITEMS_REPORT_SUCCESS);
export const getTopMovingItemsReportError = createAction(constants.GET_TOP_MOVING_ITEMS_REPORT_ERROR);
export const getTopMovingItemsReportClear = createAction(constants.GET_TOP_MOVING_ITEMS_REPORT_CLEAR);

//------------------------ GET CATEGORY SIZE WISE REPORT ------------------------

export const getCategorySizeWiseReportRequest = createAction(constants.GET_CATEGORY_SIZE_WISE_REPORT_REQUEST);
export const getCategorySizeWiseReportSuccess = createAction(constants.GET_CATEGORY_SIZE_WISE_REPORT_SUCCESS);
export const getCategorySizeWiseReportError = createAction(constants.GET_CATEGORY_SIZE_WISE_REPORT_ERROR);
export const getCategorySizeWiseReportClear = createAction(constants.GET_CATEGORY_SIZE_WISE_REPORT_CLEAR);

export const getInsertRequest = createAction(constants.GET_INSERT_REQUEST);
export const getInsertSuccess = createAction(constants.GET_INSERT_SUCCESS);
export const getInsertError = createAction(constants.GET_INSERT_ERROR);
export const getInsertClear = createAction(constants.GET_INSERT_CLEAR);

export const saveInsertRequest = createAction(constants.SAVE_INSERT_REQUEST);
export const saveInsertSuccess = createAction(constants.SAVE_INSERT_SUCCESS);
export const saveInsertError = createAction(constants.SAVE_INSERT_ERROR);
export const saveInsertClear = createAction(constants.SAVE_INSERT_CLEAR);

export const getAvailableKeysRequest = createAction(constants.GET_AVAILABLE_KEYS_REQUEST);
export const getAvailableKeysSuccess = createAction(constants.GET_AVAILABLE_KEYS_SUCCESS);
export const getAvailableKeysError = createAction(constants.GET_AVAILABLE_KEYS_ERROR);
export const getAvailableKeysClear = createAction(constants.GET_AVAILABLE_KEYS_CLEAR);

export const updateAvailableKeysRequest = createAction(constants.UPDATE_AVAILABLE_KEYS_REQUEST);
export const updateAvailableKeysSuccess = createAction(constants.UPDATE_AVAILABLE_KEYS_SUCCESS);
export const updateAvailableKeysError = createAction(constants.UPDATE_AVAILABLE_KEYS_ERROR);
export const updateAvailableKeysClear = createAction(constants.UPDATE_AVAILABLE_KEYS_CLEAR);

export const getVendorActivityRequest = createAction(constants.GET_VENDOR_ACTIVITY_REQUEST);
export const getVendorActivitySuccess = createAction(constants.GET_VENDOR_ACTIVITY_SUCCESS);
export const getVendorActivityError = createAction(constants.GET_VENDOR_ACTIVITY_ERROR);
export const getVendorActivityClear = createAction(constants.GET_VENDOR_ACTIVITY_CLEAR);

export const getVendorLogsRequest = createAction(constants.GET_VENDOR_LOGS_REQUEST);
export const getVendorLogsSuccess = createAction(constants.GET_VENDOR_LOGS_SUCCESS);
export const getVendorLogsError = createAction(constants.GET_VENDOR_LOGS_ERROR);
export const getVendorLogsClear = createAction(constants.GET_VENDOR_LOGS_CLEAR);
export const getCoreDropdownRequest = createAction(constants.GET_CORE_DROPDOWN_REQUEST);
export const getCoreDropdownSuccess = createAction(constants.GET_CORE_DROPDOWN_SUCCESS);
export const getCoreDropdownError = createAction(constants.GET_CORE_DROPDOWN_ERROR);
export const getCoreDropdownClear = createAction(constants.GET_CORE_DROPDOWN_CLEAR);

export const searchInvoiceDataRequest = createAction(constants.SEARCH_INVOICE_DATA_REQUEST);
export const searchInvoiceDataSuccess = createAction(constants.SEARCH_INVOICE_DATA_SUCCESS);
export const searchInvoiceDataError = createAction(constants.SEARCH_INVOICE_DATA_ERROR);
export const searchInvoiceDataClear = createAction(constants.SEARCH_INVOICE_DATA_CLEAR);

export const createInvoiceRequest = createAction(constants.CREATE_INVOICE_REQUEST);
export const createInvoiceSuccess = createAction(constants.CREATE_INVOICE_SUCCESS);
export const createInvoiceError = createAction(constants.CREATE_INVOICE_ERROR);
export const createInvoiceClear = createAction(constants.CREATE_INVOICE_CLEAR);
export const getAllLogTransPayReportRequest = createAction(constants.GET_ALL_LOG_TRANS_PAY_REPORT_REQUEST);
export const getAllLogTransPayReportSuccess = createAction(constants.GET_ALL_LOG_TRANS_PAY_REPORT_SUCCESS);
export const getAllLogTransPayReportError = createAction(constants.GET_ALL_LOG_TRANS_PAY_REPORT_ERROR);
export const getAllLogTransPayReportClear = createAction(constants.GET_ALL_LOG_TRANS_PAY_REPORT_CLEAR);

export const getAllLogWhStockReportRequest = createAction(constants.GET_ALL_LOG_WH_STOCK_REPORT_REQUEST);
export const getAllLogWhStockReportSuccess = createAction(constants.GET_ALL_LOG_WH_STOCK_REPORT_SUCCESS);
export const getAllLogWhStockReportError = createAction(constants.GET_ALL_LOG_WH_STOCK_REPORT_ERROR);
export const getAllLogWhStockReportClear = createAction(constants.GET_ALL_LOG_WH_STOCK_REPORT_CLEAR);
export const getPoStockReportRequest = createAction(constants.GET_PO_STOCK_REPORT_REQUEST);
export const getPoStockReportSuccess = createAction(constants.GET_PO_STOCK_REPORT_SUCCESS);
export const getPoStockReportError = createAction(constants.GET_PO_STOCK_REPORT_ERROR);
export const getPoStockReportClear = createAction(constants.GET_PO_STOCK_REPORT_CLEAR);

export const getPoColorMapRequest = createAction(constants.GET_PO_COLOR_MAP_REQUEST);
export const getPoColorMapSuccess = createAction(constants.GET_PO_COLOR_MAP_SUCCESS);
export const getPoColorMapError = createAction(constants.GET_PO_COLOR_MAP_ERROR);
export const getPoColorMapClear = createAction(constants.GET_PO_COLOR_MAP_CLEAR);

export const createPoArticleRequest = createAction(constants.CREATE_PO_ARTICLE_REQUEST);
export const createPoArticleSuccess = createAction(constants.CREATE_PO_ARTICLE_SUCCESS);
export const createPoArticleError = createAction(constants.CREATE_PO_ARTICLE_ERROR);
export const createPoArticleClear = createAction(constants.CREATE_PO_ARTICLE_CLEAR);
export const searchDataRequest = createAction(constants.SEARCH_DATA_REQUEST);
export const searchDataSuccess = createAction(constants.SEARCH_DATA_SUCCESS);
export const searchDataError = createAction(constants.SEARCH_DATA_ERROR);
export const searchDataClear = createAction(constants.SEARCH_DATA_CLEAR);
export const allRetailerCustomerRequest = createAction(constants.ALL_RETAILER_CUSTOMER_REQUEST);
export const allRetailerCustomerSuccess = createAction(constants.ALL_RETAILER_CUSTOMER_SUCCESS);
export const allRetailerCustomerError = createAction(constants.ALL_RETAILER_CUSTOMER_ERROR);
export const allRetailerCustomerClear = createAction(constants.ALL_RETAILER_CUSTOMER_CLEAR);

export const customerUserRequest = createAction(constants.CUSTOMER_USER_REQUEST);
export const customerUserSuccess = createAction(constants.CUSTOMER_USER_SUCCESS);
export const customerUserError = createAction(constants.CUSTOMER_USER_ERROR);
export const customerUserClear = createAction(constants.CUSTOMER_USER_CLEAR);

export const addUserCustomerClear = createAction(constants.ADD_USER_CUSTOMER_CLEAR);
export const addUserCustomerRequest = createAction(constants.ADD_USER_CUSTOMER_REQUEST);
export const addUserCustomerSuccess = createAction(constants.ADD_USER_CUSTOMER_SUCCESS);
export const addUserCustomerError = createAction(constants.ADD_USER_CUSTOMER_ERROR);

export const editUserCustomerClear = createAction(constants.EDIT_USER_CUSTOMER_CLEAR);
export const editUserCustomerRequest = createAction(constants.EDIT_USER_CUSTOMER_REQUEST);
export const editUserCustomerSuccess = createAction(constants.EDIT_USER_CUSTOMER_SUCCESS);
export const editUserCustomerError = createAction(constants.EDIT_USER_CUSTOMER_ERROR);

export const userStatusCustomerRequest = createAction(constants.USER_STATUS_CUSTOMER_REQUEST);
export const userStatusCustomerSuccess = createAction(constants.USER_STATUS_CUSTOMER_SUCCESS);
export const userStatusCustomerError = createAction(constants.USER_STATUS_CUSTOMER_ERROR);
export const userStatusCustomerClear = createAction(constants.USER_STATUS_CUSTOMER_CLEAR);

export const getSubscriptionCustomerRequest = createAction(constants.GET_SUBSCRIPTION_CUSTOMER_REQUEST);
export const getSubscriptionCustomerSuccess = createAction(constants.GET_SUBSCRIPTION_CUSTOMER_SUCCESS);
export const getSubscriptionCustomerError = createAction(constants.GET_SUBSCRIPTION_CUSTOMER_ERROR);
export const getSubscriptionCustomerClear = createAction(constants.GET_SUBSCRIPTION_CUSTOMER_CLEAR);

export const getTransactionCustomerRequest = createAction(constants.GET_TRANSACTION_CUSTOMER_REQUEST);
export const getTransactionCustomerSuccess = createAction(constants.GET_TRANSACTION_CUSTOMER_SUCCESS);
export const getTransactionCustomerClear = createAction(constants.GET_TRANSACTION_CUSTOMER_CLEAR);
export const getTransactionCustomerError = createAction(constants.GET_TRANSACTION_CUSTOMER_ERROR);
export const editCustomerRequest = createAction(constants.EDIT_CUSTOMER_REQUEST);
export const editCustomerClear = createAction(constants.EDIT_CUSTOMER_CLEAR);
export const editCustomerSuccess = createAction(constants.EDIT_CUSTOMER_SUCCESS);
export const editCustomerError = createAction(constants.EDIT_CUSTOMER_ERROR);
