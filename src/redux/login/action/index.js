import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import loginAjax from '../../../services/authServices';
import fireAjax from '../../../services/index';
import { AUTH_CONFIG } from '../../../authConfig/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import { parseJwt } from "../../../helper/index";
import { resolve } from 'path';



export function* loginRequest(action) {
  try {
    const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}${AUTH_CONFIG.LOGIN}`, {
      'uType': action.payload.uType,
      'username': action.payload.username,
      'password': action.payload.password,
      'fcmToken': localStorage.getItem('firebase-token')
    });
    const finalResponse = script(response);

    if (finalResponse.success) {

      sessionStorage.setItem('token', response.data.data.resource.token);
      sessionStorage.setItem('chatToken', response.data.data.resource.chatToken);
      sessionStorage.setItem('eid', response.data.data.resource.user.eid);
      sessionStorage.setItem('oid', response.data.data.resource.user.coreOrgID);
      sessionStorage.setItem('subscription', response.data.data.resource.subscription);
      
      if (response.data.data.resource.moduleList != null)
        sessionStorage.setItem('moduleList', response.data.data.resource.moduleList);
      
      if (response.data.data.resource.token != null)
        sessionStorage.setItem('prn', parseJwt(response.data.data.resource.token).prn)
      
        sessionStorage.setItem('partnerEnterpriseId', response.data.data.resource.user.eid);
      sessionStorage.setItem('partnerEnterpriseName', response.data.data.resource.user.ename);
      sessionStorage.setItem('firstName', response.data.data.resource.user.firstName);
      sessionStorage.setItem('lastName', response.data.data.resource.user.lastName);
      sessionStorage.setItem('roles', JSON.stringify(response.data.data.resource.roles));
      sessionStorage.setItem('userName', response.data.data.resource.user.username);
      sessionStorage.setItem('email', response.data.data.resource.user.email);
      sessionStorage.setItem('bucket', response.data.data.resource.user.bucket);

      sessionStorage.setItem('partnerEnterpriseCode', response.data.data.resource.user.ecode);
      sessionStorage.setItem('gstin', response.data.data.resource.user.gstin)
      // let org =[{orgId:"201",orgName:"tcloud",active:"TRUE"},{orgId:"221",orgName:"xyz",active:"FALSE"}]
      sessionStorage.setItem('orgId-name', JSON.stringify(response.data.data.resource.user.organisations))
      sessionStorage.setItem('fileUpload', false);
      sessionStorage.setItem('uType', response.data.data.resource.user.uType)

      sessionStorage.setItem('weeklyAssortmentCode', "");
      sessionStorage.setItem('monthlyAssortmentCode', "");
      sessionStorage.setItem('monthlyStart', "");
      sessionStorage.setItem('monthlyEnd', "");
      sessionStorage.setItem('weeklyStart', "");
      sessionStorage.setItem('weeklyEnd', "");
      sessionStorage.setItem('weeklyGraph', JSON.stringify([]));
      sessionStorage.setItem('monthlyGraph', JSON.stringify([]));
      sessionStorage.setItem('uType', response.data.data.resource.user.uType)

      sessionStorage.setItem('enterprises', JSON.stringify(response.data.data.resource.user.enterprises))
      sessionStorage.setItem('modules', JSON.stringify(response.data.data.resource.modules));
      if (response.data.data.resource.roles.length != 0) {
        sessionStorage.setItem('mid', JSON.stringify(response.data.data.resource.roles[0].id));
        let module = JSON.parse(sessionStorage.getItem('modules'));
        let mid = JSON.parse(sessionStorage.getItem('mid'));
        let data = module[mid]


        for (let i = 0; i < data.length; i++) {
          sessionStorage.setItem(`${data[i].name}`, JSON.stringify(data[i]));
        }
      }

      /*if (response.data.data.resource.token != null) {
        if (sessionStorage.getItem('firebase-token') != null) {
          registerFcmToken(sessionStorage.getItem('firebase-token'));
        }
      }*/

      yield put(actions.loginSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.loginError(response.data));
    }
  } catch (e) {
    yield put(actions.loginError("error occurs"));
    console.warn('Some error found in login action\n', e);
  }
}

export function* forgotPasswordRequest(action) {
  try {

    const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}${AUTH_CONFIG.FORGOT_PASSWORD}`, {
      'username': action.payload.username,
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.forgotPasswordSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.forgotPasswordError(response.data));
    }
  } catch (e) {
    yield put(actions.forgotPasswordError("error occurs"));
    console.warn('Some error found in forgotpassword action\n', e);
  }
}



export function* resetPasswordRequest(action) {
  try {

    const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}${AUTH_CONFIG.RESET_PASSWORD}`, {
      'token': action.payload.token,
      'password': action.payload.password,


    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.resetPasswordSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.resetPasswordError(response.data));
    }
  } catch (e) {
    yield put(actions.resetPasswordError("error occurs"));
    console.warn('Some error found in resetpassword action\n', e);
  }
}

export function* forgotUserRequest(action) {
  try {
    const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}${AUTH_CONFIG.FORGOT_USER}`, {
      'email': action.payload
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.forgotUserSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.forgotUserError(response.data));
    }
  } catch (e) {
    yield put(actions.forgotUserError("error occurs"));
    console.warn('Some error found in forgotUser action\n', e);
  }
}

export function* activateUserRequest(action) {
  try {
    const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}${AUTH_CONFIG.ACTIVATE_USER}`, {
      'token': action.payload.token,
      'password': action.payload.password
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.activateUserSuccess(response.data.data));

    } else if (finalResponse.failure) {
      yield put(actions.activateUserError(response.data));
    }
  } catch (e) {
    yield put(actions.activateUserError("error occurs"));
    console.warn('Some error found in activateUser action\n', e);
  }
}

export function* changePasswordRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.changePasswordClear());
  } else {
    try {
      const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}${AUTH_CONFIG.CHANGE_PASSWORD}`, {
        'token': sessionStorage.getItem('token'),
        'password': action.payload.password,
        'oldpassword': action.payload.oldPassword,
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.changePasswordSuccess(response.data.data));

      } else if (finalResponse.failure) {
        yield put(actions.changePasswordError(response.data));
      }
    } catch (e) {
      yield put(actions.changePasswordError("error occurs"));
      console.warn('Some error found in changepassword action\n', e);
    }
  }
}


export function* getUserNameRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.getUserNameClear());
  }
  else {
    try {
      let userName = action.payload
      let organization = JSON.parse(sessionStorage.getItem("orgId-name"))
      let orgId = ""
      for (let i = 0; i < organization.length; i++) {
        if (organization[i].active == "TRUE") {
          orgId = organization[i].orgId
        }
      }
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/user/get/userName/${userName}?orgId=${orgId}`, {
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.getUserNameSuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.getUserNameError(response.data));
      }
    } catch (e) {
      yield put(actions.getUserNameError("error occurs"));
      console.warn('Some error found in "getUserNameRequest" action\n', e);
    }
  }
}


export function* updateProfileRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.updateProfileClear());
  }
  else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/user/update/profile`, {
        'userName': action.payload.userName,
        'firstName': action.payload.firstName,
        'lastName': action.payload.lastName,
        'mobileNumber': action.payload.mobileNumber,
        'eid': action.payload.eid
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.updateProfileSuccess(response.data.data));
        yield put(actions.getUserNameRequest(action.payload.userName));
      } else if (finalResponse.failure) {
        yield put(actions.updateProfileError(response.data));
      }
    } catch (e) {
      yield put(actions.updateProfileError("error occurs"));
      console.warn('Some error found in updateProfile action\n', e);
    }
  }

}


export function* getProfileDetailsRequest(action) {
  try {
    let userName = action.payload
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/vendor/get/profile`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {

      yield put(actions.getProfileDetailsSuccess(response.data.data));
    } else if (finalResponse.failure) {

      yield put(actions.getProfileDetailsError(response.data));
    }
  } catch (e) {
    yield put(actions.getProfileDetailsError("error occurs"));
    console.warn('Some error found in "getProfileDetailsRequest" action\n', e);
  }
}

export function* updateUserDetailsRequest(action) {
  try {
    let userName = action.payload
    const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/core/vendor/edit/profile`, action.payload);
    const finalResponse = script(response);
    if (finalResponse.success) {

      yield put(actions.updateUserDetailsSuccess(response.data.data));
    } else if (finalResponse.failure) {

      yield put(actions.updateUserDetailsError(response.data));
    }
  } catch (e) {
    yield put(actions.updateUserDetailsError("error occurs"));
    console.warn('Some error found in "updateUserDetailsRequest" action\n', e);
  }
}

function registerFcmToken(token) {
  const url = `${CONFIG.BASE_URL}/vendorportal/comqc/register/topic`;
  const data = {
    "token": token
  };
  const param = {
    headers: {
      "content-type": "application/json; charset=UTF-8",
      "X-Auth-Token": sessionStorage.getItem('token')
    },
    body: JSON.stringify(data),
    method: "POST"
  };

  fetch(url, param)
    .then(data => { console.log(data); return data.json() })
    .then(res => { console.log(res) })
    .catch(error => console.log("Topic Registered Called..."))
}

export function* getSupportAccessRequest(action) {
  try {
    let userName = sessionStorage.getItem("userName");
    const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}/has/support/user`, {username: userName});
    const finalResponse = script(response);
    if (finalResponse.success) {
      
      yield put(actions.getSupportAccessSuccess(response.data.data));
    } else if (finalResponse.failure) {

      yield put(actions.getSupportAccessError(response.data));
    }
  } catch (e) {
    yield put(actions.getSupportAccessError("error occurs"));
    console.warn('Some error found in "getSupportAccessRequest" action\n', e);
  }
}

export function* createSupportAccessRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.createSupportAccessClear());
  }
  else {
    try {
      let userName = sessionStorage.getItem("userName");
      let email = sessionStorage.getItem("email");
      const response = yield call(loginAjax, 'POST', `${AUTH_CONFIG.BASE_URL}/create/support/user`, {
        username: userName,
        email: email,
        password: action.payload
      });
      const finalResponse = script(response);
      if (finalResponse.success) {

        yield put(actions.createSupportAccessSuccess(response.data.data));
      } else if (finalResponse.failure) {

        yield put(actions.createSupportAccessError(response.data));
      }
    } catch (e) {
      yield put(actions.createSupportAccessError("error occurs"));
      console.warn('Some error found in "createSupportAccessRequest" action\n', e);
    }
  }
}