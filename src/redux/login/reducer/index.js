import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';


let initialState = {
  login: {
    data: {},
    isLoggedIn: false,
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  forgotPassword: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  forgotUser: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  activateUser: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  resetPassword: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  changePassword: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getUserName: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateProfile: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getProfileDetails: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  updateUserDetails: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  getSupportAccess: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },
  createSupportAccess: {
    data: {},
    isLoading: false,
    isError: false,
    isSuccess: false,
    message: ''
  },


};

const loginRequest = (state, action) => update(state, {
  login: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const loginSuccess = (state, action) => update(state, {
  login: {
    data: { $set: action.payload },
    isLoggedIn: { $set: true },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Organisation success' }
  }
});
const loginError = (state, action) => update(state, {
  login: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

//forgot password

const forgotPasswordRequest = (state, action) => update(state, {
  forgotPassword: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const forgotPasswordSuccess = (state, action) => update(state, {
  forgotPassword: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'forgotPassword success' }
  }
});
const forgotPasswordError = (state, action) => update(state, {
  forgotPassword: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});


const resetPasswordRequest = (state, action) => update(state, {
  resetPassword: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const resetPasswordSuccess = (state, action) => update(state, {
  resetPassword: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'Organisation success' }
  }
});
const resetPasswordError = (state, action) => update(state, {
  resetPassword: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const forgotUserRequest = (state, action) => update(state, {
  forgotUser: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const forgotUserSuccess = (state, action) => update(state, {
  forgotUser: {
    data: { $set: action.payload },
    isLoggedIn: { $set: true },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'forgotUser success' }
  }
});
const forgotUserError = (state, action) => update(state, {
  forgotUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const activateUserRequest = (state, action) => update(state, {
  activateUser: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});
const activateUserSuccess = (state, action) => update(state, {
  activateUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'activate success' }
  }
});
const activateUserError = (state, action) => update(state, {
  activateUser: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const changePasswordRequest = (state, action) => update(state, {
  changePassword: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const changePasswordClear = (state, action) => update(state, {
  changePassword: {
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const changePasswordSuccess = (state, action) => update(state, {
  changePassword: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'changePassword success' }
  }
});
const changePasswordError = (state, action) => update(state, {
  changePassword: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});

const getUserNameRequest = (state, action) => update(state, {
  getUserName: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getUserNameSuccess = (state, action) => update(state, {
  getUserName: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: 'getUserName success' }
  }
});

const getUserNameError = (state, action) => update(state, {
  getUserName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getUserNameClear = (state, action) => update(state, {
  getUserName: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});


const updateProfileRequest = (state, action) => update(state, {
  updateProfile: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateProfileSuccess = (state, action) => update(state, {
  updateProfile: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const updateProfileError = (state, action) => update(state, {
  updateProfile: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const updateProfileClear = (state, action) => update(state, {
  updateProfile: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// get profile details

const getProfileDetailsRequest = (state, action) => update(state, {
  getProfileDetails: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getProfileDetailsSuccess = (state, action) => update(state, {
  getProfileDetails: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getProfileDetailsError = (state, action) => update(state, {
  getProfileDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getProfileDetailsClear = (state, action) => update(state, {
  getProfileDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

// update profile details

const updateUserDetailsRequest = (state, action) => update(state, {
  updateUserDetails: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const updateUserDetailsSuccess = (state, action) => update(state, {
  updateUserDetails: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const updateUserDetailsError = (state, action) => update(state, {
  updateUserDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const updateUserDetailsClear = (state, action) => update(state, {
  updateUserDetails: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const getSupportAccessRequest = (state, action) => update(state, {
  getSupportAccess: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const getSupportAccessSuccess = (state, action) => update(state, {
  getSupportAccess: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const getSupportAccessError = (state, action) => update(state, {
  getSupportAccess: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const getSupportAccessClear = (state, action) => update(state, {
  getSupportAccess: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

const createSupportAccessRequest = (state, action) => update(state, {
  createSupportAccess: {
    isLoading: { $set: true },
    isError: { $set: false },
    isSuccess: { $set: false },
    message: { $set: '' }
  }
});

const createSupportAccessSuccess = (state, action) => update(state, {
  createSupportAccess: {
    data: { $set: action.payload },
    isLoading: { $set: false },
    isError: { $set: false },
    isSuccess: { $set: true },
    message: { $set: action.payload }
  }
});

const createSupportAccessError = (state, action) => update(state, {
  createSupportAccess: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: true },
    message: { $set: action.payload }
  }
});
const createSupportAccessClear = (state, action) => update(state, {
  createSupportAccess: {
    isLoading: { $set: false },
    isSuccess: { $set: false },
    isError: { $set: false },
    message: { $set: '' }
  }
});

export default handleActions({
  [constants.LOGIN_REQUEST]: loginRequest,
  [constants.LOGIN_SUCCESS]: loginSuccess,
  [constants.LOGIN_ERROR]: loginError,
  [constants.FORGOT_PASSWORD_REQUEST]: forgotPasswordRequest,
  [constants.FORGOT_PASSWORD_SUCCESS]: forgotPasswordSuccess,
  [constants.FORGOT_PASSWORD_ERROR]: forgotPasswordError,
  [constants.RESET_PASSWORD_REQUEST]: resetPasswordRequest,
  [constants.RESET_PASSWORD_SUCCESS]: resetPasswordSuccess,
  [constants.RESET_PASSWORD_ERROR]: resetPasswordError,
  [constants.FORGOT_USER_REQUEST]: forgotUserRequest,
  [constants.FORGOT_USER_SUCCESS]: forgotUserSuccess,
  [constants.FORGOT_USER_ERROR]: forgotUserError,
  [constants.FORGOT_PASSWORD_ERROR]: forgotPasswordError,
  [constants.ACTIVATE_USER_REQUEST]: activateUserRequest,
  [constants.ACTIVATE_USER_SUCCESS]: activateUserSuccess,
  [constants.ACTIVATE_USER_ERROR]: activateUserError,
  [constants.CHANGE_PASSWORD_REQUEST]: changePasswordRequest,
  [constants.CHANGE_PASSWORD_SUCCESS]: changePasswordSuccess,
  [constants.CHANGE_PASSWORD_ERROR]: changePasswordError,
  [constants.CHANGE_PASSWORD_CLEAR]: changePasswordClear,
  [constants.GET_USERNAME_REQUEST]: getUserNameRequest,
  [constants.GET_USERNAME_SUCCESS]: getUserNameSuccess,
  [constants.GET_USERNAME_ERROR]: getUserNameError,
  [constants.GET_USERNAME_CLEAR]: getUserNameClear,
  [constants.UPDATE_PROFILE_REQUEST]: updateProfileRequest,
  [constants.UPDATE_PROFILE_SUCCESS]: updateProfileSuccess,
  [constants.UPDATE_PROFILE_ERROR]: updateProfileError,
  [constants.UPDATE_PROFILE_CLEAR]: updateProfileClear,
  [constants.GET_PROFILE_DETAILS_REQUEST]: getProfileDetailsRequest,
  [constants.GET_PROFILE_DETAILS_SUCCESS]: getProfileDetailsSuccess,
  [constants.GET_PROFILE_DETAILS_ERROR]: getProfileDetailsError,
  [constants.GET_PROFILE_DETAILS_CLEAR]: getProfileDetailsClear,
  [constants.UPDATE_USER_DETAILS_REQUEST]: updateUserDetailsRequest,
  [constants.UPDATE_USER_DETAILS_SUCCESS]: updateUserDetailsSuccess,
  [constants.UPDATE_USER_DETAILS_ERROR]: updateUserDetailsError,
  [constants.UPDATE_USER_DETAILS_CLEAR]: updateUserDetailsClear,

  [constants.GET_SUPPORT_ACCESS_REQUEST]: getSupportAccessRequest,
  [constants.GET_SUPPORT_ACCESS_SUCCESS]: getSupportAccessSuccess,
  [constants.GET_SUPPORT_ACCESS_ERROR]: getSupportAccessError,
  [constants.GET_SUPPORT_ACCESS_CLEAR]: getSupportAccessClear,

  [constants.CREATE_SUPPORT_ACCESS_REQUEST]: createSupportAccessRequest,
  [constants.CREATE_SUPPORT_ACCESS_SUCCESS]: createSupportAccessSuccess,
  [constants.CREATE_SUPPORT_ACCESS_ERROR]: createSupportAccessError,
  [constants.CREATE_SUPPORT_ACCESS_CLEAR]: createSupportAccessClear,


}, initialState);
