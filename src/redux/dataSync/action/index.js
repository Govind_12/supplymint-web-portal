import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import { OganisationIdName } from '../../../organisationIdName';

export function* createDataSyncTemplateRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.createDataSyncTemplateClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.DATASYNC}/create/template`, {
        "templateName": action.payload.templateName,
        "orgId": orgIdGlobal,
        "headers": action.payload.headers
      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.createDataSyncTemplateSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.createDataSyncTemplateError(response.data));
      }
    } catch (e) {
      yield put(actions.createDataSyncTemplateError("error occurs"));
      console.warn('Some error found in createDataSyncTemplate action\n', e);

    }
  }
}

// ______________DOWNLAOD TEMPLATE____________________________

export function* downlaodTemplateRequest(action) {
  if (action.payload == undefined) {
    yield put(actions.downlaodTemplateClear());
  } else {
    try {
      const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.DATASYNC}/create/template`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.downlaodTemplateSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.downlaodTemplateError(response.data));
      }
    } catch (e) {
      yield put(actions.downlaodTemplateError("error occurs"));
      console.warn('Some error found in downlaodTemplate action\n', e);

    }
  }
}

// +++++++++++++++++++++++++++++____UPLOADTEMPATE GET ALL_____+++++++++++++++++++++++++++++++++++++++++

export function* uploadTemplateRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.uploadTemplateClear());
  } else {
    let tempName = action.payload.tempName
    let isSelected = action.payload.isSelected
    let orgId = orgIdGlobal
    try {

      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DATASYNC}/getall/template?orgId=${orgId}&tempName=${tempName}&isSelected=${isSelected}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.uploadTemplateSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.uploadTemplateError(response.data));
      }
    } catch (e) {
      yield put(actions.uploadTemplateError("error occurs"));
      console.warn('Some error found in uploadTemplate action\n', e);

    }
  }
}


// ++++++++++++++++++++++++++_________________GET ALL TABLE______________+++++++++++++++++++++++++++++

export function* getAllDataSyncRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  if (action.payload == undefined) {
    yield put(actions.getAllDataSyncClear());
  } else {
    try {
      let orgId = orgIdGlobal;
      let no = action.payload.no == undefined ? 1 : action.payload.no;
      let type = action.payload.type;
      let search = action.payload.search == undefined ? "" : action.payload.search;
      let fileName = action.payload.fileName == undefined ? "" : action.payload.fileName;
      let templateName = action.payload.templateName == undefined ? "" : action.payload.templateName;
      let status = action.payload.status == undefined ? "" : action.payload.status;
      let bucketPath = action.payload.bucketPath == undefined ? "" : action.payload.bucketPath;
      let eventStart = action.payload.eventStart == undefined ? "" : action.payload.eventStart;
      let eventEnd = action.payload.eventEnd == undefined ? "" : action.payload.eventEnd;
      let dataCount = action.payload.dataCount == undefined ? "" : action.payload.dataCount;
      const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.DATASYNC}/get/all/custom?pageno=${no}&type=${type}&search=${search}&fileName=${fileName}&templateName=${templateName}&bucketPath=${bucketPath}&eventStart=${eventStart}&eventEnd=${eventEnd}&dataCount=${dataCount}&status=${status}&orgId=${orgId}`, {

      });
      const finalResponse = script(response);
      if (finalResponse.success) {
        yield put(actions.getAllDataSyncSuccess(response.data.data));
      } else if (finalResponse.failure) {
        yield put(actions.getAllDataSyncError(response.data));
      }
    } catch (e) {
      yield put(actions.getAllDataSyncError("error occurs"));
      console.warn('Some error found in getAllDataSync action\n', e);

    }
  }
}

// _________________________ERP GET ALL______________________________________

export function* dataSyncErpRequest(action) {
  var { orgIdGlobal } = OganisationIdName()
  try {
    let orgId = orgIdGlobal;
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type;
    let search = action.payload.search == undefined ? "" : action.payload.search;
    let name = action.payload.name == undefined ? "" : action.payload.name;
    let status = action.payload.status == undefined ? "" : action.payload.status;
    let key = action.payload.key == undefined ? "" : action.payload.key;
    let eventStart = action.payload.eventStart == undefined ? "" : action.payload.eventStart;
    let eventEnd = action.payload.eventEnd == undefined ? "" : action.payload.eventEnd;
    let dataCount = action.payload.dataCount == undefined ? "" : action.payload.dataCount;
    let channel = action.payload.channel == undefined ? "" : action.payload.channel
    const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/admin/custom/get/all/reference?pageno=${no}&type=${type}&search=${search}&name=${name}&key=${key}&eventStart=${eventStart}&eventEnd=${eventEnd}&dataCount=${dataCount}&status=${status}&channel=${channel}&orgId=${orgId}`, {
    });
    const finalResponse = script(response);
    if (finalResponse.success) {
      yield put(actions.dataSyncErpSuccess(response.data.data));
    } else if (finalResponse.failure) {
      yield put(actions.dataSyncErpError(response.data));
    }

  } catch (e) {
    yield put(actions.dataSyncErpError("error occurs"));
    console.warn('Some error found in "dataSyncErpRequest" action\n', e);
  }
}