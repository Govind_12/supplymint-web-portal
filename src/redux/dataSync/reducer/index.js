import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
    createDataSyncTemplate: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    downlaodTemplate: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    uploadTemplate: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getAllDataSync: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    dataSyncErp:{
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    }
}

const createDataSyncTemplateRequest = (state, action) => update(state, {
    createDataSyncTemplate: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const createDataSyncTemplateSuccess = (state, action) => update(state, {
    createDataSyncTemplate: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'createDataSyncTemplate success' }
    }
})

const createDataSyncTemplateError = (state, action) => update(state, {
    createDataSyncTemplate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const createDataSyncTemplateClear = (state, action) => update(state, {
    createDataSyncTemplate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
})
// _______________________DOWNLOAD TEMPLATE_________________________

const downlaodTemplateRequest = (state, action) => update(state, {
    downlaodTemplate: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const downlaodTemplateSuccess = (state, action) => update(state, {
    downlaodTemplate: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'downlaodTemplate success' }
    }
})

const downlaodTemplateError = (state, action) => update(state, {
    downlaodTemplate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const downlaodTemplateClear = (state, action) => update(state, {
    downlaodTemplate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
})

// __________________________UPLOAD TEMPLATE____________________________

const uploadTemplateRequest = (state, action) => update(state, {
    uploadTemplate: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const uploadTemplateSuccess = (state, action) => update(state, {
    uploadTemplate: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'uploadTemplate success' }
    }
})

const uploadTemplateError = (state, action) => update(state, {
    uploadTemplate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const uploadTemplateClear = (state, action) => update(state, {
    uploadTemplate: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
})

// ______________________________GET ALL DATA_SYNC______________________________

const getAllDataSyncRequest = (state, action) => update(state, {
    getAllDataSync: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const getAllDataSyncSuccess = (state, action) => update(state, {
    getAllDataSync: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'getAllDataSync success' }
    }
})

const getAllDataSyncError = (state, action) => update(state, {
    getAllDataSync: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})

const getAllDataSyncClear = (state, action) => update(state, {
    getAllDataSync: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
})

// _____________________________ERPGET ALL____________________________


const dataSyncErpRequest = (state, action) => update(state, {
    dataSyncErp: {
      isLoading: { $set: true },
      isError: { $set: false },
      isSuccess: { $set: false },
      message: { $set: '' }
    }
  });
  
  const dataSyncErpSuccess = (state, action) => update(state, {
    dataSyncErp: {
      data: { $set: action.payload },
      isLoading: { $set: false },
      isError: { $set: false },
      isSuccess: { $set: true },
      message: { $set: 'dataSyncErp success' }
    }
  });
  
  const dataSyncErpError = (state, action) => update(state, {
    dataSyncErp: {
      isLoading: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      message: { $set: action.payload }
    }
  });
  const dataSyncErpClear = (state, action) => update(state, {
    dataSyncErp: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
})


export default handleActions({

    [constants.CREATE_DATA_SYNC_TEMPLATE_REQUEST]: createDataSyncTemplateRequest,
    [constants.CREATE_DATA_SYNC_TEMPLATE_SUCCESS]: createDataSyncTemplateSuccess,
    [constants.CREATE_DATA_SYNC_TEMPLATE_ERROR]: createDataSyncTemplateError,
    [constants.CREATE_DATA_SYNC_TEMPLATE_CLEAR]: createDataSyncTemplateClear,

    // ____________________---DOWNLAOD TEMPLATE________________________

    [constants.DOWNLOAD_TEMPLATE_REQUEST]: downlaodTemplateRequest,
    [constants.DOWNLOAD_TEMPLATE_SUCCESS]: downlaodTemplateSuccess,
    [constants.DOWNLOAD_TEMPLATE_ERROR]: downlaodTemplateError,
    [constants.DOWNLOAD_TEMPLATE_CLEAR]: downlaodTemplateClear,

    // _______________________________UPLAOD TEMPLATE________________________

    [constants.UPLOAD_TEMPLATE_REQUEST]: uploadTemplateRequest,
    [constants.UPLOAD_TEMPLATE_SUCCESS]: uploadTemplateSuccess,
    [constants.UPLOAD_TEMPLATE_ERROR]: uploadTemplateError,
    [constants.UPLOAD_TEMPLATE_CLEAR]: uploadTemplateClear,

    // ____________________________GET ALL ______________________________________

    [constants.GET_ALL_DATA_SYNC_REQUEST]: getAllDataSyncRequest,
    [constants.GET_ALL_DATA_SYNC_SUCCESS]: getAllDataSyncSuccess,
    [constants.GET_ALL_DATA_SYNC_ERROR]: getAllDataSyncError,
    [constants.GET_ALL_DATA_SYNC_CLEAR]: getAllDataSyncClear,

    // ____________________________ERP DATA GET ALL________________________

    [constants.DATA_SYNC_ERP_REQUEST]: dataSyncErpRequest,
    [constants.DATA_SYNC_ERP_SUCCESS]: dataSyncErpSuccess,
    [constants.DATA_SYNC_ERP_ERROR]: dataSyncErpError,
    [constants.DATA_SYNC_ERP_CLEAR]: dataSyncErpClear,




}, initialState)