import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';


let initialState = {
    hl1: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    hl2: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    hl3: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    hl4: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    assetCode: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    createAssortment: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    getAssortment: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    viewMessage: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAssortmentStatus: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAdHock: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

    getSiteAdHock: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    createAdHock:
    {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getItemAdHock: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    editAdHoc: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    deleteItemCode: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getActivity: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    cancelAdhoc: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAssortmentDropdown: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAssortmentPlanningFilters: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAssortmentPlanningDetail: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    getAssortmentPlanningCount: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },
    saveAssortmentPlanningDetail: {
        data: {},
        isLoading: false,
        isError: false,
        isSuccess: false,
        message: ''
    },

};

const hl1Request = (state, action) => update(state, {
    hl1: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const hl1Success = (state, action) => update(state, {
    hl1: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'hl1 success' }
    }
});
const hl1Error = (state, action) => update(state, {
    hl1: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const hl2Request = (state, action) => update(state, {
    hl2: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const hl2Clear = (state, action) => update(state, {
    hl2: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const hl2Success = (state, action) => update(state, {
    hl2: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'hl2 success' }
    }
});
const hl2Error = (state, action) => update(state, {
    hl2: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const hl3Request = (state, action) => update(state, {
    hl3: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const hl3Clear = (state, action) => update(state, {
    hl3: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const hl3Success = (state, action) => update(state, {
    hl3: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'hl3 success' }
    }
});
const hl3Error = (state, action) => update(state, {
    hl3: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const hl4Request = (state, action) => update(state, {
    hl4: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const hl4Clear = (state, action) => update(state, {
    hl4: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const hl4Success = (state, action) => update(state, {
    hl4: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'hl4 success' }
    }
});
const hl4Error = (state, action) => update(state, {
    hl4: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const assetCodeRequest = (state, action) => update(state, {
    assetCode: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const assetCodeClear = (state, action) => update(state, {
    assetCode: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const assetCodeSuccess = (state, action) => update(state, {
    assetCode: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'assetCode success' }
    }
});
const assetCodeError = (state, action) => update(state, {
    assetCode: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});


const createAssortmentRequest = (state, action) => update(state, {
    createAssortment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const createAssortmentClear = (state, action) => update(state, {
    createAssortment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const createAssortmentSuccess = (state, action) => update(state, {
    createAssortment: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'createAssortment success' }
    }
});
const createAssortmentError = (state, action) => update(state, {
    createAssortment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getAssortmentRequest = (state, action) => update(state, {
    getAssortment: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentClear = (state, action) => update(state, {
    getAssortment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentSuccess = (state, action) => update(state, {
    getAssortment: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getAssortment success' }
    }
});
const getAssortmentError = (state, action) => update(state, {
    getAssortment: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const viewMessageRequest = (state, action) => update(state, {
    viewMessage: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const viewMessageClear = (state, action) => update(state, {
    viewMessage: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const viewMessageSuccess = (state, action) => update(state, {
    viewMessage: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'viewMessage success' }
    }
});
const viewMessageError = (state, action) => update(state, {
    viewMessage: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getAssortmentStatusRequest = (state, action) => update(state, {
    getAssortmentStatus: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentStatusClear = (state, action) => update(state, {
    getAssortmentStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentStatusSuccess = (state, action) => update(state, {
    getAssortmentStatus: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getAssortmentStatus success' }
    }
});
const getAssortmentStatusError = (state, action) => update(state, {
    getAssortmentStatus: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getAdHockRequest = (state, action) => update(state, {
    getAdHock: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAdHockClear = (state, action) => update(state, {
    getAdHock: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getAdHockSuccess = (state, action) => update(state, {
    getAdHock: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getAdHock success' }
    }
});
const getAdHockError = (state, action) => update(state, {
    getAdHock: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSiteAdHockRequest = (state, action) => update(state, {
    getSiteAdHock: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getSiteAdHockSuccess = (state, action) => update(state, {
    getSiteAdHock: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getSiteAdHock success' }
    }
});
const getSiteAdHockError = (state, action) => update(state, {
    getSiteAdHock: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});

const getSiteAdHockClear = (state, action) => update(state, {
    getSiteAdHock: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: "" }
    }
});

const getItemAdHockRequest = (state, action) => update(state, {
    getItemAdHock: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getItemAdHockSuccess = (state, action) => update(state, {
    getItemAdHock: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getItemAdHock success' }
    }
});
const getItemAdHockError = (state, action) => update(state, {
    getItemAdHock: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});


const createAdhocRequest = (state, action) => update(state, {
    createAdHock: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const createAdhocSuccess = (state, action) => update(state, {
    createAdHock: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'createAdHock success' }
    }
});
const createAdhocError = (state, action) => update(state, {
    createAdHock: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const createAdhocClear = (state, action) => update(state, {
    createAdHock: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const editAdhocRequest = (state, action) => update(state, {
    editAdHoc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const editAdhocSuccess = (state, action) => update(state, {
    editAdHoc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'createAdHock success' }
    }
});
const editAdhocError = (state, action) => update(state, {
    editAdHoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const editAdhocClear = (state, action) => update(state, {
    editAdHoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const deleteItemCodeRequest = (state, action) => update(state, {
    deleteItemCode: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const deleteItemCodeSuccess = (state, action) => update(state, {
    deleteItemCode: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'deleteItemCode success' }
    }
});
const deleteItemCodeError = (state, action) => update(state, {
    deleteItemCode: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const deleteItemCodeClear = (state, action) => update(state, {
    deleteItemCode: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getActivityRequest = (state, action) => update(state, {
    getActivity: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getActivitySuccess = (state, action) => update(state, {
    getActivity: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: 'getActivity success' }
    }
});
const getActivityError = (state, action) => update(state, {
    getActivity: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getActivityClear = (state, action) => update(state, {
    getActivity: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const cancelAdhocRequest = (state, action) => update(state, {
    cancelAdhoc: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const cancelAdhocSuccess = (state, action) => update(state, {
    cancelAdhoc: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: action.payload }
    }
});
const cancelAdhocError = (state, action) => update(state, {
    cancelAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const cancelAdhocClear = (state, action) => update(state, {
    cancelAdhoc: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});


const getAssortmentDropdownRequest = (state, action) => update(state, {
    getAssortmentDropdown: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentDropdownSuccess = (state, action) => update(state, {
    getAssortmentDropdown: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: action.payload }
    }
});
const getAssortmentDropdownError = (state, action) => update(state, {
    getAssortmentDropdown: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getAssortmentDropdownClear = (state, action) => update(state, {
    getAssortmentDropdown: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

// Assortment Planning:::
const getAssortmentPlanningFiltersRequest = (state, action) => update(state, {
    getAssortmentPlanningFilters: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentPlanningFiltersSuccess = (state, action) => update(state, {
    getAssortmentPlanningFilters: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: action.payload }
    }
});
const getAssortmentPlanningFiltersError = (state, action) => update(state, {
    getAssortmentPlanningFilters: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getAssortmentPlanningFiltersClear = (state, action) => update(state, {
    getAssortmentPlanningFilters: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentPlannigCountRequest = (state, action) => update(state, {
    getAssortmentPlanningCount: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentPlannigCountSuccess = (state, action) => update(state, {
    getAssortmentPlanningCount: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: action.payload }
    }
});
const getAssortmentPlannigCountError = (state, action) => update(state, {
    getAssortmentPlanningCount: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getAssortmentPlannigCountClear = (state, action) => update(state, {
    getAssortmentPlanningCount: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentPlanningDetailRequest = (state, action) => update(state, {
    getAssortmentPlanningDetail: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const getAssortmentPlanningDetailSuccess = (state, action) => update(state, {
    getAssortmentPlanningDetail: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: action.payload }
    }
});
const getAssortmentPlanningDetailError = (state, action) => update(state, {
    getAssortmentPlanningDetail: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const getAssortmentPlanningDetailClear = (state, action) => update(state, {
    getAssortmentPlanningDetail: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});

const saveAssortmentPlannigDetailRequest = (state, action) => update(state, {
    saveAssortmentPlanningDetail: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const saveAssortmentPlannigDetailSuccess = (state, action) => update(state, {
    saveAssortmentPlanningDetail: {
        data: { $set: action.payload },
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: true },
        message: { $set: action.payload }
    }
});
const saveAssortmentPlannigDetailError = (state, action) => update(state, {
    saveAssortmentPlanningDetail: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
});
const saveAssortmentPlannigDetailClear = (state, action) => update(state, {
    saveAssortmentPlanningDetail: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: '' }
    }
});







export default handleActions({
    [constants.HL1_REQUEST]: hl1Request,
    [constants.HL1_SUCCESS]: hl1Success,
    [constants.HL1_ERROR]: hl1Error,

    [constants.HL2_REQUEST]: hl2Request,
    [constants.HL2_CLEAR]: hl2Clear,
    [constants.HL2_SUCCESS]: hl2Success,
    [constants.HL2_ERROR]: hl2Error,

    [constants.HL3_REQUEST]: hl3Request,
    [constants.HL3_CLEAR]: hl3Clear,
    [constants.HL3_SUCCESS]: hl3Success,
    [constants.HL3_ERROR]: hl3Error,

    [constants.HL4_REQUEST]: hl4Request,
    [constants.HL4_CLEAR]: hl4Clear,
    [constants.HL4_SUCCESS]: hl4Success,
    [constants.HL4_ERROR]: hl4Error,

    [constants.ASSET_CODE_REQUEST]: assetCodeRequest,
    [constants.ASSET_CODE_CLEAR]: assetCodeClear,
    [constants.ASSET_CODE_SUCCESS]: assetCodeSuccess,
    [constants.ASSET_CODE_ERROR]: assetCodeError,

    [constants.CREATE_ASSORTMENT_REQUEST]: createAssortmentRequest,
    [constants.CREATE_ASSORTMENT_CLEAR]: createAssortmentClear,
    [constants.CREATE_ASSORTMENT_SUCCESS]: createAssortmentSuccess,
    [constants.CREATE_ASSORTMENT_ERROR]: createAssortmentError,

    [constants.GET_ASSORTMENT_REQUEST]: getAssortmentRequest,
    [constants.GET_ASSORTMENT_CLEAR]: getAssortmentClear,
    [constants.GET_ASSORTMENT_SUCCESS]: getAssortmentSuccess,
    [constants.GET_ASSORTMENT_ERROR]: getAssortmentError,

    [constants.VIEW_MESSAGE_REQUEST]: viewMessageRequest,
    [constants.VIEW_MESSAGE_CLEAR]: viewMessageClear,
    [constants.VIEW_MESSAGE_SUCCESS]: viewMessageSuccess,
    [constants.VIEW_MESSAGE_ERROR]: viewMessageError,

    [constants.GET_ASSORTMENT_STATUS_REQUEST]: getAssortmentStatusRequest,
    [constants.GET_ASSORTMENT_STATUS_CLEAR]: getAssortmentStatusClear,
    [constants.GET_ASSORTMENT_STATUS_SUCCESS]: getAssortmentStatusSuccess,
    [constants.GET_ASSORTMENT_STATUS_ERROR]: getAssortmentStatusError,

    [constants.GET_AD_HOCK_REQUEST]: getAdHockRequest,
    [constants.GET_AD_HOCK_CLEAR]: getAdHockClear,
    [constants.GET_AD_HOCK_SUCCESS]: getAdHockSuccess,
    [constants.GET_AD_HOCK_ERROR]: getAdHockError,

    [constants.GET_SITE_AD_HOCK_REQUEST]: getSiteAdHockRequest,
    [constants.GET_SITE_AD_HOCK_SUCCESS]: getSiteAdHockSuccess,
    [constants.GET_SITE_AD_HOCK_ERROR]: getSiteAdHockError,
    [constants.GET_SITE_AD_HOCK_CLEAR]: getSiteAdHockClear,

    [constants.GET_ITEM_AD_HOCK_REQUEST]: getItemAdHockRequest,
    [constants.GET_ITEM_AD_HOCK_SUCCESS]: getItemAdHockSuccess,
    [constants.GET_ITEM_AD_HOCK_ERROR]: getItemAdHockError,


    [constants.CREATE_ADHOCK_REQUEST]: createAdhocRequest,
    [constants.CREATE_ADHOCK_SUCCESS]: createAdhocSuccess,
    [constants.CREATE_ADHOCK_ERROR]: createAdhocError,
    [constants.CREATE_ADHOCK_CLEAR]: createAdhocClear,

    [constants.EDIT_ADHOCK_REQUEST]: editAdhocRequest,
    [constants.EDIT_ADHOCK_SUCCESS]: editAdhocSuccess,
    [constants.EDIT_ADHOCK_ERROR]: editAdhocError,
    [constants.EDIT_ADHOCK_CLEAR]: editAdhocClear,

    [constants.DELETE_ITEM_CODE_REQUEST]: deleteItemCodeRequest,
    [constants.DELETE_ITEM_CODE_SUCCESS]: deleteItemCodeSuccess,
    [constants.DELETE_ITEM_CODE_ERROR]: deleteItemCodeError,
    [constants.DELETE_ITEM_CODE_CLEAR]: deleteItemCodeClear,

    [constants.GET_ACTIVITY_REQUEST]: getActivityRequest,
    [constants.GET_ACTIVITY_SUCCESS]: getActivitySuccess,
    [constants.GET_ACTIVITY_ERROR]: getActivityError,
    [constants.GET_ACTIVITY_CLEAR]: getActivityClear,

    [constants.CANCEL_ADHOCK_REQUEST]: cancelAdhocRequest,
    [constants.CANCEL_ADHOCK_SUCCESS]: cancelAdhocSuccess,
    [constants.CANCEL_ADHOCK_ERROR]: cancelAdhocError,
    [constants.CANCEL_ADHOCK_CLEAR]: cancelAdhocClear,

    [constants.GET_ASSORTMENT_DROPDOWN_REQUEST]: getAssortmentDropdownRequest,
    [constants.GET_ASSORTMENT_DROPDOWN_SUCCESS]: getAssortmentDropdownSuccess,
    [constants.GET_ASSORTMENT_DROPDOWN_ERROR]: getAssortmentDropdownError,
    [constants.GET_ASSORTMENT_DROPDOWN_CLEAR]: getAssortmentDropdownClear,

    //Assortment Planning::: 

    [constants.GET_ASSORTMENT_PLANNING_FILTERS_REQUEST]: getAssortmentPlanningFiltersRequest,
    [constants.GET_ASSORTMENT_PLANNING_FILTERS_SUCCESS]: getAssortmentPlanningFiltersSuccess,
    [constants.GET_ASSORTMENT_PLANNING_FILTERS_ERROR]: getAssortmentPlanningFiltersError,
    [constants.GET_ASSORTMENT_PLANNING_FILTERS_CLEAR]: getAssortmentPlanningFiltersClear,

    [constants.GET_ASSORTMENT_PLANNING_COUNT_REQUEST]: getAssortmentPlannigCountRequest,
    [constants.GET_ASSORTMENT_PLANNING_COUNT_SUCCESS]: getAssortmentPlannigCountSuccess,
    [constants.GET_ASSORTMENT_PLANNING_COUNT_ERROR]: getAssortmentPlannigCountError,
    [constants.GET_ASSORTMENT_PLANNING_COUNT_CLEAR]: getAssortmentPlannigCountClear,

    [constants.GET_ASSORTMENT_PLANNING_DETAIL_REQUEST]: getAssortmentPlanningDetailRequest,
    [constants.GET_ASSORTMENT_PLANNING_DETAIL_SUCCESS]: getAssortmentPlanningDetailSuccess,
    [constants.GET_ASSORTMENT_PLANNING_DETAIL_ERROR]: getAssortmentPlanningDetailError,
    [constants.GET_ASSORTMENT_PLANNING_DETAIL_CLEAR]: getAssortmentPlanningDetailClear,

    [constants.SAVE_ASSORTMENT_PLANNING_DETAIL_REQUEST]: saveAssortmentPlannigDetailRequest,
    [constants.SAVE_ASSORTMENT_PLANNING_DETAIL_SUCCESS]: saveAssortmentPlannigDetailSuccess,
    [constants.SAVE_ASSORTMENT_PLANNING_DETAIL_ERROR]: saveAssortmentPlannigDetailError,
    [constants.SAVE_ASSORTMENT_PLANNING_DETAIL_CLEAR]: saveAssortmentPlannigDetailClear,

}, initialState);
