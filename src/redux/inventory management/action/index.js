import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';
import moment from 'moment';
import { OganisationIdName } from '../../../organisationIdName';

export function* hl1Request(action) {
    try {
        let no = action.payload.no;
        let type = action.payload.type == undefined || action.payload.type == "" ? 1 : action.payload.type;
        let search = action.payload.search == undefined ? "" : action.payload.search;
        let totalCount = action.payload.totalCount

        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/get/hl1Name?type=${type}&pageNo=${no}&totalCount=${totalCount}&search=${search}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.hl1Success(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.hl1Error(response.data));
        }

    } catch (e) {
        yield put(actions.hl1Error("error occurs"));
        console.warn('Some error found in "hl1Request" action\n', e);
    }
}

export function* hl2Request(action) {
    if (action.payload == undefined) {
        yield put(actions.hl2Clear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/find/hl2Name`, {
                hl1Name: action.payload.hl1Name,

                type: action.payload.type == undefined || action.payload.type == "" ? 1 : action.payload.type,
                pageNo: action.payload.no,
                totalCount: action.payload.totalCount,
                search: action.payload.search == undefined ? "" : action.payload.search
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.hl2Success(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.hl2Error(response.data));
            }

        } catch (e) {
            yield put(actions.hl2Error("error occurs"));
            console.warn('Some error found in "hl2Request" action\n', e);
        }
    }
}

export function* hl3Request(action) {
    if (action.payload == undefined) {
        yield put(actions.hl3Clear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/find/hl3Name`, {
                hl1Name: action.payload.hl1Name,
                hl2Name: action.payload.hl2Name,
                type: action.payload.type == undefined || action.payload.type == "" ? 1 : action.payload.type,
                pageNo: action.payload.no,
                totalCount: action.payload.totalCount,
                search: action.payload.search == undefined ? "" : action.payload.search
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.hl3Success(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.hl3Error(response.data));
            }

        } catch (e) {
            yield put(actions.hl3Error("error occurs"));
            console.warn('Some error found in "hl1Request" action\n', e);
        }
    }
}

export function* hl4Request(action) {
    if (action.payload == undefined) {
        yield put(actions.hl4Clear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/find/hl4Name`, {
                hl1Name: action.payload.hl1Name,
                hl2Name: action.payload.hl2Name,
                hl3Name: action.payload.hl3Name,
                type: action.payload.type == undefined || action.payload.type == "" ? 1 : action.payload.type,
                pageNo: action.payload.no,
                totalCount: action.payload.totalCount,
                search: action.payload.search == undefined ? "" : action.payload.search

            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.hl4Success(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.hl4Error(response.data));
            }

        } catch (e) {
            yield put(actions.hl4Error("error occurs"));
            console.warn('Some error found in "hl4Request" action\n', e);
        }
    }
}

export function* assetCodeRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.assetCodeClear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/find/assortmentCode`, {
                hl1Name: action.payload.hl1Name,
                hl2Name: action.payload.hl2Name,
                hl3Name: action.payload.hl3Name,
                hl4Name: action.payload.hl4Name,
                hl5Name: action.payload.hl5Name,
                hl6Name: action.payload.hl6Name,
                itemName: action.payload.itemName,
                brand: action.payload.brand,
                color: action.payload.color,
                size: action.payload.size,
                pattern: action.payload.pattern,
                material: action.payload.material,
                design: action.payload.design,
                barCode: action.payload.barCode,
                sell_Price: action.payload.sell_Price
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.assetCodeSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.assetCodeError(response.data));
            }

        } catch (e) {
            yield put(actions.assetCodeError("error occurs"));
            console.warn('Some error found in "assetCodeRequest" action\n', e);
        }
    }
}

export function* createAssortmentRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.createAssortmentClear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/create`, {
                hl1Name: action.payload.hl1Name,
                hl2Name: action.payload.hl2Name,
                hl3Name: action.payload.hl3Name,
                hl4Name: action.payload.hl4Name,
                hl5Name: action.payload.hl5Name,
                hl6Name: action.payload.hl6Name,
                itemName: action.payload.itemName,
                brand: action.payload.brand,
                color: action.payload.color,
                size: action.payload.size,
                pattern: action.payload.pattern,
                material: action.payload.material,
                design: action.payload.design,
                barCode: action.payload.barCode,
                sell_Price: action.payload.sell_Price,
                configureAssortment: action.payload.configureAssortment,
                assortmentcode: action.payload.assortmentcode
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.createAssortmentSuccess(response.data.data));
                let data = {
                    no: 1,
                    type: 1,
                    search: ""
                }
                yield put(actions.getAssortmentRequest(data));
            } else if (finalResponse.failure) {
                yield put(actions.createAssortmentError(response.data));
            }

        } catch (e) {
            yield put(actions.createAssortmentError("error occurs"));
            console.warn('Some error found in "createAssortmentRequest" action\n', e);
        }
    }
}

export function* getAssortmentRequest(action) {

    if (action.payload == undefined) {
        yield put(actions.getAssortmentClear());
    } else {
        try {
            let no = action.payload.no;
            let type = action.payload.type == undefined ? 1 : action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/get/all?pageNo=${no}&type=${type}&search=${search}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAssortmentSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAssortmentError(response.data));
            }

        } catch (e) {
            yield put(actions.getAssortmentError("error occurs"));
            console.warn('Some error found in "getAssortmentRequest" action\n', e);
        }
    }
}

export function* getAssortmentStatusRequest(action) {
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/get/status`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAssortmentStatusSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAssortmentStatusError(response.data));
        }

    } catch (e) {
        yield put(actions.getAssortmentStatusError("error occurs"));
        console.warn('Some error found in "getAssortmentStatusRequest" action\n', e);
    }
}



export function* viewMessageRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.viewMessageClear());
    } else {
        try {
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/view/message`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.viewMessageSuccess(response.data.data));
                yield put(actions.getAssortmentStatusRequest());
            } else if (finalResponse.failure) {
                yield put(actions.viewMessageError(response.data));
            }

        } catch (e) {
            yield put(actions.viewMessageError("error occurs"));
            console.warn('Some error found in "viewMessageRequest" action\n', e);
        }
    }
}


export function* getAdHockRequest(action) {
    var { orgIdGlobal } = OganisationIdName()
    if (action.payload == undefined) {
        yield put(actions.getAdHockClear());
    } else {
        try {
            let no = action.payload.no == undefined ? 1 : action.payload.no;
            let type = action.payload.type == undefined ? 1 : action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let workOrder = action.payload.workOrder == undefined ? "" : action.payload.workOrder;
            let status = action.payload.status == undefined ? "" : action.payload.status;
            let site = action.payload.site == undefined ? "" : action.payload.site;
            let createdOn = action.payload.createdOn == undefined || action.payload.createdOn == "" ? "" : moment(action.payload.createdOn).format("YYYY-MM-DD");
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV}/adhoc/get/all?pageNo=${no}&type=${type}&search=${search}&workOrder=${workOrder}&status=${status}&site=${site}&createdOn=${createdOn}&orgId=${orgIdGlobal}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAdHockSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAdHockError(response.data));
            }

        } catch (e) {
            yield put(actions.getAdHockError("error occurs"));
            console.warn('Some error found in "getAdHockRequest" action\n', e);
        }
    }
}

export function* getSiteAdHockRequest(action) {

    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV}/adhoc/get/site`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getSiteAdHockSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getSiteAdHockError(response.data));
        }

    } catch (e) {
        yield put(actions.getSiteAdHockError("error occurs"));
        console.warn('Some error found in "getSiteAdHockRequest" action\n', e);
    }
}


export function* getItemAdHockRequest(action) {
    let no = action.payload.no == undefined ? 1 : action.payload.no;
    let type = action.payload.type == undefined ? 1 : action.payload.type;
    let search = action.payload.search == undefined ? "" : action.payload.search;
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV}/adhoc/get/itemcode?pageNo=${no}&type=${type}&search=${search}`, {
        });
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getItemAdHockSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getItemAdHockError(response.data));
        }

    } catch (e) {
        yield put(actions.getItemAdHockError("error occurs"));
        console.warn('Some error found in "getItemAdHockRequest" action\n', e);
    }
}


export function* createAdhocRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.createAdhocClear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV}/adhoc/create`, {
                'site': action.payload.site,
                'requestedDate': moment(action.payload.requestedDate).format(),
                'item': action.payload.item,
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.createAdhocSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.createAdhocError(response.data));
            }

        } catch (e) {
            yield put(actions.createAdhocError("error occurs"));
            console.warn('Some error found in "createAdHockRequest" action\n', e);
        }
    }
}



export function* editAdhocRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.editAdhocClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.INV}/adhoc/update/status`, {
                'status': action.payload.status,
                'workOrder': action.payload.workOrder,


                // user: user,
            });
            const finalResponse = script(response);
            if (finalResponse.success) {

                yield put(actions.editAdhocSuccess(response.data.data));
                yield put(actions.getAdHockRequest(action.payload));
            } else if (finalResponse.failure) {
                yield put(actions.editAdhocError(response.data));
            }
        } catch (e) {
            yield put(actions.editAdhocError('Error Occurs !!'));
            console.warn('Some error found in "adhoc edit" action\n', e);
        }
    }

}



export function* deleteItemCodeRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteItemCodeClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV}/adhoc/delete?workOrder=${action.payload.workOrder}&itemData=${action.payload.itemData}`, {



                // user: user,
            });
            const finalResponse = script(response);
            if (finalResponse.success) {

                yield put(actions.deleteItemCodeSuccess(response.data.data));
                yield put(actions.getAdHockRequest(action.payload));
            } else if (finalResponse.failure) {
                yield put(actions.deleteItemCodeError(response.data));
            }
        } catch (e) {
            yield put(actions.deleteItemCodeError('Error Occurs !!'));
            console.warn('Some error found in "deleteItemCode" action\n', e);
        }
    }

}

export function* getActivityRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getActivityClear());
    } else {
        try {
            let no = action.payload.no == undefined ? 1 : action.payload.no;
            let type = action.payload.type == undefined ? 1 : action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let itemCount = action.payload.itemCount == undefined ? "" : action.payload.itemCount;
            let status = action.payload.status == undefined ? "" : action.payload.status;
            let startDate = action.payload.startDate == undefined ? "" : action.payload.startDate;
            let endDate = action.payload.endDate == undefined ? "" : action.payload.endDate;
            let createdOn = action.payload.createdOn == undefined || action.payload.createdOn == "" ? "" : moment(action.payload.createdOn).format("YYYY-MM-DD");
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/assortment/get/all/history?pageNo=${no}&type=${type}&itemCount=${itemCount}&startDate=${startDate}&endDate=${endDate}&createdOn=${createdOn}&status=${status}&search=${search}`, {
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getActivitySuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getActivityError(response.data));
            }

        } catch (e) {
            yield put(actions.getActivityError("error occurs"));
            console.warn('Some error found in "getActivityRequest" action\n', e);
        }
    }
}



export function* cancelAdhocRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.cancelAdhocClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.INV}/adhoc/cancel/workorder?workOrder=${action.payload}`, {



                // user: user,
            });
            const finalResponse = script(response);
            if (finalResponse.success) {

                yield put(actions.cancelAdhocSuccess(response.data.data));
                yield put(actions.getAdHockRequest(action.payload));
            } else if (finalResponse.failure) {
                yield put(actions.cancelAdhocError(response.data));
            }
        } catch (e) {
            yield put(actions.cancelAdhocError('Error Occurs !!'));
            console.warn('Some error found in "cancelAdhoc" action\n', e);
        }
    }

}


export function* getAssortmentDropdownRequest(action) {

    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/assortment/get/all/dropdown`, {



            // user: user,
        });
        const finalResponse = script(response);
        if (finalResponse.success) {

            yield put(actions.getAssortmentDropdownSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAssortmentDropdownError(response.data));
        }
    } catch (e) {
        yield put(actions.getAssortmentDropdownError('Error Occurs !!'));
        console.warn('Some error found in "cancelAdhoc" action\n', e);

    }

}

//Assortment Planning API::

export function* getAssortmentPlanningFiltersRequest(action) {
    // if (action.payload == undefined) {
    //     yield put(actions.getAssortmentPlanningFiltersClear());
    // }
    // else {
    try {
        const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/get/assortment-planning-filters`)
        const finalResponse = script(response);
        if (finalResponse.success) {
            yield put(actions.getAssortmentPlanningFiltersSuccess(response.data.data));
        } else if (finalResponse.failure) {
            yield put(actions.getAssortmentPlanningFiltersError(response.data));
        }
    } catch (e) {
        yield put(actions.getAssortmentPlanningFiltersError('Error Occurs !!'));
        console.warn('Some error found in "assortment filters" action\n', e);
    }
    // }
}

export function* getAssortmentPlannigCountRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAssortmentPlannigCountClear());
    }
    else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/get/assortment-Planning-count`, {
                "division": action.payload.division,
                "section": action.payload.section,
                "department": action.payload.department,
                "category": action.payload.category,
                "color": action.payload.color,
                "pattern": action.payload.pattern,
                "material": action.payload.material,
                "neck": action.payload.neck,
                "article": action.payload.article,
                "brand": action.payload.brand,
                "sleeve": action.payload.sleeve,
                "price": action.payload.mrp,
            })
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAssortmentPlannigCountSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAssortmentPlannigCountError(response.data));
            }
        } catch (e) {
            yield put(actions.getAssortmentPlannigCountError('Error Occurs !!'));
            console.warn('Some error found in "assortment count" action\n', e);
        }
    }
}

export function* getAssortmentPlanningDetailRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getAssortmentPlanningDetailClear());
    }
    else {
        let pageNo = action.payload.pageNo == undefined ? 1 : action.payload.pageNo;
        let totalRecord = action.payload.totalRecord == undefined ? 0 : action.payload.totalRecord;
        let type = action.payload.type = undefined ? 1 : action.payload.type;
        try {
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/get/assortment-Planning-detail?pageNo=${pageNo}&totalRecord=${totalRecord}&type=${type}`)
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getAssortmentPlanningDetailSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getAssortmentPlanningDetailError(response.data));
            }
        } catch (e) {
            yield put(actions.getAssortmentPlanningDetailError('Error Occurs !!'));
            console.warn('Some error found in "assortment detail" action\n', e);
        }
    }
}

export function* saveAssortmentPlannigDetailRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.saveAssortmentPlannigDetailClear());
    }
    else {
        let type = action.payload.type == undefined ? 1 : action.payload.type;
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.ASSORTMENT}/save/assortment-Planning?type=${type}`, {
                "division": action.payload.division,
                "section": action.payload.section,
                "department": action.payload.department,
                "category": action.payload.category,
                "color": action.payload.color,
                "pattern": action.payload.pattern,
                "material": action.payload.material,
                "neck": action.payload.neck,
                "article": action.payload.article,
                "brand": action.payload.brand,
                "sleeve": action.payload.sleeve,
                "price": action.payload.mrp,
            })
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.saveAssortmentPlannigDetailSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.saveAssortmentPlannigDetailError(response.data));
            }
        } catch (e) {
            yield put(actions.saveAssortmentPlannigDetailError('Error Occurs !!'));
            console.warn('Some error found in "save assortment" action\n', e);
        }
    }
}

export function* getExcelDownloadedPoRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getExcelDownloadedPoClear());
    } else {
        try {

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/po/export/pos/data`, {
                "fromDate": action.payload.startDate,
                "toDate": action.payload.endDate,
                "isMainHeader": action.payload.mainHeader,
                "search": action.payload.searchData,
                "headers": action.payload.headersdata,
                "filter": action.payload.filterdata
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                window.open(response.data.data.resource);
                yield put(actions.getExcelDownloadedPoSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getExcelDownloadedPoError(response.data));
            }
        } catch (e) {
            yield put(actions.getExcelDownloadedPoError("error occurs"));
            console.warn('Some error found in "getExcelDownloadedPoRequest" action\n', e);
        }
    }
}

export function* getExcelDownloadedPiRequest(action) {
    console.log(action)
    if (action.payload == undefined) {
        yield put(actions.getExcelDownloadedPiClear());
    } else {
        try {

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}/admin/pi/export/data`, {
                "fromDate": action.payload.startDate,
                "toDate": action.payload.endDate,
                "isMainHeader": action.payload.mainHeader,
                "search": action.payload.searchData,
                "headers": action.payload.headersdata,
                "filter": action.payload.filterdata
            });
            const finalResponse = script(response);
            if (finalResponse.success) {
                window.open(response.data.data.resource);
                yield put(actions.getExcelDownloadedPiSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getExcelDownloadedPiError(response.data));
            }
        } catch (e) {
            yield put(actions.getExcelDownloadedPiError("error occurs"));
            console.warn('Some error found in "getExcelDownloadedPiRequest" action\n', e);
        }
    }
}