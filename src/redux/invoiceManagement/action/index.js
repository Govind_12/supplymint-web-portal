import { call, put } from 'redux-saga/effects';
import * as actions from '../../actions';
import fireAjax from '../../../services/index';
import { CONFIG } from '../../../config/index';
import script from '../../script';

//GET ITEM:::
export function* getInvoiceManagementItemRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getInvoiceManagementItemClear())
    } else {
        try {
            let pageNo = action.payload.pageNo == undefined ? 1 : action.payload.pageNo;
            let type = action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy;
            let sortedIn = action.payload.sortedIn == undefined ? "" : action.payload.sortedIn;
            let itemType = action.payload.filter.itemType;
            let itemName = action.payload.filter.itemName;
            let itemUnit = action.payload.filter.itemUnit;
            let itemHSN = action.payload.filter.itemHSN;
            let taxPreference = action.payload.filter.taxPreference;
            let sellingPrice = action.payload.filter.sellingPrice;
            let itemDescription = action.payload.filter.itemDescription;
            let interTaxRate = action.payload.filter.interTaxRate;
            let intraTaxRate = action.payload.filter.intraTaxRate;
            let get = {
                pageNo: pageNo,
                type: type,
                search: search,
                sortedBy,
                sortedIn,
                itemType,
                itemName,
                itemUnit,
                itemHSN,
                taxPreference,
                sellingPrice,
                itemDescription,
                interTaxRate,
                intraTaxRate
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/get/items`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getInvoiceManagementItemSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getInvoiceManagementItemError(response.data));
            }

        } catch (e) {
            yield put(actions.getInvoiceManagementItemError("error occurs"));
            console.warn('Some error found in "getInvoiceManagementItemRequest" action\n', e);
        }
    }
}

//SAVE ITEM:::
export function* saveInvoiceManagementItemRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.saveInvoiceManagementItemClear())
    } else {
        try {
            let itemType = action.payload.itemType;
            let itemName = action.payload.itemName;
            let itemUnit = action.payload.itemUnit;
            let itemHSN = action.payload.itemHSN;
            let taxPreference = action.payload.taxPreference;
            let sellingPrice = action.payload.sellingPrice;
            let itemDescription = action.payload.itemDescription;
            let interTaxRate = action.payload.interTaxRate;
            let intraTaxRate = action.payload.intraTaxRate;
            let get = {
                itemType,
                itemName,
                itemUnit,
                itemHSN,
                taxPreference,
                sellingPrice,
                itemDescription,
                interTaxRate,
                intraTaxRate
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/save/items`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.saveInvoiceManagementItemSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.saveInvoiceManagementItemError(response.data));
            }

        } catch (e) {
            yield put(actions.saveInvoiceManagementItemError("error occurs"));
            console.warn('Some error found in "saveInvoiceManagementItemRequest" action\n', e);
        }
    }
}

//DELETE ITEM:::
export function* deleteInvoiceManagementItemRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteInvoiceManagementItemClear())
    } else {
        try {
            let id = action.payload.id;
            let get = {
                id
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/delete/items`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.deleteInvoiceManagementItemSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.deleteInvoiceManagementItemError(response.data));
            }

        } catch (e) {
            yield put(actions.deleteInvoiceManagementItemError("error occurs"));
            console.warn('Some error found in "deleteInvoiceManagementItemRequest" action\n', e);
        }
    }
}

//UPDATE ITEM:::
export function* updateInvoiceManagementItemRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updateInvoiceManagementItemClear())
    } else {
        try {
            let id = action.payload.id;
            let itemType = action.payload.itemType;
            let itemName = action.payload.itemName;
            let itemUnit = action.payload.itemUnit;
            let itemHSN = action.payload.itemHSN;
            let taxPreference = action.payload.taxPreference;
            let sellingPrice = action.payload.sellingPrice;
            let itemDescription = action.payload.itemDescription;
            let interTaxRate = action.payload.interTaxRate;
            let intraTaxRate = action.payload.intraTaxRate;
            let get = {
                id,
                itemType,
                itemName,
                itemUnit,
                itemHSN,
                taxPreference,
                sellingPrice,
                itemDescription,
                interTaxRate,
                intraTaxRate
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/update/items`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateInvoiceManagementItemSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.updateInvoiceManagementItemError(response.data));
            }

        } catch (e) {
            yield put(actions.updateInvoiceManagementItemError("error occurs"));
            console.warn('Some error found in "updateInvoiceManagementItemRequest" action\n', e);
        }
    }
}

//GET CUSTOMER:::
export function* getInvoiceManagementCustomerRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getInvoiceManagementCustomerClear())
    } else {
        try {
            let pageNo = action.payload.pageNo == undefined ? 1 : action.payload.pageNo;
            let type = action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy;
            let sortedIn = action.payload.sortedIn == undefined ? "" : action.payload.sortedIn;

            let custType = action.payload.filter.custType;
            let custSalutation = action.payload.filter.custSalutation;
            let custFirstName = action.payload.filter.custFirstName;
            let custLastName = action.payload.filter.custLastName;
            let taxPreference = action.payload.filter.taxPreference;
            let custCompany = action.payload.filter.custCompany;
            let custDisplayName = action.payload.filter.custDisplayName;
            let custNumber = action.payload.filter.custNumber;
            let custEmail = action.payload.filter.custEmail;
            let currency = action.payload.filter.currency;
            let custWebsite = action.payload.filter.custWebsite;
            let gstIn = action.payload.filter.gstIn;
            let gstInPos = action.payload.filter.gstInPos;
            let gstTreatment = action.payload.filter.gstTreatment;
            let paymentTerms = action.payload.filter.paymentTerms;
            let billingAddress1 = action.payload.filter.billingAddress1;
            let billingAddress2 = action.payload.filter.billingAddress2;
            let billingCountry = action.payload.filter.billingCountry;
            let billingState = action.payload.filter.billingState;
            let billingCity = action.payload.filter.billingCity;
            let billingPincode = action.payload.filter.billingPincode;
            let shippingAddress1 = action.payload.filter.shippingAddress1;
            let shippingAddress2 = action.payload.filter.shippingAddress2;
            let shippingCountry = action.payload.filter.shippingCountry;
            let shippingCity = action.payload.filter.shippingCity;
            let shippingState = action.payload.filter.shippingState;
            let shippingPincode = action.payload.filter.shippingPincode;
            let remarks = action.payload.filter.remarks;

            let get = {
                pageNo: pageNo,
                type: type,
                search: search,
                sortedBy,
                sortedIn,

                custType,
                custSalutation,
                custFirstName,
                custLastName,
                taxPreference,
                custCompany,
                custDisplayName,
                custNumber,
                custEmail,
                currency,
                custWebsite,
                gstIn,
                gstInPos,
                gstTreatment,
                paymentTerms,
                billingAddress1,
                billingAddress2,
                billingCountry,
                billingState,
                billingCity,
                billingPincode,
                shippingAddress1,
                shippingAddress2,
                shippingCountry,
                shippingCity,
                shippingState,
                shippingPincode,
                remarks
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/get/customers`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getInvoiceManagementCustomerSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getInvoiceManagementCustomerError(response.data));
            }

        } catch (e) {
            yield put(actions.getInvoiceManagementCustomerError("error occurs"));
            console.warn('Some error found in "getInvoiceManagementCustomerRequest" action\n', e);
        }
    }
}

//SAVE CUSTOMER:::
export function* saveInvoiceManagementCustomerRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.saveInvoiceManagementCustomerClear())
    } else {
        try {
            let custType = action.payload.custType;
            let custSalutation = action.payload.custSalutation;
            let custFirstName = action.payload.custFirstName;
            let custLastName = action.payload.custLastName;
            let taxPreference = action.payload.taxPreference;
            let custCompany = action.payload.custCompany;
            let custDisplayName = action.payload.custDisplayName;
            let custNumber = action.payload.custNumber;
            let custEmail = action.payload.custEmail;
            let currency = action.payload.currency;
            let custWebsite = action.payload.custWebsite;
            let gstIn = action.payload.gstIn;
            let gstInPos = action.payload.gstInPos;
            let gstTreatment = action.payload.gstTreatment;
            let paymentTerms = action.payload.paymentTerms;
            let billingAddress1 = action.payload.billingAddress1;
            let billingAddress2 = action.payload.billingAddress2;
            let billingCountry = action.payload.billingCountry;
            let billingState = action.payload.billingState;
            let billingCity = action.payload.billingCity;
            let billingPincode = action.payload.billingPincode;
            let shippingAddress1 = action.payload.shippingAddress1;
            let shippingAddress2 = action.payload.shippingAddress2;
            let shippingCountry = action.payload.shippingCountry;
            let shippingCity = action.payload.shippingCity;
            let shippingState = action.payload.shippingState;
            let shippingPincode = action.payload.shippingPincode;
            let remarks = action.payload.remarks;

            let get = {
                custType,
                custSalutation,
                custFirstName,
                custLastName,
                taxPreference,
                custCompany,
                custDisplayName,
                custNumber,
                custEmail,
                currency,
                custWebsite,
                gstIn,
                gstInPos,
                gstTreatment,
                paymentTerms,
                billingAddress1,
                billingAddress2,
                billingCountry,
                billingState,
                billingCity,
                billingPincode,
                shippingAddress1,
                shippingAddress2,
                shippingCountry,
                shippingCity,
                shippingState,
                shippingPincode,
                remarks
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/save/customers`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.saveInvoiceManagementCustomerSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.saveInvoiceManagementCustomerError(response.data));
            }

        } catch (e) {
            yield put(actions.saveInvoiceManagementCustomerError("error occurs"));
            console.warn('Some error found in "saveInvoiceManagementCustomerRequest" action\n', e);
        }
    }
}

//DELETE CUSTOMER:::
export function* deleteInvoiceManagementCustomerRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteInvoiceManagementCustomerClear())
    } else {
        try {
            let id = action.payload.id;
            let get = {
                id
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/delete/customers`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.deleteInvoiceManagementCustomerSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.deleteInvoiceManagementCustomerError(response.data));
            }

        } catch (e) {
            yield put(actions.deleteInvoiceManagementCustomerError("error occurs"));
            console.warn('Some error found in "deleteInvoiceManagementCustomerRequest" action\n', e);
        }
    }
}

//UPDATE CUSTOMER:::
export function* updateInvoiceManagementCustomerRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.updateInvoiceManagementCustomerClear())
    } else {
        try {
            let id = action.payload.id;
            let custType = action.payload.custType;
            let custSalutation = action.payload.custSalutation;
            let custFirstName = action.payload.custFirstName;
            let custLastName = action.payload.custLastName;
            let taxPreference = action.payload.taxPreference;
            let custCompany = action.payload.custCompany;
            let custDisplayName = action.payload.custDisplayName;
            let custNumber = action.payload.custNumber;
            let custEmail = action.payload.custEmail;
            let currency = action.payload.currency;
            let custWebsite = action.payload.custWebsite;
            let gstIn = action.payload.gstIn;
            let gstInPos = action.payload.gstInPos;
            let gstTreatment = action.payload.gstTreatment;
            let paymentTerms = action.payload.paymentTerms;
            let billingAddress1 = action.payload.billingAddress1;
            let billingAddress2 = action.payload.billingAddress2;
            let billingCountry = action.payload.billingCountry;
            let billingState = action.payload.billingState;
            let billingCity = action.payload.billingCity;
            let billingPincode = action.payload.billingPincode;
            let shippingAddress1 = action.payload.shippingAddress1;
            let shippingAddress2 = action.payload.shippingAddress2;
            let shippingCountry = action.payload.shippingCountry;
            let shippingCity = action.payload.shippingCity;
            let shippingState = action.payload.shippingState;
            let shippingPincode = action.payload.shippingPincode;
            let remarks = action.payload.remarks;

            let get = {
                id,
                custType,
                custSalutation,
                custFirstName,
                custLastName,
                taxPreference,
                custCompany,
                custDisplayName,
                custNumber,
                custEmail,
                currency,
                custWebsite,
                gstIn,
                gstInPos,
                gstTreatment,
                paymentTerms,
                billingAddress1,
                billingAddress2,
                billingCountry,
                billingState,
                billingCity,
                billingPincode,
                shippingAddress1,
                shippingAddress2,
                shippingCountry,
                shippingCity,
                shippingState,
                shippingPincode,
                remarks
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/update/customers`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.updateInvoiceManagementCustomerSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.updateInvoiceManagementCustomerError(response.data));
            }

        } catch (e) {
            yield put(actions.updateInvoiceManagementCustomerError("error occurs"));
            console.warn('Some error found in "updateInvoiceManagementCustomerRequest" action\n', e);
        }
    }
}

//GET INVOICE MANAGEMENT GENERIC:::
export function* getInvoiceManagementGenericRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getInvoiceManagementGenericClear())
    } else {
        try {
            let pageNo = action.payload.pageNo == undefined ? 1 : action.payload.pageNo;
            let type = action.payload.type;
            let search = action.payload.search == undefined ? "" : action.payload.search;
            let sortedBy = action.payload.sortedBy == undefined ? "" : action.payload.sortedBy;
            let sortedIn = action.payload.sortedIn == undefined ? "" : action.payload.sortedIn;
            let genericType = action.payload.genericType == undefined ? "" : action.payload.genericType;
            let filter = {...action.payload.filter}

            let get = {
                pageNo: pageNo,
                type: type,
                search: search,
                sortedBy,
                sortedIn,
                genericType
            }
            let final = {...get, filter}

            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/get/invoiceMain`, final
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.getInvoiceManagementGenericSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getInvoiceManagementGenericError(response.data));
            }

        } catch (e) {
            yield put(actions.getInvoiceManagementGenericError("error occurs"));
            console.warn('Some error found in "getInvoiceManagementGenericRequest" action\n', e);
        }
    }
}

//DELETE INVOICE MANAGEMENT GENERIC:::
export function* deleteInvoiceManagementGenericRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.deleteInvoiceManagementGenericClear())
    } else {
        try {
            let id = action.payload;
           
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/delete/invoiceMain`, id
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.deleteInvoiceManagementGenericSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.deleteInvoiceManagementGenericError(response.data));
            }

        } catch (e) {
            yield put(actions.deleteInvoiceManagementGenericError("error occurs"));
            console.warn('Some error found in "deleteInvoiceManagementGenericRequest" action\n', e);
        }
    }
}

//EXPAND INVOICE MANAGEMENT GENERIC:::
export function* expandInvoiceManagementGenericRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.expandInvoiceManagementGenericClear())
    } else {
        try {
            let id = action.payload.id;
            let get = {
                id
            }
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/get/invoiceMainDetails`, get
            );  
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.expandInvoiceManagementGenericSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.expandInvoiceManagementGenericError(response.data));
            }

        } catch (e) {
            yield put(actions.expandInvoiceManagementGenericError("error occurs"));
            console.warn('Some error found in "expandInvoiceManagementGenericRequest" action\n', e);
        }
    }
}

export function* getCoreDropdownRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.getCoreDropdownClear());
    } else {
        try {
            let type = action.payload.type
            const response = yield call(fireAjax, 'GET', `${CONFIG.BASE_URL}/core/dropdown/get?type=${type}`);
            const finalResponse = script(response);
            if (finalResponse.success) {
                response.data.data.type = type;
                yield put(actions.getCoreDropdownSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.getCoreDropdownError(response.data));
            }
        } catch (e) {
            yield put(actions.getCoreDropdownError("error occurs"));
            console.warn('Some error found in "getCoreDropdownRequest" action\n', e);
        }
    }
}

export function* searchInvoiceDataRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.searchInvoiceDataClear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/search/invoiceData`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.searchInvoiceDataSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.searchInvoiceDataError(response.data));
            }
        } catch (e) {
            yield put(actions.searchInvoiceDataError("error occurs"));
            console.warn('Some error found in "searchInvoiceDataRequest" action\n', e);
        }
    }
}

export function* createInvoiceRequest(action) {
    if (action.payload == undefined) {
        yield put(actions.createInvoiceClear());
    } else {
        try {
            const response = yield call(fireAjax, 'POST', `${CONFIG.BASE_URL}${CONFIG.VENDINVOICEMANAGEMENT}/save/invoiceMain`, action.payload);
            const finalResponse = script(response);
            if (finalResponse.success) {
                yield put(actions.createInvoiceSuccess(response.data.data));
            } else if (finalResponse.failure) {
                yield put(actions.createInvoiceError(response.data));
            }
        } catch (e) {
            yield put(actions.createInvoiceError("error occurs"));
            console.warn('Some error found in "createInvoiceRequest" action\n', e);
        }
    }
}