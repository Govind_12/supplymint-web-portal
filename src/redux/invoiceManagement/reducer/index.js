import { handleActions } from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../../constants';

let initialState = {
    getInvoiceManagementItem: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    saveInvoiceManagementItem: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    deleteInvoiceManagementItem: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    updateInvoiceManagementItem: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getInvoiceManagementCustomer: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    saveInvoiceManagementCustomer: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    deleteInvoiceManagementCustomer: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    updateInvoiceManagementCustomer: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getInvoiceManagementGeneric: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    deleteInvoiceManagementGeneric: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    expandInvoiceManagementGeneric: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    getCoreDropdown: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    searchInvoiceData: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
    createInvoice: {
        data: {},
        isLoading: false,
        isSuccess: false,
        isError: false,
        message: ''
    },
};

// GET ITEM::
const getInvoiceManagementItemRequest = (state, action) => update(state, {
    getInvoiceManagementItem: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getInvoiceManagementItemError = (state, action) => update(state, {
    getInvoiceManagementItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getInvoiceManagementItemSuccess = (state, action) => update(state, {
    getInvoiceManagementItem: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getInvoiceManagementItemClear = (state, action) => update(state, {
    getInvoiceManagementItem: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// SAVE ITEM:::
const saveInvoiceManagementItemRequest = (state, action) => update(state, {
    saveInvoiceManagementItem: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const saveInvoiceManagementItemError = (state, action) => update(state, {
    saveInvoiceManagementItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const saveInvoiceManagementItemSuccess = (state, action) => update(state, {
    saveInvoiceManagementItem: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const saveInvoiceManagementItemClear = (state, action) => update(state, {
    saveInvoiceManagementItem: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//DELETE ITEM::::
const deleteInvoiceManagementItemRequest = (state, action) => update(state, {
    deleteInvoiceManagementItem: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const deleteInvoiceManagementItemError = (state, action) => update(state, {
    deleteInvoiceManagementItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const deleteInvoiceManagementItemSuccess = (state, action) => update(state, {
    deleteInvoiceManagementItem: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const deleteInvoiceManagementItemClear = (state, action) => update(state, {
    deleteInvoiceManagementItem: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//UPDATE ITEM::::
const updateInvoiceManagementItemRequest = (state, action) => update(state, {
    updateInvoiceManagementItem: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateInvoiceManagementItemError = (state, action) => update(state, {
    updateInvoiceManagementItem: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const updateInvoiceManagementItemSuccess = (state, action) => update(state, {
    updateInvoiceManagementItem: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const updateInvoiceManagementItemClear = (state, action) => update(state, {
    updateInvoiceManagementItem: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//GET CUSTOMER:::
const getInvoiceManagementCustomerRequest = (state, action) => update(state, {
    getInvoiceManagementCustomer: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getInvoiceManagementCustomerError = (state, action) => update(state, {
    getInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getInvoiceManagementCustomerSuccess = (state, action) => update(state, {
    getInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getInvoiceManagementCustomerClear = (state, action) => update(state, {
    getInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// SAVE CUSTOMER:::
const saveInvoiceManagementCustomerRequest = (state, action) => update(state, {
    saveInvoiceManagementCustomer: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const saveInvoiceManagementCustomerError = (state, action) => update(state, {
    saveInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const saveInvoiceManagementCustomerSuccess = (state, action) => update(state, {
    saveInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const saveInvoiceManagementCustomerClear = (state, action) => update(state, {
    saveInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//DELETE CUSTOMER:::
const deleteInvoiceManagementCustomerRequest = (state, action) => update(state, {
    deleteInvoiceManagementCustomer: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const deleteInvoiceManagementCustomerError = (state, action) => update(state, {
    deleteInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const deleteInvoiceManagementCustomerSuccess = (state, action) => update(state, {
    deleteInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const deleteInvoiceManagementCustomerClear = (state, action) => update(state, {
    deleteInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//UPDATE CUSTOMER:::
const updateInvoiceManagementCustomerRequest = (state, action) => update(state, {
    updateInvoiceManagementCustomer: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const updateInvoiceManagementCustomerError = (state, action) => update(state, {
    updateInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const updateInvoiceManagementCustomerSuccess = (state, action) => update(state, {
    updateInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const updateInvoiceManagementCustomerClear = (state, action) => update(state, {
    updateInvoiceManagementCustomer: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// GET INVOICE MANAGEMENT GENERIC:::
const getInvoiceManagementGenericRequest = (state, action) => update(state, {
    getInvoiceManagementGeneric: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getInvoiceManagementGenericError = (state, action) => update(state, {
    getInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getInvoiceManagementGenericSuccess = (state, action) => update(state, {
    getInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const getInvoiceManagementGenericClear = (state, action) => update(state, {
    getInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

//DELETE INVOICE MANAGEMENT GENERIC::::
const deleteInvoiceManagementGenericRequest = (state, action) => update(state, {
    deleteInvoiceManagementGeneric: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const deleteInvoiceManagementGenericError = (state, action) => update(state, {
    deleteInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const deleteInvoiceManagementGenericSuccess = (state, action) => update(state, {
    deleteInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const deleteInvoiceManagementGenericClear = (state, action) => update(state, {
    deleteInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

// EXPAND INVOICE MANAGEMENT GENERIC:::
const expandInvoiceManagementGenericRequest = (state, action) => update(state, {
    expandInvoiceManagementGeneric: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const expandInvoiceManagementGenericError = (state, action) => update(state, {
    expandInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const expandInvoiceManagementGenericSuccess = (state, action) => update(state, {
    expandInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const expandInvoiceManagementGenericClear = (state, action) => update(state, {
    expandInvoiceManagementGeneric: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});


const getCoreDropdownRequest = (state, action) => update(state, {
    getCoreDropdown: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const getCoreDropdownError = (state, action) => update(state, {
    getCoreDropdown: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const getCoreDropdownSuccess = (state, action) => update(state, {
    getCoreDropdown: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: {...state.getCoreDropdown.data, [action.payload.type]: action.payload} }
    }
});
const getCoreDropdownClear = (state, action) => update(state, {
    getCoreDropdown: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});

const searchInvoiceDataRequest = (state, action) => update(state, {
    searchInvoiceData: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const searchInvoiceDataError = (state, action) => update(state, {
    searchInvoiceData: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const searchInvoiceDataSuccess = (state, action) => update(state, {
    searchInvoiceData: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const searchInvoiceDataClear = (state, action) => update(state, {
    searchInvoiceData: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

const createInvoiceRequest = (state, action) => update(state, {
    createInvoice: {
        isLoading: { $set: true },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
});
const createInvoiceError = (state, action) => update(state, {
    createInvoice: {
        isLoading: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: true },
        message: { $set: action.payload }
    }
})
const createInvoiceSuccess = (state, action) => update(state, {
    createInvoice: {
        isLoading: { $set: false },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: 'Success' },
        data: { $set: action.payload }
    }
});
const createInvoiceClear = (state, action) => update(state, {
    createInvoice: {
        isLoading: { $set: false },
        isError: { $set: false },
        isSuccess: { $set: false },
        message: { $set: '' }
    }
})

export default handleActions({
    [constants.GET_INVOICE_MANAGEMENT_ITEM_REQUEST]: getInvoiceManagementItemRequest,
    [constants.GET_INVOICE_MANAGEMENT_ITEM_SUCCESS]: getInvoiceManagementItemSuccess,
    [constants.GET_INVOICE_MANAGEMENT_ITEM_CLEAR]: getInvoiceManagementItemClear,
    [constants.GET_INVOICE_MANAGEMENT_ITEM_ERROR]: getInvoiceManagementItemError,

    [constants.SAVE_INVOICE_MANAGEMENT_ITEM_REQUEST]: saveInvoiceManagementItemRequest,
    [constants.SAVE_INVOICE_MANAGEMENT_ITEM_SUCCESS]: saveInvoiceManagementItemSuccess,
    [constants.SAVE_INVOICE_MANAGEMENT_ITEM_CLEAR]: saveInvoiceManagementItemClear,
    [constants.SAVE_INVOICE_MANAGEMENT_ITEM_ERROR]: saveInvoiceManagementItemError,

    [constants.DELETE_INVOICE_MANAGEMENT_ITEM_REQUEST]: deleteInvoiceManagementItemRequest,
    [constants.DELETE_INVOICE_MANAGEMENT_ITEM_SUCCESS]: deleteInvoiceManagementItemSuccess,
    [constants.DELETE_INVOICE_MANAGEMENT_ITEM_CLEAR]: deleteInvoiceManagementItemClear,
    [constants.DELETE_INVOICE_MANAGEMENT_ITEM_ERROR]: deleteInvoiceManagementItemError,

    [constants.UPDATE_INVOICE_MANAGEMENT_ITEM_REQUEST]: updateInvoiceManagementItemRequest,
    [constants.UPDATE_INVOICE_MANAGEMENT_ITEM_SUCCESS]: updateInvoiceManagementItemSuccess,
    [constants.UPDATE_INVOICE_MANAGEMENT_ITEM_CLEAR]: updateInvoiceManagementItemClear,
    [constants.UPDATE_INVOICE_MANAGEMENT_ITEM_ERROR]: updateInvoiceManagementItemError,

    [constants.GET_INVOICE_MANAGEMENT_CUSTOMER_REQUEST]: getInvoiceManagementCustomerRequest,
    [constants.GET_INVOICE_MANAGEMENT_CUSTOMER_SUCCESS]: getInvoiceManagementCustomerSuccess,
    [constants.GET_INVOICE_MANAGEMENT_CUSTOMER_CLEAR]: getInvoiceManagementCustomerClear,
    [constants.GET_INVOICE_MANAGEMENT_CUSTOMER_ERROR]: getInvoiceManagementCustomerError,

    [constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST]: saveInvoiceManagementCustomerRequest,
    [constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_SUCCESS]: saveInvoiceManagementCustomerSuccess,
    [constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_CLEAR]: saveInvoiceManagementCustomerClear,
    [constants.SAVE_INVOICE_MANAGEMENT_CUSTOMER_ERROR]: saveInvoiceManagementCustomerError,

    [constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST]: deleteInvoiceManagementCustomerRequest,
    [constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_SUCCESS]: deleteInvoiceManagementCustomerSuccess,
    [constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_CLEAR]: deleteInvoiceManagementCustomerClear,
    [constants.DELETE_INVOICE_MANAGEMENT_CUSTOMER_ERROR]: deleteInvoiceManagementCustomerError,

    [constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_REQUEST]: updateInvoiceManagementCustomerRequest,
    [constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_SUCCESS]: updateInvoiceManagementCustomerSuccess,
    [constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_CLEAR]: updateInvoiceManagementCustomerClear,
    [constants.UPDATE_INVOICE_MANAGEMENT_CUSTOMER_ERROR]: updateInvoiceManagementCustomerError,

    [constants.GET_INVOICE_MANAGEMENT_GENERIC_REQUEST]: getInvoiceManagementGenericRequest,
    [constants.GET_INVOICE_MANAGEMENT_GENERIC_SUCCESS]: getInvoiceManagementGenericSuccess,
    [constants.GET_INVOICE_MANAGEMENT_GENERIC_CLEAR]: getInvoiceManagementGenericClear,
    [constants.GET_INVOICE_MANAGEMENT_GENERIC_ERROR]: getInvoiceManagementGenericError,

    [constants.DELETE_INVOICE_MANAGEMENT_GENERIC_REQUEST]: deleteInvoiceManagementGenericRequest,
    [constants.DELETE_INVOICE_MANAGEMENT_GENERIC_SUCCESS]: deleteInvoiceManagementGenericSuccess,
    [constants.DELETE_INVOICE_MANAGEMENT_GENERIC_CLEAR]: deleteInvoiceManagementGenericClear,
    [constants.DELETE_INVOICE_MANAGEMENT_GENERIC_ERROR]: deleteInvoiceManagementGenericError,

    [constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_REQUEST]: expandInvoiceManagementGenericRequest,
    [constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_SUCCESS]: expandInvoiceManagementGenericSuccess,
    [constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_CLEAR]: expandInvoiceManagementGenericClear,
    [constants.EXPAND_INVOICE_MANAGEMENT_GENERIC_ERROR]: expandInvoiceManagementGenericError,

    [constants.GET_CORE_DROPDOWN_REQUEST]: getCoreDropdownRequest,
    [constants.GET_CORE_DROPDOWN_SUCCESS]: getCoreDropdownSuccess,
    [constants.GET_CORE_DROPDOWN_CLEAR]: getCoreDropdownClear,
    [constants.GET_CORE_DROPDOWN_ERROR]: getCoreDropdownError,

    [constants.SEARCH_INVOICE_DATA_REQUEST]: searchInvoiceDataRequest,
    [constants.SEARCH_INVOICE_DATA_SUCCESS]: searchInvoiceDataSuccess,
    [constants.SEARCH_INVOICE_DATA_CLEAR]: searchInvoiceDataClear,
    [constants.SEARCH_INVOICE_DATA_ERROR]: searchInvoiceDataError,

    [constants.CREATE_INVOICE_REQUEST]: createInvoiceRequest,
    [constants.CREATE_INVOICE_SUCCESS]: createInvoiceSuccess,
    [constants.CREATE_INVOICE_CLEAR]: createInvoiceClear,
    [constants.CREATE_INVOICE_ERROR]: createInvoiceError,

}, initialState);